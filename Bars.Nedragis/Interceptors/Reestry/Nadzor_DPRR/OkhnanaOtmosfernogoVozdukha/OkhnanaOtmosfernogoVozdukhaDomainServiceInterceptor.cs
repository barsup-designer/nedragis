namespace Bars.Nedragis
{
    using Bars.B4.Modules.Scripting.Interfaces;
    using Bars.B4.Modules.Scripting.Services;
    using System;
    using Bars.B4;

    internal class OkhnanaOtmosfernogoVozdukhaDomainServiceInterceptor : EmptyDomainInterceptor<OkhnanaOtmosfernogoVozdukha>
    {
        private readonly IScriptExecutor _scriptExecutor;
        public OkhnanaOtmosfernogoVozdukhaDomainServiceInterceptor(IScriptExecutor scriptExecutor)
        {
            _scriptExecutor = scriptExecutor;
        }

        private void RunScript(OkhnanaOtmosfernogoVozdukha entity, string scriptResourceName, string eventDisplayName)
        {
            var scriptContext = new BaseScriptContext(Container);
            scriptContext.SetValue("entity", entity);
            try
            {
                _scriptExecutor.RunFromResource(scriptContext, scriptResourceName, typeof (OkhnanaOtmosfernogoVozdukha).Assembly);
            }
            catch (Exception exc)
            {
                throw new ApplicationException($"Не удалось выполнить скрипт события сущности\"{eventDisplayName}\":<br>" + exc.Message);
            }
        }
    }
}