namespace Bars.Nedragis
{
    using Bars.B4.Modules.Scripting.Interfaces;
    using Bars.B4.Modules.Scripting.Services;
    using System;
    using Bars.B4;

    internal class VidRabotOPINFDomainServiceInterceptor : EmptyDomainInterceptor<VidRabotOPINF>
    {
        private readonly IScriptExecutor _scriptExecutor;
        public VidRabotOPINFDomainServiceInterceptor(IScriptExecutor scriptExecutor)
        {
            _scriptExecutor = scriptExecutor;
        }

        private void RunScript(VidRabotOPINF entity, string scriptResourceName, string eventDisplayName)
        {
            var scriptContext = new BaseScriptContext(Container);
            scriptContext.SetValue("entity", entity);
            try
            {
                _scriptExecutor.RunFromResource(scriptContext, scriptResourceName, typeof (VidRabotOPINF).Assembly);
            }
            catch (Exception exc)
            {
                throw new ApplicationException($"Не удалось выполнить скрипт события сущности\"{eventDisplayName}\":<br>" + exc.Message);
            }
        }
    }
}