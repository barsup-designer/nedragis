namespace Bars.Nedragis.Navigation.Providers
{
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.Security;
    using Bars.B4.Utils;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Navigation;
    using Bars.Rms.GeneratedApp;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Bars.B4.Cache;

    /// <summary>
    /// Поставщик узлов панели навигации
    /// </summary>
    public class NavContainer5d03704ccb054f0b9663371e87c63d7fProvider : NavigationPanelNodesProvider<Bars.Nedragis.Dokument>
    {
        /// <summary>
        /// Ключ, для которого формируются элементы
        /// </summary>
        public override string Key
        {
            get
            {
                return "5d03704c-cb05-4f0b-9663-371e87c63d7f";
            }
        }

        /// <summary>
        /// Формирование списка элементов дерева
        /// </summary>
        public override void FillSructure(BaseParams @params, IList<NavigationPanelNode> structure)
        {
            var entityId = @params.Params.GetAs<long ? >("EntityId");
            var entity = entityId.HasValue && entityId > 0 ? Service.Get(entityId) : null;
            // js-представление фильтра
            string dataFilterJs = null;
            // объектное представление фильтра
            DataFilter dataFilter = null;
            //-------
            structure.Add(new NavigationPanelNode($"479cef53-1bc7-a5ab-f4fc-7b2e174f75bb-{entity.Return(v => v.Id)}")
            { // устанавливаем параметры контекста
            ContextProperties = DynamicDictionary.Create().SetValue("Dokument_Id", entityId), // устанавливаем параметры представления
 ViewProperties = DynamicDictionary.Create(), ElementUid = "dad78d3b-5d89-415d-894d-6b66e61061d3", Action = null, Controller = "B4.controller.DokumentEditor_Copy_1", Name = "Основные сведения Документ ", EntityId = entity.Return(v => v.Id), IconCls = null, Key = null, SelectByDefault = false, Leaf = true});
            //-------
            dataFilterJs = "{'Group':3,'Operand':0,'DataIndex':null,'DataIndexType':null,'Value':null,'Filters':[{'Group':0,'Operand':0,'DataIndex':'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$b0627770-34ec-4c22-8112-78c21088ef09$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id','DataIndexType':null,'Value':'@Dokument_Id','Filters':null}]}";
            // заменяем ключи фильтра на значения
            // по-хорошему нужно генерировать класс-хелпер, но...
            dataFilterJs = dataFilterJs.Replace("'@Dokument_Id'", entity.Return(v => v.Id).Return(v => v.ToString()));
            // десериализуем полученный фильтр
            dataFilter = JsonNetConvert.DeserializeObject<DataFilter>(dataFilterJs);
            // формируем параметры представления
            if (entity.IsNotNull())
            {
                structure.Add(new NavigationPanelNode($"3532235e-1cc4-0f5d-7897-0c094457aa77-{entityId.GetValueOrDefault()}")
                { // устанавливаем параметры контекста
                ContextProperties = DynamicDictionary.Create().SetValue("Dokument_Id", entityId).SetValue("doc1_Id", entityId.GetValueOrDefault()), // устанавливаем параметры представления
 ViewProperties = DynamicDictionary.Create().SetValue("$EntityFilter.NavFilter", dataFilter), ElementUid = "53043749-60bb-4972-b036-e9a6a5d4cb67", Action = null, Controller = "B4.controller.DokumentyZUList", Name = "Земельные участки", EntityId = entityId.GetValueOrDefault(), IconCls = null, Key = null, SelectByDefault = false, Leaf = true});
            }

            //-------
            dataFilterJs = "{'Group':3,'Operand':0,'DataIndex':null,'DataIndexType':null,'Value':null,'Filters':[{'Group':0,'Operand':0,'DataIndex':'FieldPath://55caa4f7-4f96-47d2-a66a-4d16b044a7cf$8262a22a-9149-4329-98ec-407a445b09be$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id','DataIndexType':null,'Value':'@Dokument_Id','Filters':null}]}";
            // заменяем ключи фильтра на значения
            // по-хорошему нужно генерировать класс-хелпер, но...
            dataFilterJs = dataFilterJs.Replace("'@Dokument_Id'", entity.Return(v => v.Id).Return(v => v.ToString()));
            // десериализуем полученный фильтр
            dataFilter = JsonNetConvert.DeserializeObject<DataFilter>(dataFilterJs);
            // формируем параметры представления
            if (entity.IsNotNull())
            {
                structure.Add(new NavigationPanelNode($"9a168f51-08c2-6ebe-ec11-0a6c5d67a7df-{entityId.GetValueOrDefault()}")
                { // устанавливаем параметры контекста
                ContextProperties = DynamicDictionary.Create().SetValue("Dokument_Id", entityId).SetValue("Document_Id", entityId.GetValueOrDefault()), // устанавливаем параметры представления
 ViewProperties = DynamicDictionary.Create().SetValue("$EntityFilter.NavFilter", dataFilter), ElementUid = "411d74e8-493a-4fc4-9e66-c64624a2292f", Action = null, Controller = "B4.controller.FajjlPrivjazannyjjKDokumentuList", Name = "Файлы, привязанные к документу", EntityId = entityId.GetValueOrDefault(), IconCls = null, Key = null, SelectByDefault = false, Leaf = true});
            }
        }
    }

    /// <summary>
    /// Поставщик узлов панели навигации
    /// </summary>
    public class NavContainer5304374960bb4972b036e9a6a5d4cb67Provider : NavigationPanelNodesProvider<Bars.Nedragis.Dokument>
    {
        /// <summary>
        /// Ключ, для которого формируются элементы
        /// </summary>
        public override string Key
        {
            get
            {
                return "53043749-60bb-4972-b036-e9a6a5d4cb67";
            }
        }

        /// <summary>
        /// Формирование списка элементов дерева
        /// </summary>
        public override void FillSructure(BaseParams @params, IList<NavigationPanelNode> structure)
        {
            var entityId = @params.Params.GetAs<long ? >("EntityId");
            var entity = entityId.HasValue && entityId > 0 ? Service.Get(entityId) : null;
        }
    }

    /// <summary>
    /// Поставщик узлов панели навигации
    /// </summary>
    public class NavContainer411d74e8493a4fc49e66c64624a2292fProvider : NavigationPanelNodesProvider<Bars.Nedragis.Dokument>
    {
        /// <summary>
        /// Ключ, для которого формируются элементы
        /// </summary>
        public override string Key
        {
            get
            {
                return "411d74e8-493a-4fc4-9e66-c64624a2292f";
            }
        }

        /// <summary>
        /// Формирование списка элементов дерева
        /// </summary>
        public override void FillSructure(BaseParams @params, IList<NavigationPanelNode> structure)
        {
            var entityId = @params.Params.GetAs<long ? >("EntityId");
            var entity = entityId.HasValue && entityId > 0 ? Service.Get(entityId) : null;
        }
    }
}