namespace Bars.Nedragis.Navigation.Providers
{
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.Security;
    using Bars.B4.Utils;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Navigation;
    using Bars.Rms.GeneratedApp;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Bars.B4.Cache;

    /// <summary>
    /// Поставщик узлов панели навигации
    /// </summary>
    public class NavContainer1b976d4c8d774a428a2e6a919a934a8bProvider : NavigationPanelNodesProvider<Bars.Nedragis.ZemelNyemUchastki>
    {
        /// <summary>
        /// Ключ, для которого формируются элементы
        /// </summary>
        public override string Key
        {
            get
            {
                return "1b976d4c-8d77-4a42-8a2e-6a919a934a8b";
            }
        }

        /// <summary>
        /// Формирование списка элементов дерева
        /// </summary>
        public override void FillSructure(BaseParams @params, IList<NavigationPanelNode> structure)
        {
            var entityId = @params.Params.GetAs<long ? >("EntityId");
            var entity = entityId.HasValue && entityId > 0 ? Service.Get(entityId) : null;
            // js-представление фильтра
            string dataFilterJs = null;
            // объектное представление фильтра
            DataFilter dataFilter = null;
            //-------
            if (entity.IsNotNull())
            {
                structure.Add(new NavigationPanelNode($"75f20de4-6b1e-90d7-5ae9-bd2026a9931a-{entity.Return(v => v.Id)}")
                { // устанавливаем параметры контекста
                ContextProperties = DynamicDictionary.Create().SetValue("ZemelNyemUchastki_Id", entityId), // устанавливаем параметры представления
 ViewProperties = DynamicDictionary.Create(), ElementUid = "e1bea8f6-d4b5-40a1-8c8d-59757082edfd", Action = null, Controller = "B4.controller.ZemelNyemUchastkiEditor_Copy_1", Name = "Основные сведения", EntityId = entity.Return(v => v.Id), IconCls = null, Key = null, SelectByDefault = false, Leaf = true});
            }

            //-------
            dataFilterJs = "{'Group':3,'Operand':0,'DataIndex':null,'DataIndexType':null,'Value':null,'Filters':[{'Group':0,'Operand':0,'DataIndex':'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$bac8dff6-5e22-4e24-8a69-c82f1802f8e9$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id','DataIndexType':null,'Value':'@ZemelNyemUchastki_Id','Filters':null}]}";
            // заменяем ключи фильтра на значения
            // по-хорошему нужно генерировать класс-хелпер, но...
            dataFilterJs = dataFilterJs.Replace("'@ZemelNyemUchastki_Id'", entity.Return(v => v.Id).Return(v => v.ToString()));
            // десериализуем полученный фильтр
            dataFilter = JsonNetConvert.DeserializeObject<DataFilter>(dataFilterJs);
            // формируем параметры представления
            if (entity.IsNotNull())
            {
                structure.Add(new NavigationPanelNode($"c0325e2d-610e-da2c-0067-808aed33e599-{entityId.GetValueOrDefault()}")
                { // устанавливаем параметры контекста
                ContextProperties = DynamicDictionary.Create().SetValue("ZemelNyemUchastki_Id", entityId).SetValue("zemuch_Id", entityId.GetValueOrDefault()).SetValue("zemuch_Id", entityId.GetValueOrDefault()), // устанавливаем параметры представления
 ViewProperties = DynamicDictionary.Create().SetValue("$EntityFilter.NavFilter", dataFilter), ElementUid = "df950a51-1512-4d09-81e0-b31e091eddcc", Action = null, Controller = "B4.controller.DokumentyZUList2", Name = "Реестр документов", EntityId = entityId.GetValueOrDefault(), IconCls = null, Key = null, SelectByDefault = false, Leaf = true});
            }

            //-------
            if (entity.IsNotNull())
            {
                structure.Add(new NavigationPanelNode($"b706dae8-3b3b-1c3d-e2f8-ac97f22aaf1a-{entity.Return(v => v.Id)}")
                { // устанавливаем параметры контекста
                ContextProperties = DynamicDictionary.Create().SetValue("ZemelNyemUchastki_Id", entityId), // устанавливаем параметры представления
 ViewProperties = DynamicDictionary.Create(), ElementUid = "68e7833f-3d98-4f39-88e7-e0ac01f6a810", Action = null, Controller = "B4.controller.ZUKarta", Name = "Карта", EntityId = entity.Return(v => v.Id), IconCls = null, Key = null, SelectByDefault = false, Leaf = true});
            }
        }
    }

    /// <summary>
    /// Поставщик узлов панели навигации
    /// </summary>
    public class NavContainerdf950a5115124d0981e0b31e091eddccProvider : NavigationPanelNodesProvider<Bars.Nedragis.ZemelNyemUchastki>
    {
        /// <summary>
        /// Ключ, для которого формируются элементы
        /// </summary>
        public override string Key
        {
            get
            {
                return "df950a51-1512-4d09-81e0-b31e091eddcc";
            }
        }

        /// <summary>
        /// Формирование списка элементов дерева
        /// </summary>
        public override void FillSructure(BaseParams @params, IList<NavigationPanelNode> structure)
        {
            var entityId = @params.Params.GetAs<long ? >("EntityId");
            var entity = entityId.HasValue && entityId > 0 ? Service.Get(entityId) : null;
        }
    }
}