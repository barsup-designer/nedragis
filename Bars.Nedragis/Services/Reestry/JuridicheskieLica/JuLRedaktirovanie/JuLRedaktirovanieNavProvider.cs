namespace Bars.Nedragis.Navigation.Providers
{
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.Security;
    using Bars.B4.Utils;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Navigation;
    using Bars.Rms.GeneratedApp;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Bars.B4.Cache;

    /// <summary>
    /// Поставщик узлов панели навигации
    /// </summary>
    public class NavContainer509910caf47b4029b06a8a5b91d4cdc0Provider : NavigationPanelNodesProvider<Bars.Nedragis.JuridicheskieLica>
    {
        /// <summary>
        /// Ключ, для которого формируются элементы
        /// </summary>
        public override string Key
        {
            get
            {
                return "509910ca-f47b-4029-b06a-8a5b91d4cdc0";
            }
        }

        /// <summary>
        /// Формирование списка элементов дерева
        /// </summary>
        public override void FillSructure(BaseParams @params, IList<NavigationPanelNode> structure)
        {
            var entityId = @params.Params.GetAs<long ? >("EntityId");
            var entity = entityId.HasValue && entityId > 0 ? Service.Get(entityId) : null;
            // js-представление фильтра
            string dataFilterJs = null;
            // объектное представление фильтра
            DataFilter dataFilter = null;
            //-------
            if (entity.IsNotNull())
            {
                structure.Add(new NavigationPanelNode($"555c98d6-edd2-bb9e-b849-c14d2c774046-{entity.Return(v => v.Id)}")
                { // устанавливаем параметры контекста
                ContextProperties = DynamicDictionary.Create().SetValue("JuridicheskieLica_Id", entityId), // устанавливаем параметры представления
 ViewProperties = DynamicDictionary.Create(), ElementUid = "3ef0a36b-3085-4603-a66a-48577f5cc089", Action = null, Controller = "B4.controller.JuLOsnovnyeSvedenija", Name = "Основные сведения", EntityId = entity.Return(v => v.Id), IconCls = null, Key = null, SelectByDefault = false, Leaf = true});
            }

            //-------
            dataFilterJs = "{'Group':3,'Operand':0,'DataIndex':null,'DataIndexType':null,'Value':null,'Filters':[{'Group':0,'Operand':0,'DataIndex':'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$c50abffe-a4aa-4ce8-ae37-e8f78c51a056$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id','DataIndexType':null,'Value':'@JuridicheskieLica_Id','Filters':null}]}";
            // заменяем ключи фильтра на значения
            // по-хорошему нужно генерировать класс-хелпер, но...
            dataFilterJs = dataFilterJs.Replace("'@JuridicheskieLica_Id'", entity.Return(v => v.Id).Return(v => v.ToString()));
            // десериализуем полученный фильтр
            dataFilter = JsonNetConvert.DeserializeObject<DataFilter>(dataFilterJs);
            // формируем параметры представления
            if (entity.IsNotNull())
            {
                structure.Add(new NavigationPanelNode($"94e08ca6-67e4-7fe8-38d1-6702aab77e16-{entityId.GetValueOrDefault()}")
                { // устанавливаем параметры контекста
                ContextProperties = DynamicDictionary.Create().SetValue("JuridicheskieLica_Id", entityId).SetValue("YurLitco_Id", entityId.GetValueOrDefault()), // устанавливаем параметры представления
 ViewProperties = DynamicDictionary.Create().SetValue("$EntityFilter.NavFilter", dataFilter), ElementUid = "cd41d47b-6abf-4c2a-8d0e-c68d48bfb98a", Action = null, Controller = "B4.controller.RukovoditeliList", Name = "Руководители", EntityId = entityId.GetValueOrDefault(), IconCls = null, Key = null, SelectByDefault = false, Leaf = true});
            }

            //-------
            if (entity.IsNotNull())
            {
                structure.Add(new NavigationPanelNode($"a9803618-e722-7629-271e-5271a5b1a678-{entityId.GetValueOrDefault()}")
                { // устанавливаем параметры контекста
                ContextProperties = DynamicDictionary.Create().SetValue("JuridicheskieLica_Id", entityId).SetValue("Subject_Id", entityId.GetValueOrDefault()), // устанавливаем параметры представления
 ViewProperties = DynamicDictionary.Create(), ElementUid = "c6aac255-359b-447b-8bb9-101dcae25c36", Action = null, Controller = "B4.controller.DokumentList", Name = "Документы", EntityId = entityId.GetValueOrDefault(), IconCls = null, Key = null, SelectByDefault = false, Leaf = true});
            }
        }
    }

    /// <summary>
    /// Поставщик узлов панели навигации
    /// </summary>
    public class NavContainercd41d47b6abf4c2a8d0ec68d48bfb98aProvider : NavigationPanelNodesProvider<Bars.Nedragis.JuridicheskieLica>
    {
        /// <summary>
        /// Ключ, для которого формируются элементы
        /// </summary>
        public override string Key
        {
            get
            {
                return "cd41d47b-6abf-4c2a-8d0e-c68d48bfb98a";
            }
        }

        /// <summary>
        /// Формирование списка элементов дерева
        /// </summary>
        public override void FillSructure(BaseParams @params, IList<NavigationPanelNode> structure)
        {
            var entityId = @params.Params.GetAs<long ? >("EntityId");
            var entity = entityId.HasValue && entityId > 0 ? Service.Get(entityId) : null;
        }
    }

    /// <summary>
    /// Поставщик узлов панели навигации
    /// </summary>
    public class NavContainerc6aac255359b447b8bb9101dcae25c36Provider : NavigationPanelNodesProvider<Bars.Nedragis.JuridicheskieLica>
    {
        /// <summary>
        /// Ключ, для которого формируются элементы
        /// </summary>
        public override string Key
        {
            get
            {
                return "c6aac255-359b-447b-8bb9-101dcae25c36";
            }
        }

        /// <summary>
        /// Формирование списка элементов дерева
        /// </summary>
        public override void FillSructure(BaseParams @params, IList<NavigationPanelNode> structure)
        {
            var entityId = @params.Params.GetAs<long ? >("EntityId");
            var entity = entityId.HasValue && entityId > 0 ? Service.Get(entityId) : null;
        }
    }
}