namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности ОКАТО
    /// </summary>
    public class OKATOMap : ClassMapping<Bars.Nedragis.OKATO>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public OKATOMap()
        {
            Table("OKATO");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: OKATO
            Property(x => x.code, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_CODE");
                }

                );
                p.Unique(true);
            }

            );
            Property(x => x.name1, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_NAME1");
                }

                );
            }

            );
            Property(x => x.center_name, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_CENTER_NAME");
                }

                );
            }

            );
            ManyToOne(x => x.parent_id, m =>
            {
                m.Column("F_PARENT_ID");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}