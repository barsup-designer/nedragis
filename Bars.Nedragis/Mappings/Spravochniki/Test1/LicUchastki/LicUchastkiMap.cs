namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Лицензионные участки
    /// </summary>
    public class LicUchastkiMap : ClassMapping<Bars.Nedragis.LicUchastki>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public LicUchastkiMap()
        {
            Table("LICUCHASTKI");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: LicUchastki
            Property(x => x.name, p =>
            {
                p.Column(col =>
                {
                    col.Name("NAME");
                }

                );
            }

            );
            Property(x => x.Element1478588881912, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478588881912");
                }

                );
            }

            );
            Property(x => x.Element1478589271306, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478589271306");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}