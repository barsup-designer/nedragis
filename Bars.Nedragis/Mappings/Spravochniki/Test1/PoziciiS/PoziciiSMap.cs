namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Позиции спецификации
    /// </summary>
    public class PoziciiSMap : ClassMapping<Bars.Nedragis.PoziciiS>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public PoziciiSMap()
        {
            Table("POZICIIS");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: PoziciiS
            Property(x => x.name, p =>
            {
                p.Column(col =>
                {
                    col.Name("NAME");
                }

                );
            }

            );
            Property(x => x.s_pod, p =>
            {
                p.Column(col =>
                {
                    col.Name("S_POD");
                }

                );
            }

            );
            Property(x => x.s_hran, p =>
            {
                p.Column(col =>
                {
                    col.Name("S_HRAN");
                }

                );
            }

            );
            Property(x => x.naklad, p =>
            {
                p.Column(col =>
                {
                    col.Name("NAKLAD");
                }

                );
            }

            );
            Property(x => x.nds, p =>
            {
                p.Column(col =>
                {
                    col.Name("NDS");
                }

                );
            }

            );
            Property(x => x.Element1478602748211, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478602748211");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}