namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Позиции спецификации 2
    /// </summary>
    public class PoziciiSpecifikacii2Map : ClassMapping<Bars.Nedragis.PoziciiSpecifikacii2>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public PoziciiSpecifikacii2Map()
        {
            Table("POZICIISPECIFIKACII2");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: PoziciiSpecifikacii2
            Property(x => x.Element1478603373738, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478603373738");
                }

                );
            }

            );
            Property(x => x.Element1478603394415, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478603394415");
                }

                );
            }

            );
            Property(x => x.Element1478603416645, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478603416645");
                }

                );
            }

            );
            Property(x => x.Element1478603450746, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478603450746");
                }

                );
            }

            );
            Property(x => x.Element1478603494464, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478603494464");
                }

                );
            }

            );
            Property(x => x.Element1478603523493, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478603523493");
                }

                );
            }

            );
            Property(x => x.Element1478603543580, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478603543580");
                }

                );
            }

            );
            Property(x => x.Element1478603563538, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478603563538");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}