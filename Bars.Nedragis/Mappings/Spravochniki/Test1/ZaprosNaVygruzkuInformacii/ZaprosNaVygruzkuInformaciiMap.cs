namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Результат выгрузки информации
    /// </summary>
    public class ZaprosNaVygruzkuInformaciiMap : ClassMapping<Bars.Nedragis.ZaprosNaVygruzkuInformacii>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ZaprosNaVygruzkuInformaciiMap()
        {
            Table("ZAPROSNAVYGRUZKUINFORMACII");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: ZaprosNaVygruzkuInformacii
            Property(x => x.Element1478675644658, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478675644658");
                }

                );
            }

            );
            Property(x => x.Element1478675679144, p => p.Formula("Договор и прило"));
            Property(x => x.Element1478682384618, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478682384618");
                }

                );
            }

            );
            Property(x => x.Element1478682415266, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478682415266");
                }

                );
            }

            );
            Property(x => x.Element1478682460890, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478682460890");
                }

                );
            }

            );
            Property(x => x.Element1478682508834, p => p.Formula("Запрос на выгруз"));
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}