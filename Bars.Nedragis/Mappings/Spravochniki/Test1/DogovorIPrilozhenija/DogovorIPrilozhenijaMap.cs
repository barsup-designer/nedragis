namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Договор и приложения
    /// </summary>
    public class DogovorIPrilozhenijaMap : ClassMapping<Bars.Nedragis.DogovorIPrilozhenija>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public DogovorIPrilozhenijaMap()
        {
            Table("RESHENIEPOPRAVUPRIOB");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: DogovorIPrilozhenija
            Property(x => x.Element1478683081624, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478683081624");
                }

                );
            }

            );
            Property(x => x.Element1478683584922, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478683584922");
                }

                );
            }

            );
            Property(x => x.Element1478683617170, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478683617170");
                }

                );
            }

            );
            Property(x => x.Element1478683683665, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478683683665");
                }

                );
            }

            );
            Property(x => x.Element1478683765504, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478683765504");
                }

                );
            }

            );
            Property(x => x.Element1478683832391, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478683832391");
                }

                );
            }

            );
            Property(x => x.Element1478683872055, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478683872055");
                }

                );
            }

            );
            Property(x => x.Element1478683916351, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478683916351");
                }

                );
            }

            );
            Property(x => x.Element1478683945974, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478683945974");
                }

                );
            }

            );
            Property(x => x.Element1478684021941, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478684021941");
                }

                );
            }

            );
            Property(x => x.Element1478684113244, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478684113244");
                }

                );
            }

            );
            Property(x => x.Element1478684143764, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478684143764");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}