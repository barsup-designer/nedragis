namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Недропользователь
    /// </summary>
    public class NedropolZovatelMap : ClassMapping<Bars.Nedragis.NedropolZovatel>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public NedropolZovatelMap()
        {
            Table("NEDROPOLZOVATEL");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: NedropolZovatel
            Property(x => x.name, p =>
            {
                p.Column(col =>
                {
                    col.Name("NAME");
                }

                );
            }

            );
            Property(x => x.adres, p =>
            {
                p.Column(col =>
                {
                    col.Name("ADRES");
                }

                );
            }

            );
            Property(x => x.tel, p =>
            {
                p.Column(col =>
                {
                    col.Name("TEL");
                }

                );
            }

            );
            Property(x => x.mail, p =>
            {
                p.Column(col =>
                {
                    col.Name("MAIL");
                }

                );
            }

            );
            Property(x => x.fax, p =>
            {
                p.Column(col =>
                {
                    col.Name("FAX");
                }

                );
            }

            );
            Property(x => x.member, p =>
            {
                p.Column(col =>
                {
                    col.Name("MEMBER");
                }

                );
            }

            );
            Property(x => x.inn, p =>
            {
                p.Column(col =>
                {
                    col.Name("INN");
                }

                );
            }

            );
            Property(x => x.kpp, p =>
            {
                p.Column(col =>
                {
                    col.Name("KPP");
                }

                );
            }

            );
            Property(x => x.rs, p =>
            {
                p.Column(col =>
                {
                    col.Name("RS");
                }

                );
            }

            );
            Property(x => x.bik, p =>
            {
                p.Column(col =>
                {
                    col.Name("BIK");
                }

                );
            }

            );
            Property(x => x.kors, p =>
            {
                p.Column(col =>
                {
                    col.Name("KORS");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}