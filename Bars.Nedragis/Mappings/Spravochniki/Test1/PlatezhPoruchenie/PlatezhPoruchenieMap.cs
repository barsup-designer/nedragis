namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Платежное поручение
    /// </summary>
    public class PlatezhPoruchenieMap : ClassMapping<Bars.Nedragis.PlatezhPoruchenie>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public PlatezhPoruchenieMap()
        {
            Table("PLATEZHPORUCHENIE");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: PlatezhPoruchenie
            Property(x => x.dattr, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATTR");
                }

                );
            }

            );
            Property(x => x.suma, p =>
            {
                p.Column(col =>
                {
                    col.Name("SUMA");
                }

                );
            }

            );
            Property(x => x.Element1478594361589, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478594361589");
                }

                );
            }

            );
            Property(x => x.dataa, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATAA");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}