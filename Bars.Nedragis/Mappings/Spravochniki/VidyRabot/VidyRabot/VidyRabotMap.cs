namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Виды работ
    /// </summary>
    public class VidyRabotMap : ClassMapping<Bars.Nedragis.VidyRabot>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public VidyRabotMap()
        {
            Table("VIDYRABOT");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: VidyRabot
            Property(x => x.Element1474020299386, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1474020299386");
                }

                );
            }

            );
            Bag(x => x.Element1474349449685, bag =>
            {
                bag.Table("PODVIDY");
                bag.Key(k => k.Column("ELEMENT1474349306046"));
                bag.Inverse(true);
                bag.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }

            , m => m.OneToMany(r => r.Class(typeof (Bars.Nedragis.Podvidy))));
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}