namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Документы удостоверяющие личность
    /// </summary>
    public class DokumentyUdostoverjajushhieLichnostMap : ClassMapping<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public DokumentyUdostoverjajushhieLichnostMap()
        {
            Table("DOKUMENTYUDOSTOVERJAJUSHHIELICHNOST");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: DokumentyUdostoverjajushhieLichnost
            Property(x => x.code, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_CODE");
                }

                );
            }

            );
            Property(x => x.name1, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_NAME1");
                }

                );
            }

            );
            Property(x => x.description, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_DESCRIPTION");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}