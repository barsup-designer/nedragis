namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Руководители
    /// </summary>
    public class RukovoditeliMap : ClassMapping<Bars.Nedragis.Rukovoditeli>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public RukovoditeliMap()
        {
            Table("RUKOVODITELI");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Rukovoditeli
            ManyToOne(x => x.YurLitco, m =>
            {
                m.Column("F_YURLITCO");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Surname, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_SURNAME");
                }

                );
            }

            );
            Property(x => x.FirstName, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_FIRSTNAME");
                }

                );
            }

            );
            Property(x => x.Patronymic, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_PATRONYMIC");
                }

                );
            }

            );
            Property(x => x.Sex, p =>
            {
                p.Column(cm =>
                {
                    cm.Name("F_SEX");
                    cm.Default(1);
                }

                );
            }

            );
            ManyToOne(x => x.PostType, m =>
            {
                m.Column("F_POSTTYPE");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.PostText, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_POSTTEXT");
                }

                );
            }

            );
            Property(x => x.Basis, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_BASIS");
                }

                );
            }

            );
            Property(x => x.Phone, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_PHONE");
                }

                );
            }

            );
            ManyToOne(x => x.udostovereniye, m =>
            {
                m.Column("F_UDOSTOVERENIYE");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}