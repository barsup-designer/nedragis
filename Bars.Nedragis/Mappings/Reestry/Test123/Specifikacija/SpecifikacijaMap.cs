namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Спецификация
    /// </summary>
    public class SpecifikacijaMap : ClassMapping<Bars.Nedragis.Specifikacija>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public SpecifikacijaMap()
        {
            Table("SPECIFIKACIJA");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Specifikacija
            Property(x => x.nomsp, p =>
            {
                p.Column(col =>
                {
                    col.Name("NOMSP");
                }

                );
            }

            );
            Property(x => x.Element1478601989143, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478601989143");
                }

                );
            }

            );
            Property(x => x.Element1478602041541, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478602041541");
                }

                );
            }

            );
            Property(x => x.Element1478602079636, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478602079636");
                }

                );
            }

            );
            Property(x => x.Element1478602280592, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478602280592");
                }

                );
            }

            );
            Property(x => x.Element1478607128570, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478607128570");
                }

                );
            }

            );
            ManyToOne(x => x.Element1478607204176, m =>
            {
                m.Column("ELEMENT1478607204176");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}