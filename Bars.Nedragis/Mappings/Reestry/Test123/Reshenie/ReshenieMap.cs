namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Решение по праву приобретения информации
    /// </summary>
    public class ReshenieMap : ClassMapping<Bars.Nedragis.Reshenie>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ReshenieMap()
        {
            Table("RESHENIE");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Reshenie
            Property(x => x.Element1478752620339, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478752620339");
                }

                );
            }

            );
            Property(x => x.Element1478752748169, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478752748169");
                }

                );
            }

            );
            Property(x => x.Element1478752768753, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478752768753");
                }

                );
            }

            );
            Property(x => x.Element1478752791585, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478752791585");
                }

                );
            }

            );
            Property(x => x.Element1478752831192, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478752831192");
                }

                );
            }

            );
            Property(x => x.Element1478752856240, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478752856240");
                }

                );
            }

            );
            Property(x => x.Element1478752884224, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478752884224");
                }

                );
            }

            );
            Property(x => x.Element1478753246124, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478753246124");
                }

                );
            }

            );
            Property(x => x.Element1478753305771, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478753305771");
                }

                );
            }

            );
            Property(x => x.Element1478753329859, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1478753329859");
                }

                );
            }

            );
            ManyToOne(x => x.Element1478753351594, m =>
            {
                m.Column("ELEMENT1478753351594");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}