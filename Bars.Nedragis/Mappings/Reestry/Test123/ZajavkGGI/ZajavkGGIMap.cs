namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Заявка на приобретение ГГИ
    /// </summary>
    public class ZajavkGGIMap : ClassMapping<Bars.Nedragis.ZajavkGGI>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ZajavkGGIMap()
        {
            Table("ZAJAVKGGI");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: ZajavkGGI
            Property(x => x.nomz, p =>
            {
                p.Column(col =>
                {
                    col.Name("NOMZ");
                }

                );
            }

            );
            Property(x => x.dataz, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATAZ");
                }

                );
            }

            );
            Property(x => x.dati, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATI");
                }

                );
            }

            );
            ManyToOne(x => x.NedropolZovatel, m =>
            {
                m.Column("NEDR");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.luch, m =>
            {
                m.Column("LUCH");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.texto, p =>
            {
                p.Column(col =>
                {
                    col.Name("TEXTO");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}