namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Объекты права
    /// </summary>
    public class ObEktyPravaMap : ClassMapping<Bars.Nedragis.ObEktyPrava>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ObEktyPravaMap()
        {
            Table("OBEKTYPRAVA");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: ObEktyPrava
            Property(x => x.Representation, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_REPRESENTATION");
                }

                );
            }

            );
            ManyToOne(x => x.type, m =>
            {
                m.Column("F_TYPE");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.owner_id, m =>
            {
                m.Column("F_OWNER_ID");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.holder_id, m =>
            {
                m.Column("F_HOLDER_ID");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.property_manager_id, m =>
            {
                m.Column("F_PROPERTY_MANAGER_ID");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.Status_obj, m =>
            {
                m.Column("F_STATUS_OBJ");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.typeobj, m =>
            {
                m.Column("F_TYPEOBJ");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}