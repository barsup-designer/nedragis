namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Реестр природопользования
    /// </summary>
    public class ReestrPrirodopolZovanijaMap : ClassMapping<Bars.Nedragis.ReestrPrirodopolZovanija>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ReestrPrirodopolZovanijaMap()
        {
            Table("REESTRPRIRODA");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: ReestrPrirodopolZovanija
            ManyToOne(x => x.name_otdel_pr, m =>
            {
                m.Column("NAME_OTDEL_PR");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.vid_rabot_pr, m =>
            {
                m.Column("VID_RABOT_PR");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.kolvo_pr, p =>
            {
                p.Column(col =>
                {
                    col.Name("KOLVO_PR");
                }

                );
            }

            );
            Property(x => x.date_vh_pr, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATE_VH_PR");
                }

                );
            }

            );
            Property(x => x.date_isp_pr, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATE_ISP_PR");
                }

                );
            }

            );
            ManyToOne(x => x.org_vlast_pr, m =>
            {
                m.Column("ORG_VLAST_PR");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.object_pr, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_PR");
                }

                );
            }

            );
            ManyToOne(x => x.ispoln_pr, m =>
            {
                m.Column("ISPOLN_PR");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.prim_pr, p =>
            {
                p.Column(col =>
                {
                    col.Name("PRIM_PR");
                }

                );
            }

            );
            Property(x => x.nom_dogovor_pr, p =>
            {
                p.Column(col =>
                {
                    col.Name("NOM_DOGOVOR_PR");
                }

                );
            }

            );
            Property(x => x.sum_oplata_pr, p =>
            {
                p.Column(col =>
                {
                    col.Name("SUM_OPLATA_PR");
                }

                );
            }

            );
            Property(x => x.id_nom_pr, p =>
            {
                p.Column(col =>
                {
                    col.Name("ID_NOM_PR");
                }

                );
            }

            );
            Property(x => x.n_vh_pr, p =>
            {
                p.Column(col =>
                {
                    col.Name("N_VH_PR");
                }

                );
            }

            );
            ManyToOne(x => x.o_month_pr, m =>
            {
                m.Column("O_MONTH_PR");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.P_rabot_pr, m =>
            {
                m.Column("P_RABOT_PR");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}