namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Природопользование 2
    /// </summary>
    public class PrirodopolZovanie2Map : ClassMapping<Bars.Nedragis.PrirodopolZovanie2>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public PrirodopolZovanie2Map()
        {
            Table("PRIRODOPOLZOVANIE2");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: PrirodopolZovanie2
            Property(x => x.DateInPR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATEINPR2");
                }

                );
            }

            );
            Property(x => x.NumInDocsPr2, p =>
            {
                p.Column(col =>
                {
                    col.Name("NUMINDOCSPR2");
                }

                );
            }

            );
            Property(x => x.DateCompletePR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATECOMPLETEPR2");
                }

                );
            }

            );
            Property(x => x.CompanyPR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("COMPANYPR2");
                }

                );
            }

            );
            ManyToOne(x => x.VidRabotPR2, m =>
            {
                m.Column("VIDRABOTPR2");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.QuantityPR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("QUANTITYPR2");
                }

                );
            }

            );
            Property(x => x.ObjectPR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECTPR2");
                }

                );
            }

            );
            Property(x => x.NumDogovorPR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("NUMDOGOVORPR2");
                }

                );
            }

            );
            Property(x => x.NumZayvkaPR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("NUMZAYVKAPR2");
                }

                );
            }

            );
            Property(x => x.CodePR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("CODEPR2");
                }

                );
            }

            );
            Property(x => x.SummToPayPR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("SUMMTOPAYPR2");
                }

                );
            }

            );
            Property(x => x.IdentNumderPR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("IDENTNUMDERPR2");
                }

                );
            }

            );
            ManyToOne(x => x.IspolnitelPR2, m =>
            {
                m.Column("ISPOLNITELPR2");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.MonthPR2, m =>
            {
                m.Column("MONTHPR2");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.FIOZakazchikaPR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("FIOZAKAZCHIKAPR2");
                }

                );
            }

            );
            Property(x => x.PrimechaniePR2, p =>
            {
                p.Column(col =>
                {
                    col.Name("PRIMECHANIEPR2");
                }

                );
            }

            );
            ManyToOne(x => x.SoispolnitelPR2, m =>
            {
                m.Column("SOISPOLNITELPR2");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}