namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Земельные участки
    /// </summary>
    public class ZemelNyemUchastkiMap : JoinedSubclassMapping<Bars.Nedragis.ZemelNyemUchastki>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ZemelNyemUchastkiMap()
        {
            Table("ZEMELNYEMUCHASTKI");
            Key(k => k.Column("id"));
#region Class: ZemelNyemUchastki
            Property(x => x.name, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_NAME");
                }

                );
            }

            );
            ManyToOne(x => x.category, m =>
            {
                m.Column("F_CATEGORY");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.vid_use, m =>
            {
                m.Column("F_VID_USE");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.vid_prava, m =>
            {
                m.Column("F_VID_PRAVA");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.area, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_AREA");
                }

                );
            }

            );
            Property(x => x.naznach, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_NAZNACH");
                }

                );
            }

            );
            Property(x => x.MapID, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_MAPID");
                }

                );
            }

            );
            Property(x => x.cad_okrug, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_CAD_OKRUG");
                }

                );
            }

            );
            Property(x => x.cad_raion, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_CAD_RAION");
                }

                );
            }

            );
            Property(x => x.cad_block, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_CAD_BLOCK");
                }

                );
            }

            );
            Property(x => x.cad_massiv, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_CAD_MASSIV");
                }

                );
            }

            );
            Property(x => x.cad_kvartal, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_CAD_KVARTAL");
                }

                );
            }

            );
            Property(x => x.number_ZU, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_NUMBER_ZU");
                }

                );
            }

            );
            ManyToOne(x => x.okato, m =>
            {
                m.Column("F_OKATO");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.oktmo, m =>
            {
                m.Column("F_OKTMO");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Borders, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_BORDERS");
                }

                );
            }

            );
            Property(x => x.cadnum1, p => p.Formula("F_CAD_OKRUG||\':\'|| F_CAD_RAION||\':\'|| F_CAD_BLOCK|| F_CAD_MASSIV|| F_CAD_KVARTAL||\':\'|| F_NUMBER_ZU"));
            Bag(x => x.doc, bag =>
            {
                bag.Table("DOKUMENTYZU");
                bag.Key(k => k.Column("F_ZEMUCH"));
                bag.Inverse(true);
                bag.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }

            , m => m.OneToMany(r => r.Class(typeof (Bars.Nedragis.DokumentyZU))));
            ManyToOne(x => x.bremya, m =>
            {
                m.Column("F_BREMYA");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.TypeZU, m =>
            {
                m.Column("F_TYPEZU");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
        }
    }
}