namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Отходы
    /// </summary>
    public class OtkhodyMap : ClassMapping<Bars.Nedragis.Otkhody>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public OtkhodyMap()
        {
            Table("OTKHODY");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Otkhody
            Property(x => x.Element1507790021761, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507790021761");
                }

                );
            }

            );
            Property(x => x.Element1507791662904, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507791662904");
                }

                );
            }

            );
            Property(x => x.Element1507791696742, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507791696742");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}