namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Реестр объектов
    /// </summary>
    public class ReestrObEktovMap : ClassMapping<Bars.Nedragis.ReestrObEktov>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ReestrObEktovMap()
        {
            Table("REESTROBEKTOV");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: ReestrObEktov
            ManyToOne(x => x.Element1507784253669, m =>
            {
                m.Column("ELEMENT1507784253669");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Element1507784343465, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507784343465");
                }

                );
            }

            );
            Property(x => x.Element1507784366035, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507784366035");
                }

                );
                p.Unique(true);
            }

            );
            Bag(x => x.Element1508137072874, bag =>
            {
                bag.Table("OBEKTOPI");
                bag.Key(k => k.Column("ELEMENT1508136885094"));
                bag.Inverse(true);
                bag.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }

            , m => m.OneToMany(r => r.Class(typeof (Bars.Nedragis.ObEktopi))));
            Bag(x => x.Element1508151176209, bag =>
            {
                bag.Table("m2m_ReestrObEktov_Element1508151176209");
                bag.Key(k => k.Column("parent_id"));
                bag.Inverse(false);
                bag.Cascade(Cascade.None);
            }

            , m => m.ManyToMany(r => r.Column("child_id")));
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}