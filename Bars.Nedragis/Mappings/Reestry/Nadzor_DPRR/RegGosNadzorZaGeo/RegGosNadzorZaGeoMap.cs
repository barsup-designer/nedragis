namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности ОПИ
    /// </summary>
    public class RegGosNadzorZaGeoMap : ClassMapping<Bars.Nedragis.RegGosNadzorZaGeo>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public RegGosNadzorZaGeoMap()
        {
            Table("REGGOSNADZORZAGEO");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: RegGosNadzorZaGeo
            Property(x => x.Element1507635501632, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507635501632");
                }

                );
            }

            );
            Property(x => x.Element1507635537755, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507635537755");
                }

                );
            }

            );
            Property(x => x.Element1507635559781, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507635559781");
                }

                );
            }

            );
            Property(x => x.Element1507635586209, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507635586209");
                }

                );
            }

            );
            Bag(x => x.Element1508137368182, bag =>
            {
                bag.Table("OBEKTOPI");
                bag.Key(k => k.Column("ELEMENT1508137003934"));
                bag.Inverse(true);
                bag.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }

            , m => m.OneToMany(r => r.Class(typeof (Bars.Nedragis.ObEktopi))));
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}