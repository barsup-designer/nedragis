namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Вода
    /// </summary>
    public class RegionalNyjjNadzorVodnykhObEktovMap : ClassMapping<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public RegionalNyjjNadzorVodnykhObEktovMap()
        {
            Table("REGIONALNYJJNADZORVODNYKHOBEKTOV");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: RegionalNyjjNadzorVodnykhObEktov
            Property(x => x.Element1507787714216, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507787714216");
                }

                );
            }

            );
            Property(x => x.Element1507787771917, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507787771917");
                }

                );
            }

            );
            Property(x => x.Element1507787816822, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507787816822");
                }

                );
            }

            );
            Property(x => x.Element1507787841624, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507787841624");
                }

                );
            }

            );
            ManyToOne(x => x.Element1508150872914, m =>
            {
                m.Column("ELEMENT1508150872914");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}