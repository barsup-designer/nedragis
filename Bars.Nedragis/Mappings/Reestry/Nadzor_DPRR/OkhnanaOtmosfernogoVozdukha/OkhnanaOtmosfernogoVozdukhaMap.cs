namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Воздух
    /// </summary>
    public class OkhnanaOtmosfernogoVozdukhaMap : ClassMapping<Bars.Nedragis.OkhnanaOtmosfernogoVozdukha>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public OkhnanaOtmosfernogoVozdukhaMap()
        {
            Table("OKHNANAOTMOSFERNOGOVOZDUKHA");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: OkhnanaOtmosfernogoVozdukha
            Property(x => x.Element1507788951680, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507788951680");
                }

                );
            }

            );
            Property(x => x.Element1507788999087, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507788999087");
                }

                );
            }

            );
            Property(x => x.Element1507789018715, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507789018715");
                }

                );
            }

            );
            Property(x => x.Element1507789044428, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507789044428");
                }

                );
            }

            );
            Property(x => x.Element1507791810720, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507791810720");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}