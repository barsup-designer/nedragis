namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Объекты регионального надзора
    /// </summary>
    public class NadzorMap : ClassMapping<Bars.Nedragis.Nadzor>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public NadzorMap()
        {
            Table("NADZOR");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Nadzor
            Property(x => x.nomer, p =>
            {
                p.Column(col =>
                {
                    col.Name("NOMER");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.inn, p =>
            {
                p.Column(col =>
                {
                    col.Name("INN");
                }

                );
            }

            );
            Property(x => x.Element1507010532348, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507010532348");
                }

                );
            }

            );
            Property(x => x.date, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATE");
                }

                );
            }

            );
            Property(x => x.srok_end, p =>
            {
                p.Column(col =>
                {
                    col.Name("SROK_END");
                }

                );
            }

            );
            Property(x => x.mesto, p =>
            {
                p.Column(col =>
                {
                    col.Name("MESTO");
                }

                );
            }

            );
            Property(x => x.Element1507261981597, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507261981597");
                }

                );
            }

            );
            Property(x => x.Element1507262033494, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507262033494");
                }

                );
            }

            );
            Property(x => x.Element1507262072161, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507262072161");
                }

                );
            }

            );
            Property(x => x.Element1507262105889, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507262105889");
                }

                );
            }

            );
            Property(x => x.Element1507262162260, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507262162260");
                }

                );
            }

            );
            Property(x => x.Element1507262266009, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507262266009");
                }

                );
            }

            );
            Property(x => x.Element1507262301335, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507262301335");
                }

                );
            }

            );
            Property(x => x.Element1507262339156, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507262339156");
                }

                );
            }

            );
            Property(x => x.Element1507262379380, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507262379380");
                }

                );
            }

            );
            Property(x => x.Element1507262414231, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507262414231");
                }

                );
            }

            );
            Property(x => x.Element1507262455649, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507262455649");
                }

                );
            }

            );
            ManyToOne(x => x.Element1507271571833, m =>
            {
                m.Column("ELEMENT1507271571833");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Element1507611045539, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507611045539");
                }

                );
            }

            );
            Property(x => x.Element1507619429093, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507619429093");
                }

                );
            }

            );
            Property(x => x.Element1507703919662, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1507703919662");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}