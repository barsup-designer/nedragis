namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Картография
    /// </summary>
    public class KartografMap : ClassMapping<Bars.Nedragis.Kartograf>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public KartografMap()
        {
            Table("KARTOGRAF");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Kartograf
            ManyToOne(x => x.vid, m =>
            {
                m.Column("ELEMENT1474278116576");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.col, p =>
            {
                p.Column(col =>
                {
                    col.Name("COL");
                }

                );
            }

            );
            Property(x => x.Element1474351766399, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1474351766399");
                }

                );
            }

            );
            Property(x => x.zakaz1, p =>
            {
                p.Column(col =>
                {
                    col.Name("ZAKAZ1");
                }

                );
            }

            );
            Property(x => x.harakter, p =>
            {
                p.Column(col =>
                {
                    col.Name("HARAKTER");
                }

                );
            }

            );
            ManyToOne(x => x.Element1474352034381, m =>
            {
                m.Column("ELEMENT1474352034381");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.organ, p =>
            {
                p.Column(col =>
                {
                    col.Name("ORGAN");
                }

                );
            }

            );
            Property(x => x.chas, p =>
            {
                p.Column(col =>
                {
                    col.Name("CHAS");
                }

                );
            }

            );
            Property(x => x.stoumost, p =>
            {
                p.Column(col =>
                {
                    col.Name("STOUMOST");
                }

                );
            }

            );
            Property(x => x.oplata, p =>
            {
                p.Column(col =>
                {
                    col.Name("OPLATA");
                }

                );
            }

            );
            ManyToOne(x => x.name, m =>
            {
                m.Column("NAME");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.opis, p =>
            {
                p.Column(col =>
                {
                    col.Name("OPIS");
                }

                );
            }

            );
            Property(x => x.zakazchik, p =>
            {
                p.Column(col =>
                {
                    col.Name("ZAKAZCHIK");
                }

                );
            }

            );
            Property(x => x.podvid, p =>
            {
                p.Column(col =>
                {
                    col.Name("PODVID");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}