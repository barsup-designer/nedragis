namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Документ
    /// </summary>
    public class DokumentMap : ClassMapping<Bars.Nedragis.Dokument>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public DokumentMap()
        {
            Table("DOKUMENT");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Dokument
            Property(x => x.Date, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_DATE");
                }

                );
            }

            );
            Property(x => x.ActualDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_ACTUALDATE");
                }

                );
            }

            );
            Property(x => x.Number, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_NUMBER");
                }

                );
                p.Unique(true);
            }

            );
            ManyToOne(x => x.Signatory, m =>
            {
                m.Column("F_SIGNATORY");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.Type, m =>
            {
                m.Column("F_TYPE");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.ObjectOfLaw, m =>
            {
                m.Column("F_OBJECTOFLAW");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Basis, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_BASIS");
                }

                );
                p.Unique(true);
            }

            );
            ManyToOne(x => x.Subject, m =>
            {
                m.Column("F_SUBJECT");
                m.Cascade(Cascade.Persist);
            }

            );
            Bag(x => x.files, bag =>
            {
                bag.Table("FAJJLPRIVJAZANNYJJKDOKUMENTU");
                bag.Key(k => k.Column("F_DOCUMENT"));
                bag.Inverse(true);
                bag.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }

            , m => m.OneToMany(r => r.Class(typeof (Bars.Nedragis.FajjlPrivjazannyjjKDokumentu))));
            Bag(x => x.zemuch, bag =>
            {
                bag.Table("DOKUMENTYZU");
                bag.Key(k => k.Column("F_DOC1"));
                bag.Inverse(true);
                bag.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }

            , m => m.OneToMany(r => r.Class(typeof (Bars.Nedragis.DokumentyZU))));
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}