namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Файл, привязанный к документу
    /// </summary>
    public class FajjlPrivjazannyjjKDokumentuMap : ClassMapping<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public FajjlPrivjazannyjjKDokumentuMap()
        {
            Table("FAJJLPRIVJAZANNYJJKDOKUMENTU");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: FajjlPrivjazannyjjKDokumentu
            ManyToOne(x => x.Document, m =>
            {
                m.Column("F_DOCUMENT");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Comment, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_COMMENT");
                }

                );
            }

            );
            ManyToOne(x => x.FileInfo, m =>
            {
                m.Column("F_FILEINFO");
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}