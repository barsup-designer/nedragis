namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Мониторинг
    /// </summary>
    public class monitorMap : ClassMapping<Bars.Nedragis.monitor>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public monitorMap()
        {
            Table("MONITOR");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: monitor
            Property(x => x.Karotazs_o, p =>
            {
                p.Column(col =>
                {
                    col.Name("KAROTAZS_O");
                }

                );
            }

            );
            Property(x => x.Karotazs_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("KAROTAZS_D");
                }

                );
            }

            );
            Property(x => x.Dela_od, p =>
            {
                p.Column(col =>
                {
                    col.Name("DELA_OD");
                }

                );
            }

            );
            Property(x => x.Dela_o, p =>
            {
                p.Column(col =>
                {
                    col.Name("DELA_O");
                }

                );
            }

            );
            Property(x => x.Karotazc, p =>
            {
                p.Column(col =>
                {
                    col.Name("KAROTAZC");
                }

                );
            }

            );
            Property(x => x.Karotazc_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("KAROTAZC_D");
                }

                );
            }

            );
            Property(x => x.geolog_otchet, p =>
            {
                p.Column(col =>
                {
                    col.Name("GEOLOG_OTCHET");
                }

                );
            }

            );
            Property(x => x.geolog_otchet_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("GEOLOG_OTCHET_D");
                }

                );
            }

            );
            Property(x => x.Element1462359767016, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1462359767016");
                }

                );
            }

            );
            Property(x => x.Element1462359769527, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1462359769527");
                }

                );
            }

            );
            Property(x => x.Element1462359853756, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1462359853756");
                }

                );
            }

            );
            Property(x => x.Element1462359855708, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1462359855708");
                }

                );
            }

            );
            Property(x => x.ocenka_vreda, p =>
            {
                p.Column(col =>
                {
                    col.Name("OCENKA_VREDA");
                }

                );
            }

            );
            Property(x => x.ocenka_vreda_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("OCENKA_VREDA_D");
                }

                );
            }

            );
            Property(x => x.Element1462360050013, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1462360050013");
                }

                );
            }

            );
            Property(x => x.raschet_ubytkov, p =>
            {
                p.Column(col =>
                {
                    col.Name("RASCHET_UBYTKOV");
                }

                );
            }

            );
            Property(x => x.kub, p =>
            {
                p.Column(col =>
                {
                    col.Name("KUB");
                }

                );
            }

            );
            Property(x => x.kub_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("KUB_D");
                }

                );
            }

            );
            Property(x => x.seismika_otchet, p =>
            {
                p.Column(col =>
                {
                    col.Name("SEISMIKA_OTCHET");
                }

                );
            }

            );
            Property(x => x.seismika_otchet_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("SEISMIKA_OTCHET_D");
                }

                );
            }

            );
            Property(x => x.karta_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("KARTA_D");
                }

                );
            }

            );
            Property(x => x.karta820, p =>
            {
                p.Column(col =>
                {
                    col.Name("KARTA820");
                }

                );
            }

            );
            Property(x => x.zapas, p =>
            {
                p.Column(col =>
                {
                    col.Name("ZAPAS");
                }

                );
            }

            );
            Property(x => x.zapas_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("ZAPAS_D");
                }

                );
            }

            );
            Property(x => x.karta_raion, p =>
            {
                p.Column(col =>
                {
                    col.Name("KARTA_RAION");
                }

                );
            }

            );
            Property(x => x.karta_raion_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("KARTA_RAION_D");
                }

                );
            }

            );
            Property(x => x.karta_nedra, p =>
            {
                p.Column(col =>
                {
                    col.Name("KARTA_NEDRA");
                }

                );
            }

            );
            Property(x => x.karta_nedra_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("KARTA_NEDRA_D");
                }

                );
            }

            );
            Property(x => x.karta_n_p9622, p =>
            {
                p.Column(col =>
                {
                    col.Name("KARTA_N_P9622");
                }

                );
            }

            );
            Property(x => x.karta_n_p6180, p =>
            {
                p.Column(col =>
                {
                    col.Name("KARTA_N_P6180");
                }

                );
            }

            );
            Property(x => x.shema_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("SHEMA_D");
                }

                );
            }

            );
            Property(x => x.shema, p =>
            {
                p.Column(col =>
                {
                    col.Name("SHEMA");
                }

                );
            }

            );
            Property(x => x.pecat_g_m, p =>
            {
                p.Column(col =>
                {
                    col.Name("PECAT_G_M");
                }

                );
            }

            );
            Property(x => x.pecat_g_m_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("PECAT_G_M_D");
                }

                );
            }

            );
            Property(x => x.analiz, p =>
            {
                p.Column(col =>
                {
                    col.Name("ANALIZ");
                }

                );
            }

            );
            Property(x => x.analiz_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("ANALIZ_D");
                }

                );
            }

            );
            Property(x => x.karta_chema, p =>
            {
                p.Column(col =>
                {
                    col.Name("KARTA_CHEMA");
                }

                );
            }

            );
            Property(x => x.karta_shema_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("KARTA_SHEMA_D");
                }

                );
            }

            );
            Property(x => x.pr_analiz, p =>
            {
                p.Column(col =>
                {
                    col.Name("PR_ANALIZ");
                }

                );
            }

            );
            Property(x => x.pr_analiz_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("PR_ANALIZ_D");
                }

                );
            }

            );
            Property(x => x.kadastr, p =>
            {
                p.Column(col =>
                {
                    col.Name("KADASTR");
                }

                );
            }

            );
            Property(x => x.kadastr_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("KADASTR_D");
                }

                );
            }

            );
            Property(x => x.seismika, p =>
            {
                p.Column(col =>
                {
                    col.Name("SEISMIKA");
                }

                );
            }

            );
            Property(x => x.seismika_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("SEISMIKA_D");
                }

                );
            }

            );
            Property(x => x.koor, p =>
            {
                p.Column(col =>
                {
                    col.Name("KOOR");
                }

                );
            }

            );
            Property(x => x.koor_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("KOOR_D");
                }

                );
            }

            );
            ManyToOne(x => x.otdel, m =>
            {
                m.Column("OTDEL");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.priroda_rpoverka_coor, p =>
            {
                p.Column(col =>
                {
                    col.Name("PRIRODA_RPOVERKA_COOR");
                }

                );
            }

            );
            Property(x => x.priroda_rpoverka_coor_d, p =>
            {
                p.Column(col =>
                {
                    col.Name("PRIRODA_RPOVERKA_COOR_D");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}