namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Субсидии
    /// </summary>
    public class subsidiesMap : ClassMapping<Bars.Nedragis.subsidies>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public subsidiesMap()
        {
            Table("SUBSIDIES");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: subsidies
            ManyToOne(x => x.NAME_MO, m =>
            {
                m.Column("NAME_MO");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.Element1455708491495, m =>
            {
                m.Column("ELEMENT1455708491495");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Element1455709095130, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1455709095130");
                }

                );
            }

            );
            Property(x => x.Element1455709437278, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1455709437278");
                }

                );
            }

            );
            Property(x => x.Element1455709476348, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1455709476348");
                }

                );
            }

            );
            Property(x => x.Element1456977571408, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456977571408");
                }

                );
            }

            );
            Property(x => x.Element1456979244748, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456979244748");
                }

                );
            }

            );
            Property(x => x.Element1456979287919, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456979287919");
                }

                );
            }

            );
            Property(x => x.Element1456982258068, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456982258068");
                }

                );
            }

            );
            Property(x => x.Element1456982634223, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456982634223");
                }

                );
            }

            );
            Property(x => x.Element1456982674444, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456982674444");
                }

                );
            }

            );
            Property(x => x.Element1456982760976, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456982760976");
                }

                );
            }

            );
            Property(x => x.Element1456982777199, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456982777199");
                }

                );
            }

            );
            Property(x => x.Element1456982822174, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456982822174");
                }

                );
            }

            );
            Property(x => x.Element1456982887080, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456982887080");
                }

                );
            }

            );
            Property(x => x.Element1456982966340, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456982966340");
                }

                );
            }

            );
            Property(x => x.Element1456983021798, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456983021798");
                }

                );
            }

            );
            Property(x => x.Element1456983065031, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1456983065031");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}