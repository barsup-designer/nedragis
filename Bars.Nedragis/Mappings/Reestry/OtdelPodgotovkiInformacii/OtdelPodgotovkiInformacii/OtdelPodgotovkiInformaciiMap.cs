namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Отдел подготовки информации
    /// </summary>
    public class OtdelPodgotovkiInformaciiMap : ClassMapping<Bars.Nedragis.OtdelPodgotovkiInformacii>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public OtdelPodgotovkiInformaciiMap()
        {
            Table("OTDELPODGOTOVKIINFORMACII");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: OtdelPodgotovkiInformacii
            Property(x => x.Element1510134896283, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1510134896283");
                }

                );
            }

            );
            ManyToOne(x => x.Element1510135000178, m =>
            {
                m.Column("ELEMENT1510135000178");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.Element1510135042374, m =>
            {
                m.Column("ELEMENT1510135042374");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Element1510135074618, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1510135074618");
                }

                );
            }

            );
            ManyToOne(x => x.Element1510135121705, m =>
            {
                m.Column("ELEMENT1510135121705");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Element1510135249843, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1510135249843");
                }

                );
            }

            );
            Property(x => x.Element1510135325790, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1510135325790");
                }

                );
            }

            );
            Property(x => x.Element1510135360701, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1510135360701");
                }

                );
            }

            );
            Property(x => x.Element1510135484012, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1510135484012");
                }

                );
            }

            );
            Property(x => x.Element1510135527140, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1510135527140");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}