namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Землеустройство
    /// </summary>
    public class ZemleustrMap : ClassMapping<Bars.Nedragis.Zemleustr>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ZemleustrMap()
        {
            Table("ZEMLEUSTR");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Zemleustr
            Property(x => x.NDelo, p =>
            {
                p.Column(col =>
                {
                    col.Name("NDELO");
                }

                );
            }

            );
            Property(x => x.vid_rabot, p =>
            {
                p.Column(col =>
                {
                    col.Name("VID_RABOT");
                }

                );
            }

            );
            Property(x => x.komu, p =>
            {
                p.Column(col =>
                {
                    col.Name("KOMU");
                }

                );
            }

            );
            Property(x => x.company, p =>
            {
                p.Column(col =>
                {
                    col.Name("COMPANY");
                }

                );
            }

            );
            Property(x => x.name_obj, p =>
            {
                p.Column(col =>
                {
                    col.Name("NAME_OBJ");
                }

                );
            }

            );
            Property(x => x.kad_nom, p =>
            {
                p.Column(col =>
                {
                    col.Name("KAD_NOM");
                }

                );
            }

            );
            Property(x => x.kateg_zem, p =>
            {
                p.Column(col =>
                {
                    col.Name("KATEG_ZEM");
                }

                );
            }

            );
            Property(x => x.konech_kategor, p =>
            {
                p.Column(col =>
                {
                    col.Name("KONECH_KATEGOR");
                }

                );
            }

            );
            Property(x => x.square, p =>
            {
                p.Column(col =>
                {
                    col.Name("SQUARE");
                }

                );
            }

            );
            ManyToOne(x => x.region, m =>
            {
                m.Column("REGION");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.date_postupl, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATE_POSTUPL");
                }

                );
            }

            );
            Property(x => x.Etap, p =>
            {
                p.Column(col =>
                {
                    col.Name("ETAP");
                }

                );
            }

            );
            Property(x => x.date_vh_doc, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATE_VH_DOC");
                }

                );
            }

            );
            Property(x => x.nom_vh_doc, p =>
            {
                p.Column(col =>
                {
                    col.Name("NOM_VH_DOC");
                }

                );
            }

            );
            ManyToOne(x => x.autor, m =>
            {
                m.Column("AUTOR");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.format, p =>
            {
                p.Column(col =>
                {
                    col.Name("FORMAT");
                }

                );
            }

            );
            Property(x => x.kolvo_ed, p =>
            {
                p.Column(col =>
                {
                    col.Name("KOLVO_ED");
                }

                );
            }

            );
            Property(x => x.prim, p =>
            {
                p.Column(col =>
                {
                    col.Name("PRIM");
                }

                );
            }

            );
            Property(x => x.sum_kalk, p =>
            {
                p.Column(col =>
                {
                    col.Name("SUM_KALK");
                }

                );
            }

            );
            Property(x => x.nomer_pisma, p =>
            {
                p.Column(col =>
                {
                    col.Name("NOMER_PISMA");
                }

                );
            }

            );
            Property(x => x.data_usp, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATA_USP");
                }

                );
            }

            );
            Property(x => x.ispol, p =>
            {
                p.Column(col =>
                {
                    col.Name("ISPOL");
                }

                );
            }

            );
            ManyToOne(x => x.name_z, m =>
            {
                m.Column("NAME_Z");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}