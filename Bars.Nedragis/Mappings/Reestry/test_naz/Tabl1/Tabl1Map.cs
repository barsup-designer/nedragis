namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности табл1
    /// </summary>
    public class Tabl1Map : ClassMapping<Bars.Nedragis.Tabl1>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public Tabl1Map()
        {
            Table("TABL1");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Tabl1
            Property(x => x.vid_r, p =>
            {
                p.Column(cm =>
                {
                    cm.Name("VID_R");
                    cm.Default(1);
                }

                );
            }

            );
            Property(x => x.count, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_COUNT");
                }

                );
            }

            );
            Property(x => x.rowid, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_ROWID");
                }

                );
                p.Unique(true);
            }

            );
            Property(x => x.sum_DS, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_SUM_DS");
                }

                );
            }

            );
            ManyToOne(x => x.kok, m =>
            {
                m.Column("KOK");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}