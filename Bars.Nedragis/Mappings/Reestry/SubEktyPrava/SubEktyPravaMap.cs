namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Субъекты права
    /// </summary>
    public class SubEktyPravaMap : ClassMapping<Bars.Nedragis.SubEktyPrava>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public SubEktyPravaMap()
        {
            Table("SUBEKTYPRAVA");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: SubEktyPrava
            Property(x => x.Representation, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_REPRESENTATION");
                }

                );
            }

            );
            ManyToOne(x => x.Status, m =>
            {
                m.Column("F_STATUS");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.type, m =>
            {
                m.Column("F_TYPE");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.ogrn, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_OGRN");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}