namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Приема и контроля исполнения заявок
    /// </summary>
    public class IspolnenijaZajavokMap : ClassMapping<Bars.Nedragis.IspolnenijaZajavok>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public IspolnenijaZajavokMap()
        {
            Table("ISPOLNENIJAZAJAVOK");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: IspolnenijaZajavok
            Property(x => x.Element1511506083922, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1511506083922");
                }

                );
            }

            );
            Property(x => x.Element1511506316909, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1511506316909");
                }

                );
            }

            );
            ManyToOne(x => x.Element1511506563562, m =>
            {
                m.Column("ELEMENT1511506563562");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.Element1511506733063, m =>
            {
                m.Column("ELEMENT1511506733063");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.Element1511762285082, m =>
            {
                m.Column("ELEMENT1511762285082");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Element1511781998051, p =>
            {
                p.Column(col =>
                {
                    col.Name("ELEMENT1511781998051");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}