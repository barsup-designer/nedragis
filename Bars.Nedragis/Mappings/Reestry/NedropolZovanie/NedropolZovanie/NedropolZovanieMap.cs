namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Недропользования
    /// </summary>
    public class NedropolZovanieMap : ClassMapping<Bars.Nedragis.NedropolZovanie>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public NedropolZovanieMap()
        {
            Table("NEDROPOLZOVANIE");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: NedropolZovanie
            Property(x => x.vid_n, p =>
            {
                p.Column(col =>
                {
                    col.Name("VID_N");
                }

                );
            }

            );
            Property(x => x.col_n, p =>
            {
                p.Column(col =>
                {
                    col.Name("COL_N");
                }

                );
            }

            );
            Property(x => x.obr_n, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBR_N");
                }

                );
            }

            );
            Property(x => x.zakaz_n, p =>
            {
                p.Column(col =>
                {
                    col.Name("ZAKAZ_N");
                }

                );
            }

            );
            Property(x => x.harakter_n, p =>
            {
                p.Column(col =>
                {
                    col.Name("HARAKTER_N");
                }

                );
            }

            );
            ManyToOne(x => x.sotr_n, m =>
            {
                m.Column("SOTR_N");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.organ, p =>
            {
                p.Column(col =>
                {
                    col.Name("ORGAN");
                }

                );
            }

            );
            Property(x => x.chas_n, p =>
            {
                p.Column(col =>
                {
                    col.Name("CHAS_N");
                }

                );
            }

            );
            Property(x => x.stoumost, p =>
            {
                p.Column(col =>
                {
                    col.Name("STOUMOST");
                }

                );
            }

            );
            Property(x => x.oplata_n, p =>
            {
                p.Column(col =>
                {
                    col.Name("OPLATA_N");
                }

                );
            }

            );
            ManyToOne(x => x.name_n, m =>
            {
                m.Column("NAME_N");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.opis_n, p =>
            {
                p.Column(col =>
                {
                    col.Name("OPIS_N");
                }

                );
            }

            );
            Property(x => x.zakazchik, p =>
            {
                p.Column(col =>
                {
                    col.Name("ZAKAZCHIK");
                }

                );
            }

            );
            Property(x => x.podvid, p =>
            {
                p.Column(col =>
                {
                    col.Name("PODVID");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}