namespace Bars.Nedragis.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Юридические лица
    /// </summary>
    public class JuridicheskieLicaMap : JoinedSubclassMapping<Bars.Nedragis.JuridicheskieLica>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public JuridicheskieLicaMap()
        {
            Table("JURIDICHESKIELICA");
            Key(k => k.Column("id"));
#region Class: JuridicheskieLica
            ManyToOne(x => x.OKATO, m =>
            {
                m.Column("F_OKATO");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Fullname, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_FULLNAME");
                }

                );
            }

            );
            ManyToOne(x => x.Okopf, m =>
            {
                m.Column("F_OKOPF");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Okpo, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_OKPO");
                }

                );
            }

            );
            ManyToOne(x => x.Okved, m =>
            {
                m.Column("F_OKVED");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Phone, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_PHONE");
                }

                );
            }

            );
            Property(x => x.Fax, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_FAX");
                }

                );
            }

            );
            ManyToOne(x => x.HeadSubject, m =>
            {
                m.Column("F_HEADSUBJECT");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.PayeeSubject, m =>
            {
                m.Column("F_PAYEESUBJECT");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.Email, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_EMAIL");
                }

                );
            }

            );
            ManyToOne(x => x.Okfs, m =>
            {
                m.Column("F_OKFS");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.DateReg, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_DATEREG");
                }

                );
            }

            );
            Property(x => x.CapitalStock, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_CAPITALSTOCK");
                }

                );
            }

            );
            Property(x => x.CapitalPeople, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_CAPITALPEOPLE");
                }

                );
            }

            );
            ManyToOne(x => x.Sphere, m =>
            {
                m.Column("F_SPHERE");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.Okogu, m =>
            {
                m.Column("F_OKOGU");
                m.Cascade(Cascade.Persist);
            }

            );
            Bag(x => x.bosses, bag =>
            {
                bag.Table("RUKOVODITELI");
                bag.Key(k => k.Column("F_YURLITCO"));
                bag.Inverse(true);
                bag.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }

            , m => m.OneToMany(r => r.Class(typeof (Bars.Nedragis.Rukovoditeli))));
            ManyToOne(x => x.LegalAddress, m =>
            {
                m.Column("F_LEGALADDRESS");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.FactAddress, m =>
            {
                m.Column("F_FACTADDRESS");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.INN1, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_INN1");
                }

                );
            }

            );
            Property(x => x.KPP1, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_KPP1");
                }

                );
            }

            );
            Bag(x => x.docs, bag =>
            {
                bag.Table("DOKUMENT");
                bag.Key(k => k.Column("F_SUBJECT"));
                bag.Inverse(true);
                bag.Cascade(Cascade.All | Cascade.DeleteOrphans);
            }

            , m => m.OneToMany(r => r.Class(typeof (Bars.Nedragis.Dokument))));
            Property(x => x.ato_date, p =>
            {
                p.Column(col =>
                {
                    col.Name("F_ATO_DATE");
                }

                );
            }

            );
#endregion
        }
    }
}