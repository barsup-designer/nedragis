namespace Bars.Nedragis.Security
{
    using Bars.B4.Utils;
    using Bars.B4;

    public class TestRazPermissonMap : PermissionMap
    {
        public TestRazPermissonMap()
        {
            var razmodultest = Namespace<Bars.Nedragis.Reestry>("razmodultest", "Разрешение_test");
            var razmodultestmenytest = Namespace<Bars.Nedragis.Reestry>("razmodultest.menytest", "Разрешение меню");
            var razmodultestmenytestur1 = Namespace<Bars.Nedragis.Reestry>("razmodultest.menytest.ur1", "Уровень1");
            razmodultestmenytestur1.Permission("razreestrur1", "Реестры");
            var razmodultestmenytestur2 = Namespace<Bars.Nedragis.Reestry>("razmodultest.menytest.ur2", "Уровень2");
            razmodultestmenytestur2.Permission("razreestrur2", "Реестры");
            var razmodultestmenytestur3 = Namespace<Bars.Nedragis.Reestry>("razmodultest.menytest.ur3", "Уровень3");
            razmodultestmenytestur3.Permission("raznadraur3", "Недропользование");
            razmodultestmenytestur3.Permission("razkartaur3", "Картография");
            razmodultestmenytestur3.Permission("razzemur3", "Землеустройство");
            razmodultestmenytestur3.Permission("razprirur3", "Природопользование");
            razmodultestmenytestur3.Permission("razdocur3", "Документы");
            razmodultestmenytestur3.Permission("razfailur3", "Реестр файлы");
            razmodultestmenytestur3.Permission("razzemuchur3", "Земельные участки");
            razmodultestmenytestur3.Permission("razurlur3", "Юридические лица");
            razmodultestmenytestur3.Permission("razrukur3", "Руководители");
            razmodultestmenytestur3.Permission("razvidpr", "Вид права");
            razmodultestmenytestur3.Permission("razvidprur3", "Вид права объекта");
            razmodultestmenytestur3.Permission("razvidur3", "Вид права на землю");
            razmodultestmenytestur3.Permission("razbogrur3", "Вид ограничения");
            razmodultestmenytestur3.Permission("razgrur3", "График платежей");
            razmodultestmenytestur3.Permission("razdoclur3", "Документы, удостоверяющие");
            razmodultestmenytestur3.Permission("razdur3", "Должности");
            razmodultestmenytestur3.Permission("razotur3", "Отрасль");
            razmodultestmenytestur3.Permission("razsybjectur3", "Статусы субъекта");
            razmodultestmenytestur3.Permission("razobjectur3", "Статусы объекта");
            razmodultestmenytestur3.Permission("razsostur3", "Состояние объекта");
            razmodultestmenytestur3.Permission("razarendaur3", "Тип договора аренды");
            razmodultestmenytestur3.Permission("razprodur3", "Купли-продажи");
            razmodultestmenytestur3.Permission("raztipdokumur3", "Типы документов");
            razmodultestmenytestur3.Permission("razzemuchur31", "Тип земельного участка");
            razmodultestmenytestur3.Permission("razsybprur3", "Типы субъекта права");
            razmodultestmenytestur3.Permission("razjbprur3", "Типы объекта права");
            razmodultestmenytestur3.Permission("razraionur3", "Наименование МО");
            razmodultestmenytestur3.Permission("razotdelur3", "Отдел");
            razmodultestmenytestur3.Permission("razvidrabotur3", "Виды работ");
            razmodultestmenytestur3.Permission("razpodvidur3", "Подвиды работ");
            razmodultestmenytestur3.Permission("razgosur3", "Госвласть");
            razmodultestmenytestur3.Permission("razsotrur3", "Сотрудники");
            razmodultestmenytestur3.Permission("razrainur3", "Районы");
            razmodultestmenytestur3.Permission("razorgur3", "Органы власти(Природа)");
            razmodultestmenytestur3.Permission("razvidprur31", "Вид работ(природа)");
            razmodultestmenytestur3.Permission("razmprur3", "Отчетный месяц(природа)");
            razmodultestmenytestur3.Permission("razpodvidprur3", "Подвид работ(природа)");
            razmodultestmenytestur3.Permission("raznadur3", "Субъекты надзора");
            razmodultestmenytestur3.Permission("zima3", "zima3");
            razmodultestmenytestur3.Permission("zim_table", "zim");
            var razmodultestmenytestrazreestrnedra1 = Namespace<Bars.Nedragis.NedropolZovanie>("razmodultest.menytest.razreestrnedra1", "Реестр Недропользования");
            razmodultestmenytestrazreestrnedra1.Permission("Update", "Изменение");
            razmodultestmenytestrazreestrnedra1.Permission("Read", "Просмотр");
            razmodultestmenytestrazreestrnedra1.Permission("Delete", "Удаление");
            razmodultestmenytestrazreestrnedra1.Permission("Create", "Создание");
            var razmodultestmenytestrazreestpriroda = Namespace<Bars.Nedragis.ReestrPrirodopolZovanija>("razmodultest.menytest.razreestpriroda", "Реестр Природопользования");
            razmodultestmenytestrazreestpriroda.Permission("Update", "Изменение");
            razmodultestmenytestrazreestpriroda.Permission("Read", "Просмотр");
            razmodultestmenytestrazreestpriroda.Permission("Delete", "Удаление");
            razmodultestmenytestrazreestpriroda.Permission("Create", "Создание");
            var razmodultestmenytestrazreesrtkartog = Namespace<Bars.Nedragis.Kartograf>("razmodultest.menytest.razreesrtkartog", "Ресстр Картография");
            razmodultestmenytestrazreesrtkartog.Permission("Update", "Изменение");
            razmodultestmenytestrazreesrtkartog.Permission("Read", "Просмотр");
            razmodultestmenytestrazreesrtkartog.Permission("Delete", "Удаление");
            razmodultestmenytestrazreesrtkartog.Permission("Create", "Создание");
            var razmodultestmenytestrazreestrzemlya = Namespace<Bars.Nedragis.Zemleustr>("razmodultest.menytest.razreestrzemlya", "Реестр Землеустройства");
            razmodultestmenytestrazreestrzemlya.Permission("Update", "Изменение");
            razmodultestmenytestrazreestrzemlya.Permission("Read", "Просмотр");
            razmodultestmenytestrazreestrzemlya.Permission("Delete", "Удаление");
            razmodultestmenytestrazreestrzemlya.Permission("Create", "Создание");
            var razmodultestmenytesttest1234 = Namespace<Bars.Nedragis.ReestrObEktov>("razmodultest.menytest.test1234", "Объекты регионального надзора");
            razmodultestmenytesttest1234.Permission("Update", "Изменение");
            razmodultestmenytesttest1234.Permission("Read", "Просмотр");
            razmodultestmenytesttest1234.Permission("Delete", "Удаление");
            razmodultestmenytesttest1234.Permission("Create", "Создание");
        }
    }
}