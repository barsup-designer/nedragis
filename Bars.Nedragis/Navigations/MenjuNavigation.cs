namespace Bars.Nedragis
{
    using Bars.B4.Utils;
    using System.Linq;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Castle.MicroKernel.Registration;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    public class MenjuNavigation : INavigationProvider
    {
        public IWindsorContainer Container
        {
            get;
            set;
        }

#region Implementation of INavigationProvider
        /// <summary>
        /// Ключ меню
        /// </summary>
        public string Key
        {
            get
            {
                return MainNavigationInfo.MenuName;
            }
        }

        /// <summary>
        /// Описание меню
        /// </summary>
        public string Description
        {
            get;
            private set;
        }

        /// <summary>
        /// Метод инифиализации
        /// </summary>
        public void Init(MenuItem root)
        {
            MenuItem @ref0 = null;
            MenuItem @ref1 = null;
            MenuItem @ref2 = null;
            @ref0 = root.Add("Справочники");
            @ref1 = @ref0.Add("Общероссийские справочники");
            @ref2 = @ref1.Add("Виды разрешенного использования", "VRIZUIerarkhija");
            @ref2 = @ref1.Add("Категории земель", "KategoriiZemelList");
            @ref2 = @ref1.Add("КБК", "KBKList");
            @ref2 = @ref1.Add("ОКАТО", "OKATO1");
            @ref2 = @ref1.Add("ОКТМО", "OKTMO1");
            @ref2 = @ref1.Add("ОКВЭД", "OKVEhD1");
            @ref2 = @ref1.Add("ОКОГУ", "OKOGU1");
            @ref2 = @ref1.Add("ОКОПФ", "OKOPFList");
            @ref2 = @ref1.Add("Классификация ОКОФ", "KlassifikacijaOKOFList");
            @ref2 = @ref1.Add("ОКОФ", "OKOF1");
            @ref2 = @ref1.Add("ОКФС", "OKFSList");
            @ref2 = @ref1.Add("Тест");
            @ref1 = @ref0.Add("Специальные справочники");
            @ref2 = @ref1.Add("Вид права", "VidPravaList");
            @ref2 = @ref1.Add("Вид права объекта", "VidPravaObEktaList");
            @ref2 = @ref1.Add("Вид права на землю", "VidPravaNaZemljuList");
            @ref2 = @ref1.Add("Вид ограничения", "VidOgranichenijaList");
            @ref2 = @ref1.Add("График платежей", "GrafikPlatezhejjList");
            @ref2 = @ref1.Add("Документы, удостоверяющие личность", "DokumentyUdostoverjajushhieLichnostList");
            @ref2 = @ref1.Add("Должности", "DolzhnostiList");
            @ref2 = @ref1.Add("Отрасль", "OtraslList");
            @ref2 = @ref1.Add("Статусы объекта", "StatusyObEktaList");
            @ref2 = @ref1.Add("Статусы субъекта", "StatusySubEktaList");
            @ref2 = @ref1.Add("Состояние объекта", "SostojanieObEktaList");
            @ref2 = @ref1.Add("Тип договора аренды", "TipDogovoraArendyList");
            @ref2 = @ref1.Add("Тип договора купли-продажи", "TipDogovoraKupliProdazhiList");
            @ref2 = @ref1.Add("Типы документов", "TipyDokumentovList");
            @ref2 = @ref1.Add("Тип земельного участка", "TipZemelNogoUchastkaList");
            @ref2 = @ref1.Add("Типы субъекта права", "TipySubEktaPravaList");
            @ref2 = @ref1.Add("Типы объекта права", "TipyObEktaPravaList");
            @ref2 = @ref1.Add("Реестр Наименование МО", "NaimenovanieMOList");
            @ref2 = @ref1.Add("Реестр Отдел", "Otdel1List");
            @ref2 = @ref1.Add("Виды работ", "VidyRabotList");
            @ref2 = @ref1.Add("Подвиды работ", "PodvidyList");
            @ref2 = @ref1.Add("Госвласть", "GosvlastList");
            @ref2 = @ref1.Add("Сотрудники", "Sotrudniki1List");
            @ref2 = @ref1.Add("Реестр Районы ЯНАО", "RajjonyJaNAOList");
            @ref2 = @ref1.Add("Органы власти Природопользование", "OrganyVlastiPrirodaList");
            @ref2 = @ref1.Add("Вид работ Природопользование", "VidRabotPrirList");
            @ref2 = @ref1.Add("Отчетный месяц Природопользование", "MesjacPrirodaList");
            @ref2 = @ref1.Add("Подвид работ Природопользование", "PodvidRabotPrirodaList");
            @ref2 = @ref1.Add("Перечень хозяйствующих субъектов, осуществляющий хозяйственную  и иную деятельность с использованием объектов, не подлежащим федеральному государственному экологическому надзору", "ReestrNaimenovanieObEktovNadzora");
            @ref2 = @ref1.Add("Реестр Организации", "OrganizaciiList");
            @ref2 = @ref1.Add("Реестр Проекты", "ProektyList");
            @ref2 = @ref1.Add("Реестр Наименование проекта", "NaimenovanieProektaList");
            @ref2 = @ref1.Add("Наименование направления работ", "NaimenovanieNapravlenijaRaskhodovanijaSredstvList");
            @ref2 = @ref1.Add("Реестр zima1", "Zima1List");
            root.ReOrder(@ref0.Caption);
            @ref0 = root.Add("Реестры");
            @ref1 = @ref0.Add("Субъекты права");
            @ref2 = @ref1.Add("Юридические лица", "JuridicheskieLicaList");
            @ref2 = @ref1.Add("Руководители", "RukovoditeliList2");
            @ref1 = @ref0.Add("Недвижимое имущество");
            @ref2 = @ref1.Add("Земельные участки", "ZemelNyemUchastkiList");
            @ref1 = @ref0.Add("Документы, операции");
            @ref2 = @ref1.Add("Документы", "DokumentList");
            @ref2 = @ref1.Add("Реестр Файлы", "FajjlPrivjazannyjjKDokumentuList");
            @ref1 = @ref0.Add("Мониторинг деятельности ГУ");
            @ref2 = @ref1.Add("Реестр Картография", "KartografList");
            @ref2.Icon = "fi-align-justify";
            @ref2 = @ref1.Add("Реестр Землеустройство", "ZemleustrList");
            @ref2.Icon = "fm-codepen2";
            @ref2 = @ref1.Add("Реестр Недропользования", "NedropolZovanieList");
            @ref2.Icon = "fm-clipboard7";
            @ref2 = @ref1.Add("Реестр Природопользования", "PrirodopolZovanie2List");
            @ref2.Icon = "fm-tree2";
            @ref2 = @ref1.Add("Реестр Отдел подготовки информации", "OtdelPodgotovkiInformaciiList");
            @ref2.Icon = "fm-collections";
            @ref2 = @ref1.Add("Reestr zim", "ReestrZim");
            @ref1 = @ref0.Add("Реестр объектов регионального надзора ДПРР ЯНАО");
            @ref2 = @ref1.Add("Реестр объектов регионального надзора ДПРР ЯНАО", "ReestrNadzor");
            @ref1 = @ref0.Add("Call-центр");
            @ref2 = @ref1.Add("Реестр Приема и контроля исполнения заявок", "IspolnenijaZajavokList");
            root.ReOrder(@ref0.Caption);
            @ref0 = root.Add("Тестовый");
            @ref1 = @ref0.Add("Элемент меню");
            @ref2 = @ref1.Add("Реестр Реестр объектов", "ReestrObEktovList");
            @ref2 = @ref1.Add("Реестр объектопи2", "ObEktopiList2");
            @ref2 = @ref1.Add("Реестр Региональный надзор водных объектов", "RegionalNyjjNadzorVodnykhObEktovList");
            @ref2 = @ref1.Add("Реестр объектопи", "ObEktopiList");
            root.ReOrder(@ref0.Caption);
        }
#endregion
    }
}