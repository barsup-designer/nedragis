namespace Bars.Nedragis
{
    using Bars.B4;

    public class MenjuClientRoute : IClientRouteMapRegistrar
    {
#region Implementation of IClientRouteMapRegistrar
        /// <summary>
        /// Метод регистрации роута в общей карте
        /// </summary>
        /// <param name = "map"></param>
        public void RegisterRoutes(ClientRouteMap map)
        {
            map.AddRoute(new ClientRoute("VRIZUIerarkhija/", "B4.controller.VRIZUIerarkhija"));
            map.AddRoute(new ClientRoute("KategoriiZemelList/", "B4.controller.KategoriiZemelList"));
            map.AddRoute(new ClientRoute("KBKList/", "B4.controller.KBKList"));
            map.AddRoute(new ClientRoute("OKATO1/", "B4.controller.OKATO1"));
            map.AddRoute(new ClientRoute("OKTMO1/", "B4.controller.OKTMO1"));
            map.AddRoute(new ClientRoute("OKVEhD1/", "B4.controller.OKVEhD1"));
            map.AddRoute(new ClientRoute("OKOGU1/", "B4.controller.OKOGU1"));
            map.AddRoute(new ClientRoute("OKOPFList/", "B4.controller.OKOPFList"));
            map.AddRoute(new ClientRoute("KlassifikacijaOKOFList/", "B4.controller.KlassifikacijaOKOFList"));
            map.AddRoute(new ClientRoute("OKOF1/", "B4.controller.OKOF1"));
            map.AddRoute(new ClientRoute("OKFSList/", "B4.controller.OKFSList"));
            map.AddRoute(new ClientRoute("VidPravaList/", "B4.controller.VidPravaList"));
            map.AddRoute(new ClientRoute("VidPravaObEktaList/", "B4.controller.VidPravaObEktaList"));
            map.AddRoute(new ClientRoute("VidPravaNaZemljuList/", "B4.controller.VidPravaNaZemljuList"));
            map.AddRoute(new ClientRoute("VidOgranichenijaList/", "B4.controller.VidOgranichenijaList"));
            map.AddRoute(new ClientRoute("GrafikPlatezhejjList/", "B4.controller.GrafikPlatezhejjList"));
            map.AddRoute(new ClientRoute("DokumentyUdostoverjajushhieLichnostList/", "B4.controller.DokumentyUdostoverjajushhieLichnostList"));
            map.AddRoute(new ClientRoute("DolzhnostiList/", "B4.controller.DolzhnostiList"));
            map.AddRoute(new ClientRoute("OtraslList/", "B4.controller.OtraslList"));
            map.AddRoute(new ClientRoute("StatusyObEktaList/", "B4.controller.StatusyObEktaList"));
            map.AddRoute(new ClientRoute("StatusySubEktaList/", "B4.controller.StatusySubEktaList"));
            map.AddRoute(new ClientRoute("SostojanieObEktaList/", "B4.controller.SostojanieObEktaList"));
            map.AddRoute(new ClientRoute("TipDogovoraArendyList/", "B4.controller.TipDogovoraArendyList"));
            map.AddRoute(new ClientRoute("TipDogovoraKupliProdazhiList/", "B4.controller.TipDogovoraKupliProdazhiList"));
            map.AddRoute(new ClientRoute("TipyDokumentovList/", "B4.controller.TipyDokumentovList"));
            map.AddRoute(new ClientRoute("TipZemelNogoUchastkaList/", "B4.controller.TipZemelNogoUchastkaList"));
            map.AddRoute(new ClientRoute("TipySubEktaPravaList/", "B4.controller.TipySubEktaPravaList"));
            map.AddRoute(new ClientRoute("TipyObEktaPravaList/", "B4.controller.TipyObEktaPravaList"));
            map.AddRoute(new ClientRoute("NaimenovanieMOList/", "B4.controller.NaimenovanieMOList"));
            map.AddRoute(new ClientRoute("Otdel1List/", "B4.controller.Otdel1List"));
            map.AddRoute(new ClientRoute("VidyRabotList/", "B4.controller.VidyRabotList"));
            map.AddRoute(new ClientRoute("PodvidyList/", "B4.controller.PodvidyList"));
            map.AddRoute(new ClientRoute("GosvlastList/", "B4.controller.GosvlastList"));
            map.AddRoute(new ClientRoute("Sotrudniki1List/", "B4.controller.Sotrudniki1List"));
            map.AddRoute(new ClientRoute("RajjonyJaNAOList/", "B4.controller.RajjonyJaNAOList"));
            map.AddRoute(new ClientRoute("OrganyVlastiPrirodaList/", "B4.controller.OrganyVlastiPrirodaList"));
            map.AddRoute(new ClientRoute("VidRabotPrirList/", "B4.controller.VidRabotPrirList"));
            map.AddRoute(new ClientRoute("MesjacPrirodaList/", "B4.controller.MesjacPrirodaList"));
            map.AddRoute(new ClientRoute("PodvidRabotPrirodaList/", "B4.controller.PodvidRabotPrirodaList"));
            map.AddRoute(new ClientRoute("ReestrNaimenovanieObEktovNadzora/", "B4.controller.ReestrNaimenovanieObEktovNadzora"));
            map.AddRoute(new ClientRoute("OrganizaciiList/", "B4.controller.OrganizaciiList"));
            map.AddRoute(new ClientRoute("ProektyList/", "B4.controller.ProektyList"));
            map.AddRoute(new ClientRoute("NaimenovanieProektaList/", "B4.controller.NaimenovanieProektaList"));
            map.AddRoute(new ClientRoute("NaimenovanieNapravlenijaRaskhodovanijaSredstvList/", "B4.controller.NaimenovanieNapravlenijaRaskhodovanijaSredstvList"));
            map.AddRoute(new ClientRoute("Zima1List/", "B4.controller.Zima1List"));
            map.AddRoute(new ClientRoute("JuridicheskieLicaList/", "B4.controller.JuridicheskieLicaList"));
            map.AddRoute(new ClientRoute("RukovoditeliList2/", "B4.controller.RukovoditeliList2"));
            map.AddRoute(new ClientRoute("ZemelNyemUchastkiList/", "B4.controller.ZemelNyemUchastkiList"));
            map.AddRoute(new ClientRoute("DokumentList/", "B4.controller.DokumentList"));
            map.AddRoute(new ClientRoute("FajjlPrivjazannyjjKDokumentuList/", "B4.controller.FajjlPrivjazannyjjKDokumentuList"));
            map.AddRoute(new ClientRoute("KartografList/", "B4.controller.KartografList"));
            map.AddRoute(new ClientRoute("ZemleustrList/", "B4.controller.ZemleustrList"));
            map.AddRoute(new ClientRoute("NedropolZovanieList/", "B4.controller.NedropolZovanieList"));
            map.AddRoute(new ClientRoute("PrirodopolZovanie2List/", "B4.controller.PrirodopolZovanie2List"));
            map.AddRoute(new ClientRoute("OtdelPodgotovkiInformaciiList/", "B4.controller.OtdelPodgotovkiInformaciiList"));
            map.AddRoute(new ClientRoute("ReestrZim/", "B4.controller.ReestrZim"));
            map.AddRoute(new ClientRoute("ReestrNadzor/", "B4.controller.ReestrNadzor"));
            map.AddRoute(new ClientRoute("IspolnenijaZajavokList/", "B4.controller.IspolnenijaZajavokList"));
            map.AddRoute(new ClientRoute("ReestrObEktovList/", "B4.controller.ReestrObEktovList"));
            map.AddRoute(new ClientRoute("ObEktopiList2/", "B4.controller.ObEktopiList2"));
            map.AddRoute(new ClientRoute("RegionalNyjjNadzorVodnykhObEktovList/", "B4.controller.RegionalNyjjNadzorVodnykhObEktovList"));
            map.AddRoute(new ClientRoute("ObEktopiList/", "B4.controller.ObEktopiList"));
        }
#endregion
    }
}