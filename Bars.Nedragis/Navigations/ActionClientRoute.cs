namespace Bars.Nedragis
{
    using Bars.B4;

    public class ActionClientRoute : IClientRouteMapRegistrar
    {
#region Implementation of IClientRouteMapRegistrar
        /// <summary>
        /// Метод регистрации роута в общей карте
        /// </summary>
        /// <param name = "map"></param>
        public void RegisterRoutes(ClientRouteMap map)
        {
            map.AddRoute(new ClientRoute("ZemelNyemUchastkiList-Edit/{id}/", "B4.controller.ZemelNyemUchastkiList", "executeActionEdit"));
            map.AddRoute(new ClientRoute("JuridicheskieLicaList-Edit/{id}/", "B4.controller.JuridicheskieLicaList", "executeActionEdit"));
        }
#endregion
    }
}