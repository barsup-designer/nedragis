namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Организации
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Организации")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("fc380625-4f7d-4faf-a793-8060a22ae929")]
    [Bars.Rms.Core.Attributes.Uid("fc380625-4f7d-4faf-a793-8060a22ae929")]
    public class Organizacii : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Organizacii(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("fb062969-e7d0-4b44-b9d8-684e21206cf8")]
        public virtual System.String Element1511331964541
        {
            get;
            set;
        }

        /// <summary>
        /// мо
        /// </summary>
        [Bars.B4.Utils.Display("мо")]
        [Bars.Rms.Core.Attributes.Uid("5882f6fe-bacb-4a10-8f87-5487022ca7f1")]
        public virtual Bars.Nedragis.NaimenovanieMO Element1511331983555
        {
            get;
            set;
        }
    }
}