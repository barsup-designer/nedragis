namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Проекты
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Проекты")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("4e1f118d-0d5d-441f-97ac-09bbc5021945")]
    [Bars.Rms.Core.Attributes.Uid("4e1f118d-0d5d-441f-97ac-09bbc5021945")]
    public class Proekty : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Proekty(): base ()
        {
        }

        /// <summary>
        /// Наименование проекта
        /// </summary>
        [Bars.B4.Utils.Display("Наименование проекта")]
        [Bars.Rms.Core.Attributes.Uid("a0efe821-6ced-455d-ae25-34f46eae7020")]
        public virtual System.String Element1511335229126
        {
            get;
            set;
        }
    }
}