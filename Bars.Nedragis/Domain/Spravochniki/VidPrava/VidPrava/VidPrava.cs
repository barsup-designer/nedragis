namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Вид права
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Вид права")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("d9f0752c-b27c-4f7f-8667-e2d8dbbe8970")]
    [Bars.Rms.Core.Attributes.Uid("d9f0752c-b27c-4f7f-8667-e2d8dbbe8970")]
    public class VidPrava : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public VidPrava(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("415d8b60-7bb2-4842-8a80-d0a62e77c620")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("22060e1d-de06-429a-a66f-05eaa0d7d769")]
        public virtual System.String name1
        {
            get;
            set;
        }
    }
}