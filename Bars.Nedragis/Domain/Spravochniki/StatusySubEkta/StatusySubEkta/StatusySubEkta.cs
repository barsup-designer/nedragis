namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Статусы субъекта
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Статусы субъекта")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("a367a6ea-1623-42b3-9c72-267cf54c472d")]
    [Bars.Rms.Core.Attributes.Uid("a367a6ea-1623-42b3-9c72-267cf54c472d")]
    public class StatusySubEkta : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public StatusySubEkta(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("c4f364d4-15bc-4167-971f-37ac707c5c93")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("dad10535-22da-4ceb-8343-fa8e0ee90e05")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}