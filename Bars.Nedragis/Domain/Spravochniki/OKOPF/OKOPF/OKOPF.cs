namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// ОКОПФ
    /// Общероссийский Классификатор Организационно-Правовых Форм. Объекты классификации ОКОПФ - это организационно-правовые формы хозяйствующих субъектов.
    /// </summary>
    [Bars.B4.Utils.Display(@"ОКОПФ")]
    [Bars.B4.Utils.Description(@"Общероссийский Классификатор Организационно-Правовых Форм. Объекты классификации ОКОПФ - это организационно-правовые формы хозяйствующих субъектов.")]
    [System.Runtime.InteropServices.GuidAttribute("1f07d59f-1ccc-4e40-8cca-0cf84d26e430")]
    [Bars.Rms.Core.Attributes.Uid("1f07d59f-1ccc-4e40-8cca-0cf84d26e430")]
    public class OKOPF : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public OKOPF(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("2b8f68da-5a1a-45ba-ba03-2164ddc070b0")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("60790613-0b07-4d62-9ca9-96c24bfa1c94")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}