namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Должности
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Должности")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("c615f281-09eb-4a12-9530-5dcccef6f1ae")]
    [Bars.Rms.Core.Attributes.Uid("c615f281-09eb-4a12-9530-5dcccef6f1ae")]
    public class Dolzhnosti : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Dolzhnosti(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("c175adc4-fbd3-4348-8db8-87841ee37856")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("9b116f06-db34-48a5-a6ea-feb1c59effc8")]
        public virtual System.String name1
        {
            get;
            set;
        }
    }
}