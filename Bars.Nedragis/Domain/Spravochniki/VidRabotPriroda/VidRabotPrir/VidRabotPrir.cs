namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Вид работ природа
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Вид работ природа")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("3d1718cc-c093-46d2-974e-1b0bc0fd51cb")]
    [Bars.Rms.Core.Attributes.Uid("3d1718cc-c093-46d2-974e-1b0bc0fd51cb")]
    public class VidRabotPrir : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public VidRabotPrir(): base ()
        {
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("5b610cce-715f-4684-99f3-eb7068b94320")]
        public virtual System.String vid_rabot_prirod_s
        {
            get;
            set;
        }
    }
}