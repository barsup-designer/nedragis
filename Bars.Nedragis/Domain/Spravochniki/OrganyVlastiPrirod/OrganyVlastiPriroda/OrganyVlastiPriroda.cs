namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Органы власти Природа
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Органы власти Природа")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("800fae24-3869-4df3-be5d-3f5b9955b5e0")]
    [Bars.Rms.Core.Attributes.Uid("800fae24-3869-4df3-be5d-3f5b9955b5e0")]
    public class OrganyVlastiPriroda : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public OrganyVlastiPriroda(): base ()
        {
        }

        /// <summary>
        /// Органы власти
        /// </summary>
        [Bars.B4.Utils.Display("Органы власти")]
        [Bars.Rms.Core.Attributes.Uid("b3e21364-b273-4fd4-843d-4e41315ce65f")]
        public virtual System.String organ_vlasti_pr
        {
            get;
            set;
        }
    }
}