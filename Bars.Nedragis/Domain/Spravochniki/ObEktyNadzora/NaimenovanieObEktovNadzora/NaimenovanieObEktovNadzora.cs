namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Наименование объектов надзора
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Наименование объектов надзора")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("d41afb95-b52e-4013-b714-2fc08404a9f9")]
    [Bars.Rms.Core.Attributes.Uid("d41afb95-b52e-4013-b714-2fc08404a9f9")]
    public class NaimenovanieObEktovNadzora : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public NaimenovanieObEktovNadzora(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("65de9155-a350-4206-be94-ec95bbf9ef90")]
        public virtual System.String Element1507203167462
        {
            get;
            set;
        }
    }
}