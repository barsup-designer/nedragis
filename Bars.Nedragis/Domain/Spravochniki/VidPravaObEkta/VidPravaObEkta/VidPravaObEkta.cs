namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Вид права объекта
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Вид права объекта")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("bc0928cd-24af-4ec6-9445-ba8b0e7367f7")]
    [Bars.Rms.Core.Attributes.Uid("bc0928cd-24af-4ec6-9445-ba8b0e7367f7")]
    public class VidPravaObEkta : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public VidPravaObEkta(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("788096e4-9847-430d-a9e9-b7aeb4a7a31e")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("53a0c652-b83f-4495-abdf-9e22694d7d4f")]
        public virtual System.String name1
        {
            get;
            set;
        }
    }
}