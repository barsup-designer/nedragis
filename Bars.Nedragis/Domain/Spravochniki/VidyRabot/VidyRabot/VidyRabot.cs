namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Виды работ
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Виды работ")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("f1b2473f-a514-468d-95fc-58e1fbb40f2b")]
    [Bars.Rms.Core.Attributes.Uid("f1b2473f-a514-468d-95fc-58e1fbb40f2b")]
    public class VidyRabot : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public VidyRabot(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("b5a48978-083d-4fd5-853f-117af3c5798e")]
        public virtual System.String Element1474020299386
        {
            get;
            set;
        }

        private System.Collections.Generic.IList<Bars.Nedragis.Podvidy> _Element1474349449685;
        /// <summary>
        /// Подвид
        /// </summary>
        [Bars.B4.Utils.Display("Подвид")]
        [Bars.Rms.Core.Attributes.Uid("df8b3c6c-9346-406e-b4cb-79a445967b3e")]
        public virtual System.Collections.Generic.IList<Bars.Nedragis.Podvidy> Element1474349449685
        {
            get
            {
                return _Element1474349449685 ?? (_Element1474349449685 = new System.Collections.Generic.List<Bars.Nedragis.Podvidy>());
            }

            set
            {
                _Element1474349449685 = value;
            }
        }
    }
}