namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Типы документов
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Типы документов")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("905a597c-579b-4fb5-a6b8-4f88cbdaf279")]
    [Bars.Rms.Core.Attributes.Uid("905a597c-579b-4fb5-a6b8-4f88cbdaf279")]
    public class TipyDokumentov : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public TipyDokumentov(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("f1cf426e-de48-4696-9ae2-82f5bb94b8e1")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("4de0c197-d1b9-4da2-90d6-320d9c727e26")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент")]
        [Bars.Rms.Core.Attributes.Uid("203caae2-6f31-4f6d-b44e-e61cd1fe18c1")]
        public virtual Bars.Nedragis.TipyDokumentov parent_id
        {
            get;
            set;
        }
    }
}