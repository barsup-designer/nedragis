namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Вид права на землю
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Вид права на землю")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("5a74b33e-dffc-409a-af27-01e29e56ce83")]
    [Bars.Rms.Core.Attributes.Uid("5a74b33e-dffc-409a-af27-01e29e56ce83")]
    public class VidPravaNaZemlju : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public VidPravaNaZemlju(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("c96ee2e7-1e0a-4f09-bcd1-96337d21c91a")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("ea2c10dd-7114-412e-9b4a-7c4936c0c672")]
        public virtual System.String name1
        {
            get;
            set;
        }
    }
}