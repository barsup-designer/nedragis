namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Тип земельного участка
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Тип земельного участка")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("8682edd5-0d38-494b-b3b9-245397ccddcb")]
    [Bars.Rms.Core.Attributes.Uid("8682edd5-0d38-494b-b3b9-245397ccddcb")]
    public class TipZemelNogoUchastka : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public TipZemelNogoUchastka(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("c92e97c9-4fdc-4a2d-af71-4769011d5abe")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("80af50f7-f60d-4cf8-b564-5b9b51bc7c36")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}