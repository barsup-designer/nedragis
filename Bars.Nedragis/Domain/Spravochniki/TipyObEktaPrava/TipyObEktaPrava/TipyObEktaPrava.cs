namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Типы объекта права
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Типы объекта права")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("3ac22ac5-70e7-4061-a191-d95169f8895c")]
    [Bars.Rms.Core.Attributes.Uid("3ac22ac5-70e7-4061-a191-d95169f8895c")]
    public class TipyObEktaPrava : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public TipyObEktaPrava(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("a4b65e41-3028-4b33-b824-d0f52bb6dd23")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("a2161223-e3f1-4eee-99a8-c005909756b1")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}