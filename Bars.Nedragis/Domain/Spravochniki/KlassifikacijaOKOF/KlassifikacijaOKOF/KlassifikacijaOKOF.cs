namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Классификация ОКОФ
    /// ОКОФ - Общероссийский классификатор основных фондов, его категории
    /// </summary>
    [Bars.B4.Utils.Display(@"Классификация ОКОФ")]
    [Bars.B4.Utils.Description(@"ОКОФ - Общероссийский классификатор основных фондов, его категории")]
    [System.Runtime.InteropServices.GuidAttribute("f103b0bd-3d0c-418c-b3cc-f68fd6b5ec2f")]
    [Bars.Rms.Core.Attributes.Uid("f103b0bd-3d0c-418c-b3cc-f68fd6b5ec2f")]
    public class KlassifikacijaOKOF : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public KlassifikacijaOKOF(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("de4d4f76-5db4-4670-8729-17cd42a01e3f")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("6cc00b9e-2a55-4b59-b182-8af607b62fa3")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}