namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// ОКТМО
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"ОКТМО")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("151d2f6f-fbb5-4684-9f56-92b8ed775121")]
    [Bars.Rms.Core.Attributes.Uid("151d2f6f-fbb5-4684-9f56-92b8ed775121")]
    public class OKTMO : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public OKTMO(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("492e145e-47fb-4922-a77b-d83f999ade55")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("1d9789e7-46d7-4057-97e8-24ce9183a254")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент")]
        [Bars.Rms.Core.Attributes.Uid("61a7ea09-4dda-48e0-9b6c-002f238220a6")]
        public virtual Bars.Nedragis.OKTMO parent_id
        {
            get;
            set;
        }
    }
}