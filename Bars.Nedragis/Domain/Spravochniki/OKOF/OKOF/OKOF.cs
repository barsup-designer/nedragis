namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// ОКОФ
    /// ОКОФ - Общероссийский классификатор основных фондов 
    /// </summary>
    [Bars.B4.Utils.Display(@"ОКОФ")]
    [Bars.B4.Utils.Description(@"ОКОФ - Общероссийский классификатор основных фондов ")]
    [System.Runtime.InteropServices.GuidAttribute("594403ac-d85f-4ef6-8815-bf4c01f3b7f9")]
    [Bars.Rms.Core.Attributes.Uid("594403ac-d85f-4ef6-8815-bf4c01f3b7f9")]
    public class OKOF : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public OKOF(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("68c5f8ed-cccf-4b7a-9b3e-2ca04e22eb5f")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("c50ac8dc-07e1-43a6-a3e2-43c64c9567c4")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Классификация
        /// </summary>
        [Bars.B4.Utils.Display("Классификация")]
        [Bars.Rms.Core.Attributes.Uid("e267b6bd-c137-496e-87f7-295177a8b7ca")]
        public virtual Bars.Nedragis.KlassifikacijaOKOF clasifier
        {
            get;
            set;
        }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент")]
        [Bars.Rms.Core.Attributes.Uid("83cb6be4-8b49-4019-b660-32060ffeec63")]
        public virtual Bars.Nedragis.OKOF parent_id
        {
            get;
            set;
        }

        /// <summary>
        /// Контрольное число
        /// </summary>
        [Bars.B4.Utils.Display("Контрольное число")]
        [Bars.Rms.Core.Attributes.Uid("dcf91b29-5256-4fd7-97e1-ecdb0a8a626d")]
        public virtual System.Int32 check_number
        {
            get;
            set;
        }
    }
}