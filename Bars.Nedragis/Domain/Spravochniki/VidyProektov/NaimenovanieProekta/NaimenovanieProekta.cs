namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Наименование проекта
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Наименование проекта")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("216f9ac4-1368-4dff-97b0-1c1e19d23db4")]
    [Bars.Rms.Core.Attributes.Uid("216f9ac4-1368-4dff-97b0-1c1e19d23db4")]
    public class NaimenovanieProekta : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public NaimenovanieProekta(): base ()
        {
        }

        /// <summary>
        /// Наименование проекта
        /// </summary>
        [Bars.B4.Utils.Display("Наименование проекта")]
        [Bars.Rms.Core.Attributes.Uid("9d7dce0e-6352-4dfb-8120-d110671a70b2")]
        public virtual System.String Element1511755872431
        {
            get;
            set;
        }
    }
}