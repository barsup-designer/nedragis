namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// ОКФС
    /// Общероссийский Классификатор Форм Собственности. 
    /// </summary>
    [Bars.B4.Utils.Display(@"ОКФС")]
    [Bars.B4.Utils.Description(@"Общероссийский Классификатор Форм Собственности. ")]
    [System.Runtime.InteropServices.GuidAttribute("c2b0994f-1f3c-411c-9f98-f03071f14265")]
    [Bars.Rms.Core.Attributes.Uid("c2b0994f-1f3c-411c-9f98-f03071f14265")]
    public class OKFS : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public OKFS(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("8c54f47d-9f25-42e8-ba8c-2d7f8eca023a")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("6f904d04-8b48-4c89-ae50-0bed3d2b32fb")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Алгоритм сбора
        /// </summary>
        [Bars.B4.Utils.Display("Алгоритм сбора")]
        [Bars.Rms.Core.Attributes.Uid("d1410cf8-8e6c-4e00-a451-1b2012e31470")]
        public virtual System.String check_algorithm
        {
            get;
            set;
        }
    }
}