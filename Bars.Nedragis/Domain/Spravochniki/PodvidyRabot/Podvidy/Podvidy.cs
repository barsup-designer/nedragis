namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Подвиды
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Подвиды")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("71b344e1-9560-43aa-90d5-b5ea06bf434e")]
    [Bars.Rms.Core.Attributes.Uid("71b344e1-9560-43aa-90d5-b5ea06bf434e")]
    public class Podvidy : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Podvidy(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("3adac4b4-f38f-4571-af4f-ba58ff616ffa")]
        public virtual System.String Element1474267693056
        {
            get;
            set;
        }

        /// <summary>
        /// вид
        /// </summary>
        [Bars.B4.Utils.Display("вид")]
        [Bars.Rms.Core.Attributes.Uid("41be5a35-d47a-4fdf-8ada-af29c956e8c7")]
        public virtual Bars.Nedragis.VidyRabot Element1474349306046
        {
            get;
            set;
        }
    }
}