namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// zima1
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"zima1")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("346b2315-bb41-485e-b013-fb0a7c9936d8")]
    [Bars.Rms.Core.Attributes.Uid("346b2315-bb41-485e-b013-fb0a7c9936d8")]
    public class Zima1 : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Zima1(): base ()
        {
        }

        /// <summary>
        /// zima2
        /// </summary>
        [Bars.B4.Utils.Display("zima2")]
        [Bars.Rms.Core.Attributes.Uid("faa39da7-bb2a-496f-ab65-c8d1b7068ceb")]
        public virtual System.String Element1517294546876
        {
            get;
            set;
        }
    }
}