namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Отрасль
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Отрасль")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("86d46288-6a31-43c1-93b8-e0320f3c1bc6")]
    [Bars.Rms.Core.Attributes.Uid("86d46288-6a31-43c1-93b8-e0320f3c1bc6")]
    public class Otrasl : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Otrasl(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("c63953a0-70a5-4e2f-9ea5-faa965e53b28")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("fc883ba1-41e9-4fa1-aef3-2e4e317efb74")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}