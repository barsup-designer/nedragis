namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Вид ошибки
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Вид ошибки")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("3a3bd732-b623-4fde-9f5f-d0420fd87b86")]
    [Bars.Rms.Core.Attributes.Uid("3a3bd732-b623-4fde-9f5f-d0420fd87b86")]
    public class VidOshibki : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public VidOshibki(): base ()
        {
        }

        /// <summary>
        /// Вид ошибки
        /// </summary>
        [Bars.B4.Utils.Display("Вид ошибки")]
        [Bars.Rms.Core.Attributes.Uid("543afeb1-d32b-48e2-af94-16e7532daf6e")]
        public virtual System.String Element1511848489595
        {
            get;
            set;
        }
    }
}