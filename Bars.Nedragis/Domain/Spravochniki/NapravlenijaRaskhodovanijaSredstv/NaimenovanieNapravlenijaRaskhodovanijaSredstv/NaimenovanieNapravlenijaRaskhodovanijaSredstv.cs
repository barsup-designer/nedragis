namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Наименование проекта
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Наименование проекта")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("fae507ae-27bd-4670-8651-083e8137f7b7")]
    [Bars.Rms.Core.Attributes.Uid("fae507ae-27bd-4670-8651-083e8137f7b7")]
    public class NaimenovanieNapravlenijaRaskhodovanijaSredstv : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public NaimenovanieNapravlenijaRaskhodovanijaSredstv(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("c34cb84e-a373-4b8a-961c-b3bc90e327ad")]
        public virtual System.String Element1455708457805
        {
            get;
            set;
        }

        /// <summary>
        /// Вид
        /// </summary>
        [Bars.B4.Utils.Display("Вид")]
        [Bars.Rms.Core.Attributes.Uid("3e87114a-562c-491a-9a90-581e274e59f0")]
        public virtual System.String Element1511847414376
        {
            get;
            set;
        }
    }
}