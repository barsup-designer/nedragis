namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Вид работ ОПИНФ
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Вид работ ОПИНФ")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("d718884f-b8e6-4ddd-a77f-109da91c10ae")]
    [Bars.Rms.Core.Attributes.Uid("d718884f-b8e6-4ddd-a77f-109da91c10ae")]
    public class VidRabotOPINF : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public VidRabotOPINF(): base ()
        {
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("c2c721fc-08af-4176-a058-c8add2eb2efa")]
        public virtual System.String VidRabotOPINF222
        {
            get;
            set;
        }

        /// <summary>
        /// Подвид работ
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("824011ee-c2df-41ef-b2d0-a051aba5c1cd")]
        public virtual System.String Element1510134562537
        {
            get;
            set;
        }
    }
}