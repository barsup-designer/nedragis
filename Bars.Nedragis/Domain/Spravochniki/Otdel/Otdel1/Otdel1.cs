namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Отдел
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Отдел")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("cae99050-ad22-476a-a24b-f2db200de1ed")]
    [Bars.Rms.Core.Attributes.Uid("cae99050-ad22-476a-a24b-f2db200de1ed")]
    public class Otdel1 : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Otdel1(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("c9cfc806-3d3e-4aa7-b12c-d9218426a9d7")]
        public virtual System.String NAME_SP
        {
            get;
            set;
        }
    }
}