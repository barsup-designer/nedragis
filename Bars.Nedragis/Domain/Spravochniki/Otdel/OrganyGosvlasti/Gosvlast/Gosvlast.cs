namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Госвласть
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Госвласть")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("c562c480-75c2-4db5-8d1f-fef70d1c2e0c")]
    [Bars.Rms.Core.Attributes.Uid("c562c480-75c2-4db5-8d1f-fef70d1c2e0c")]
    public class Gosvlast : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Gosvlast(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("7112a776-b91e-4e76-8e1b-95d04755a2a6")]
        public virtual System.String Element1474350949546
        {
            get;
            set;
        }

        /// <summary>
        /// Полное наименование
        /// </summary>
        [Bars.B4.Utils.Display("Полное наименование")]
        [Bars.Rms.Core.Attributes.Uid("b0410ba4-aee5-4996-8bd4-fbe114dd73da")]
        public virtual System.String Element1474350976371
        {
            get;
            set;
        }
    }
}