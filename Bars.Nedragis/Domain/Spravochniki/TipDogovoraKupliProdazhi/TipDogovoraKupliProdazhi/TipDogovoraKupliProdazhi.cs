namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Тип договора купли-продажи
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Тип договора купли-продажи")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("925276c8-b776-4527-87f9-f859857aa018")]
    [Bars.Rms.Core.Attributes.Uid("925276c8-b776-4527-87f9-f859857aa018")]
    public class TipDogovoraKupliProdazhi : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public TipDogovoraKupliProdazhi(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("bd170a0d-5b3b-431d-ba1d-66c4f6e9b2e8")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("9120c06f-ae68-4115-9a53-6b34fb6a35d8")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}