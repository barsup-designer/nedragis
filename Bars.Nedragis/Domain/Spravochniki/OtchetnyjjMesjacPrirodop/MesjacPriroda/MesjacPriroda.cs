namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// МесяцПрирода
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"МесяцПрирода")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("706f6c24-2651-4253-b1a9-71d0b627945e")]
    [Bars.Rms.Core.Attributes.Uid("706f6c24-2651-4253-b1a9-71d0b627945e")]
    public class MesjacPriroda : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public MesjacPriroda(): base ()
        {
        }

        /// <summary>
        /// Отчетный месяц
        /// </summary>
        [Bars.B4.Utils.Display("Отчетный месяц")]
        [Bars.Rms.Core.Attributes.Uid("6f3bdf0a-69b1-44b0-87bb-6c627aa28f92")]
        public virtual System.String month_pr
        {
            get;
            set;
        }
    }
}