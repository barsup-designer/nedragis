namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// ОКОГУ
    /// Общероссийский Классификатор Органов Государственной власти и Управления. ОКОГУ нужен для: -  для упорядочения и систематизации информации об органах государственной власти и управления, -  для индентификации органов государственной власти и управления в Едином государственном регистре предприятий и организаций, -  для удобства статистических наблюдений за входящими в классификатор органами. 
    /// </summary>
    [Bars.B4.Utils.Display(@"ОКОГУ")]
    [Bars.B4.Utils.Description(@"Общероссийский Классификатор Органов Государственной власти и Управления. ОКОГУ нужен для: -  для упорядочения и систематизации информации об органах государственной власти и управления, -  для индентификации органов государственной власти и управления в Едином государственном регистре предприятий и организаций, -  для удобства статистических наблюдений за входящими в классификатор органами. ")]
    [System.Runtime.InteropServices.GuidAttribute("25e226c5-654b-43a6-b9ff-36d48b8e7d0b")]
    [Bars.Rms.Core.Attributes.Uid("25e226c5-654b-43a6-b9ff-36d48b8e7d0b")]
    public class OKOGU : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public OKOGU(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("29c6426f-9226-4196-8657-18f2392e98c1")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("37563da5-e29b-4a9b-8591-e73d00cb14ce")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент")]
        [Bars.Rms.Core.Attributes.Uid("005d56c1-90c3-4611-8d0e-efb1378307f6")]
        public virtual Bars.Nedragis.OKOGU parent_id
        {
            get;
            set;
        }

        /// <summary>
        /// Краткое наименование
        /// </summary>
        [Bars.B4.Utils.Display("Краткое наименование")]
        [Bars.Rms.Core.Attributes.Uid("37aee374-c664-411b-a3d9-134df085d2a3")]
        public virtual System.String name_short
        {
            get;
            set;
        }
    }
}