namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// ОКАТО
    /// Общероссийский классификатор административно-территориального деления объектов. Предназначен для обеспечения достоверности, сопоставимости и автоматизированной обработки информации в разрезах административно-территориального деления в таких сферах, как статистика, экономика и другие.
    /// </summary>
    [Bars.B4.Utils.Display(@"ОКАТО")]
    [Bars.B4.Utils.Description(@"Общероссийский классификатор административно-территориального деления объектов. Предназначен для обеспечения достоверности, сопоставимости и автоматизированной обработки информации в разрезах административно-территориального деления в таких сферах, как статистика, экономика и другие.")]
    [System.Runtime.InteropServices.GuidAttribute("209f5156-b2b9-4a80-b96a-100c2cf0083d")]
    [Bars.Rms.Core.Attributes.Uid("209f5156-b2b9-4a80-b96a-100c2cf0083d")]
    public class OKATO : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public OKATO(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("f1c73dea-11f0-43fc-b595-04b84516caea")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("1d67444a-bfea-4a7f-8add-1f6e1362b9a0")]
        public virtual System.String name1
        {
            get;
            set;
        }

        /// <summary>
        /// Районный центр
        /// </summary>
        [Bars.B4.Utils.Display("Районный центр")]
        [Bars.Rms.Core.Attributes.Uid("687b9925-7fcb-48da-86ae-aa647ff4e01b")]
        public virtual System.String center_name
        {
            get;
            set;
        }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент")]
        [Bars.Rms.Core.Attributes.Uid("1bcffb34-e126-4435-b34a-c7f8f7ec8696")]
        public virtual Bars.Nedragis.OKATO parent_id
        {
            get;
            set;
        }
    }
}