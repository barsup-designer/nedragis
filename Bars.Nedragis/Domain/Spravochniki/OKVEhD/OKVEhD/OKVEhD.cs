namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// ОКВЭД
    /// Общероссийский классификатор видов экономической деятельности
    /// </summary>
    [Bars.B4.Utils.Display(@"ОКВЭД")]
    [Bars.B4.Utils.Description(@"Общероссийский классификатор видов экономической деятельности")]
    [System.Runtime.InteropServices.GuidAttribute("c6f1d936-9d21-451f-b3c5-8c562fc1cff7")]
    [Bars.Rms.Core.Attributes.Uid("c6f1d936-9d21-451f-b3c5-8c562fc1cff7")]
    public class OKVEhD : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public OKVEhD(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("12db5f89-3ecd-4d5b-87c4-60b76bd51438")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("2e4c719a-5963-490f-999e-2e5cbc07b9b8")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент")]
        [Bars.Rms.Core.Attributes.Uid("ef9b6802-fe78-4392-bfa9-13340e5dbf4f")]
        public virtual Bars.Nedragis.OKVEhD parent_id
        {
            get;
            set;
        }
    }
}