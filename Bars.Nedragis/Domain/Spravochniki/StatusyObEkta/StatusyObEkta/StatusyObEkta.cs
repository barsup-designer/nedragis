namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Статусы объекта
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Статусы объекта")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("149b70cd-8d3e-4355-87d7-5562fad24e4e")]
    [Bars.Rms.Core.Attributes.Uid("149b70cd-8d3e-4355-87d7-5562fad24e4e")]
    public class StatusyObEkta : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public StatusyObEkta(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("35cea1b5-b148-4ef3-870c-c4a1a7eb24b4")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("fa2620ae-91ce-42d7-be8e-2ee25559b3c7")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}