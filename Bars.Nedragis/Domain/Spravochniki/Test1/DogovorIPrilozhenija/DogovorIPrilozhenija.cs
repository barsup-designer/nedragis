namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Договор и приложения
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Договор и приложения")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("54f68f92-3a6c-4807-8596-3761387bcde7")]
    [Bars.Rms.Core.Attributes.Uid("54f68f92-3a6c-4807-8596-3761387bcde7")]
    public class DogovorIPrilozhenija : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public DogovorIPrilozhenija(): base ()
        {
        }

        /// <summary>
        /// Номер договора
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("b09f20db-e362-4d94-ad62-466eb290ff92")]
        public virtual System.Int32 Element1478683081624
        {
            get;
            set;
        }

        /// <summary>
        /// Дата договора
        /// </summary>
        [Bars.B4.Utils.Display("Дата договора")]
        [Bars.Rms.Core.Attributes.Uid("5cd41ef4-a26b-4e27-ba1b-41dfce70827a")]
        public virtual System.DateTime Element1478683584922
        {
            get;
            set;
        }

        /// <summary>
        /// Номер спецификации
        /// </summary>
        [Bars.B4.Utils.Display("Номер спецификации")]
        [Bars.Rms.Core.Attributes.Uid("e389ac76-f782-43ff-a63e-3c544c1d0b5a")]
        public virtual System.String Element1478683617170
        {
            get;
            set;
        }

        /// <summary>
        /// Дата спецификации
        /// </summary>
        [Bars.B4.Utils.Display("Дата спецификации")]
        [Bars.Rms.Core.Attributes.Uid("d78b040a-eff5-48ad-ab02-4ae584ae55a8")]
        public virtual System.String Element1478683683665
        {
            get;
            set;
        }

        /// <summary>
        /// Номер соглашения
        /// </summary>
        [Bars.B4.Utils.Display("Номер соглашения")]
        [Bars.Rms.Core.Attributes.Uid("f5088855-43ee-469a-ab57-b7e60ee59748")]
        public virtual System.Int32 Element1478683765504
        {
            get;
            set;
        }

        /// <summary>
        /// Дата соглашения
        /// </summary>
        [Bars.B4.Utils.Display("Дата соглашения")]
        [Bars.Rms.Core.Attributes.Uid("7bf3e523-20f5-413c-bbad-d72ce7def3da")]
        public virtual System.DateTime Element1478683832391
        {
            get;
            set;
        }

        /// <summary>
        /// НДС на естеств языке
        /// </summary>
        [Bars.B4.Utils.Display("НДС на естеств языке")]
        [Bars.Rms.Core.Attributes.Uid("f8c50432-3ce1-4275-8fcc-e40076cd9fe0")]
        public virtual System.String Element1478683872055
        {
            get;
            set;
        }

        /// <summary>
        /// Сумма на естеств языке
        /// </summary>
        [Bars.B4.Utils.Display("Сумма на естеств языке")]
        [Bars.Rms.Core.Attributes.Uid("fddb562a-4392-4d86-82a2-3e624b47b233")]
        public virtual System.String Element1478683916351
        {
            get;
            set;
        }

        /// <summary>
        /// ФИО Руководителя ГКУ 
        /// </summary>
        [Bars.B4.Utils.Display("ФИО Руководителя ГКУ ")]
        [Bars.Rms.Core.Attributes.Uid("f1c2062c-d493-450a-b6d8-8528faffe37e")]
        public virtual System.String Element1478683945974
        {
            get;
            set;
        }

        /// <summary>
        /// Номер акта сдачи-приемки
        /// </summary>
        [Bars.B4.Utils.Display("Номер акта сдачи-приемки")]
        [Bars.Rms.Core.Attributes.Uid("160ee9ef-d5d4-424c-9260-9859d1baf364")]
        public virtual System.String Element1478684021941
        {
            get;
            set;
        }

        /// <summary>
        /// Дата акта сдачи-приемки
        /// </summary>
        [Bars.B4.Utils.Display("Дата акта сдачи-приемки")]
        [Bars.Rms.Core.Attributes.Uid("2c8d6a81-29f9-49eb-b133-9160b2bf56ed")]
        public virtual System.DateTime Element1478684113244
        {
            get;
            set;
        }

        /// <summary>
        /// Номер спецификации
        /// </summary>
        [Bars.B4.Utils.Display("Номер спецификации")]
        [Bars.Rms.Core.Attributes.Uid("d5140e72-1e18-478c-a8af-a0072f47e416")]
        public virtual System.String Element1478684143764
        {
            get;
            set;
        }
    }
}