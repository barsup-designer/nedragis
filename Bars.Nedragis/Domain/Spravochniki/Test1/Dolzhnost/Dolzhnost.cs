namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// должность
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"должность")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("c02a3f65-ec3a-44cf-b531-1608eaf5b276")]
    [Bars.Rms.Core.Attributes.Uid("c02a3f65-ec3a-44cf-b531-1608eaf5b276")]
    public class Dolzhnost : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Dolzhnost(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("be94622b-36b7-4326-918a-fa6875f02121")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}