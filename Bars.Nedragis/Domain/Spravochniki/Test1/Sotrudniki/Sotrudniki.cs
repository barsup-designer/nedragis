namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Сотрудники
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Сотрудники")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("7fec6da9-d7ef-4a54-b1c1-53d17443322d")]
    [Bars.Rms.Core.Attributes.Uid("7fec6da9-d7ef-4a54-b1c1-53d17443322d")]
    public class Sotrudniki : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Sotrudniki(): base ()
        {
        }

        /// <summary>
        /// ФИО
        /// </summary>
        [Bars.B4.Utils.Display("ФИО")]
        [Bars.Rms.Core.Attributes.Uid("6a596978-953b-45e7-9691-cc6ae503cb70")]
        public virtual System.String fio
        {
            get;
            set;
        }

        /// <summary>
        /// Дата
        /// </summary>
        [Bars.B4.Utils.Display("Дата")]
        [Bars.Rms.Core.Attributes.Uid("97177188-d8a3-4cfe-bfb2-a5c633f0420a")]
        public virtual System.DateTime dat
        {
            get;
            set;
        }

        /// <summary>
        /// Должность
        /// </summary>
        [Bars.B4.Utils.Display("Должность")]
        [Bars.Rms.Core.Attributes.Uid("e9eccec8-49aa-4ac8-8aff-ba8569b3821a")]
        public virtual Bars.Nedragis.Dolzhnost dolzh
        {
            get;
            set;
        }
    }
}