namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Магнитные носители
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Магнитные носители")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("3e627ca5-3cf8-4bec-8d07-a5998d6b28df")]
    [Bars.Rms.Core.Attributes.Uid("3e627ca5-3cf8-4bec-8d07-a5998d6b28df")]
    public class MagnitNositeli : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public MagnitNositeli(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("5a871f71-c1f9-498c-b5d1-0be78bebbcf1")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Бар-код
        /// </summary>
        [Bars.B4.Utils.Display("Бар-код")]
        [Bars.Rms.Core.Attributes.Uid("a8bd2154-cf74-4434-ab5b-048fb3710e7c")]
        public virtual System.String barcod
        {
            get;
            set;
        }
    }
}