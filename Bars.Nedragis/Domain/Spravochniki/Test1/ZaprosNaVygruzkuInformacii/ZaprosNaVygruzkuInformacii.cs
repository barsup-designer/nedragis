namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Результат выгрузки информации
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Результат выгрузки информации")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("3c1b2822-3c88-44a1-935b-14a08dbbc646")]
    [Bars.Rms.Core.Attributes.Uid("3c1b2822-3c88-44a1-935b-14a08dbbc646")]
    public class ZaprosNaVygruzkuInformacii : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public ZaprosNaVygruzkuInformacii(): base ()
        {
        }

        /// <summary>
        /// Дата
        /// </summary>
        [Bars.B4.Utils.Display("Дата")]
        [Bars.Rms.Core.Attributes.Uid("5e8db345-5295-4ca4-9e91-426ebc1944f6")]
        public virtual System.DateTime Element1478675644658
        {
            get;
            set;
        }

        /// <summary>
        /// Объем выгруженной информации
        /// </summary>
        [Bars.B4.Utils.Display("Объем выгруженной информации")]
        [Bars.Rms.Core.Attributes.Uid("f52bb5d1-acdc-40f9-b9e0-9a7362d38438")]
        public virtual System.String Element1478675679144
        {
            get;
            set;
        }

        /// <summary>
        /// Количество магнитных носителей
        /// </summary>
        [Bars.B4.Utils.Display("Количество магнитных носителей")]
        [Bars.Rms.Core.Attributes.Uid("364ccbca-d4eb-4abc-a715-ed3bda84b6a7")]
        public virtual System.Int32 Element1478682384618
        {
            get;
            set;
        }

        /// <summary>
        /// Номер акта приема передачи
        /// </summary>
        [Bars.B4.Utils.Display("Номер акта приема передачи")]
        [Bars.Rms.Core.Attributes.Uid("624eb41d-a0aa-4c1f-a762-2e786349da89")]
        public virtual System.Int32 Element1478682415266
        {
            get;
            set;
        }

        /// <summary>
        /// Дата акта приема передачи
        /// </summary>
        [Bars.B4.Utils.Display("Дата акта приема передачи")]
        [Bars.Rms.Core.Attributes.Uid("03ee21a1-d170-4089-8a6e-db49d4f79931")]
        public virtual System.DateTime Element1478682460890
        {
            get;
            set;
        }

        /// <summary>
        /// Номер запроса
        /// </summary>
        [Bars.B4.Utils.Display("Номер запроса")]
        [Bars.Rms.Core.Attributes.Uid("ac5ac944-727a-4cbe-aba4-1c2d68ecff4b")]
        public virtual System.String Element1478682508834
        {
            get;
            set;
        }
    }
}