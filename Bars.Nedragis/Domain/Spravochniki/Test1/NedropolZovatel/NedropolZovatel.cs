namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Недропользователь
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Недропользователь")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb")]
    [Bars.Rms.Core.Attributes.Uid("0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb")]
    public class NedropolZovatel : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public NedropolZovatel(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("a89e07c3-c289-45c4-8389-931826ce22ab")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Адрес
        /// </summary>
        [Bars.B4.Utils.Display("Адрес")]
        [Bars.Rms.Core.Attributes.Uid("24a10b32-e511-4396-925b-ad1fbb6e3482")]
        public virtual System.String adres
        {
            get;
            set;
        }

        /// <summary>
        /// Телефон
        /// </summary>
        [Bars.B4.Utils.Display("Телефон")]
        [Bars.Rms.Core.Attributes.Uid("4fe69fb9-ffcc-4db7-823e-f8f3a8860834")]
        public virtual System.String tel
        {
            get;
            set;
        }

        /// <summary>
        /// e-mail
        /// </summary>
        [Bars.B4.Utils.Display("e-mail")]
        [Bars.Rms.Core.Attributes.Uid("359a580a-717e-496a-8c5c-b0388c7ceff9")]
        public virtual System.String mail
        {
            get;
            set;
        }

        /// <summary>
        /// Факс
        /// </summary>
        [Bars.B4.Utils.Display("Факс")]
        [Bars.Rms.Core.Attributes.Uid("5191b69b-a48b-460d-b6eb-db4b64dd8042")]
        public virtual System.String fax
        {
            get;
            set;
        }

        /// <summary>
        /// Руководитель
        /// </summary>
        [Bars.B4.Utils.Display("Руководитель")]
        [Bars.Rms.Core.Attributes.Uid("443d11dd-2ca9-4432-86c4-802676f4d0f2")]
        public virtual System.String member
        {
            get;
            set;
        }

        /// <summary>
        /// ИНН
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("1de99098-c379-46bc-8d3a-6b2b13873c95")]
        public virtual System.String inn
        {
            get;
            set;
        }

        /// <summary>
        /// КПП
        /// </summary>
        [Bars.B4.Utils.Display("КПП")]
        [Bars.Rms.Core.Attributes.Uid("95777287-2d75-44a3-b8c5-af6cf8e05fa2")]
        public virtual System.String kpp
        {
            get;
            set;
        }

        /// <summary>
        /// Расчетный счет
        /// </summary>
        [Bars.B4.Utils.Display("Расчетный счет")]
        [Bars.Rms.Core.Attributes.Uid("0a357ad8-a6fc-426f-a775-50a030141637")]
        public virtual System.String rs
        {
            get;
            set;
        }

        /// <summary>
        /// БИК
        /// </summary>
        [Bars.B4.Utils.Display("БИК")]
        [Bars.Rms.Core.Attributes.Uid("154766bb-ce6a-4dd3-a18c-f1cf06aaf9d3")]
        public virtual System.String bik
        {
            get;
            set;
        }

        /// <summary>
        /// Корресп. счет
        /// </summary>
        [Bars.B4.Utils.Display("Корресп. счет")]
        [Bars.Rms.Core.Attributes.Uid("9fe34937-d8a5-4d56-aabc-ea818ac5cc20")]
        public virtual System.String kors
        {
            get;
            set;
        }
    }
}