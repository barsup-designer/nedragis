namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Позиции спецификации
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Позиции спецификации")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("6a80d697-d2cc-4fc4-ae2f-12a4cb52d3c4")]
    [Bars.Rms.Core.Attributes.Uid("6a80d697-d2cc-4fc4-ae2f-12a4cb52d3c4")]
    public class PoziciiS : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public PoziciiS(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("3609fe40-219f-463a-a3c6-a85f30719b3a")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Стоимость подготовки
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость подготовки")]
        [Bars.Rms.Core.Attributes.Uid("78aa5961-b7bc-4b11-ab7b-36ab39105b82")]
        public virtual System.Decimal s_pod
        {
            get;
            set;
        }

        /// <summary>
        /// Стоимость хранения
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость хранения")]
        [Bars.Rms.Core.Attributes.Uid("4f42ff2b-85ef-462e-a0e4-7641d9eee9ab")]
        public virtual System.Decimal s_hran
        {
            get;
            set;
        }

        /// <summary>
        /// Накладные расходы
        /// </summary>
        [Bars.B4.Utils.Display("Накладные расходы")]
        [Bars.Rms.Core.Attributes.Uid("d3fa4905-3e0c-4ab7-b621-d6869cc55b0f")]
        public virtual System.Decimal naklad
        {
            get;
            set;
        }

        /// <summary>
        /// НДС
        /// </summary>
        [Bars.B4.Utils.Display("НДС")]
        [Bars.Rms.Core.Attributes.Uid("67bf7cc3-093e-4ec1-9fd3-726d1029cce3")]
        public virtual System.Decimal nds
        {
            get;
            set;
        }

        /// <summary>
        /// Количество
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("eea7c85c-7109-478c-af80-f9603aec0994")]
        public virtual System.Int32 Element1478602748211
        {
            get;
            set;
        }
    }
}