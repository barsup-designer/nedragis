namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Связь носитель выгрузка
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Связь носитель выгрузка")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("6d6396be-595c-4d8e-bfd7-98531fb4982b")]
    [Bars.Rms.Core.Attributes.Uid("6d6396be-595c-4d8e-bfd7-98531fb4982b")]
    public class SvjazNositelVygruzka : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public SvjazNositelVygruzka(): base ()
        {
        }
    }
}