namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Платежное поручение
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Платежное поручение")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("c719553b-685c-415f-a312-0e6fb3bba679")]
    [Bars.Rms.Core.Attributes.Uid("c719553b-685c-415f-a312-0e6fb3bba679")]
    public class PlatezhPoruchenie : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public PlatezhPoruchenie(): base ()
        {
        }

        /// <summary>
        /// Дата
        /// </summary>
        [Bars.B4.Utils.Display("Дата")]
        [Bars.Rms.Core.Attributes.Uid("9ba36e14-63f3-40f1-8207-93ae85089637")]
        public virtual System.DateTime dattr
        {
            get;
            set;
        }

        /// <summary>
        /// Сумма
        /// </summary>
        [Bars.B4.Utils.Display("Сумма")]
        [Bars.Rms.Core.Attributes.Uid("ced0b693-0832-47ad-bbfb-5ee88113e126")]
        public virtual System.Decimal suma
        {
            get;
            set;
        }

        /// <summary>
        /// Номер договора
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("fa34704c-ab4e-4b72-9618-dca72a5da222")]
        public virtual System.Int32 Element1478594361589
        {
            get;
            set;
        }

        /// <summary>
        /// Дата договора
        /// </summary>
        [Bars.B4.Utils.Display("Дата договора")]
        [Bars.Rms.Core.Attributes.Uid("a4303c99-a5bc-40e0-a537-021123a343bd")]
        public virtual System.DateTime dataa
        {
            get;
            set;
        }
    }
}