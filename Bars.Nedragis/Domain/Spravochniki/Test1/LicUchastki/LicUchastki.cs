namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Лицензионные участки
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Лицензионные участки")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("2675cadb-dc1f-436f-8d36-cd55fc784aaf")]
    [Bars.Rms.Core.Attributes.Uid("2675cadb-dc1f-436f-8d36-cd55fc784aaf")]
    public class LicUchastki : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public LicUchastki(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("437d81a1-790e-4ac7-95f3-1211e28f4f44")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Номер лицензии
        /// </summary>
        [Bars.B4.Utils.Display("Номер лицензии")]
        [Bars.Rms.Core.Attributes.Uid("8b76ae1a-a1b2-4969-aced-c8b728df598c")]
        public virtual System.String Element1478588881912
        {
            get;
            set;
        }

        /// <summary>
        /// Описание расположение участка
        /// </summary>
        [Bars.B4.Utils.Display("Описание расположение участка")]
        [Bars.Rms.Core.Attributes.Uid("b08a62b2-ddef-4bd1-973b-0c1474be0759")]
        public virtual System.String Element1478589271306
        {
            get;
            set;
        }
    }
}