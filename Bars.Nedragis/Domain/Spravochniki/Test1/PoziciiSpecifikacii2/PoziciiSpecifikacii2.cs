namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Позиции спецификации 2
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Позиции спецификации 2")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("081e877c-5378-462d-819d-97b8035e72d1")]
    [Bars.Rms.Core.Attributes.Uid("081e877c-5378-462d-819d-97b8035e72d1")]
    public class PoziciiSpecifikacii2 : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public PoziciiSpecifikacii2(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("f84ba8c3-f3ab-49a0-b801-99f4ccb81d1f")]
        public virtual System.String Element1478603373738
        {
            get;
            set;
        }

        /// <summary>
        /// Количество
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("6368bc00-9335-45cb-ad1b-ea27b3bda5d3")]
        public virtual System.String Element1478603394415
        {
            get;
            set;
        }

        /// <summary>
        /// Стоимость подготовки
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость подготовки")]
        [Bars.Rms.Core.Attributes.Uid("5a16d0e2-a2a9-453f-af69-71106f9bb5e2")]
        public virtual System.String Element1478603416645
        {
            get;
            set;
        }

        /// <summary>
        /// Стоимость хранения
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость хранения")]
        [Bars.Rms.Core.Attributes.Uid("6e501614-8d15-45cc-b2bc-080bde26e2f5")]
        public virtual System.String Element1478603450746
        {
            get;
            set;
        }

        /// <summary>
        /// Итого
        /// </summary>
        [Bars.B4.Utils.Display("Итого")]
        [Bars.Rms.Core.Attributes.Uid("f2d77729-1a88-4b7a-aa7f-e1c64a1b2806")]
        public virtual System.String Element1478603494464
        {
            get;
            set;
        }

        /// <summary>
        /// Накладные расходы
        /// </summary>
        [Bars.B4.Utils.Display("Накладные расходы")]
        [Bars.Rms.Core.Attributes.Uid("06d9b311-7f2e-4db9-9775-a940e3452002")]
        public virtual System.String Element1478603523493
        {
            get;
            set;
        }

        /// <summary>
        /// НДС
        /// </summary>
        [Bars.B4.Utils.Display("НДС")]
        [Bars.Rms.Core.Attributes.Uid("77f64c39-8890-46d3-8b3d-d620a06732bf")]
        public virtual System.String Element1478603543580
        {
            get;
            set;
        }

        /// <summary>
        /// Всего
        /// </summary>
        [Bars.B4.Utils.Display("Всего")]
        [Bars.Rms.Core.Attributes.Uid("0c53dffe-dadf-4994-b8d5-82843c9eaec8")]
        public virtual System.String Element1478603563538
        {
            get;
            set;
        }
    }
}