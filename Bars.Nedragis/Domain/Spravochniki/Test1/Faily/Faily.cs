namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Файлы
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Файлы")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("e2632129-9137-4a44-be60-edee3d85b0eb")]
    [Bars.Rms.Core.Attributes.Uid("e2632129-9137-4a44-be60-edee3d85b0eb")]
    public class Faily : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Faily(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("543d9410-a627-443e-a77b-074ed8370e2f")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Объем
        /// </summary>
        [Bars.B4.Utils.Display("Объем")]
        [Bars.Rms.Core.Attributes.Uid("27cad8bd-33a1-4990-b687-6d493c6df45c")]
        public virtual System.Int32 obem
        {
            get;
            set;
        }
    }
}