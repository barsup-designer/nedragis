namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Состояние объекта
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Состояние объекта")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("f48fa006-5e53-47ff-8494-db17c0232275")]
    [Bars.Rms.Core.Attributes.Uid("f48fa006-5e53-47ff-8494-db17c0232275")]
    public class SostojanieObEkta : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public SostojanieObEkta(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("3a19a888-e16a-4112-91d3-e3cd223f7992")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("6450ad45-696c-4bf5-ba43-75d0422922a0")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}