namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// График платежей
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"График платежей")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("533b04e9-5bdd-49b7-bec8-07e684d2ecc2")]
    [Bars.Rms.Core.Attributes.Uid("533b04e9-5bdd-49b7-bec8-07e684d2ecc2")]
    public class GrafikPlatezhejj : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public GrafikPlatezhejj(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("ff9b0b05-acc1-4092-94fc-5e7e92587ec4")]
        public virtual System.String name1
        {
            get;
            set;
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("8029d330-d496-435d-8d12-e1245d8442a1")]
        public virtual System.String code1
        {
            get;
            set;
        }
    }
}