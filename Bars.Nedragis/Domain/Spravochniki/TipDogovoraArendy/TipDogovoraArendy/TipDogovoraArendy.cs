namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Тип договора аренды
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Тип договора аренды")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("0e17ebf5-8501-4e52-b1d9-2fc926e2bd3e")]
    [Bars.Rms.Core.Attributes.Uid("0e17ebf5-8501-4e52-b1d9-2fc926e2bd3e")]
    public class TipDogovoraArendy : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public TipDogovoraArendy(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("5000c02a-9371-4aab-b281-7e0ca10a6175")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("f253c434-4aec-4fa4-8f66-72bc252f46f3")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}