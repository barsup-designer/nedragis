namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Документы удостоверяющие личность
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Документы удостоверяющие личность")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("3d17ce3e-0019-4ef3-a9cf-ce1304271693")]
    [Bars.Rms.Core.Attributes.Uid("3d17ce3e-0019-4ef3-a9cf-ce1304271693")]
    public class DokumentyUdostoverjajushhieLichnost : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public DokumentyUdostoverjajushhieLichnost(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("5ce026a8-89e4-44ec-9669-1c928ba4b878")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("9375b11f-ca95-422e-ba8f-747109900d60")]
        public virtual System.String name1
        {
            get;
            set;
        }

        /// <summary>
        /// Примечание
        /// </summary>
        [Bars.B4.Utils.Display("Примечание")]
        [Bars.Rms.Core.Attributes.Uid("f8281e46-56ff-4768-95cd-9d7fdb24d397")]
        public virtual System.String description
        {
            get;
            set;
        }
    }
}