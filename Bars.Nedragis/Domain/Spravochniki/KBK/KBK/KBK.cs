namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// КБК
    /// Коды бюджетной классификации — 20-значные коды, используемые для учета доходов и расходов бюджетов всех уровней в Российской Федерации
    /// </summary>
    [Bars.B4.Utils.Display(@"КБК")]
    [Bars.B4.Utils.Description(@"Коды бюджетной классификации — 20-значные коды, используемые для учета доходов и расходов бюджетов всех уровней в Российской Федерации")]
    [System.Runtime.InteropServices.GuidAttribute("27c717a5-3146-4aff-8401-828c5f8734b5")]
    [Bars.Rms.Core.Attributes.Uid("27c717a5-3146-4aff-8401-828c5f8734b5")]
    public class KBK : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public KBK(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("41aa2184-aff2-49ec-ab87-6dc69fedc573")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("8124086d-2d5e-474e-9bbd-36fb89349d63")]
        public virtual System.String name1
        {
            get;
            set;
        }

        /// <summary>
        /// Дата начала действия
        /// </summary>
        [Bars.B4.Utils.Display("Дата начала действия")]
        [Bars.Rms.Core.Attributes.Uid("99f431a4-802c-462d-8795-ac07da6eeb5b")]
        public virtual System.DateTime date_from
        {
            get;
            set;
        }
    }
}