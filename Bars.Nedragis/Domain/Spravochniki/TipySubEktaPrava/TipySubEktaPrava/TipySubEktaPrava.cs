namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Типы субъекта права
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Типы субъекта права")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("f795e28f-f5a6-4099-a88d-c9e75f73f1a1")]
    [Bars.Rms.Core.Attributes.Uid("f795e28f-f5a6-4099-a88d-c9e75f73f1a1")]
    public class TipySubEktaPrava : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public TipySubEktaPrava(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("cd47999b-7443-4cce-a020-70e9c52e8068")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("e50661fe-ede5-42db-b264-fcf1b63a0011")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}