namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// ВРИ ЗУ
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"ВРИ ЗУ")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("5db62416-3df3-4972-a540-4abd50af9c5c")]
    [Bars.Rms.Core.Attributes.Uid("5db62416-3df3-4972-a540-4abd50af9c5c")]
    public class VRIZU : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public VRIZU(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("d96612dd-5047-4630-ab93-03c58f1eab71")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("868835cf-0e3b-47aa-be83-eb3e66045386")]
        public virtual System.String name1
        {
            get;
            set;
        }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент")]
        [Bars.Rms.Core.Attributes.Uid("e26161f2-dabf-43d5-878e-4c9d62051a71")]
        public virtual Bars.Nedragis.VRIZU parent
        {
            get;
            set;
        }

        /// <summary>
        /// Деятельность правообладателя
        /// </summary>
        [Bars.B4.Utils.Display("Деятельность правообладателя")]
        [Bars.Rms.Core.Attributes.Uid("5672c027-5051-4296-9cdd-8ac839de220e")]
        public virtual System.String description
        {
            get;
            set;
        }
    }
}