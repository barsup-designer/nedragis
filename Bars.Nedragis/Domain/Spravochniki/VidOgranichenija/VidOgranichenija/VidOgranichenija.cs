namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Вид ограничения
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Вид ограничения")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("819d41cc-49ef-46e6-8c2d-b426986e0144")]
    [Bars.Rms.Core.Attributes.Uid("819d41cc-49ef-46e6-8c2d-b426986e0144")]
    public class VidOgranichenija : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public VidOgranichenija(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("67cfc38b-90d4-40ab-8144-1201f1dcaa54")]
        public virtual System.String name1
        {
            get;
            set;
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("a98c533d-9824-443f-a9b9-a60baff91fcc")]
        public virtual System.String code
        {
            get;
            set;
        }
    }
}