namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Реестры
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Реестры")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("a8d3824d-faba-4511-b605-17138fee2b75")]
    [Bars.Rms.Core.Attributes.Uid("a8d3824d-faba-4511-b605-17138fee2b75")]
    public class Reestry : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Reestry(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("872b83c7-64fa-426a-9bb1-d412c41f46b9")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("6154cc31-efb1-43b7-bba7-5d64d82c3dbf")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}