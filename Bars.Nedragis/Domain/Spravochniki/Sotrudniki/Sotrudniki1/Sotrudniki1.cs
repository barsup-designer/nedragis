namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Сотрудники
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Сотрудники")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("3ae04932-5d4f-410a-9da5-985f7dfcb52b")]
    [Bars.Rms.Core.Attributes.Uid("3ae04932-5d4f-410a-9da5-985f7dfcb52b")]
    public class Sotrudniki1 : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Sotrudniki1(): base ()
        {
        }

        /// <summary>
        /// ФИО
        /// </summary>
        [Bars.B4.Utils.Display("ФИО")]
        [Bars.Rms.Core.Attributes.Uid("5c9e0c3e-7dcb-483d-89fd-a12dacaa7279")]
        public virtual System.String Element1474352136232
        {
            get;
            set;
        }
    }
}