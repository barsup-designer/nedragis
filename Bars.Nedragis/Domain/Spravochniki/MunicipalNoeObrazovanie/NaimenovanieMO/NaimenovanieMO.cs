namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Наименование МО
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Наименование МО")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("3195a3ba-40cc-4d49-b2d0-7a9d8153792b")]
    [Bars.Rms.Core.Attributes.Uid("3195a3ba-40cc-4d49-b2d0-7a9d8153792b")]
    public class NaimenovanieMO : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public NaimenovanieMO(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("1d0a20ca-f1bd-48b3-9747-eb292da7de90")]
        public virtual System.String Element1455707802552
        {
            get;
            set;
        }
    }
}