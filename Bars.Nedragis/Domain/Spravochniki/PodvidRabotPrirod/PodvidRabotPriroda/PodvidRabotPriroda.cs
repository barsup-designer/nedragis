namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Подвид работ природа
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Подвид работ природа")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("bbf0e8a9-5ab2-491d-899f-4f2e652309b1")]
    [Bars.Rms.Core.Attributes.Uid("bbf0e8a9-5ab2-491d-899f-4f2e652309b1")]
    public class PodvidRabotPriroda : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public PodvidRabotPriroda(): base ()
        {
        }

        /// <summary>
        /// Подвид работ 
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ ")]
        [Bars.Rms.Core.Attributes.Uid("618375fc-f98a-45b1-b759-96866251edc2")]
        public virtual System.String Podvid_rab_pr
        {
            get;
            set;
        }
    }
}