namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Районы ЯНАО
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Районы ЯНАО")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("d214293d-e00f-4a6c-a27e-795e72642b89")]
    [Bars.Rms.Core.Attributes.Uid("d214293d-e00f-4a6c-a27e-795e72642b89")]
    public class RajjonyJaNAO : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public RajjonyJaNAO(): base ()
        {
        }

        /// <summary>
        /// Наименование района
        /// </summary>
        [Bars.B4.Utils.Display("Наименование района")]
        [Bars.Rms.Core.Attributes.Uid("8808232e-7586-4c64-b5ce-c8e0d8fa30c2")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}