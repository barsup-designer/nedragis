namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Категории земель
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Категории земель")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("50693b32-18d4-45e9-81f8-9b94eaf76c13")]
    [Bars.Rms.Core.Attributes.Uid("50693b32-18d4-45e9-81f8-9b94eaf76c13")]
    public class KategoriiZemel : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public KategoriiZemel(): base ()
        {
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("f140929b-ea44-4abb-8156-a2c7341e81c6")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("1cda49c6-b9e1-4437-873b-7c67d9cbe02f")]
        public virtual System.String name1
        {
            get;
            set;
        }
    }
}