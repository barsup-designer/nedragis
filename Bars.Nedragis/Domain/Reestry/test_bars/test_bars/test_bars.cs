namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// test_bars
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"test_bars")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("7dd5ad97-a603-4dbf-acec-30db779b157d")]
    [Bars.Rms.Core.Attributes.Uid("7dd5ad97-a603-4dbf-acec-30db779b157d")]
    public class test_bars : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public test_bars(): base ()
        {
        }

        /// <summary>
        /// площадь
        /// </summary>
        [Bars.B4.Utils.Display("площадь")]
        [Bars.Rms.Core.Attributes.Uid("b30575d0-57a6-4476-92cd-0f6957fb60f2")]
        public virtual System.Decimal Element1481264972872
        {
            get;
            set;
        }

        /// <summary>
        /// Test
        /// </summary>
        [Bars.B4.Utils.Display("Test")]
        [Bars.Rms.Core.Attributes.Uid("039ca39a-60f6-4d3a-af26-60734e9d150b")]
        public virtual System.String Element1496214131857
        {
            get;
            set;
        }
    }
}