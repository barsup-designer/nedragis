namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// ДокументыЗУ
    /// Для организации связи ""многие ко многим"" между сущностями Земельные участки и Документы
    /// </summary>
    [Bars.B4.Utils.Display(@"ДокументыЗУ")]
    [Bars.B4.Utils.Description(@"Для организации связи ""многие ко многим"" между сущностями Земельные участки и Документы")]
    [System.Runtime.InteropServices.GuidAttribute("4996ce7a-3313-46fc-979a-fe45071d30e9")]
    [Bars.Rms.Core.Attributes.Uid("4996ce7a-3313-46fc-979a-fe45071d30e9")]
    public class DokumentyZU : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public DokumentyZU(): base ()
        {
        }

        /// <summary>
        /// Земельный участок
        /// </summary>
        [Bars.B4.Utils.Display("Земельный участок")]
        [Bars.Rms.Core.Attributes.Uid("bac8dff6-5e22-4e24-8a69-c82f1802f8e9")]
        public virtual Bars.Nedragis.ZemelNyemUchastki zemuch
        {
            get;
            set;
        }

        /// <summary>
        /// Документ
        /// </summary>
        [Bars.B4.Utils.Display("Документ")]
        [Bars.Rms.Core.Attributes.Uid("b0627770-34ec-4c22-8112-78c21088ef09")]
        public virtual Bars.Nedragis.Dokument doc1
        {
            get;
            set;
        }
    }
}