namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Субсидии
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Субсидии")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("59ae1a57-2649-408b-9f30-33fc49b5a7cb")]
    [Bars.Rms.Core.Attributes.Uid("59ae1a57-2649-408b-9f30-33fc49b5a7cb")]
    public class subsidies : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public subsidies(): base ()
        {
        }

        /// <summary>
        /// Наименование муниципального образования
        /// </summary>
        [Bars.B4.Utils.Display("Наименование муниципального образования")]
        [Bars.Rms.Core.Attributes.Uid("c0dbf801-1551-438f-9261-61a84604bacc")]
        public virtual Bars.Nedragis.NaimenovanieMO NAME_MO
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование направления расходования средств
        /// </summary>
        [Bars.B4.Utils.Display("Наименование направления расходования средств")]
        [Bars.Rms.Core.Attributes.Uid("c71979b3-9c05-40c6-a568-204f5b4b6d86")]
        public virtual Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv Element1455708491495
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование объекта, с группировкой по населенным пунктам
        /// </summary>
        [Bars.B4.Utils.Display("Наименование объекта, с группировкой по населенным пунктам")]
        [Bars.Rms.Core.Attributes.Uid("36a20294-4f66-4a0e-8408-8a4c74d668ae")]
        public virtual System.String Element1455709095130
        {
            get;
            set;
        }

        /// <summary>
        /// Реквизиты свидетельства о государственной регистрации
        /// </summary>
        [Bars.B4.Utils.Display("Реквизиты свидетельства о государственной регистрации")]
        [Bars.Rms.Core.Attributes.Uid("a49fea85-9564-453a-a5b9-928459474e82")]
        public virtual System.String Element1455709437278
        {
            get;
            set;
        }

        /// <summary>
        /// Производственный процесс СМР ПИР
        /// </summary>
        [Bars.B4.Utils.Display("Производственный процесс СМР ПИР")]
        [Bars.Rms.Core.Attributes.Uid("9b59331d-98df-4881-baf8-6843d6aff2ab")]
        public virtual System.String Element1455709476348
        {
            get;
            set;
        }

        /// <summary>
        /// Вид работ 
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ ")]
        [Bars.Rms.Core.Attributes.Uid("741f222e-bfed-411d-9a9c-d703f4d111ef")]
        public virtual System.String Element1456977571408
        {
            get;
            set;
        }

        /// <summary>
        /// Дата начала выполнения работ
        /// </summary>
        [Bars.B4.Utils.Display("Дата начала выполнения работ")]
        [Bars.Rms.Core.Attributes.Uid("d681eb14-0360-471a-8a4b-d213f1294a0c")]
        public virtual System.DateTime Element1456979244748
        {
            get;
            set;
        }

        /// <summary>
        /// Дата окончания выполнения работ
        /// </summary>
        [Bars.B4.Utils.Display("Дата окончания выполнения работ")]
        [Bars.Rms.Core.Attributes.Uid("c59564cb-9f0e-47d6-9e6f-68a59602cacd")]
        public virtual System.DateTime Element1456979287919
        {
            get;
            set;
        }

        /// <summary>
        /// Мощность(км)
        /// </summary>
        [Bars.B4.Utils.Display("Мощность(км)")]
        [Bars.Rms.Core.Attributes.Uid("f283d849-77fc-40a0-bbd4-89883f5bd27d")]
        public virtual System.Int32 Element1456982258068
        {
            get;
            set;
        }

        /// <summary>
        /// Мощность(м2)
        /// </summary>
        [Bars.B4.Utils.Display("Мощность(м2)")]
        [Bars.Rms.Core.Attributes.Uid("288a4975-84d6-401a-822a-87ad6afa22ad")]
        public virtual System.Int32 Element1456982634223
        {
            get;
            set;
        }

        /// <summary>
        /// Максимальная цена контракта(руб.)
        /// </summary>
        [Bars.B4.Utils.Display("Максимальная цена контракта(руб.)")]
        [Bars.Rms.Core.Attributes.Uid("51baa3c5-7068-41e5-bb62-4e3347fd4f48")]
        public virtual System.Decimal Element1456982674444
        {
            get;
            set;
        }

        /// <summary>
        /// км
        /// </summary>
        [Bars.B4.Utils.Display("км")]
        [Bars.Rms.Core.Attributes.Uid("55654c99-bad1-4e76-8ca3-2ed25065410e")]
        public virtual System.Int32 Element1456982760976
        {
            get;
            set;
        }

        /// <summary>
        /// м2
        /// </summary>
        [Bars.B4.Utils.Display("м2")]
        [Bars.Rms.Core.Attributes.Uid("26024bc8-b51b-406f-bc01-b5846ea67296")]
        public virtual System.Int32 Element1456982777199
        {
            get;
            set;
        }

        /// <summary>
        /// Количество объектов с положительным заключением
        /// </summary>
        [Bars.B4.Utils.Display("Количество объектов с положительным заключением")]
        [Bars.Rms.Core.Attributes.Uid("44a2592f-0dac-4147-96ba-f07ae8164731")]
        public virtual System.Int32 Element1456982822174
        {
            get;
            set;
        }

        /// <summary>
        /// Доля автомобильных дорог(%)
        /// </summary>
        [Bars.B4.Utils.Display("Доля автомобильных дорог(%)")]
        [Bars.Rms.Core.Attributes.Uid("a4928865-8e27-4b14-9cda-e8e0372955b8")]
        public virtual System.Int32 Element1456982887080
        {
            get;
            set;
        }

        /// <summary>
        /// Объем финансирования(Бюджет ЯНАО)
        /// </summary>
        [Bars.B4.Utils.Display("Объем финансирования(Бюджет ЯНАО)")]
        [Bars.Rms.Core.Attributes.Uid("136e1429-1444-4f96-a44c-8f40f41f6a95")]
        public virtual System.Decimal Element1456982966340
        {
            get;
            set;
        }

        /// <summary>
        /// Объем финансирования(Бюджет МО)
        /// </summary>
        [Bars.B4.Utils.Display("Объем финансирования(Бюджет МО)")]
        [Bars.Rms.Core.Attributes.Uid("2e78adb3-638d-49f8-810a-ec55b117dfd0")]
        public virtual System.Decimal Element1456983021798
        {
            get;
            set;
        }

        /// <summary>
        /// Процент долевого участия бюджета МО
        /// </summary>
        [Bars.B4.Utils.Display("Процент долевого участия бюджета МО")]
        [Bars.Rms.Core.Attributes.Uid("e18a8c53-a865-4138-8180-27fc3a54682a")]
        public virtual System.Int32 Element1456983065031
        {
            get;
            set;
        }
    }
}