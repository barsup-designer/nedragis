namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Файлы
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Файлы")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("a1132509-a77c-43bc-b29b-43438c19283b")]
    [Bars.Rms.Core.Attributes.Uid("a1132509-a77c-43bc-b29b-43438c19283b")]
    public class Fajjly : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Fajjly(): base ()
        {
        }

        /// <summary>
        /// файл
        /// </summary>
        [Bars.B4.Utils.Display("файл")]
        [Bars.Rms.Core.Attributes.Uid("2fe4a2b3-e48e-4d8c-acfa-a00c80cdf3cb")]
        public virtual System.String Element1457586389312
        {
            get;
            set;
        }

        /// <summary>
        /// Display 1457587503567
        /// </summary>
        [Bars.B4.Utils.Display("Display 1457587503567")]
        [Bars.Rms.Core.Attributes.Uid("01874ac4-2af1-4715-9196-5a1b5c131eac")]
        public virtual System.Int32 Element1457587503567
        {
            get;
            set;
        }
    }
}