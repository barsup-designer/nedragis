namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Приема и контроля исполнения заявок
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Приема и контроля исполнения заявок")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("570c3381-31ef-424d-b058-c74c0a28ed56")]
    [Bars.Rms.Core.Attributes.Uid("570c3381-31ef-424d-b058-c74c0a28ed56")]
    public class IspolnenijaZajavok : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public IspolnenijaZajavok(): base ()
        {
        }

        /// <summary>
        /// Консультация по телефону
        /// </summary>
        [Bars.B4.Utils.Display("Консультация по телефону")]
        [Bars.Rms.Core.Attributes.Uid("6e686cd5-c68f-42a8-90f6-197469350b6b")]
        public virtual System.Boolean Element1511506083922
        {
            get;
            set;
        }

        /// <summary>
        /// Инцидент(заявка)
        /// </summary>
        [Bars.B4.Utils.Display("Инцидент(заявка)")]
        [Bars.Rms.Core.Attributes.Uid("d8fcee08-5459-4aa7-9737-15d0bff22e0b")]
        public virtual System.Boolean Element1511506316909
        {
            get;
            set;
        }

        /// <summary>
        /// Муниципальное образование
        /// </summary>
        [Bars.B4.Utils.Display("Муниципальное образование")]
        [Bars.Rms.Core.Attributes.Uid("ea16a9b0-4dc3-40d0-b38a-0c11f8268752")]
        public virtual Bars.Nedragis.NaimenovanieMO Element1511506563562
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование организации
        /// </summary>
        [Bars.B4.Utils.Display("Наименование организации")]
        [Bars.Rms.Core.Attributes.Uid("7a7eedb4-b777-4d30-9c1a-2c5d6b80a7c5")]
        public virtual Bars.Nedragis.Organizacii Element1511506733063
        {
            get;
            set;
        }

        /// <summary>
        /// проект
        /// </summary>
        [Bars.B4.Utils.Display("проект")]
        [Bars.Rms.Core.Attributes.Uid("6876e46e-7bbe-409b-aea5-822d65f84a1e")]
        public virtual Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv Element1511762285082
        {
            get;
            set;
        }

        /// <summary>
        /// Дата заявки
        /// </summary>
        [Bars.B4.Utils.Display("Дата заявки")]
        [Bars.Rms.Core.Attributes.Uid("8f67ea0f-c7de-4e15-9952-0df51655c8b7")]
        public virtual System.DateTime Element1511781998051
        {
            get;
            set;
        }
    }
}