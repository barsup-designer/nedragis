namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Мониторинг
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Мониторинг")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("293381ae-d0ca-4bbf-8b8c-0e2c31bd13c4")]
    [Bars.Rms.Core.Attributes.Uid("293381ae-d0ca-4bbf-8b8c-0e2c31bd13c4")]
    public class monitor : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public monitor(): base ()
        {
        }

        /// <summary>
        /// Каротаж - сканобраз (количество обращений)
        /// </summary>
        [Bars.B4.Utils.Display("Каротаж - сканобраз (количество обращений)")]
        [Bars.Rms.Core.Attributes.Uid("33e9f2f5-7430-4489-9b4d-67ad9f924495")]
        public virtual System.Int32 Karotazs_o
        {
            get;
            set;
        }

        /// <summary>
        /// Каротаж-сканобраз (денежные средства)
        /// </summary>
        [Bars.B4.Utils.Display("Каротаж-сканобраз (денежные средства)")]
        [Bars.Rms.Core.Attributes.Uid("fc30ec7e-6461-4916-b117-eb8dc2af2530")]
        public virtual System.Decimal Karotazs_d
        {
            get;
            set;
        }

        /// <summary>
        /// Дела скважин (д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Дела скважин (д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("6e112d82-731f-4804-bb56-fcb30025cd4d")]
        public virtual System.Decimal Dela_od
        {
            get;
            set;
        }

        /// <summary>
        /// Дело скважин(кол.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Дело скважин(кол.об.)")]
        [Bars.Rms.Core.Attributes.Uid("47336c55-a9b8-4011-902f-4dedeea842d6")]
        public virtual System.Int32 Dela_o
        {
            get;
            set;
        }

        /// <summary>
        /// Каротаж (оцифрованный формат LAS)_к.об.
        /// </summary>
        [Bars.B4.Utils.Display("Каротаж (оцифрованный формат LAS)_к.об.")]
        [Bars.Rms.Core.Attributes.Uid("75fcf2c4-0f33-46d9-a8d7-c7cf6d40f3e0")]
        public virtual System.Int32 Karotazc
        {
            get;
            set;
        }

        /// <summary>
        /// Каротаж (оцифрованный формат LAS)_д.ср.
        /// </summary>
        [Bars.B4.Utils.Display("Каротаж (оцифрованный формат LAS)_д.ср.")]
        [Bars.Rms.Core.Attributes.Uid("888bf9a3-8028-4c5f-86f2-278f85fd39eb")]
        public virtual System.Decimal Karotazc_d
        {
            get;
            set;
        }

        /// <summary>
        /// Геологические отчеты(количество обращений)
        /// </summary>
        [Bars.B4.Utils.Display("Геологические отчеты(количество обращений)")]
        [Bars.Rms.Core.Attributes.Uid("26cf2a9b-8d34-4b34-baeb-25160c3a77f0")]
        public virtual System.Int32 geolog_otchet
        {
            get;
            set;
        }

        /// <summary>
        /// Геологические отчеты(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Геологические отчеты(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("b765b25f-0f85-4d40-80ff-c4ab3d95fd3b")]
        public virtual System.Decimal geolog_otchet_d
        {
            get;
            set;
        }

        /// <summary>
        /// Сейсмика: исходный полевой материал (к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмика: исходный полевой материал (к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("bced0110-594a-4222-95b9-83055a3f08bf")]
        public virtual System.Int32 Element1462359767016
        {
            get;
            set;
        }

        /// <summary>
        /// Сейсмика: исходный полевой материал (д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмика: исходный полевой материал (д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("90f07a2f-2a5f-4ce0-b57f-e52d986ec753")]
        public virtual System.Decimal Element1462359769527
        {
            get;
            set;
        }

        /// <summary>
        /// Обзорная карта ЯНАО(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта ЯНАО(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("70f99e38-b115-4b3b-8a13-fe1c97a870bf")]
        public virtual System.Int32 Element1462359853756
        {
            get;
            set;
        }

        /// <summary>
        /// Обзорная карта ЯНАО(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта ЯНАО(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("c371f09d-50c6-463e-a49d-380522af28db")]
        public virtual System.Decimal Element1462359855708
        {
            get;
            set;
        }

        /// <summary>
        /// Оценка вреда и исчисление размера ущерба(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Оценка вреда и исчисление размера ущерба(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("346e67b5-e04e-4477-934a-0898500d6352")]
        public virtual System.Int32 ocenka_vreda
        {
            get;
            set;
        }

        /// <summary>
        /// Оценка вреда и исчисление размера ущерба(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Оценка вреда и исчисление размера ущерба(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("7ef13911-a289-4b2e-bf86-2a29043ed829")]
        public virtual System.Decimal ocenka_vreda_d
        {
            get;
            set;
        }

        /// <summary>
        /// Расчет убытков землепользователей(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Расчет убытков землепользователей(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("68cd5b14-deb4-4a8c-9e07-9b6d34117708")]
        public virtual System.Decimal Element1462360050013
        {
            get;
            set;
        }

        /// <summary>
        /// Расчет убытков землепользователей(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Расчет убытков землепользователей(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("a39eab40-5775-4f96-9477-1d284b643f93")]
        public virtual System.Int32 raschet_ubytkov
        {
            get;
            set;
        }

        /// <summary>
        /// Сейсмика_кубы(кол.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмика_кубы(кол.об.)")]
        [Bars.Rms.Core.Attributes.Uid("f8463465-d0dc-48e2-8455-17b5a6197284")]
        public virtual System.Int32 kub
        {
            get;
            set;
        }

        /// <summary>
        /// Сейсмика_кубы(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмика_кубы(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("9063840a-b3f9-4d90-867f-bd2e59d5e927")]
        public virtual System.Decimal kub_d
        {
            get;
            set;
        }

        /// <summary>
        /// Сейсмические отчеты(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмические отчеты(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("ee100898-c97f-423f-9901-d15541ae8917")]
        public virtual System.Int32 seismika_otchet
        {
            get;
            set;
        }

        /// <summary>
        /// Сейсмические отчеты(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмические отчеты(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("1098b77e-04db-422b-acfc-dc31445c90a0")]
        public virtual System.Decimal seismika_otchet_d
        {
            get;
            set;
        }

        /// <summary>
        /// Карты_н(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Карты_н(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("4b26a829-dd61-4734-9994-a07d41785080")]
        public virtual System.Decimal karta_d
        {
            get;
            set;
        }

        /// <summary>
        /// Карты_н(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Карты_н(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("6eb2e335-9e22-4922-97d9-0843ad6f5514")]
        public virtual System.Int32 karta820
        {
            get;
            set;
        }

        /// <summary>
        /// Планы подсчетов запасов(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Планы подсчетов запасов(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("0f021e80-c14f-460a-ab20-d94d0761dd69")]
        public virtual System.Int32 zapas
        {
            get;
            set;
        }

        /// <summary>
        /// Планы подсчетов запасов(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Планы подсчетов запасов(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("f90b0081-f1ab-4ede-9f31-3a81db0f5510")]
        public virtual System.Decimal zapas_d
        {
            get;
            set;
        }

        /// <summary>
        /// Обзорная карта района ЯНАО(к.об.))
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта района ЯНАО(к.об.))")]
        [Bars.Rms.Core.Attributes.Uid("11442065-4910-4543-aebb-b5973a130bc9")]
        public virtual System.Int32 karta_raion
        {
            get;
            set;
        }

        /// <summary>
        /// Обзорная карта района ЯНАО(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта района ЯНАО(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("2ef6ab5f-c956-4d22-ba77-90fc8370bedc")]
        public virtual System.Decimal karta_raion_d
        {
            get;
            set;
        }

        /// <summary>
        /// Обзорная карта недропользования ЯНАО(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта недропользования ЯНАО(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("9114bf52-0d09-4b4b-a6d5-71e77ece161d")]
        public virtual System.Int32 karta_nedra
        {
            get;
            set;
        }

        /// <summary>
        /// Обзорная карта недропользования ЯНАО(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта недропользования ЯНАО(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("d091b4e5-d984-4632-93f3-695e272bd964")]
        public virtual System.Decimal karta_nedra_d
        {
            get;
            set;
        }

        /// <summary>
        /// Карта населенного пункта ЯНАО(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Карта населенного пункта ЯНАО(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("1f223ad1-d1da-4a2e-94e2-20153327c6c5")]
        public virtual System.Int32 karta_n_p9622
        {
            get;
            set;
        }

        /// <summary>
        /// Карта населенного пункта ЯНАО(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Карта населенного пункта ЯНАО(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("1ee48615-4748-4782-9c61-b3453771f92a")]
        public virtual System.Decimal karta_n_p6180
        {
            get;
            set;
        }

        /// <summary>
        /// Схемы различного формата(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Схемы различного формата(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("ba6fbd8d-8418-401a-9d7a-e1fa24bdaa9c")]
        public virtual System.Decimal shema_d
        {
            get;
            set;
        }

        /// <summary>
        /// Схемы различного формата(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Схемы различного формата(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("050aecf6-8d6f-41e1-ae0e-1d33e6fc37be")]
        public virtual System.Int32 shema
        {
            get;
            set;
        }

        /// <summary>
        /// Печать графического материала(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Печать графического материала(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("ff809940-7a0a-4d84-a85d-016e64c2c666")]
        public virtual System.Int32 pecat_g_m
        {
            get;
            set;
        }

        /// <summary>
        /// Печать графического материала(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Печать графического материала(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("fc673198-5e9c-4792-86a6-2957ea205acf")]
        public virtual System.Decimal pecat_g_m_d
        {
            get;
            set;
        }

        /// <summary>
        /// Пространственный анализ размещения проектируемых участков(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Пространственный анализ размещения проектируемых участков(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("0024c28e-7be7-4609-aa04-9917ea36eb66")]
        public virtual System.Int32 analiz
        {
            get;
            set;
        }

        /// <summary>
        /// Пространственный анализ размещения проектируемых участков(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Пространственный анализ размещения проектируемых участков(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("342e3eef-bed1-4295-af7c-b1766072070a")]
        public virtual System.Decimal analiz_d
        {
            get;
            set;
        }

        /// <summary>
        /// Выполнение обзорных карт-схем(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Выполнение обзорных карт-схем(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("2d551038-e809-48f8-9937-49d57a22ea41")]
        public virtual System.Int32 karta_chema
        {
            get;
            set;
        }

        /// <summary>
        /// Выполнение обзорных карт-схем(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Выполнение обзорных карт-схем(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("ce2c477a-0e9a-488f-bde6-6a8bf3a7fbea")]
        public virtual System.Decimal karta_shema_d
        {
            get;
            set;
        }

        /// <summary>
        /// Пространственный анализ(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Пространственный анализ(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("59e1fc47-13ac-41e8-8d1e-875a0fec764b")]
        public virtual System.Int32 pr_analiz
        {
            get;
            set;
        }

        /// <summary>
        /// Пространственный анализ(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Пространственный анализ(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("1446e0a7-9e17-4790-a62c-93a78605be45")]
        public virtual System.Decimal pr_analiz_d
        {
            get;
            set;
        }

        /// <summary>
        /// Выполнение кадастровых работ(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Выполнение кадастровых работ(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("97dec106-60c4-4a17-88c1-8f723af4ed4f")]
        public virtual System.Int32 kadastr
        {
            get;
            set;
        }

        /// <summary>
        /// Выполнение кадастровых работ(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Выполнение кадастровых работ(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("78ca34a4-fab0-4afc-9805-9f4e4c038c7c")]
        public virtual System.Decimal kadastr_d
        {
            get;
            set;
        }

        /// <summary>
        /// Сейсмические отчеты без графического материала(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмические отчеты без графического материала(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("fbc2c1a2-70e4-4a34-8556-f6bea2922c7b")]
        public virtual System.Int32 seismika
        {
            get;
            set;
        }

        /// <summary>
        /// Сейсмические отчеты без графического материала(д.ср.)
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмические отчеты без графического материала(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("ac00839b-8143-4403-82c4-5082839dbd32")]
        public virtual System.Decimal seismika_d
        {
            get;
            set;
        }

        /// <summary>
        /// Координаты скважины и альтитуды(к.об.)
        /// </summary>
        [Bars.B4.Utils.Display("Координаты скважины и альтитуды(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("a85f8ba6-85f0-493e-a13a-97529dcb10a9")]
        public virtual System.Int32 koor
        {
            get;
            set;
        }

        /// <summary>
        /// Координаты скважины и альтитуды(д.ж.)
        /// </summary>
        [Bars.B4.Utils.Display("Координаты скважины и альтитуды(д.ж.)")]
        [Bars.Rms.Core.Attributes.Uid("22535101-7a4d-4b41-a114-c18608aecb3c")]
        public virtual System.Decimal koor_d
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("398295bc-f5eb-4ed2-b4de-d5f97e28d9da")]
        public virtual Bars.Nedragis.Otdel1 otdel
        {
            get;
            set;
        }

        /// <summary>
        /// Проверка координат и подготовка схем_к.о.
        /// </summary>
        [Bars.B4.Utils.Display("Проверка координат и подготовка схем_к.о.")]
        [Bars.Rms.Core.Attributes.Uid("2daa03a5-a3e4-4cb2-882e-0577ddee3832")]
        public virtual System.Int32 priroda_rpoverka_coor
        {
            get;
            set;
        }

        /// <summary>
        /// Проверка координат и подготовка схем_д.с.
        /// </summary>
        [Bars.B4.Utils.Display("Проверка координат и подготовка схем_д.с.")]
        [Bars.Rms.Core.Attributes.Uid("885ca8e1-6429-40b4-86b4-f0df223647e7")]
        public virtual System.Decimal priroda_rpoverka_coor_d
        {
            get;
            set;
        }
    }
}