namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Природопользование_таб
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Природопользование_таб")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("c7d6a096-fd4f-4705-81ef-4c534005dd1a")]
    [Bars.Rms.Core.Attributes.Uid("c7d6a096-fd4f-4705-81ef-4c534005dd1a")]
    public class PrirodopolZovanieTab : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public PrirodopolZovanieTab(): base ()
        {
        }
    }
}