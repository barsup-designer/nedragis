namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Отдел подготовки информации
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Отдел подготовки информации")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("ef5acf69-1403-413d-ac6e-11caae67cfa8")]
    [Bars.Rms.Core.Attributes.Uid("ef5acf69-1403-413d-ac6e-11caae67cfa8")]
    public class OtdelPodgotovkiInformacii : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public OtdelPodgotovkiInformacii(): base ()
        {
        }

        /// <summary>
        /// Номер п/п
        /// </summary>
        [Bars.B4.Utils.Display("Номер п/п")]
        [Bars.Rms.Core.Attributes.Uid("7e95bdea-1ebc-4988-a9a2-4e73dce84535")]
        public virtual System.Int32 Element1510134896283
        {
            get;
            set;
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("5979a624-309a-4045-95ca-bb02a2831f8a")]
        public virtual Bars.Nedragis.VidRabotOPINF Element1510135000178
        {
            get;
            set;
        }

        /// <summary>
        /// Подвид работ
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("2bca0ac8-fc0b-4d1a-84ee-ba86f65548fa")]
        public virtual Bars.Nedragis.VidRabotOPINF Element1510135042374
        {
            get;
            set;
        }

        /// <summary>
        /// Источник поступления информации
        /// </summary>
        [Bars.B4.Utils.Display("Источник поступления информации")]
        [Bars.Rms.Core.Attributes.Uid("8e1f35d8-fbac-435e-8c66-54ccf74a8dc8")]
        public virtual System.String Element1510135074618
        {
            get;
            set;
        }

        /// <summary>
        /// Исполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("4376a9f6-b33d-46d0-bf82-5765836fd5ca")]
        public virtual Bars.Nedragis.Sotrudniki1 Element1510135121705
        {
            get;
            set;
        }

        /// <summary>
        /// Колчество заявок
        /// </summary>
        [Bars.B4.Utils.Display("Колчество заявок")]
        [Bars.Rms.Core.Attributes.Uid("3d267e74-2a70-4a0a-879d-1d6570650bbc")]
        public virtual System.Int32 Element1510135249843
        {
            get;
            set;
        }

        /// <summary>
        /// Количество листов
        /// </summary>
        [Bars.B4.Utils.Display("Количество листов")]
        [Bars.Rms.Core.Attributes.Uid("b24f2f60-8a4d-44f7-9225-1b37b03d1613")]
        public virtual System.Int32 Element1510135325790
        {
            get;
            set;
        }

        /// <summary>
        /// Номер документа
        /// </summary>
        [Bars.B4.Utils.Display("Номер документа")]
        [Bars.Rms.Core.Attributes.Uid("b6921b85-f07a-4d41-a252-b0ae59362473")]
        public virtual System.String Element1510135360701
        {
            get;
            set;
        }

        /// <summary>
        /// Количество папок
        /// </summary>
        [Bars.B4.Utils.Display("Количество папок")]
        [Bars.Rms.Core.Attributes.Uid("53a2c08c-4679-4a4c-aa8d-772f8f4b8caf")]
        public virtual System.Int32 Element1510135484012
        {
            get;
            set;
        }

        /// <summary>
        /// Количество файлов
        /// </summary>
        [Bars.B4.Utils.Display("Количество файлов")]
        [Bars.Rms.Core.Attributes.Uid("ad4418a4-8c8e-4fa0-b765-98bdbc8e37c8")]
        public virtual System.Int32 Element1510135527140
        {
            get;
            set;
        }
    }
}