namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Землеустройство
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Землеустройство")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("f43e2a1e-85d1-44ff-8cb6-87595813d7ad")]
    [Bars.Rms.Core.Attributes.Uid("f43e2a1e-85d1-44ff-8cb6-87595813d7ad")]
    public class Zemleustr : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Zemleustr(): base ()
        {
        }

        /// <summary>
        /// Номер дела/проекта
        /// </summary>
        [Bars.B4.Utils.Display("Номер дела/проекта")]
        [Bars.Rms.Core.Attributes.Uid("faa81b6d-44b1-4aa9-85e7-c855125d9094")]
        public virtual System.String NDelo
        {
            get;
            set;
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("c378de4a-8343-4bc9-be6e-fa791eca7e8e")]
        public virtual System.String vid_rabot
        {
            get;
            set;
        }

        /// <summary>
        /// Кому
        /// </summary>
        [Bars.B4.Utils.Display("Кому")]
        [Bars.Rms.Core.Attributes.Uid("75df8dc4-d003-4f45-b9e7-1ee24f23357c")]
        public virtual System.String komu
        {
            get;
            set;
        }

        /// <summary>
        /// Компания
        /// </summary>
        [Bars.B4.Utils.Display("Компания")]
        [Bars.Rms.Core.Attributes.Uid("6b25f7c0-c1eb-4c77-abe0-8f98b8a78c42")]
        public virtual System.String company
        {
            get;
            set;
        }

        /// <summary>
        /// Название объекта
        /// </summary>
        [Bars.B4.Utils.Display("Название объекта")]
        [Bars.Rms.Core.Attributes.Uid("260f0832-e300-4378-8428-c84e39ac3ce5")]
        public virtual System.String name_obj
        {
            get;
            set;
        }

        /// <summary>
        /// Кадастровый номер
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый номер")]
        [Bars.Rms.Core.Attributes.Uid("7cd74c40-7379-49e9-86fe-74230954e494")]
        public virtual System.String kad_nom
        {
            get;
            set;
        }

        /// <summary>
        /// Исходная категория земель
        /// </summary>
        [Bars.B4.Utils.Display("Исходная категория земель")]
        [Bars.Rms.Core.Attributes.Uid("ae229ccf-f963-4bae-80c6-d1770c88814e")]
        public virtual System.String kateg_zem
        {
            get;
            set;
        }

        /// <summary>
        /// Конечная категория земель
        /// </summary>
        [Bars.B4.Utils.Display("Конечная категория земель")]
        [Bars.Rms.Core.Attributes.Uid("5b06f3cc-ddf8-427c-aaab-10e3ed9911be")]
        public virtual System.String konech_kategor
        {
            get;
            set;
        }

        /// <summary>
        /// Площадь
        /// </summary>
        [Bars.B4.Utils.Display("Площадь")]
        [Bars.Rms.Core.Attributes.Uid("44abd96b-8d29-4d0a-9fc2-74fef0ae2f5b")]
        public virtual System.Double square
        {
            get;
            set;
        }

        /// <summary>
        /// Район
        /// </summary>
        [Bars.B4.Utils.Display("Район")]
        [Bars.Rms.Core.Attributes.Uid("0fd97ee2-7dff-4b7e-8f14-ae335f2ee16f")]
        public virtual Bars.Nedragis.RajjonyJaNAO region
        {
            get;
            set;
        }

        /// <summary>
        /// Дата поступления в ГКУ "Ресурсы Ямала"
        /// </summary>
        [Bars.B4.Utils.Display("Дата поступления в ГКУ \"Ресурсы Ямала\"")]
        [Bars.Rms.Core.Attributes.Uid("b8d2eb9c-e90e-4f6b-adc1-34cb4dacb0cc")]
        public virtual System.DateTime date_postupl
        {
            get;
            set;
        }

        /// <summary>
        /// Этап
        /// </summary>
        [Bars.B4.Utils.Display("Этап")]
        [Bars.Rms.Core.Attributes.Uid("36988660-bfcb-4925-9395-92cc696d745a")]
        public virtual System.String Etap
        {
            get;
            set;
        }

        /// <summary>
        /// Дата входящего документа
        /// </summary>
        [Bars.B4.Utils.Display("Дата входящего документа")]
        [Bars.Rms.Core.Attributes.Uid("3a98587e-b435-4545-a235-379ea3682772")]
        public virtual System.DateTime date_vh_doc
        {
            get;
            set;
        }

        /// <summary>
        /// Номер входящего документа
        /// </summary>
        [Bars.B4.Utils.Display("Номер входящего документа")]
        [Bars.Rms.Core.Attributes.Uid("651adb48-4570-4819-bd23-290913a02d1d")]
        public virtual System.String nom_vh_doc
        {
            get;
            set;
        }

        /// <summary>
        /// Автор(исполнитель)
        /// </summary>
        [Bars.B4.Utils.Display("Автор(исполнитель)")]
        [Bars.Rms.Core.Attributes.Uid("4d8b55f7-d0da-46b3-aabc-6ef35c082650")]
        public virtual Bars.Nedragis.Sotrudniki1 autor
        {
            get;
            set;
        }

        /// <summary>
        /// Формат данных
        /// </summary>
        [Bars.B4.Utils.Display("Формат данных")]
        [Bars.Rms.Core.Attributes.Uid("4dcb9882-85e7-483a-b9b5-cb4963db8138")]
        public virtual System.String format
        {
            get;
            set;
        }

        /// <summary>
        /// Количество единиц
        /// </summary>
        [Bars.B4.Utils.Display("Количество единиц")]
        [Bars.Rms.Core.Attributes.Uid("e38204d2-f9c5-4003-8c4a-af275a04388b")]
        public virtual System.Int32 kolvo_ed
        {
            get;
            set;
        }

        /// <summary>
        /// Примечание
        /// </summary>
        [Bars.B4.Utils.Display("Примечание")]
        [Bars.Rms.Core.Attributes.Uid("41bb7d7b-97ee-4fcb-8b76-0a406c35644c")]
        public virtual System.String prim
        {
            get;
            set;
        }

        /// <summary>
        /// Сумма калькуляции
        /// </summary>
        [Bars.B4.Utils.Display("Сумма калькуляции")]
        [Bars.Rms.Core.Attributes.Uid("eda1f75f-1955-4812-add0-edd6f7653c9a")]
        public virtual System.Double sum_kalk
        {
            get;
            set;
        }

        /// <summary>
        /// Номер письма заявителя
        /// </summary>
        [Bars.B4.Utils.Display("Номер письма заявителя")]
        [Bars.Rms.Core.Attributes.Uid("ea1a36fe-5c60-4beb-b944-fe17156ffb31")]
        public virtual System.Int32 nomer_pisma
        {
            get;
            set;
        }

        /// <summary>
        /// Дата исполнения
        /// </summary>
        [Bars.B4.Utils.Display("Дата исполнения")]
        [Bars.Rms.Core.Attributes.Uid("8c776cd0-27a8-4d66-adcb-852c69c869bf")]
        public virtual System.DateTime data_usp
        {
            get;
            set;
        }

        /// <summary>
        /// Исполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("09a0f52c-82e4-4d7a-8427-8a654fea8d1e")]
        public virtual System.String ispol
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("f0c7fc6d-bb89-4362-ba18-51ab9d589ccf")]
        public virtual Bars.Nedragis.Otdel1 name_z
        {
            get;
            set;
        }
    }
}