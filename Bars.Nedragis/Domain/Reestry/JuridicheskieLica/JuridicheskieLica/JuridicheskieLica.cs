namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Юридические лица
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Юридические лица")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f")]
    [Bars.Rms.Core.Attributes.Uid("252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f")]
    public class JuridicheskieLica : Bars.Nedragis.SubEktyPrava
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public JuridicheskieLica(): base ()
        {
        }

        /// <summary>
        /// ОКАТО
        /// </summary>
        [Bars.B4.Utils.Display("ОКАТО")]
        [Bars.Rms.Core.Attributes.Uid("f0b4bfc8-c239-4416-a46e-40b9d6783b78")]
        public virtual Bars.Nedragis.OKATO OKATO
        {
            get;
            set;
        }

        /// <summary>
        /// Полное наименование
        /// </summary>
        [Bars.B4.Utils.Display("Полное наименование")]
        [Bars.Rms.Core.Attributes.Uid("f4bb14d7-eefc-49fc-b7d9-a28420f04bea")]
        public virtual System.String Fullname
        {
            get;
            set;
        }

        /// <summary>
        /// ОКОПФ
        /// </summary>
        [Bars.B4.Utils.Display("ОКОПФ")]
        [Bars.Rms.Core.Attributes.Uid("77bb017b-d50d-4096-b963-16fcee043152")]
        public virtual Bars.Nedragis.OKOPF Okopf
        {
            get;
            set;
        }

        /// <summary>
        /// ОКПО
        /// </summary>
        [Bars.B4.Utils.Display("ОКПО")]
        [Bars.Rms.Core.Attributes.Uid("1e69934d-e6ce-4c2a-a422-c938a687b101")]
        public virtual System.String Okpo
        {
            get;
            set;
        }

        /// <summary>
        /// ОКВЭД
        /// </summary>
        [Bars.B4.Utils.Display("ОКВЭД")]
        [Bars.Rms.Core.Attributes.Uid("8e6a7f52-b6b5-4c10-850e-7a19a54f6c31")]
        public virtual Bars.Nedragis.OKVEhD Okved
        {
            get;
            set;
        }

        /// <summary>
        /// Телефон
        /// </summary>
        [Bars.B4.Utils.Display("Телефон")]
        [Bars.Rms.Core.Attributes.Uid("31472d04-e007-4c28-80ef-35f2781f7774")]
        public virtual System.String Phone
        {
            get;
            set;
        }

        /// <summary>
        /// Факс
        /// </summary>
        [Bars.B4.Utils.Display("Факс")]
        [Bars.Rms.Core.Attributes.Uid("91912f52-f7e2-4daf-a6f9-4c6548a16f9d")]
        public virtual System.String Fax
        {
            get;
            set;
        }

        /// <summary>
        /// Вышестоящая организация
        /// </summary>
        [Bars.B4.Utils.Display("Вышестоящая организация")]
        [Bars.Rms.Core.Attributes.Uid("68c3616c-fd73-4524-98e1-f49fb0973ce7")]
        public virtual Bars.Nedragis.JuridicheskieLica HeadSubject
        {
            get;
            set;
        }

        /// <summary>
        /// Получатель платежа
        /// </summary>
        [Bars.B4.Utils.Display("Получатель платежа")]
        [Bars.Rms.Core.Attributes.Uid("3b5e504c-dc88-4de9-abdf-c58899a7106d")]
        public virtual Bars.Nedragis.JuridicheskieLica PayeeSubject
        {
            get;
            set;
        }

        /// <summary>
        /// Email
        /// </summary>
        [Bars.B4.Utils.Display("Email")]
        [Bars.Rms.Core.Attributes.Uid("6d45d9fc-cfe2-4407-b44a-9873b0abdfbb")]
        public virtual System.String Email
        {
            get;
            set;
        }

        /// <summary>
        /// ОКФС
        /// </summary>
        [Bars.B4.Utils.Display("ОКФС")]
        [Bars.Rms.Core.Attributes.Uid("9b22ab49-e4f4-4300-9cd9-36a0642bdb31")]
        public virtual Bars.Nedragis.OKFS Okfs
        {
            get;
            set;
        }

        /// <summary>
        /// Дата гос. регистрации
        /// </summary>
        [Bars.B4.Utils.Display("Дата гос. регистрации")]
        [Bars.Rms.Core.Attributes.Uid("894edac0-66d8-479b-89d0-e7c1f498bdc7")]
        public virtual System.DateTime DateReg
        {
            get;
            set;
        }

        /// <summary>
        /// Размер уставного фонда организации
        /// </summary>
        [Bars.B4.Utils.Display("Размер уставного фонда организации")]
        [Bars.Rms.Core.Attributes.Uid("9ffc7a90-3074-4f35-88b9-3a77741ceb41")]
        public virtual System.Decimal CapitalStock
        {
            get;
            set;
        }

        /// <summary>
        /// Среднесписочный состав
        /// </summary>
        [Bars.B4.Utils.Display("Среднесписочный состав")]
        [Bars.Rms.Core.Attributes.Uid("b96aa57d-068f-4186-a357-828a1664f300")]
        public virtual System.Int32 CapitalPeople
        {
            get;
            set;
        }

        /// <summary>
        /// Отрасль
        /// </summary>
        [Bars.B4.Utils.Display("Отрасль")]
        [Bars.Rms.Core.Attributes.Uid("298b6558-2c50-423b-b1d1-ae3f9691d879")]
        public virtual Bars.Nedragis.Otrasl Sphere
        {
            get;
            set;
        }

        /// <summary>
        /// ОКОГУ
        /// </summary>
        [Bars.B4.Utils.Display("ОКОГУ")]
        [Bars.Rms.Core.Attributes.Uid("15f7fa6d-138c-439f-9138-10dc0335c912")]
        public virtual Bars.Nedragis.OKOGU Okogu
        {
            get;
            set;
        }

        private System.Collections.Generic.IList<Bars.Nedragis.Rukovoditeli> _bosses;
        /// <summary>
        /// Руководители
        /// </summary>
        [Bars.B4.Utils.Display("Руководители")]
        [Bars.Rms.Core.Attributes.Uid("636f2441-f965-4e47-9749-ccb79aba712e")]
        public virtual System.Collections.Generic.IList<Bars.Nedragis.Rukovoditeli> bosses
        {
            get
            {
                return _bosses ?? (_bosses = new System.Collections.Generic.List<Bars.Nedragis.Rukovoditeli>());
            }

            set
            {
                _bosses = value;
            }
        }

        /// <summary>
        /// Юридический адрес
        /// </summary>
        [Bars.B4.Utils.Display("Юридический адрес")]
        [Bars.Rms.Core.Attributes.Uid("2cc25b53-5924-4f99-800e-f2d2b10086d5")]
        public virtual Bars.B4.Modules.FIAS.FiasAddress LegalAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Фактический адрес
        /// </summary>
        [Bars.B4.Utils.Display("Фактический адрес")]
        [Bars.Rms.Core.Attributes.Uid("a932425e-fa61-4da0-9baa-2057bf1741c6")]
        public virtual Bars.B4.Modules.FIAS.FiasAddress FactAddress
        {
            get;
            set;
        }

        /// <summary>
        /// ИНН
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("22fe3d6a-1012-4408-b751-c7f03d4d04f6")]
        public virtual System.String INN1
        {
            get;
            set;
        }

        /// <summary>
        /// КПП
        /// </summary>
        [Bars.B4.Utils.Display("КПП")]
        [Bars.Rms.Core.Attributes.Uid("636b8684-7724-4d1a-bd69-1bb828e16569")]
        public virtual System.String KPP1
        {
            get;
            set;
        }

        private System.Collections.Generic.IList<Bars.Nedragis.Dokument> _docs;
        /// <summary>
        /// Документы
        /// </summary>
        [Bars.B4.Utils.Display("Документы")]
        [Bars.Rms.Core.Attributes.Uid("16c7b253-8cf4-4804-8cf5-5092bebbe876")]
        public virtual System.Collections.Generic.IList<Bars.Nedragis.Dokument> docs
        {
            get
            {
                return _docs ?? (_docs = new System.Collections.Generic.List<Bars.Nedragis.Dokument>());
            }

            set
            {
                _docs = value;
            }
        }

        /// <summary>
        /// Регистрация в АТО
        /// </summary>
        [Bars.B4.Utils.Display("Регистрация в АТО")]
        [Bars.Rms.Core.Attributes.Uid("c6d06806-ae7f-4e04-baf7-bffd5f02991c")]
        public virtual System.DateTime ato_date
        {
            get;
            set;
        }
    }
}