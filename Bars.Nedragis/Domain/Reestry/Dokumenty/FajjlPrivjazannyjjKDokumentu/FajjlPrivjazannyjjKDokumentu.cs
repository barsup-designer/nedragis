namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Файл, привязанный к документу
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Файл, привязанный к документу")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("55caa4f7-4f96-47d2-a66a-4d16b044a7cf")]
    [Bars.Rms.Core.Attributes.Uid("55caa4f7-4f96-47d2-a66a-4d16b044a7cf")]
    public class FajjlPrivjazannyjjKDokumentu : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public FajjlPrivjazannyjjKDokumentu(): base ()
        {
        }

        /// <summary>
        /// Документ
        /// </summary>
        [Bars.B4.Utils.Display("Документ")]
        [Bars.Rms.Core.Attributes.Uid("8262a22a-9149-4329-98ec-407a445b09be")]
        public virtual Bars.Nedragis.Dokument Document
        {
            get;
            set;
        }

        /// <summary>
        /// Комментарий
        /// </summary>
        [Bars.B4.Utils.Display("Комментарий")]
        [Bars.Rms.Core.Attributes.Uid("0f87c4c9-82e2-4c41-b4b7-ff5fdcfc469d")]
        public virtual System.String Comment
        {
            get;
            set;
        }

        /// <summary>
        /// Запись файла
        /// </summary>
        [Bars.B4.Utils.Display("Запись файла")]
        [Bars.Rms.Core.Attributes.Uid("33d87bcc-3c11-4abc-847e-845dc3c99e8c")]
        public virtual Bars.B4.Modules.FileStorage.FileInfo FileInfo
        {
            get;
            set;
        }
    }
}