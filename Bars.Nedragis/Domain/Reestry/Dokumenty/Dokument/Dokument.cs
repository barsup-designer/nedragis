namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Документ
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Документ")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("bb2ade91-5fc2-4846-8ee8-8b717abf46fb")]
    [Bars.Rms.Core.Attributes.Uid("bb2ade91-5fc2-4846-8ee8-8b717abf46fb")]
    public class Dokument : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Dokument(): base ()
        {
        }

        /// <summary>
        /// Дата документа
        /// </summary>
        [Bars.B4.Utils.Display("Дата документа")]
        [Bars.Rms.Core.Attributes.Uid("b04912a6-57a3-4cbf-9877-2c47c960c735")]
        public virtual System.DateTime Date
        {
            get;
            set;
        }

        /// <summary>
        /// Дата вступления в силу
        /// </summary>
        [Bars.B4.Utils.Display("Дата вступления в силу")]
        [Bars.Rms.Core.Attributes.Uid("87563068-b9c1-4bd9-a886-ef395b1c0417")]
        public virtual System.DateTime ActualDate
        {
            get;
            set;
        }

        /// <summary>
        /// Номер документа
        /// </summary>
        [Bars.B4.Utils.Display("Номер документа")]
        [Bars.Rms.Core.Attributes.Uid("a786d3f3-7c06-42e9-aca6-8d86b2ff7b68")]
        public virtual System.String Number
        {
            get;
            set;
        }

        /// <summary>
        /// Орган подписавший документ
        /// </summary>
        [Bars.B4.Utils.Display("Орган подписавший документ")]
        [Bars.Rms.Core.Attributes.Uid("6dc50ef8-c0c5-4d09-b077-202a53e27303")]
        public virtual Bars.Nedragis.SubEktyPrava Signatory
        {
            get;
            set;
        }

        /// <summary>
        /// Тип документа
        /// </summary>
        [Bars.B4.Utils.Display("Тип документа")]
        [Bars.Rms.Core.Attributes.Uid("9c89dcca-5979-4614-8413-7afafab6ae59")]
        public virtual Bars.Nedragis.TipyDokumentov Type
        {
            get;
            set;
        }

        /// <summary>
        /// Объекты
        /// </summary>
        [Bars.B4.Utils.Display("Объекты")]
        [Bars.Rms.Core.Attributes.Uid("ef184b2c-ff77-4a0d-9228-0f590871ee19")]
        public virtual Bars.Nedragis.ObEktyPrava ObjectOfLaw
        {
            get;
            set;
        }

        /// <summary>
        /// Основание
        /// </summary>
        [Bars.B4.Utils.Display("Основание")]
        [Bars.Rms.Core.Attributes.Uid("1a3cc3b0-ae31-4f92-81f3-ffab64789947")]
        public virtual System.String Basis
        {
            get;
            set;
        }

        /// <summary>
        /// Субъект
        /// </summary>
        [Bars.B4.Utils.Display("Субъект")]
        [Bars.Rms.Core.Attributes.Uid("4265a9b2-1918-4248-b839-42bac455b5a0")]
        public virtual Bars.Nedragis.JuridicheskieLica Subject
        {
            get;
            set;
        }

        private System.Collections.Generic.IList<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu> _files;
        /// <summary>
        /// Файлы
        /// </summary>
        [Bars.B4.Utils.Display("Файлы")]
        [Bars.Rms.Core.Attributes.Uid("4d6e5eb0-63ec-4a9b-893a-01c5cc6e4ea3")]
        public virtual System.Collections.Generic.IList<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu> files
        {
            get
            {
                return _files ?? (_files = new System.Collections.Generic.List<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu>());
            }

            set
            {
                _files = value;
            }
        }

        private System.Collections.Generic.IList<Bars.Nedragis.DokumentyZU> _zemuch;
        /// <summary>
        /// Земельный участок
        /// </summary>
        [Bars.B4.Utils.Display("Земельный участок")]
        [Bars.Rms.Core.Attributes.Uid("d3a05f52-a5e3-490c-b570-3d38ccb73b08")]
        public virtual System.Collections.Generic.IList<Bars.Nedragis.DokumentyZU> zemuch
        {
            get
            {
                return _zemuch ?? (_zemuch = new System.Collections.Generic.List<Bars.Nedragis.DokumentyZU>());
            }

            set
            {
                _zemuch = value;
            }
        }
    }
}