namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Руководители
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Руководители")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("cf26c7ab-6cf7-49ad-888a-7fb500a92d49")]
    [Bars.Rms.Core.Attributes.Uid("cf26c7ab-6cf7-49ad-888a-7fb500a92d49")]
    public class Rukovoditeli : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Rukovoditeli(): base ()
        {
        }

        /// <summary>
        /// Юридическое лицо
        /// </summary>
        [Bars.B4.Utils.Display("Юридическое лицо")]
        [Bars.Rms.Core.Attributes.Uid("c50abffe-a4aa-4ce8-ae37-e8f78c51a056")]
        public virtual Bars.Nedragis.JuridicheskieLica YurLitco
        {
            get;
            set;
        }

        /// <summary>
        /// Фамилия
        /// </summary>
        [Bars.B4.Utils.Display("Фамилия")]
        [Bars.Rms.Core.Attributes.Uid("8b8bb5a0-c6eb-4d45-a0c7-9dd5a8e7e78b")]
        public virtual System.String Surname
        {
            get;
            set;
        }

        /// <summary>
        /// Имя
        /// </summary>
        [Bars.B4.Utils.Display("Имя")]
        [Bars.Rms.Core.Attributes.Uid("79d3cf0f-cffa-4856-80b6-405056ed5837")]
        public virtual System.String FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Отчество
        /// </summary>
        [Bars.B4.Utils.Display("Отчество")]
        [Bars.Rms.Core.Attributes.Uid("9a9a3d4b-9609-4819-9d7c-4bdefbee63be")]
        public virtual System.String Patronymic
        {
            get;
            set;
        }

        /// <summary>
        /// Пол
        /// </summary>
        [Bars.B4.Utils.Display("Пол")]
        [Bars.Rms.Core.Attributes.Uid("4ea78322-6925-45d1-8e88-8014a0e3eb20")]
        public virtual Bars.Nedragis.Pol? Sex
        {
            get;
            set;
        }

        /// <summary>
        /// Тип должности
        /// </summary>
        [Bars.B4.Utils.Display("Тип должности")]
        [Bars.Rms.Core.Attributes.Uid("27df2a9e-b98e-4c89-8744-3bae756f275f")]
        public virtual Bars.Nedragis.Dolzhnosti PostType
        {
            get;
            set;
        }

        /// <summary>
        /// Должность
        /// </summary>
        [Bars.B4.Utils.Display("Должность")]
        [Bars.Rms.Core.Attributes.Uid("f892df1d-562c-42a9-8f14-3a153fd3f9d5")]
        public virtual System.String PostText
        {
            get;
            set;
        }

        /// <summary>
        /// Действует на основании
        /// </summary>
        [Bars.B4.Utils.Display("Действует на основании")]
        [Bars.Rms.Core.Attributes.Uid("af41a491-d34c-4e5f-ab12-f93aa7f7048e")]
        public virtual System.String Basis
        {
            get;
            set;
        }

        /// <summary>
        /// Телефон
        /// </summary>
        [Bars.B4.Utils.Display("Телефон")]
        [Bars.Rms.Core.Attributes.Uid("ea28d160-6202-49c5-abb9-e9c67bd9b94f")]
        public virtual System.String Phone
        {
            get;
            set;
        }

        /// <summary>
        /// Документ, удостоверяющий личность
        /// </summary>
        [Bars.B4.Utils.Display("Документ, удостоверяющий личность")]
        [Bars.Rms.Core.Attributes.Uid("4b020c55-29a2-4a93-b066-f9c51bbd70c6")]
        public virtual Bars.Nedragis.DokumentyUdostoverjajushhieLichnost udostovereniye
        {
            get;
            set;
        }
    }
}