namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Недропользования
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Недропользования")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("319ddc80-7273-4a3b-b12c-91ad4b942e52")]
    [Bars.Rms.Core.Attributes.Uid("319ddc80-7273-4a3b-b12c-91ad4b942e52")]
    public class NedropolZovanie : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public NedropolZovanie(): base ()
        {
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("2f0d9e92-8733-41b0-8aa7-508fc47215c7")]
        public virtual System.String vid_n
        {
            get;
            set;
        }

        /// <summary>
        /// Количество
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("fea2db08-2b52-4d62-a77e-4a32d15fd69b")]
        public virtual System.Int32 col_n
        {
            get;
            set;
        }

        /// <summary>
        /// Безвозмездное обращение
        /// </summary>
        [Bars.B4.Utils.Display("Безвозмездное обращение")]
        [Bars.Rms.Core.Attributes.Uid("8f552098-4039-4a44-aca7-3c8e5b084a1b")]
        public virtual System.String obr_n
        {
            get;
            set;
        }

        /// <summary>
        /// Дополнительная информация о заказчике
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительная информация о заказчике")]
        [Bars.Rms.Core.Attributes.Uid("fbdaae74-2f64-471f-a689-3b4103e7c4ba")]
        public virtual System.String zakaz_n
        {
            get;
            set;
        }

        /// <summary>
        /// Характер обращения
        /// </summary>
        [Bars.B4.Utils.Display("Характер обращения")]
        [Bars.Rms.Core.Attributes.Uid("1ca35e91-ad00-404b-90a2-cdacaa5f0774")]
        public virtual System.String harakter_n
        {
            get;
            set;
        }

        /// <summary>
        /// Исполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("882a5873-c777-474a-8969-22014fc971df")]
        public virtual Bars.Nedragis.Sotrudniki1 sotr_n
        {
            get;
            set;
        }

        /// <summary>
        /// Орган госвласти
        /// </summary>
        [Bars.B4.Utils.Display("Орган госвласти")]
        [Bars.Rms.Core.Attributes.Uid("04ebe600-fec0-47a3-8112-2dfc3ade4fdd")]
        public virtual System.String organ
        {
            get;
            set;
        }

        /// <summary>
        /// Количество затраченных человека часов
        /// </summary>
        [Bars.B4.Utils.Display("Количество затраченных человека часов")]
        [Bars.Rms.Core.Attributes.Uid("1563d613-528a-4e83-88e8-5d5cf712717d")]
        public virtual System.Int32 chas_n
        {
            get;
            set;
        }

        /// <summary>
        /// Стоимость по калькуляции руб.
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость по калькуляции руб.")]
        [Bars.Rms.Core.Attributes.Uid("7740a407-eb76-438c-8d5a-c317222f00f3")]
        public virtual System.Decimal stoumost
        {
            get;
            set;
        }

        /// <summary>
        /// Оплачено
        /// </summary>
        [Bars.B4.Utils.Display("Оплачено")]
        [Bars.Rms.Core.Attributes.Uid("57026230-4bb4-4e28-a890-df4dfa9b7c0a")]
        public virtual System.String oplata_n
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("85d731d7-2fd6-4257-83e7-8c14153422ef")]
        public virtual Bars.Nedragis.Otdel1 name_n
        {
            get;
            set;
        }

        /// <summary>
        /// Дополнительное описание
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительное описание")]
        [Bars.Rms.Core.Attributes.Uid("12269dd0-f08e-4865-a4e4-9ef1ed483088")]
        public virtual System.String opis_n
        {
            get;
            set;
        }

        /// <summary>
        /// Заказчик
        /// </summary>
        [Bars.B4.Utils.Display("Заказчик")]
        [Bars.Rms.Core.Attributes.Uid("08bf8f5c-3dd0-4382-883a-2fedff98e501")]
        public virtual System.String zakazchik
        {
            get;
            set;
        }

        /// <summary>
        /// Подвид работ
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("3275a278-cd6d-4885-acdd-9596c6041639")]
        public virtual System.String podvid
        {
            get;
            set;
        }
    }
}