namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Объекты регионального надзора
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Объекты регионального надзора")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b")]
    [Bars.Rms.Core.Attributes.Uid("ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b")]
    public class Nadzor : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Nadzor(): base ()
        {
        }

        /// <summary>
        /// Номер по порядку
        /// </summary>
        [Bars.B4.Utils.Display("Номер по порядку")]
        [Bars.Rms.Core.Attributes.Uid("0c0af0ca-330d-4238-8ca7-4b0fd93533d3")]
        public virtual System.Guid nomer
        {
            get;
            set;
        }

        /// <summary>
        /// ИНН
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("6b23037f-c1ec-41fd-9325-e83fda6bb667")]
        public virtual System.Int64 inn
        {
            get;
            set;
        }

        /// <summary>
        /// Номер Лицензии
        /// </summary>
        [Bars.B4.Utils.Display("Номер Лицензии")]
        [Bars.Rms.Core.Attributes.Uid("724a1ace-9cc8-4eaa-b93c-a3ebab18ed40")]
        public virtual System.String Element1507010532348
        {
            get;
            set;
        }

        /// <summary>
        /// Дата регистации
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистации")]
        [Bars.Rms.Core.Attributes.Uid("d44caab2-6388-43f9-9b24-7cf5ed19c4ac")]
        public virtual System.DateTime date
        {
            get;
            set;
        }

        /// <summary>
        /// Окончание срока действия
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия")]
        [Bars.Rms.Core.Attributes.Uid("fe470564-0ffd-462f-b0d1-2e72f2babd67")]
        public virtual System.DateTime? srok_end
        {
            get;
            set;
        }

        /// <summary>
        /// Местонахождение
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("6378e4a9-1904-497b-9f2c-77be3c3a56ba")]
        public virtual System.String mesto
        {
            get;
            set;
        }

        /// <summary>
        /// Номер договора
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("73bba776-4b6f-4a1e-927a-c8b318bab372")]
        public virtual System.Int32 Element1507261981597
        {
            get;
            set;
        }

        /// <summary>
        /// Дата регистрации договора
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистрации договора")]
        [Bars.Rms.Core.Attributes.Uid("31893987-a9d1-442b-81e0-9417baff4a65")]
        public virtual System.DateTime Element1507262033494
        {
            get;
            set;
        }

        /// <summary>
        /// Окончание срока действия договора
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия договора")]
        [Bars.Rms.Core.Attributes.Uid("d49378a8-7478-4ba4-962d-19a02ba50403")]
        public virtual System.DateTime Element1507262072161
        {
            get;
            set;
        }

        /// <summary>
        /// Местоположение
        /// </summary>
        [Bars.B4.Utils.Display("Местоположение")]
        [Bars.Rms.Core.Attributes.Uid("ac975412-32d2-45f8-b41a-87ae55ef2984")]
        public virtual System.String Element1507262105889
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование стационарного источника загрязнения
        /// </summary>
        [Bars.B4.Utils.Display("Наименование стационарного источника загрязнения")]
        [Bars.Rms.Core.Attributes.Uid("1ad85c9f-1b43-48fa-8aee-870883896309")]
        public virtual System.String Element1507262162260
        {
            get;
            set;
        }

        /// <summary>
        /// Местонахождение
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("0b083d72-bdab-46c8-ac2f-fe420d69b6f2")]
        public virtual System.String Element1507262266009
        {
            get;
            set;
        }

        /// <summary>
        /// Наличие тома ПДВ
        /// </summary>
        [Bars.B4.Utils.Display("Наличие тома ПДВ")]
        [Bars.Rms.Core.Attributes.Uid("91e1e0ab-b9da-4c29-8cd0-f86678e88e1c")]
        public virtual System.String Element1507262301335
        {
            get;
            set;
        }

        /// <summary>
        /// Разрешение на выброс в атмосферу
        /// </summary>
        [Bars.B4.Utils.Display("Разрешение на выброс в атмосферу")]
        [Bars.Rms.Core.Attributes.Uid("e0b4ead1-2dea-4027-8fe7-31fb183c1cbb")]
        public virtual System.String Element1507262339156
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование объекта образования отходов
        /// </summary>
        [Bars.B4.Utils.Display("Наименование объекта образования отходов")]
        [Bars.Rms.Core.Attributes.Uid("4647ced5-c421-4bde-bc8b-29cb2374ec4e")]
        public virtual System.String Element1507262379380
        {
            get;
            set;
        }

        /// <summary>
        /// Местонахождение
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("a9adbe34-7d92-4a53-a6d1-15e78f9a9830")]
        public virtual System.String Element1507262414231
        {
            get;
            set;
        }

        /// <summary>
        /// Проект нормативов образования отходов и лимитов на их размещение
        /// </summary>
        [Bars.B4.Utils.Display("Проект нормативов образования отходов и лимитов на их размещение")]
        [Bars.Rms.Core.Attributes.Uid("10c8749a-0faa-4c82-92b9-660d008b0b34")]
        public virtual System.String Element1507262455649
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование субъекта надзора
        /// </summary>
        [Bars.B4.Utils.Display("Наименование субъекта надзора")]
        [Bars.Rms.Core.Attributes.Uid("26d536f1-dc05-4d6f-a7e5-98e87be26490")]
        public virtual Bars.Nedragis.NaimenovanieObEktovNadzora Element1507271571833
        {
            get;
            set;
        }

        /// <summary>
        /// Код свидетельства
        /// </summary>
        [Bars.B4.Utils.Display("Код свидетельства")]
        [Bars.Rms.Core.Attributes.Uid("bd254f04-6ed8-4f8f-b98a-8e073a42a86b")]
        public virtual System.String Element1507611045539
        {
            get;
            set;
        }

        /// <summary>
        /// Номер
        /// </summary>
        [Bars.B4.Utils.Display("Номер")]
        [Bars.Rms.Core.Attributes.Uid("b0cb6807-9191-4f9a-9af3-58b5140f5b80")]
        public virtual System.Int32 Element1507619429093
        {
            get;
            set;
        }

        /// <summary>
        /// Договор водопользования
        /// </summary>
        [Bars.B4.Utils.Display("Договор водопользования")]
        [Bars.Rms.Core.Attributes.Uid("9c3d7d3e-8176-4a13-a464-95f00cc83d42")]
        public virtual System.String Element1507703919662
        {
            get;
            set;
        }
    }
}