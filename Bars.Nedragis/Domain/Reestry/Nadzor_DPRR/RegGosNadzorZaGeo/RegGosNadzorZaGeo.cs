namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// ОПИ
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"ОПИ")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("d14687ca-1601-4731-a8ab-e4e2311cdb65")]
    [Bars.Rms.Core.Attributes.Uid("d14687ca-1601-4731-a8ab-e4e2311cdb65")]
    public class RegGosNadzorZaGeo : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public RegGosNadzorZaGeo(): base ()
        {
        }

        /// <summary>
        /// Номер лицензии
        /// </summary>
        [Bars.B4.Utils.Display("Номер лицензии")]
        [Bars.Rms.Core.Attributes.Uid("57067dcd-fbdc-4de0-b6d0-0c2c63bd14e4")]
        public virtual System.String Element1507635501632
        {
            get;
            set;
        }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистрации")]
        [Bars.Rms.Core.Attributes.Uid("0832b728-0baf-41a0-80c5-d3afa04fee3e")]
        public virtual System.DateTime Element1507635537755
        {
            get;
            set;
        }

        /// <summary>
        /// Окончание срока действия
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия")]
        [Bars.Rms.Core.Attributes.Uid("d2b08b92-00bc-45ae-850d-6ec56e5820e1")]
        public virtual System.DateTime Element1507635559781
        {
            get;
            set;
        }

        /// <summary>
        /// Местонаходжение
        /// </summary>
        [Bars.B4.Utils.Display("Местонаходжение")]
        [Bars.Rms.Core.Attributes.Uid("ede82dfa-23a3-4bb4-808b-03b818537dbc")]
        public virtual System.String Element1507635586209
        {
            get;
            set;
        }

        private System.Collections.Generic.IList<Bars.Nedragis.ObEktopi> _Element1508137368182;
        /// <summary>
        /// объектсвязь
        /// </summary>
        [Bars.B4.Utils.Display("объектсвязь")]
        [Bars.Rms.Core.Attributes.Uid("e0a7a7dc-a999-46dc-9b83-c0e3f1dffd90")]
        public virtual System.Collections.Generic.IList<Bars.Nedragis.ObEktopi> Element1508137368182
        {
            get
            {
                return _Element1508137368182 ?? (_Element1508137368182 = new System.Collections.Generic.List<Bars.Nedragis.ObEktopi>());
            }

            set
            {
                _Element1508137368182 = value;
            }
        }
    }
}