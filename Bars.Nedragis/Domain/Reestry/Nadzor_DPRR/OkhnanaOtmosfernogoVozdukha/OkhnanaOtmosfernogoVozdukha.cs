namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Воздух
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Воздух")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("54322ad9-c6fe-46a8-9767-7496ae71046b")]
    [Bars.Rms.Core.Attributes.Uid("54322ad9-c6fe-46a8-9767-7496ae71046b")]
    public class OkhnanaOtmosfernogoVozdukha : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public OkhnanaOtmosfernogoVozdukha(): base ()
        {
        }

        /// <summary>
        /// Название стационарного источника загрязнения
        /// </summary>
        [Bars.B4.Utils.Display("Название стационарного источника загрязнения")]
        [Bars.Rms.Core.Attributes.Uid("3f19a355-f26f-4187-b3a0-f360121f7c92")]
        public virtual System.String Element1507788951680
        {
            get;
            set;
        }

        /// <summary>
        /// Местонахождение
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("4b2d36e1-440a-4cc7-8074-f3142e5a4540")]
        public virtual System.String Element1507788999087
        {
            get;
            set;
        }

        /// <summary>
        /// Наличие тома ПДВ
        /// </summary>
        [Bars.B4.Utils.Display("Наличие тома ПДВ")]
        [Bars.Rms.Core.Attributes.Uid("b9132014-687a-441a-b4cf-0ee994c671fa")]
        public virtual System.String Element1507789018715
        {
            get;
            set;
        }

        /// <summary>
        /// Разрешение на выброс в атмосферу
        /// </summary>
        [Bars.B4.Utils.Display("Разрешение на выброс в атмосферу")]
        [Bars.Rms.Core.Attributes.Uid("57e3f20d-9ee1-4db7-8808-223c95223266")]
        public virtual System.String Element1507789044428
        {
            get;
            set;
        }

        /// <summary>
        /// Код свидетельства
        /// </summary>
        [Bars.B4.Utils.Display("Код свидетельства")]
        [Bars.Rms.Core.Attributes.Uid("ea0ebecf-c028-4c33-9710-c8f6def9b2d7")]
        public virtual System.String Element1507791810720
        {
            get;
            set;
        }
    }
}