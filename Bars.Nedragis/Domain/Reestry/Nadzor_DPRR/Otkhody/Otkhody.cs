namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Отходы
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Отходы")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("07fadf5a-6010-4fa1-8cf6-39f3e5cf7590")]
    [Bars.Rms.Core.Attributes.Uid("07fadf5a-6010-4fa1-8cf6-39f3e5cf7590")]
    public class Otkhody : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Otkhody(): base ()
        {
        }

        /// <summary>
        /// Наименование объекта образования отходов
        /// </summary>
        [Bars.B4.Utils.Display("Наименование объекта образования отходов")]
        [Bars.Rms.Core.Attributes.Uid("f446c90f-b549-4d26-bbf4-82f8cc2ee71e")]
        public virtual System.String Element1507790021761
        {
            get;
            set;
        }

        /// <summary>
        /// Местонахождение
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("5a616d20-8796-493e-b547-c004f80861d6")]
        public virtual System.String Element1507791662904
        {
            get;
            set;
        }

        /// <summary>
        /// Проект нормативов образования отходов и лимитов на их размещение
        /// </summary>
        [Bars.B4.Utils.Display("Проект нормативов образования отходов и лимитов на их размещение")]
        [Bars.Rms.Core.Attributes.Uid("2efd4c92-2535-447d-83cd-4d18c3c6a031")]
        public virtual System.String Element1507791696742
        {
            get;
            set;
        }
    }
}