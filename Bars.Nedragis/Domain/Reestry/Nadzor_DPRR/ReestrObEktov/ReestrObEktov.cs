namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Реестр объектов
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Реестр объектов")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("69f498ef-df00-409a-a699-c742d843e803")]
    [Bars.Rms.Core.Attributes.Uid("69f498ef-df00-409a-a699-c742d843e803")]
    public class ReestrObEktov : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public ReestrObEktov(): base ()
        {
        }

        /// <summary>
        /// Наименование субъектов надзора
        /// </summary>
        [Bars.B4.Utils.Display("Наименование субъектов надзора")]
        [Bars.Rms.Core.Attributes.Uid("ee0925dd-63d7-4d98-a2a5-150dd0a9ca04")]
        public virtual Bars.Nedragis.NaimenovanieObEktovNadzora Element1507784253669
        {
            get;
            set;
        }

        /// <summary>
        /// ИНН
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("aff3cb72-823c-46a9-81b1-13115aaa6464")]
        public virtual System.Int32 Element1507784343465
        {
            get;
            set;
        }

        /// <summary>
        /// Номер по порядку
        /// </summary>
        [Bars.B4.Utils.Display("Номер по порядку")]
        [Bars.Rms.Core.Attributes.Uid("290a41e8-833d-446f-a7f1-aa9dc04f99cb")]
        public virtual System.Int32 Element1507784366035
        {
            get;
            set;
        }

        private System.Collections.Generic.IList<Bars.Nedragis.ObEktopi> _Element1508137072874;
        /// <summary>
        /// описвязь
        /// </summary>
        [Bars.B4.Utils.Display("описвязь")]
        [Bars.Rms.Core.Attributes.Uid("eab1b07d-b45f-41cc-bc50-0ebc646d94be")]
        public virtual System.Collections.Generic.IList<Bars.Nedragis.ObEktopi> Element1508137072874
        {
            get
            {
                return _Element1508137072874 ?? (_Element1508137072874 = new System.Collections.Generic.List<Bars.Nedragis.ObEktopi>());
            }

            set
            {
                _Element1508137072874 = value;
            }
        }

        private System.Collections.Generic.IList<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov> _Element1508151176209;
        /// <summary>
        /// Вода
        /// </summary>
        [Bars.B4.Utils.Display("Вода")]
        [Bars.Rms.Core.Attributes.Uid("f913cbf5-a000-4f15-927a-f911bce5cade")]
        public virtual System.Collections.Generic.IList<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov> Element1508151176209
        {
            get
            {
                return _Element1508151176209 ?? (_Element1508151176209 = new System.Collections.Generic.List<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov>());
            }

            set
            {
                _Element1508151176209 = value;
            }
        }
    }
}