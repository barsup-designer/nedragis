namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// объектопи
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"объектопи")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("2fd9e43e-bd97-4446-a65e-77f22758149b")]
    [Bars.Rms.Core.Attributes.Uid("2fd9e43e-bd97-4446-a65e-77f22758149b")]
    public class ObEktopi : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public ObEktopi(): base ()
        {
        }

        /// <summary>
        /// Объект
        /// </summary>
        [Bars.B4.Utils.Display("Объект")]
        [Bars.Rms.Core.Attributes.Uid("77e911b2-1933-408c-ace6-faa249142519")]
        public virtual Bars.Nedragis.ReestrObEktov Element1508136885094
        {
            get;
            set;
        }

        /// <summary>
        /// опи
        /// </summary>
        [Bars.B4.Utils.Display("опи")]
        [Bars.Rms.Core.Attributes.Uid("8e828051-8b23-447a-abc8-e0828173b132")]
        public virtual Bars.Nedragis.RegGosNadzorZaGeo Element1508137003934
        {
            get;
            set;
        }
    }
}