namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Вода
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Вода")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("8d4cd756-d656-4caa-805b-242b484000f7")]
    [Bars.Rms.Core.Attributes.Uid("8d4cd756-d656-4caa-805b-242b484000f7")]
    public class RegionalNyjjNadzorVodnykhObEktov : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public RegionalNyjjNadzorVodnykhObEktov(): base ()
        {
        }

        /// <summary>
        /// Номер договора
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("242c25a8-da1c-46f3-8483-03b694abeac9")]
        public virtual System.String Element1507787714216
        {
            get;
            set;
        }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистрации")]
        [Bars.Rms.Core.Attributes.Uid("aefa3f0d-955d-44a4-9595-5cf81ebbbb9c")]
        public virtual System.DateTime Element1507787771917
        {
            get;
            set;
        }

        /// <summary>
        /// Окончание срока действия
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия")]
        [Bars.Rms.Core.Attributes.Uid("feab30c0-ce5b-4340-a322-854df625398b")]
        public virtual System.DateTime Element1507787816822
        {
            get;
            set;
        }

        /// <summary>
        /// Местонахождение
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("b9f05a6e-fd13-4cad-8a22-e517b56ec08f")]
        public virtual System.String Element1507787841624
        {
            get;
            set;
        }

        /// <summary>
        /// Объект
        /// </summary>
        [Bars.B4.Utils.Display("Объект")]
        [Bars.Rms.Core.Attributes.Uid("a3873da8-01f3-4c19-b876-ed44d8041cd5")]
        public virtual Bars.Nedragis.ReestrObEktov Element1508150872914
        {
            get;
            set;
        }
    }
}