namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Картография
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Картография")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("cdbf139a-d2ac-4731-87f9-40f673f96b6c")]
    [Bars.Rms.Core.Attributes.Uid("cdbf139a-d2ac-4731-87f9-40f673f96b6c")]
    public class Kartograf : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Kartograf(): base ()
        {
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("855ca052-45ad-4242-9833-43401bd61655")]
        public virtual Bars.Nedragis.VidyRabot vid
        {
            get;
            set;
        }

        /// <summary>
        /// Количество
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("2b6965ac-365b-4fca-8a15-ece0a950a0bc")]
        public virtual System.Int32 col
        {
            get;
            set;
        }

        /// <summary>
        /// Безвозмездное обращение
        /// </summary>
        [Bars.B4.Utils.Display("Безвозмездное обращение")]
        [Bars.Rms.Core.Attributes.Uid("0610999c-b538-4d33-b5cc-c20f7ffb82dc")]
        public virtual System.String Element1474351766399
        {
            get;
            set;
        }

        /// <summary>
        /// Дополнительная информация о заказчике
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительная информация о заказчике")]
        [Bars.Rms.Core.Attributes.Uid("850821f5-5d53-44c8-aa98-f106899453fa")]
        public virtual System.String zakaz1
        {
            get;
            set;
        }

        /// <summary>
        /// Характер обращения
        /// </summary>
        [Bars.B4.Utils.Display("Характер обращения")]
        [Bars.Rms.Core.Attributes.Uid("d9a6c3b5-d79d-413a-9238-41b3665a2db9")]
        public virtual System.String harakter
        {
            get;
            set;
        }

        /// <summary>
        /// Исполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("1b083742-4ba4-4acc-9458-1770b3938096")]
        public virtual Bars.Nedragis.Sotrudniki1 Element1474352034381
        {
            get;
            set;
        }

        /// <summary>
        /// Орган госвласти
        /// </summary>
        [Bars.B4.Utils.Display("Орган госвласти")]
        [Bars.Rms.Core.Attributes.Uid("a66379e8-57f2-43f2-9e10-cbf2ad762efc")]
        public virtual System.String organ
        {
            get;
            set;
        }

        /// <summary>
        /// Количество затраченных человека часов
        /// </summary>
        [Bars.B4.Utils.Display("Количество затраченных человека часов")]
        [Bars.Rms.Core.Attributes.Uid("98b11871-fdf4-4314-a422-fc681d38f492")]
        public virtual System.Int32 chas
        {
            get;
            set;
        }

        /// <summary>
        /// Стоимость по калькуляции руб.
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость по калькуляции руб.")]
        [Bars.Rms.Core.Attributes.Uid("24b35972-d5e2-4a46-bdb2-c08b7c752a79")]
        public virtual System.Decimal stoumost
        {
            get;
            set;
        }

        /// <summary>
        /// Оплачено
        /// </summary>
        [Bars.B4.Utils.Display("Оплачено")]
        [Bars.Rms.Core.Attributes.Uid("dbf67e1f-2787-4114-a0bd-e30e144337e9")]
        public virtual System.String oplata
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("78b1a24b-73bc-4275-8e6d-f813f05aa59c")]
        public virtual Bars.Nedragis.Otdel1 name
        {
            get;
            set;
        }

        /// <summary>
        /// Дополнительное описание
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительное описание")]
        [Bars.Rms.Core.Attributes.Uid("d4799bbc-5212-4cf1-8c11-829143516132")]
        public virtual System.String opis
        {
            get;
            set;
        }

        /// <summary>
        /// Заказчик
        /// </summary>
        [Bars.B4.Utils.Display("Заказчик")]
        [Bars.Rms.Core.Attributes.Uid("197c87d0-88ed-4004-b1fb-eae10895b230")]
        public virtual System.String zakazchik
        {
            get;
            set;
        }

        /// <summary>
        /// Подвид работ
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("be7b8c57-394d-4734-9b44-a413b2a06388")]
        public virtual System.String podvid
        {
            get;
            set;
        }
    }
}