namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Природопользование 2
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Природопользование 2")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("825f8a0a-cc49-42e0-8d04-df96f9f2e519")]
    [Bars.Rms.Core.Attributes.Uid("825f8a0a-cc49-42e0-8d04-df96f9f2e519")]
    public class PrirodopolZovanie2 : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public PrirodopolZovanie2(): base ()
        {
        }

        /// <summary>
        /// Дата входящего
        /// </summary>
        [Bars.B4.Utils.Display("Дата входящего")]
        [Bars.Rms.Core.Attributes.Uid("a2516249-c410-4d40-902e-08e5d0369bb6")]
        public virtual System.DateTime DateInPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Номер входящего
        /// </summary>
        [Bars.B4.Utils.Display("Номер входящего")]
        [Bars.Rms.Core.Attributes.Uid("49c48009-3fc5-40bf-8d24-95eb5de7fa79")]
        public virtual System.String NumInDocsPr2
        {
            get;
            set;
        }

        /// <summary>
        /// Дата исполнения
        /// </summary>
        [Bars.B4.Utils.Display("Дата исполнения")]
        [Bars.Rms.Core.Attributes.Uid("610bbaee-be1b-4d36-919c-2663c80128cc")]
        public virtual System.DateTime DateCompletePR2
        {
            get;
            set;
        }

        /// <summary>
        /// Компания Заказчик
        /// </summary>
        [Bars.B4.Utils.Display("Компания Заказчик")]
        [Bars.Rms.Core.Attributes.Uid("d697ddd7-4d57-4d7d-96e3-58910dc06743")]
        public virtual System.String CompanyPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("5b77a419-637f-4b69-82a5-80c3756e9c44")]
        public virtual Bars.Nedragis.VidRabotPrir VidRabotPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Количество
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("1fd92fbe-274a-4ca5-b920-9b2449cba2d3")]
        public virtual System.Int32 QuantityPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Объект
        /// </summary>
        [Bars.B4.Utils.Display("Объект")]
        [Bars.Rms.Core.Attributes.Uid("b11561d7-197a-4e13-9128-1fbbb0cc0400")]
        public virtual System.String ObjectPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Номер договора
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("cb1e975c-ff01-4461-a14d-94fe18775ee1")]
        public virtual System.String NumDogovorPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Номер заявки 
        /// </summary>
        [Bars.B4.Utils.Display("Номер заявки ")]
        [Bars.Rms.Core.Attributes.Uid("0eace844-d207-48e6-b327-e680938246c6")]
        public virtual System.Int32 NumZayvkaPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Код
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("5fb58b91-57ba-4489-8e52-2dda9782cd78")]
        public virtual System.String CodePR2
        {
            get;
            set;
        }

        /// <summary>
        /// Сумма к оплате
        /// </summary>
        [Bars.B4.Utils.Display("Сумма к оплате")]
        [Bars.Rms.Core.Attributes.Uid("2411d741-27e1-4d00-a1cc-3fff960aa7ae")]
        public virtual System.Double SummToPayPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификационный номер
        /// </summary>
        [Bars.B4.Utils.Display("Идентификационный номер")]
        [Bars.Rms.Core.Attributes.Uid("ff4e643d-7f7f-4475-94c7-0e6752be8b2e")]
        public virtual System.String IdentNumderPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Исполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("ace26669-298f-42bb-86a3-85b45d0665d9")]
        public virtual Bars.Nedragis.Sotrudniki1 IspolnitelPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Месяц
        /// </summary>
        [Bars.B4.Utils.Display("Месяц")]
        [Bars.Rms.Core.Attributes.Uid("839608b1-d5e6-47f9-ab37-611fc6140b01")]
        public virtual Bars.Nedragis.MesjacPriroda MonthPR2
        {
            get;
            set;
        }

        /// <summary>
        /// ФИО заказчика
        /// </summary>
        [Bars.B4.Utils.Display("ФИО заказчика")]
        [Bars.Rms.Core.Attributes.Uid("3be09518-41c0-4b19-bdc8-a17aab9bc9e3")]
        public virtual System.String FIOZakazchikaPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Примечание
        /// </summary>
        [Bars.B4.Utils.Display("Примечание")]
        [Bars.Rms.Core.Attributes.Uid("9e8cacdb-e604-4ff6-add7-48be233b46ac")]
        public virtual System.String PrimechaniePR2
        {
            get;
            set;
        }

        /// <summary>
        /// Соисполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Соисполнитель")]
        [Bars.Rms.Core.Attributes.Uid("83377ef3-bbf0-4218-a843-123c59f4ad60")]
        public virtual Bars.Nedragis.Sotrudniki1 SoispolnitelPR2
        {
            get;
            set;
        }
    }
}