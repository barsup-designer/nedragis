namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Реестр природопользования
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Реестр природопользования")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("94066dd1-2a30-4f0c-8bde-3504bc823788")]
    [Bars.Rms.Core.Attributes.Uid("94066dd1-2a30-4f0c-8bde-3504bc823788")]
    public class ReestrPrirodopolZovanija : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public ReestrPrirodopolZovanija(): base ()
        {
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("d075d6e4-698b-4947-bd4c-5426fececdfd")]
        public virtual Bars.Nedragis.Otdel1 name_otdel_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("1bdf36bf-a652-42a8-8ccb-22798fcae151")]
        public virtual Bars.Nedragis.VidRabotPrir vid_rabot_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Кол-во
        /// </summary>
        [Bars.B4.Utils.Display("Кол-во")]
        [Bars.Rms.Core.Attributes.Uid("68eb3eb7-59ac-4e33-8b1d-28e0e6743be9")]
        public virtual System.Int32 kolvo_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Дата входящего
        /// </summary>
        [Bars.B4.Utils.Display("Дата входящего")]
        [Bars.Rms.Core.Attributes.Uid("41bf4202-ad76-4dbf-b460-73382dd8d18b")]
        public virtual System.DateTime date_vh_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Дата исполнения
        /// </summary>
        [Bars.B4.Utils.Display("Дата исполнения")]
        [Bars.Rms.Core.Attributes.Uid("5c889cf3-10d8-4ec1-998b-08b3576edbf2")]
        public virtual System.DateTime date_isp_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Органы власти
        /// </summary>
        [Bars.B4.Utils.Display("Органы власти")]
        [Bars.Rms.Core.Attributes.Uid("c7270a46-590d-4566-bfe3-9f8be7030245")]
        public virtual Bars.Nedragis.OrganyVlastiPriroda org_vlast_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Объект
        /// </summary>
        [Bars.B4.Utils.Display("Объект")]
        [Bars.Rms.Core.Attributes.Uid("269e896b-149a-4faf-83a1-34e176add189")]
        public virtual System.String object_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Исполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("e51ef3a9-2595-4762-9cbd-b2b28d6e38e4")]
        public virtual Bars.Nedragis.Sotrudniki1 ispoln_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Примечание
        /// </summary>
        [Bars.B4.Utils.Display("Примечание")]
        [Bars.Rms.Core.Attributes.Uid("cfc88432-d778-48a4-a09e-f61d92a71cb4")]
        public virtual System.String prim_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Номер договора
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("39d9c3b6-6958-4982-94c2-855bfcaf4169")]
        public virtual System.String nom_dogovor_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Сумма к оплате
        /// </summary>
        [Bars.B4.Utils.Display("Сумма к оплате")]
        [Bars.Rms.Core.Attributes.Uid("8ad113c4-655e-46e8-8b39-cee9f1f667b3")]
        public virtual System.Double sum_oplata_pr
        {
            get;
            set;
        }

        /// <summary>
        /// идентификационный номер 
        /// </summary>
        [Bars.B4.Utils.Display("идентификационный номер ")]
        [Bars.Rms.Core.Attributes.Uid("82ca742e-99bc-460e-be42-1e8c9a41a733")]
        public virtual System.String id_nom_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Номер входящего
        /// </summary>
        [Bars.B4.Utils.Display("Номер входящего")]
        [Bars.Rms.Core.Attributes.Uid("a014bf94-740f-4e3d-9e47-6ca152ae3983")]
        public virtual System.String n_vh_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Отчетный месяц
        /// </summary>
        [Bars.B4.Utils.Display("Отчетный месяц")]
        [Bars.Rms.Core.Attributes.Uid("2089064e-6880-4f5c-9bb3-2270333944f3")]
        public virtual Bars.Nedragis.MesjacPriroda o_month_pr
        {
            get;
            set;
        }

        /// <summary>
        /// Подвид работ
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("c4397871-9f11-486d-a805-cdaf1bed6b89")]
        public virtual Bars.Nedragis.PodvidRabotPriroda P_rabot_pr
        {
            get;
            set;
        }
    }
}