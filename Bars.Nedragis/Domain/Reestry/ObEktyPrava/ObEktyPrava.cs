namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Объекты права
    /// Объекты права, т.е. сущности, которыми владеют субъекты права. Родитель для сущности Земельные участки.
    /// </summary>
    [Bars.B4.Utils.Display(@"Объекты права")]
    [Bars.B4.Utils.Description(@"Объекты права, т.е. сущности, которыми владеют субъекты права. Родитель для сущности Земельные участки.")]
    [System.Runtime.InteropServices.GuidAttribute("5ae8ff22-f535-4943-a5fc-d30b873125e6")]
    [Bars.Rms.Core.Attributes.Uid("5ae8ff22-f535-4943-a5fc-d30b873125e6")]
    public class ObEktyPrava : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public ObEktyPrava(): base ()
        {
        }

        /// <summary>
        /// Представление для отображения в гридах со смешанным типом потомков
        /// </summary>
        [Bars.B4.Utils.Display("Представление для отображения в гридах со смешанным типом потомков")]
        [Bars.Rms.Core.Attributes.Uid("4ac476ce-6e8b-4e0a-a1bd-e3b99cf5a134")]
        public virtual System.String Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Тип реестра
        /// </summary>
        [Bars.B4.Utils.Display("Тип реестра")]
        [Bars.Rms.Core.Attributes.Uid("58e7e466-95d0-44f4-b225-cfe9f75019cd")]
        public virtual Bars.Nedragis.Reestry type
        {
            get;
            set;
        }

        /// <summary>
        /// Собственник
        /// </summary>
        [Bars.B4.Utils.Display("Собственник")]
        [Bars.Rms.Core.Attributes.Uid("a47f4db2-bfc6-454c-bfc9-f30970ac0faf")]
        public virtual Bars.Nedragis.SubEktyPrava owner_id
        {
            get;
            set;
        }

        /// <summary>
        /// Правообладатель
        /// </summary>
        [Bars.B4.Utils.Display("Правообладатель")]
        [Bars.Rms.Core.Attributes.Uid("5859faf6-8367-4bcf-b7d2-d5c80692f032")]
        public virtual Bars.Nedragis.SubEktyPrava holder_id
        {
            get;
            set;
        }

        /// <summary>
        /// Уполномоченный орган
        /// </summary>
        [Bars.B4.Utils.Display("Уполномоченный орган")]
        [Bars.Rms.Core.Attributes.Uid("d63fb382-865b-4a7a-abe3-4806066b552b")]
        public virtual Bars.Nedragis.JuridicheskieLica property_manager_id
        {
            get;
            set;
        }

        /// <summary>
        /// Статус объекта
        /// </summary>
        [Bars.B4.Utils.Display("Статус объекта")]
        [Bars.Rms.Core.Attributes.Uid("aa32199a-b973-4bb9-9006-1c63dc9ae5f9")]
        public virtual Bars.Nedragis.StatusyObEkta Status_obj
        {
            get;
            set;
        }

        /// <summary>
        /// Тип объекта права
        /// </summary>
        [Bars.B4.Utils.Display("Тип объекта права")]
        [Bars.Rms.Core.Attributes.Uid("cebda652-11c0-44e4-a938-3104514ebc81")]
        public virtual Bars.Nedragis.TipyObEktaPrava typeobj
        {
            get;
            set;
        }
    }
}