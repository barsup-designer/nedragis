namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// zim1
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"zim1")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("65ab9d8b-bf41-47db-9196-b2c564e8e49f")]
    [Bars.Rms.Core.Attributes.Uid("65ab9d8b-bf41-47db-9196-b2c564e8e49f")]
    public class Zim1 : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Zim1(): base ()
        {
        }

        /// <summary>
        /// наименование
        /// </summary>
        [Bars.B4.Utils.Display("наименование")]
        [Bars.Rms.Core.Attributes.Uid("cd7d2787-5464-4350-b280-7dc2ef45e80b")]
        public virtual Bars.Nedragis.Zima1 Element1517482864074
        {
            get;
            set;
        }
    }
}