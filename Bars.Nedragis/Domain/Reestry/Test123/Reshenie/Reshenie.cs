namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Решение по праву приобретения информации
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Решение по праву приобретения информации")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("b8942305-e948-41d0-a03a-72e1d0f028e6")]
    [Bars.Rms.Core.Attributes.Uid("b8942305-e948-41d0-a03a-72e1d0f028e6")]
    public class Reshenie : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Reshenie(): base ()
        {
        }

        /// <summary>
        /// Номер решения
        /// </summary>
        [Bars.B4.Utils.Display("Номер решения")]
        [Bars.Rms.Core.Attributes.Uid("a29d7d78-c63c-485c-9edf-300bcbdb26c8")]
        public virtual System.String Element1478752620339
        {
            get;
            set;
        }

        /// <summary>
        /// Дата решения
        /// </summary>
        [Bars.B4.Utils.Display("Дата решения")]
        [Bars.Rms.Core.Attributes.Uid("bde6e68f-2b25-4bfc-ad42-abac1138ee78")]
        public virtual System.DateTime Element1478752748169
        {
            get;
            set;
        }

        /// <summary>
        /// Текст
        /// </summary>
        [Bars.B4.Utils.Display("Текст")]
        [Bars.Rms.Core.Attributes.Uid("1670c70c-78c8-4177-916c-d1c4574b0b4a")]
        public virtual System.String Element1478752768753
        {
            get;
            set;
        }

        /// <summary>
        /// Номер запроса в фед. агенство
        /// </summary>
        [Bars.B4.Utils.Display("Номер запроса в фед. агенство")]
        [Bars.Rms.Core.Attributes.Uid("b688b599-b11b-4070-b637-1e04bbc997d3")]
        public virtual System.Int64 Element1478752791585
        {
            get;
            set;
        }

        /// <summary>
        /// Дата запроса в фед агенство
        /// </summary>
        [Bars.B4.Utils.Display("Дата запроса в фед агенство")]
        [Bars.Rms.Core.Attributes.Uid("56458763-bb83-4c8a-8c59-b96a241d2e6f")]
        public virtual System.DateTime Element1478752831192
        {
            get;
            set;
        }

        /// <summary>
        /// Текст запроса в фед. агенство
        /// </summary>
        [Bars.B4.Utils.Display("Текст запроса в фед. агенство")]
        [Bars.Rms.Core.Attributes.Uid("f445b1c6-48ed-4036-b1c3-a74867f33644")]
        public virtual System.String Element1478752856240
        {
            get;
            set;
        }

        /// <summary>
        /// Номер решения фед.агенства
        /// </summary>
        [Bars.B4.Utils.Display("Номер решения фед.агенства")]
        [Bars.Rms.Core.Attributes.Uid("32d01253-3d7a-4ceb-acbd-cff87365dff2")]
        public virtual System.String Element1478752884224
        {
            get;
            set;
        }

        /// <summary>
        /// Дата решения фед агенства
        /// </summary>
        [Bars.B4.Utils.Display("Дата решения фед агенства")]
        [Bars.Rms.Core.Attributes.Uid("f0abd0db-3b22-462e-99f9-55742f5195be")]
        public virtual System.DateTime Element1478753246124
        {
            get;
            set;
        }

        /// <summary>
        /// Текст решения фед агенства
        /// </summary>
        [Bars.B4.Utils.Display("Текст решения фед агенства")]
        [Bars.Rms.Core.Attributes.Uid("153db206-4c7a-498d-a883-045d9070f003")]
        public virtual System.String Element1478753305771
        {
            get;
            set;
        }

        /// <summary>
        /// Руководитель фед. агенства
        /// </summary>
        [Bars.B4.Utils.Display("Руководитель фед. агенства")]
        [Bars.Rms.Core.Attributes.Uid("1a335281-3ff3-46a1-959b-fcdf91b36425")]
        public virtual System.String Element1478753329859
        {
            get;
            set;
        }

        /// <summary>
        /// Номер спецификации по заявке
        /// </summary>
        [Bars.B4.Utils.Display("Номер спецификации по заявке")]
        [Bars.Rms.Core.Attributes.Uid("db316b82-4af0-4c9b-b44f-8a7cec10f291")]
        public virtual Bars.Nedragis.Specifikacija Element1478753351594
        {
            get;
            set;
        }
    }
}