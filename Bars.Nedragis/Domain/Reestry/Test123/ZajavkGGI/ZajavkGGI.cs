namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Заявка на приобретение ГГИ
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Заявка на приобретение ГГИ")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("76a3b40d-31b1-464f-9d1e-ff5f739b513d")]
    [Bars.Rms.Core.Attributes.Uid("76a3b40d-31b1-464f-9d1e-ff5f739b513d")]
    public class ZajavkGGI : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public ZajavkGGI(): base ()
        {
        }

        /// <summary>
        /// Номер заявки
        /// </summary>
        [Bars.B4.Utils.Display("Номер заявки")]
        [Bars.Rms.Core.Attributes.Uid("b33837b9-bc11-4156-8bed-1d985424c588")]
        public virtual System.Int32 nomz
        {
            get;
            set;
        }

        /// <summary>
        /// Дата заявки
        /// </summary>
        [Bars.B4.Utils.Display("Дата заявки")]
        [Bars.Rms.Core.Attributes.Uid("e45439e7-688f-4fce-a497-7cda8744b21b")]
        public virtual System.DateTime dataz
        {
            get;
            set;
        }

        /// <summary>
        /// Дата исполнения
        /// </summary>
        [Bars.B4.Utils.Display("Дата исполнения")]
        [Bars.Rms.Core.Attributes.Uid("323eb7f8-9223-4911-957e-0eaa7faee193")]
        public virtual System.DateTime dati
        {
            get;
            set;
        }

        /// <summary>
        /// Недропользователь
        /// </summary>
        [Bars.B4.Utils.Display("Недропользователь")]
        [Bars.Rms.Core.Attributes.Uid("a15c2ebc-29f9-42eb-8248-2edcc118ffbb")]
        public virtual Bars.Nedragis.NedropolZovatel NedropolZovatel
        {
            get;
            set;
        }

        /// <summary>
        /// Лицензионный участок
        /// </summary>
        [Bars.B4.Utils.Display("Лицензионный участок")]
        [Bars.Rms.Core.Attributes.Uid("657161ad-c8cc-4f08-aca2-e6f3a754e23a")]
        public virtual Bars.Nedragis.LicUchastki luch
        {
            get;
            set;
        }

        /// <summary>
        /// Текст
        /// </summary>
        [Bars.B4.Utils.Display("Текст")]
        [Bars.Rms.Core.Attributes.Uid("64e0fb82-fcab-4ca5-b848-a28a15a2e705")]
        public virtual System.String texto
        {
            get;
            set;
        }
    }
}