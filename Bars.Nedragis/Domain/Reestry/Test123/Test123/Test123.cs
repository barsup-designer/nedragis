namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// test123
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"test123")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("e0eea06f-d065-4925-bc2a-2e90e7cee3e3")]
    [Bars.Rms.Core.Attributes.Uid("e0eea06f-d065-4925-bc2a-2e90e7cee3e3")]
    public class Test123 : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Test123(): base ()
        {
        }

        /// <summary>
        /// ФИО
        /// </summary>
        [Bars.B4.Utils.Display("ФИО")]
        [Bars.Rms.Core.Attributes.Uid("719af634-f464-45d3-8db1-82b994fb2e49")]
        public virtual System.String name1234
        {
            get;
            set;
        }
    }
}