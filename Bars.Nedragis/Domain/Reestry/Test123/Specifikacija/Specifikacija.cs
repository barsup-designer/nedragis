namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Спецификация
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Спецификация")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("640f4644-e8f4-4163-978a-3065538260cc")]
    [Bars.Rms.Core.Attributes.Uid("640f4644-e8f4-4163-978a-3065538260cc")]
    public class Specifikacija : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Specifikacija(): base ()
        {
        }

        /// <summary>
        /// Номер спецификации
        /// </summary>
        [Bars.B4.Utils.Display("Номер спецификации")]
        [Bars.Rms.Core.Attributes.Uid("6908ec42-aacc-41a3-b10f-81a6105a3679")]
        public virtual System.Int32 nomsp
        {
            get;
            set;
        }

        /// <summary>
        /// Дата спецификации
        /// </summary>
        [Bars.B4.Utils.Display("Дата спецификации")]
        [Bars.Rms.Core.Attributes.Uid("f3ffae9c-bc3b-4c33-b690-feb28490986d")]
        public virtual System.DateTime Element1478601989143
        {
            get;
            set;
        }

        /// <summary>
        /// Количество
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("b8318736-6d63-4c84-a4a2-8ebf0d05b809")]
        public virtual System.Int32 Element1478602041541
        {
            get;
            set;
        }

        /// <summary>
        /// Итоговая сумма
        /// </summary>
        [Bars.B4.Utils.Display("Итоговая сумма")]
        [Bars.Rms.Core.Attributes.Uid("0e196576-a92d-4f2b-adc7-bd34847f8563")]
        public virtual System.Decimal Element1478602079636
        {
            get;
            set;
        }

        /// <summary>
        /// Номер позиции спецификации
        /// </summary>
        [Bars.B4.Utils.Display("Номер позиции спецификации")]
        [Bars.Rms.Core.Attributes.Uid("9dabae63-3e8f-42fd-b76d-789867ac91c7")]
        public virtual System.Int32 Element1478602280592
        {
            get;
            set;
        }

        /// <summary>
        /// Номер заявки
        /// </summary>
        [Bars.B4.Utils.Display("Номер заявки")]
        [Bars.Rms.Core.Attributes.Uid("01006813-3c42-4c54-b53e-45c39e432b18")]
        public virtual System.String Element1478607128570
        {
            get;
            set;
        }

        /// <summary>
        /// Сотрудник
        /// </summary>
        [Bars.B4.Utils.Display("Сотрудник")]
        [Bars.Rms.Core.Attributes.Uid("2a0475fe-57db-4ba9-8f6d-1125bbf25706")]
        public virtual Bars.Nedragis.Sotrudniki Element1478607204176
        {
            get;
            set;
        }
    }
}