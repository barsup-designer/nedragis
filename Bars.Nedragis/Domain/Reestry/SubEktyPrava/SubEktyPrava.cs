namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Субъекты права
    /// Представляет собой общего предка для юрлиц и физлиц.
    /// </summary>
    [Bars.B4.Utils.Display(@"Субъекты права")]
    [Bars.B4.Utils.Description(@"Представляет собой общего предка для юрлиц и физлиц.")]
    [System.Runtime.InteropServices.GuidAttribute("7f137d0e-e613-4277-8440-352925b3595a")]
    [Bars.Rms.Core.Attributes.Uid("7f137d0e-e613-4277-8440-352925b3595a")]
    public class SubEktyPrava : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public SubEktyPrava(): base ()
        {
        }

        /// <summary>
        /// Название субъекта
        /// </summary>
        [Bars.B4.Utils.Display("Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("6f8b2489-4290-4747-93ba-ff199586d3bb")]
        public virtual System.String Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние
        /// </summary>
        [Bars.B4.Utils.Display("Состояние")]
        [Bars.Rms.Core.Attributes.Uid("27a4dd90-efb8-4dfa-a0f7-82077f3f74b4")]
        public virtual Bars.Nedragis.StatusySubEkta Status
        {
            get;
            set;
        }

        /// <summary>
        /// Тип
        /// </summary>
        [Bars.B4.Utils.Display("Тип")]
        [Bars.Rms.Core.Attributes.Uid("2ab56c4c-0946-4fbd-a257-df55e5430428")]
        public virtual Bars.Nedragis.TipySubEktaPrava type
        {
            get;
            set;
        }

        /// <summary>
        /// ОГРН
        /// </summary>
        [Bars.B4.Utils.Display("ОГРН")]
        [Bars.Rms.Core.Attributes.Uid("a9e4705d-7b4f-4def-810f-122f0c5044c1")]
        public virtual System.String ogrn
        {
            get;
            set;
        }
    }
}