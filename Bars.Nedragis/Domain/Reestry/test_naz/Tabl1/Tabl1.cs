namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// табл1
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"табл1")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("5c6dabec-3287-45f8-95ad-3ccf9672f548")]
    [Bars.Rms.Core.Attributes.Uid("5c6dabec-3287-45f8-95ad-3ccf9672f548")]
    public class Tabl1 : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Tabl1(): base ()
        {
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("5ace9031-2b02-487e-b337-1a13a32efe9c")]
        public virtual Bars.Nedragis.VidRabot? vid_r
        {
            get;
            set;
        }

        /// <summary>
        /// count
        /// </summary>
        [Bars.B4.Utils.Display("count")]
        [Bars.Rms.Core.Attributes.Uid("b9880006-caef-49d3-8d5a-4373b8a239da")]
        public virtual System.Int32 count
        {
            get;
            set;
        }

        /// <summary>
        /// rowid
        /// </summary>
        [Bars.B4.Utils.Display("rowid")]
        [Bars.Rms.Core.Attributes.Uid("72bfcfca-9ec0-438c-9eb3-99cb7666cfc8")]
        public virtual System.Int64? rowid
        {
            get;
            set;
        }

        /// <summary>
        /// sum_DS
        /// </summary>
        [Bars.B4.Utils.Display("sum_DS")]
        [Bars.Rms.Core.Attributes.Uid("0e043bb4-3cdc-46b4-a1ab-3c78785b3db1")]
        public virtual System.Decimal sum_DS
        {
            get;
            set;
        }

        /// <summary>
        /// kok
        /// </summary>
        [Bars.B4.Utils.Display("kok")]
        [Bars.Rms.Core.Attributes.Uid("f646679c-a5ae-45cd-b17e-8a56f460c10d")]
        public virtual Bars.Nedragis.VidPrava kok
        {
            get;
            set;
        }
    }
}