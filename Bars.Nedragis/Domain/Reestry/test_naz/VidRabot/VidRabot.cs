namespace Bars.Nedragis
{
    using Bars.B4.Utils;
    using System.Collections.Generic;
    using System;

    /// <summary>
    /// Вид работ
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Вид работ")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("1fae474f-6d50-40d0-a50b-c39a4fc8b590")]
    [Bars.Rms.Core.Attributes.Uid("1fae474f-6d50-40d0-a50b-c39a4fc8b590")]
    public enum VidRabot
    {
        /// <summary>
        /// Каротаж		
        /// </summary>
        [Bars.B4.Utils.Display(@"Каротаж")]
        [Bars.B4.Utils.Description(@"")]
        Literal1 = 1,
        /// <summary>
        /// Сейсмика		
        /// </summary>
        [Bars.B4.Utils.Display(@"Сейсмика")]
        [Bars.B4.Utils.Description(@"")]
        Literal2 = 2,
        /// <summary>
        /// Еще		
        /// </summary>
        [Bars.B4.Utils.Display(@"Еще")]
        [Bars.B4.Utils.Description(@"Описание 1453876132599")]
        Literal1453876132599 = 3
    }
}