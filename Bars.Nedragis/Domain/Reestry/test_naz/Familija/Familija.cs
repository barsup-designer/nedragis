namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Фамилия
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Фамилия")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("09621ed4-44dc-41a9-8a10-34cc816cb4e4")]
    [Bars.Rms.Core.Attributes.Uid("09621ed4-44dc-41a9-8a10-34cc816cb4e4")]
    public class Familija : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Familija(): base ()
        {
        }

        /// <summary>
        /// Фамилия
        /// </summary>
        [Bars.B4.Utils.Display("Фамилия")]
        [Bars.Rms.Core.Attributes.Uid("939f8222-1f33-4c34-9678-5f24f345933a")]
        public virtual System.String Surname
        {
            get;
            set;
        }

        /// <summary>
        /// Имя
        /// </summary>
        [Bars.B4.Utils.Display("Имя")]
        [Bars.Rms.Core.Attributes.Uid("ed82e4bf-e4fe-4433-8af9-5faee73ee1ce")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}