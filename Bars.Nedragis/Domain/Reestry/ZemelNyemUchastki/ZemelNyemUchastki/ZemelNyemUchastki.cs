namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Земельные участки
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Земельные участки")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("6885fdfb-7ec7-4566-8395-5542b58c6305")]
    [Bars.Rms.Core.Attributes.Uid("6885fdfb-7ec7-4566-8395-5542b58c6305")]
    public class ZemelNyemUchastki : Bars.Nedragis.ObEktyPrava
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public ZemelNyemUchastki(): base ()
        {
            cad_okrug = 0;
            cad_raion = 0;
            cad_block = 0;
            cad_massiv = 0;
            cad_kvartal = 0;
            number_ZU = 0;
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("105ff081-e494-4ec2-be01-d5b976574ee2")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Категория земли
        /// </summary>
        [Bars.B4.Utils.Display("Категория земли")]
        [Bars.Rms.Core.Attributes.Uid("00e67e7c-cee8-4669-b13e-131cc1fb153c")]
        public virtual Bars.Nedragis.KategoriiZemel category
        {
            get;
            set;
        }

        /// <summary>
        /// Вид разрешенного использования
        /// </summary>
        [Bars.B4.Utils.Display("Вид разрешенного использования")]
        [Bars.Rms.Core.Attributes.Uid("dbf89b5c-94fe-42a3-8300-6ca22f8bfb16")]
        public virtual Bars.Nedragis.VRIZU vid_use
        {
            get;
            set;
        }

        /// <summary>
        /// Вид права на землю
        /// </summary>
        [Bars.B4.Utils.Display("Вид права на землю")]
        [Bars.Rms.Core.Attributes.Uid("e70e27a6-b885-42f3-94ba-cda4ef7e3427")]
        public virtual Bars.Nedragis.VidPravaNaZemlju vid_prava
        {
            get;
            set;
        }

        /// <summary>
        /// Общая площадь, кв. м
        /// </summary>
        [Bars.B4.Utils.Display("Общая площадь, кв. м")]
        [Bars.Rms.Core.Attributes.Uid("27a43f2d-8fd1-4e39-95ca-ace3fcd5c436")]
        public virtual System.Decimal area
        {
            get;
            set;
        }

        /// <summary>
        /// Назначение
        /// </summary>
        [Bars.B4.Utils.Display("Назначение")]
        [Bars.Rms.Core.Attributes.Uid("3cb61f3d-e6a5-42a9-b91c-4c15cfd1d5d1")]
        public virtual System.String naznach
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор ГИС
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор ГИС")]
        [Bars.Rms.Core.Attributes.Uid("ec2bd77e-9b6e-4045-adac-92776ae1818b")]
        public virtual System.Int64 MapID
        {
            get;
            set;
        }

        /// <summary>
        /// Кадастровый округ
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый округ")]
        [Bars.Rms.Core.Attributes.Uid("bcdba244-90f3-43da-8f0c-e9324fefa9f8")]
        public virtual System.Int32 cad_okrug
        {
            get;
            set;
        }

        /// <summary>
        /// Кадастровый район
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый район")]
        [Bars.Rms.Core.Attributes.Uid("1a946502-20c6-4082-a66b-52981adfd07a")]
        public virtual System.Int32 cad_raion
        {
            get;
            set;
        }

        /// <summary>
        /// Кадастровый блок
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый блок")]
        [Bars.Rms.Core.Attributes.Uid("2429c060-ddc2-44b3-a783-c27e86c42671")]
        public virtual System.Int32 cad_block
        {
            get;
            set;
        }

        /// <summary>
        /// Кадастровый массив
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый массив")]
        [Bars.Rms.Core.Attributes.Uid("67df656f-f029-41cc-a90b-1fce1e7749b4")]
        public virtual System.Int32 cad_massiv
        {
            get;
            set;
        }

        /// <summary>
        /// Кадастровый квартал
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый квартал")]
        [Bars.Rms.Core.Attributes.Uid("d56a7941-6d16-4d6b-9b27-0cb41a54bb84")]
        public virtual System.Int32 cad_kvartal
        {
            get;
            set;
        }

        /// <summary>
        /// Номер ЗУ
        /// </summary>
        [Bars.B4.Utils.Display("Номер ЗУ")]
        [Bars.Rms.Core.Attributes.Uid("d25e9206-5297-456c-9d48-b20b8936c26a")]
        public virtual System.Int32 number_ZU
        {
            get;
            set;
        }

        /// <summary>
        /// ОКАТО
        /// </summary>
        [Bars.B4.Utils.Display("ОКАТО")]
        [Bars.Rms.Core.Attributes.Uid("10f299b0-ddbb-45f5-b254-e2ac381832db")]
        public virtual Bars.Nedragis.OKATO okato
        {
            get;
            set;
        }

        /// <summary>
        /// ОКТМО
        /// </summary>
        [Bars.B4.Utils.Display("ОКТМО")]
        [Bars.Rms.Core.Attributes.Uid("7bbf2b28-e2de-4fdb-9a08-6846cd0465b2")]
        public virtual Bars.Nedragis.OKTMO oktmo
        {
            get;
            set;
        }

        /// <summary>
        /// Описание месторасположения границ
        /// </summary>
        [Bars.B4.Utils.Display("Описание месторасположения границ")]
        [Bars.Rms.Core.Attributes.Uid("86c9dc3c-3402-4971-8eab-48fe2bc2e8b5")]
        public virtual System.String Borders
        {
            get;
            set;
        }

        /// <summary>
        /// Кадастровый номер
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый номер")]
        [Bars.Rms.Core.Attributes.Uid("12badbbd-e1ea-40e7-b969-532cea517fcc")]
        public virtual System.String cadnum1
        {
            get;
            set;
        }

        private System.Collections.Generic.IList<Bars.Nedragis.DokumentyZU> _doc;
        /// <summary>
        /// Документ
        /// </summary>
        [Bars.B4.Utils.Display("Документ")]
        [Bars.Rms.Core.Attributes.Uid("8951d9cc-3b4f-49f2-bc80-5431fec6efa6")]
        public virtual System.Collections.Generic.IList<Bars.Nedragis.DokumentyZU> doc
        {
            get
            {
                return _doc ?? (_doc = new System.Collections.Generic.List<Bars.Nedragis.DokumentyZU>());
            }

            set
            {
                _doc = value;
            }
        }

        /// <summary>
        /// Обременения
        /// </summary>
        [Bars.B4.Utils.Display("Обременения")]
        [Bars.Rms.Core.Attributes.Uid("6e5513a9-9295-469c-8d57-2e603ae147d3")]
        public virtual Bars.Nedragis.VidOgranichenija bremya
        {
            get;
            set;
        }

        /// <summary>
        /// Тип земельного участка
        /// </summary>
        [Bars.B4.Utils.Display("Тип земельного участка")]
        [Bars.Rms.Core.Attributes.Uid("578018bd-9082-4fe3-9b50-aaea272d8f84")]
        public virtual Bars.Nedragis.TipZemelNogoUchastka TypeZU
        {
            get;
            set;
        }
    }
}