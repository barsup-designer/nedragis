namespace Bars.Nedragis
{
    using Bars.B4.Utils;
    using System.Collections.Generic;
    using System;

    /// <summary>
    /// test2
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"test2")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("66114bbb-dc35-407e-9205-98f61b79f7a0")]
    [Bars.Rms.Core.Attributes.Uid("66114bbb-dc35-407e-9205-98f61b79f7a0")]
    public enum Test2
    {
        /// <summary>
        /// Литерал 1		
        /// </summary>
        [Bars.B4.Utils.Display(@"Литерал 1")]
        [Bars.B4.Utils.Description(@"")]
        Literal1 = 0,
        /// <summary>
        /// Литерал 2		
        /// </summary>
        [Bars.B4.Utils.Display(@"Литерал 2")]
        [Bars.B4.Utils.Description(@"")]
        Literal2 = 1
    }
}