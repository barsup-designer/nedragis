namespace Bars.Nedragis
{
    using Bars.B4.Utils;
    using System.Collections.Generic;
    using System;

    /// <summary>
    /// Пол
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Пол")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("d1fc21fc-3f4b-4ec2-a379-3c3ea8821bcf")]
    [Bars.Rms.Core.Attributes.Uid("d1fc21fc-3f4b-4ec2-a379-3c3ea8821bcf")]
    public enum Pol
    {
        /// <summary>
        /// Мужской		
        /// </summary>
        [Bars.B4.Utils.Display(@"Мужской")]
        [Bars.B4.Utils.Description(@"")]
        Male = 1,
        /// <summary>
        /// Женский		
        /// </summary>
        [Bars.B4.Utils.Display(@"Женский")]
        [Bars.B4.Utils.Description(@"")]
        Female = 2
    }
}