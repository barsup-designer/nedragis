namespace Bars.Nedragis
{
    using Bars.B4.Utils;
    using System.Collections.Generic;
    using System;

    /// <summary>
    /// Формат
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Формат")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("3b75c246-ee63-40c5-9104-77ef30ee6c6b")]
    [Bars.Rms.Core.Attributes.Uid("3b75c246-ee63-40c5-9104-77ef30ee6c6b")]
    public enum Format
    {
        /// <summary>
        /// Текстовое значение		
        /// </summary>
        [Bars.B4.Utils.Display(@"Текстовое значение")]
        [Bars.B4.Utils.Description(@"")]
        Text = 1,
        /// <summary>
        /// Денежное значение		
        /// </summary>
        [Bars.B4.Utils.Display(@"Денежное значение")]
        [Bars.B4.Utils.Description(@"")]
        Money = 2,
        /// <summary>
        /// Целое значение		
        /// </summary>
        [Bars.B4.Utils.Display(@"Целое значение")]
        [Bars.B4.Utils.Description(@"Описание 1443521875968")]
        Integer = 3,
        /// <summary>
        /// Дата		
        /// </summary>
        [Bars.B4.Utils.Display(@"Дата")]
        [Bars.B4.Utils.Description(@"Описание 1443521930261")]
        Date = 4,
        /// <summary>
        /// Вещественное значение		
        /// </summary>
        [Bars.B4.Utils.Display(@"Вещественное значение")]
        [Bars.B4.Utils.Description(@"Описание 1443521957027")]
        Real = 5,
        /// <summary>
        /// Булевское значение		
        /// </summary>
        [Bars.B4.Utils.Display(@"Булевское значение")]
        [Bars.B4.Utils.Description(@"Описание 1443521997605")]
        Boolean = 6
    }
}