-- NumberTest.sql
-- NumberTest

CREATE OR REPLACE FUNCTION NumberTest(integer)
RETURNS integer AS $$
select $1
$$ LANGUAGE 'sql' VOLATILE;
COMMENT ON FUNCTION NumberTest(integer) is 'NumberTest';
