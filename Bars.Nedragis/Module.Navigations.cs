namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;
    using Bars.B4.ResourceBundling;

    public partial class Module
    {
        protected virtual void RegisterNavigationProviders()
        {
            Component.For<Bars.B4.IClientRouteMapRegistrar>().ImplementedBy<Bars.Nedragis.MenjuClientRoute>().LifestyleTransient().RegisterIn(Container);
            Container.RegisterSessionScoped<Bars.Rms.GeneratedApp.Navigation.INavigationPanelNodesProvider, Bars.Nedragis.Navigation.Providers.NavContainer509910caf47b4029b06a8a5b91d4cdc0Provider>();
            Container.RegisterSessionScoped<Bars.Rms.GeneratedApp.Navigation.INavigationPanelNodesProvider, Bars.Nedragis.Navigation.Providers.NavContainercd41d47b6abf4c2a8d0ec68d48bfb98aProvider>();
            Container.RegisterSessionScoped<Bars.Rms.GeneratedApp.Navigation.INavigationPanelNodesProvider, Bars.Nedragis.Navigation.Providers.NavContainerc6aac255359b447b8bb9101dcae25c36Provider>();
            Component.For<Bars.B4.IClientRouteMapRegistrar>().ImplementedBy<Bars.Nedragis.ActionClientRoute>().LifestyleTransient().RegisterIn(Container);
            Container.RegisterSessionScoped<Bars.Rms.GeneratedApp.Navigation.INavigationPanelNodesProvider, Bars.Nedragis.Navigation.Providers.NavContainer1b976d4c8d774a428a2e6a919a934a8bProvider>();
            Container.RegisterSessionScoped<Bars.Rms.GeneratedApp.Navigation.INavigationPanelNodesProvider, Bars.Nedragis.Navigation.Providers.NavContainerdf950a5115124d0981e0b31e091eddccProvider>();
            Container.RegisterSessionScoped<Bars.Rms.GeneratedApp.Navigation.INavigationPanelNodesProvider, Bars.Nedragis.Navigation.Providers.NavContainer5d03704ccb054f0b9663371e87c63d7fProvider>();
            Container.RegisterSessionScoped<Bars.Rms.GeneratedApp.Navigation.INavigationPanelNodesProvider, Bars.Nedragis.Navigation.Providers.NavContainer5304374960bb4972b036e9a6a5d4cb67Provider>();
            Container.RegisterSessionScoped<Bars.Rms.GeneratedApp.Navigation.INavigationPanelNodesProvider, Bars.Nedragis.Navigation.Providers.NavContainer411d74e8493a4fc49e66c64624a2292fProvider>();
            Container.RegisterNavigationProvider<Bars.Nedragis.MenjuNavigation>();
        }
    }
}