Ext.define('B4.CustomVTypes', {
    singleton: true,
    requires: ['Ext.form.field.VTypes'],
    constructor: function() {
        Ext.form.field.VTypes['tel'] = function(value, field) {
            var regexp = /^\([0-9][0-9][0-9]\)[0-9][0-9]-[0-9][0-9]-[0-9][0-9]$/g;
            return regexp.test(value);
        };
        Ext.form.field.VTypes['telText'] = 'Error';
        Ext.form.field.VTypes['inn'] = function(value, field) {
            var regexp = /^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/g;
            return regexp.test(value);
        };
    }
});
Ext.override(Ext.window.MessageBox, {
    autoScroll: true,
    alert: function(cfg, msg, fn, scope) {
        if (Ext.isString(cfg)) {
            cfg = {
                title: cfg,
                msg: msg,
                buttons: this.OK,
                fn: fn,
                scope: scope,
                minWidth: this.minWidth
            };
        }
        return this.show(cfg);
    }
});