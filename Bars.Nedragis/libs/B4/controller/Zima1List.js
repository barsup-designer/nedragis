Ext.define('B4.controller.Zima1List', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.Zima1List',
        'B4.model.Zima1ListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-zima1list',
    viewDataName: 'Zima1List',
    // набор описаний действий реестра
    actions: {
        'Addition-Zima1Editor-InWindow': {
            'editor': 'Zima1Editor',
            'editorAlias': 'rms-zima1editor',
            'editorUid': '824ad90e-cbef-4118-8aae-00892a1c6721',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-Zima1Editor-InWindow': {
            'editor': 'Zima1Editor',
            'editorAlias': 'rms-zima1editor',
            'editorUid': '824ad90e-cbef-4118-8aae-00892a1c6721',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-zima1list': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-Zima1Editor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-zima1list': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});