Ext.define('B4.controller.OKOGU1', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OKOGU1',
        'B4.model.OKOGU1Model'],
    // псевдоним класса реестра
    viewAlias: 'rms-okogu1',
    viewDataName: 'OKOGU1',
    // набор описаний действий реестра
    actions: {
        'Addition-OKOGUEditor-InWindow': {
            'editor': 'OKOGUEditor',
            'editorAlias': 'rms-okogueditor',
            'editorUid': '06c495f0-1243-44f5-bbc1-cb45263105a3',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 250,
                'width': 500
            }
        },
        'Editing-OKOGUEditor-InWindow': {
            'editor': 'OKOGUEditor',
            'editorAlias': 'rms-okogueditor',
            'editorUid': '06c495f0-1243-44f5-bbc1-cb45263105a3',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 250,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-okogu1': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OKOGUEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-okogu1': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});