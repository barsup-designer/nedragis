Ext.define('B4.controller.OKOGUList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OKOGUList',
        'B4.model.OKOGUListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-okogulist',
    viewDataName: 'OKOGUList',
    // набор описаний действий реестра
    actions: {
        'Addition-OKOGUEditor-InWindow': {
            'editor': 'OKOGUEditor',
            'editorAlias': 'rms-okogueditor',
            'editorUid': '06c495f0-1243-44f5-bbc1-cb45263105a3',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 250,
                'width': 500
            }
        },
        'Editing-OKOGUEditor-InWindow': {
            'editor': 'OKOGUEditor',
            'editorAlias': 'rms-okogueditor',
            'editorUid': '06c495f0-1243-44f5-bbc1-cb45263105a3',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 250,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-okogulist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OKOGUEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-okogulist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['parent_id_Id'])) {
            this.hideColumnByDataIndex('parent_id_name', true, view);
        }
    },
});