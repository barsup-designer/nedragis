Ext.define('B4.controller.DokumentyUdostoverjajushhieLichnostList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.DokumentyUdostoverjajushhieLichnostList',
        'B4.model.DokumentyUdostoverjajushhieLichnostListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-dokumentyudostoverjajushhielichnostlist',
    viewDataName: 'DokumentyUdostoverjajushhieLichnostList',
    // набор описаний действий реестра
    actions: {
        'Addition-DokumentyUdostoverjajushhieLichnostEditor-InWindow': {
            'editor': 'DokumentyUdostoverjajushhieLichnostEditor',
            'editorAlias': 'rms-dokumentyudostoverjajushhielichnosteditor',
            'editorUid': 'ad3a4cd5-6eb7-434b-8221-8ea081492761',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 185,
                'width': 500
            }
        },
        'Editing-DokumentyUdostoverjajushhieLichnostEditor-InWindow': {
            'editor': 'DokumentyUdostoverjajushhieLichnostEditor',
            'editorAlias': 'rms-dokumentyudostoverjajushhielichnosteditor',
            'editorUid': 'ad3a4cd5-6eb7-434b-8221-8ea081492761',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 185,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-dokumentyudostoverjajushhielichnostlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-DokumentyUdostoverjajushhieLichnostEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-dokumentyudostoverjajushhielichnostlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});