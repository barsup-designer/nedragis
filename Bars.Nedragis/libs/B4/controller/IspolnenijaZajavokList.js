Ext.define('B4.controller.IspolnenijaZajavokList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.IspolnenijaZajavokList',
        'B4.model.IspolnenijaZajavokListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-ispolnenijazajavoklist',
    viewDataName: 'IspolnenijaZajavokList',
    // набор описаний действий реестра
    actions: {
        'Addition-IspolnenijaZajavokEditor-InWindow': {
            'editor': 'IspolnenijaZajavokEditor',
            'editorAlias': 'rms-ispolnenijazajavokeditor',
            'editorUid': '2a6776c4-56fe-4d20-9a51-20db8e151e80',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-IspolnenijaZajavokEditor-InWindow': {
            'editor': 'IspolnenijaZajavokEditor',
            'editorAlias': 'rms-ispolnenijazajavokeditor',
            'editorUid': '2a6776c4-56fe-4d20-9a51-20db8e151e80',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-ispolnenijazajavoklist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-IspolnenijaZajavokEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-ispolnenijazajavoklist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['Element1511506563562_Id'])) {
            this.hideColumnByDataIndex('reestrmo456', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Element1511506733063_Id'])) {
            this.hideColumnByDataIndex('reestrname456', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Element1511762285082_Id'])) {
            this.hideColumnByDataIndex('reestrpr', true, view);
        }
    },
});