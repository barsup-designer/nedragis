Ext.define('B4.controller.OtdelPodgotovkiInformaciiList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OtdelPodgotovkiInformaciiList',
        'B4.model.OtdelPodgotovkiInformaciiListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-otdelpodgotovkiinformaciilist',
    viewDataName: 'OtdelPodgotovkiInformaciiList',
    // набор описаний действий реестра
    actions: {
        'Addition-OtdelPodgotovkiInformaciiEditor-InWindow': {
            'editor': 'OtdelPodgotovkiInformaciiEditor',
            'editorAlias': 'rms-otdelpodgotovkiinformaciieditor',
            'editorUid': 'dc3b457d-7646-4047-979a-9d37d1e8d355',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 700,
                'width': 700
            }
        },
        'Editing-OtdelPodgotovkiInformaciiEditor-InWindow': {
            'editor': 'OtdelPodgotovkiInformaciiEditor',
            'editorAlias': 'rms-otdelpodgotovkiinformaciieditor',
            'editorUid': 'dc3b457d-7646-4047-979a-9d37d1e8d355',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 700,
                'width': 700
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-otdelpodgotovkiinformaciilist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OtdelPodgotovkiInformaciiEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-otdelpodgotovkiinformaciilist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['Element1510135000178_Id'])) {
            this.hideColumnByDataIndex('Element1510135000178_VidRabotOPINF222', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Element1510135042374_Id'])) {
            this.hideColumnByDataIndex('Element1510135042374_Element1510134562537', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Element1510135121705_Id'])) {
            this.hideColumnByDataIndex('Element1510135121705_Element1474352136232', true, view);
        }
    },
});