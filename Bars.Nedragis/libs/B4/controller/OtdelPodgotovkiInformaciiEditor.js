Ext.define('B4.controller.OtdelPodgotovkiInformaciiEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'OtdelPodgotovkiInformaciiEditorModel'],
    views: [
        'OtdelPodgotovkiInformaciiEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.OtdelPodgotovkiInformaciiEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-otdelpodgotovkiinformaciieditor',
    viewDataModel: 'OtdelPodgotovkiInformaciiEditorModel',
    viewDataController: 'OtdelPodgotovkiInformaciiEditor',
    viewDataName: 'OtdelPodgotovkiInformaciiEditor',
    init: function() {
        var me = this;
        me.callParent(arguments);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['Element1510135000178_Id'])) {
            this.preventFieldInput(form, ['[name=Element1510135000178]']);
        }
        if (!Ext.isEmpty(ctxParams['Element1510135042374_Id'])) {
            this.preventFieldInput(form, ['[name=Element1510135042374]']);
        }
        if (!Ext.isEmpty(ctxParams['Element1510135121705_Id'])) {
            this.preventFieldInput(form, ['[name=Element1510135121705]']);
        }
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});