Ext.define('B4.controller.TipDogovoraArendyList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.TipDogovoraArendyList',
        'B4.model.TipDogovoraArendyListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-tipdogovoraarendylist',
    viewDataName: 'TipDogovoraArendyList',
    // набор описаний действий реестра
    actions: {
        'Addition-TipDogovoraArendyEditor-InWindow': {
            'editor': 'TipDogovoraArendyEditor',
            'editorAlias': 'rms-tipdogovoraarendyeditor',
            'editorUid': '640fab8a-79e9-4ed6-91be-47f36f8209e9',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-TipDogovoraArendyEditor-InWindow': {
            'editor': 'TipDogovoraArendyEditor',
            'editorAlias': 'rms-tipdogovoraarendyeditor',
            'editorUid': '640fab8a-79e9-4ed6-91be-47f36f8209e9',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-tipdogovoraarendylist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-TipDogovoraArendyEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-tipdogovoraarendylist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});