Ext.define('B4.controller.VidOshibkiList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.VidOshibkiList',
        'B4.model.VidOshibkiListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-vidoshibkilist',
    viewDataName: 'VidOshibkiList',
    // набор описаний действий реестра
    actions: {
        'Addition-VidOshibkiEditor-InWindow': {
            'editor': 'VidOshibkiEditor',
            'editorAlias': 'rms-vidoshibkieditor',
            'editorUid': '7f01485a-5358-4a23-bba3-4a7832bff84a',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-VidOshibkiEditor-InWindow': {
            'editor': 'VidOshibkiEditor',
            'editorAlias': 'rms-vidoshibkieditor',
            'editorUid': '7f01485a-5358-4a23-bba3-4a7832bff84a',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-vidoshibkilist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-VidOshibkiEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-vidoshibkilist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});