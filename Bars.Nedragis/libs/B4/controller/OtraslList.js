Ext.define('B4.controller.OtraslList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OtraslList',
        'B4.model.OtraslListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-otrasllist',
    viewDataName: 'OtraslList',
    // набор описаний действий реестра
    actions: {
        'Addition-OtraslEditor-InWindow': {
            'editor': 'OtraslEditor',
            'editorAlias': 'rms-otrasleditor',
            'editorUid': '7ecc5ddc-2d0e-46f5-a6e9-058b5a79fe72',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-OtraslEditor-InWindow': {
            'editor': 'OtraslEditor',
            'editorAlias': 'rms-otrasleditor',
            'editorUid': '7ecc5ddc-2d0e-46f5-a6e9-058b5a79fe72',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-otrasllist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OtraslEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-otrasllist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});