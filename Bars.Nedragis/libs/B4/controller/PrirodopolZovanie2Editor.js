Ext.define('B4.controller.PrirodopolZovanie2Editor', {
    extend: 'B4.base.form.Controller',
    models: [
        'PrirodopolZovanie2EditorModel'],
    views: [
        'PrirodopolZovanie2Editor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.PrirodopolZovanie2Editor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-prirodopolzovanie2editor',
    viewDataModel: 'PrirodopolZovanie2EditorModel',
    viewDataController: 'PrirodopolZovanie2Editor',
    viewDataName: 'PrirodopolZovanie2Editor',
    init: function() {
        var me = this;
        me.callParent(arguments);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['VidRabotPR2_Id'])) {
            this.preventFieldInput(form, ['[name=VidRabotPR2]']);
        }
        if (!Ext.isEmpty(ctxParams['IspolnitelPR2_Id'])) {
            this.preventFieldInput(form, ['[name=IspolnitelPR2]']);
        }
        if (!Ext.isEmpty(ctxParams['SoispolnitelPR2_Id'])) {
            this.preventFieldInput(form, ['[name=SoispolnitelPR2]']);
        }
        if (!Ext.isEmpty(ctxParams['MonthPR2_Id'])) {
            this.preventFieldInput(form, ['[name=MonthPR2]']);
        }
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});