Ext.define('B4.controller.ZemleustrList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.ZemleustrList',
        'B4.model.ZemleustrListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-zemleustrlist',
    viewDataName: 'ZemleustrList',
    // набор описаний действий реестра
    actions: {
        'Addition-ZemleustrEditor-InWindow': {
            'editor': 'ZemleustrEditor',
            'editorAlias': 'rms-zemleustreditor',
            'editorUid': '4dd905da-08f4-43d3-a5e1-a61dea0c71bc',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-ZemleustrEditor-InWindow': {
            'editor': 'ZemleustrEditor',
            'editorAlias': 'rms-zemleustreditor',
            'editorUid': '4dd905da-08f4-43d3-a5e1-a61dea0c71bc',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-zemleustrlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-ZemleustrEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-zemleustrlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['name_z_Id'])) {
            this.hideColumnByDataIndex('name_z_NAME_SP', true, view);
        }
        if (!Ext.isEmpty(ctxParams['region_Id'])) {
            this.hideColumnByDataIndex('region_name', true, view);
        }
        if (!Ext.isEmpty(ctxParams['autor_Id'])) {
            this.hideColumnByDataIndex('autor_Element1474352136232', true, view);
        }
    },
});