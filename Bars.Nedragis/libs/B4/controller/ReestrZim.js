Ext.define('B4.controller.ReestrZim', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.ReestrZim',
        'B4.model.ReestrZimModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-reestrzim',
    viewDataName: 'ReestrZim',
    // набор описаний действий реестра
    actions: {
        'Addition-FormaZim-InWindow': {
            'editor': 'FormaZim',
            'editorAlias': 'rms-formazim',
            'editorUid': '8bfce9cc-8f53-4d1f-8ad8-c1fec6964744',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow'
        },
        'Editing-FormaZim-InWindow': {
            'editor': 'FormaZim',
            'editorAlias': 'rms-formazim',
            'editorUid': '8bfce9cc-8f53-4d1f-8ad8-c1fec6964744',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-reestrzim': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-FormaZim-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-reestrzim': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['Element1517482864074_Id'])) {
            this.hideColumnByDataIndex('zzz', true, view);
        }
    },
});