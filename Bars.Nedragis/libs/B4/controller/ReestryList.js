Ext.define('B4.controller.ReestryList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.ReestryList',
        'B4.model.ReestryListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-reestrylist',
    viewDataName: 'ReestryList',
    // набор описаний действий реестра
    actions: {
        'Addition-ReestryEditor-InWindow': {
            'editor': 'ReestryEditor',
            'editorAlias': 'rms-reestryeditor',
            'editorUid': 'c033f698-9e04-4dbd-840e-bd4652ec3132',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-ReestryEditor-InWindow': {
            'editor': 'ReestryEditor',
            'editorAlias': 'rms-reestryeditor',
            'editorUid': 'c033f698-9e04-4dbd-840e-bd4652ec3132',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-reestrylist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-ReestryEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-reestrylist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});