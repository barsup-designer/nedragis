Ext.define('B4.controller.DokumentyZUList2', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.DokumentyZUList2',
        'B4.model.DokumentyZUList2Model'],
    // псевдоним класса реестра
    viewAlias: 'rms-dokumentyzulist2',
    viewDataName: 'DokumentyZUList2',
    // набор описаний действий реестра
    actions: {
        'Addition-DokumentyZUEditor-InWindow': {
            'editor': 'DokumentyZUEditor',
            'editorAlias': 'rms-dokumentyzueditor',
            'editorUid': '1ae324f2-320e-42e8-9df3-df8484def876',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 155,
                'width': 500
            }
        },
        'Editing-DokumentyZUEditor3-InWindow': {
            'editor': 'DokumentyZUEditor3',
            'editorAlias': 'rms-dokumentyzueditor3',
            'editorUid': 'b2dea0bc-a377-4cfc-897f-60f37a6211b4',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 480,
                'width': 600
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-dokumentyzulist2': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-DokumentyZUEditor3-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-dokumentyzulist2': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['doc1_Type_Id'])) {
            this.hideColumnByDataIndex('doc1_Type_name', true, view);
        }
        if (!Ext.isEmpty(ctxParams['doc1_Id'])) {
            this.hideColumnByDataIndex('doc1_Number', true, view);
            this.hideColumnByDataIndex('doc1_Date', true, view);
            this.hideColumnByDataIndex('doc1_ActualDate', true, view);
            this.hideColumnByDataIndex('doc1_Type_name', true, view);
            this.hideColumnByDataIndex('doc1_Subject_Representation', true, view);
            this.hideColumnByDataIndex('doc1_Signatory_Representation', true, view);
        }
        if (!Ext.isEmpty(ctxParams['doc1_Subject_Id'])) {
            this.hideColumnByDataIndex('doc1_Subject_Representation', true, view);
        }
        if (!Ext.isEmpty(ctxParams['zemuch_Id'])) {
            this.hideColumnByDataIndex('zemuch_Id', true, view);
        }
        if (!Ext.isEmpty(ctxParams['doc1_Signatory_Id'])) {
            this.hideColumnByDataIndex('doc1_Signatory_Representation', true, view);
        }
    },
});