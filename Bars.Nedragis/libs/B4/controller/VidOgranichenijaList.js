Ext.define('B4.controller.VidOgranichenijaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.VidOgranichenijaList',
        'B4.model.VidOgranichenijaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-vidogranichenijalist',
    viewDataName: 'VidOgranichenijaList',
    // набор описаний действий реестра
    actions: {
        'Addition-VidOgranichenijaEditor-InWindow': {
            'editor': 'VidOgranichenijaEditor',
            'editorAlias': 'rms-vidogranichenijaeditor',
            'editorUid': '9f96466d-2660-46cb-bb39-52553bf240fd',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 150,
                'width': 500
            }
        },
        'Editing-VidOgranichenijaEditor-InWindow': {
            'editor': 'VidOgranichenijaEditor',
            'editorAlias': 'rms-vidogranichenijaeditor',
            'editorUid': '9f96466d-2660-46cb-bb39-52553bf240fd',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 150,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-vidogranichenijalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-VidOgranichenijaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-vidogranichenijalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});