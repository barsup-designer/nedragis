Ext.define('B4.controller.Otdel1List', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.Otdel1List',
        'B4.model.Otdel1ListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-otdel1list',
    viewDataName: 'Otdel1List',
    // набор описаний действий реестра
    actions: {
        'Addition-Otdel1Editor-InWindow': {
            'editor': 'Otdel1Editor',
            'editorAlias': 'rms-otdel1editor',
            'editorUid': '971e2167-b0d8-4d69-a2de-c8e374329bb4',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-Otdel1Editor-InWindow': {
            'editor': 'Otdel1Editor',
            'editorAlias': 'rms-otdel1editor',
            'editorUid': '971e2167-b0d8-4d69-a2de-c8e374329bb4',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-otdel1list': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-Otdel1Editor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-otdel1list': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});