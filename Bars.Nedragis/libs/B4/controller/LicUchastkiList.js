Ext.define('B4.controller.LicUchastkiList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.LicUchastkiList',
        'B4.model.LicUchastkiListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-licuchastkilist',
    viewDataName: 'LicUchastkiList',
    // набор описаний действий реестра
    actions: {
        'Addition-LicUchastkiEditor-InWindow': {
            'editor': 'LicUchastkiEditor',
            'editorAlias': 'rms-licuchastkieditor',
            'editorUid': '420d1621-1a7f-404d-99b6-fa3b4a6b0347',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-LicUchastkiEditor-InWindow': {
            'editor': 'LicUchastkiEditor',
            'editorAlias': 'rms-licuchastkieditor',
            'editorUid': '420d1621-1a7f-404d-99b6-fa3b4a6b0347',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-licuchastkilist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-LicUchastkiEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-licuchastkilist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});