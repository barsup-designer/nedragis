Ext.define('B4.controller.NedropolZovatelList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.NedropolZovatelList',
        'B4.model.NedropolZovatelListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-nedropolzovatellist',
    viewDataName: 'NedropolZovatelList',
    // набор описаний действий реестра
    actions: {
        'Addition-NedropolZovatelEditor-InWindow': {
            'editor': 'NedropolZovatelEditor',
            'editorAlias': 'rms-nedropolzovateleditor',
            'editorUid': '4cd9045d-1da6-4042-8dfd-32785bc509b8',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-NedropolZovatelEditor-InWindow': {
            'editor': 'NedropolZovatelEditor',
            'editorAlias': 'rms-nedropolzovateleditor',
            'editorUid': '4cd9045d-1da6-4042-8dfd-32785bc509b8',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-nedropolzovatellist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-NedropolZovatelEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-nedropolzovatellist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});