Ext.define('B4.controller.NedropolZovanieList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.NedropolZovanieList',
        'B4.model.NedropolZovanieListModel',
        'B4.aspects.Permission'],
    // псевдоним класса реестра
    viewAlias: 'rms-nedropolzovanielist',
    viewDataName: 'NedropolZovanieList',
    // набор описаний действий реестра
    actions: {
        'Addition-NedropolZovanieEditor-InWindow': {
            'editor': 'NedropolZovanieEditor',
            'editorAlias': 'rms-nedropolzovanieeditor',
            'editorUid': 'f6940730-dfdf-4248-9827-e0aa0837dc71',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-NedropolZovanieEditor-InWindow': {
            'editor': 'NedropolZovanieEditor',
            'editorAlias': 'rms-nedropolzovanieeditor',
            'editorUid': 'f6940730-dfdf-4248-9827-e0aa0837dc71',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [{
        'permissions': [{
            'applyBy': function(component, allowed) {
                component.setVisible(allowed);
            },
            'applyOn': {
                'selector': 'nedropolzovanielist'
            },
            'applyTo': 'rms-nedropolzovanielist #Addition-NedropolZovanieEditor-InWindow',
            'name': 'razmodultest.menytest.razreestrnedra1.Create'
        }, {
            'applyBy': function(component, allowed) {
                component.setVisible(allowed);
            },
            'applyOn': {
                'selector': 'nedropolzovanielist'
            },
            'applyTo': 'rms-nedropolzovanielist #Editing-NedropolZovanieEditor-InWindow',
            'name': 'razmodultest.menytest.razreestrnedra1.Read'
        }, {
            'applyBy': function(component, allowed) {
                component.setVisible(allowed);
            },
            'applyOn': {
                'selector': 'nedropolzovanielist'
            },
            'applyTo': 'rms-nedropolzovanielist #Deletion',
            'name': 'razmodultest.menytest.razreestrnedra1.Delete'
        }],
        'xtype': 'permissionaspect'
    }],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-nedropolzovanielist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-NedropolZovanieEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-nedropolzovanielist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['name_n_Id'])) {
            this.hideColumnByDataIndex('name_n_NAME_SP', true, view);
        }
        if (!Ext.isEmpty(ctxParams['sotr_n_Id'])) {
            this.hideColumnByDataIndex('sotr_n_Element1474352136232', true, view);
        }
    },
});