Ext.define('B4.controller.MagnitNositeliList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.MagnitNositeliList',
        'B4.model.MagnitNositeliListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-magnitnositelilist',
    viewDataName: 'MagnitNositeliList',
    // набор описаний действий реестра
    actions: {
        'Addition-MagnitNositeliEditor-InWindow': {
            'editor': 'MagnitNositeliEditor',
            'editorAlias': 'rms-magnitnositelieditor',
            'editorUid': 'e235f18e-ccdd-403c-8a6f-0a2f3542ad8f',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-MagnitNositeliEditor-InWindow': {
            'editor': 'MagnitNositeliEditor',
            'editorAlias': 'rms-magnitnositelieditor',
            'editorUid': 'e235f18e-ccdd-403c-8a6f-0a2f3542ad8f',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-magnitnositelilist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-MagnitNositeliEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-magnitnositelilist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});