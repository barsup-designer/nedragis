Ext.define('B4.controller.DolzhnostiList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.DolzhnostiList',
        'B4.model.DolzhnostiListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-dolzhnostilist',
    viewDataName: 'DolzhnostiList',
    // набор описаний действий реестра
    actions: {
        'Addition-DolzhnostiEditor-InWindow': {
            'editor': 'DolzhnostiEditor',
            'editorAlias': 'rms-dolzhnostieditor',
            'editorUid': '63691359-e50a-418a-b0fe-9804ec7ac373',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-DolzhnostiEditor-InWindow': {
            'editor': 'DolzhnostiEditor',
            'editorAlias': 'rms-dolzhnostieditor',
            'editorUid': '63691359-e50a-418a-b0fe-9804ec7ac373',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-dolzhnostilist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-DolzhnostiEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-dolzhnostilist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});