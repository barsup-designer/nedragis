Ext.define('B4.controller.ProektyList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.ProektyList',
        'B4.model.ProektyListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-proektylist',
    viewDataName: 'ProektyList',
    // набор описаний действий реестра
    actions: {
        'Addition-ProektyEditor-InWindow': {
            'editor': 'ProektyEditor',
            'editorAlias': 'rms-proektyeditor',
            'editorUid': '16a3a0ff-c3fe-4109-996a-e5478a02e3ba',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-ProektyEditor-InWindow': {
            'editor': 'ProektyEditor',
            'editorAlias': 'rms-proektyeditor',
            'editorUid': '16a3a0ff-c3fe-4109-996a-e5478a02e3ba',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-proektylist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-ProektyEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-proektylist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});