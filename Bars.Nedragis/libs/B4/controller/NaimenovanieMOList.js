Ext.define('B4.controller.NaimenovanieMOList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.NaimenovanieMOList',
        'B4.model.NaimenovanieMOListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-naimenovaniemolist',
    viewDataName: 'NaimenovanieMOList',
    // набор описаний действий реестра
    actions: {
        'Addition-NaimenovanieMOEditor-InWindow': {
            'editor': 'NaimenovanieMOEditor',
            'editorAlias': 'rms-naimenovaniemoeditor',
            'editorUid': '8f98ff79-ba93-4391-be7a-2d77977fccae',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-NaimenovanieMOEditor-InWindow': {
            'editor': 'NaimenovanieMOEditor',
            'editorAlias': 'rms-naimenovaniemoeditor',
            'editorUid': '8f98ff79-ba93-4391-be7a-2d77977fccae',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-naimenovaniemolist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-NaimenovanieMOEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-naimenovaniemolist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});