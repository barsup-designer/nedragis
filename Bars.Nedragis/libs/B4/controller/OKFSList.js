Ext.define('B4.controller.OKFSList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OKFSList',
        'B4.model.OKFSListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-okfslist',
    viewDataName: 'OKFSList',
    // набор описаний действий реестра
    actions: {
        'Addition-OKFSEditor-InWindow': {
            'editor': 'OKFSEditor',
            'editorAlias': 'rms-okfseditor',
            'editorUid': 'ee917c97-23f8-4623-9af4-6d051f92a997',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 190,
                'width': 600
            }
        },
        'Editing-OKFSEditor-InWindow': {
            'editor': 'OKFSEditor',
            'editorAlias': 'rms-okfseditor',
            'editorUid': 'ee917c97-23f8-4623-9af4-6d051f92a997',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 190,
                'width': 600
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-okfslist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OKFSEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-okfslist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});