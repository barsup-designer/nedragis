Ext.define('B4.controller.ReestrObEktovEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'ReestrObEktovEditorModel'],
    views: [
        'ReestrObEktovEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.ReestrObEktovEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-reestrobektoveditor',
    viewDataModel: 'ReestrObEktovEditorModel',
    viewDataController: 'ReestrObEktovEditor',
    viewDataName: 'ReestrObEktovEditor',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            '[rmsUid=17fcb539-7acf-4f48-8cfb-12b62076e1cf]': {
                'change': function(sender, newValue, oldValue) {}
            }
        });
        me.callParent(arguments);
        this.initControllers(['B4.controller.ObEktopiList2', 'B4.controller.RegionalNyjjNadzorVodnykhObEktovList']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Реестр объектопи2
        element = form.down('[name=ObEktopiList2]');
        ctrl = me.getController('B4.controller.ObEktopiList2');
        element.data.set('Element1508136885094_Id', rec.get('Id') || 0);
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(element);
        // Реестр Региональный надзор водных объектов
        element = form.down('[name=RegionalNyjjNadzorVodnykhObEktovList]');
        ctrl = me.getController('B4.controller.RegionalNyjjNadzorVodnykhObEktovList');
        element.data.set('Element1508150872914_Id', rec.get('Id') || 0);
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['Element1507784253669_Id'])) {
            this.preventFieldInput(form, ['[name=Element1507784253669]']);
        }
        if (isNewRecord == true) {
            form.grid_ObEktopiList2.disableGrid();
            form.grid_RegionalNyjjNadzorVodnykhObEktovList.disableGrid();
        } else {
            form.grid_ObEktopiList2.enable();
            form.grid_ObEktopiList2.getStore().load();
            form.grid_RegionalNyjjNadzorVodnykhObEktovList.enable();
            form.grid_RegionalNyjjNadzorVodnykhObEktovList.getStore().load();
        }
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Реестр объектопи2
        element = view.down('[name=ObEktopiList2]');
        ctrl = me.getController('B4.controller.ObEktopiList2');
        ctrl.connectView(element, view.ctxKey);
        // Реестр Региональный надзор водных объектов
        element = view.down('[name=RegionalNyjjNadzorVodnykhObEktovList]');
        ctrl = me.getController('B4.controller.RegionalNyjjNadzorVodnykhObEktovList');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});