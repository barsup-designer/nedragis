Ext.define('B4.controller.RukovoditeliList2', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.RukovoditeliList2',
        'B4.model.RukovoditeliList2Model'],
    // псевдоним класса реестра
    viewAlias: 'rms-rukovoditelilist2',
    viewDataName: 'RukovoditeliList2',
    // набор описаний действий реестра
    actions: {
        'Addition-RukovoditeliEditor-InWindow': {
            'editor': 'RukovoditeliEditor',
            'editorAlias': 'rms-rukovoditelieditor',
            'editorUid': '6edae791-3f32-48d2-9478-ddcb67a712c3',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 335,
                'width': 800
            }
        },
        'Editing-RukovoditeliEditor-InWindow': {
            'editor': 'RukovoditeliEditor',
            'editorAlias': 'rms-rukovoditelieditor',
            'editorUid': '6edae791-3f32-48d2-9478-ddcb67a712c3',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 335,
                'width': 800
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-rukovoditelilist2': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-RukovoditeliEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-rukovoditelilist2': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['YurLitco_Id'])) {
            this.hideColumnByDataIndex('YurLitco_Representation', true, view);
        }
        if (!Ext.isEmpty(ctxParams['PostType_Id'])) {
            this.hideColumnByDataIndex('PostType_name1', true, view);
        }
    },
});