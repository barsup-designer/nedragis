Ext.define('B4.controller.OKOPFList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OKOPFList',
        'B4.model.OKOPFListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-okopflist',
    viewDataName: 'OKOPFList',
    // набор описаний действий реестра
    actions: {
        'Addition-OKOPFEditor-InWindow': {
            'editor': 'OKOPFEditor',
            'editorAlias': 'rms-okopfeditor',
            'editorUid': '821f31b0-9c1a-4914-897b-86c7681df4ea',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 155,
                'width': 600
            }
        },
        'Editing-OKOPFEditor-InWindow': {
            'editor': 'OKOPFEditor',
            'editorAlias': 'rms-okopfeditor',
            'editorUid': '821f31b0-9c1a-4914-897b-86c7681df4ea',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 155,
                'width': 600
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-okopflist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OKOPFEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-okopflist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});