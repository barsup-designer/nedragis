Ext.define('B4.controller.StatusyObEktaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.StatusyObEktaList',
        'B4.model.StatusyObEktaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-statusyobektalist',
    viewDataName: 'StatusyObEktaList',
    // набор описаний действий реестра
    actions: {
        'Addition-StatusyObEktaEditor-InWindow': {
            'editor': 'StatusyObEktaEditor',
            'editorAlias': 'rms-statusyobektaeditor',
            'editorUid': '384cb7d4-0f1b-41f7-8a5d-45c24c12b689',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-StatusyObEktaEditor-InWindow': {
            'editor': 'StatusyObEktaEditor',
            'editorAlias': 'rms-statusyobektaeditor',
            'editorUid': '384cb7d4-0f1b-41f7-8a5d-45c24c12b689',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-statusyobektalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-StatusyObEktaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-statusyobektalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});