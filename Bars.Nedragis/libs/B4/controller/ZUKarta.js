Ext.define('B4.controller.ZUKarta', {
    extend: 'B4.base.form.Controller',
    models: [
        'ZUKartaModel'],
    views: [
        'ZUKarta'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.ZUKarta'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-zukarta',
    viewDataModel: 'ZUKartaModel',
    viewDataController: 'ZUKarta',
    viewDataName: 'ZUKarta',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            '[rmsUid=ac7b400f-b8e5-426f-9f41-eb462564c6d6]': {
                'afterrender': function(sender) {
                    var cmp0 = this.down('[rmsUid=a9e6b84d-ab5c-45e0-977c-b9079ac37238]');
                    var InternalId;
                    InternalId = cmp0.getValue();
                    var me = this; /*br*/
                    this.alphaAuthToken = this.alphaAuthToken || ''; /*br*/
                    var data = { /*br*/
                        Login: 'admin',
                        /*br*/
                        Password: 'yanao',
                        /*br*/
                        AuthToken: this.alphaAuthToken,
                        /*br*/
                        ForceNewSession: false /*br*/
                    }; /*br*/
                    $.ajax({ /*br*/
                        type: "POST",
                        /*br*/
                        url: 'http://ias.yanao.ru/alpha/default/login/login',
                        /*br*/
                        crossDomain: true,
                        /*br*/
                        data: data,
                        /*br*/
                        error: function(resp) { /*br*/
                            console.log('Login error'); /*br*/
                            console.log(resp); /*br*/
                        },
                        /*br*/
                        success: function(resp) { /*br*/
                            me.alphaAuthToken = resp.TokenValue; /*br*/
                            var url = 'http://ias.yanao.ru/alpha/default/?OpenAction=144824&InternalId=' + InternalId + '&InternalRef=http://ias.yanao.ru/nedra/%2523ZURedaktirovanieNavigacija/Edit/' + InternalId + '/'; /*br*/
                            url += '&authToken=' + me.alphaAuthToken; /*br*/
                            sender.el.createChild({ /*br*/
                                tag: "iframe",
                                /*br*/
                                src: url,
                                /*br*/
                                width: "100%",
                                /*br*/
                                height: "100%",
                                /*br*/
                            }); /*br*/
                            console.log(InternalId); /*br*/
                            console.log(url); /*br*/
                        } /*br*/
                    });
                }
            }
        });
        me.callParent(arguments);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});