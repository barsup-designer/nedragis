Ext.define('B4.controller.SotrudnikiList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.SotrudnikiList',
        'B4.model.SotrudnikiListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-sotrudnikilist',
    viewDataName: 'SotrudnikiList',
    // набор описаний действий реестра
    actions: {
        'Addition-SotrudnikiEditor-InWindow': {
            'editor': 'SotrudnikiEditor',
            'editorAlias': 'rms-sotrudnikieditor',
            'editorUid': '9a8aaaad-953d-4101-b278-637eb524ffec',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-SotrudnikiEditor-InWindow': {
            'editor': 'SotrudnikiEditor',
            'editorAlias': 'rms-sotrudnikieditor',
            'editorUid': '9a8aaaad-953d-4101-b278-637eb524ffec',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-sotrudnikilist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-SotrudnikiEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-sotrudnikilist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['dolzh_Id'])) {
            this.hideColumnByDataIndex('dolzh_name', true, view);
        }
    },
});