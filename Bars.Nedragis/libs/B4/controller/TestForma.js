Ext.define('B4.controller.TestForma', {
    extend: 'B4.base.form.Controller',
    models: [
        'TestFormaModel'],
    views: [
        'TestForma'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.TestForma'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-testforma',
    viewDataModel: 'TestFormaModel',
    viewDataController: 'TestForma',
    viewDataName: 'TestForma',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            '[rmsUid=952cad89-af4c-4341-9695-606b5174e772]': {
                'click': function(cmp) {
                    var grid = this.isComponent ? (this.is('grid') ? this : this.up('grid')) : (this.getDataGrid ? this.getDataGrid() : null),
                        ctxKey = grid ? grid.ctxKey : this.getCurrentContextKey(),
                        inWindow = true;
                    if (!grid) {
                        var rmsParentDataGridId = B4.Variables.getContainer(ctxKey).getValue("RmsParentDataGridId");
                        if (!Ext.isEmpty(rmsParentDataGridId)) {
                            grid = Ext.ComponentQuery.query(this.viewAlias + "#" + rmsParentDataGridId)[0];
                        }
                    }
                    if (inWindow && grid && grid.ctxKey) {
                        ctxKey = grid.ctxKey;
                    }
                    B4.Navigation.openForm("NedropolZovanija", null, inWindow, false, {
                        dataVariables: grid ? grid.data.getVariables() : new B4.variables.Container(),
                        RmsDataGridAlias: this.viewAlias,
                        RmsDataGridId: grid ? grid.getId() : null,
                        ctxKey: ctxKey,
                        isDataGridEditor: !Ext.isEmpty(grid),
                        maximizable: true
                    });
                }
            }
        });
        me.control({
            scope: me,
            '[rmsUid=108155db-dc57-44cc-9b78-a4b637692c83]': {
                'click': function(cmp) {
                    var grid = this.isComponent ? (this.is('grid') ? this : this.up('grid')) : (this.getDataGrid ? this.getDataGrid() : null),
                        ctxKey = grid ? grid.ctxKey : this.getCurrentContextKey(),
                        inWindow = true;
                    if (!grid) {
                        var rmsParentDataGridId = B4.Variables.getContainer(ctxKey).getValue("RmsParentDataGridId");
                        if (!Ext.isEmpty(rmsParentDataGridId)) {
                            grid = Ext.ComponentQuery.query(this.viewAlias + "#" + rmsParentDataGridId)[0];
                        }
                    }
                    if (inWindow && grid && grid.ctxKey) {
                        ctxKey = grid.ctxKey;
                    }
                    B4.Navigation.openForm("PrirodopolZovanie", null, inWindow, false, {
                        dataVariables: grid ? grid.data.getVariables() : new B4.variables.Container(),
                        RmsDataGridAlias: this.viewAlias,
                        RmsDataGridId: grid ? grid.getId() : null,
                        ctxKey: ctxKey,
                        isDataGridEditor: !Ext.isEmpty(grid),
                        maximizable: true
                    });
                }
            }
        });
        me.control({
            scope: me,
            '[rmsUid=4b3f79bb-a53d-4988-ae14-fec9084911c0]': {
                'click': function(cmp) {
                    var grid = this.isComponent ? (this.is('grid') ? this : this.up('grid')) : (this.getDataGrid ? this.getDataGrid() : null),
                        ctxKey = grid ? grid.ctxKey : this.getCurrentContextKey(),
                        inWindow = true;
                    if (!grid) {
                        var rmsParentDataGridId = B4.Variables.getContainer(ctxKey).getValue("RmsParentDataGridId");
                        if (!Ext.isEmpty(rmsParentDataGridId)) {
                            grid = Ext.ComponentQuery.query(this.viewAlias + "#" + rmsParentDataGridId)[0];
                        }
                    }
                    if (inWindow && grid && grid.ctxKey) {
                        ctxKey = grid.ctxKey;
                    }
                    B4.Navigation.openForm("Zemleustrojjstva", null, inWindow, false, {
                        dataVariables: grid ? grid.data.getVariables() : new B4.variables.Container(),
                        RmsDataGridAlias: this.viewAlias,
                        RmsDataGridId: grid ? grid.getId() : null,
                        ctxKey: ctxKey,
                        isDataGridEditor: !Ext.isEmpty(grid),
                        maximizable: true
                    });
                }
            }
        });
        me.control({
            scope: me,
            '[rmsUid=b66daf3a-d04f-4a74-b37d-8f65dd41db08]': {
                'click': function(cmp) {
                    var grid = this.isComponent ? (this.is('grid') ? this : this.up('grid')) : (this.getDataGrid ? this.getDataGrid() : null),
                        ctxKey = grid ? grid.ctxKey : this.getCurrentContextKey(),
                        inWindow = true;
                    if (!grid) {
                        var rmsParentDataGridId = B4.Variables.getContainer(ctxKey).getValue("RmsParentDataGridId");
                        if (!Ext.isEmpty(rmsParentDataGridId)) {
                            grid = Ext.ComponentQuery.query(this.viewAlias + "#" + rmsParentDataGridId)[0];
                        }
                    }
                    if (inWindow && grid && grid.ctxKey) {
                        ctxKey = grid.ctxKey;
                    }
                    B4.Navigation.openForm("Kartografija", null, inWindow, false, {
                        dataVariables: grid ? grid.data.getVariables() : new B4.variables.Container(),
                        RmsDataGridAlias: this.viewAlias,
                        RmsDataGridId: grid ? grid.getId() : null,
                        ctxKey: ctxKey,
                        isDataGridEditor: !Ext.isEmpty(grid),
                        maximizable: true
                    });
                }
            }
        });
        me.callParent(arguments);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});