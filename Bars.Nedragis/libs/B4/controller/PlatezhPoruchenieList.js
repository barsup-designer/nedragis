Ext.define('B4.controller.PlatezhPoruchenieList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.PlatezhPoruchenieList',
        'B4.model.PlatezhPoruchenieListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-platezhporuchenielist',
    viewDataName: 'PlatezhPoruchenieList',
    // набор описаний действий реестра
    actions: {
        'Addition-PlatezhPoruchenieEditor-InWindow': {
            'editor': 'PlatezhPoruchenieEditor',
            'editorAlias': 'rms-platezhporuchenieeditor',
            'editorUid': '9efb2dda-35c6-44ab-8773-dd2158591323',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-PlatezhPoruchenieEditor-InWindow': {
            'editor': 'PlatezhPoruchenieEditor',
            'editorAlias': 'rms-platezhporuchenieeditor',
            'editorUid': '9efb2dda-35c6-44ab-8773-dd2158591323',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-platezhporuchenielist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-PlatezhPoruchenieEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-platezhporuchenielist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});