Ext.define('B4.controller.KartografList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.KartografList',
        'B4.model.KartografListModel',
        'B4.aspects.Permission'],
    // псевдоним класса реестра
    viewAlias: 'rms-kartograflist',
    viewDataName: 'KartografList',
    // набор описаний действий реестра
    actions: {
        'Addition-KartografEditor-InWindow': {
            'editor': 'KartografEditor',
            'editorAlias': 'rms-kartografeditor',
            'editorUid': 'e7c7b9ec-acc7-43ac-9fc2-112e8fafdda0',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 650,
                'width': 500
            }
        },
        'Editing-KartografEditor-InWindow': {
            'editor': 'KartografEditor',
            'editorAlias': 'rms-kartografeditor',
            'editorUid': 'e7c7b9ec-acc7-43ac-9fc2-112e8fafdda0',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 650,
                'width': 500
            }
        }
    },
    aspects: [{
        'permissions': [{
            'applyBy': function(component, allowed) {
                component.setVisible(allowed);
            },
            'applyOn': {
                'selector': 'kartograflist'
            },
            'applyTo': 'rms-kartograflist #Addition-KartografEditor-InWindow',
            'name': 'razmodultest.menytest.razreesrtkartog.Create'
        }, {
            'applyBy': function(component, allowed) {
                component.setVisible(allowed);
            },
            'applyOn': {
                'selector': 'kartograflist'
            },
            'applyTo': 'rms-kartograflist #Editing-KartografEditor-InWindow',
            'name': 'razmodultest.menytest.razreesrtkartog.Update'
        }, {
            'applyBy': function(component, allowed) {
                component.setVisible(allowed);
            },
            'applyOn': {
                'selector': 'kartograflist'
            },
            'applyTo': 'rms-kartograflist #Deletion',
            'name': 'razmodultest.menytest.razreesrtkartog.Delete'
        }],
        'xtype': 'permissionaspect'
    }],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-kartograflist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-KartografEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-kartograflist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['name_Id'])) {
            this.hideColumnByDataIndex('name_NAME_SP', true, view);
        }
        if (!Ext.isEmpty(ctxParams['vid_Id'])) {
            this.hideColumnByDataIndex('Element1474278116576_Element1474020299386', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Element1474352034381_Id'])) {
            this.hideColumnByDataIndex('Element1474352034381_Element1474352136232', true, view);
        }
    },
});