Ext.define('B4.controller.ReestrObEktovList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.ReestrObEktovList',
        'B4.model.ReestrObEktovListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-reestrobektovlist',
    viewDataName: 'ReestrObEktovList',
    // набор описаний действий реестра
    actions: {
        'Addition-ReestrObEktovEditor-InWindow': {
            'editor': 'ReestrObEktovEditor',
            'editorAlias': 'rms-reestrobektoveditor',
            'editorUid': 'd367ecdd-9904-4fb0-a747-252b4ad39a4b',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 700,
                'width': 1500
            }
        },
        'Editing-ReestrObEktovEditor-InWindow': {
            'editor': 'ReestrObEktovEditor',
            'editorAlias': 'rms-reestrobektoveditor',
            'editorUid': 'd367ecdd-9904-4fb0-a747-252b4ad39a4b',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 700,
                'width': 1500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-reestrobektovlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-ReestrObEktovEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-reestrobektovlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['Element1507784253669_Id'])) {
            this.hideColumnByDataIndex('objectname12', true, view);
        }
    },
});