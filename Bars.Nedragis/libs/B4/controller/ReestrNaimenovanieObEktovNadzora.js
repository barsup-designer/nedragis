Ext.define('B4.controller.ReestrNaimenovanieObEktovNadzora', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.ReestrNaimenovanieObEktovNadzora',
        'B4.model.ReestrNaimenovanieObEktovNadzoraModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-reestrnaimenovanieobektovnadzora',
    viewDataName: 'ReestrNaimenovanieObEktovNadzora',
    // набор описаний действий реестра
    actions: {
        'Addition-FormaRedaktirovanijaObEktov-InWindow': {
            'editor': 'FormaRedaktirovanijaObEktov',
            'editorAlias': 'rms-formaredaktirovanijaobektov',
            'editorUid': '7b9141d5-2d9e-4baf-b298-144af3ad4e90',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow'
        },
        'Editing-FormaRedaktirovanijaObEktov-InWindow': {
            'editor': 'FormaRedaktirovanijaObEktov',
            'editorAlias': 'rms-formaredaktirovanijaobektov',
            'editorUid': '7b9141d5-2d9e-4baf-b298-144af3ad4e90',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-reestrnaimenovanieobektovnadzora': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-FormaRedaktirovanijaObEktov-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-reestrnaimenovanieobektovnadzora': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});