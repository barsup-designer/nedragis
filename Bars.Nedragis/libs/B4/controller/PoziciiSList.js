Ext.define('B4.controller.PoziciiSList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.PoziciiSList',
        'B4.model.PoziciiSListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-poziciislist',
    viewDataName: 'PoziciiSList',
    // набор описаний действий реестра
    actions: {
        'Addition-PoziciiSEditor-InWindow': {
            'editor': 'PoziciiSEditor',
            'editorAlias': 'rms-poziciiseditor',
            'editorUid': '77d87dcf-5697-47e8-bbfc-4eb054b9e7ae',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-PoziciiSEditor-InWindow': {
            'editor': 'PoziciiSEditor',
            'editorAlias': 'rms-poziciiseditor',
            'editorUid': '77d87dcf-5697-47e8-bbfc-4eb054b9e7ae',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-poziciislist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-PoziciiSEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-poziciislist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});