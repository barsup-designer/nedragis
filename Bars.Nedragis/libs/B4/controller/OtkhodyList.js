Ext.define('B4.controller.OtkhodyList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OtkhodyList',
        'B4.model.OtkhodyListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-otkhodylist',
    viewDataName: 'OtkhodyList',
    // набор описаний действий реестра
    actions: {
        'Addition-OtkhodyEditor-InWindow': {
            'editor': 'OtkhodyEditor',
            'editorAlias': 'rms-otkhodyeditor',
            'editorUid': '4b113066-676a-46d9-81db-8129608656be',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-OtkhodyEditor-InWindow': {
            'editor': 'OtkhodyEditor',
            'editorAlias': 'rms-otkhodyeditor',
            'editorUid': '4b113066-676a-46d9-81db-8129608656be',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-otkhodylist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OtkhodyEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-otkhodylist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});