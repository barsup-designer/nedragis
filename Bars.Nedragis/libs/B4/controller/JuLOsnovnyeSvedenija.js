Ext.define('B4.controller.JuLOsnovnyeSvedenija', {
    extend: 'B4.base.form.Controller',
    models: [
        'JuLOsnovnyeSvedenijaModel'],
    views: [
        'JuLOsnovnyeSvedenija'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.JuLOsnovnyeSvedenija'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-julosnovnyesvedenija',
    viewDataModel: 'JuLOsnovnyeSvedenijaModel',
    viewDataController: 'JuLOsnovnyeSvedenija',
    viewDataName: 'JuLOsnovnyeSvedenija',
    init: function() {
        var me = this;
        me.callParent(arguments);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['Status_Id'])) {
            this.preventFieldInput(form, ['[name=Status]']);
        }
        if (!Ext.isEmpty(ctxParams['Okopf_Id'])) {
            this.preventFieldInput(form, ['[name=Okopf]']);
        }
        if (!Ext.isEmpty(ctxParams['Okved_Id'])) {
            this.preventFieldInput(form, ['[name=Okved]']);
        }
        if (!Ext.isEmpty(ctxParams['Okogu_Id'])) {
            this.preventFieldInput(form, ['[name=Okogu]']);
        }
        if (!Ext.isEmpty(ctxParams['Okfs_Id'])) {
            this.preventFieldInput(form, ['[name=Okfs]']);
        }
        if (!Ext.isEmpty(ctxParams['OKATO_Id'])) {
            this.preventFieldInput(form, ['[name=OKATO]']);
        }
        if (!Ext.isEmpty(ctxParams['LegalAddress_Id'])) {
            this.preventFieldInput(form, ['[name=LegalAddress]']);
        }
        if (!Ext.isEmpty(ctxParams['FactAddress_Id'])) {
            this.preventFieldInput(form, ['[name=FactAddress]']);
        }
        if (!Ext.isEmpty(ctxParams['Sphere_Id'])) {
            this.preventFieldInput(form, ['[name=Sphere]']);
        }
        if (!Ext.isEmpty(ctxParams['HeadSubject_Id'])) {
            this.preventFieldInput(form, ['[name=HeadSubject]']);
        }
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});