Ext.define('B4.controller.TipZemelNogoUchastkaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.TipZemelNogoUchastkaList',
        'B4.model.TipZemelNogoUchastkaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-tipzemelnogouchastkalist',
    viewDataName: 'TipZemelNogoUchastkaList',
    // набор описаний действий реестра
    actions: {
        'Addition-TipZemelNogoUchastkaEditor-InWindow': {
            'editor': 'TipZemelNogoUchastkaEditor',
            'editorAlias': 'rms-tipzemelnogouchastkaeditor',
            'editorUid': '44456141-fd11-4749-825e-91eab8dcc9ef',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-TipZemelNogoUchastkaEditor-InWindow': {
            'editor': 'TipZemelNogoUchastkaEditor',
            'editorAlias': 'rms-tipzemelnogouchastkaeditor',
            'editorUid': '44456141-fd11-4749-825e-91eab8dcc9ef',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-tipzemelnogouchastkalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-TipZemelNogoUchastkaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-tipzemelnogouchastkalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});