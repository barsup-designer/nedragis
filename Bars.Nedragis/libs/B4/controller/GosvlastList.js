Ext.define('B4.controller.GosvlastList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.GosvlastList',
        'B4.model.GosvlastListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-gosvlastlist',
    viewDataName: 'GosvlastList',
    // набор описаний действий реестра
    actions: {
        'Addition-GosvlastEditor-InWindow': {
            'editor': 'GosvlastEditor',
            'editorAlias': 'rms-gosvlasteditor',
            'editorUid': '6e48a104-ec8b-481c-bed8-d96f663f69fc',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-GosvlastEditor-InWindow': {
            'editor': 'GosvlastEditor',
            'editorAlias': 'rms-gosvlasteditor',
            'editorUid': '6e48a104-ec8b-481c-bed8-d96f663f69fc',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-gosvlastlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-GosvlastEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-gosvlastlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});