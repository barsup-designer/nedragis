Ext.define('B4.controller.DokumentyZUEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'DokumentyZUEditorModel'],
    views: [
        'DokumentyZUEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.DokumentyZUEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-dokumentyzueditor',
    viewDataModel: 'DokumentyZUEditorModel',
    viewDataController: 'DokumentyZUEditor',
    viewDataName: 'DokumentyZUEditor',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            '[rmsUid=6ead281c-dfcd-46ff-8e78-59be23cbafc8]': {
                'afterrender': function(sender) {
                    sender.onTriggerClick(); /*br*/
                    sender.ownerCt.ownerCt.ownerCt.focusOnToFront = false;
                }
            }
        });
        me.control({
            scope: me,
            '[rmsUid=6ead281c-dfcd-46ff-8e78-59be23cbafc8]': {
                'change': function(sender, newValue, oldValue) {
                    if (oldValue === undefined && newValue === null) /*br*/
                    { /*br*/
                        return; /*br*/
                    } /*br*/
                    var formsc = sender.rmsUid == '1ae324f2-320e-42e8-9df3-df8484def876' ? sender : sender.up('[rmsUid=1ae324f2-320e-42e8-9df3-df8484def876]'); /*br*/
                    var formzu = formsc.down('[xtype=b4saveclosebutton]'); /*br*/
                    formzu.fireEvent('click', formzu);
                }
            }
        });
        me.callParent(arguments);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['zemuch_Id'])) {
            this.preventFieldInput(form, ['[name=zemuch]']);
        }
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});