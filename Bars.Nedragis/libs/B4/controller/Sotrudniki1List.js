Ext.define('B4.controller.Sotrudniki1List', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.Sotrudniki1List',
        'B4.model.Sotrudniki1ListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-sotrudniki1list',
    viewDataName: 'Sotrudniki1List',
    // набор описаний действий реестра
    actions: {
        'Addition-Sotrudniki1Editor-InWindow': {
            'editor': 'Sotrudniki1Editor',
            'editorAlias': 'rms-sotrudniki1editor',
            'editorUid': '7471584e-f4de-43dd-ae67-35cad87871fa',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-Sotrudniki1Editor-InWindow': {
            'editor': 'Sotrudniki1Editor',
            'editorAlias': 'rms-sotrudniki1editor',
            'editorUid': '7471584e-f4de-43dd-ae67-35cad87871fa',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-sotrudniki1list': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-Sotrudniki1Editor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-sotrudniki1list': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});