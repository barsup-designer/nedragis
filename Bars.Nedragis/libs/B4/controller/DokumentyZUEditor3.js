Ext.define('B4.controller.DokumentyZUEditor3', {
    extend: 'B4.base.form.Controller',
    models: [
        'DokumentyZUEditor3Model'],
    views: [
        'DokumentyZUEditor3'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.DokumentyZUEditor3'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-dokumentyzueditor3',
    viewDataModel: 'DokumentyZUEditor3Model',
    viewDataController: 'DokumentyZUEditor3',
    viewDataName: 'DokumentyZUEditor3',
    init: function() {
        var me = this;
        me.callParent(arguments);
        this.initControllers(['B4.controller.DokumentEditor']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Форма редактирования Документ
        element = form.down('[rmsUid=ab13d7a3-88c6-4a42-b88b-6d25911e7a44]');
        ctrl = me.getController('B4.controller.DokumentEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(rec.get('DokumentEditor'));
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(model, element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['zemuch_Id'])) {
            this.preventFieldInput(form, ['[name=zemuch]']);
        }
        if (!Ext.isEmpty(ctxParams['doc1_Id'])) {
            this.preventFieldInput(form, ['[name=doc1]']);
        }
        // Форма редактирования Документ
        element = form.down('[rmsUid=ab13d7a3-88c6-4a42-b88b-6d25911e7a44]');
        ctrl = me.getController('B4.controller.DokumentEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(record.get('DokumentEditor'));
        ctrl.onSetViewData(model, element);
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Форма редактирования Документ
        element = view.down('[rmsUid=ab13d7a3-88c6-4a42-b88b-6d25911e7a44]');
        ctrl = me.getController('B4.controller.DokumentEditor');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        // Форма редактирования Документ
        element = view.down('[rmsUid=ab13d7a3-88c6-4a42-b88b-6d25911e7a44]');
        ctrl = me.getController('B4.controller.DokumentEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(element.getEditorValues());
        ctrl.onViewDeployed(element, model);
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});