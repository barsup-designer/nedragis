Ext.define('B4.controller.PodvidyList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.PodvidyList',
        'B4.model.PodvidyListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-podvidylist',
    viewDataName: 'PodvidyList',
    // набор описаний действий реестра
    actions: {
        'Addition-PodvidyEditor-InWindow': {
            'editor': 'PodvidyEditor',
            'editorAlias': 'rms-podvidyeditor',
            'editorUid': 'fd3dc875-ffc1-40d9-b037-70bc0673f9f9',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-PodvidyEditor-InWindow': {
            'editor': 'PodvidyEditor',
            'editorAlias': 'rms-podvidyeditor',
            'editorUid': 'fd3dc875-ffc1-40d9-b037-70bc0673f9f9',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-podvidylist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-PodvidyEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-podvidylist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['Element1474349306046_Id'])) {
            this.hideColumnByDataIndex('Element1474349306046_Element1474020299386', true, view);
        }
    },
});