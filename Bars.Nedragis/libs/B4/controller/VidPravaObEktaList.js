Ext.define('B4.controller.VidPravaObEktaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.VidPravaObEktaList',
        'B4.model.VidPravaObEktaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-vidpravaobektalist',
    viewDataName: 'VidPravaObEktaList',
    // набор описаний действий реестра
    actions: {
        'Addition-VidPravaObEktaEditor-InWindow': {
            'editor': 'VidPravaObEktaEditor',
            'editorAlias': 'rms-vidpravaobektaeditor',
            'editorUid': '02d4e467-6c09-461d-ad03-be32a4146d75',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-VidPravaObEktaEditor-InWindow': {
            'editor': 'VidPravaObEktaEditor',
            'editorAlias': 'rms-vidpravaobektaeditor',
            'editorUid': '02d4e467-6c09-461d-ad03-be32a4146d75',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-vidpravaobektalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-VidPravaObEktaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-vidpravaobektalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});