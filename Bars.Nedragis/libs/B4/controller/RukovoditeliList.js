Ext.define('B4.controller.RukovoditeliList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.RukovoditeliList',
        'B4.model.RukovoditeliListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-rukovoditelilist',
    viewDataName: 'RukovoditeliList',
    // набор описаний действий реестра
    actions: {
        'Addition-RukovoditeliEditor-InWindow': {
            'editor': 'RukovoditeliEditor',
            'editorAlias': 'rms-rukovoditelieditor',
            'editorUid': '6edae791-3f32-48d2-9478-ddcb67a712c3',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 335,
                'width': 800
            }
        },
        'Editing-RukovoditeliEditor-InWindow': {
            'editor': 'RukovoditeliEditor',
            'editorAlias': 'rms-rukovoditelieditor',
            'editorUid': '6edae791-3f32-48d2-9478-ddcb67a712c3',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 335,
                'width': 800
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-rukovoditelilist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-RukovoditeliEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-rukovoditelilist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['PostType_Id'])) {
            this.hideColumnByDataIndex('PostType_name1', true, view);
        }
    },
});