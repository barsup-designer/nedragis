Ext.define('B4.controller.OKTMO1', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OKTMO1',
        'B4.model.OKTMO1Model'],
    // псевдоним класса реестра
    viewAlias: 'rms-oktmo1',
    viewDataName: 'OKTMO1',
    // набор описаний действий реестра
    actions: {
        'Addition-OKTMOEditor-InWindow': {
            'editor': 'OKTMOEditor',
            'editorAlias': 'rms-oktmoeditor',
            'editorUid': '08ab56e2-51ac-4690-9998-869c9887e489',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 190,
                'width': 600
            }
        },
        'Editing-OKTMOEditor-InWindow': {
            'editor': 'OKTMOEditor',
            'editorAlias': 'rms-oktmoeditor',
            'editorUid': '08ab56e2-51ac-4690-9998-869c9887e489',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 190,
                'width': 600
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-oktmo1': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OKTMOEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-oktmo1': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});