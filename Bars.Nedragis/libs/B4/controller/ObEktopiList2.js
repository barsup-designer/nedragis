Ext.define('B4.controller.ObEktopiList2', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.ObEktopiList2',
        'B4.model.ObEktopiList2Model'],
    // псевдоним класса реестра
    viewAlias: 'rms-obektopilist2',
    viewDataName: 'ObEktopiList2',
    // набор описаний действий реестра
    actions: {
        'Addition-ObEktopiEditor-InWindow': {
            'editor': 'ObEktopiEditor',
            'editorAlias': 'rms-obektopieditor',
            'editorUid': '414ffe01-f268-40bb-9261-9efc56a2c442',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-ObEktopiEditor-InWindow': {
            'editor': 'ObEktopiEditor',
            'editorAlias': 'rms-obektopieditor',
            'editorUid': '414ffe01-f268-40bb-9261-9efc56a2c442',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-obektopilist2': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-ObEktopiEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-obektopilist2': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['Element1508136885094_Id'])) {
            this.hideColumnByDataIndex('objectopi17', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Element1508137003934_Id'])) {
            this.hideColumnByDataIndex('opinamber17', true, view);
            this.hideColumnByDataIndex('opidatereest17', true, view);
            this.hideColumnByDataIndex('opidateend17', true, view);
            this.hideColumnByDataIndex('opimesto17', true, view);
        }
    },
});