Ext.define('B4.controller.test_barsList2', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.test_barsList2',
        'B4.model.test_barsList2Model',
        'B4.aspects.Permission'],
    // псевдоним класса реестра
    viewAlias: 'rms-test_barslist2',
    viewDataName: 'test_barsList2',
    // набор описаний действий реестра
    actions: {
        'Addition-test_bars1-InWindow': {
            'editor': 'test_bars1',
            'editorAlias': 'rms-test_bars1',
            'editorUid': 'f6c40bc9-ec41-427d-8ae3-df43cc30d3f9',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-test_barsEditor-InWindow': {
            'editor': 'test_barsEditor',
            'editorAlias': 'rms-test_barseditor',
            'editorUid': '1e97280b-1ec4-45bb-a193-22fceb8b3a1b',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [{
        'permissions': [{
            'applyBy': function(component, allowed) {
                component.setVisible(allowed);
            },
            'applyOn': {
                'selector': 'test_barslist2'
            },
            'applyTo': 'rms-test_barslist2 #Addition-test_bars1-InWindow',
            'name': 'razmodultest.menytest.test1234.Create'
        }, {
            'applyBy': function(component, allowed) {
                component.setVisible(allowed);
            },
            'applyOn': {
                'selector': 'test_barslist2'
            },
            'applyTo': 'rms-test_barslist2 #Editing-test_barsEditor-InWindow',
            'name': 'razmodultest.menytest.test1234.Update'
        }, {
            'applyBy': function(component, allowed) {
                component.setVisible(allowed);
            },
            'applyOn': {
                'selector': 'test_barslist2'
            },
            'applyTo': 'rms-test_barslist2 #Deletion',
            'name': 'razmodultest.menytest.test1234.Delete'
        }],
        'xtype': 'permissionaspect'
    }],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-test_barslist2': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-test_barsEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-test_barslist2': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});