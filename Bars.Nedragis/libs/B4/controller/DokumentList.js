Ext.define('B4.controller.DokumentList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.DokumentList',
        'B4.model.DokumentListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-dokumentlist',
    viewDataName: 'DokumentList',
    // набор описаний действий реестра
    actions: {
        'Addition-DokumentEditor-InWindow': {
            'editor': 'DokumentEditor',
            'editorAlias': 'rms-dokumenteditor',
            'editorUid': 'a4178b63-4c2d-4869-81ca-95523ab4036d',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 480,
                'width': 600
            }
        },
        'Editing-DokumentEditor-InWindow': {
            'editor': 'DokumentEditor',
            'editorAlias': 'rms-dokumenteditor',
            'editorUid': 'a4178b63-4c2d-4869-81ca-95523ab4036d',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 480,
                'width': 600
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-dokumentlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-DokumentEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-dokumentlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['Type_Id'])) {
            this.hideColumnByDataIndex('Type_name', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Subject_Id'])) {
            this.hideColumnByDataIndex('Subject_Representation', true, view);
        }
    },
});