Ext.define('B4.controller.OKTMOList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OKTMOList',
        'B4.model.OKTMOListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-oktmolist',
    viewDataName: 'OKTMOList',
    // набор описаний действий реестра
    actions: {
        'Addition-OKTMOEditor-InWindow': {
            'editor': 'OKTMOEditor',
            'editorAlias': 'rms-oktmoeditor',
            'editorUid': '08ab56e2-51ac-4690-9998-869c9887e489',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 190,
                'width': 600
            }
        },
        'Editing-OKTMOEditor-InWindow': {
            'editor': 'OKTMOEditor',
            'editorAlias': 'rms-oktmoeditor',
            'editorUid': '08ab56e2-51ac-4690-9998-869c9887e489',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 190,
                'width': 600
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-oktmolist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OKTMOEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-oktmolist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});