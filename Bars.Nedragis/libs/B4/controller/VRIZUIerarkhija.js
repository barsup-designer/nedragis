Ext.define('B4.controller.VRIZUIerarkhija', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.VRIZUIerarkhija',
        'B4.model.VRIZUIerarkhijaModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-vrizuierarkhija',
    viewDataName: 'VRIZUIerarkhija',
    // набор описаний действий реестра
    actions: {
        'Addition-VRIZUEditor-InWindow': {
            'editor': 'VRIZUEditor',
            'editorAlias': 'rms-vrizueditor',
            'editorUid': '9cf46c22-57af-4c74-b5ec-d2816ab28b67',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 293,
                'width': 500
            }
        },
        'Editing-VRIZUEditor-InWindow': {
            'editor': 'VRIZUEditor',
            'editorAlias': 'rms-vrizueditor',
            'editorUid': '9cf46c22-57af-4c74-b5ec-d2816ab28b67',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 293,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-vrizuierarkhija': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-VRIZUEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-vrizuierarkhija': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});