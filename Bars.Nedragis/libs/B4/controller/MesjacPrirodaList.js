Ext.define('B4.controller.MesjacPrirodaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.MesjacPrirodaList',
        'B4.model.MesjacPrirodaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-mesjacprirodalist',
    viewDataName: 'MesjacPrirodaList',
    // набор описаний действий реестра
    actions: {
        'Addition-MesjacPrirodaEditor-InWindow': {
            'editor': 'MesjacPrirodaEditor',
            'editorAlias': 'rms-mesjacprirodaeditor',
            'editorUid': '485ab932-dec6-4253-8440-c1e85750719d',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-MesjacPrirodaEditor-InWindow': {
            'editor': 'MesjacPrirodaEditor',
            'editorAlias': 'rms-mesjacprirodaeditor',
            'editorUid': '485ab932-dec6-4253-8440-c1e85750719d',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-mesjacprirodalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-MesjacPrirodaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-mesjacprirodalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});