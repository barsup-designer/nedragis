Ext.define('B4.controller.KBKList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.KBKList',
        'B4.model.KBKListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-kbklist',
    viewDataName: 'KBKList',
    // набор описаний действий реестра
    actions: {
        'Addition-KBKEditor-InWindow': {
            'editor': 'KBKEditor',
            'editorAlias': 'rms-kbkeditor',
            'editorUid': '56275727-c762-4891-b1e5-66389e1dfc5d',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 300,
                'width': 500
            }
        },
        'Editing-KBKEditor-InWindow': {
            'editor': 'KBKEditor',
            'editorAlias': 'rms-kbkeditor',
            'editorUid': '56275727-c762-4891-b1e5-66389e1dfc5d',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 300,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-kbklist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-KBKEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-kbklist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});