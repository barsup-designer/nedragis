Ext.define('B4.controller.ZemelNyemUchastkiEditor_Copy_1', {
    extend: 'B4.base.form.Controller',
    models: [
        'ZemelNyemUchastkiEditor_Copy_1Model'],
    views: [
        'ZemelNyemUchastkiEditor_Copy_1'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.ZemelNyemUchastkiEditor_Copy_1'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-zemelnyemuchastkieditor_copy_1',
    viewDataModel: 'ZemelNyemUchastkiEditor_Copy_1Model',
    viewDataController: 'ZemelNyemUchastkiEditor_Copy_1',
    viewDataName: 'ZemelNyemUchastkiEditor_Copy_1',
    init: function() {
        var me = this;
        me.callParent(arguments);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['typeobj_Id'])) {
            this.preventFieldInput(form, ['[name=typeobj]']);
        }
        if (!Ext.isEmpty(ctxParams['TypeZU_Id'])) {
            this.preventFieldInput(form, ['[name=TypeZU]']);
        }
        if (!Ext.isEmpty(ctxParams['Status_obj_Id'])) {
            this.preventFieldInput(form, ['[name=Status_obj]']);
        }
        if (!Ext.isEmpty(ctxParams['category_Id'])) {
            this.preventFieldInput(form, ['[name=category]']);
        }
        if (!Ext.isEmpty(ctxParams['vid_prava_Id'])) {
            this.preventFieldInput(form, ['[name=vid_prava]']);
        }
        if (!Ext.isEmpty(ctxParams['vid_use_Id'])) {
            this.preventFieldInput(form, ['[name=vid_use]']);
        }
        if (!Ext.isEmpty(ctxParams['holder_id_Id'])) {
            this.preventFieldInput(form, ['[name=holder_id]']);
        }
        if (!Ext.isEmpty(ctxParams['bremya_Id'])) {
            this.preventFieldInput(form, ['[name=bremya]']);
        }
        if (!Ext.isEmpty(ctxParams['owner_id_Id'])) {
            this.preventFieldInput(form, ['[name=owner_id]']);
        }
        if (!Ext.isEmpty(ctxParams['okato_Id'])) {
            this.preventFieldInput(form, ['[name=okato]']);
        }
        if (!Ext.isEmpty(ctxParams['oktmo_Id'])) {
            this.preventFieldInput(form, ['[name=oktmo]']);
        }
        if (!Ext.isEmpty(ctxParams['property_manager_id_Id'])) {
            this.preventFieldInput(form, ['[name=property_manager_id]']);
        }
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});