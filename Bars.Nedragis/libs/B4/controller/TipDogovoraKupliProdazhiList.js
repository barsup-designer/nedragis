Ext.define('B4.controller.TipDogovoraKupliProdazhiList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.TipDogovoraKupliProdazhiList',
        'B4.model.TipDogovoraKupliProdazhiListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-tipdogovorakupliprodazhilist',
    viewDataName: 'TipDogovoraKupliProdazhiList',
    // набор описаний действий реестра
    actions: {
        'Addition-TipDogovoraKupliProdazhiEditor-InWindow': {
            'editor': 'TipDogovoraKupliProdazhiEditor',
            'editorAlias': 'rms-tipdogovorakupliprodazhieditor',
            'editorUid': '16c0c2f0-fc70-4047-b183-ca551f2554a3',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-TipDogovoraKupliProdazhiEditor-InWindow': {
            'editor': 'TipDogovoraKupliProdazhiEditor',
            'editorAlias': 'rms-tipdogovorakupliprodazhieditor',
            'editorUid': '16c0c2f0-fc70-4047-b183-ca551f2554a3',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-tipdogovorakupliprodazhilist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-TipDogovoraKupliProdazhiEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-tipdogovorakupliprodazhilist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});