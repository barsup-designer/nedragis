Ext.define('B4.controller.ZemelNyemUchastkiEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'ZemelNyemUchastkiEditorModel'],
    views: [
        'ZemelNyemUchastkiEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.ZemelNyemUchastkiEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-zemelnyemuchastkieditor',
    viewDataModel: 'ZemelNyemUchastkiEditorModel',
    viewDataController: 'ZemelNyemUchastkiEditor',
    viewDataName: 'ZemelNyemUchastkiEditor',
    init: function() {
        var me = this;
        me.callParent(arguments);
        this.initControllers(['B4.controller.DokumentList']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Реестр Документ
        element = form.down('[name=DokumentList]');
        ctrl = me.getController('B4.controller.DokumentList');
        element.data.set('ObjectOfLaw_Id', rec.get('Id') || 0);
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['type_Id'])) {
            this.preventFieldInput(form, ['[name=type]']);
        }
        if (!Ext.isEmpty(ctxParams['vid_prava_Id'])) {
            this.preventFieldInput(form, ['[name=vid_prava]']);
        }
        if (!Ext.isEmpty(ctxParams['category_Id'])) {
            this.preventFieldInput(form, ['[name=category]']);
        }
        if (!Ext.isEmpty(ctxParams['vid_use_Id'])) {
            this.preventFieldInput(form, ['[name=vid_use]']);
        }
        if (!Ext.isEmpty(ctxParams['owner_id_Id'])) {
            this.preventFieldInput(form, ['[name=owner_id]']);
        }
        if (!Ext.isEmpty(ctxParams['holder_id_Id'])) {
            this.preventFieldInput(form, ['[name=holder_id]']);
        }
        if (isNewRecord == true) {
            form.grid_DokumentList.disableGrid();
        } else {
            form.grid_DokumentList.enable();
            form.grid_DokumentList.getStore().load();
        }
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Реестр Документ
        element = view.down('[name=DokumentList]');
        ctrl = me.getController('B4.controller.DokumentList');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});