Ext.define('B4.controller.PodvidRabotPrirodaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.PodvidRabotPrirodaList',
        'B4.model.PodvidRabotPrirodaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-podvidrabotprirodalist',
    viewDataName: 'PodvidRabotPrirodaList',
    // набор описаний действий реестра
    actions: {
        'Addition-PodvidRabotPrirodaEditor-InWindow': {
            'editor': 'PodvidRabotPrirodaEditor',
            'editorAlias': 'rms-podvidrabotprirodaeditor',
            'editorUid': '05ad296c-a1b1-4cb3-b207-b98a0b584bc0',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-PodvidRabotPrirodaEditor-InWindow': {
            'editor': 'PodvidRabotPrirodaEditor',
            'editorAlias': 'rms-podvidrabotprirodaeditor',
            'editorUid': '05ad296c-a1b1-4cb3-b207-b98a0b584bc0',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-podvidrabotprirodalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-PodvidRabotPrirodaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-podvidrabotprirodalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});