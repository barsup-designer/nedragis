Ext.define('B4.controller.DokumentyZUList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.DokumentyZUList',
        'B4.model.DokumentyZUListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-dokumentyzulist',
    viewDataName: 'DokumentyZUList',
    // набор описаний действий реестра
    actions: {
        'Addition-DokumentyZUEditor3-InWindow': {
            'editor': 'DokumentyZUEditor3',
            'editorAlias': 'rms-dokumentyzueditor3',
            'editorUid': 'b2dea0bc-a377-4cfc-897f-60f37a6211b4',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 480,
                'width': 600
            }
        },
        'Editing-DokumentyZUEditor3-InWindow': {
            'editor': 'DokumentyZUEditor3',
            'editorAlias': 'rms-dokumentyzueditor3',
            'editorUid': 'b2dea0bc-a377-4cfc-897f-60f37a6211b4',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 480,
                'width': 600
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-dokumentyzulist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-DokumentyZUEditor3-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-dokumentyzulist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['zemuch_Id'])) {
            this.hideColumnByDataIndex('zemuch_cadnum1', true, view);
            this.hideColumnByDataIndex('zemuch_name', true, view);
            this.hideColumnByDataIndex('zemuch_owner_id_Representation', true, view);
            this.hideColumnByDataIndex('zemuch_vid_prava_name1', true, view);
            this.hideColumnByDataIndex('zemuch_category_name1', true, view);
            this.hideColumnByDataIndex('zemuch_vid_use_name1', true, view);
        }
        if (!Ext.isEmpty(ctxParams['zemuch_owner_id_Id'])) {
            this.hideColumnByDataIndex('zemuch_owner_id_Representation', true, view);
        }
        if (!Ext.isEmpty(ctxParams['doc1_Id'])) {
            this.hideColumnByDataIndex('doc1_Id', true, view);
        }
        if (!Ext.isEmpty(ctxParams['zemuch_vid_prava_Id'])) {
            this.hideColumnByDataIndex('zemuch_vid_prava_name1', true, view);
        }
        if (!Ext.isEmpty(ctxParams['zemuch_category_Id'])) {
            this.hideColumnByDataIndex('zemuch_category_name1', true, view);
        }
        if (!Ext.isEmpty(ctxParams['zemuch_vid_use_Id'])) {
            this.hideColumnByDataIndex('zemuch_vid_use_name1', true, view);
        }
    },
});