Ext.define('B4.controller.OKATOList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OKATOList',
        'B4.model.OKATOListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-okatolist',
    viewDataName: 'OKATOList',
    // набор описаний действий реестра
    actions: {
        'Addition-OKATOEditor-InWindow': {
            'editor': 'OKATOEditor',
            'editorAlias': 'rms-okatoeditor',
            'editorUid': '125aabc1-a228-4220-a185-855d4c0041d7',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 230,
                'width': 500
            }
        },
        'Editing-OKATOEditor-InWindow': {
            'editor': 'OKATOEditor',
            'editorAlias': 'rms-okatoeditor',
            'editorUid': '125aabc1-a228-4220-a185-855d4c0041d7',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 230,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-okatolist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OKATOEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-okatolist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['parent_id_Id'])) {
            this.hideColumnByDataIndex('parent_id_name1', true, view);
        }
    },
});