Ext.define('B4.controller.test_bars2', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.test_bars2',
        'B4.model.test_bars2Model'],
    // псевдоним класса реестра
    viewAlias: 'rms-test_bars2',
    viewDataName: 'test_bars2',
    // набор описаний действий реестра
    actions: {},
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-test_bars2': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});