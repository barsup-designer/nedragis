Ext.define('B4.controller.ZURedaktirovanieNavigacija', {
    extend: 'B4.base.form.Controller',
    models: [
        'ZURedaktirovanieNavigacijaModel'],
    views: [
        'ZURedaktirovanieNavigacija'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.ZURedaktirovanieNavigacija'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-zuredaktirovanienavigacija',
    viewDataModel: 'ZURedaktirovanieNavigacijaModel',
    viewDataController: 'ZURedaktirovanieNavigacija',
    viewDataName: 'ZURedaktirovanieNavigacija',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-navigationpanel[rmsUid=1b976d4c-8d77-4a42-8a2e-6a919a934a8b]': {
                'beforeload': function(cmp, store, oper) {
                    var values = cmp.up('rms-zuredaktirovanienavigacija').getEditorValues();
                    Ext.apply(oper.params, {
                        EditorValues: values,
                        ZemelNyemUchastki_Id: values.Id
                    });
                }
            }
        });
        me.control({
            scope: me,
            'rms-navigationpanel[rmsUid=1b976d4c-8d77-4a42-8a2e-6a919a934a8b]': {
                'load': function(cmp, store, node, records, success) {
                    var tree = cmp.getTree(),
                        treeStore = tree && tree.getStore(),
                        rootNode = treeStore && treeStore.getRootNode();
                    if (rootNode) {
                        var foundedNode = rootNode.findChildBy(function(node) {
                            return node.get('Name') == 'Основные сведения';
                        }, rootNode, true);
                        if (foundedNode) {
                            tree.getSelectionModel().select(foundedNode);
                            tree.fireEvent('itemClick', tree, foundedNode);
                        }
                    }
                }
            }
        });
        me.callParent(arguments);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {
            view.down('[rmsUid=1b976d4c-8d77-4a42-8a2e-6a919a934a8b]').disable();
        } else {
            view.down('[rmsUid=1b976d4c-8d77-4a42-8a2e-6a919a934a8b]').enable();
            view.down('[rmsUid=1b976d4c-8d77-4a42-8a2e-6a919a934a8b]').getRootNode().set('EntityId', record.get('Id'));
            view.down('[rmsUid=1b976d4c-8d77-4a42-8a2e-6a919a934a8b]').getRootNode().expand();
        }
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});