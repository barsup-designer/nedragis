Ext.define('B4.controller.SostojanieObEktaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.SostojanieObEktaList',
        'B4.model.SostojanieObEktaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-sostojanieobektalist',
    viewDataName: 'SostojanieObEktaList',
    // набор описаний действий реестра
    actions: {
        'Addition-SostojanieObEktaEditor-InWindow': {
            'editor': 'SostojanieObEktaEditor',
            'editorAlias': 'rms-sostojanieobektaeditor',
            'editorUid': '92c9229d-2d85-4c41-9751-ae96b8b40751',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-SostojanieObEktaEditor-InWindow': {
            'editor': 'SostojanieObEktaEditor',
            'editorAlias': 'rms-sostojanieobektaeditor',
            'editorUid': '92c9229d-2d85-4c41-9751-ae96b8b40751',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-sostojanieobektalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-SostojanieObEktaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-sostojanieobektalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});