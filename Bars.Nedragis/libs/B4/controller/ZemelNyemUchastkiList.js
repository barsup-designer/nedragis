Ext.define('B4.controller.ZemelNyemUchastkiList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.ZemelNyemUchastkiList',
        'B4.model.ZemelNyemUchastkiListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-zemelnyemuchastkilist',
    viewDataName: 'ZemelNyemUchastkiList',
    // набор описаний действий реестра
    actions: {
        'Addition-ZUFormaVvoda-InWindow': {
            'editor': 'ZUFormaVvoda',
            'editorAlias': 'rms-zuformavvoda',
            'editorUid': '3cbbcbbc-d8b8-4098-9b0f-7df6d4fee84d',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 280,
                'width': 700
            }
        },
        'Editing-ZURedaktirovanieNavigacija-Default': {
            'editor': 'ZURedaktirovanieNavigacija',
            'editorAlias': 'rms-zuredaktirovanienavigacija',
            'editorUid': 'aecb5a82-7da6-4a28-b7ea-0539c124ebf0',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'Default',
            'redirectTo': 'ZURedaktirovanieNavigacija/Edit/{id}/'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-zemelnyemuchastkilist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-ZURedaktirovanieNavigacija-Default', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-zemelnyemuchastkilist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['category_Id'])) {
            this.hideColumnByDataIndex('category_name1', true, view);
        }
        if (!Ext.isEmpty(ctxParams['vid_use_Id'])) {
            this.hideColumnByDataIndex('vid_use_name1', true, view);
        }
        if (!Ext.isEmpty(ctxParams['vid_prava_Id'])) {
            this.hideColumnByDataIndex('vid_prava_name1', true, view);
        }
        if (!Ext.isEmpty(ctxParams['holder_id_Id'])) {
            this.hideColumnByDataIndex('holder_id_Representation', true, view);
        }
        if (!Ext.isEmpty(ctxParams['owner_id_Id'])) {
            this.hideColumnByDataIndex('owner_id_Representation', true, view);
        }
        if (!Ext.isEmpty(ctxParams['property_manager_id_Id'])) {
            this.hideColumnByDataIndex('property_manager_id_Representation', true, view);
        }
        if (!Ext.isEmpty(ctxParams['typeobj_Id'])) {
            this.hideColumnByDataIndex('typeobj_name', true, view);
        }
    },
});