Ext.define('B4.controller.OKATO1', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OKATO1',
        'B4.model.OKATO1Model'],
    // псевдоним класса реестра
    viewAlias: 'rms-okato1',
    viewDataName: 'OKATO1',
    // набор описаний действий реестра
    actions: {
        'Addition-OKATOEditor-InWindow': {
            'editor': 'OKATOEditor',
            'editorAlias': 'rms-okatoeditor',
            'editorUid': '125aabc1-a228-4220-a185-855d4c0041d7',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 230,
                'width': 500
            }
        },
        'Editing-OKATOEditor-InWindow': {
            'editor': 'OKATOEditor',
            'editorAlias': 'rms-okatoeditor',
            'editorUid': '125aabc1-a228-4220-a185-855d4c0041d7',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 230,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-okato1': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OKATOEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-okato1': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});