Ext.define('B4.controller.monitorList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.monitorList',
        'B4.model.monitorListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-monitorlist',
    viewDataName: 'monitorList',
    // набор описаний действий реестра
    actions: {
        'Addition-TestForma-InWindow': {
            'editor': 'TestForma',
            'editorAlias': 'rms-testforma',
            'editorUid': 'e88098a7-cae7-4e52-9120-22ca81f28fe3',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 400,
                'width': 500
            }
        },
        'Editing-Kartografija-InWindow': {
            'editor': 'Kartografija',
            'editorAlias': 'rms-kartografija',
            'editorUid': 'eef9d875-1b80-4901-bd2e-bdd681162d46',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 450
            }
        },
        'Editing-NedropolZovanija-InWindow': {
            'editor': 'NedropolZovanija',
            'editorAlias': 'rms-nedropolzovanija',
            'editorUid': 'bd53db70-595b-41a0-98d7-2636aa6605bd',
            'hideToolbar': true,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-PrirodopolZovanie-InWindow': {
            'editor': 'PrirodopolZovanie',
            'editorAlias': 'rms-prirodopolzovanie',
            'editorUid': 'c3840f17-1916-4ff9-be6b-499a2ea16441',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow'
        },
        'Editing-Zemleustrojjstva-InWindow': {
            'editor': 'Zemleustrojjstva',
            'editorAlias': 'rms-zemleustrojjstva',
            'editorUid': 'e96ce26c-72eb-4c4f-8695-326c998b58e1',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 450
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-monitorlist': {
                'itemdblclick': function(sender, record, item, index) {
                    var _D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82;
                    _D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82 = record.get('otdel_NAME_SP');
                    if (_D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82 == 'Недропользование') {
                        var grid = this.isComponent ? (this.is('grid') ? this : this.up('grid')) : (this.getDataGrid ? this.getDataGrid() : null),
                            ctxKey = grid ? grid.ctxKey : this.getCurrentContextKey(),
                            inWindow = true;
                        if (!grid) {
                            var rmsParentDataGridId = B4.Variables.getContainer(ctxKey).getValue("RmsParentDataGridId");
                            if (!Ext.isEmpty(rmsParentDataGridId)) {
                                grid = Ext.ComponentQuery.query(this.viewAlias + "#" + rmsParentDataGridId)[0];
                            }
                        }
                        if (inWindow && grid && grid.ctxKey) {
                            ctxKey = grid.ctxKey;
                        }
                        B4.Navigation.openForm("NedropolZovanija", null, inWindow, false, {
                            dataVariables: grid ? grid.data.getVariables() : new B4.variables.Container(),
                            RmsDataGridAlias: this.viewAlias,
                            RmsDataGridId: grid ? grid.getId() : null,
                            ctxKey: ctxKey,
                            isDataGridEditor: !Ext.isEmpty(grid),
                            maximizable: true
                        });
                    }
                }
            }
        });
        me.control({
            scope: me,
            'rms-monitorlist': {
                'itemdblclick': function(sender, record, item, index) {
                    var _D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82;
                    _D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82 = record.get('otdel_NAME_SP');
                    if (_D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82 == 'Недропользование') {
                        var grid = this.isComponent ? (this.is('grid') ? this : this.up('grid')) : (this.getDataGrid ? this.getDataGrid() : null),
                            ctxKey = grid ? grid.ctxKey : this.getCurrentContextKey(),
                            inWindow = true;
                        if (!grid) {
                            var rmsParentDataGridId = B4.Variables.getContainer(ctxKey).getValue("RmsParentDataGridId");
                            if (!Ext.isEmpty(rmsParentDataGridId)) {
                                grid = Ext.ComponentQuery.query(this.viewAlias + "#" + rmsParentDataGridId)[0];
                            }
                        }
                        if (inWindow && grid && grid.ctxKey) {
                            ctxKey = grid.ctxKey;
                        }
                        B4.Navigation.openForm("NedropolZovanija", null, inWindow, false, {
                            dataVariables: grid ? grid.data.getVariables() : new B4.variables.Container(),
                            RmsDataGridAlias: this.viewAlias,
                            RmsDataGridId: grid ? grid.getId() : null,
                            ctxKey: ctxKey,
                            isDataGridEditor: !Ext.isEmpty(grid),
                            maximizable: true
                        });
                    }
                }
            }
        });
        me.control({
            scope: me,
            'rms-monitorlist': {
                'itemdblclick': function(sender, record, item, index) {
                    var _D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82;
                    _D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82 = record.get('otdel_NAME_SP');
                    if (_D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82 == 'Недропользование') {
                        var grid = this.isComponent ? (this.is('grid') ? this : this.up('grid')) : (this.getDataGrid ? this.getDataGrid() : null),
                            ctxKey = grid ? grid.ctxKey : this.getCurrentContextKey(),
                            inWindow = true;
                        if (!grid) {
                            var rmsParentDataGridId = B4.Variables.getContainer(ctxKey).getValue("RmsParentDataGridId");
                            if (!Ext.isEmpty(rmsParentDataGridId)) {
                                grid = Ext.ComponentQuery.query(this.viewAlias + "#" + rmsParentDataGridId)[0];
                            }
                        }
                        if (inWindow && grid && grid.ctxKey) {
                            ctxKey = grid.ctxKey;
                        }
                        B4.Navigation.openForm("NedropolZovanija", null, inWindow, false, {
                            dataVariables: grid ? grid.data.getVariables() : new B4.variables.Container(),
                            RmsDataGridAlias: this.viewAlias,
                            RmsDataGridId: grid ? grid.getId() : null,
                            ctxKey: ctxKey,
                            isDataGridEditor: !Ext.isEmpty(grid),
                            maximizable: true
                        });
                    }
                }
            }
        });
        me.control({
            scope: me,
            'rms-monitorlist': {
                'itemdblclick': function(sender, record, item, index) {
                    var _D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82;
                    _D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82 = record.get('otdel_NAME_SP');
                    if (_D1_8D_D0_BB_D0_B5_D0_BC_D0_B5_D0_BD_D1_82 == 'Недропользование') {
                        var grid = this.isComponent ? (this.is('grid') ? this : this.up('grid')) : (this.getDataGrid ? this.getDataGrid() : null),
                            ctxKey = grid ? grid.ctxKey : this.getCurrentContextKey(),
                            inWindow = true;
                        if (!grid) {
                            var rmsParentDataGridId = B4.Variables.getContainer(ctxKey).getValue("RmsParentDataGridId");
                            if (!Ext.isEmpty(rmsParentDataGridId)) {
                                grid = Ext.ComponentQuery.query(this.viewAlias + "#" + rmsParentDataGridId)[0];
                            }
                        }
                        if (inWindow && grid && grid.ctxKey) {
                            ctxKey = grid.ctxKey;
                        }
                        B4.Navigation.openForm("NedropolZovanija", null, inWindow, false, {
                            dataVariables: grid ? grid.data.getVariables() : new B4.variables.Container(),
                            RmsDataGridAlias: this.viewAlias,
                            RmsDataGridId: grid ? grid.getId() : null,
                            ctxKey: ctxKey,
                            isDataGridEditor: !Ext.isEmpty(grid),
                            maximizable: true
                        });
                    }
                }
            }
        });
        me.control({
            scope: me,
            'rms-monitorlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['otdel_Id'])) {
            this.hideColumnByDataIndex('otdel_NAME_SP', true, view);
        }
    },
});