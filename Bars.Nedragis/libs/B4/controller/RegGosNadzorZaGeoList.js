Ext.define('B4.controller.RegGosNadzorZaGeoList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.RegGosNadzorZaGeoList',
        'B4.model.RegGosNadzorZaGeoListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-reggosnadzorzageolist',
    viewDataName: 'RegGosNadzorZaGeoList',
    // набор описаний действий реестра
    actions: {
        'Addition-RegGosNadzorZaGeoEditor-InWindow': {
            'editor': 'RegGosNadzorZaGeoEditor',
            'editorAlias': 'rms-reggosnadzorzageoeditor',
            'editorUid': 'cb78b2fc-1f56-4337-a67a-98d0120034a5',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-RegGosNadzorZaGeoEditor-InWindow': {
            'editor': 'RegGosNadzorZaGeoEditor',
            'editorAlias': 'rms-reggosnadzorzageoeditor',
            'editorUid': 'cb78b2fc-1f56-4337-a67a-98d0120034a5',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-reggosnadzorzageolist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-RegGosNadzorZaGeoEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-reggosnadzorzageolist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});