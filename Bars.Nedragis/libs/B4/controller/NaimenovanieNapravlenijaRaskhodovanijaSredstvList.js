Ext.define('B4.controller.NaimenovanieNapravlenijaRaskhodovanijaSredstvList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.NaimenovanieNapravlenijaRaskhodovanijaSredstvList',
        'B4.model.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-naimenovanienapravlenijaraskhodovanijasredstvlist',
    viewDataName: 'NaimenovanieNapravlenijaRaskhodovanijaSredstvList',
    // набор описаний действий реестра
    actions: {
        'Addition-NaimenovanieNapravlenijaRaskhodovanijaSredstvEditor-InWindow': {
            'editor': 'NaimenovanieNapravlenijaRaskhodovanijaSredstvEditor',
            'editorAlias': 'rms-naimenovanienapravlenijaraskhodovanijasredstveditor',
            'editorUid': 'ae586287-dc77-432a-9eec-0cc8367eb259',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-NaimenovanieNapravlenijaRaskhodovanijaSredstvEditor-InWindow': {
            'editor': 'NaimenovanieNapravlenijaRaskhodovanijaSredstvEditor',
            'editorAlias': 'rms-naimenovanienapravlenijaraskhodovanijasredstveditor',
            'editorUid': 'ae586287-dc77-432a-9eec-0cc8367eb259',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-naimenovanienapravlenijaraskhodovanijasredstvlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-NaimenovanieNapravlenijaRaskhodovanijaSredstvEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-naimenovanienapravlenijaraskhodovanijasredstvlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});