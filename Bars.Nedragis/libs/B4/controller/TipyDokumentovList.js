Ext.define('B4.controller.TipyDokumentovList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.TipyDokumentovList',
        'B4.model.TipyDokumentovListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-tipydokumentovlist',
    viewDataName: 'TipyDokumentovList',
    // набор описаний действий реестра
    actions: {
        'Addition-TipyDokumentovEditor-InWindow': {
            'editor': 'TipyDokumentovEditor',
            'editorAlias': 'rms-tipydokumentoveditor',
            'editorUid': '60b4dc7f-e8b7-4707-9bbc-24467f8d612a',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 200,
                'width': 500
            }
        },
        'Editing-TipyDokumentovEditor-InWindow': {
            'editor': 'TipyDokumentovEditor',
            'editorAlias': 'rms-tipydokumentoveditor',
            'editorUid': '60b4dc7f-e8b7-4707-9bbc-24467f8d612a',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 200,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-tipydokumentovlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-TipyDokumentovEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-tipydokumentovlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});