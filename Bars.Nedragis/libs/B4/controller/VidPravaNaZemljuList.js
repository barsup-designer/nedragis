Ext.define('B4.controller.VidPravaNaZemljuList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.VidPravaNaZemljuList',
        'B4.model.VidPravaNaZemljuListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-vidpravanazemljulist',
    viewDataName: 'VidPravaNaZemljuList',
    // набор описаний действий реестра
    actions: {
        'Addition-VidPravaNaZemljuEditor-InWindow': {
            'editor': 'VidPravaNaZemljuEditor',
            'editorAlias': 'rms-vidpravanazemljueditor',
            'editorUid': '1ff32656-60eb-4d76-a40a-4bf5f1cc6893',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-VidPravaNaZemljuEditor-InWindow': {
            'editor': 'VidPravaNaZemljuEditor',
            'editorAlias': 'rms-vidpravanazemljueditor',
            'editorUid': '1ff32656-60eb-4d76-a40a-4bf5f1cc6893',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-vidpravanazemljulist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-VidPravaNaZemljuEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-vidpravanazemljulist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});