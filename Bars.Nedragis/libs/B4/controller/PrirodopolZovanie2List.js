Ext.define('B4.controller.PrirodopolZovanie2List', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.PrirodopolZovanie2List',
        'B4.model.PrirodopolZovanie2ListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-prirodopolzovanie2list',
    viewDataName: 'PrirodopolZovanie2List',
    // набор описаний действий реестра
    actions: {
        'Addition-PrirodopolZovanie2Editor-InWindow': {
            'editor': 'PrirodopolZovanie2Editor',
            'editorAlias': 'rms-prirodopolzovanie2editor',
            'editorUid': '77996565-8563-4a00-b4b6-5f7691a289cd',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 700,
                'width': 700
            }
        },
        'Editing-PrirodopolZovanie2Editor-InWindow': {
            'editor': 'PrirodopolZovanie2Editor',
            'editorAlias': 'rms-prirodopolzovanie2editor',
            'editorUid': '77996565-8563-4a00-b4b6-5f7691a289cd',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 700,
                'width': 700
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-prirodopolzovanie2list': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-PrirodopolZovanie2Editor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-prirodopolzovanie2list': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['VidRabotPR2_Id'])) {
            this.hideColumnByDataIndex('VidRabotPR2_vid_rabot_prirod_s222', true, view);
        }
        if (!Ext.isEmpty(ctxParams['IspolnitelPR2_Id'])) {
            this.hideColumnByDataIndex('IspolnitelPR2_Element1474352136232PR2', true, view);
        }
        if (!Ext.isEmpty(ctxParams['SoispolnitelPR2_Id'])) {
            this.hideColumnByDataIndex('SoispolnitelPR22222', true, view);
        }
        if (!Ext.isEmpty(ctxParams['MonthPR2_Id'])) {
            this.hideColumnByDataIndex('MonthPR2_month_pr222', false, view);
        }
    },
});