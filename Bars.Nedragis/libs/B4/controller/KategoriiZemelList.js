Ext.define('B4.controller.KategoriiZemelList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.KategoriiZemelList',
        'B4.model.KategoriiZemelListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-kategoriizemellist',
    viewDataName: 'KategoriiZemelList',
    // набор описаний действий реестра
    actions: {
        'Addition-KategoriiZemelEditor-InWindow': {
            'editor': 'KategoriiZemelEditor',
            'editorAlias': 'rms-kategoriizemeleditor',
            'editorUid': '7854d448-c1e5-41dc-b616-58a6650ffc03',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 600
            }
        },
        'Editing-KategoriiZemelEditor-InWindow': {
            'editor': 'KategoriiZemelEditor',
            'editorAlias': 'rms-kategoriizemeleditor',
            'editorUid': '7854d448-c1e5-41dc-b616-58a6650ffc03',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 600
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-kategoriizemellist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-KategoriiZemelEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-kategoriizemellist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});