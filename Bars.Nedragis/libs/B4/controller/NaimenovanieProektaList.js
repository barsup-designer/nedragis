Ext.define('B4.controller.NaimenovanieProektaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.NaimenovanieProektaList',
        'B4.model.NaimenovanieProektaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-naimenovanieproektalist',
    viewDataName: 'NaimenovanieProektaList',
    // набор описаний действий реестра
    actions: {
        'Addition-NaimenovanieProektaEditor-InWindow': {
            'editor': 'NaimenovanieProektaEditor',
            'editorAlias': 'rms-naimenovanieproektaeditor',
            'editorUid': 'e79a2917-f34b-43dd-ac6b-32d488700d2f',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-NaimenovanieProektaEditor-InWindow': {
            'editor': 'NaimenovanieProektaEditor',
            'editorAlias': 'rms-naimenovanieproektaeditor',
            'editorUid': 'e79a2917-f34b-43dd-ac6b-32d488700d2f',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-naimenovanieproektalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-NaimenovanieProektaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-naimenovanieproektalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});