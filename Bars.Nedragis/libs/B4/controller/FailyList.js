Ext.define('B4.controller.FailyList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.FailyList',
        'B4.model.FailyListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-failylist',
    viewDataName: 'FailyList',
    // набор описаний действий реестра
    actions: {
        'Addition-FailyEditor-InWindow': {
            'editor': 'FailyEditor',
            'editorAlias': 'rms-failyeditor',
            'editorUid': '9a7aac91-3084-45d5-98e5-556c2690ccfc',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-FailyEditor-InWindow': {
            'editor': 'FailyEditor',
            'editorAlias': 'rms-failyeditor',
            'editorUid': '9a7aac91-3084-45d5-98e5-556c2690ccfc',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-failylist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-FailyEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-failylist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});