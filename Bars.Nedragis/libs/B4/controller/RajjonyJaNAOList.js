Ext.define('B4.controller.RajjonyJaNAOList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.RajjonyJaNAOList',
        'B4.model.RajjonyJaNAOListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-rajjonyjanaolist',
    viewDataName: 'RajjonyJaNAOList',
    // набор описаний действий реестра
    actions: {
        'Addition-RajjonyJaNAOEditor-InWindow': {
            'editor': 'RajjonyJaNAOEditor',
            'editorAlias': 'rms-rajjonyjanaoeditor',
            'editorUid': 'e2309e62-7c98-42bf-8913-afe305ba0dd7',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-RajjonyJaNAOEditor-InWindow': {
            'editor': 'RajjonyJaNAOEditor',
            'editorAlias': 'rms-rajjonyjanaoeditor',
            'editorUid': 'e2309e62-7c98-42bf-8913-afe305ba0dd7',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-rajjonyjanaolist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-RajjonyJaNAOEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-rajjonyjanaolist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});