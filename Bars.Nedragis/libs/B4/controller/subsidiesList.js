Ext.define('B4.controller.subsidiesList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.subsidiesList',
        'B4.model.subsidiesListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-subsidieslist',
    viewDataName: 'subsidiesList',
    // набор описаний действий реестра
    actions: {
        'Addition-subsidiesEditor-InWindow': {
            'editor': 'subsidiesEditor',
            'editorAlias': 'rms-subsidieseditor',
            'editorUid': '9bb20aab-4f3d-4023-88ec-8f20243f4bae',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-subsidieslist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['NAME_MO_Id'])) {
            this.hideColumnByDataIndex('NAME_MO_Element1455707802552', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Element1455708491495_Id'])) {
            this.hideColumnByDataIndex('Element1455708491495_Element1455708457805', true, view);
        }
    },
});