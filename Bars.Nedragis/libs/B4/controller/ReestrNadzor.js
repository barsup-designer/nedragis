Ext.define('B4.controller.ReestrNadzor', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.ReestrNadzor',
        'B4.model.ReestrNadzorModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-reestrnadzor',
    viewDataName: 'ReestrNadzor',
    // набор описаний действий реестра
    actions: {
        'Addition-FormaNadzor-InWindow': {
            'editor': 'FormaNadzor',
            'editorAlias': 'rms-formanadzor',
            'editorUid': 'aad7f4a7-f6f4-4551-b733-1fcb9f2ec7a3',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 700,
                'width': 600
            }
        },
        'Editing-FormaNadzor-InWindow': {
            'editor': 'FormaNadzor',
            'editorAlias': 'rms-formanadzor',
            'editorUid': 'aad7f4a7-f6f4-4551-b733-1fcb9f2ec7a3',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 700,
                'width': 600
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-reestrnadzor': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-FormaNadzor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-reestrnadzor': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['Element1507271571833_Id'])) {
            this.hideColumnByDataIndex('subjectnadzorann', false, view);
        }
    },
});