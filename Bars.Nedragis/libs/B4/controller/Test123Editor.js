Ext.define('B4.controller.Test123Editor', {
    extend: 'B4.base.form.Controller',
    models: [
        'Test123EditorModel'],
    views: [
        'Test123Editor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.Test123Editor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-test123editor',
    viewDataModel: 'Test123EditorModel',
    viewDataController: 'Test123Editor',
    viewDataName: 'Test123Editor',
    init: function() {
        var me = this;
        me.callParent(arguments);
        this.initControllers(['B4.controller.SotrudnikiList']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Справочник Сотрудники
        element = form.down('[name=SotrudnikiList]');
        ctrl = me.getController('B4.controller.SotrudnikiList');
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (isNewRecord == true) {
            form.grid_SotrudnikiList.disableGrid();
        } else {
            form.grid_SotrudnikiList.enable();
            form.grid_SotrudnikiList.getStore().load();
        }
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Справочник Сотрудники
        element = view.down('[name=SotrudnikiList]');
        ctrl = me.getController('B4.controller.SotrudnikiList');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});