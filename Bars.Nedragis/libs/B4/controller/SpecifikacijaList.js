Ext.define('B4.controller.SpecifikacijaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.SpecifikacijaList',
        'B4.model.SpecifikacijaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-specifikacijalist',
    viewDataName: 'SpecifikacijaList',
    // набор описаний действий реестра
    actions: {
        'Addition-SpecifikacijaEditor-InWindow': {
            'editor': 'SpecifikacijaEditor',
            'editorAlias': 'rms-specifikacijaeditor',
            'editorUid': 'baa5124e-1891-4bec-8daa-56dd3e48f578',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-SpecifikacijaEditor-InWindow': {
            'editor': 'SpecifikacijaEditor',
            'editorAlias': 'rms-specifikacijaeditor',
            'editorUid': 'baa5124e-1891-4bec-8daa-56dd3e48f578',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-specifikacijalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-SpecifikacijaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-specifikacijalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});