Ext.define('B4.controller.DokumentyNavigacija', {
    extend: 'B4.base.form.Controller',
    models: [
        'DokumentyNavigacijaModel'],
    views: [
        'DokumentyNavigacija'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.DokumentyNavigacija'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-dokumentynavigacija',
    viewDataModel: 'DokumentyNavigacijaModel',
    viewDataController: 'DokumentyNavigacija',
    viewDataName: 'DokumentyNavigacija',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-navigationpanel[rmsUid=5d03704c-cb05-4f0b-9663-371e87c63d7f]': {
                'beforeload': function(cmp, store, oper) {
                    var values = cmp.up('rms-dokumentynavigacija').getEditorValues();
                    Ext.apply(oper.params, {
                        EditorValues: values,
                        Dokument_Id: values.Id
                    });
                }
            }
        });
        me.control({
            scope: me,
            'rms-navigationpanel[rmsUid=5d03704c-cb05-4f0b-9663-371e87c63d7f]': {
                'load': function(cmp, store, node, records, success) {
                    var tree = cmp.getTree(),
                        treeStore = tree && tree.getStore(),
                        rootNode = treeStore && treeStore.getRootNode();
                    if (rootNode) {
                        var foundedNode = rootNode.findChildBy(function(node) {
                            return node.get('Name') == 'Основные сведения Документ ';
                        }, rootNode, true);
                        if (foundedNode) {
                            tree.getSelectionModel().select(foundedNode);
                            tree.fireEvent('itemClick', tree, foundedNode);
                        }
                    }
                }
            }
        });
        me.control({
            scope: me,
            'rms-navigationpanel[rmsUid=5d03704c-cb05-4f0b-9663-371e87c63d7f] [rmsUid=dad78d3b-5d89-415d-894d-6b66e61061d3]': {
                'actioncomplete': function(basicForm, action, eOpts) {
                    var form = basicForm.owner,
                        rec = form.getRecord(),
                        id = rec ? rec.get('Id') : 0,
                        isNewRecord = Ext.isEmpty(id) || id <= 0 || !id;
                    if (!isNewRecord) return;
                    var nav = form.up('rms-navigationpanel[rmsUid=5d03704c-cb05-4f0b-9663-371e87c63d7f]'),
                        root = nav.getRootNode();
                    root.set('EntityId', action.result.data.Id);
                    nav.getTree().getStore().load({
                        node: root,
                        callback: function() {
                            nav.down('tabpanel').removeAll();
                        }
                    });
                }
            }
        });
        me.callParent(arguments);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {
            view.down('[rmsUid=5d03704c-cb05-4f0b-9663-371e87c63d7f]').enable();
            view.down('[rmsUid=5d03704c-cb05-4f0b-9663-371e87c63d7f]').getRootNode().set('EntityId', record.get('Id'));
            view.down('[rmsUid=5d03704c-cb05-4f0b-9663-371e87c63d7f]').getRootNode().expand();
        } else {
            view.down('[rmsUid=5d03704c-cb05-4f0b-9663-371e87c63d7f]').enable();
            view.down('[rmsUid=5d03704c-cb05-4f0b-9663-371e87c63d7f]').getRootNode().set('EntityId', record.get('Id'));
            view.down('[rmsUid=5d03704c-cb05-4f0b-9663-371e87c63d7f]').getRootNode().expand();
        }
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});