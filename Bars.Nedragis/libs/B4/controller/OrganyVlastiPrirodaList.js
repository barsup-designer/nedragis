Ext.define('B4.controller.OrganyVlastiPrirodaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OrganyVlastiPrirodaList',
        'B4.model.OrganyVlastiPrirodaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-organyvlastiprirodalist',
    viewDataName: 'OrganyVlastiPrirodaList',
    // набор описаний действий реестра
    actions: {
        'Addition-OrganyVlastiPrirodaEditor-InWindow': {
            'editor': 'OrganyVlastiPrirodaEditor',
            'editorAlias': 'rms-organyvlastiprirodaeditor',
            'editorUid': '09ce0ebc-15ea-4823-859a-49aaa5a94c12',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-OrganyVlastiPrirodaEditor-InWindow': {
            'editor': 'OrganyVlastiPrirodaEditor',
            'editorAlias': 'rms-organyvlastiprirodaeditor',
            'editorUid': '09ce0ebc-15ea-4823-859a-49aaa5a94c12',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-organyvlastiprirodalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OrganyVlastiPrirodaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-organyvlastiprirodalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});