Ext.define('B4.controller.JuLRedaktirovanie', {
    extend: 'B4.base.form.Controller',
    models: [
        'JuLRedaktirovanieModel'],
    views: [
        'JuLRedaktirovanie'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.JuLRedaktirovanie'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-julredaktirovanie',
    viewDataModel: 'JuLRedaktirovanieModel',
    viewDataController: 'JuLRedaktirovanie',
    viewDataName: 'JuLRedaktirovanie',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-navigationpanel[rmsUid=509910ca-f47b-4029-b06a-8a5b91d4cdc0]': {
                'beforeload': function(cmp, store, oper) {
                    var values = cmp.up('rms-julredaktirovanie').getEditorValues();
                    Ext.apply(oper.params, {
                        EditorValues: values,
                        JuridicheskieLica_Id: values.Id
                    });
                }
            }
        });
        me.control({
            scope: me,
            'rms-navigationpanel[rmsUid=509910ca-f47b-4029-b06a-8a5b91d4cdc0]': {
                'load': function(cmp, store, node, records, success) {
                    var tree = cmp.getTree(),
                        treeStore = tree && tree.getStore(),
                        rootNode = treeStore && treeStore.getRootNode();
                    if (rootNode) {
                        var foundedNode = rootNode.findChildBy(function(node) {
                            return node.get('Name') == 'Основные сведения';
                        }, rootNode, true);
                        if (foundedNode) {
                            tree.getSelectionModel().select(foundedNode);
                            tree.fireEvent('itemClick', tree, foundedNode);
                        }
                    }
                }
            }
        });
        me.callParent(arguments);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {
            view.down('[rmsUid=509910ca-f47b-4029-b06a-8a5b91d4cdc0]').disable();
        } else {
            view.down('[rmsUid=509910ca-f47b-4029-b06a-8a5b91d4cdc0]').enable();
            view.down('[rmsUid=509910ca-f47b-4029-b06a-8a5b91d4cdc0]').getRootNode().set('EntityId', record.get('Id'));
            view.down('[rmsUid=509910ca-f47b-4029-b06a-8a5b91d4cdc0]').getRootNode().expand();
        }
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});