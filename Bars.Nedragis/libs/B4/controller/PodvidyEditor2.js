Ext.define('B4.controller.PodvidyEditor2', {
    extend: 'B4.base.form.Controller',
    models: [
        'PodvidyEditor2Model'],
    views: [
        'PodvidyEditor2'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.PodvidyEditor2'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-podvidyeditor2',
    viewDataModel: 'PodvidyEditor2Model',
    viewDataController: 'PodvidyEditor2',
    viewDataName: 'PodvidyEditor2',
    init: function() {
        var me = this;
        me.callParent(arguments);
        this.initControllers(['B4.controller.VidyRabotList']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Реестр Виды работ
        element = form.down('[name=VidyRabotList]');
        ctrl = me.getController('B4.controller.VidyRabotList');
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['Element1474349306046_Id'])) {
            this.preventFieldInput(form, ['[name=Element1474349306046]']);
        }
        if (isNewRecord == true) {
            form.grid_VidyRabotList.disableGrid();
        } else {
            form.grid_VidyRabotList.enable();
            form.grid_VidyRabotList.getStore().load();
        }
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Реестр Виды работ
        element = view.down('[name=VidyRabotList]');
        ctrl = me.getController('B4.controller.VidyRabotList');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});