Ext.define('B4.controller.FajjlPrivjazannyjjKDokumentuList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.FajjlPrivjazannyjjKDokumentuList',
        'B4.model.FajjlPrivjazannyjjKDokumentuListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-fajjlprivjazannyjjkdokumentulist',
    viewDataName: 'FajjlPrivjazannyjjKDokumentuList',
    // набор описаний действий реестра
    actions: {
        'Addition-FajjlPrivjazannyjjKDokumentuEditor-InWindow': {
            'editor': 'FajjlPrivjazannyjjKDokumentuEditor',
            'editorAlias': 'rms-fajjlprivjazannyjjkdokumentueditor',
            'editorUid': '0cde53c9-41ae-4e83-857e-98b6537f660a',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 300,
                'width': 500
            }
        },
        'Editing-FajjlPrivjazannyjjKDokumentuEditor-InWindow': {
            'editor': 'FajjlPrivjazannyjjKDokumentuEditor',
            'editorAlias': 'rms-fajjlprivjazannyjjkdokumentueditor',
            'editorUid': '0cde53c9-41ae-4e83-857e-98b6537f660a',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 300,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-fajjlprivjazannyjjkdokumentulist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-FajjlPrivjazannyjjKDokumentuEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-fajjlprivjazannyjjkdokumentulist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['FileInfo_Id'])) {
            this.hideColumnByDataIndex('FileInfo_Name', true, view);
            this.hideColumnByDataIndex('FileInfo_Size', true, view);
            this.hideColumnByDataIndex('FileInfo_Content', true, view);
        }
    },
});