Ext.define('B4.controller.VidRabotPrirList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.VidRabotPrirList',
        'B4.model.VidRabotPrirListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-vidrabotprirlist',
    viewDataName: 'VidRabotPrirList',
    // набор описаний действий реестра
    actions: {
        'Addition-VidRabotPrirEditor-InWindow': {
            'editor': 'VidRabotPrirEditor',
            'editorAlias': 'rms-vidrabotprireditor',
            'editorUid': '4979193c-53df-4acd-817a-37eccf59f986',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-VidRabotPrirEditor-InWindow': {
            'editor': 'VidRabotPrirEditor',
            'editorAlias': 'rms-vidrabotprireditor',
            'editorUid': '4979193c-53df-4acd-817a-37eccf59f986',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-vidrabotprirlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-VidRabotPrirEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-vidrabotprirlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});