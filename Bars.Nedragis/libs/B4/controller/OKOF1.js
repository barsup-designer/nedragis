Ext.define('B4.controller.OKOF1', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OKOF1',
        'B4.model.OKOF1Model'],
    // псевдоним класса реестра
    viewAlias: 'rms-okof1',
    viewDataName: 'OKOF1',
    // набор описаний действий реестра
    actions: {
        'Addition-OKOFEditor-InWindow': {
            'editor': 'OKOFEditor',
            'editorAlias': 'rms-okofeditor',
            'editorUid': '6e4451b4-f9a0-45c5-8282-ea74f43b7628',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 310,
                'width': 550
            }
        },
        'Editing-OKOFEditor-InWindow': {
            'editor': 'OKOFEditor',
            'editorAlias': 'rms-okofeditor',
            'editorUid': '6e4451b4-f9a0-45c5-8282-ea74f43b7628',
            'hideToolbar': false,
            'maximizable': false,
            'mode': 'InWindow',
            'size': {
                'height': 310,
                'width': 550
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-okof1': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OKOFEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-okof1': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['clasifier_Id'])) {
            this.hideColumnByDataIndex('clasifier_code', true, view);
            this.hideColumnByDataIndex('clasifier_name', true, view);
        }
    },
});