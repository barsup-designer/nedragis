Ext.define('B4.controller.OKVEhDList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OKVEhDList',
        'B4.model.OKVEhDListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-okvehdlist',
    viewDataName: 'OKVEhDList',
    // набор описаний действий реестра
    actions: {
        'Addition-OKVEhDEditor-InWindow': {
            'editor': 'OKVEhDEditor',
            'editorAlias': 'rms-okvehdeditor',
            'editorUid': '4641dbd1-c764-43d5-8a95-aef8c0ae76b8',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 240,
                'width': 500
            }
        },
        'Editing-OKVEhDEditor-InWindow': {
            'editor': 'OKVEhDEditor',
            'editorAlias': 'rms-okvehdeditor',
            'editorUid': '4641dbd1-c764-43d5-8a95-aef8c0ae76b8',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 240,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-okvehdlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OKVEhDEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-okvehdlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});