Ext.define('B4.controller.OrganizaciiList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.OrganizaciiList',
        'B4.model.OrganizaciiListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-organizaciilist',
    viewDataName: 'OrganizaciiList',
    // набор описаний действий реестра
    actions: {
        'Addition-OrganizaciiEditor-InWindow': {
            'editor': 'OrganizaciiEditor',
            'editorAlias': 'rms-organizaciieditor',
            'editorUid': 'd032a020-5c30-4d8a-9a3b-1907940cac53',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-OrganizaciiEditor-InWindow': {
            'editor': 'OrganizaciiEditor',
            'editorAlias': 'rms-organizaciieditor',
            'editorUid': 'd032a020-5c30-4d8a-9a3b-1907940cac53',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-organizaciilist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-OrganizaciiEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-organizaciilist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['Element1511331983555_Id'])) {
            this.hideColumnByDataIndex('hgf123', true, view);
        }
    },
});