Ext.define('B4.controller.KlassifikacijaOKOFList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.KlassifikacijaOKOFList',
        'B4.model.KlassifikacijaOKOFListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-klassifikacijaokoflist',
    viewDataName: 'KlassifikacijaOKOFList',
    // набор описаний действий реестра
    actions: {
        'Addition-KlassifikacijaOKOFEditor-InWindow': {
            'editor': 'KlassifikacijaOKOFEditor',
            'editorAlias': 'rms-klassifikacijaokofeditor',
            'editorUid': '42f494a8-8d1b-40c9-a55d-10978a409c0a',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 150,
                'width': 500
            }
        },
        'Editing-KlassifikacijaOKOFEditor-InWindow': {
            'editor': 'KlassifikacijaOKOFEditor',
            'editorAlias': 'rms-klassifikacijaokofeditor',
            'editorUid': '42f494a8-8d1b-40c9-a55d-10978a409c0a',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 150,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-klassifikacijaokoflist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-KlassifikacijaOKOFEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-klassifikacijaokoflist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});