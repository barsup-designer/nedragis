Ext.define('B4.controller.GrafikPlatezhejjList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.GrafikPlatezhejjList',
        'B4.model.GrafikPlatezhejjListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-grafikplatezhejjlist',
    viewDataName: 'GrafikPlatezhejjList',
    // набор описаний действий реестра
    actions: {
        'Addition-GrafikPlatezhejjEditor-InWindow': {
            'editor': 'GrafikPlatezhejjEditor',
            'editorAlias': 'rms-grafikplatezhejjeditor',
            'editorUid': '40299054-d19e-45ad-af91-76cb7b5e6ebe',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-GrafikPlatezhejjEditor-InWindow': {
            'editor': 'GrafikPlatezhejjEditor',
            'editorAlias': 'rms-grafikplatezhejjeditor',
            'editorUid': '40299054-d19e-45ad-af91-76cb7b5e6ebe',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-grafikplatezhejjlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-GrafikPlatezhejjEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-grafikplatezhejjlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});