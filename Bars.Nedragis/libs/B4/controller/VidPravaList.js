Ext.define('B4.controller.VidPravaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.VidPravaList',
        'B4.model.VidPravaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-vidpravalist',
    viewDataName: 'VidPravaList',
    // набор описаний действий реестра
    actions: {
        'Addition-VidPravaEditor-InWindow': {
            'editor': 'VidPravaEditor',
            'editorAlias': 'rms-vidpravaeditor',
            'editorUid': 'ca2440ee-b16c-4786-9566-d4420a6ecf20',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 150,
                'width': 500
            }
        },
        'Editing-VidPravaEditor-InWindow': {
            'editor': 'VidPravaEditor',
            'editorAlias': 'rms-vidpravaeditor',
            'editorUid': 'ca2440ee-b16c-4786-9566-d4420a6ecf20',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 150,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-vidpravalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-VidPravaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-vidpravalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});