Ext.define('B4.controller.JuridicheskieLicaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.JuridicheskieLicaList',
        'B4.model.JuridicheskieLicaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-juridicheskielicalist',
    viewDataName: 'JuridicheskieLicaList',
    // набор описаний действий реестра
    actions: {
        'Addition-JuridicheskieLicaEditor2-InWindow': {
            'editor': 'JuridicheskieLicaEditor2',
            'editorAlias': 'rms-juridicheskielicaeditor2',
            'editorUid': 'a0c97a59-b5de-4236-9d1f-49b5f005deab',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 320,
                'width': 650
            }
        },
        'Editing-JuLRedaktirovanie-Default': {
            'editor': 'JuLRedaktirovanie',
            'editorAlias': 'rms-julredaktirovanie',
            'editorUid': '7e363d6f-723a-4ba0-9ef6-5ccdb915a46c',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'Default',
            'redirectTo': 'JuLRedaktirovanie/Edit/{id}/'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-juridicheskielicalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-JuLRedaktirovanie-Default', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-juridicheskielicalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['OKATO_Id'])) {
            this.hideColumnByDataIndex('OKATO_name1', true, view);
        }
    },
});