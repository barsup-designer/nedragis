Ext.define('B4.controller.VRIZUList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.VRIZUList',
        'B4.model.VRIZUListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-vrizulist',
    viewDataName: 'VRIZUList',
    // набор описаний действий реестра
    actions: {
        'Addition-VRIZUEditor-InWindow': {
            'editor': 'VRIZUEditor',
            'editorAlias': 'rms-vrizueditor',
            'editorUid': '9cf46c22-57af-4c74-b5ec-d2816ab28b67',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 293,
                'width': 500
            }
        },
        'Editing-VRIZUEditor-InWindow': {
            'editor': 'VRIZUEditor',
            'editorAlias': 'rms-vrizueditor',
            'editorUid': '9cf46c22-57af-4c74-b5ec-d2816ab28b67',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 293,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-vrizulist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-VRIZUEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-vrizulist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['parent_Id'])) {
            this.hideColumnByDataIndex('parent_name1', true, view);
        }
    },
});