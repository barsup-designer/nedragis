Ext.define('B4.controller.DolzhnostList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.DolzhnostList',
        'B4.model.DolzhnostListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-dolzhnostlist',
    viewDataName: 'DolzhnostList',
    // набор описаний действий реестра
    actions: {
        'Addition-DolzhnostEditor-InWindow': {
            'editor': 'DolzhnostEditor',
            'editorAlias': 'rms-dolzhnosteditor',
            'editorUid': '6e91eafb-81f3-41ba-be06-38cd3d096021',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-DolzhnostEditor-InWindow': {
            'editor': 'DolzhnostEditor',
            'editorAlias': 'rms-dolzhnosteditor',
            'editorUid': '6e91eafb-81f3-41ba-be06-38cd3d096021',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-dolzhnostlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-DolzhnostEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-dolzhnostlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});