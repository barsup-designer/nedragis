Ext.define('B4.controller.VidyRabotList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.VidyRabotList',
        'B4.model.VidyRabotListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-vidyrabotlist',
    viewDataName: 'VidyRabotList',
    // набор описаний действий реестра
    actions: {
        'Addition-VidyRabotEditor-InWindow': {
            'editor': 'VidyRabotEditor',
            'editorAlias': 'rms-vidyraboteditor',
            'editorUid': 'c07ed5a6-fef5-4f86-af50-b532ed48b849',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-VidyRabotEditor-InWindow': {
            'editor': 'VidyRabotEditor',
            'editorAlias': 'rms-vidyraboteditor',
            'editorUid': 'c07ed5a6-fef5-4f86-af50-b532ed48b849',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-vidyrabotlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-VidyRabotEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-vidyrabotlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});