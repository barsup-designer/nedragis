Ext.define('B4.controller.VidRabotOPINFList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.VidRabotOPINFList',
        'B4.model.VidRabotOPINFListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-vidrabotopinflist',
    viewDataName: 'VidRabotOPINFList',
    // набор описаний действий реестра
    actions: {
        'Addition-VidRabotOPINFEditor-InWindow': {
            'editor': 'VidRabotOPINFEditor',
            'editorAlias': 'rms-vidrabotopinfeditor',
            'editorUid': '77208b5f-1a03-457a-b073-5f16995563c7',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-VidRabotOPINFEditor-InWindow': {
            'editor': 'VidRabotOPINFEditor',
            'editorAlias': 'rms-vidrabotopinfeditor',
            'editorUid': '77208b5f-1a03-457a-b073-5f16995563c7',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-vidrabotopinflist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-VidRabotOPINFEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-vidrabotopinflist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});