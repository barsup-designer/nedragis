Ext.define('B4.controller.RegionalNyjjNadzorVodnykhObEktovList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.RegionalNyjjNadzorVodnykhObEktovList',
        'B4.model.RegionalNyjjNadzorVodnykhObEktovListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-regionalnyjjnadzorvodnykhobektovlist',
    viewDataName: 'RegionalNyjjNadzorVodnykhObEktovList',
    // набор описаний действий реестра
    actions: {
        'Addition-RegionalNyjjNadzorVodnykhObEktovEditor-InWindow': {
            'editor': 'RegionalNyjjNadzorVodnykhObEktovEditor',
            'editorAlias': 'rms-regionalnyjjnadzorvodnykhobektoveditor',
            'editorUid': '11af8bd4-b527-4bad-907c-75e4db60d043',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-RegionalNyjjNadzorVodnykhObEktovEditor-InWindow': {
            'editor': 'RegionalNyjjNadzorVodnykhObEktovEditor',
            'editorAlias': 'rms-regionalnyjjnadzorvodnykhobektoveditor',
            'editorUid': '11af8bd4-b527-4bad-907c-75e4db60d043',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-regionalnyjjnadzorvodnykhobektovlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-RegionalNyjjNadzorVodnykhObEktovEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-regionalnyjjnadzorvodnykhobektovlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});