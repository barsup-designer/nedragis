Ext.define('B4.controller.StatusySubEktaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.StatusySubEktaList',
        'B4.model.StatusySubEktaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-statusysubektalist',
    viewDataName: 'StatusySubEktaList',
    // набор описаний действий реестра
    actions: {
        'Addition-StatusySubEktaEditor-InWindow': {
            'editor': 'StatusySubEktaEditor',
            'editorAlias': 'rms-statusysubektaeditor',
            'editorUid': 'e23e4a32-5a36-4c14-90a7-33b2ede3434e',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        },
        'Editing-StatusySubEktaEditor-InWindow': {
            'editor': 'StatusySubEktaEditor',
            'editorAlias': 'rms-statusysubektaeditor',
            'editorUid': 'e23e4a32-5a36-4c14-90a7-33b2ede3434e',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 160,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-statusysubektalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-StatusySubEktaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-statusysubektalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});