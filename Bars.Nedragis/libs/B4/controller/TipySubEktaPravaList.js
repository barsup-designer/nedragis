Ext.define('B4.controller.TipySubEktaPravaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.TipySubEktaPravaList',
        'B4.model.TipySubEktaPravaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-tipysubektapravalist',
    viewDataName: 'TipySubEktaPravaList',
    // набор описаний действий реестра
    actions: {
        'Addition-TipySubEktaPravaEditor-InWindow': {
            'editor': 'TipySubEktaPravaEditor',
            'editorAlias': 'rms-tipysubektapravaeditor',
            'editorUid': 'cb5ff098-eac9-4e70-ad96-759a2a0681a4',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 150,
                'width': 500
            }
        },
        'Editing-TipySubEktaPravaEditor-InWindow': {
            'editor': 'TipySubEktaPravaEditor',
            'editorAlias': 'rms-tipysubektapravaeditor',
            'editorUid': 'cb5ff098-eac9-4e70-ad96-759a2a0681a4',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 150,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-tipysubektapravalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-TipySubEktaPravaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-tipysubektapravalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});