Ext.define('B4.controller.DokumentEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'DokumentEditorModel'],
    views: [
        'DokumentEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.DokumentEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-dokumenteditor',
    viewDataModel: 'DokumentEditorModel',
    viewDataController: 'DokumentEditor',
    viewDataName: 'DokumentEditor',
    init: function() {
        var me = this;
        me.callParent(arguments);
        this.initControllers(['B4.controller.FajjlPrivjazannyjjKDokumentuList', 'B4.controller.DokumentyZUList']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Реестр Файл, привязанный к документу
        element = form.down('[name=FajjlPrivjazannyjjKDokumentuList]');
        ctrl = me.getController('B4.controller.FajjlPrivjazannyjjKDokumentuList');
        element.data.set('Document_Id', rec.get('Id') || 0);
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(element);
        // Реестр ЗУ (ДокументыЗУ)
        element = form.down('[name=DokumentyZUList]');
        ctrl = me.getController('B4.controller.DokumentyZUList');
        element.data.set('doc1_Id', rec.get('Id') || 0);
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['Type_Id'])) {
            this.preventFieldInput(form, ['[name=Type]']);
        }
        if (!Ext.isEmpty(ctxParams['Signatory_Id'])) {
            this.preventFieldInput(form, ['[name=Signatory]']);
        }
        if (!Ext.isEmpty(ctxParams['Subject_Id'])) {
            this.preventFieldInput(form, ['[name=Subject]']);
        }
        if (isNewRecord == true) {
            form.grid_FajjlPrivjazannyjjKDokumentuList.disableGrid();
            form.grid_DokumentyZUList.disableGrid();
        } else {
            form.grid_FajjlPrivjazannyjjKDokumentuList.enable();
            form.grid_FajjlPrivjazannyjjKDokumentuList.getStore().load();
            form.grid_DokumentyZUList.enable();
            form.grid_DokumentyZUList.getStore().load();
        }
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Реестр Файл, привязанный к документу
        element = view.down('[name=FajjlPrivjazannyjjKDokumentuList]');
        ctrl = me.getController('B4.controller.FajjlPrivjazannyjjKDokumentuList');
        ctrl.connectView(element, view.ctxKey);
        // Реестр ЗУ (ДокументыЗУ)
        element = view.down('[name=DokumentyZUList]');
        ctrl = me.getController('B4.controller.DokumentyZUList');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});