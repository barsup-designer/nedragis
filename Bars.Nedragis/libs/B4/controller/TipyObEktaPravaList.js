Ext.define('B4.controller.TipyObEktaPravaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.TipyObEktaPravaList',
        'B4.model.TipyObEktaPravaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-tipyobektapravalist',
    viewDataName: 'TipyObEktaPravaList',
    // набор описаний действий реестра
    actions: {
        'Addition-TipyObEktaPravaEditor-InWindow': {
            'editor': 'TipyObEktaPravaEditor',
            'editorAlias': 'rms-tipyobektapravaeditor',
            'editorUid': '0f659b89-1171-4d59-9ee0-edcda09c5044',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 150,
                'width': 500
            }
        },
        'Editing-TipyObEktaPravaEditor-InWindow': {
            'editor': 'TipyObEktaPravaEditor',
            'editorAlias': 'rms-tipyobektapravaeditor',
            'editorUid': '0f659b89-1171-4d59-9ee0-edcda09c5044',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 150,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-tipyobektapravalist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-TipyObEktaPravaEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-tipyobektapravalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});