Ext.define('B4.view.DokumentyNavigacija', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-dokumentynavigacija',
    title: 'Документы - навигация',
    rmsUid: '610b3a42-df42-456d-a052-db0552cb1c09',
    requires: [
        'B4.view.NavigationPanel'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Dokument_Id',
        'type': 'hidden'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '610b3a42-df42-456d-a052-db0552cb1c09-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'DokumentyNavigacija-container',
        'layout': {
            'type': 'fit'
        },
        items: [{
            'border': false,
            'fieldLabel': 'Панель навигации',
            'items': [],
            'rmsUid': '5d03704c-cb05-4f0b-9663-371e87c63d7f',
            'rootKey': '5d03704c-cb05-4f0b-9663-371e87c63d7f',
            'title': null,
            'treeWidth': 250,
            'xtype': 'rms-navigationpanel'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Dokument_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});