Ext.define('B4.view.RegionalNyjjNadzorVodnykhObEktovEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-regionalnyjjnadzorvodnykhobektoveditor',
    title: 'Форма редактирования Региональный надзор водных объектов',
    rmsUid: '11af8bd4-b527-4bad-907c-75e4db60d043',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'RegionalNyjjNadzorVodnykhObEktov_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1507787714216',
        'modelProperty': 'RegionalNyjjNadzorVodnykhObEktov_Element1507787714216',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507787771917',
        'modelProperty': 'RegionalNyjjNadzorVodnykhObEktov_Element1507787771917',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1507787816822',
        'modelProperty': 'RegionalNyjjNadzorVodnykhObEktov_Element1507787816822',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1507787841624',
        'modelProperty': 'RegionalNyjjNadzorVodnykhObEktov_Element1507787841624',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '11af8bd4-b527-4bad-907c-75e4db60d043-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'RegionalNyjjNadzorVodnykhObEktovEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Номер договора',
            'modelProperty': 'RegionalNyjjNadzorVodnykhObEktov_Element1507787714216',
            'name': 'Element1507787714216',
            'rmsUid': 'c697962c-ac13-4069-a233-adc2146fa259',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата регистрации',
            'format': 'd.m.Y',
            'modelProperty': 'RegionalNyjjNadzorVodnykhObEktov_Element1507787771917',
            'name': 'Element1507787771917',
            'rmsUid': '7bc2ceeb-670b-4504-8987-b42bf7a017f0',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Окончание срока действия',
            'format': 'd.m.Y',
            'modelProperty': 'RegionalNyjjNadzorVodnykhObEktov_Element1507787816822',
            'name': 'Element1507787816822',
            'rmsUid': 'aa813e95-8d47-4812-a841-33cb414b88a4',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Местонахождение',
            'modelProperty': 'RegionalNyjjNadzorVodnykhObEktov_Element1507787841624',
            'name': 'Element1507787841624',
            'rmsUid': 'ad18f575-c9ea-4f29-aa6d-5e96accfa8f3',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'RegionalNyjjNadzorVodnykhObEktov_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1507787714216'));
        } else {}
        return res;
    },
});