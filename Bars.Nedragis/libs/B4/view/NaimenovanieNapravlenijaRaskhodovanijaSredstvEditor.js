Ext.define('B4.view.NaimenovanieNapravlenijaRaskhodovanijaSredstvEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-naimenovanienapravlenijaraskhodovanijasredstveditor',
    title: 'Наименование направления',
    rmsUid: 'ae586287-dc77-432a-9eec-0cc8367eb259',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'NaimenovanieNapravlenijaRaskhodovanijaSredstv_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1455708457805',
        'modelProperty': 'NaimenovanieNapravlenijaRaskhodovanijaSredstv_Element1455708457805',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1511847414376',
        'modelProperty': 'NaimenovanieNapravlenijaRaskhodovanijaSredstv_Element1511847414376',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'ae586287-dc77-432a-9eec-0cc8367eb259-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'NaimenovanieNapravlenijaRaskhodovanijaSredstvEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'modelProperty': 'NaimenovanieNapravlenijaRaskhodovanijaSredstv_Element1455708457805',
            'name': 'Element1455708457805',
            'rmsUid': 'fb16cd5b-f7c0-4161-b7ce-42804a2db7af',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Вид',
            'modelProperty': 'NaimenovanieNapravlenijaRaskhodovanijaSredstv_Element1511847414376',
            'name': 'Element1511847414376',
            'rmsUid': '627f4239-2e08-4180-b10a-0917bbe6d66a',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'NaimenovanieNapravlenijaRaskhodovanijaSredstv_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1455708457805'));
        } else {}
        return res;
    },
});