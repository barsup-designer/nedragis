Ext.define('B4.view.PlatezhPoruchenieEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-platezhporuchenieeditor',
    title: 'Форма редактирования Платежное поручение',
    rmsUid: '9efb2dda-35c6-44ab-8773-dd2158591323',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'PlatezhPoruchenie_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'dattr',
        'modelProperty': 'PlatezhPoruchenie_dattr',
        'type': 'DateField'
    }, {
        'dataIndex': 'suma',
        'modelProperty': 'PlatezhPoruchenie_suma',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '9efb2dda-35c6-44ab-8773-dd2158591323-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'PlatezhPoruchenieEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Идентификатор',
            'margin': '10 220 10 10',
            'modelProperty': 'f63fa4bd-274a-4ce2-bbb6-794dfd41c23a',
            'readOnly': true,
            'rmsUid': 'f63fa4bd-274a-4ce2-bbb6-794dfd41c23a',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата',
            'format': 'd.m.Y',
            'margin': '10 200 10 10',
            'modelProperty': 'PlatezhPoruchenie_dattr',
            'name': 'dattr',
            'rmsUid': 'f4ca64ec-d3a2-4b0d-94fa-97421450a977',
            'xtype': 'datefield'
        }, {
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Дата договора',
            'margin': '10 200 10 10',
            'modelProperty': '5dd7109f-3439-4f98-a3b5-5058064374e0',
            'readOnly': true,
            'rmsUid': '5dd7109f-3439-4f98-a3b5-5058064374e0',
            'xtype': 'textfield'
        }, {
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Номер договора',
            'margin': '10 220 10 10',
            'modelProperty': '39d35a44-a9c4-4433-84f7-b704f3743ffe',
            'readOnly': true,
            'rmsUid': '39d35a44-a9c4-4433-84f7-b704f3743ffe',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Сумма',
            'hideTrigger': true,
            'margin': '10 200 10 10',
            'minValue': 0,
            'modelProperty': 'PlatezhPoruchenie_suma',
            'name': 'suma',
            'rmsUid': 'c7fee6e8-eb1f-45d8-8622-bdbf50fd604f',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'PlatezhPoruchenie_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});