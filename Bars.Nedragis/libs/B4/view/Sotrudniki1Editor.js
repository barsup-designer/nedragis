Ext.define('B4.view.Sotrudniki1Editor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-sotrudniki1editor',
    title: 'Форма редактирования Сотрудники',
    rmsUid: '7471584e-f4de-43dd-ae67-35cad87871fa',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Sotrudniki1_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1474352136232',
        'modelProperty': 'Sotrudniki1_Element1474352136232',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '7471584e-f4de-43dd-ae67-35cad87871fa-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'Sotrudniki1Editor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'ФИО',
            'modelProperty': 'Sotrudniki1_Element1474352136232',
            'name': 'Element1474352136232',
            'rmsUid': '37355298-777c-426d-9e4d-1a312cdd1091',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Sotrudniki1_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});