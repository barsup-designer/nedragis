Ext.define('B4.view.ProektyEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-proektyeditor',
    title: 'Форма редактирования Проекты',
    rmsUid: '16a3a0ff-c3fe-4109-996a-e5478a02e3ba',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Proekty_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1511335229126',
        'modelProperty': 'Proekty_Element1511335229126',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '16a3a0ff-c3fe-4109-996a-e5478a02e3ba-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ProektyEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование проекта',
            'modelProperty': 'Proekty_Element1511335229126',
            'name': 'Element1511335229126',
            'rmsUid': '6b9513dd-7f7d-4247-b619-6813c9b41788',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Proekty_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1511335229126'));
        } else {}
        return res;
    },
});