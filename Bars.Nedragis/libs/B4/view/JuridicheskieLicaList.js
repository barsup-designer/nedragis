Ext.define('B4.view.JuridicheskieLicaList', {
    'alias': 'widget.rms-juridicheskielicalist',
    'dataSourceUid': '252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.JuridicheskieLicaListModel',
    'stateful': true,
    'title': 'Реестр Юридические лица',
    requires: [
        'B4.model.JuridicheskieLicaListModel',
        'B4.ux.grid.plugin.DataExport',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-JuridicheskieLicaEditor2-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-JuLRedaktirovanie-Default',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }, {
        'handler': function() {
            me = this.up('rms-juridicheskielicalist'); /*br*/
            if (!me.getSelectionModel().lastSelected) /*br*/
            return; /*br*/
            var formData = new FormData; /*br*/
            formData.append('id', me.getSelectionModel().lastSelected.raw.Id); /*br*/
            formData.append('registry', 'Юридические лица'); /*br*/
            $.ajax({ /*br*/
                url: 'Copy/CopyRaw',
                /*br*/
                data: formData,
                /*br*/
                processData: false,
                /*br*/
                contentType: false,
                /*br*/
                type: 'POST',
                /*br*/
                success: function(respData) { /*br*/
                    me.store.reload(); /*br*/
                },
                /*br*/
                error: function(xhr, status, data) { /*br*/
                    /*br*/
                } /*br*/
            });
        },
        'hidden': false,
        'iconCls': 'content-img-icons-page_copy-png',
        'itemId': 'Custom',
        'rmsUid': null,
        'text': 'Копировать строку'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'OKATO_name1',
                'dataIndexAbsoluteUid': 'FieldPath://252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f$f0b4bfc8-c239-4416-a46e-40b9d6783b78$1d67444a-bfea-4a7f-8add-1f6e1362b9a0',
                'dataIndexRelativePath': 'OKATO.name1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '77ccee4a-58d8-4034-a91d-c6c550f40ee3',
                'sortable': true,
                'text': 'ОКАТО',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Representation',
                'dataIndexAbsoluteUid': 'FieldPath://252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f$6f8b2489-4290-4747-93ba-ff199586d3bb',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 2,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5a337cde-3ed5-4c5a-9844-0b17274e5581',
                'sortable': true,
                'text': 'Краткое наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Fullname',
                'dataIndexAbsoluteUid': 'FieldPath://252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f$f4bb14d7-eefc-49fc-b7d9-a28420f04bea',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 3,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '32ed9836-2282-4109-93dd-a4e5a466b0a0',
                'sortable': true,
                'text': 'Полное наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'ogrn',
                'dataIndexAbsoluteUid': 'FieldPath://252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f$a9e4705d-7b4f-4def-810f-122f0c5044c1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '73f4608b-1bde-4482-851d-6b760f1c4b9d',
                'sortable': true,
                'text': 'ОГРН',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'INN1',
                'dataIndexAbsoluteUid': 'FieldPath://252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f$22fe3d6a-1012-4408-b751-c7f03d4d04f6',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'e58468ce-2ec1-42ac-b4ec-c421d72c18ee',
                'sortable': true,
                'text': 'ИНН',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'KPP1',
                'dataIndexAbsoluteUid': 'FieldPath://252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f$636b8684-7724-4d1a-bd69-1bb828e16569',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '8d47ad74-16f9-4e90-8d90-bfc7e99df6ab',
                'sortable': true,
                'text': 'КПП',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'DateReg',
                'dataIndexAbsoluteUid': 'FieldPath://252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f$894edac0-66d8-479b-89d0-e7c1f498bdc7',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'd2e22fe3-1112-44c1-bab8-c9a593ba79c1',
                'sortable': true,
                'text': 'Дата гос. регистрации',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'CapitalStock',
                'dataIndexAbsoluteUid': 'FieldPath://252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f$9ffc7a90-3074-4f35-88b9-3a77741ceb41',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a95e3ef7-c786-4ebf-9e70-2f94f5463e4c',
                'sortable': true,
                'text': 'Уставной капитал, руб.',
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'CapitalPeople',
                'dataIndexAbsoluteUid': 'FieldPath://252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f$b96aa57d-068f-4186-a357-828a1664f300',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '76b4d8c9-798b-4a08-9533-7d5f02b33032',
                'sortable': true,
                'text': 'Среднесписочный состав, чел.',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }, {
                'ptype': 'gridDataExport',
                'reportTitle': 'Юридические лица'
            }]
        });
        me.callParent(arguments);
    }
});