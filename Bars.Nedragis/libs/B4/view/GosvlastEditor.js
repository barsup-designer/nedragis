Ext.define('B4.view.GosvlastEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-gosvlasteditor',
    title: 'Форма редактирования Госвласть',
    rmsUid: '6e48a104-ec8b-481c-bed8-d96f663f69fc',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Gosvlast_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1474350949546',
        'modelProperty': 'Gosvlast_Element1474350949546',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1474350976371',
        'modelProperty': 'Gosvlast_Element1474350976371',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '6e48a104-ec8b-481c-bed8-d96f663f69fc-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'GosvlastEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'modelProperty': 'Gosvlast_Element1474350949546',
            'name': 'Element1474350949546',
            'rmsUid': '03f0f7f0-abdc-42f6-ac34-7c857a5d89ad',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Полное наименование',
            'modelProperty': 'Gosvlast_Element1474350976371',
            'name': 'Element1474350976371',
            'rmsUid': '8eff27e8-9762-4bbe-93df-36e614d973cd',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Gosvlast_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1474350949546'));
        } else {}
        return res;
    },
});