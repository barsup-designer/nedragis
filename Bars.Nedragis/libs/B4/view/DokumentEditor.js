Ext.define('B4.view.DokumentEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-dokumenteditor',
    title: 'Форма редактирования Документ',
    rmsUid: 'a4178b63-4c2d-4869-81ca-95523ab4036d',
    requires: [
        'B4.form.PickerField',
        'B4.model.JuLRedaktirovanieModel',
        'B4.model.JuridicheskieLicaListModel',
        'B4.model.TipyDokumentovEditorModel',
        'B4.model.TipyDokumentovListModel',
        'B4.view.DokumentyZUList',
        'B4.view.FajjlPrivjazannyjjKDokumentuList',
        'B4.view.JuridicheskieLicaList',
        'B4.view.TipyDokumentovList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Dokument_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Type',
        'modelProperty': 'Dokument_Type',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Date',
        'modelProperty': 'Dokument_Date',
        'type': 'DateField'
    }, {
        'dataIndex': 'Number',
        'modelProperty': 'Dokument_Number',
        'type': 'TextField'
    }, {
        'dataIndex': 'ActualDate',
        'modelProperty': 'Dokument_ActualDate',
        'type': 'DateField'
    }, {
        'dataIndex': 'Signatory',
        'modelProperty': 'Dokument_Signatory',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Subject',
        'modelProperty': 'Dokument_Subject',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Basis',
        'modelProperty': 'Dokument_Basis',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'a4178b63-4c2d-4869-81ca-95523ab4036d-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'DokumentEditor-container',
        'autoScroll': true,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 0'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Тип документа',
                    'flex': 1,
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'listView': 'B4.view.TipyDokumentovList',
                    'listViewCtl': 'B4.controller.TipyDokumentovList',
                    'margin': '10 0 10 0',
                    'maximizable': true,
                    'model': 'B4.model.TipyDokumentovListModel',
                    'modelProperty': 'Dokument_Type',
                    'name': 'Type',
                    'rmsUid': '1fca0c02-f035-4319-bc15-2a827b29d4c0',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Тип документа',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Дата документа',
                    'flex': 1,
                    'format': 'd.m.Y',
                    'margin': '12 0 0 0',
                    'modelProperty': 'Dokument_Date',
                    'name': 'Date',
                    'rmsUid': 'f1c147fa-42d5-4f2f-8467-90be2c023460',
                    'xtype': 'datefield'
                }],
                'layout': 'anchor',
                'margin': '10 0 0 10',
                'rmsUid': '0030791c-4d63-4bfb-988e-9b9c64b7790a',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 0 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Номер документа',
                    'flex': 1,
                    'margin': '5 0 5 5',
                    'modelProperty': 'Dokument_Number',
                    'name': 'Number',
                    'rmsUid': '90d69d7f-7054-4979-b389-acf15aba9cb6',
                    'xtype': 'textfield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Дата вступления в силу',
                    'format': 'd.m.Y',
                    'modelProperty': 'Dokument_ActualDate',
                    'name': 'ActualDate',
                    'rmsUid': '6748bec2-b33d-42eb-8e3a-cbb13af12b17',
                    'xtype': 'datefield'
                }],
                'layout': 'anchor',
                'margin': '10 10 0 10',
                'rmsUid': '375dbfdf-2290-4ec5-99a3-788612e64a2a',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'a9b67c40-c087-41c1-89ba-b20368fab4ee',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'layout': {
                'type': 'fit'
            },
            'margin': '0 0 5 0',
            'rmsUid': '7342e49d-b132-4281-ae5d-3531d355f302',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Основание пользованием',
            'items': [{
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Орган, подписавший документ',
                'flex': 1,
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.JuridicheskieLicaList',
                'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                'margin': '0 5 0 5',
                'maximizable': true,
                'model': 'B4.model.JuridicheskieLicaListModel',
                'modelProperty': 'Dokument_Signatory',
                'name': 'Signatory',
                'rmsUid': '68dcb983-1063-42e5-8119-8637636c62c8',
                'textProperty': 'OKATO_name1',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Орган, подписавший документ',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }, {
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Юридическое лицо',
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.JuridicheskieLicaList',
                'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                'margin': '5 5 15 5',
                'maximizable': true,
                'model': 'B4.model.JuridicheskieLicaListModel',
                'modelProperty': 'Dokument_Subject',
                'name': 'Subject',
                'rmsUid': 'f5febecb-309b-45e1-a77c-5e3dcebdd4df',
                'textProperty': 'OKATO_name1',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Юридическое лицо',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Основание',
                'flex': 1,
                'margin': '0 5 0 5',
                'modelProperty': 'Dokument_Basis',
                'name': 'Basis',
                'rmsUid': '2883f083-cff4-4ccc-8dbb-0c5c189a38ba',
                'xtype': 'textfield'
            }],
            'layout': {
                'type': 'fit'
            },
            'margin': '0 0 5 0',
            'rmsUid': '21c4c46e-b032-41a8-bad2-076d20606e21',
            'title': 'Основание пользованием',
            'xtype': 'fieldset'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'autoScroll': true,
                'dockedItems': [],
                'fieldLabel': 'Файлы',
                'items': [{
                    '$EntityFilter': {
                        'LinkFilter': {
                            "Group": 3,
                            "Operand": 0,
                            "DataIndex": null,
                            "DataIndexType": null,
                            "Value": null,
                            "Filters": [{
                                "Group": 0,
                                "Operand": 0,
                                "DataIndex": "FieldPath://55caa4f7-4f96-47d2-a66a-4d16b044a7cf$8262a22a-9149-4329-98ec-407a445b09be$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id",
                                "DataIndexType": null,
                                "Value": "@Dokument_Id",
                                "Filters": null
                            }]
                        }
                    },
                    'closable': false,
                    'header': false,
                    'height': 200,
                    'margin': '0 0 5 0',
                    'name': 'FajjlPrivjazannyjjKDokumentuList',
                    'rmsUid': '45300e66-933d-474a-b396-a088b63574a3',
                    'title': null,
                    'xtype': 'rms-fajjlprivjazannyjjkdokumentulist'
                }],
                'layout': {
                    'type': 'fit'
                },
                'rmsUid': '34e4af06-f86b-4bdf-8c23-b1e1e442eaf7',
                'title': 'Файлы',
                'xtype': 'panel'
            }, {
                'autoScroll': true,
                'dockedItems': [],
                'fieldLabel': 'Земельные участки',
                'items': [{
                    '$EntityFilter': {
                        'LinkFilter': {
                            "Group": 3,
                            "Operand": 0,
                            "DataIndex": null,
                            "DataIndexType": null,
                            "Value": null,
                            "Filters": [{
                                "Group": 0,
                                "Operand": 0,
                                "DataIndex": "FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$b0627770-34ec-4c22-8112-78c21088ef09$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id",
                                "DataIndexType": null,
                                "Value": "@Dokument_Id",
                                "Filters": null
                            }]
                        }
                    },
                    'closable': false,
                    'header': false,
                    'height': 200,
                    'name': 'DokumentyZUList',
                    'rmsUid': '3ac0d625-6f1b-47ca-b871-c9407671cb87',
                    'title': null,
                    'xtype': 'rms-dokumentyzulist'
                }],
                'layout': {
                    'type': 'fit'
                },
                'rmsUid': 'affa7803-a193-467a-8b86-86ea8905f739',
                'title': 'Земельные участки',
                'xtype': 'panel'
            }],
            'layout': {
                'type': 'accordion'
            },
            'rmsUid': 'c07aeb21-db54-4597-b448-29c3ff432960',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Dokument_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
        me.grid_FajjlPrivjazannyjjKDokumentuList = me.down('rms-fajjlprivjazannyjjkdokumentulist[name=FajjlPrivjazannyjjKDokumentuList]');
        me.grid_DokumentyZUList = me.down('rms-dokumentyzulist[name=DokumentyZUList]');
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle('Изменение документа');
        } else {
            me.setTitle('Создание документа');
        }
        return res;
    },
});