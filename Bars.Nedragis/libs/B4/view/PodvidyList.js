Ext.define('B4.view.PodvidyList', {
    'alias': 'widget.rms-podvidylist',
    'dataSourceUid': '71b344e1-9560-43aa-90d5-b5ea06bf434e',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.PodvidyListModel',
    'stateful': true,
    'title': 'Реестр Подвиды',
    requires: [
        'B4.model.PodvidyListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-PodvidyEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-PodvidyEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Element1474267693056',
                'dataIndexAbsoluteUid': 'FieldPath://71b344e1-9560-43aa-90d5-b5ea06bf434e$3adac4b4-f38f-4571-af4f-ba58ff616ffa',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '232ed86e-5480-4f0c-a8a4-24a36502e437',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1474349306046_Element1474020299386',
                'dataIndexAbsoluteUid': 'FieldPath://71b344e1-9560-43aa-90d5-b5ea06bf434e$41be5a35-d47a-4fdf-8ada-af29c956e8c7$b5a48978-083d-4fd5-853f-117af3c5798e',
                'dataIndexRelativePath': 'Element1474349306046.Element1474020299386',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd334a927-8b31-4a0d-be06-69981f98517e',
                'sortable': true,
                'text': 'вид.Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});