Ext.define('B4.view.OrganizaciiEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-organizaciieditor',
    title: 'Форма редактирования Организации',
    rmsUid: 'd032a020-5c30-4d8a-9a3b-1907940cac53',
    requires: [
        'B4.form.PickerField',
        'B4.model.NaimenovanieMOEditorModel',
        'B4.model.NaimenovanieMOListModel',
        'B4.view.NaimenovanieMOList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Organizacii_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1511331964541',
        'modelProperty': 'Organizacii_Element1511331964541',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1511331983555',
        'modelProperty': 'Organizacii_Element1511331983555',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'd032a020-5c30-4d8a-9a3b-1907940cac53-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OrganizaciiEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'modelProperty': 'Organizacii_Element1511331964541',
            'name': 'Element1511331964541',
            'rmsUid': '64e0b549-f5f9-40b1-9f4c-68dced028de9',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'мо',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.NaimenovanieMOList',
            'listViewCtl': 'B4.controller.NaimenovanieMOList',
            'model': 'B4.model.NaimenovanieMOListModel',
            'modelProperty': 'Organizacii_Element1511331983555',
            'name': 'Element1511331983555',
            'rmsUid': 'dc883c7b-82b6-49a6-a5f8-153e667a3ccd',
            'textProperty': 'Element1455707802552',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'мо',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Organizacii_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1511331964541'));
        } else {}
        return res;
    },
});