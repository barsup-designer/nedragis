Ext.define('B4.view.OtdelPodgotovkiInformaciiList', {
    'alias': 'widget.rms-otdelpodgotovkiinformaciilist',
    'dataSourceUid': 'ef5acf69-1403-413d-ac6e-11caae67cfa8',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.OtdelPodgotovkiInformaciiListModel',
    'stateful': true,
    'title': 'Реестр Отдел подготовки информации',
    requires: [
        'B4.model.OtdelPodgotovkiInformaciiListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-OtdelPodgotovkiInformaciiEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-OtdelPodgotovkiInformaciiEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Element1510134896283',
                'dataIndexAbsoluteUid': 'FieldPath://ef5acf69-1403-413d-ac6e-11caae67cfa8$7e95bdea-1ebc-4988-a9a2-4e73dce84535',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0e7ccd86-897b-459a-bab0-d9591c561c50',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер п/п',
                'width': 40,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1510135000178_VidRabotOPINF222',
                'dataIndexAbsoluteUid': 'FieldPath://ef5acf69-1403-413d-ac6e-11caae67cfa8$5979a624-309a-4045-95ca-bb02a2831f8a$c2c721fc-08af-4176-a058-c8add2eb2efa',
                'dataIndexRelativePath': 'Element1510135000178.VidRabotOPINF222',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'c0f2bf3b-4a68-4392-a51e-3f807a69ebbd',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Вид работ',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1510135042374_Element1510134562537',
                'dataIndexAbsoluteUid': 'FieldPath://ef5acf69-1403-413d-ac6e-11caae67cfa8$2bca0ac8-fc0b-4d1a-84ee-ba86f65548fa$824011ee-c2df-41ef-b2d0-a051aba5c1cd',
                'dataIndexRelativePath': 'Element1510135042374.Element1510134562537',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'eb88f1e4-a728-4416-9383-e3acdfe919aa',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Подвид работ',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1510135074618',
                'dataIndexAbsoluteUid': 'FieldPath://ef5acf69-1403-413d-ac6e-11caae67cfa8$8e1f35d8-fbac-435e-8c66-54ccf74a8dc8',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'e90a021e-e14d-415e-8ca4-b2f7523ef799',
                'sortable': true,
                'text': 'Источник поступления информации',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1510135121705_Element1474352136232',
                'dataIndexAbsoluteUid': 'FieldPath://ef5acf69-1403-413d-ac6e-11caae67cfa8$4376a9f6-b33d-46d0-bf82-5765836fd5ca$5c9e0c3e-7dcb-483d-89fd-a12dacaa7279',
                'dataIndexRelativePath': 'Element1510135121705.Element1474352136232',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0824bd71-6d9b-46c5-a502-952e5b2e638e',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Исполнитель',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1510135249843',
                'dataIndexAbsoluteUid': 'FieldPath://ef5acf69-1403-413d-ac6e-11caae67cfa8$3d267e74-2a70-4a0a-879d-1d6570650bbc',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b5397769-406a-4306-a295-0d00535db478',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Количество заявок',
                'width': 70,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1510135325790',
                'dataIndexAbsoluteUid': 'FieldPath://ef5acf69-1403-413d-ac6e-11caae67cfa8$b24f2f60-8a4d-44f7-9225-1b37b03d1613',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'cfdc357c-0665-4601-98f4-d37c88677a6c',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Количество листов',
                'width': 70,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1510135360701',
                'dataIndexAbsoluteUid': 'FieldPath://ef5acf69-1403-413d-ac6e-11caae67cfa8$b6921b85-f07a-4d41-a252-b0ae59362473',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b2bb34b5-07ac-4407-99db-594e38ec8a7b',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер документа',
                'width': 70,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1510135484012',
                'dataIndexAbsoluteUid': 'FieldPath://ef5acf69-1403-413d-ac6e-11caae67cfa8$53a2c08c-4679-4a4c-aa8d-772f8f4b8caf',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '608be445-a165-40a9-9cc7-dd7ec18f51e6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Количество папок',
                'width': 70,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1510135527140',
                'dataIndexAbsoluteUid': 'FieldPath://ef5acf69-1403-413d-ac6e-11caae67cfa8$ad4418a4-8c8e-4fa0-b765-98bdbc8e37c8',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5a253ced-6ebd-4a7a-b757-722ddc56ce1f',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Количество файлов',
                'width': 70,
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});