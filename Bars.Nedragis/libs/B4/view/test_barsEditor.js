Ext.define('B4.view.test_barsEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-test_barseditor',
    title: 'Форма редактирования test_bars',
    rmsUid: '1e97280b-1ec4-45bb-a193-22fceb8b3a1b',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'test_bars_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1481264972872',
        'modelProperty': 'test_bars_Element1481264972872',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1496214131857',
        'modelProperty': 'test_bars_Element1496214131857',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '1e97280b-1ec4-45bb-a193-22fceb8b3a1b-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'test_barsEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'площадь',
            'modelProperty': 'test_bars_Element1481264972872',
            'name': 'Element1481264972872',
            'rmsUid': '751519a6-780f-4f5c-af88-aeb5c3891a5a',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Test',
            'modelProperty': 'test_bars_Element1496214131857',
            'name': 'Element1496214131857',
            'rmsUid': 'c2730a8d-b92d-4c9a-a2b5-049354344371',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'test_bars_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});