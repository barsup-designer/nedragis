Ext.define('B4.view.PodvidyEditor2', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-podvidyeditor2',
    title: 'Форма редактирования Подвиды2',
    rmsUid: 'a1c3d14d-4e0c-4e2a-8877-5ab97a0aab70',
    requires: [
        'B4.form.PickerField',
        'B4.model.VidyRabotEditorModel',
        'B4.model.VidyRabotListModel',
        'B4.view.VidyRabotList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Podvidy_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Id',
        'modelProperty': 'Podvidy_Id',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1474267693056',
        'modelProperty': 'Podvidy_Element1474267693056',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1474349306046',
        'modelProperty': 'Podvidy_Element1474349306046',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'a1c3d14d-4e0c-4e2a-8877-5ab97a0aab70-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'PodvidyEditor2-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': false,
            'allowDecimals': false,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Идентификатор',
            'minValue': 0,
            'modelProperty': 'Podvidy_Id',
            'name': 'Id',
            'rmsUid': '764e5aad-1d65-429d-a69b-f8db20529994',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'modelProperty': 'Podvidy_Element1474267693056',
            'name': 'Element1474267693056',
            'rmsUid': 'b7f2f2e8-b740-40c3-a7ea-4f706f3a080c',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'вид',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.VidyRabotList',
            'listViewCtl': 'B4.controller.VidyRabotList',
            'model': 'B4.model.VidyRabotListModel',
            'modelProperty': 'Podvidy_Element1474349306046',
            'name': 'Element1474349306046',
            'rmsUid': 'fc4d3472-7d6d-4a6c-9057-d4b32b443ad0',
            'textProperty': 'Element1474020299386',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'вид',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            '$EntityFilter': {
                'LinkFilter': {
                    "Group": 3,
                    "Operand": 0,
                    "DataIndex": null,
                    "DataIndexType": null,
                    "Value": null,
                    "Filters": [{
                        "Group": 0,
                        "Operand": 0,
                        "DataIndex": "FieldPath://f1b2473f-a514-468d-95fc-58e1fbb40f2b$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id",
                        "DataIndexType": null,
                        "Value": "@Podvidy_Element1474349306046_Id",
                        "Filters": null
                    }]
                }
            },
            'anchor': '100%',
            'closable': false,
            'header': false,
            'name': 'VidyRabotList',
            'rmsUid': '4a6af4b4-ebba-4f8b-b48e-7d0d1115e539',
            'title': null,
            'xtype': 'rms-vidyrabotlist'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Podvidy_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
        me.grid_VidyRabotList = me.down('rms-vidyrabotlist[name=VidyRabotList]');
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1474267693056'));
        } else {}
        return res;
    },
});