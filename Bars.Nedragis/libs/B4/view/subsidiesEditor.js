Ext.define('B4.view.subsidiesEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-subsidieseditor',
    title: 'Форма редактирования Субсидии',
    rmsUid: '9bb20aab-4f3d-4023-88ec-8f20243f4bae',
    requires: [
        'B4.form.PickerField',
        'B4.model.NaimenovanieMOEditorModel',
        'B4.model.NaimenovanieMOListModel',
        'B4.model.NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel',
        'B4.model.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel',
        'B4.view.NaimenovanieMOList',
        'B4.view.NaimenovanieNapravlenijaRaskhodovanijaSredstvList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'subsidies_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'NAME_MO',
        'modelProperty': 'subsidies_NAME_MO',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Element1455708491495',
        'modelProperty': 'subsidies_Element1455708491495',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Element1455709095130',
        'modelProperty': 'subsidies_Element1455709095130',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1455709437278',
        'modelProperty': 'subsidies_Element1455709437278',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1455709476348',
        'modelProperty': 'subsidies_Element1455709476348',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1456977571408',
        'modelProperty': 'subsidies_Element1456977571408',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1456979244748',
        'modelProperty': 'subsidies_Element1456979244748',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1456979287919',
        'modelProperty': 'subsidies_Element1456979287919',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1456982258068',
        'modelProperty': 'subsidies_Element1456982258068',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1456982634223',
        'modelProperty': 'subsidies_Element1456982634223',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1456982674444',
        'modelProperty': 'subsidies_Element1456982674444',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1456982760976',
        'modelProperty': 'subsidies_Element1456982760976',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1456982777199',
        'modelProperty': 'subsidies_Element1456982777199',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1456982822174',
        'modelProperty': 'subsidies_Element1456982822174',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1456982887080',
        'modelProperty': 'subsidies_Element1456982887080',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1456982966340',
        'modelProperty': 'subsidies_Element1456982966340',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1456983021798',
        'modelProperty': 'subsidies_Element1456983021798',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1456983065031',
        'modelProperty': 'subsidies_Element1456983065031',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '9bb20aab-4f3d-4023-88ec-8f20243f4bae-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'subsidiesEditor-container',
        'autoScroll': true,
        'bodyPadding': '0 20 0 5',
        'layout': {
            'type': 'anchor'
        },
        'margin': '10 20 0 10',
        'padding': '0 20 0 0',
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Наименование муниципального образования',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.NaimenovanieMOList',
            'listViewCtl': 'B4.controller.NaimenovanieMOList',
            'maximizable': true,
            'model': 'B4.model.NaimenovanieMOListModel',
            'modelProperty': 'subsidies_NAME_MO',
            'name': 'NAME_MO',
            'rmsUid': '816b1a2b-9cae-43f9-90cb-94f4862f631b',
            'textProperty': 'Element1455707802552',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Наименование муниципального образования',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Наименование направления расходования средств',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.NaimenovanieNapravlenijaRaskhodovanijaSredstvList',
            'listViewCtl': 'B4.controller.NaimenovanieNapravlenijaRaskhodovanijaSredstvList',
            'model': 'B4.model.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel',
            'modelProperty': 'subsidies_Element1455708491495',
            'name': 'Element1455708491495',
            'rmsUid': 'ae4dd62f-3002-40e4-9b71-ff6241c2b844',
            'textProperty': 'Element1455708457805',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Наименование направления расходования средств',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование объекта, с группировкой по населенным пунктам',
            'modelProperty': 'subsidies_Element1455709095130',
            'name': 'Element1455709095130',
            'rmsUid': '5fc7de46-db00-4660-b246-e21ac5c450bc',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Реквизиты свидетельства о государственной регистрации',
            'modelProperty': 'subsidies_Element1455709437278',
            'name': 'Element1455709437278',
            'rmsUid': 'fd3b849e-1f95-4eff-9774-3aa11547313d',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Производственный процесс СМР ПИР',
            'modelProperty': 'subsidies_Element1455709476348',
            'name': 'Element1455709476348',
            'rmsUid': 'ca009da3-05cc-4075-92b8-24e3fa341bab',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Вид работ ',
            'modelProperty': 'subsidies_Element1456977571408',
            'name': 'Element1456977571408',
            'rmsUid': '29aad86a-23ac-4d77-aa0f-6a0d0c027a8a',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата начала выполнения работ',
            'format': 'd.m.Y',
            'modelProperty': 'subsidies_Element1456979244748',
            'name': 'Element1456979244748',
            'rmsUid': '218c0fd8-a4be-4295-8774-9946eaa141ae',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата окончания выполнения работ',
            'format': 'd.m.Y',
            'modelProperty': 'subsidies_Element1456979287919',
            'name': 'Element1456979287919',
            'rmsUid': 'cf12ab37-2d9e-4431-a188-f1977b43a59a',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'Мощность(км)',
            'modelProperty': 'subsidies_Element1456982258068',
            'name': 'Element1456982258068',
            'rmsUid': '0e88db2c-1395-4747-9e2d-7549dce67304',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'Мощность(м2)',
            'modelProperty': 'subsidies_Element1456982634223',
            'name': 'Element1456982634223',
            'rmsUid': 'fd14d6c9-334e-4f13-926c-b854bf2a0a0b',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Максимальная цена контракта(руб.)',
            'modelProperty': 'subsidies_Element1456982674444',
            'name': 'Element1456982674444',
            'rmsUid': '5817b927-7b4a-4421-bddc-67b811c5f223',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'км',
            'modelProperty': 'subsidies_Element1456982760976',
            'name': 'Element1456982760976',
            'rmsUid': 'a23d56e3-b2d0-416c-9dc0-f65326d89e61',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'м2',
            'modelProperty': 'subsidies_Element1456982777199',
            'name': 'Element1456982777199',
            'rmsUid': 'b4737eec-5ebd-4b0a-b61d-0a24b4a520cb',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Количество объектов с положительным заключением',
            'minValue': 0,
            'modelProperty': 'subsidies_Element1456982822174',
            'name': 'Element1456982822174',
            'rmsUid': 'd28c6035-a566-4842-ba16-b66f7f78aa75',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'Доля автомобильных дорог(%)',
            'modelProperty': 'subsidies_Element1456982887080',
            'name': 'Element1456982887080',
            'rmsUid': '7867d610-c8b6-43f7-bc53-b7d33c7e1e2e',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Объем финансирования(Бюджет ЯНАО)',
            'modelProperty': 'subsidies_Element1456982966340',
            'name': 'Element1456982966340',
            'rmsUid': '569630c8-792a-4f15-8202-3a3f9c4a99d8',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Объем финансирования(Бюджет МО)',
            'modelProperty': 'subsidies_Element1456983021798',
            'name': 'Element1456983021798',
            'rmsUid': '9aff1ef1-0ab7-4f7e-bcf7-788b237d04c6',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'Процент долевого участия бюджета МО',
            'modelProperty': 'subsidies_Element1456983065031',
            'name': 'Element1456983065031',
            'rmsUid': '0ba296d2-3775-445c-bcd5-349c70331bb2',
            'xtype': 'numberfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'subsidies_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});