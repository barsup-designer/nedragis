Ext.define('B4.view.OKOF1', {
    'alias': 'widget.rms-okof1',
    'dataSourceUid': '594403ac-d85f-4ef6-8815-bf4c01f3b7f9',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': false,
    'extend': 'B4.base.registry.TreeView',
    'model': 'B4.model.OKOF1Model',
    'parentFieldName': 'parent_id',
    'stateful': true,
    'title': 'ОКОФ',
    requires: [
        'B4.model.OKOF1Model',
        'B4.ux.grid.plugin.DataExport',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'content-img-icons-reload-png',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-add-png',
        'itemId': 'Addition-OKOFEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-edit-png',
        'itemId': 'Editing-OKOFEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-delete-png',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'clasifier_code',
                'dataIndexAbsoluteUid': 'FieldPath://594403ac-d85f-4ef6-8815-bf4c01f3b7f9$e267b6bd-c137-496e-87f7-295177a8b7ca$de4d4f76-5db4-4670-8729-17cd42a01e3f',
                'dataIndexRelativePath': 'clasifier.code',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'fd6c2de8-d531-4c73-9ed8-d9f5a82d6730',
                'sortable': true,
                'text': 'Классификация.Код',
                'xtype': 'treecolumn'
            }, {
                'dataIndex': 'clasifier_name',
                'dataIndexAbsoluteUid': 'FieldPath://594403ac-d85f-4ef6-8815-bf4c01f3b7f9$e267b6bd-c137-496e-87f7-295177a8b7ca$6cc00b9e-2a55-4b59-b182-8af607b62fa3',
                'dataIndexRelativePath': 'clasifier.name',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 2,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0333f746-1fd3-4402-a7c6-0d2fbb56463f',
                'sortable': true,
                'text': 'Классификация.Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://594403ac-d85f-4ef6-8815-bf4c01f3b7f9$68c5f8ed-cccf-4b7a-9b3e-2ca04e22eb5f',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'ecb679fc-138c-4b56-8405-311529c9900e',
                'sortable': true,
                'text': 'Код',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://594403ac-d85f-4ef6-8815-bf4c01f3b7f9$c50ac8dc-07e1-43a6-a3e2-43c64c9567c4',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 2,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b1834d91-8bc2-46a4-8b6f-a9c6d2712d57',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'check_number',
                'dataIndexAbsoluteUid': 'FieldPath://594403ac-d85f-4ef6-8815-bf4c01f3b7f9$dcf91b29-5256-4fd7-97e1-ecdb0a8a626d',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '39f5a8d3-4a53-4749-ab0b-26e384951a66',
                'sortable': true,
                'text': 'Контрольное число',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'ptype': 'gridDataExport',
                'reportTitle': 'ОКОФ'
            }]
        });
        me.callParent(arguments);
    }
});