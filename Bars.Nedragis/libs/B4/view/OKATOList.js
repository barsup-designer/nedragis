Ext.define('B4.view.OKATOList', {
    'alias': 'widget.rms-okatolist',
    'dataSourceUid': '209f5156-b2b9-4a80-b96a-100c2cf0083d',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.OKATOListModel',
    'stateful': true,
    'title': 'Реестр ОКАТО',
    requires: [
        'B4.model.OKATOListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-OKATOEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-OKATOEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://209f5156-b2b9-4a80-b96a-100c2cf0083d$f1c73dea-11f0-43fc-b595-04b84516caea',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '927612ca-e3db-4112-90a1-b2ea6254b95d',
                'sortable': true,
                'text': 'Код',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name1',
                'dataIndexAbsoluteUid': 'FieldPath://209f5156-b2b9-4a80-b96a-100c2cf0083d$1d67444a-bfea-4a7f-8add-1f6e1362b9a0',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '8f346b9d-a961-43a7-9db3-8b70f23cdb29',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'center_name',
                'dataIndexAbsoluteUid': 'FieldPath://209f5156-b2b9-4a80-b96a-100c2cf0083d$687b9925-7fcb-48da-86ae-aa647ff4e01b',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '9df69523-3746-4ee0-bbeb-17c1b5506e8b',
                'sortable': true,
                'text': 'Районный центр',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'parent_id_name1',
                'dataIndexAbsoluteUid': 'FieldPath://209f5156-b2b9-4a80-b96a-100c2cf0083d$1bcffb34-e126-4435-b34a-c7f8f7ec8696$1d67444a-bfea-4a7f-8add-1f6e1362b9a0',
                'dataIndexRelativePath': 'parent_id.name1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd17d6fc4-790a-4204-88cb-71d5a524baa1',
                'sortable': true,
                'text': 'Родительский элемент.Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});