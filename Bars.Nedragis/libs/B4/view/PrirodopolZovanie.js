Ext.define('B4.view.PrirodopolZovanie', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-prirodopolzovanie',
    title: 'Форма отдела "Природопользования"',
    rmsUid: 'c3840f17-1916-4ff9-be6b-499a2ea16441',
    requires: [
        'B4.form.PickerField',
        'B4.model.Otdel1EditorModel',
        'B4.model.Otdel1ListModel',
        'B4.view.Otdel1List'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'monitor_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'otdel',
        'modelProperty': 'monitor_otdel',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'ocenka_vreda',
        'modelProperty': 'monitor_ocenka_vreda',
        'type': 'NumberField'
    }, {
        'dataIndex': 'ocenka_vreda_d',
        'modelProperty': 'monitor_ocenka_vreda_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'raschet_ubytkov',
        'modelProperty': 'monitor_raschet_ubytkov',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1462360050013',
        'modelProperty': 'monitor_Element1462360050013',
        'type': 'NumberField'
    }, {
        'dataIndex': 'priroda_rpoverka_coor',
        'modelProperty': 'monitor_priroda_rpoverka_coor',
        'type': 'NumberField'
    }, {
        'dataIndex': 'priroda_rpoverka_coor_d',
        'modelProperty': 'monitor_priroda_rpoverka_coor_d',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'c3840f17-1916-4ff9-be6b-499a2ea16441-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'PrirodopolZovanie-container',
        'autoScroll': true,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Наименование отдела',
            'idProperty': 'Id',
            'isContextAware': true,
            'labelWidth': 150,
            'listView': 'B4.view.Otdel1List',
            'listViewCtl': 'B4.controller.Otdel1List',
            'margin': '10 35 0 35',
            'maximizable': true,
            'model': 'B4.model.Otdel1ListModel',
            'modelProperty': 'monitor_otdel',
            'name': 'otdel',
            'rmsUid': '4666233e-ad74-489d-a628-c65295d96eb7',
            'textProperty': 'NAME_SP',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 350,
                'maximizable': true,
                'title': 'Наименование отдела',
                'width': 350
            },
            'xtype': 'b4pickerfield'
        }, {
            'anchor': '100%',
            'columnWidth': 1,
            'dockedItems': [],
            'items': [{
                'columnWidth': 1,
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'autoScroll': true,
                    'columnWidth': 1,
                    'dockedItems': [],
                    'fieldLabel': 'Оценка вреда и исчисление размера ущерба от уничтожения объектов животного мира или нарушения среды их обитания на территории ЯНАО',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'columnWidth': 1,
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '2 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_ocenka_vreda',
                        'name': 'ocenka_vreda',
                        'rmsUid': 'a4bba18c-28c4-492c-8057-68c9fbaca7e0',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'columnWidth': 1,
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '2 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_ocenka_vreda_d',
                        'name': 'ocenka_vreda_d',
                        'rmsUid': '73040d03-17ba-4c4a-8002-d6d295ebf4cd',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'layout': {
                        'align': 'middle',
                        'type': null
                    },
                    'rmsUid': '414c8b12-1574-42a1-8480-929d4b1ef8bc',
                    'title': 'Оценка вреда и исчисление размера ущерба от уничтожения объектов животного мира или нарушения среды их обитания на территории ЯНАО',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'columnWidth': '0.90',
                    'dockedItems': [],
                    'fieldLabel': 'Расчет убытков землепользователей ри изъятии, самовольном захвате и порче земельных угодий традиционного природопользования ЯНАО',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'columnWidth': 1,
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '2 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_raschet_ubytkov',
                        'name': 'raschet_ubytkov',
                        'rmsUid': '7e894002-c22c-43af-a66a-bd11e5451522',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'columnWidth': 1,
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '2 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_Element1462360050013',
                        'name': 'Element1462360050013',
                        'rmsUid': '824de177-0da4-4486-bced-192c6d731368',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': '8872ce3a-9c7d-4eb6-9e01-13508848cdf2',
                    'title': 'Расчет убытков землепользователей ри изъятии, самовольном захвате и порче земельных угодий традиционного природопользования ЯНАО',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'columnWidth': '0.90',
                    'dockedItems': [],
                    'fieldLabel': 'Проверка координат и подготовка схем расположения участков водопользования для получения договоров и решений',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'columnWidth': 1,
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '2 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_priroda_rpoverka_coor',
                        'name': 'priroda_rpoverka_coor',
                        'rmsUid': '23b73f05-8761-4cf0-be95-af48e63341f6',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'columnWidth': 1,
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '2 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_priroda_rpoverka_coor_d',
                        'name': 'priroda_rpoverka_coor_d',
                        'rmsUid': 'f5cef265-ced8-4fab-96cd-aa471ed8a8be',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': 'ad93f220-e50d-4ed5-a791-6fec0be2d200',
                    'title': 'Проверка координат и подготовка схем расположения участков водопользования для получения договоров и решений',
                    'xtype': 'fieldset'
                }],
                'layout': 'anchor',
                'margin': '0 10 0 10',
                'rmsUid': '5807d939-a218-4980-a70e-c69648d484db',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '10 10 0 10',
            'rmsUid': 'f752fbb9-ea5b-42a9-8d6b-2eaffd904d81',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'monitor_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});