Ext.define('B4.view.TestForma', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-testforma',
    title: 'Отделы',
    rmsUid: 'e88098a7-cae7-4e52-9120-22ca81f28fe3',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'monitor_Id',
        'type': 'hidden'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'e88098a7-cae7-4e52-9120-22ca81f28fe3-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'TestForma-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Выберите отдел',
            'items': [{
                'anchor': '100%',
                'dockedItems': [],
                'items': [{
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'anchor': '100%',
                        'dockedItems': [],
                        'items': [{
                            'defaults': {
                                'margin': '5 5 5 5'
                            },
                            'dockedItems': [],
                            'flex': 1,
                            'items': [{
                                'anchor': '100%',
                                'fieldLabel': 'Отдел Недропользования',
                                'rmsUid': 'db486384-2de7-46cf-8118-c02475913a03',
                                'text': 'Отдел Недропользования',
                                'xtype': 'label'
                            }],
                            'layout': 'anchor',
                            'rmsUid': '734c0517-7b5a-432d-9c53-1e8fbaf4aa30',
                            'xtype': 'container'
                        }],
                        'layout': 'hbox',
                        'margin': '12 0 0 5',
                        'rmsUid': 'f7fe97d7-30e3-410b-b3eb-52d2df0b9f1d',
                        'xtype': 'container'
                    }, {
                        'anchor': '100%',
                        'dockedItems': [],
                        'items': [{
                            'defaults': {
                                'margin': '5 5 5 5'
                            },
                            'dockedItems': [],
                            'flex': 1,
                            'items': [{
                                'anchor': '100%',
                                'fieldLabel': 'Отдел Природопользования',
                                'rmsUid': 'a4790e3d-5259-4e28-a330-2e574787a0e3',
                                'text': 'Отдел Природопользования',
                                'xtype': 'label'
                            }],
                            'layout': 'anchor',
                            'rmsUid': 'd0776fb3-a691-417a-8cd8-aff7c6f50af8',
                            'xtype': 'container'
                        }],
                        'layout': 'hbox',
                        'margin': '12 0 0 5',
                        'rmsUid': 'f66efa88-aa8c-4bfc-9731-08eb6e41f1ef',
                        'xtype': 'container'
                    }, {
                        'anchor': '100%',
                        'dockedItems': [],
                        'items': [{
                            'defaults': {
                                'margin': '5 5 5 5'
                            },
                            'dockedItems': [],
                            'flex': 1,
                            'items': [{
                                'anchor': '100%',
                                'fieldLabel': 'Отдел Землеустройства',
                                'rmsUid': '7043ed61-4412-4b19-8261-1644dbb897e7',
                                'text': 'Отдел Землеустройства',
                                'xtype': 'label'
                            }],
                            'layout': 'anchor',
                            'rmsUid': '60cd1a55-43f1-4d4d-8c8b-123ec2bee5dc',
                            'xtype': 'container'
                        }],
                        'layout': 'hbox',
                        'margin': '12 0 0 5',
                        'rmsUid': '318cf9f2-29f5-4a9e-bff7-e7f1d2ed937e',
                        'xtype': 'container'
                    }, {
                        'anchor': '100%',
                        'dockedItems': [],
                        'items': [{
                            'defaults': {
                                'margin': '5 5 5 5'
                            },
                            'dockedItems': [],
                            'flex': 1,
                            'items': [{
                                'anchor': '100%',
                                'fieldLabel': 'Отдел Картографии',
                                'rmsUid': '9f0ab6b6-8835-4f5c-be35-b94b18aae684',
                                'text': 'Отдел Картографии',
                                'xtype': 'label'
                            }],
                            'layout': 'anchor',
                            'rmsUid': '84fd246c-b368-4de0-905a-f4ebb37be29c',
                            'xtype': 'container'
                        }],
                        'layout': 'hbox',
                        'margin': '12 0 0 5',
                        'rmsUid': 'feb1d41f-a59b-4e0d-9d41-2ff9852cc7ec',
                        'xtype': 'container'
                    }],
                    'layout': 'anchor',
                    'rmsUid': 'f6c5feca-1cbd-4a0b-9dd2-0c62d8dc5a56',
                    'xtype': 'container'
                }, {
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'anchor': '100%',
                        'dockedItems': [],
                        'items': [{
                            'defaults': {
                                'margin': '5 5 5 5'
                            },
                            'dockedItems': [],
                            'flex': 1,
                            'items': [{
                                'anchor': '100%',
                                'fieldLabel': 'ОК',
                                'rmsUid': '952cad89-af4c-4341-9695-606b5174e772',
                                'text': 'ОК',
                                'xtype': 'button'
                            }],
                            'layout': 'anchor',
                            'rmsUid': '7812a43a-70e1-4f12-b7a4-3b68e0db4039',
                            'xtype': 'container'
                        }],
                        'layout': 'hbox',
                        'rmsUid': 'c420125c-1cc2-4297-ae70-3d8a9f8b85ab',
                        'xtype': 'container'
                    }, {
                        'anchor': '100%',
                        'dockedItems': [],
                        'items': [{
                            'defaults': {
                                'margin': '5 5 5 5'
                            },
                            'dockedItems': [],
                            'flex': 1,
                            'items': [{
                                'anchor': '100%',
                                'fieldLabel': 'ОК',
                                'rmsUid': '108155db-dc57-44cc-9b78-a4b637692c83',
                                'text': 'ОК',
                                'xtype': 'button'
                            }],
                            'layout': 'anchor',
                            'rmsUid': '481820ec-efb5-4956-9abf-5bd60fbaa78f',
                            'xtype': 'container'
                        }],
                        'layout': 'hbox',
                        'rmsUid': '48922fca-6901-46b8-ad5b-eb62f957440d',
                        'xtype': 'container'
                    }, {
                        'anchor': '100%',
                        'dockedItems': [],
                        'items': [{
                            'defaults': {
                                'margin': '5 5 5 5'
                            },
                            'dockedItems': [],
                            'flex': 1,
                            'items': [{
                                'anchor': '100%',
                                'fieldLabel': 'ОК',
                                'rmsUid': '4b3f79bb-a53d-4988-ae14-fec9084911c0',
                                'text': 'ОК',
                                'xtype': 'button'
                            }],
                            'layout': 'anchor',
                            'rmsUid': '5ece5825-6b35-4085-a413-25321ecc6cd6',
                            'xtype': 'container'
                        }],
                        'layout': 'hbox',
                        'rmsUid': '26d8cff8-ccf4-4eca-b979-23d97b45bc63',
                        'xtype': 'container'
                    }, {
                        'anchor': '100%',
                        'dockedItems': [],
                        'items': [{
                            'defaults': {
                                'margin': '5 5 5 5'
                            },
                            'dockedItems': [],
                            'flex': 1,
                            'items': [{
                                'anchor': '100%',
                                'fieldLabel': 'ОК',
                                'rmsUid': 'b66daf3a-d04f-4a74-b37d-8f65dd41db08',
                                'text': 'ОК',
                                'xtype': 'button'
                            }],
                            'layout': 'anchor',
                            'rmsUid': '3e730e82-c506-4a34-bbf8-59b1b2acf588',
                            'xtype': 'container'
                        }],
                        'layout': 'hbox',
                        'rmsUid': 'e9445c92-01f0-4c15-affb-78ce6ea66aa2',
                        'xtype': 'container'
                    }],
                    'layout': 'anchor',
                    'rmsUid': '9f464daa-9caf-4f2b-908f-6d95bb0c1821',
                    'xtype': 'container'
                }],
                'layout': 'hbox',
                'rmsUid': 'f0b2495f-2d50-4a34-8663-ae8ef031d6e0',
                'xtype': 'container'
            }],
            'margin': '50 20 20 20',
            'rmsUid': 'f1c9b757-aa0f-48b7-9468-ca2e999136af',
            'title': 'Выберите отдел',
            'xtype': 'fieldset'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'monitor_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});