Ext.define('B4.view.Kartografija', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-kartografija',
    title: 'Форма отдела "Картографии"',
    rmsUid: 'eef9d875-1b80-4901-bd2e-bdd681162d46',
    requires: [
        'B4.form.PickerField',
        'B4.model.Otdel1EditorModel',
        'B4.model.Otdel1ListModel',
        'B4.view.Otdel1List'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'monitor_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'otdel',
        'modelProperty': 'monitor_otdel',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Element1462359853756',
        'modelProperty': 'monitor_Element1462359853756',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1462359855708',
        'modelProperty': 'monitor_Element1462359855708',
        'type': 'NumberField'
    }, {
        'dataIndex': 'karta_raion',
        'modelProperty': 'monitor_karta_raion',
        'type': 'NumberField'
    }, {
        'dataIndex': 'karta_raion_d',
        'modelProperty': 'monitor_karta_raion_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'karta_nedra',
        'modelProperty': 'monitor_karta_nedra',
        'type': 'NumberField'
    }, {
        'dataIndex': 'karta_nedra_d',
        'modelProperty': 'monitor_karta_nedra_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'karta_n_p9622',
        'modelProperty': 'monitor_karta_n_p9622',
        'type': 'NumberField'
    }, {
        'dataIndex': 'karta_n_p6180',
        'modelProperty': 'monitor_karta_n_p6180',
        'type': 'NumberField'
    }, {
        'dataIndex': 'shema',
        'modelProperty': 'monitor_shema',
        'type': 'NumberField'
    }, {
        'dataIndex': 'shema_d',
        'modelProperty': 'monitor_shema_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'pecat_g_m',
        'modelProperty': 'monitor_pecat_g_m',
        'type': 'NumberField'
    }, {
        'dataIndex': 'pecat_g_m_d',
        'modelProperty': 'monitor_pecat_g_m_d',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'eef9d875-1b80-4901-bd2e-bdd681162d46-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'Kartografija-container',
        'autoScroll': true,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Наименование отдела',
            'idProperty': 'Id',
            'isContextAware': true,
            'labelWidth': 150,
            'listView': 'B4.view.Otdel1List',
            'listViewCtl': 'B4.controller.Otdel1List',
            'margin': '10 35 0 35',
            'maximizable': true,
            'model': 'B4.model.Otdel1ListModel',
            'modelProperty': 'monitor_otdel',
            'name': 'otdel',
            'rmsUid': '1e94f171-abf5-481d-b7c8-fb0696336526',
            'textProperty': 'NAME_SP',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 350,
                'maximizable': true,
                'title': 'Наименование отдела',
                'width': 350
            },
            'xtype': 'b4pickerfield'
        }, {
            'anchor': '100%',
            'autoScroll': true,
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Обзорная карта ЯНАО',
                    'height': 80,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_Element1462359853756',
                        'name': 'Element1462359853756',
                        'rmsUid': '70fdfa54-5e4e-4150-aa58-02ef7bdb6e30',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '2 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_Element1462359855708',
                        'name': 'Element1462359855708',
                        'rmsUid': 'e63e2ab8-becc-4bbd-a739-74f183cf581a',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'layout': {
                        'align': 'middle',
                        'pack': 'center',
                        'type': null
                    },
                    'margin': '0 5 0 5',
                    'region': 'East',
                    'rmsUid': '1b9ab590-3449-4089-9e07-9bd0b16e0fab',
                    'title': 'Обзорная карта ЯНАО',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Обзорная карта района ЯНАО',
                    'height': 80,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_karta_raion',
                        'name': 'karta_raion',
                        'rmsUid': 'c5a5a233-b007-4a11-a8b7-9fe84f3d7508',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '2 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_karta_raion_d',
                        'name': 'karta_raion_d',
                        'rmsUid': '1cd567c5-928d-4729-9b3c-7e97046c1db5',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'margin': '0 5 0 5',
                    'rmsUid': 'a475c214-0171-4a9a-b117-52266d8830df',
                    'title': 'Обзорная карта района ЯНАО',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Обзорная карта недропользования ЯНАО',
                    'height': 80,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_karta_nedra',
                        'name': 'karta_nedra',
                        'rmsUid': '1c5085eb-f79f-449a-b303-b9de08bf2c0b',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '2 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_karta_nedra_d',
                        'name': 'karta_nedra_d',
                        'rmsUid': 'd2801af6-06fa-42ca-bd10-bfa18ebd8c6a',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'margin': '0 5 0 5',
                    'rmsUid': '689bb0b0-674b-4db7-8ae6-0b89fcc89b62',
                    'title': 'Обзорная карта недропользования ЯНАО',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Карта населенного пункта ЯНАО',
                    'height': 80,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_karta_n_p9622',
                        'name': 'karta_n_p9622',
                        'rmsUid': 'ea2f5e71-dbd4-495c-bff5-514160961dda',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '2 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_karta_n_p6180',
                        'name': 'karta_n_p6180',
                        'rmsUid': '2fcb70df-99ab-4608-8b47-4d8bc5d0629d',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'margin': '0 5 0 5',
                    'rmsUid': '4748e128-6296-428f-aea3-621a372ffa34',
                    'title': 'Карта населенного пункта ЯНАО',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Схемы различного формата',
                    'height': 80,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_shema',
                        'name': 'shema',
                        'rmsUid': 'ed0acc7c-1680-4831-9d08-374ea79987b7',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '2 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_shema_d',
                        'name': 'shema_d',
                        'rmsUid': 'f4246854-0539-46b8-9472-02dc8f5d7ad1',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'margin': '0 5 0 5',
                    'rmsUid': 'b2fffd98-8ddc-4966-89ba-b325c1a99537',
                    'title': 'Схемы различного формата',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Печать графического материала',
                    'height': 80,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_pecat_g_m',
                        'name': 'pecat_g_m',
                        'rmsUid': 'ddfc43cb-bdc7-41b1-a3a1-dcc73835d1c3',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '2 50 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_pecat_g_m_d',
                        'name': 'pecat_g_m_d',
                        'rmsUid': 'db3ef534-e3b7-42d8-aa6b-1357dbfa00cf',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'margin': '0 5 0 5',
                    'rmsUid': 'bd8119dd-b258-41bc-bdf3-71b672cc43c0',
                    'title': 'Печать графического материала',
                    'xtype': 'fieldset'
                }],
                'layout': 'anchor',
                'rmsUid': 'f48ba7df-b3f3-4a4c-80e0-af6a912dd80c',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '20 10 0 10',
            'rmsUid': '027176d3-59c2-43ad-8e2b-6b4c6169dc2c',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'monitor_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});