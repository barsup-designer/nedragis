Ext.define('B4.view.RukovoditeliList2', {
    'alias': 'widget.rms-rukovoditelilist2',
    'dataSourceUid': 'cf26c7ab-6cf7-49ad-888a-7fb500a92d49',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.RukovoditeliList2Model',
    'stateful': true,
    'title': 'Реестр Руководители',
    requires: [
        'B4.enums.Pol',
        'B4.model.RukovoditeliList2Model',
        'B4.ux.grid.plugin.DataExport',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-RukovoditeliEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-RukovoditeliEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }, {
        'handler': function() {
            me = this.up('rms-rukovoditelilist2'); /*br*/
            if (!me.getSelectionModel().lastSelected) /*br*/
            return; /*br*/
            var formData = new FormData; /*br*/
            formData.append('id', me.getSelectionModel().lastSelected.raw.Id); /*br*/
            formData.append('registry', 'Руководители'); /*br*/
            $.ajax({ /*br*/
                url: 'Copy/CopyRaw',
                /*br*/
                data: formData,
                /*br*/
                processData: false,
                /*br*/
                contentType: false,
                /*br*/
                type: 'POST',
                /*br*/
                success: function(respData) { /*br*/
                    me.store.reload(); /*br*/
                },
                /*br*/
                error: function(xhr, status, data) { /*br*/
                    /*br*/
                } /*br*/
            });
        },
        'hidden': false,
        'iconCls': 'content-img-icons-page_copy-png',
        'itemId': 'Custom',
        'rmsUid': null,
        'text': 'Копировать строку'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'YurLitco_Representation',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$c50abffe-a4aa-4ce8-ae37-e8f78c51a056$6f8b2489-4290-4747-93ba-ff199586d3bb',
                'dataIndexRelativePath': 'YurLitco.Representation',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'c1d2903d-89a0-4ba2-b6a7-a8de32ab844e',
                'sortable': true,
                'text': 'Юридическое лицо',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Surname',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$8b8bb5a0-c6eb-4d45-a0c7-9dd5a8e7e78b',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'e86463fe-7967-4e30-ab79-88c5f9208fc6',
                'sortable': true,
                'text': 'Фамилия',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'FirstName',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$79d3cf0f-cffa-4856-80b6-405056ed5837',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '71f62d64-0174-495f-a9f1-3157d1e86e4f',
                'sortable': true,
                'text': 'Имя',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Patronymic',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$9a9a3d4b-9609-4819-9d7c-4bdefbee63be',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '4f244150-2d37-4370-99d7-20dc136d355e',
                'sortable': true,
                'text': 'Отчество',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Sex',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$4ea78322-6925-45d1-8e88-8014a0e3eb20',
                'enumName': 'B4.enums.Pol',
                'filter': {
                    'enumCls': B4.enums.Pol,
                    'xtype': 'b4-filter-field-enum'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '3061c757-ccca-4d81-9ac2-1e51c1edbca6',
                'sortable': true,
                'text': 'Пол',
                'xtype': 'b4enumcolumn'
            }, {
                'dataIndex': 'PostType_name1',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$27df2a9e-b98e-4c89-8744-3bae756f275f$9b116f06-db34-48a5-a6ea-feb1c59effc8',
                'dataIndexRelativePath': 'PostType.name1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b98e6b21-81b4-4e91-8cb8-e86b7a0ce7a5',
                'sortable': true,
                'text': 'Тип должности',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'PostText',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$f892df1d-562c-42a9-8f14-3a153fd3f9d5',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '4c083c9c-7e3e-446e-86bf-2e9e3bc50397',
                'sortable': true,
                'text': 'Должность',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Basis',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$af41a491-d34c-4e5f-ab12-f93aa7f7048e',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '58c5e8e5-f005-4186-b77b-b01e82f95922',
                'sortable': true,
                'text': 'Действует на основании',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Phone',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$ea28d160-6202-49c5-abb9-e9c67bd9b94f',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'be2b24c3-e3b2-4608-9a9f-df369d8e5994',
                'sortable': true,
                'text': 'Телефон',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }, {
                'ptype': 'gridDataExport',
                'reportTitle': 'Реестр Руководители'
            }]
        });
        me.callParent(arguments);
    }
});