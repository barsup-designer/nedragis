Ext.define('B4.view.Zemleustrojjstva', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-zemleustrojjstva',
    title: 'Форма отдела "Землеустройства" ',
    rmsUid: 'e96ce26c-72eb-4c4f-8695-326c998b58e1',
    requires: [
        'B4.form.PickerField',
        'B4.model.Otdel1EditorModel',
        'B4.model.Otdel1ListModel',
        'B4.view.Otdel1List'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'monitor_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'otdel',
        'modelProperty': 'monitor_otdel',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'analiz',
        'modelProperty': 'monitor_analiz',
        'type': 'NumberField'
    }, {
        'dataIndex': 'analiz_d',
        'modelProperty': 'monitor_analiz_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'karta_chema',
        'modelProperty': 'monitor_karta_chema',
        'type': 'NumberField'
    }, {
        'dataIndex': 'karta_shema_d',
        'modelProperty': 'monitor_karta_shema_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'pr_analiz',
        'modelProperty': 'monitor_pr_analiz',
        'type': 'NumberField'
    }, {
        'dataIndex': 'pr_analiz_d',
        'modelProperty': 'monitor_pr_analiz_d',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'e96ce26c-72eb-4c4f-8695-326c998b58e1-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'Zemleustrojjstva-container',
        'autoScroll': true,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Наименование отдела',
            'idProperty': 'Id',
            'isContextAware': true,
            'labelWidth': 150,
            'listView': 'B4.view.Otdel1List',
            'listViewCtl': 'B4.controller.Otdel1List',
            'margin': '10 35 0 35',
            'maximizable': true,
            'model': 'B4.model.Otdel1ListModel',
            'modelProperty': 'monitor_otdel',
            'name': 'otdel',
            'rmsUid': 'c2d314fc-f5b9-4a51-901d-fd7fd62f61df',
            'textProperty': 'NAME_SP',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Наименование отдела',
                'width': 450
            },
            'xtype': 'b4pickerfield'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Пространственный анализ размещения проектируемых участков, выполнение схем в разрезе в разрезе   лесопользования, землепользования и недропользования',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_analiz',
                        'name': 'analiz',
                        'rmsUid': '513cf552-f48f-4848-85ab-5c6574f79122',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '2 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_analiz_d',
                        'name': 'analiz_d',
                        'rmsUid': '2c49cddf-dc4d-4b99-a107-dcec1e95a579',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'layout': {
                        'pack': 'center',
                        'type': null
                    },
                    'region': 'East',
                    'rmsUid': '34705e55-a572-4e8e-9ea0-597077e8692d',
                    'title': 'Пространственный анализ размещения проектируемых участков, выполнение схем в разрезе в разрезе   лесопользования, землепользования и недропользования',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Выполнение обзорных карт-схем, содержащие сведения: о границах земель лесного фонда, о категориях защитности лесов, лесных кварталов и выделов, классам пожарной опасности, породному составу',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_karta_chema',
                        'name': 'karta_chema',
                        'rmsUid': '621d83d1-a0c2-482b-98b2-d7a70bd891c3',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_karta_shema_d',
                        'name': 'karta_shema_d',
                        'rmsUid': '47242321-1d40-4774-b048-82778030cc27',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': 'c67927c9-4200-47e2-acfb-2a8b02de214f',
                    'title': 'Выполнение обзорных карт-схем, содержащие сведения: о границах земель лесного фонда, о категориях защитности лесов, лесных кварталов и выделов, классам пожарной опасности, породному составу',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Пространственный анализ ,предоставление информации в цифровом формате (SHP, MAPTAB,JPG, EXCEL) в разрезе   лесопользования, землепользования и недропользования',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_pr_analiz',
                        'name': 'pr_analiz',
                        'rmsUid': '2351d247-fcad-4f60-bd0d-b5a342703ee1',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_pr_analiz_d',
                        'name': 'pr_analiz_d',
                        'rmsUid': 'bcc9eec3-dd9b-44bb-8187-565d41c44229',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': 'e276cb4b-85cc-4940-b212-03ac80f6fffa',
                    'title': 'Пространственный анализ ,предоставление информации в цифровом формате (SHP, MAPTAB,JPG, EXCEL) в разрезе   лесопользования, землепользования и недропользования',
                    'xtype': 'fieldset'
                }],
                'layout': 'anchor',
                'rmsUid': '8ae9c459-4978-442c-a6e3-b9b0e95c55fd',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '20 10 0 10',
            'rmsUid': '0de862e9-9c31-4399-8c8a-dcf9a0fdae81',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'monitor_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});