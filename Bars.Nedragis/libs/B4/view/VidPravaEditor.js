Ext.define('B4.view.VidPravaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-vidpravaeditor',
    title: 'Форма редактирования Вид права',
    rmsUid: 'ca2440ee-b16c-4786-9566-d4420a6ecf20',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'VidPrava_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'VidPrava_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name1',
        'modelProperty': 'VidPrava_name1',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'ca2440ee-b16c-4786-9566-d4420a6ecf20-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'VidPravaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'VidPrava_code',
            'name': 'code',
            'rmsUid': '1db24f0f-c96d-48c0-9fc8-521ef56e7aba',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'VidPrava_name1',
            'name': 'name1',
            'rmsUid': '6025c970-257a-4c27-b1aa-6fef7508e34f',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'VidPrava_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name1'));
        } else {}
        return res;
    },
});