Ext.define('B4.view.FormaZim', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-formazim',
    title: 'Форма зим',
    rmsUid: '8bfce9cc-8f53-4d1f-8ad8-c1fec6964744',
    requires: [
        'B4.form.PickerField',
        'B4.model.Zima1EditorModel',
        'B4.model.Zima1ListModel',
        'B4.view.Zima1List'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Zim1_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1517482864074',
        'modelProperty': 'Zim1_Element1517482864074',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '8bfce9cc-8f53-4d1f-8ad8-c1fec6964744-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'FormaZim-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'наименование',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.Zima1List',
            'listViewCtl': 'B4.controller.Zima1List',
            'maximizable': true,
            'model': 'B4.model.Zima1ListModel',
            'modelProperty': 'Zim1_Element1517482864074',
            'name': 'Element1517482864074',
            'rmsUid': 'd427d8b7-101b-4789-a11f-b2526f6bac23',
            'textProperty': 'Element1517294546876',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'наименование',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Zim1_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});