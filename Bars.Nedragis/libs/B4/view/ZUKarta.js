Ext.define('B4.view.ZUKarta', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-zukarta',
    title: 'ЗУ Карта',
    rmsUid: '1c4e02ef-7e0f-4ae9-9a0f-cfae7e6ec043',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'ZemelNyemUchastki_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Id',
        'modelProperty': 'ZemelNyemUchastki_Id',
        'type': 'HiddenField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '1c4e02ef-7e0f-4ae9-9a0f-cfae7e6ec043-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ZUKarta-container',
        'layout': {
            'type': 'fit'
        },
        'region': 'East',
        items: [{
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'layout': {
                'type': 'fit'
            },
            'region': 'East',
            'rmsUid': 'ac7b400f-b8e5-426f-9f41-eb462564c6d6',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'allowBlank': false,
            'modelProperty': 'ZemelNyemUchastki_Id',
            'name': 'Id',
            'rmsUid': 'a9e6b84d-ab5c-45e0-977c-b9079ac37238',
            'xtype': 'hiddenfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'ZemelNyemUchastki_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle('Карта');
        } else {}
        return res;
    },
});