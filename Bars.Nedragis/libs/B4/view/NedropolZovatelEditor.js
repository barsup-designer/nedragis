Ext.define('B4.view.NedropolZovatelEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-nedropolzovateleditor',
    title: 'Редактирование справочника Недропользователь',
    rmsUid: '4cd9045d-1da6-4042-8dfd-32785bc509b8',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'NedropolZovatel_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'NedropolZovatel_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'adres',
        'modelProperty': 'NedropolZovatel_adres',
        'type': 'TextField'
    }, {
        'dataIndex': 'tel',
        'modelProperty': 'NedropolZovatel_tel',
        'type': 'TextField'
    }, {
        'dataIndex': 'mail',
        'modelProperty': 'NedropolZovatel_mail',
        'type': 'TextField'
    }, {
        'dataIndex': 'fax',
        'modelProperty': 'NedropolZovatel_fax',
        'type': 'TextField'
    }, {
        'dataIndex': 'member',
        'modelProperty': 'NedropolZovatel_member',
        'type': 'TextField'
    }, {
        'dataIndex': 'inn',
        'modelProperty': 'NedropolZovatel_inn',
        'type': 'TextField'
    }, {
        'dataIndex': 'kpp',
        'modelProperty': 'NedropolZovatel_kpp',
        'type': 'TextField'
    }, {
        'dataIndex': 'kors',
        'modelProperty': 'NedropolZovatel_kors',
        'type': 'TextField'
    }, {
        'dataIndex': 'rs',
        'modelProperty': 'NedropolZovatel_rs',
        'type': 'TextField'
    }, {
        'dataIndex': 'bik',
        'modelProperty': 'NedropolZovatel_bik',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '4cd9045d-1da6-4042-8dfd-32785bc509b8-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'NedropolZovatelEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Идентификатор',
            'margin': '10 250 10 10',
            'modelProperty': '546753f0-fe75-4d5b-b43a-fc16e22f7f41',
            'readOnly': true,
            'rmsUid': '546753f0-fe75-4d5b-b43a-fc16e22f7f41',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'margin': '10 10 10 10',
            'modelProperty': 'NedropolZovatel_name',
            'name': 'name',
            'rmsUid': '7b0af945-5bb1-4643-b7a3-9bd3467e3f8f',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Адрес',
            'margin': '10 10 10 10',
            'modelProperty': 'NedropolZovatel_adres',
            'name': 'adres',
            'rmsUid': 'f80f3153-3459-4124-96df-671a06837782',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Телефон',
            'margin': '10 250 10 10',
            'modelProperty': 'NedropolZovatel_tel',
            'name': 'tel',
            'rmsUid': 'ff333848-ee84-4735-8905-9a5480f997e0',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'e-mail',
            'margin': '10 100 10 10',
            'maxLength': 50,
            'maxLengthText': 'Превышена максимальная длина',
            'minLength': 0,
            'minLengthText': 'Текст короче минимальной длины',
            'modelProperty': 'NedropolZovatel_mail',
            'name': 'mail',
            'rmsUid': '9126b6ca-8dc6-41c8-b12f-eea8306b6248',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Факс',
            'margin': '10 250 10 10',
            'maxLength': 8,
            'maxLengthText': 'Превышена максимальная длина',
            'minLength': 0,
            'minLengthText': 'Текст короче минимальной длины',
            'modelProperty': 'NedropolZovatel_fax',
            'name': 'fax',
            'rmsUid': '68e3cc2e-2976-40bd-aa20-57de07d39073',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Руководитель',
            'margin': '10 10 10 10',
            'maxLength': 150,
            'maxLengthText': 'Превышена максимальная длина',
            'minLength': 0,
            'minLengthText': 'Текст короче минимальной длины',
            'modelProperty': 'NedropolZovatel_member',
            'name': 'member',
            'rmsUid': '6fc03e44-99bb-45d5-b048-4577f43ddd08',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'ИНН',
            'margin': '10 250 10 10',
            'modelProperty': 'NedropolZovatel_inn',
            'name': 'inn',
            'rmsUid': '0055a2d0-3718-4e35-a59e-2eb413106f8a',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'КПП',
            'margin': '10 250 10 10',
            'modelProperty': 'NedropolZovatel_kpp',
            'name': 'kpp',
            'rmsUid': '4f11701b-45b7-4b1b-89da-aa8b693dcaec',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Корресп. счет',
            'margin': '10 250 10 10',
            'modelProperty': 'NedropolZovatel_kors',
            'name': 'kors',
            'rmsUid': '38b9e099-0e55-4b19-94fd-878e59040c79',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Расчетный счет',
            'margin': '10 250 10 10',
            'modelProperty': 'NedropolZovatel_rs',
            'name': 'rs',
            'rmsUid': '2668fd8b-ce2d-4568-bfac-0a37367cc1a5',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'БИК',
            'margin': '10 250 10 10',
            'modelProperty': 'NedropolZovatel_bik',
            'name': 'bik',
            'rmsUid': '42060565-5060-4443-bd5d-f0c3fa6349db',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'NedropolZovatel_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});