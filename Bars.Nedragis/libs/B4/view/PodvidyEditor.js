Ext.define('B4.view.PodvidyEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-podvidyeditor',
    title: 'Форма редактирования Подвиды',
    rmsUid: 'fd3dc875-ffc1-40d9-b037-70bc0673f9f9',
    requires: [
        'B4.form.PickerField',
        'B4.model.VidyRabotEditorModel',
        'B4.model.VidyRabotListModel',
        'B4.view.VidyRabotList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Podvidy_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1474267693056',
        'modelProperty': 'Podvidy_Element1474267693056',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1474349306046',
        'modelProperty': 'Podvidy_Element1474349306046',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'fd3dc875-ffc1-40d9-b037-70bc0673f9f9-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'PodvidyEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'modelProperty': 'Podvidy_Element1474267693056',
            'name': 'Element1474267693056',
            'rmsUid': 'c45c9a0f-b6ba-49b4-92c9-c945e8d56994',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'вид',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.VidyRabotList',
            'listViewCtl': 'B4.controller.VidyRabotList',
            'maximizable': true,
            'model': 'B4.model.VidyRabotListModel',
            'modelProperty': 'Podvidy_Element1474349306046',
            'name': 'Element1474349306046',
            'rmsUid': 'bd34f7f4-57ec-4eb1-b81b-21c28ea7c9ef',
            'textProperty': 'Element1474020299386',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'вид',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Podvidy_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1474267693056'));
        } else {}
        return res;
    },
});