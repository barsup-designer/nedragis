Ext.define('B4.view.DokumentyZUEditor3', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-dokumentyzueditor3',
    title: 'Форма редактирования ДокументыЗУ3',
    rmsUid: 'b2dea0bc-a377-4cfc-897f-60f37a6211b4',
    requires: [
        'B4.form.PickerField',
        'B4.form.SelectField',
        'B4.model.DokumentEditorModel',
        'B4.model.DokumentListModel',
        'B4.model.ZemelNyemUchastkiListModel',
        'B4.model.ZURedaktirovanieNavigacijaModel',
        'B4.view.DokumentEditor',
        'B4.view.DokumentList',
        'B4.view.ZemelNyemUchastkiList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'DokumentyZU_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'zemuch',
        'modelProperty': 'DokumentyZU_zemuch',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'doc1',
        'modelProperty': 'DokumentyZU_doc1',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'DokumentEditor',
        'modelProperty': 'DokumentyZU_DokumentEditor',
        'type': 'Form'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'b2dea0bc-a377-4cfc-897f-60f37a6211b4-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'DokumentyZUEditor3-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Земельный участок',
            'hidden': true,
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.ZemelNyemUchastkiList',
            'listViewCtl': 'B4.controller.ZemelNyemUchastkiList',
            'maximizable': true,
            'model': 'B4.model.ZemelNyemUchastkiListModel',
            'modelProperty': 'DokumentyZU_zemuch',
            'name': 'zemuch',
            'rmsUid': '69ff3ed9-3f6e-45ee-a5ac-bcc5f016c041',
            'textProperty': 'name',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Земельный участок',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Документ',
            'hidden': true,
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.DokumentList',
            'listViewCtl': 'B4.controller.DokumentList',
            'maximizable': true,
            'model': 'B4.model.DokumentListModel',
            'modelProperty': 'DokumentyZU_doc1',
            'name': 'doc1',
            'rmsUid': '0175eda3-2737-4fe9-a482-4229737acd93',
            'textProperty': 'Number',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Документ',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'dockedItems': null,
            'header': false,
            'idProperty': 'Id',
            'margin': '10 10 0 10',
            'modelProperty': 'DokumentyZU_DokumentEditor',
            'name': 'DokumentEditor',
            'rmsUid': 'ab13d7a3-88c6-4a42-b88b-6d25911e7a44',
            'tbar': null,
            'title': null,
            'width': 500,
            'xtype': 'rms-dokumenteditor'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'DokumentyZU_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});