Ext.define('B4.view.subsidiesList', {
    'alias': 'widget.rms-subsidieslist',
    'dataSourceUid': '59ae1a57-2649-408b-9f30-33fc49b5a7cb',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.subsidiesListModel',
    'stateful': true,
    'title': 'Реестр Субсидии',
    requires: [
        'B4.model.subsidiesListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': null,
        'itemId': 'Addition-subsidiesEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'NAME_MO_Element1455707802552',
                'dataIndexAbsoluteUid': 'FieldPath://59ae1a57-2649-408b-9f30-33fc49b5a7cb$c0dbf801-1551-438f-9261-61a84604bacc$1d0a20ca-f1bd-48b3-9747-eb292da7de90',
                'dataIndexRelativePath': 'NAME_MO.Element1455707802552',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b784bb9f-43b4-476c-9fab-027a35c5b645',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование муниципального образования.Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1455708491495_Element1455708457805',
                'dataIndexAbsoluteUid': 'FieldPath://59ae1a57-2649-408b-9f30-33fc49b5a7cb$c71979b3-9c05-40c6-a568-204f5b4b6d86$c34cb84e-a373-4b8a-961c-b3bc90e327ad',
                'dataIndexRelativePath': 'Element1455708491495.Element1455708457805',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '83fff8dd-690e-4fac-8589-b3cb025e9973',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование направления расходования средств.Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1455709095130',
                'dataIndexAbsoluteUid': 'FieldPath://59ae1a57-2649-408b-9f30-33fc49b5a7cb$36a20294-4f66-4a0e-8408-8a4c74d668ae',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'ea01de67-91fe-43bc-b898-93c483cee372',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование объекта, с группировкой по населенным пунктам',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1455709437278',
                'dataIndexAbsoluteUid': 'FieldPath://59ae1a57-2649-408b-9f30-33fc49b5a7cb$a49fea85-9564-453a-a5b9-928459474e82',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '297f12ce-fda3-4382-a506-ebb6cfb18db6',
                'sortable': true,
                'text': 'Реквизиты свидетельства о государственной регистрации',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});