Ext.define('B4.view.KartografList', {
    'alias': 'widget.rms-kartograflist',
    'dataSourceUid': 'cdbf139a-d2ac-4731-87f9-40f673f96b6c',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.KartografListModel',
    'stateful': true,
    'title': 'Реестр Картография',
    requires: [
        'B4.model.KartografListModel',
        'B4.ux.grid.plugin.DataExport',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-KartografEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-KartografEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2e19bdf5-a741-420c-82b6-1bb1cbabafcf',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name_NAME_SP',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$78b1a24b-73bc-4275-8e6d-f813f05aa59c$c9cfc806-3d3e-4aa7-b12c-d9218426a9d7',
                'dataIndexRelativePath': 'name.NAME_SP',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '3602d578-775d-4c6a-ba83-790116b99cb6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование отдела',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'ObjectCreateDate',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$Bars.B4.DataAccess.BaseEntity, Bars.B4.Core/ObjectCreateDate',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '29440dfb-32d1-49cd-ba05-ae06d0c51d2c',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата создания',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1474278116576_Element1474020299386',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$855ca052-45ad-4242-9833-43401bd61655$b5a48978-083d-4fd5-853f-117af3c5798e',
                'dataIndexRelativePath': 'vid.Element1474020299386',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '6f16a007-cb29-47e5-976f-81ca18a9fb66',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Вид работ',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'podvid',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$be7b8c57-394d-4734-9b44-a413b2a06388',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5c24101b-211c-48a7-87bc-d77f53a8b877',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Подвид работ',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'col',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$2b6965ac-365b-4fca-8a15-ece0a950a0bc',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'af7015fc-153c-447e-a6a0-2e82dd0e661f',
                'sortable': true,
                'text': 'Количество',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'opis',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$d4799bbc-5212-4cf1-8c11-829143516132',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '4c9d2f60-612f-4c7b-919e-0ee2c033dcb6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дополнительное описание',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1474351766399',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$0610999c-b538-4d33-b5cc-c20f7ffb82dc',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '1bb6a143-4634-4837-882a-5113bfd05c3e',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Безвозмездное обращение',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'zakazchik',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$197c87d0-88ed-4004-b1fb-eae10895b230',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '9fc20532-ed28-4e2f-a97a-3e53a1b3acbd',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Заказчик',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'zakaz1',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$850821f5-5d53-44c8-aa98-f106899453fa',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0dfd12bd-c9e5-4e71-b284-67f65924398f',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дополнительная информация о заказчике',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'organ',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$a66379e8-57f2-43f2-9e10-cbf2ad762efc',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '82ba0519-4d17-4c2c-9701-142cd74f4244',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Орган госвласти',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'harakter',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$d9a6c3b5-d79d-413a-9238-41b3665a2db9',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '11b8c3e5-4787-4e44-8379-83a8577ccbf8',
                'sortable': true,
                'text': 'Характер обращения',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1474352034381_Element1474352136232',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$1b083742-4ba4-4acc-9458-1770b3938096$5c9e0c3e-7dcb-483d-89fd-a12dacaa7279',
                'dataIndexRelativePath': 'Element1474352034381.Element1474352136232',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'c367d55b-47ff-4410-b346-9327723472eb',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Исполнитель',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'chas',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$98b11871-fdf4-4314-a422-fc681d38f492',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'c3d0de13-d118-4439-8c65-41836c3a59c5',
                'sortable': true,
                'text': 'Количество затраченных человека часов',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'stoumost',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$24b35972-d5e2-4a46-bdb2-c08b7c752a79',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'e2cf3b7c-b6de-4f11-aa27-8caba662d661',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Стоимость по калькуляции руб.',
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'oplata',
                'dataIndexAbsoluteUid': 'FieldPath://cdbf139a-d2ac-4731-87f9-40f673f96b6c$dbf67e1f-2787-4114-a0bd-e30e144337e9',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'c39a69ba-5916-40c4-87a8-de075dbecd49',
                'sortable': true,
                'text': 'Оплачено',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }, {
                'ptype': 'gridDataExport',
                'reportTitle': 'Реестр Картография'
            }]
        });
        me.callParent(arguments);
    }
});