Ext.define('B4.view.test_barsList2', {
    'alias': 'widget.rms-test_barslist2',
    'dataSourceUid': '7dd5ad97-a603-4dbf-acec-30db779b157d',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.test_barsList2Model',
    'stateful': true,
    'title': 'Реестр test_bars2',
    requires: [
        'B4.model.test_barsList2Model',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-test_bars1-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-test_barsEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Element1481264972872',
                'dataIndexAbsoluteUid': 'FieldPath://7dd5ad97-a603-4dbf-acec-30db779b157d$b30575d0-57a6-4476-92cd-0f6957fb60f2',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5e63c8a6-ad9c-4429-ad39-cbf515b11288',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', Ext.util.Format.number(value, '0,000.00'));
                },
                'summaryType': 'none',
                'text': 'площадь',
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'Element1496214131857',
                'dataIndexAbsoluteUid': 'FieldPath://7dd5ad97-a603-4dbf-acec-30db779b157d$039ca39a-60f6-4d3a-af26-60734e9d150b',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '7657832b-0fa8-4fef-9e78-88d68d70fd70',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Test',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});