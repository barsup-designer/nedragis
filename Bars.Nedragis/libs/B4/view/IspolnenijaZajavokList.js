Ext.define('B4.view.IspolnenijaZajavokList', {
    'alias': 'widget.rms-ispolnenijazajavoklist',
    'dataSourceUid': '570c3381-31ef-424d-b058-c74c0a28ed56',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.IspolnenijaZajavokListModel',
    'stateful': true,
    'title': 'Реестр Приема и контроля исполнения заявок',
    requires: [
        'B4.model.IspolnenijaZajavokListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-IspolnenijaZajavokEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-IspolnenijaZajavokEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Date456789',
                'dataIndexAbsoluteUid': 'FieldPath://570c3381-31ef-424d-b058-c74c0a28ed56$Bars.B4.DataAccess.BaseEntity, Bars.B4.Core/ObjectCreateDate',
                'dataIndexRelativePath': 'ObjectCreateDate',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'f8b7092b-1914-48cc-8c99-88ec160c1a1a',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата создания',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'reestr_date',
                'dataIndexAbsoluteUid': 'FieldPath://570c3381-31ef-424d-b058-c74c0a28ed56$8f67ea0f-c7de-4e15-9952-0df51655c8b7',
                'dataIndexRelativePath': 'Element1511781998051',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '1f6b11a5-4833-4ad5-b8b7-007038ff916f',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата заявки',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'kons456',
                'dataIndexAbsoluteUid': 'FieldPath://570c3381-31ef-424d-b058-c74c0a28ed56$6e686cd5-c68f-42a8-90f6-197469350b6b',
                'dataIndexRelativePath': 'Element1511506083922',
                'filter': true,
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.check,
                'rmsUid': 'c909b69f-24b1-4692-b0f0-c0c9814afac1',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Консультация по телефону',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'ins456',
                'dataIndexAbsoluteUid': 'FieldPath://570c3381-31ef-424d-b058-c74c0a28ed56$d8fcee08-5459-4aa7-9737-15d0bff22e0b',
                'dataIndexRelativePath': 'Element1511506316909',
                'filter': true,
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.check,
                'rmsUid': '5b82459a-3784-41a5-a9ee-49a4fbec3b34',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Инцидент(заявка)',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'reestrmo456',
                'dataIndexAbsoluteUid': 'FieldPath://570c3381-31ef-424d-b058-c74c0a28ed56$ea16a9b0-4dc3-40d0-b38a-0c11f8268752$1d0a20ca-f1bd-48b3-9747-eb292da7de90',
                'dataIndexRelativePath': 'Element1511506563562.Element1455707802552',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a3270e8b-a8fd-4fda-b752-ce36de95eec4',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Муниципальное образование.Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'reestrname456',
                'dataIndexAbsoluteUid': 'FieldPath://570c3381-31ef-424d-b058-c74c0a28ed56$7a7eedb4-b777-4d30-9c1a-2c5d6b80a7c5$fb062969-e7d0-4b44-b9d8-684e21206cf8',
                'dataIndexRelativePath': 'Element1511506733063.Element1511331964541',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '16884788-f00b-486c-acfe-1af4d0eaae67',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование организации.Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'reestrpr',
                'dataIndexAbsoluteUid': 'FieldPath://570c3381-31ef-424d-b058-c74c0a28ed56$6876e46e-7bbe-409b-aea5-822d65f84a1e$c34cb84e-a373-4b8a-961c-b3bc90e327ad',
                'dataIndexRelativePath': 'Element1511762285082.Element1455708457805',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '41f14168-8779-4bb8-9c14-4261f672777b',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'проект.Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});