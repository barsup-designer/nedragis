Ext.define('B4.view.RegGosNadzorZaGeoList', {
    'alias': 'widget.rms-reggosnadzorzageolist',
    'dataSourceUid': 'd14687ca-1601-4731-a8ab-e4e2311cdb65',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.RegGosNadzorZaGeoListModel',
    'stateful': true,
    'title': 'Реестр Региональный государственный надзор за геологическим изучением',
    requires: [
        'B4.model.RegGosNadzorZaGeoListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-RegGosNadzorZaGeoEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-RegGosNadzorZaGeoEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'licn11',
                'dataIndexAbsoluteUid': 'FieldPath://d14687ca-1601-4731-a8ab-e4e2311cdb65$57067dcd-fbdc-4de0-b6d0-0c2c63bd14e4',
                'dataIndexRelativePath': 'Element1507635501632',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '48d36341-df91-440c-8c33-fefc303a383d',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер лицензии',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'licdate11',
                'dataIndexAbsoluteUid': 'FieldPath://d14687ca-1601-4731-a8ab-e4e2311cdb65$0832b728-0baf-41a0-80c5-d3afa04fee3e',
                'dataIndexRelativePath': 'Element1507635537755',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '473c0c39-9bf3-49e9-a54d-6110406f6f7f',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата регистрации',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'licdateend11',
                'dataIndexAbsoluteUid': 'FieldPath://d14687ca-1601-4731-a8ab-e4e2311cdb65$d2b08b92-00bc-45ae-850d-6ec56e5820e1',
                'dataIndexRelativePath': 'Element1507635559781',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '22c70618-15bb-40c8-948d-8a70f0661227',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Окончание срока действия',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'licmesto11',
                'dataIndexAbsoluteUid': 'FieldPath://d14687ca-1601-4731-a8ab-e4e2311cdb65$ede82dfa-23a3-4bb4-808b-03b818537dbc',
                'dataIndexRelativePath': 'Element1507635586209',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a09bb209-52f4-4799-a626-07c8bdb4c58e',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Местонаходжение',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});