Ext.define('B4.view.ZajavkGGIEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-zajavkggieditor',
    title: 'Заявка на приобретение геолого-геофизической информации',
    rmsUid: 'a81d9be5-bfc7-410d-9882-3b546a21a478',
    requires: [
        'B4.form.PickerField',
        'B4.model.LicUchastkiEditorModel',
        'B4.model.LicUchastkiListModel',
        'B4.model.NedropolZovatelEditorModel',
        'B4.model.NedropolZovatelListModel',
        'B4.view.LicUchastkiList',
        'B4.view.NedropolZovatelList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'ZajavkGGI_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'nomz',
        'modelProperty': 'ZajavkGGI_nomz',
        'type': 'NumberField'
    }, {
        'dataIndex': 'dataz',
        'modelProperty': 'ZajavkGGI_dataz',
        'type': 'DateField'
    }, {
        'dataIndex': 'dati',
        'modelProperty': 'ZajavkGGI_dati',
        'type': 'DateField'
    }, {
        'dataIndex': 'NedropolZovatel',
        'modelProperty': 'ZajavkGGI_NedropolZovatel',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'luch',
        'modelProperty': 'ZajavkGGI_luch',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'texto',
        'modelProperty': 'ZajavkGGI_texto',
        'type': 'TextAreaField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'a81d9be5-bfc7-410d-9882-3b546a21a478-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ZajavkGGIEditor-container',
        'autoScroll': true,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'allowBlank': true,
                'allowDecimals': false,
                'decimalPrecision': 2,
                'fieldLabel': 'Номер заявки',
                'margin': '5 5 5 5',
                'minValue': 0,
                'modelProperty': 'ZajavkGGI_nomz',
                'name': 'nomz',
                'rmsUid': '44c54402-dd63-4523-bfa0-35da1cd4838c',
                'step': 1,
                'width': 200,
                'xtype': 'numberfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Дата заявки',
                'format': 'd.m.Y',
                'margin': '5 5 5 5',
                'modelProperty': 'ZajavkGGI_dataz',
                'name': 'dataz',
                'rmsUid': '6459d64e-5001-4a57-910f-da99b3d27f24',
                'xtype': 'datefield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Дата исполнения',
                'format': 'd.m.Y',
                'margin': '5 5 5 5',
                'modelProperty': 'ZajavkGGI_dati',
                'name': 'dati',
                'rmsUid': 'e0d68586-5c04-4403-98b5-2e2d8924396c',
                'xtype': 'datefield'
            }],
            'layout': {
                'type': 'column'
            },
            'margin': '5 5 0 10',
            'rmsUid': 'c9941ccb-d13d-4d1e-9b78-4da6f385d996',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'autoScroll': true,
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Недро-пользователь',
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.NedropolZovatelList',
                'listViewCtl': 'B4.controller.NedropolZovatelList',
                'margin': '10 0 10 10',
                'maximizable': true,
                'model': 'B4.model.NedropolZovatelListModel',
                'modelProperty': 'ZajavkGGI_NedropolZovatel',
                'name': 'NedropolZovatel',
                'rmsUid': 'e807e052-e339-4a4e-81cb-8471681d6b60',
                'textProperty': 'name',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Недро-пользователь',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }, {
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Лицензионный участок',
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.LicUchastkiList',
                'listViewCtl': 'B4.controller.LicUchastkiList',
                'margin': '10 0 10 10',
                'maximizable': true,
                'model': 'B4.model.LicUchastkiListModel',
                'modelProperty': 'ZajavkGGI_luch',
                'name': 'luch',
                'rmsUid': '7528d83f-6e39-41df-8f53-de5905f6f128',
                'textProperty': 'name',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Лицензионный участок',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Текст',
                'labelAlign': 'top',
                'margin': '10 0 10 10',
                'modelProperty': 'ZajavkGGI_texto',
                'name': 'texto',
                'rmsUid': '68e53a60-0d38-4fec-ba18-f99c144d0fed',
                'xtype': 'textarea'
            }],
            'layout': {
                'type': 'fit'
            },
            'margin': '5 5 5 5',
            'rmsUid': '3840dad4-a456-4442-9e2f-4bf70413a9ac',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'dockedItems': [],
            'fieldLabel': 'Составить',
            'height': 70,
            'items': [{
                'fieldLabel': 'Решение',
                'margin': '5 10 5 10',
                'rmsUid': '5e04e054-d9da-48eb-9863-b5be163db0c8',
                'text': 'Решение',
                'xtype': 'button'
            }, {
                'fieldLabel': 'Спецификацию',
                'margin': '5 10 5 10',
                'rmsUid': '7a6d5d89-7363-4689-a6af-33c9528062eb',
                'text': 'Спецификацию',
                'xtype': 'button'
            }, {
                'fieldLabel': 'Запрос в фед. агенство',
                'margin': '5 10 5 10',
                'rmsUid': '2a02543a-80aa-4c85-a68a-0477662acc07',
                'text': 'Запрос в фед. агенство',
                'xtype': 'button'
            }],
            'layout': {
                'type': 'hbox'
            },
            'margin': '10 10 10 10',
            'rmsUid': 'dac1897e-f541-4b0d-872a-ca41b15ae1fd',
            'title': 'Составить',
            'width': 100,
            'xtype': 'fieldset'
        }, {
            'anchor': '100%',
            'autoScroll': true,
            'columnWidth': '0.20',
            'dockedItems': [],
            'fieldLabel': 'Посмотреть',
            'height': 70,
            'items': [{
                'fieldLabel': 'Договор',
                'margin': '5 10 5 10',
                'rmsUid': '4a0d2982-cbd5-484e-ac2d-6e660c88dbe7',
                'text': 'Договор',
                'xtype': 'button'
            }, {
                'fieldLabel': 'Спецификацию',
                'margin': '5 10 5 10',
                'rmsUid': '8055e9ba-6bec-4c5c-b73f-f1347a4b6b2c',
                'text': 'Спецификацию',
                'xtype': 'button'
            }, {
                'fieldLabel': 'Решение',
                'margin': '5 10 5 10',
                'rmsUid': 'c2f53485-2674-4ad1-82ef-22a11987b5c0',
                'text': 'Решение',
                'xtype': 'button'
            }],
            'layout': {
                'align': 'stretch',
                'pack': 'center',
                'type': 'column'
            },
            'margin': '10 10 10 10',
            'rmsUid': '2757dd27-d821-476d-9d1e-990bdd381a84',
            'title': 'Посмотреть',
            'xtype': 'fieldset'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'ZajavkGGI_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('nomz'));
        } else {}
        return res;
    },
});