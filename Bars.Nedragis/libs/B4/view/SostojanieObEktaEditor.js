Ext.define('B4.view.SostojanieObEktaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-sostojanieobektaeditor',
    title: 'Форма редактирования Состояние объекта',
    rmsUid: '92c9229d-2d85-4c41-9751-ae96b8b40751',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'SostojanieObEkta_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'SostojanieObEkta_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'SostojanieObEkta_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '92c9229d-2d85-4c41-9751-ae96b8b40751-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'SostojanieObEktaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'SostojanieObEkta_code',
            'name': 'code',
            'rmsUid': 'd5bc6234-4361-4a13-8bde-c618d7dc97b2',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'SostojanieObEkta_name',
            'name': 'name',
            'rmsUid': '5620cba8-f875-4345-be71-6235acf41c1f',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'SostojanieObEkta_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});