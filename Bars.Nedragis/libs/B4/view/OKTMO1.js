Ext.define('B4.view.OKTMO1', {
    'alias': 'widget.rms-oktmo1',
    'dataSourceUid': '151d2f6f-fbb5-4684-9f56-92b8ed775121',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': false,
    'extend': 'B4.base.registry.TreeView',
    'model': 'B4.model.OKTMO1Model',
    'parentFieldName': 'parent_id',
    'stateful': true,
    'title': 'ОКТМО',
    requires: [
        'B4.model.OKTMO1Model',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'content-img-icons-reload-png',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-add-png',
        'itemId': 'Addition-OKTMOEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-edit-png',
        'itemId': 'Editing-OKTMOEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-delete-png',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://151d2f6f-fbb5-4684-9f56-92b8ed775121$492e145e-47fb-4922-a77b-d83f999ade55',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b6ed7946-7ebf-4353-b9c8-b20d2ef03eec',
                'sortable': true,
                'text': 'Код',
                'xtype': 'treecolumn'
            }, {
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://151d2f6f-fbb5-4684-9f56-92b8ed775121$1d9789e7-46d7-4057-97e8-24ce9183a254',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0e9d4ba9-3eff-46ae-8b39-513fa6381197',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }]
        });
        me.callParent(arguments);
    }
});