Ext.define('B4.view.DolzhnostEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-dolzhnosteditor',
    title: 'Форма редактирования должность',
    rmsUid: '6e91eafb-81f3-41ba-be06-38cd3d096021',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Dolzhnost_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'Dolzhnost_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '6e91eafb-81f3-41ba-be06-38cd3d096021-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'DolzhnostEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'margin': '10 10 10 10',
            'modelProperty': 'Dolzhnost_name',
            'name': 'name',
            'rmsUid': 'f3cd10cb-8f44-47a9-9f34-045730c59adc',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Dolzhnost_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});