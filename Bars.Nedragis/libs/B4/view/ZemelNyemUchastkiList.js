Ext.define('B4.view.ZemelNyemUchastkiList', {
    'alias': 'widget.rms-zemelnyemuchastkilist',
    'dataSourceUid': '6885fdfb-7ec7-4566-8395-5542b58c6305',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.ZemelNyemUchastkiListModel',
    'stateful': true,
    'title': 'Реестр Земельные участки',
    requires: [
        'B4.model.ZemelNyemUchastkiListModel',
        'B4.ux.grid.plugin.DataExport',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-ZUFormaVvoda-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-ZURedaktirovanieNavigacija-Default',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }, {
        'handler': function() {
            me = this.up('rms-zemelnyemuchastkilist'); /*br*/
            if (!me.getSelectionModel().lastSelected) /*br*/
            return; /*br*/
            var formData = new FormData; /*br*/
            formData.append('id', me.getSelectionModel().lastSelected.raw.Id); /*br*/
            formData.append('registry', 'Земельные участки'); /*br*/
            $.ajax({ /*br*/
                url: 'Copy/CopyRaw',
                /*br*/
                data: formData,
                /*br*/
                processData: false,
                /*br*/
                contentType: false,
                /*br*/
                type: 'POST',
                /*br*/
                success: function(respData) { /*br*/
                    me.store.reload(); /*br*/
                },
                /*br*/
                error: function(xhr, status, data) { /*br*/
                    /*br*/
                } /*br*/
            });
        },
        'hidden': false,
        'iconCls': 'content-img-icons-page_copy-png',
        'itemId': 'Custom',
        'rmsUid': null,
        'text': 'Копировать строку'
    }, {
        'handler': function() {
            var me = this; /*br*/
            this.alphaAuthToken = this.alphaAuthToken || ''; /*br*/
            var data = { /*br*/
                Login: 'admin',
                /*br*/
                Password: 'yanao',
                /*br*/
                AuthToken: this.alphaAuthToken,
                /*br*/
                ForceNewSession: false /*br*/
            }; /*br*/
            /*br*/
            $.ajax({ /*br*/
                type: "POST",
                /*br*/
                url: 'http://ias.yanao.ru/alpha/default/login/login',
                /*br*/
                crossDomain: true,
                /*br*/
                data: data,
                /*br*/
                error: function(resp) { /*br*/
                    console.log('Login error'); /*br*/
                    console.log(resp); /*br*/
                },
                /*br*/
                success: function(resp) { /*br*/
                    me.alphaAuthToken = resp.TokenValue; /*br*/
                    var url; /*br*/
                    url = 'http://ias.yanao.ru/alpha/default/?OpenAction=66'; /*br*/
                    url += '&authToken=' + me.alphaAuthToken; /*br*/
                    window.open(url); /*br*/
                } /*br*/
            });
        },
        'hidden': false,
        'iconCls': 'content-img-icons-map-png',
        'itemId': 'Custom1',
        'rmsUid': null,
        'text': 'Карта'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'cadnum1',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$12badbbd-e1ea-40e7-b969-532cea517fcc',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0692e90c-28b5-4c95-a886-0b6b8815a2bd',
                'sortable': true,
                'text': 'Кадастровый номер',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$105ff081-e494-4ec2-be01-d5b976574ee2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a8c7e280-caea-4a5e-a982-472457f04984',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'area',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$27a43f2d-8fd1-4e39-95ca-ace3fcd5c436',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'f26b6e3c-10f2-4519-88e8-7776a0a02b64',
                'sortable': true,
                'text': 'Общая площадь, кв. м',
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'naznach',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$3cb61f3d-e6a5-42a9-b91c-4c15cfd1d5d1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '813b1596-5ad6-4988-abda-f7667183bbbe',
                'sortable': true,
                'text': 'Назначение',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'category_name1',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$00e67e7c-cee8-4669-b13e-131cc1fb153c$1cda49c6-b9e1-4437-873b-7c67d9cbe02f',
                'dataIndexRelativePath': 'category.name1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'cea25bb5-e119-461a-8de0-2dfbf4f2a7b0',
                'sortable': true,
                'text': 'Категория земель',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'vid_use_name1',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$dbf89b5c-94fe-42a3-8300-6ca22f8bfb16$868835cf-0e3b-47aa-be83-eb3e66045386',
                'dataIndexRelativePath': 'vid_use.name1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2620d922-8c45-439a-97eb-60553fc7d187',
                'sortable': true,
                'text': 'Вид разрешенного использования',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'vid_prava_name1',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$e70e27a6-b885-42f3-94ba-cda4ef7e3427$ea2c10dd-7114-412e-9b4a-7c4936c0c672',
                'dataIndexRelativePath': 'vid_prava.name1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd10fae9a-0ba0-4e37-a83f-6ce6a7810251',
                'sortable': true,
                'text': 'Вид права на землю',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'holder_id_Representation',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$5859faf6-8367-4bcf-b7d2-d5c80692f032$6f8b2489-4290-4747-93ba-ff199586d3bb',
                'dataIndexRelativePath': 'holder_id.Representation',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '382b156a-e663-4e7d-9af8-74439c78ca97',
                'sortable': true,
                'text': 'Правообладатель',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'owner_id_Representation',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$a47f4db2-bfc6-454c-bfc9-f30970ac0faf$6f8b2489-4290-4747-93ba-ff199586d3bb',
                'dataIndexRelativePath': 'owner_id.Representation',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a15d93fb-a101-4103-8222-fb26adc477ae',
                'sortable': true,
                'text': 'Собственник',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'property_manager_id_Representation',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$d63fb382-865b-4a7a-abe3-4806066b552b$6f8b2489-4290-4747-93ba-ff199586d3bb',
                'dataIndexRelativePath': 'property_manager_id.Representation',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'f96e4f3a-a1ce-4110-ad72-37ba2fd34e8f',
                'sortable': true,
                'text': 'Уполномоченный орган',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'MapID',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$ec2bd77e-9b6e-4045-adac-92776ae1818b',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 0.5,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2f6ec407-16f0-42ce-93f0-7df04a0293d9',
                'sortable': true,
                'text': 'Идентификатор ГИС',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'typeobj_name',
                'dataIndexAbsoluteUid': 'FieldPath://6885fdfb-7ec7-4566-8395-5542b58c6305$cebda652-11c0-44e4-a938-3104514ebc81$a2161223-e3f1-4eee-99a8-c005909756b1',
                'dataIndexRelativePath': 'typeobj.name',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'dea362d9-641d-41b6-80a8-2fc44c8f91e3',
                'sortable': true,
                'text': 'Тип объекта права',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }, {
                'ptype': 'gridDataExport',
                'reportTitle': 'Земельные участки'
            }]
        });
        me.callParent(arguments);
    }
});