Ext.define('B4.view.OrganyVlastiPrirodaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-organyvlastiprirodaeditor',
    title: 'Форма редактирования Органы власти Природа',
    rmsUid: '09ce0ebc-15ea-4823-859a-49aaa5a94c12',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'OrganyVlastiPriroda_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'organ_vlasti_pr',
        'modelProperty': 'OrganyVlastiPriroda_organ_vlasti_pr',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '09ce0ebc-15ea-4823-859a-49aaa5a94c12-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OrganyVlastiPrirodaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Органы власти',
            'margin': '20 20 20 20',
            'modelProperty': 'OrganyVlastiPriroda_organ_vlasti_pr',
            'name': 'organ_vlasti_pr',
            'rmsUid': '5453d845-4763-4f40-a5be-7b6b77c2079e',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'OrganyVlastiPriroda_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});