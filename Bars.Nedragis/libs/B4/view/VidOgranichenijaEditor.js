Ext.define('B4.view.VidOgranichenijaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-vidogranichenijaeditor',
    title: 'Форма редактирования Вид ограничения',
    rmsUid: '9f96466d-2660-46cb-bb39-52553bf240fd',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'VidOgranichenija_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'VidOgranichenija_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name1',
        'modelProperty': 'VidOgranichenija_name1',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '9f96466d-2660-46cb-bb39-52553bf240fd-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'VidOgranichenijaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'VidOgranichenija_code',
            'name': 'code',
            'rmsUid': 'ca5a018e-36a8-43cf-a4ed-47650d3c3e9b',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'VidOgranichenija_name1',
            'name': 'name1',
            'rmsUid': '3d0c7178-1fc1-4375-9eb2-40451a20f988',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'VidOgranichenija_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});