Ext.define('B4.view.VRIZUList', {
    'alias': 'widget.rms-vrizulist',
    'dataSourceUid': '5db62416-3df3-4972-a540-4abd50af9c5c',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.VRIZUListModel',
    'stateful': true,
    'title': 'Реестр ВРИ ЗУ',
    requires: [
        'B4.model.VRIZUListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-VRIZUEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-VRIZUEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://5db62416-3df3-4972-a540-4abd50af9c5c$d96612dd-5047-4630-ab93-03c58f1eab71',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '003e706e-3d8f-4c2f-89fb-911fb655f16c',
                'sortable': true,
                'text': 'Код',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name1',
                'dataIndexAbsoluteUid': 'FieldPath://5db62416-3df3-4972-a540-4abd50af9c5c$868835cf-0e3b-47aa-be83-eb3e66045386',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'e490efb4-b88d-497d-be00-8bd48b42e225',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'description',
                'dataIndexAbsoluteUid': 'FieldPath://5db62416-3df3-4972-a540-4abd50af9c5c$5672c027-5051-4296-9cdd-8ac839de220e',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '1e1de963-1f78-47c9-9714-ebe9f9e24446',
                'sortable': true,
                'text': 'Деятельность правообладателя',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'parent_name1',
                'dataIndexAbsoluteUid': 'FieldPath://5db62416-3df3-4972-a540-4abd50af9c5c$e26161f2-dabf-43d5-878e-4c9d62051a71$868835cf-0e3b-47aa-be83-eb3e66045386',
                'dataIndexRelativePath': 'parent.name1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0e4a390e-011f-45da-9068-7fb303b33cf9',
                'sortable': true,
                'text': 'Родительский элемент.Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});