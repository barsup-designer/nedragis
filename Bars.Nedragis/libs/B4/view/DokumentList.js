Ext.define('B4.view.DokumentList', {
    'alias': 'widget.rms-dokumentlist',
    'dataSourceUid': 'bb2ade91-5fc2-4846-8ee8-8b717abf46fb',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.DokumentListModel',
    'stateful': true,
    'title': 'Реестр Документ',
    requires: [
        'B4.model.DokumentListModel',
        'B4.ux.grid.plugin.DataExport',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-DokumentEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-DokumentEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Type_name',
                'dataIndexAbsoluteUid': 'FieldPath://bb2ade91-5fc2-4846-8ee8-8b717abf46fb$9c89dcca-5979-4614-8413-7afafab6ae59$4de0c197-d1b9-4da2-90d6-320d9c727e26',
                'dataIndexRelativePath': 'Type.name',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '45f37bd3-781b-41da-b783-d73673f11fb7',
                'sortable': true,
                'text': 'Тип документа',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Number',
                'dataIndexAbsoluteUid': 'FieldPath://bb2ade91-5fc2-4846-8ee8-8b717abf46fb$a786d3f3-7c06-42e9-aca6-8d86b2ff7b68',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2bf6e712-a570-4af8-8537-c6d929fd6fe6',
                'sortable': true,
                'text': 'Номер документа',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Date',
                'dataIndexAbsoluteUid': 'FieldPath://bb2ade91-5fc2-4846-8ee8-8b717abf46fb$b04912a6-57a3-4cbf-9877-2c47c960c735',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '982a8c78-5222-42b7-bb6b-b2f80847c813',
                'sortable': true,
                'text': 'Дата документа',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Basis',
                'dataIndexAbsoluteUid': 'FieldPath://bb2ade91-5fc2-4846-8ee8-8b717abf46fb$1a3cc3b0-ae31-4f92-81f3-ffab64789947',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '8010b5fc-fadc-4004-ad72-5bc4f10f0825',
                'sortable': true,
                'text': 'Основание',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Subject_Representation',
                'dataIndexAbsoluteUid': 'FieldPath://bb2ade91-5fc2-4846-8ee8-8b717abf46fb$4265a9b2-1918-4248-b839-42bac455b5a0$6f8b2489-4290-4747-93ba-ff199586d3bb',
                'dataIndexRelativePath': 'Subject.Representation',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'f6ee4a03-7641-485d-93be-3571d6e50d4c',
                'sortable': true,
                'text': 'Название субъекта',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }, {
                'ptype': 'gridDataExport',
                'reportTitle': 'Реестр Документ'
            }]
        });
        me.callParent(arguments);
    }
});