Ext.define('B4.view.ObEktopiList', {
    'alias': 'widget.rms-obektopilist',
    'dataSourceUid': '2fd9e43e-bd97-4446-a65e-77f22758149b',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.ObEktopiListModel',
    'stateful': true,
    'title': 'Реестр объектопи',
    requires: [
        'B4.model.ObEktopiListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-ObEktopiEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-ObEktopiEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'objectnamereest16',
                'dataIndexAbsoluteUid': 'FieldPath://2fd9e43e-bd97-4446-a65e-77f22758149b$77e911b2-1933-408c-ace6-faa249142519$ee0925dd-63d7-4d98-a2a5-150dd0a9ca04$65de9155-a350-4206-be94-ec95bbf9ef90',
                'dataIndexRelativePath': 'Element1508136885094.Element1507784253669.Element1507203167462',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'f42441ed-59c2-4f9d-9bbf-80e0d18f9473',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование субъектов надзора',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'objectinnreestr16',
                'dataIndexAbsoluteUid': 'FieldPath://2fd9e43e-bd97-4446-a65e-77f22758149b$77e911b2-1933-408c-ace6-faa249142519$aff3cb72-823c-46a9-81b1-13115aaa6464',
                'dataIndexRelativePath': 'Element1508136885094.Element1507784343465',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '3328c592-98b4-487e-8945-1fb5384f29a9',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'ИНН',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'objectn16',
                'dataIndexAbsoluteUid': 'FieldPath://2fd9e43e-bd97-4446-a65e-77f22758149b$77e911b2-1933-408c-ace6-faa249142519$290a41e8-833d-446f-a7f1-aa9dc04f99cb',
                'dataIndexRelativePath': 'Element1508136885094.Element1507784366035',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'adab0974-cf2c-4f49-8ddb-441c4696ba6c',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер по порядку',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'opireestr16',
                'dataIndexAbsoluteUid': 'FieldPath://2fd9e43e-bd97-4446-a65e-77f22758149b$8e828051-8b23-447a-abc8-e0828173b132$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'dataIndexRelativePath': 'Element1508137003934.Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '579ff6d6-98db-4349-b2b3-d25d2aa3eb6c',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'опи.Идентификатор',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});