Ext.define('B4.view.SotrudnikiList', {
    'alias': 'widget.rms-sotrudnikilist',
    'dataSourceUid': '7fec6da9-d7ef-4a54-b1c1-53d17443322d',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.SotrudnikiListModel',
    'stateful': true,
    'title': 'Справочник Сотрудники',
    requires: [
        'B4.model.SotrudnikiListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-SotrudnikiEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-SotrudnikiEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://7fec6da9-d7ef-4a54-b1c1-53d17443322d$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '21de2b08-ceb7-4d16-8f24-716d20e0c3f9',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'fio',
                'dataIndexAbsoluteUid': 'FieldPath://7fec6da9-d7ef-4a54-b1c1-53d17443322d$6a596978-953b-45e7-9691-cc6ae503cb70',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'bc29a6b5-7341-4eeb-be54-ec47e4c96866',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'ФИО',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'dolzh_name',
                'dataIndexAbsoluteUid': 'FieldPath://7fec6da9-d7ef-4a54-b1c1-53d17443322d$e9eccec8-49aa-4ac8-8aff-ba8569b3821a$be94622b-36b7-4326-918a-fa6875f02121',
                'dataIndexRelativePath': 'dolzh.name',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2f8555c1-b382-4463-b975-11595c235046',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Должность.Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'dat',
                'dataIndexAbsoluteUid': 'FieldPath://7fec6da9-d7ef-4a54-b1c1-53d17443322d$97177188-d8a3-4cfe-bfb2-a5c633f0420a',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '24283a36-9bb2-40fe-849c-cfa3dc21ffbb',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});