Ext.define('B4.view.RedaktFamilija', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-redaktfamilija',
    title: 'Редакт_Фамилия',
    rmsUid: '95068834-bc81-432f-b15f-07808dddd60c',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Familija_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Surname',
        'modelProperty': 'Familija_Surname',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '95068834-bc81-432f-b15f-07808dddd60c-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'RedaktFamilija-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Фамилия',
            'maxLength': 60,
            'maxLengthText': '100',
            'minLength': 0,
            'minLengthText': 'Текст короче минимальной длины',
            'modelProperty': 'Familija_Surname',
            'name': 'Surname',
            'rmsUid': 'f93d0e29-47fe-4725-bd0e-0f694ba6215b',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Familija_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});