Ext.define('B4.view.GrafikPlatezhejjEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-grafikplatezhejjeditor',
    title: 'Форма редактирования График платежей',
    rmsUid: '40299054-d19e-45ad-af91-76cb7b5e6ebe',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'GrafikPlatezhejj_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code1',
        'modelProperty': 'GrafikPlatezhejj_code1',
        'type': 'TextField'
    }, {
        'dataIndex': 'name1',
        'modelProperty': 'GrafikPlatezhejj_name1',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '40299054-d19e-45ad-af91-76cb7b5e6ebe-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'GrafikPlatezhejjEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'GrafikPlatezhejj_code1',
            'name': 'code1',
            'rmsUid': '3d19583c-05df-45b2-914b-7933053dc3db',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'GrafikPlatezhejj_name1',
            'name': 'name1',
            'rmsUid': '6f11f713-5faa-4706-bb3c-1374b2e28f60',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'GrafikPlatezhejj_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name1'));
        } else {}
        return res;
    },
});