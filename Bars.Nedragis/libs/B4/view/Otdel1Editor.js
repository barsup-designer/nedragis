Ext.define('B4.view.Otdel1Editor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-otdel1editor',
    title: 'Форма редактирования Отдел',
    rmsUid: '971e2167-b0d8-4d69-a2de-c8e374329bb4',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Otdel1_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'NAME_SP',
        'modelProperty': 'Otdel1_NAME_SP',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '971e2167-b0d8-4d69-a2de-c8e374329bb4-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'Otdel1Editor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'modelProperty': 'Otdel1_NAME_SP',
            'name': 'NAME_SP',
            'rmsUid': '2e2caf06-c7c9-488f-abb7-0437812e6643',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Otdel1_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('NAME_SP'));
        } else {}
        return res;
    },
});