Ext.define('B4.view.OKFSEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-okfseditor',
    title: 'Форма редактирования ОКФС',
    rmsUid: 'ee917c97-23f8-4623-9af4-6d051f92a997',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'OKFS_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'OKFS_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'OKFS_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'check_algorithm',
        'modelProperty': 'OKFS_check_algorithm',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'ee917c97-23f8-4623-9af4-6d051f92a997-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OKFSEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'OKFS_code',
            'name': 'code',
            'rmsUid': '7a75115a-3bb5-4d85-b3dd-42ff01741b2a',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'OKFS_name',
            'name': 'name',
            'rmsUid': '9bcae5e8-e4da-4b3f-aa21-b4d5cd622e8a',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Алгоритм сбора',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'OKFS_check_algorithm',
            'name': 'check_algorithm',
            'rmsUid': '077bcf76-c3a9-4882-b925-4f8489f1148d',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'OKFS_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});