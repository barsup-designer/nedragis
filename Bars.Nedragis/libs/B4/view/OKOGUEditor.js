Ext.define('B4.view.OKOGUEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-okogueditor',
    title: 'Форма редактирования ОКОГУ',
    rmsUid: '06c495f0-1243-44f5-bbc1-cb45263105a3',
    requires: [
        'B4.form.PickerField',
        'B4.model.OKOGU1Model',
        'B4.model.OKOGUEditorModel',
        'B4.view.OKOGU1'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'OKOGU_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'OKOGU_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'OKOGU_name',
        'type': 'TextAreaField'
    }, {
        'dataIndex': 'name_short',
        'modelProperty': 'OKOGU_name_short',
        'type': 'TextField'
    }, {
        'dataIndex': 'parent_id',
        'modelProperty': 'OKOGU_parent_id',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '06c495f0-1243-44f5-bbc1-cb45263105a3-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OKOGUEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'OKOGU_code',
            'name': 'code',
            'rmsUid': '13bd0c3b-43a7-44fa-ad44-b5eb7c28cb72',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'labelAlign': 'left',
            'margin': '10 10 0 10',
            'modelProperty': 'OKOGU_name',
            'name': 'name',
            'rmsUid': '36d081e3-40be-46e4-804f-90fc8bfa3439',
            'xtype': 'textarea'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Краткое наименование',
            'margin': '10 10 0 10',
            'modelProperty': 'OKOGU_name_short',
            'name': 'name_short',
            'rmsUid': '0ee34f55-9cf8-4203-8149-2aa66c22224f',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Родительский элемент',
            'flex': 1,
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.OKOGU1',
            'listViewCtl': 'B4.controller.OKOGU1',
            'margin': '10 10 10 10',
            'maximizable': true,
            'model': 'B4.model.OKOGU1Model',
            'modelProperty': 'OKOGU_parent_id',
            'name': 'parent_id',
            'rmsUid': '14e3015c-9986-4134-8640-db20a69e49a2',
            'textProperty': 'code',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Родительский элемент',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'OKOGU_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});