Ext.define('B4.view.RajjonyJaNAOEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-rajjonyjanaoeditor',
    title: 'Форма редактирования Районы ЯНАО',
    rmsUid: 'e2309e62-7c98-42bf-8913-afe305ba0dd7',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'RajjonyJaNAO_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'RajjonyJaNAO_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'e2309e62-7c98-42bf-8913-afe305ba0dd7-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'RajjonyJaNAOEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование района',
            'modelProperty': 'RajjonyJaNAO_name',
            'name': 'name',
            'rmsUid': 'c1ae4b30-1b5f-4fb8-8349-e7335c2daea6',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'RajjonyJaNAO_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});