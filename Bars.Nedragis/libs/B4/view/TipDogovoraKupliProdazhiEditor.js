Ext.define('B4.view.TipDogovoraKupliProdazhiEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-tipdogovorakupliprodazhieditor',
    title: 'Форма редактирования Тип договора купли-продажи',
    rmsUid: '16c0c2f0-fc70-4047-b183-ca551f2554a3',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'TipDogovoraKupliProdazhi_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'TipDogovoraKupliProdazhi_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'TipDogovoraKupliProdazhi_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '16c0c2f0-fc70-4047-b183-ca551f2554a3-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'TipDogovoraKupliProdazhiEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'TipDogovoraKupliProdazhi_code',
            'name': 'code',
            'rmsUid': '642b7555-e540-4e0b-98f9-ed63ab8fcbc8',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'TipDogovoraKupliProdazhi_name',
            'name': 'name',
            'rmsUid': 'ff042637-9088-483b-a0df-bc2e10591207',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'TipDogovoraKupliProdazhi_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});