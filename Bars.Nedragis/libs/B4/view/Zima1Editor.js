Ext.define('B4.view.Zima1Editor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-zima1editor',
    title: 'Форма редактирования zima1',
    rmsUid: '824ad90e-cbef-4118-8aae-00892a1c6721',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Zima1_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1517294546876',
        'modelProperty': 'Zima1_Element1517294546876',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '824ad90e-cbef-4118-8aae-00892a1c6721-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'Zima1Editor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'zima2',
            'modelProperty': 'Zima1_Element1517294546876',
            'name': 'Element1517294546876',
            'rmsUid': '09e88b87-4487-48f8-a0c1-386346274ee3',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Zima1_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});