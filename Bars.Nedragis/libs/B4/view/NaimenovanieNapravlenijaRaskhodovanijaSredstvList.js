Ext.define('B4.view.NaimenovanieNapravlenijaRaskhodovanijaSredstvList', {
    'alias': 'widget.rms-naimenovanienapravlenijaraskhodovanijasredstvlist',
    'dataSourceUid': 'fae507ae-27bd-4670-8651-083e8137f7b7',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel',
    'stateful': true,
    'title': 'Наименование направления работ',
    requires: [
        'B4.model.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-NaimenovanieNapravlenijaRaskhodovanijaSredstvEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-NaimenovanieNapravlenijaRaskhodovanijaSredstvEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Element1455708457805',
                'dataIndexAbsoluteUid': 'FieldPath://fae507ae-27bd-4670-8651-083e8137f7b7$c34cb84e-a373-4b8a-961c-b3bc90e327ad',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b92c89ff-e21b-4fde-8a86-a055c8336ea0',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'reestrvp',
                'dataIndexAbsoluteUid': 'FieldPath://fae507ae-27bd-4670-8651-083e8137f7b7$3e87114a-562c-491a-9a90-581e274e59f0',
                'dataIndexRelativePath': 'Element1511847414376',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '52fbd57f-e150-4b09-8bc5-ea5404376dc9',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Вид',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});