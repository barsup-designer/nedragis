Ext.define('B4.view.SotrudnikiEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-sotrudnikieditor',
    title: 'Редактирование справочника Сотрудники',
    rmsUid: '9a8aaaad-953d-4101-b278-637eb524ffec',
    requires: [
        'B4.form.PickerField',
        'B4.model.DolzhnostEditorModel',
        'B4.model.DolzhnostListModel',
        'B4.view.DolzhnostList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Sotrudniki_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'fio',
        'modelProperty': 'Sotrudniki_fio',
        'type': 'TextField'
    }, {
        'dataIndex': 'dolzh',
        'modelProperty': 'Sotrudniki_dolzh',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'dat',
        'modelProperty': 'Sotrudniki_dat',
        'type': 'DateField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '9a8aaaad-953d-4101-b278-637eb524ffec-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'SotrudnikiEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Идентификатор',
            'margin': '10 250 10 10',
            'modelProperty': 'cb85f46f-20f7-4e08-a9ea-2ba782988dbb',
            'readOnly': true,
            'rmsUid': 'cb85f46f-20f7-4e08-a9ea-2ba782988dbb',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'ФИО',
            'margin': '10 50 10 10',
            'modelProperty': 'Sotrudniki_fio',
            'name': 'fio',
            'rmsUid': 'f1f9b94d-ab38-4fce-9dd5-f8bb3302b983',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Должность',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.DolzhnostList',
            'listViewCtl': 'B4.controller.DolzhnostList',
            'margin': '10 10 10 10',
            'maximizable': true,
            'model': 'B4.model.DolzhnostListModel',
            'modelProperty': 'Sotrudniki_dolzh',
            'name': 'dolzh',
            'rmsUid': '04370ea8-e4aa-4946-9e12-9822b3a17e49',
            'textProperty': 'name',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Должность',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата',
            'format': 'd.m.Y',
            'margin': '10 250 10 10',
            'modelProperty': 'Sotrudniki_dat',
            'name': 'dat',
            'rmsUid': 'c6f441fc-9776-4e05-ae9f-647858cc5a4d',
            'xtype': 'datefield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Sotrudniki_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});