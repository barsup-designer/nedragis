Ext.define('B4.view.VidPravaNaZemljuEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-vidpravanazemljueditor',
    title: 'Форма редактирования Вид права на землю',
    rmsUid: '1ff32656-60eb-4d76-a40a-4bf5f1cc6893',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'VidPravaNaZemlju_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'VidPravaNaZemlju_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name1',
        'modelProperty': 'VidPravaNaZemlju_name1',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '1ff32656-60eb-4d76-a40a-4bf5f1cc6893-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'VidPravaNaZemljuEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'VidPravaNaZemlju_code',
            'name': 'code',
            'rmsUid': '538a2108-172a-4b20-a293-d3cc18127cd2',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'VidPravaNaZemlju_name1',
            'name': 'name1',
            'rmsUid': '7228ce27-251d-4bf2-8c8f-dbf1ba83ccd8',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'VidPravaNaZemlju_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name1'));
        } else {}
        return res;
    },
});