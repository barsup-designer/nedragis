Ext.define('B4.view.DokumentyUdostoverjajushhieLichnostList', {
    'alias': 'widget.rms-dokumentyudostoverjajushhielichnostlist',
    'dataSourceUid': '3d17ce3e-0019-4ef3-a9cf-ce1304271693',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.DokumentyUdostoverjajushhieLichnostListModel',
    'stateful': true,
    'title': 'Реестр Документы удостоверяющие личность',
    requires: [
        'B4.model.DokumentyUdostoverjajushhieLichnostListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-DokumentyUdostoverjajushhieLichnostEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-DokumentyUdostoverjajushhieLichnostEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://3d17ce3e-0019-4ef3-a9cf-ce1304271693$5ce026a8-89e4-44ec-9669-1c928ba4b878',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '9fe61ce3-40e6-4b48-8b15-696577b6c727',
                'sortable': true,
                'text': 'Код',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name1',
                'dataIndexAbsoluteUid': 'FieldPath://3d17ce3e-0019-4ef3-a9cf-ce1304271693$9375b11f-ca95-422e-ba8f-747109900d60',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 2,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '15468d28-6a6c-4426-8ccd-b63ab3979270',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'description',
                'dataIndexAbsoluteUid': 'FieldPath://3d17ce3e-0019-4ef3-a9cf-ce1304271693$f8281e46-56ff-4768-95cd-9d7fdb24d397',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 2,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '1f9470ac-1226-4ea4-918d-0716b8ba8944',
                'sortable': true,
                'text': 'Примечание',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});