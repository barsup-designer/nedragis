Ext.define('B4.view.VidOshibkiEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-vidoshibkieditor',
    title: 'Форма редактирования Вид ошибки',
    rmsUid: '7f01485a-5358-4a23-bba3-4a7832bff84a',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'VidOshibki_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1511848489595',
        'modelProperty': 'VidOshibki_Element1511848489595',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '7f01485a-5358-4a23-bba3-4a7832bff84a-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'VidOshibkiEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Вид ошибки',
            'modelProperty': 'VidOshibki_Element1511848489595',
            'name': 'Element1511848489595',
            'rmsUid': '956af6c7-444d-43d5-bc9a-7943c3c65654',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'VidOshibki_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});