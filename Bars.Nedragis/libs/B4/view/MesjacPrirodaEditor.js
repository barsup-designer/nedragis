Ext.define('B4.view.MesjacPrirodaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-mesjacprirodaeditor',
    title: 'Форма редактирования МесяцПрирода',
    rmsUid: '485ab932-dec6-4253-8440-c1e85750719d',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'MesjacPriroda_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'month_pr',
        'modelProperty': 'MesjacPriroda_month_pr',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '485ab932-dec6-4253-8440-c1e85750719d-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'MesjacPrirodaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Идентификатор',
            'margin': '20 200 20 20',
            'modelProperty': '8a806b4a-9793-472d-9fb5-b3f66a7a9194',
            'readOnly': true,
            'rmsUid': '8a806b4a-9793-472d-9fb5-b3f66a7a9194',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Отчетный месяц',
            'margin': '20 200 20 20',
            'modelProperty': 'MesjacPriroda_month_pr',
            'name': 'month_pr',
            'rmsUid': '37c37dba-d5ba-45a7-ab64-2c0189faeb9a',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'MesjacPriroda_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});