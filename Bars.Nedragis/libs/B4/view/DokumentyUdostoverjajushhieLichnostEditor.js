Ext.define('B4.view.DokumentyUdostoverjajushhieLichnostEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-dokumentyudostoverjajushhielichnosteditor',
    title: 'Форма редактирования Документы, удостоверяющие личность',
    rmsUid: 'ad3a4cd5-6eb7-434b-8221-8ea081492761',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'DokumentyUdostoverjajushhieLichnost_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'DokumentyUdostoverjajushhieLichnost_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name1',
        'modelProperty': 'DokumentyUdostoverjajushhieLichnost_name1',
        'type': 'TextField'
    }, {
        'dataIndex': 'description',
        'modelProperty': 'DokumentyUdostoverjajushhieLichnost_description',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'ad3a4cd5-6eb7-434b-8221-8ea081492761-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'DokumentyUdostoverjajushhieLichnostEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'DokumentyUdostoverjajushhieLichnost_code',
            'name': 'code',
            'rmsUid': 'f6929b3a-ab62-41e8-8b52-efd4377e8fd9',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'DokumentyUdostoverjajushhieLichnost_name1',
            'name': 'name1',
            'rmsUid': '9bb32a94-4a9e-430d-b40c-cad954330be1',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Примечание',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'DokumentyUdostoverjajushhieLichnost_description',
            'name': 'description',
            'rmsUid': 'f1e62af5-7eff-4e39-a7de-6b4aefe6200d',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'DokumentyUdostoverjajushhieLichnost_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name1'));
        } else {}
        return res;
    },
});