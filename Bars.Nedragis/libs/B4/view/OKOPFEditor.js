Ext.define('B4.view.OKOPFEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-okopfeditor',
    title: 'Форма редактирования ОКОПФ',
    rmsUid: '821f31b0-9c1a-4914-897b-86c7681df4ea',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'OKOPF_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'OKOPF_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'OKOPF_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '821f31b0-9c1a-4914-897b-86c7681df4ea-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OKOPFEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 0 10',
            'modelProperty': 'OKOPF_code',
            'name': 'code',
            'rmsUid': '376efe7f-8a2e-45f5-94e7-a045c319aa6f',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'OKOPF_name',
            'name': 'name',
            'rmsUid': 'd089190f-1e2c-4723-bbfc-2956959fc3f6',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'OKOPF_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});