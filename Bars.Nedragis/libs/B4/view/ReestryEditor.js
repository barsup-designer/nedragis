Ext.define('B4.view.ReestryEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-reestryeditor',
    title: 'Форма редактирования Реестры',
    rmsUid: 'c033f698-9e04-4dbd-840e-bd4652ec3132',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Reestry_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'Reestry_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'Reestry_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'c033f698-9e04-4dbd-840e-bd4652ec3132-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ReestryEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'Reestry_code',
            'name': 'code',
            'rmsUid': '71ff4afa-37be-478e-86b1-61e986cb492f',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'Reestry_name',
            'name': 'name',
            'rmsUid': '4cab3045-8b25-4595-ab00-b56b5c4bc547',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Reestry_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});