Ext.define('B4.view.NaimenovanieProektaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-naimenovanieproektaeditor',
    title: 'Форма редактирования Наименование проекта',
    rmsUid: 'e79a2917-f34b-43dd-ac6b-32d488700d2f',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'NaimenovanieProekta_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1511755872431',
        'modelProperty': 'NaimenovanieProekta_Element1511755872431',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'e79a2917-f34b-43dd-ac6b-32d488700d2f-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'NaimenovanieProektaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование проекта',
            'modelProperty': 'NaimenovanieProekta_Element1511755872431',
            'name': 'Element1511755872431',
            'rmsUid': '497aa7be-b446-4b0c-9936-80e67280b5c2',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'NaimenovanieProekta_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1511755872431'));
        } else {}
        return res;
    },
});