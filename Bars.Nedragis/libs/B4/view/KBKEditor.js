Ext.define('B4.view.KBKEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-kbkeditor',
    title: 'Форма редактирования КБК',
    rmsUid: '56275727-c762-4891-b1e5-66389e1dfc5d',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'KBK_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'KBK_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name1',
        'modelProperty': 'KBK_name1',
        'type': 'TextAreaField'
    }, {
        'dataIndex': 'date_from',
        'modelProperty': 'KBK_date_from',
        'type': 'DateField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '56275727-c762-4891-b1e5-66389e1dfc5d-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'KBKEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'KBK_code',
            'name': 'code',
            'rmsUid': '969731c1-0faf-49ca-af75-765815afdbb5',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'height': 130,
            'labelAlign': 'left',
            'margin': '10 10 0 10',
            'modelProperty': 'KBK_name1',
            'name': 'name1',
            'rmsUid': '4f9def96-b24b-432d-91cb-82249612b9cb',
            'xtype': 'textarea'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата начала действия',
            'flex': 1,
            'format': 'd.m.Y',
            'margin': '10 10 10 10',
            'modelProperty': 'KBK_date_from',
            'name': 'date_from',
            'rmsUid': '59a6782e-2acd-45f9-b1c8-461771adf6e2',
            'xtype': 'datefield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'KBK_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name1'));
        } else {}
        return res;
    },
});