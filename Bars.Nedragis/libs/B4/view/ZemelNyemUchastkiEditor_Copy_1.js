Ext.define('B4.view.ZemelNyemUchastkiEditor_Copy_1', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-zemelnyemuchastkieditor_copy_1',
    title: 'ЗУ Основные сведения',
    rmsUid: '315c8824-5d75-403a-af20-9ea6254be00b',
    requires: [
        'B4.form.PickerField',
        'B4.model.JuLRedaktirovanieModel',
        'B4.model.JuridicheskieLicaListModel',
        'B4.model.KategoriiZemelEditorModel',
        'B4.model.KategoriiZemelListModel',
        'B4.model.OKATO1Model',
        'B4.model.OKATOEditorModel',
        'B4.model.OKTMO1Model',
        'B4.model.OKTMOEditorModel',
        'B4.model.StatusyObEktaEditorModel',
        'B4.model.StatusyObEktaListModel',
        'B4.model.TipyObEktaPravaEditorModel',
        'B4.model.TipyObEktaPravaListModel',
        'B4.model.TipZemelNogoUchastkaEditorModel',
        'B4.model.TipZemelNogoUchastkaListModel',
        'B4.model.VidOgranichenijaEditorModel',
        'B4.model.VidOgranichenijaListModel',
        'B4.model.VidPravaNaZemljuEditorModel',
        'B4.model.VidPravaNaZemljuListModel',
        'B4.model.VRIZUEditorModel',
        'B4.model.VRIZUIerarkhijaModel',
        'B4.view.JuridicheskieLicaList',
        'B4.view.KategoriiZemelList',
        'B4.view.OKATO1',
        'B4.view.OKTMO1',
        'B4.view.StatusyObEktaList',
        'B4.view.TipyObEktaPravaList',
        'B4.view.TipZemelNogoUchastkaList',
        'B4.view.VidOgranichenijaList',
        'B4.view.VidPravaNaZemljuList',
        'B4.view.VRIZUIerarkhija'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'ZemelNyemUchastki_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'typeobj',
        'modelProperty': 'ZemelNyemUchastki_typeobj',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'TypeZU',
        'modelProperty': 'ZemelNyemUchastki_TypeZU',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'cad_okrug',
        'modelProperty': 'ZemelNyemUchastki_cad_okrug',
        'type': 'NumberField'
    }, {
        'dataIndex': 'cad_raion',
        'modelProperty': 'ZemelNyemUchastki_cad_raion',
        'type': 'NumberField'
    }, {
        'dataIndex': 'cad_block',
        'modelProperty': 'ZemelNyemUchastki_cad_block',
        'type': 'NumberField'
    }, {
        'dataIndex': 'cad_massiv',
        'modelProperty': 'ZemelNyemUchastki_cad_massiv',
        'type': 'NumberField'
    }, {
        'dataIndex': 'cad_kvartal',
        'modelProperty': 'ZemelNyemUchastki_cad_kvartal',
        'type': 'NumberField'
    }, {
        'dataIndex': 'number_ZU',
        'modelProperty': 'ZemelNyemUchastki_number_ZU',
        'type': 'NumberField'
    }, {
        'dataIndex': 'cadnum1',
        'modelProperty': 'ZemelNyemUchastki_cadnum1',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'ZemelNyemUchastki_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'naznach',
        'modelProperty': 'ZemelNyemUchastki_naznach',
        'type': 'TextField'
    }, {
        'dataIndex': 'area',
        'modelProperty': 'ZemelNyemUchastki_area',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Status_obj',
        'modelProperty': 'ZemelNyemUchastki_Status_obj',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'category',
        'modelProperty': 'ZemelNyemUchastki_category',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'vid_prava',
        'modelProperty': 'ZemelNyemUchastki_vid_prava',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'vid_use',
        'modelProperty': 'ZemelNyemUchastki_vid_use',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'holder_id',
        'modelProperty': 'ZemelNyemUchastki_holder_id',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'bremya',
        'modelProperty': 'ZemelNyemUchastki_bremya',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'owner_id',
        'modelProperty': 'ZemelNyemUchastki_owner_id',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'okato',
        'modelProperty': 'ZemelNyemUchastki_okato',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'oktmo',
        'modelProperty': 'ZemelNyemUchastki_oktmo',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'property_manager_id',
        'modelProperty': 'ZemelNyemUchastki_property_manager_id',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Borders',
        'modelProperty': 'ZemelNyemUchastki_Borders',
        'type': 'TextAreaField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '315c8824-5d75-403a-af20-9ea6254be00b-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ZemelNyemUchastkiEditor_Copy_1-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Тип объекта права',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'listView': 'B4.view.TipyObEktaPravaList',
                    'listViewCtl': 'B4.controller.TipyObEktaPravaList',
                    'maximizable': true,
                    'model': 'B4.model.TipyObEktaPravaListModel',
                    'modelProperty': 'ZemelNyemUchastki_typeobj',
                    'name': 'typeobj',
                    'rmsUid': 'f45dd4d8-3e0a-464b-910e-6ecf8f7b3a66',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Тип объекта права',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '9cdfc907-550c-45c1-bff6-ed94c1ba22d0',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Тип земельного участка',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'listView': 'B4.view.TipZemelNogoUchastkaList',
                    'listViewCtl': 'B4.controller.TipZemelNogoUchastkaList',
                    'maximizable': true,
                    'model': 'B4.model.TipZemelNogoUchastkaListModel',
                    'modelProperty': 'ZemelNyemUchastki_TypeZU',
                    'name': 'TypeZU',
                    'rmsUid': 'de3b7de1-5e32-4e8a-a778-4c57e178492c',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Тип земельного участка',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'margin': '0 0 0 10',
                'rmsUid': 'f12d9bef-6891-414b-9252-f7e0bdb4b0a0',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '0 0 10 0',
            'rmsUid': '6e3f64a5-944e-402d-9848-ffad4a0f1280',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'dockedItems': [],
                'fieldLabel': 'Кадастровый номер',
                'items': [{
                    'allowBlank': true,
                    'allowDecimals': false,
                    'decimalPrecision': 0,
                    'fieldLabel': 'Кад. округ',
                    'flex': 1,
                    'hideTrigger': true,
                    'labelAlign': 'top',
                    'labelWidth': 50,
                    'margin': '0 15 0 5',
                    'maxValue': 91,
                    'minValue': 1,
                    'modelProperty': 'ZemelNyemUchastki_cad_okrug',
                    'name': 'cad_okrug',
                    'rmsUid': '98cab5b3-10af-4147-a3d8-3c25639eceae',
                    'step': 1,
                    'width': 28,
                    'xtype': 'numberfield'
                }, {
                    'allowBlank': true,
                    'allowDecimals': false,
                    'decimalPrecision': 0,
                    'fieldLabel': 'Кад. район',
                    'flex': 1,
                    'hideTrigger': true,
                    'labelAlign': 'top',
                    'labelWidth': 50,
                    'margin': '0 15 0 5',
                    'maxValue': 99,
                    'minValue': 1,
                    'modelProperty': 'ZemelNyemUchastki_cad_raion',
                    'name': 'cad_raion',
                    'rmsUid': '2b41b5cd-e104-40d6-9ac5-176c6f20bdd1',
                    'step': 1,
                    'width': 28,
                    'xtype': 'numberfield'
                }, {
                    'allowBlank': true,
                    'allowDecimals': false,
                    'decimalPrecision': 0,
                    'fieldLabel': 'Кад. блок',
                    'flex': 1,
                    'hideTrigger': true,
                    'labelAlign': 'top',
                    'margin': '0 15 0 5',
                    'maxValue': 99,
                    'minValue': 1,
                    'modelProperty': 'ZemelNyemUchastki_cad_block',
                    'name': 'cad_block',
                    'rmsUid': 'cceee588-c86b-4cad-b33d-6968d4a33f7d',
                    'step': 1,
                    'width': 28,
                    'xtype': 'numberfield'
                }, {
                    'allowBlank': true,
                    'allowDecimals': false,
                    'decimalPrecision': 0,
                    'fieldLabel': 'Кад. массив',
                    'flex': 1,
                    'hideTrigger': true,
                    'labelAlign': 'top',
                    'margin': '0 15 0 5',
                    'maxValue': 99,
                    'minValue': 1,
                    'modelProperty': 'ZemelNyemUchastki_cad_massiv',
                    'name': 'cad_massiv',
                    'rmsUid': '9114bbeb-07b7-45b4-92be-851083ae3151',
                    'step': 1,
                    'width': 28,
                    'xtype': 'numberfield'
                }, {
                    'allowBlank': true,
                    'allowDecimals': false,
                    'anchorPx': 40,
                    'decimalPrecision': 0,
                    'fieldLabel': 'Кад. квартал',
                    'flex': 2,
                    'hideTrigger': true,
                    'labelAlign': 'top',
                    'margin': '0 15 0 5',
                    'minValue': 1,
                    'modelProperty': 'ZemelNyemUchastki_cad_kvartal',
                    'name': 'cad_kvartal',
                    'rmsUid': 'f17c362e-2a96-40dd-8a98-4aadbe7c186e',
                    'step': 1,
                    'width': 60,
                    'xtype': 'numberfield'
                }, {
                    'allowBlank': true,
                    'allowDecimals': false,
                    'decimalPrecision': 0,
                    'fieldLabel': 'Номер участка',
                    'flex': 2,
                    'hideTrigger': true,
                    'labelAlign': 'top',
                    'labelWidth': 60,
                    'margin': '0 25 0 5',
                    'maxValue': 9999,
                    'minValue': 1,
                    'modelProperty': 'ZemelNyemUchastki_number_ZU',
                    'name': 'number_ZU',
                    'rmsUid': '45aa6ef9-23b9-41d4-a1ba-fad9cc61da48',
                    'step': 1,
                    'width': 70,
                    'xtype': 'numberfield'
                }, {
                    'allowBlank': true,
                    'fieldCls': 'x-form-field x-form-field-custom',
                    'fieldLabel': 'Полный кадастровый номер',
                    'labelAlign': 'top',
                    'labelWidth': 140,
                    'margin': '13 0 0 10',
                    'modelProperty': 'ZemelNyemUchastki_cadnum1',
                    'name': 'cadnum1',
                    'readOnly': true,
                    'rmsReadOnly': true,
                    'rmsUid': 'e7329ddb-1276-49c6-ba0a-821ea948ad72',
                    'width': 320,
                    'xtype': 'textfield'
                }],
                'layout': {
                    'align': 'stretch',
                    'pack': 'center',
                    'type': 'column'
                },
                'margin': '0 5 5 5',
                'region': 'East',
                'rmsUid': 'ee89b7e7-5334-4049-a820-584e618e68b0',
                'title': 'Кадастровый номер',
                'xtype': 'fieldset'
            }],
            'layout': {
                'type': 'fit'
            },
            'rmsUid': '1c6ee763-7a26-4393-a688-19b4ae539bfa',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 2,
            'items': [{
                'allowBlank': true,
                'fieldLabel': 'Наименование',
                'labelWidth': 135,
                'margin': '15 0 5 0',
                'modelProperty': 'ZemelNyemUchastki_name',
                'name': 'name',
                'rmsUid': 'ec2e5b64-7591-4af1-a53b-434c1f263ae3',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Назначение',
                'flex': 5,
                'labelWidth': 135,
                'margin': '10 0 10 0',
                'modelProperty': 'ZemelNyemUchastki_naznach',
                'name': 'naznach',
                'rmsUid': '90696a1e-c508-41d6-a586-32216ab1688f',
                'xtype': 'textfield'
            }],
            'layout': {
                'type': 'fit'
            },
            'margin': '5 5 5 5',
            'rmsUid': '638daebf-fc9e-422e-a39a-c5743dd29e3b',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'decimalPrecision': 2,
                    'fieldLabel': 'Общая площадь, кв. м',
                    'labelWidth': 135,
                    'margin': '5 5 5 5',
                    'minValue': 0,
                    'modelProperty': 'ZemelNyemUchastki_area',
                    'name': 'area',
                    'rmsUid': '684a8903-5e4a-4811-9af6-453a5e522145',
                    'step': 1,
                    'xtype': 'numberfield'
                }],
                'layout': 'anchor',
                'rmsUid': '2f33458a-b716-4631-a95d-3c31fd33a4d8',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Статус объекта',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 120,
                    'listView': 'B4.view.StatusyObEktaList',
                    'listViewCtl': 'B4.controller.StatusyObEktaList',
                    'margin': '5 5 5 10',
                    'maximizable': true,
                    'model': 'B4.model.StatusyObEktaListModel',
                    'modelProperty': 'ZemelNyemUchastki_Status_obj',
                    'name': 'Status_obj',
                    'rmsUid': '2fd14bb8-a655-49cc-8848-dadefa4e5a7f',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Статус объекта',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '730a3050-396f-462c-a7ea-a169a38661f2',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '449da937-098a-49a5-a453-ca2490e7675d',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Категория земель',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 135,
                    'listView': 'B4.view.KategoriiZemelList',
                    'listViewCtl': 'B4.controller.KategoriiZemelList',
                    'margin': '5 5 5 5',
                    'maximizable': true,
                    'model': 'B4.model.KategoriiZemelListModel',
                    'modelProperty': 'ZemelNyemUchastki_category',
                    'name': 'category',
                    'rmsUid': '8eef49a7-bcf0-479e-82bf-171993dc6998',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Категория земель',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '30e68800-1fe0-4040-893c-09c808324cfb',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Вид права на землю',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 120,
                    'listView': 'B4.view.VidPravaNaZemljuList',
                    'listViewCtl': 'B4.controller.VidPravaNaZemljuList',
                    'margin': '5 5 5 10',
                    'maximizable': true,
                    'model': 'B4.model.VidPravaNaZemljuListModel',
                    'modelProperty': 'ZemelNyemUchastki_vid_prava',
                    'name': 'vid_prava',
                    'rmsUid': 'e6b82795-fd0c-4e0d-8e39-0091102d1c94',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Вид права на землю',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '13bea559-978d-4782-8633-599c9813e24f',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '8 0 0 0',
            'rmsUid': 'e1e54308-dfd8-47b0-818e-6b6464680530',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Вид разрешенного использования',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 135,
                    'listView': 'B4.view.VRIZUIerarkhija',
                    'listViewCtl': 'B4.controller.VRIZUIerarkhija',
                    'margin': '5 5 5 5',
                    'maximizable': true,
                    'model': 'B4.model.VRIZUIerarkhijaModel',
                    'modelProperty': 'ZemelNyemUchastki_vid_use',
                    'name': 'vid_use',
                    'rmsUid': 'b4335fef-0f01-42ec-9da3-0d49177474d0',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Вид разрешенного использования',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '4ac985dd-abb2-45b6-8761-335b094eff29',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Правообладатель',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 120,
                    'listView': 'B4.view.JuridicheskieLicaList',
                    'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                    'margin': '8 5 5 10',
                    'maximizable': true,
                    'model': 'B4.model.JuridicheskieLicaListModel',
                    'modelProperty': 'ZemelNyemUchastki_holder_id',
                    'name': 'holder_id',
                    'rmsUid': '6d4b7379-2e15-4d1f-893d-8f631e4e8608',
                    'textProperty': 'OKATO_name1',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Правообладатель',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'd591486f-2749-4f46-8c72-4072f753b482',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'fb9cc4a9-883d-4252-abe0-e6ee12ab16f8',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Обременения',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 135,
                    'listView': 'B4.view.VidOgranichenijaList',
                    'listViewCtl': 'B4.controller.VidOgranichenijaList',
                    'margin': '5 5 5 5',
                    'maximizable': true,
                    'model': 'B4.model.VidOgranichenijaListModel',
                    'modelProperty': 'ZemelNyemUchastki_bremya',
                    'name': 'bremya',
                    'rmsUid': '9181e440-1e37-44df-9cc7-4c9751787065',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Обременения',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'e3a26beb-ab5d-4e77-af78-dab1efd09ecf',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Собственник',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 120,
                    'listView': 'B4.view.JuridicheskieLicaList',
                    'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                    'margin': '5 5 5 10',
                    'maximizable': true,
                    'model': 'B4.model.JuridicheskieLicaListModel',
                    'modelProperty': 'ZemelNyemUchastki_owner_id',
                    'name': 'owner_id',
                    'rmsUid': '75060ae0-0e43-49df-90ce-7dd8cb1bc9cd',
                    'textProperty': 'OKATO_name1',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Собственник',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '5a56a0e0-bcfd-4d25-9f7f-6eb72d398947',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '24c79702-f6b0-4129-ac91-6a5a80a7fc84',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'ОКАТО',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 135,
                    'listView': 'B4.view.OKATO1',
                    'listViewCtl': 'B4.controller.OKATO1',
                    'margin': '5 5 5 5',
                    'maximizable': true,
                    'model': 'B4.model.OKATO1Model',
                    'modelProperty': 'ZemelNyemUchastki_okato',
                    'name': 'okato',
                    'rmsUid': '2f67ca2d-2709-450c-b9df-fb6193c1b27d',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'ОКАТО',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '622e91dd-f923-402f-852a-24da98b6df66',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'ОКТМО',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 120,
                    'listView': 'B4.view.OKTMO1',
                    'listViewCtl': 'B4.controller.OKTMO1',
                    'margin': '5 5 5 10',
                    'maximizable': true,
                    'model': 'B4.model.OKTMO1Model',
                    'modelProperty': 'ZemelNyemUchastki_oktmo',
                    'name': 'oktmo',
                    'rmsUid': '6908a298-30aa-407c-a735-eaac3f30d0b0',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'ОКТМО',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '369facd3-27da-40b4-baeb-fd3d03a54e3d',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '5 0 0 0',
            'rmsUid': '2ad7507e-6b03-40af-ad6a-413f6011335a',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Уполномоченный орган',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 135,
                    'listView': 'B4.view.JuridicheskieLicaList',
                    'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                    'maximizable': true,
                    'model': 'B4.model.JuridicheskieLicaListModel',
                    'modelProperty': 'ZemelNyemUchastki_property_manager_id',
                    'name': 'property_manager_id',
                    'rmsUid': '5d33ff7c-a2c9-4696-959b-4270d3dd886f',
                    'textProperty': 'OKATO_name1',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Уполномоченный орган',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '6090ea3a-644b-4cc3-a172-f9288d1228b8',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '05583d23-1608-4f3d-9a72-f9198dfe0c0e',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'allowBlank': true,
                'fieldLabel': 'Описание месторасположения границ',
                'labelAlign': 'left',
                'labelWidth': 135,
                'margin': '15 5 5 5',
                'modelProperty': 'ZemelNyemUchastki_Borders',
                'name': 'Borders',
                'rmsUid': 'a94f8dc5-cc96-411f-bc62-141402bb5dd7',
                'xtype': 'textarea'
            }],
            'layout': {
                'type': 'fit'
            },
            'rmsUid': '3861c26f-7503-4a53-99b5-3f4718e0738e',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'ZemelNyemUchastki_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle('Основные сведения');
        } else {}
        return res;
    },
});