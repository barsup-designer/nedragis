Ext.define('B4.view.RegGosNadzorZaGeoEditor2', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-reggosnadzorzageoeditor2',
    title: 'Форма редактирования ОПИ2',
    rmsUid: 'c622aba8-216c-498c-ac35-2d4fb7263564',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'RegGosNadzorZaGeo_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1507635501632',
        'modelProperty': 'RegGosNadzorZaGeo_Element1507635501632',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507635537755',
        'modelProperty': 'RegGosNadzorZaGeo_Element1507635537755',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1507635559781',
        'modelProperty': 'RegGosNadzorZaGeo_Element1507635559781',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1507635586209',
        'modelProperty': 'RegGosNadzorZaGeo_Element1507635586209',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'c622aba8-216c-498c-ac35-2d4fb7263564-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'RegGosNadzorZaGeoEditor2-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Номер лицензии',
            'margin': '20 15 0 15',
            'modelProperty': 'RegGosNadzorZaGeo_Element1507635501632',
            'name': 'Element1507635501632',
            'rmsUid': '8c484782-6b37-4a14-8f9e-d41975e8a0df',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата регистрации',
            'format': 'd.m.Y',
            'margin': '0 15 0 15',
            'modelProperty': 'RegGosNadzorZaGeo_Element1507635537755',
            'name': 'Element1507635537755',
            'rmsUid': '87176540-1bac-454f-a424-667fb657dbb6',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Окончание срока действия',
            'format': 'd.m.Y',
            'margin': '0 15 0 15',
            'modelProperty': 'RegGosNadzorZaGeo_Element1507635559781',
            'name': 'Element1507635559781',
            'rmsUid': '8542b1b3-2992-4916-8401-fa81eb76b58a',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Местонаходжение',
            'margin': '0 15 0 15',
            'modelProperty': 'RegGosNadzorZaGeo_Element1507635586209',
            'name': 'Element1507635586209',
            'rmsUid': 'ac8e46cc-ecbd-4b0c-b969-f40cca78f534',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'RegGosNadzorZaGeo_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1507635501632'));
        } else {}
        return res;
    },
});