Ext.define('B4.view.VidRabotOPINFEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-vidrabotopinfeditor',
    title: 'Форма редактирования Вид работ ОПИНФ',
    rmsUid: '77208b5f-1a03-457a-b073-5f16995563c7',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'VidRabotOPINF_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'VidRabotOPINF222',
        'modelProperty': 'VidRabotOPINF_VidRabotOPINF222',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1510134562537',
        'modelProperty': 'VidRabotOPINF_Element1510134562537',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '77208b5f-1a03-457a-b073-5f16995563c7-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'VidRabotOPINFEditor-container',
        'layout': {
            'type': 'anchor'
        },
        'margin': '20 10 0 10',
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Вид работ',
            'height': 40,
            'margin': '20 0 20 0',
            'modelProperty': 'VidRabotOPINF_VidRabotOPINF222',
            'name': 'VidRabotOPINF222',
            'rmsUid': 'c731c7b3-967d-49b9-a6c3-fd7548d43550',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Подвид работ',
            'height': 40,
            'modelProperty': 'VidRabotOPINF_Element1510134562537',
            'name': 'Element1510134562537',
            'rmsUid': 'b3015764-0612-4ec5-b840-378d7561a476',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'VidRabotOPINF_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});