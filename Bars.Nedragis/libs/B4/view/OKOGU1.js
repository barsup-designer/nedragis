Ext.define('B4.view.OKOGU1', {
    'alias': 'widget.rms-okogu1',
    'dataSourceUid': '25e226c5-654b-43a6-b9ff-36d48b8e7d0b',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': false,
    'extend': 'B4.base.registry.TreeView',
    'model': 'B4.model.OKOGU1Model',
    'parentFieldName': 'parent_id',
    'stateful': true,
    'title': 'ОКОГУ',
    requires: [
        'B4.model.OKOGU1Model',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'content-img-icons-reload-png',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-add-png',
        'itemId': 'Addition-OKOGUEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-edit-png',
        'itemId': 'Editing-OKOGUEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-delete-png',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://25e226c5-654b-43a6-b9ff-36d48b8e7d0b$29c6426f-9226-4196-8657-18f2392e98c1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '503d5ac7-9a9f-43b7-8357-d93907feee0c',
                'sortable': true,
                'text': 'Код',
                'xtype': 'treecolumn'
            }, {
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://25e226c5-654b-43a6-b9ff-36d48b8e7d0b$37563da5-e29b-4a9b-8591-e73d00cb14ce',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 2,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '1c48979d-ea60-4247-b5e5-aa16ffc93de6',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name_short',
                'dataIndexAbsoluteUid': 'FieldPath://25e226c5-654b-43a6-b9ff-36d48b8e7d0b$37aee374-c664-411b-a3d9-134df085d2a3',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '24117f21-c246-43e1-aca6-25be2cd4aa18',
                'sortable': true,
                'text': 'Краткое наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://25e226c5-654b-43a6-b9ff-36d48b8e7d0b$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '4e7913b3-30d6-4969-9b39-c3246da5a3d2',
                'sortable': true,
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }]
        });
        me.callParent(arguments);
    }
});