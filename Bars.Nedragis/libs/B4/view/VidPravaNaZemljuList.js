Ext.define('B4.view.VidPravaNaZemljuList', {
    'alias': 'widget.rms-vidpravanazemljulist',
    'dataSourceUid': '5a74b33e-dffc-409a-af27-01e29e56ce83',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.VidPravaNaZemljuListModel',
    'stateful': true,
    'title': 'Реестр Вид права на землю',
    requires: [
        'B4.model.VidPravaNaZemljuListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-VidPravaNaZemljuEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-VidPravaNaZemljuEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://5a74b33e-dffc-409a-af27-01e29e56ce83$c96ee2e7-1e0a-4f09-bcd1-96337d21c91a',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'fc366282-dd80-4ba7-9b9b-e810502cef39',
                'sortable': true,
                'text': 'Код',
                'width': 55,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name1',
                'dataIndexAbsoluteUid': 'FieldPath://5a74b33e-dffc-409a-af27-01e29e56ce83$ea2c10dd-7114-412e-9b4a-7c4936c0c672',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'f0862288-4fe3-40b8-988d-c276c5b76946',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});