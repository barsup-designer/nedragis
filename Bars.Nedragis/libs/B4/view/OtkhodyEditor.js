Ext.define('B4.view.OtkhodyEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-otkhodyeditor',
    title: 'Форма редактирования Отходы',
    rmsUid: '4b113066-676a-46d9-81db-8129608656be',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Otkhody_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1507790021761',
        'modelProperty': 'Otkhody_Element1507790021761',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507791662904',
        'modelProperty': 'Otkhody_Element1507791662904',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507791696742',
        'modelProperty': 'Otkhody_Element1507791696742',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '4b113066-676a-46d9-81db-8129608656be-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OtkhodyEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование объекта образования отходов',
            'modelProperty': 'Otkhody_Element1507790021761',
            'name': 'Element1507790021761',
            'rmsUid': '4dcd4047-cb4c-4fd1-8bca-3b0fe52016ec',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Местонахождение',
            'modelProperty': 'Otkhody_Element1507791662904',
            'name': 'Element1507791662904',
            'rmsUid': 'bd24bf5b-5952-4747-890d-d085c1715ac7',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Проект нормативов образования отходов и лимитов на их размещение',
            'modelProperty': 'Otkhody_Element1507791696742',
            'name': 'Element1507791696742',
            'rmsUid': '2c23a81b-7b71-4a50-8a7d-4b27bff96aac',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Otkhody_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1507790021761'));
        } else {}
        return res;
    },
});