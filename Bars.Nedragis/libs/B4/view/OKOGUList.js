Ext.define('B4.view.OKOGUList', {
    'alias': 'widget.rms-okogulist',
    'dataSourceUid': '25e226c5-654b-43a6-b9ff-36d48b8e7d0b',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.OKOGUListModel',
    'stateful': true,
    'title': 'Реестр ОКОГУ',
    requires: [
        'B4.model.OKOGUListModel',
        'B4.ux.grid.plugin.DataExport',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-OKOGUEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-OKOGUEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://25e226c5-654b-43a6-b9ff-36d48b8e7d0b$29c6426f-9226-4196-8657-18f2392e98c1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'e19e5626-05b4-444f-a45f-51c9a77fe1bf',
                'sortable': true,
                'text': 'Код',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://25e226c5-654b-43a6-b9ff-36d48b8e7d0b$37563da5-e29b-4a9b-8591-e73d00cb14ce',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '048c4620-54f0-4efe-8c2a-a6a54af4c42f',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'parent_id_name',
                'dataIndexAbsoluteUid': 'FieldPath://25e226c5-654b-43a6-b9ff-36d48b8e7d0b$005d56c1-90c3-4611-8d0e-efb1378307f6$37563da5-e29b-4a9b-8591-e73d00cb14ce',
                'dataIndexRelativePath': 'parent_id.name',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '72e41904-bb24-4de3-a9b3-dc50b1c50aab',
                'sortable': true,
                'text': 'Родительский элемент.Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://25e226c5-654b-43a6-b9ff-36d48b8e7d0b$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0f63879d-44f8-40ab-83f8-81c7d3b72425',
                'sortable': true,
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }, {
                'ptype': 'gridDataExport',
                'reportTitle': 'Реестр ОКОГУ'
            }]
        });
        me.callParent(arguments);
    }
});