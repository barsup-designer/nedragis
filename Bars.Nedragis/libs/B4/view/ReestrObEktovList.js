Ext.define('B4.view.ReestrObEktovList', {
    'alias': 'widget.rms-reestrobektovlist',
    'dataSourceUid': '69f498ef-df00-409a-a699-c742d843e803',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.ReestrObEktovListModel',
    'stateful': true,
    'title': 'Реестр Реестр объектов',
    requires: [
        'B4.model.ReestrObEktovListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-ReestrObEktovEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-ReestrObEktovEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'objectnamber12',
                'dataIndexAbsoluteUid': 'FieldPath://69f498ef-df00-409a-a699-c742d843e803$290a41e8-833d-446f-a7f1-aa9dc04f99cb',
                'dataIndexRelativePath': 'Element1507784366035',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'cac6456b-ef00-4e4d-8ad0-892ac1966491',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер по порядку',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'objectname12',
                'dataIndexAbsoluteUid': 'FieldPath://69f498ef-df00-409a-a699-c742d843e803$ee0925dd-63d7-4d98-a2a5-150dd0a9ca04$65de9155-a350-4206-be94-ec95bbf9ef90',
                'dataIndexRelativePath': 'Element1507784253669.Element1507203167462',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5d94bbb3-a0b4-43cd-852d-f67452ed1759',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование субъектов надзора.Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'objectinn12',
                'dataIndexAbsoluteUid': 'FieldPath://69f498ef-df00-409a-a699-c742d843e803$aff3cb72-823c-46a9-81b1-13115aaa6464',
                'dataIndexRelativePath': 'Element1507784343465',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'fc085bba-d168-441d-be1c-0ee5bdff63a3',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'ИНН',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Id789',
                'dataIndexAbsoluteUid': 'FieldPath://69f498ef-df00-409a-a699-c742d843e803$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'dataIndexRelativePath': 'Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'af78306a-2d8a-4637-ac35-ca30778a463b',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'NumberTest24adcf55df184f4bbfc8952c7687d0ad',
                'flex': 1,
                'menuDisabled': true,
                'multisortable': false,
                'rmsUid': null,
                'sortable': false,
                'text': 'NumberTest',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});