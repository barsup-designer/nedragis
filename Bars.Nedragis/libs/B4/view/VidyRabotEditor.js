Ext.define('B4.view.VidyRabotEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-vidyraboteditor',
    title: 'Форма редактирования Виды работ',
    rmsUid: 'c07ed5a6-fef5-4f86-af50-b532ed48b849',
    requires: [
        'B4.view.PodvidyList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'VidyRabot_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Id',
        'modelProperty': 'VidyRabot_Id',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1474020299386',
        'modelProperty': 'VidyRabot_Element1474020299386',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'c07ed5a6-fef5-4f86-af50-b532ed48b849-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'VidyRabotEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': false,
            'allowDecimals': false,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Идентификатор',
            'minValue': 0,
            'modelProperty': 'VidyRabot_Id',
            'name': 'Id',
            'rmsUid': 'e1073507-fa4f-4a7a-ae07-a5a91a7bb6a1',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'modelProperty': 'VidyRabot_Element1474020299386',
            'name': 'Element1474020299386',
            'rmsUid': '2db1b8ca-fb0a-41f9-80ef-e0985f13b82c',
            'xtype': 'textfield'
        }, {
            '$EntityFilter': {
                'LinkFilter': {
                    "Group": 3,
                    "Operand": 0,
                    "DataIndex": null,
                    "DataIndexType": null,
                    "Value": null,
                    "Filters": [{
                        "Group": 0,
                        "Operand": 0,
                        "DataIndex": "FieldPath://71b344e1-9560-43aa-90d5-b5ea06bf434e$41be5a35-d47a-4fdf-8ada-af29c956e8c7$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id",
                        "DataIndexType": null,
                        "Value": "@VidyRabot_Id",
                        "Filters": null
                    }]
                }
            },
            'anchor': '100%',
            'closable': false,
            'header': false,
            'name': 'PodvidyList',
            'rmsUid': '21ebcc49-b727-4217-b02d-05902bf33cb4',
            'title': null,
            'xtype': 'rms-podvidylist'
        }, {
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Дата создания',
            'modelProperty': 'e50130aa-c6c6-4261-a580-4a0bc67982db',
            'readOnly': true,
            'rmsUid': 'e50130aa-c6c6-4261-a580-4a0bc67982db',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'VidyRabot_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
        me.grid_PodvidyList = me.down('rms-podvidylist[name=PodvidyList]');
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1474020299386'));
        } else {}
        return res;
    },
});