Ext.define('B4.view.ZemelNyemUchastkiEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-zemelnyemuchastkieditor',
    title: 'Форма редактирования Земельные участки',
    rmsUid: '51a834c0-5ae3-45e8-8135-c5afa098aa20',
    requires: [
        'B4.form.PickerField',
        'B4.model.JuridicheskieLicaListModel',
        'B4.model.KategoriiZemelEditorModel',
        'B4.model.KategoriiZemelListModel',
        'B4.model.ReestryEditorModel',
        'B4.model.ReestryListModel',
        'B4.model.VidPravaNaZemljuEditorModel',
        'B4.model.VidPravaNaZemljuListModel',
        'B4.model.VRIZUEditorModel',
        'B4.model.VRIZUListModel',
        'B4.view.DokumentList',
        'B4.view.JuridicheskieLicaList',
        'B4.view.KategoriiZemelList',
        'B4.view.ReestryList',
        'B4.view.VidPravaNaZemljuList',
        'B4.view.VRIZUList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'ZemelNyemUchastki_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'ZemelNyemUchastki_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'type',
        'modelProperty': 'ZemelNyemUchastki_type',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'vid_prava',
        'modelProperty': 'ZemelNyemUchastki_vid_prava',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'category',
        'modelProperty': 'ZemelNyemUchastki_category',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'vid_use',
        'modelProperty': 'ZemelNyemUchastki_vid_use',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'owner_id',
        'modelProperty': 'ZemelNyemUchastki_owner_id',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'holder_id',
        'modelProperty': 'ZemelNyemUchastki_holder_id',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'area',
        'modelProperty': 'ZemelNyemUchastki_area',
        'type': 'NumberField'
    }, {
        'dataIndex': 'naznach',
        'modelProperty': 'ZemelNyemUchastki_naznach',
        'type': 'TextField'
    }, {
        'dataIndex': 'MapID',
        'modelProperty': 'ZemelNyemUchastki_MapID',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '51a834c0-5ae3-45e8-8135-c5afa098aa20-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ZemelNyemUchastkiEditor-container',
        'layout': {
            'type': 'anchor'
        },
        'region': 'Center',
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 2,
                'items': [{
                    'anchor': '100%',
                    'fieldLabel': 'Просмотр на карте',
                    'margin': '9 0 0 0',
                    'rmsUid': '0f076d6b-3308-4aa5-9157-b6287c620407',
                    'text': 'Просмотр на карте',
                    'xtype': 'button'
                }],
                'layout': 'anchor',
                'rmsUid': 'd0f5db2a-4bda-40bb-93d7-25e374b1868b',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'b3e8883e-9c5a-4fc8-b513-11c642529ac1',
            'xtype': 'container'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'margin': '5 5 5 5',
            'modelProperty': 'ZemelNyemUchastki_name',
            'name': 'name',
            'rmsUid': '01cca3c6-dcc2-466d-99d6-421c77f816a9',
            'xtype': 'textfield'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Панель закладок',
            'items': [{
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Основные сведения',
                'height': 300,
                'items': [{
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Контейнер',
                    'flex': 2,
                    'margin': '5 5 5 5',
                    'rmsUid': '822145c5-22ae-4142-a761-0afe63aebf2f',
                    'title': 'Контейнер',
                    'xtype': 'container'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Контейнер',
                    'flex': 1,
                    'items': [{
                        'allowBlank': true,
                        'editable': false,
                        'fieldLabel': 'Тип объекта права',
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'listView': 'B4.view.ReestryList',
                        'listViewCtl': 'B4.controller.ReestryList',
                        'maximizable': true,
                        'model': 'B4.model.ReestryListModel',
                        'modelProperty': 'ZemelNyemUchastki_type',
                        'name': 'type',
                        'rmsUid': '03039803-cb89-4535-83db-49527b8a175a',
                        'textProperty': 'code',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Тип объекта права',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }, {
                        'allowBlank': true,
                        'editable': false,
                        'fieldLabel': 'Вид права на землю',
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'listView': 'B4.view.VidPravaNaZemljuList',
                        'listViewCtl': 'B4.controller.VidPravaNaZemljuList',
                        'margin': '0 0 0 10',
                        'maximizable': true,
                        'model': 'B4.model.VidPravaNaZemljuListModel',
                        'modelProperty': 'ZemelNyemUchastki_vid_prava',
                        'name': 'vid_prava',
                        'rmsUid': '1ba4ca11-8c8b-4bcd-856b-f82f28cb5067',
                        'textProperty': 'code',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Вид права на землю',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }, {
                        'allowBlank': true,
                        'editable': false,
                        'fieldLabel': 'Категория земель',
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'listView': 'B4.view.KategoriiZemelList',
                        'listViewCtl': 'B4.controller.KategoriiZemelList',
                        'maximizable': true,
                        'model': 'B4.model.KategoriiZemelListModel',
                        'modelProperty': 'ZemelNyemUchastki_category',
                        'name': 'category',
                        'rmsUid': '46a2b822-c72a-4273-adde-17c98f842374',
                        'textProperty': 'code',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Категория земель',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }, {
                        'allowBlank': true,
                        'editable': false,
                        'fieldLabel': 'Вид разрешенного использования',
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'listView': 'B4.view.VRIZUList',
                        'listViewCtl': 'B4.controller.VRIZUList',
                        'margin': '0 0 0 10',
                        'maximizable': true,
                        'model': 'B4.model.VRIZUListModel',
                        'modelProperty': 'ZemelNyemUchastki_vid_use',
                        'name': 'vid_use',
                        'rmsUid': '5c1e37b5-7e89-46e3-9be7-c7bf95477646',
                        'textProperty': 'code',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Вид разрешенного использования',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }, {
                        'allowBlank': true,
                        'editable': false,
                        'fieldLabel': 'Собственник',
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'listView': 'B4.view.JuridicheskieLicaList',
                        'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                        'maximizable': true,
                        'model': 'B4.model.JuridicheskieLicaListModel',
                        'modelProperty': 'ZemelNyemUchastki_owner_id',
                        'name': 'owner_id',
                        'rmsUid': 'e2ac23e4-fcc5-4cc3-bcf5-79c3aa11302c',
                        'textProperty': 'Representation',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Собственник',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }, {
                        'allowBlank': true,
                        'editable': false,
                        'fieldLabel': 'Правообладатель',
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'listView': 'B4.view.JuridicheskieLicaList',
                        'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                        'margin': '0 0 0 10',
                        'maximizable': true,
                        'model': 'B4.model.JuridicheskieLicaListModel',
                        'modelProperty': 'ZemelNyemUchastki_holder_id',
                        'name': 'holder_id',
                        'rmsUid': '6eb1e1bb-5806-4a8b-a658-e91b45f5031d',
                        'textProperty': 'Representation',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Правообладатель',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }, {
                        'allowBlank': true,
                        'decimalPrecision': 2,
                        'fieldLabel': 'Общая площадь, кв. м',
                        'minValue': 0,
                        'modelProperty': 'ZemelNyemUchastki_area',
                        'name': 'area',
                        'rmsUid': '61c717a3-6a98-47db-8d67-189f094470ea',
                        'step': 1,
                        'width': 200,
                        'xtype': 'numberfield'
                    }],
                    'layout': {
                        'type': 'column'
                    },
                    'margin': '5 5 5 5',
                    'rmsUid': 'b989f7ec-6658-4802-b640-ec421eae9707',
                    'title': 'Контейнер',
                    'xtype': 'container'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Контейнер',
                    'items': [{
                        'allowBlank': true,
                        'fieldLabel': 'Назначение',
                        'modelProperty': 'ZemelNyemUchastki_naznach',
                        'name': 'naznach',
                        'rmsUid': '3ef4790b-f6ac-4b7f-8591-7618882626da',
                        'xtype': 'textfield'
                    }],
                    'layout': {
                        'type': 'fit'
                    },
                    'margin': '15 5 5 5',
                    'rmsUid': 'b133a273-3c71-4861-a318-1ebfe2b5ddc3',
                    'title': 'Контейнер',
                    'xtype': 'container'
                }, {
                    'allowBlank': true,
                    'allowDecimals': false,
                    'anchor': '100%',
                    'decimalPrecision': 2,
                    'fieldLabel': 'Идентификатор ГИС',
                    'minValue': 0,
                    'modelProperty': 'ZemelNyemUchastki_MapID',
                    'name': 'MapID',
                    'rmsUid': '79eff5c5-6995-409a-b9bf-f4a2f879bb9a',
                    'step': 1,
                    'xtype': 'numberfield'
                }],
                'rmsUid': '98f58828-5134-4ffe-9a6b-6848e3d3009b',
                'title': 'Основные сведения',
                'xtype': 'container'
            }, {
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Документы',
                'items': [{
                    'anchor': '100%',
                    'closable': false,
                    'header': false,
                    'height': 300,
                    'name': 'DokumentList',
                    'rmsUid': '89384547-7102-4a1b-a951-1d05a6bc87bf',
                    'title': null,
                    'xtype': 'rms-dokumentlist'
                }],
                'rmsUid': '1801d8f1-47aa-41d6-b816-ded7a4186fa2',
                'title': 'Документы',
                'xtype': 'container'
            }, {
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Карта',
                'height': 500,
                'rmsUid': '681c12cb-36e5-4ac2-9149-aa591d59fdd8',
                'title': 'Карта',
                'xtype': 'container'
            }],
            'rmsUid': '0ed4decc-f765-4e9e-b5af-cc4c2c2732e9',
            'xtype': 'tabpanel'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'ZemelNyemUchastki_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
        me.grid_DokumentList = me.down('rms-dokumentlist[name=DokumentList]');
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});