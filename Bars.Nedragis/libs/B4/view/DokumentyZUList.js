Ext.define('B4.view.DokumentyZUList', {
    'alias': 'widget.rms-dokumentyzulist',
    'dataSourceUid': '4996ce7a-3313-46fc-979a-fe45071d30e9',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.DokumentyZUListModel',
    'stateful': true,
    'title': 'Реестр ЗУ (ДокументыЗУ)',
    requires: [
        'B4.model.DokumentyZUListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-DokumentyZUEditor3-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-DokumentyZUEditor3-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'zemuch_cadnum1',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$bac8dff6-5e22-4e24-8a69-c82f1802f8e9$12badbbd-e1ea-40e7-b969-532cea517fcc',
                'dataIndexRelativePath': 'zemuch.cadnum1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '561443fe-c5c5-4cf4-8d63-f9389223052c',
                'sortable': true,
                'text': 'Кадастровый номер',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'zemuch_name',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$bac8dff6-5e22-4e24-8a69-c82f1802f8e9$105ff081-e494-4ec2-be01-d5b976574ee2',
                'dataIndexRelativePath': 'zemuch.name',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '4d29f3ec-c6cb-44eb-8c8e-3a1b00439e3e',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'zemuch_owner_id_Representation',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$bac8dff6-5e22-4e24-8a69-c82f1802f8e9$a47f4db2-bfc6-454c-bfc9-f30970ac0faf$6f8b2489-4290-4747-93ba-ff199586d3bb',
                'dataIndexRelativePath': 'zemuch.owner_id.Representation',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'c208d106-12c4-418e-b88a-6efbc4b6d4ab',
                'sortable': true,
                'text': 'Собственник',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'doc1_Id',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$b0627770-34ec-4c22-8112-78c21088ef09$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'dataIndexRelativePath': 'doc1.Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd96883f9-211b-48ee-8749-2d052764e550',
                'sortable': true,
                'text': 'Документ.Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'zemuch_vid_prava_name1',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$bac8dff6-5e22-4e24-8a69-c82f1802f8e9$e70e27a6-b885-42f3-94ba-cda4ef7e3427$ea2c10dd-7114-412e-9b4a-7c4936c0c672',
                'dataIndexRelativePath': 'zemuch.vid_prava.name1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '9ce92f87-cd8e-4858-9cd1-aa44f736d3ff',
                'sortable': true,
                'text': 'Вид права на землю',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'zemuch_category_name1',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$bac8dff6-5e22-4e24-8a69-c82f1802f8e9$00e67e7c-cee8-4669-b13e-131cc1fb153c$1cda49c6-b9e1-4437-873b-7c67d9cbe02f',
                'dataIndexRelativePath': 'zemuch.category.name1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'ef1b20d1-07bf-409e-bc34-70170e8fed86',
                'sortable': true,
                'text': 'Категория земли',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'zemuch_vid_use_name1',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$bac8dff6-5e22-4e24-8a69-c82f1802f8e9$dbf89b5c-94fe-42a3-8300-6ca22f8bfb16$868835cf-0e3b-47aa-be83-eb3e66045386',
                'dataIndexRelativePath': 'zemuch.vid_use.name1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5cb0686c-d64a-4ae6-88df-f91df26f0696',
                'sortable': true,
                'text': 'Вид разрешенного использования',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});