Ext.define('B4.view.FajjlyEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-fajjlyeditor',
    title: 'Форма редактирования Файлы',
    rmsUid: '92e0e82b-e208-4902-9fce-8050b8898edb',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Fajjly_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'ObjectCreateDate',
        'modelProperty': 'Fajjly_ObjectCreateDate',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1457586389312',
        'modelProperty': 'Fajjly_Element1457586389312',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '92e0e82b-e208-4902-9fce-8050b8898edb-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'FajjlyEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': false,
                    'anchor': '100%',
                    'fieldLabel': 'Дата создания',
                    'format': 'd.m.Y',
                    'modelProperty': 'Fajjly_ObjectCreateDate',
                    'name': 'ObjectCreateDate',
                    'rmsUid': 'b74cbba2-b1bb-40db-9c10-84fc9e05c199',
                    'xtype': 'datefield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'файл',
                    'modelProperty': 'Fajjly_Element1457586389312',
                    'name': 'Element1457586389312',
                    'rmsUid': '7f827558-aab1-4e7a-a5bd-a0878801dc5b',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': '0084a2b4-c645-4eae-b708-745cd63a3bda',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'ea2f442f-79c1-48fd-9b3a-89651a7ec7ea',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Fajjly_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});