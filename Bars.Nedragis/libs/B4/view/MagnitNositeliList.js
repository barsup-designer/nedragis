Ext.define('B4.view.MagnitNositeliList', {
    'alias': 'widget.rms-magnitnositelilist',
    'dataSourceUid': '3e627ca5-3cf8-4bec-8d07-a5998d6b28df',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.MagnitNositeliListModel',
    'stateful': true,
    'title': 'Справочник Магнитные носители',
    requires: [
        'B4.model.MagnitNositeliListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-MagnitNositeliEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-MagnitNositeliEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://3e627ca5-3cf8-4bec-8d07-a5998d6b28df$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd5dcb79f-b8ab-49de-a7ae-0e08520d31df',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://3e627ca5-3cf8-4bec-8d07-a5998d6b28df$5a871f71-c1f9-498c-b5d1-0be78bebbcf1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '36328c7d-7f2f-4648-b68f-8a425baf78d9',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'barcod',
                'dataIndexAbsoluteUid': 'FieldPath://3e627ca5-3cf8-4bec-8d07-a5998d6b28df$a8bd2154-cf74-4434-ab5b-048fb3710e7c',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '3c84ef85-8fbd-42ec-863e-65449169071d',
                'sortable': true,
                'text': 'Бар-код',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});