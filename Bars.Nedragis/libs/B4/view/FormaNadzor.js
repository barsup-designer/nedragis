Ext.define('B4.view.FormaNadzor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-formanadzor',
    title: 'Форма редактирования Надзор',
    rmsUid: 'aad7f4a7-f6f4-4551-b733-1fcb9f2ec7a3',
    requires: [
        'B4.form.PickerField',
        'B4.model.FormaRedaktirovanijaObEktovModel',
        'B4.model.ReestrNaimenovanieObEktovNadzoraModel',
        'B4.view.ReestrNaimenovanieObEktovNadzora'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Nadzor_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1507619429093',
        'modelProperty': 'Nadzor_Element1507619429093',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1507271571833',
        'modelProperty': 'Nadzor_Element1507271571833',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Element1507611045539',
        'modelProperty': 'Nadzor_Element1507611045539',
        'type': 'TextField'
    }, {
        'dataIndex': 'inn',
        'modelProperty': 'Nadzor_inn',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1507010532348',
        'modelProperty': 'Nadzor_Element1507010532348',
        'type': 'TextField'
    }, {
        'dataIndex': 'date',
        'modelProperty': 'Nadzor_date',
        'type': 'DateField'
    }, {
        'dataIndex': 'srok_end',
        'modelProperty': 'Nadzor_srok_end',
        'type': 'DateField'
    }, {
        'dataIndex': 'mesto',
        'modelProperty': 'Nadzor_mesto',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507703919662',
        'modelProperty': 'Nadzor_Element1507703919662',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507262033494',
        'modelProperty': 'Nadzor_Element1507262033494',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1507262072161',
        'modelProperty': 'Nadzor_Element1507262072161',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1507262105889',
        'modelProperty': 'Nadzor_Element1507262105889',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507262162260',
        'modelProperty': 'Nadzor_Element1507262162260',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507262266009',
        'modelProperty': 'Nadzor_Element1507262266009',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507262301335',
        'modelProperty': 'Nadzor_Element1507262301335',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507262339156',
        'modelProperty': 'Nadzor_Element1507262339156',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507262414231',
        'modelProperty': 'Nadzor_Element1507262414231',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507262455649',
        'modelProperty': 'Nadzor_Element1507262455649',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507262379380',
        'modelProperty': 'Nadzor_Element1507262379380',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'aad7f4a7-f6f4-4551-b733-1fcb9f2ec7a3-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'FormaNadzor-container',
        'autoScroll': true,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Номер п/п',
            'margin': '10 15 0 15',
            'minValue': 0,
            'modelProperty': 'Nadzor_Element1507619429093',
            'name': 'Element1507619429093',
            'rmsUid': '18ecbcd9-eb50-4402-99aa-6f7ab18dc74a',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Наименование субъекта надзора',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.ReestrNaimenovanieObEktovNadzora',
            'listViewCtl': 'B4.controller.ReestrNaimenovanieObEktovNadzora',
            'margin': '0 15 0 15',
            'maximizable': true,
            'model': 'B4.model.ReestrNaimenovanieObEktovNadzoraModel',
            'modelProperty': 'Nadzor_Element1507271571833',
            'name': 'Element1507271571833',
            'rmsUid': '4ccdc9b2-4cf2-4b4a-ba22-d4bacb9275b2',
            'textProperty': 'Element1507203167462',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Наименование субъекта надзора',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код свидетельства',
            'margin': '0 15 0 15',
            'modelProperty': 'Nadzor_Element1507611045539',
            'name': 'Element1507611045539',
            'rmsUid': 'd27a9356-6d24-4d36-9baa-b53dfede45b5',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'ИНН',
            'margin': '0 15 0 15',
            'minValue': 0,
            'modelProperty': 'Nadzor_inn',
            'name': 'inn',
            'rmsUid': '5a3dfdfa-b02c-43ee-b1ac-d31c397c02ba',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'dockedItems': [],
            'fieldLabel': 'Объекты регионального надзора',
            'height': 900,
            'items': [{
                'anchor': '100%',
                'dockedItems': [],
                'items': [{
                    'columnWidth': '0.90',
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'dockedItems': [],
                        'fieldLabel': 'Региональный государственный надзор за геологическим изучением, рациональным использованием и охраной недр в отношении участков недр местного значения.',
                        'items': [{
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Номер Лицензии',
                            'labelAlign': 'left',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_Element1507010532348',
                            'name': 'Element1507010532348',
                            'rmsUid': '3768cab9-df03-47ab-b0b5-e54043749f48',
                            'xtype': 'textfield'
                        }, {
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Дата регистации',
                            'format': 'd.m.Y',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_date',
                            'name': 'date',
                            'rmsUid': '74f99050-3f36-4c8b-a29d-428dd519f0b1',
                            'xtype': 'datefield'
                        }, {
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Окончание срока действия',
                            'format': 'd.m.Y',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_srok_end',
                            'name': 'srok_end',
                            'rmsUid': 'dd296446-ee41-4714-a88b-8193bee998de',
                            'xtype': 'datefield'
                        }, {
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Местонахождение',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_mesto',
                            'name': 'mesto',
                            'rmsUid': '08b32b21-d6a6-4c2c-83f6-0dce5bf37d47',
                            'xtype': 'textfield'
                        }],
                        'rmsUid': 'fa118a9a-a39a-4415-900d-a69f0d42eedd',
                        'title': 'Региональный государственный надзор за геологическим изучением, рациональным использованием и охраной недр в отношении участков недр местного значения.',
                        'width': 580,
                        'xtype': 'fieldset'
                    }, {
                        'dockedItems': [],
                        'fieldLabel': 'Региональный государственный надзор в области использования и охраны водных объектов, за исключением водных объектов, подлежащих федеральному государственному надзору, а так же за соблюдением особых условий водопользования и использования участков береговой полосы (в том числе участков примыкания к гидроэнергетическим объектам) в границах охранных зон гидроэнергетических объектов, расположенных на водных объектах, подлежащих региональному государственному надзору за их использованием и охраной',
                        'items': [{
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Договор водопользования',
                            'modelProperty': 'Nadzor_Element1507703919662',
                            'name': 'Element1507703919662',
                            'rmsUid': '0b9988ea-2187-4186-b0af-75592f39aadc',
                            'xtype': 'textfield'
                        }, {
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Дата регистрации договора',
                            'format': 'd.m.Y',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_Element1507262033494',
                            'name': 'Element1507262033494',
                            'rmsUid': '70c56e25-c4b1-466c-8be4-e6f2f9cdf31c',
                            'xtype': 'datefield'
                        }, {
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Окончание срока действия договора',
                            'format': 'd.m.Y',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_Element1507262072161',
                            'name': 'Element1507262072161',
                            'rmsUid': 'e3396cb2-bca6-4d42-9563-71947f16b74c',
                            'xtype': 'datefield'
                        }, {
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Местоположение',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_Element1507262105889',
                            'name': 'Element1507262105889',
                            'rmsUid': 'e31cb04c-6b63-4e91-bd5f-825014e10788',
                            'xtype': 'textfield'
                        }],
                        'rmsUid': '637ad455-610b-4145-97cf-98c0708321a9',
                        'title': 'Региональный государственный надзор в области использования и охраны водных объектов, за исключением водных объектов, подлежащих федеральному государственному надзору, а так же за соблюдением особых условий водопользования и использования участков береговой полосы (в том числе участков примыкания к гидроэнергетическим объектам) в границах охранных зон гидроэнергетических объектов, расположенных на водных объектах, подлежащих региональному государственному надзору за их использованием и охраной',
                        'width': 580,
                        'xtype': 'fieldset'
                    }, {
                        'dockedItems': [],
                        'fieldLabel': 'Государственный надзор в области охраны атмосферного воздуха на объектах хозяйственной и иной деятельности, подлежащих региональному государственному экологическому надзору',
                        'items': [{
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Наименование стационарного источника загрязнения',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_Element1507262162260',
                            'name': 'Element1507262162260',
                            'rmsUid': 'f6b15c5c-a3ef-4d8b-baf3-697993883611',
                            'xtype': 'textfield'
                        }, {
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Местонахождение',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_Element1507262266009',
                            'name': 'Element1507262266009',
                            'rmsUid': '3316e8da-b65d-4fa5-bde7-f3f10c186d70',
                            'xtype': 'textfield'
                        }, {
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Наличие тома ПДВ',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_Element1507262301335',
                            'name': 'Element1507262301335',
                            'rmsUid': '4ce5111e-eaa2-40ac-a796-f61a953b110f',
                            'xtype': 'textfield'
                        }, {
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Разрешение на выброс в атмосферу',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_Element1507262339156',
                            'name': 'Element1507262339156',
                            'rmsUid': 'eebc0a22-deb5-4577-9bf9-d89ce5e94fe1',
                            'xtype': 'textfield'
                        }],
                        'rmsUid': 'b2426158-0806-4254-a610-06c133b422ee',
                        'title': 'Государственный надзор в области охраны атмосферного воздуха на объектах хозяйственной и иной деятельности, подлежащих региональному государственному экологическому надзору',
                        'width': 580,
                        'xtype': 'fieldset'
                    }, {
                        'dockedItems': [],
                        'fieldLabel': 'Государственный надзор в области обращения с отходами на объектах хозяйственной и иной деятельности, подлежащих региональному государственному экологическому надзору',
                        'items': [{
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Местонахождение',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_Element1507262414231',
                            'name': 'Element1507262414231',
                            'rmsUid': '0d71b970-884d-4ff6-85be-a385521e3684',
                            'xtype': 'textfield'
                        }, {
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Проект нормативов образования отходов и лимитов на их размещение',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_Element1507262455649',
                            'name': 'Element1507262455649',
                            'rmsUid': '0fd09144-47c6-4995-9cfc-7e7298487558',
                            'xtype': 'textfield'
                        }, {
                            'allowBlank': true,
                            'anchor': '100%',
                            'fieldLabel': 'Наименование объекта образования отходов',
                            'labelWidth': 150,
                            'modelProperty': 'Nadzor_Element1507262379380',
                            'name': 'Element1507262379380',
                            'rmsUid': '7f90303d-a0cf-4fbe-aa0a-944bac64b849',
                            'xtype': 'textfield'
                        }],
                        'rmsUid': '7d9a1103-c3c3-49c7-8ebf-cb2e9a78a2f1',
                        'title': 'Государственный надзор в области обращения с отходами на объектах хозяйственной и иной деятельности, подлежащих региональному государственному экологическому надзору',
                        'width': 580,
                        'xtype': 'fieldset'
                    }],
                    'layout': 'anchor',
                    'rmsUid': '91403b69-1366-4ff7-96c5-ded13d72242e',
                    'xtype': 'container'
                }],
                'layout': 'hbox',
                'margin': '10 0 0 10',
                'rmsUid': 'b0ed188c-679b-4e16-a78b-0c136bc3bc86',
                'xtype': 'container'
            }],
            'rmsUid': '7402382e-cb33-4478-bd4b-4881aee656a5',
            'title': 'Объекты регионального надзора',
            'width': 590,
            'xtype': 'fieldset'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Nadzor_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});