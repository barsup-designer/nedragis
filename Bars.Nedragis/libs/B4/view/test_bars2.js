Ext.define('B4.view.test_bars2', {
    'alias': 'widget.rms-test_bars2',
    'dataSourceUid': '7dd5ad97-a603-4dbf-acec-30db779b157d',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': false,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.test_bars2Model',
    'stateful': true,
    'title': 'test_bars2',
    requires: [
        'B4.model.test_bars2Model',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Element1481264972872',
                'dataIndexAbsoluteUid': 'FieldPath://7dd5ad97-a603-4dbf-acec-30db779b157d$b30575d0-57a6-4476-92cd-0f6957fb60f2',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'renderer': 'Ext.util.Format.usMoney',
                'rmsUid': '02b161a0-c76a-45eb-adb4-eceb54baf604',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0:N6}', Ext.util.Format.number(value, '0,000.00'));
                },
                'summaryType': 'none',
                'text': 'площадь',
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'Element1496214131857',
                'dataIndexAbsoluteUid': 'FieldPath://7dd5ad97-a603-4dbf-acec-30db779b157d$039ca39a-60f6-4d3a-af26-60734e9d150b',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '023ca7a2-7c0e-4260-a541-51b83e948f1d',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Test',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }]
        });
        me.callParent(arguments);
    }
});