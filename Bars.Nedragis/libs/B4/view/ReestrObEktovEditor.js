Ext.define('B4.view.ReestrObEktovEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-reestrobektoveditor',
    title: 'Форма редактирования Реестр объектов',
    rmsUid: 'd367ecdd-9904-4fb0-a747-252b4ad39a4b',
    requires: [
        'B4.form.PickerField',
        'B4.model.FormaRedaktirovanijaObEktovModel',
        'B4.model.ReestrNaimenovanieObEktovNadzoraModel',
        'B4.view.ObEktopiList2',
        'B4.view.ReestrNaimenovanieObEktovNadzora',
        'B4.view.RegionalNyjjNadzorVodnykhObEktovList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'ReestrObEktov_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1507784366035',
        'modelProperty': 'ReestrObEktov_Element1507784366035',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1507784253669',
        'modelProperty': 'ReestrObEktov_Element1507784253669',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Element1507784343465',
        'modelProperty': 'ReestrObEktov_Element1507784343465',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'd367ecdd-9904-4fb0-a747-252b4ad39a4b-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ReestrObEktovEditor-container',
        'autoScroll': true,
        'layout': {
            'align': 'stretch',
            'type': 'vbox'
        },
        items: [{
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'allowDecimals': false,
                    'anchor': '100%',
                    'decimalPrecision': 0,
                    'fieldLabel': 'Номер по порядку',
                    'hideTrigger': true,
                    'labelWidth': 200,
                    'maxValue': 5000000,
                    'minValue': 1,
                    'modelProperty': 'ReestrObEktov_Element1507784366035',
                    'name': 'Element1507784366035',
                    'rmsUid': '17fcb539-7acf-4f48-8cfb-12b62076e1cf',
                    'step': 1,
                    'xtype': 'numberfield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Наименование субъектов надзора',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 200,
                    'listView': 'B4.view.ReestrNaimenovanieObEktovNadzora',
                    'listViewCtl': 'B4.controller.ReestrNaimenovanieObEktovNadzora',
                    'maximizable': true,
                    'model': 'B4.model.ReestrNaimenovanieObEktovNadzoraModel',
                    'modelProperty': 'ReestrObEktov_Element1507784253669',
                    'name': 'Element1507784253669',
                    'rmsUid': '1cc7b218-b70e-4c21-b94f-658d4e6cd515',
                    'textProperty': 'Element1507203167462',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Наименование субъектов надзора',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }, {
                    'allowBlank': true,
                    'allowDecimals': false,
                    'anchor': '100%',
                    'decimalPrecision': 2,
                    'fieldLabel': 'ИНН',
                    'hideTrigger': true,
                    'labelWidth': 200,
                    'minValue': 1,
                    'modelProperty': 'ReestrObEktov_Element1507784343465',
                    'name': 'Element1507784343465',
                    'rmsUid': '158bc50f-a333-43fd-91df-572dd65090e8',
                    'step': 1,
                    'xtype': 'numberfield'
                }],
                'layout': 'anchor',
                'rmsUid': '23ddcbdf-a663-4729-98a3-8c5c42ecfbfd',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '0eede132-6bc9-4a25-9a3a-9ea8ff5e0655',
            'xtype': 'container'
        }, {
            'autoScroll': true,
            'columnWidth': 1,
            'dockedItems': [],
            'fieldLabel': 'Панель закладок',
            'flex': 1,
            'items': [{
                'columnWidth': '0.15',
                'dockedItems': [],
                'fieldLabel': 'Региональный надзор за геологическим изучением',
                'items': [{
                    '$EntityFilter': {
                        'LinkFilter': {
                            "Group": 3,
                            "Operand": 0,
                            "DataIndex": null,
                            "DataIndexType": null,
                            "Value": null,
                            "Filters": [{
                                "Group": 0,
                                "Operand": 0,
                                "DataIndex": "FieldPath://2fd9e43e-bd97-4446-a65e-77f22758149b$77e911b2-1933-408c-ace6-faa249142519$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id",
                                "DataIndexType": null,
                                "Value": "@ReestrObEktov_Id",
                                "Filters": null
                            }]
                        }
                    },
                    'closable': false,
                    'margin': '5 5 5 5',
                    'name': 'ObEktopiList2',
                    'rmsUid': '8acd84b3-f1da-40ab-9a4c-5eeccb4b6d09',
                    'title': 'Реестр объектопи2',
                    'xtype': 'rms-obektopilist2'
                }],
                'layout': {
                    'type': 'fit'
                },
                'rmsUid': 'c6230693-f754-4330-8c87-765ed24bb48c',
                'title': 'Региональный надзор за геологическим изучением',
                'xtype': 'container'
            }, {
                'bodyPadding': '5 5 5 5',
                'columnWidth': '0.15',
                'dockedItems': [],
                'fieldLabel': 'Региональный надзор в области охраны водных объектов',
                'items': [{
                    '$EntityFilter': {
                        'LinkFilter': {
                            "Group": 3,
                            "Operand": 0,
                            "DataIndex": null,
                            "DataIndexType": null,
                            "Value": null,
                            "Filters": [{
                                "Group": 0,
                                "Operand": 0,
                                "DataIndex": "FieldPath://8d4cd756-d656-4caa-805b-242b484000f7$a3873da8-01f3-4c19-b876-ed44d8041cd5$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id",
                                "DataIndexType": null,
                                "Value": "@ReestrObEktov_Id",
                                "Filters": null
                            }]
                        }
                    },
                    'closable': false,
                    'margin': '5 5 5 5',
                    'name': 'RegionalNyjjNadzorVodnykhObEktovList',
                    'rmsUid': 'a09a52fb-6bb6-45bb-8a2c-c1d392e9d82b',
                    'title': 'Реестр Региональный надзор водных объектов',
                    'xtype': 'rms-regionalnyjjnadzorvodnykhobektovlist'
                }],
                'layout': {
                    'type': 'fit'
                },
                'rmsUid': '18fa1384-90ae-474c-beb9-e899f2c3f442',
                'title': 'Региональный надзор в области охраны водных объектов',
                'xtype': 'container'
            }, {
                'columnWidth': '0.15',
                'dockedItems': [],
                'fieldLabel': 'Государственный надзор в области охраны атмосферного воздуха',
                'rmsUid': '8b059be2-9c23-45f8-a41a-71cf6526c937',
                'title': 'Государственный надзор в области охраны атмосферного воздуха',
                'xtype': 'container'
            }, {
                'columnWidth': '0.15',
                'dockedItems': [],
                'fieldLabel': 'Государственный надзор в области обращения с отходами',
                'rmsUid': 'a59caa48-8cb1-44e7-a04a-1ed2e3010ac1',
                'title': 'Государственный надзор в области обращения с отходами',
                'xtype': 'container'
            }],
            'layout': {
                'type': 'fit'
            },
            'margin': '50 0 0 0',
            'rmsUid': 'dadc225e-d33a-4b7c-bf7f-89c7c5c71183',
            'xtype': 'tabpanel'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'ReestrObEktov_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
        me.grid_ObEktopiList2 = me.down('rms-obektopilist2[name=ObEktopiList2]');
        me.grid_RegionalNyjjNadzorVodnykhObEktovList = me.down('rms-regionalnyjjnadzorvodnykhobektovlist[name=RegionalNyjjNadzorVodnykhObEktovList]');
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});