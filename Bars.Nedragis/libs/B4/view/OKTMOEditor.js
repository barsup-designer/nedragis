Ext.define('B4.view.OKTMOEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-oktmoeditor',
    title: 'Форма редактирования ОКТМО',
    rmsUid: '08ab56e2-51ac-4690-9998-869c9887e489',
    requires: [
        'B4.form.PickerField',
        'B4.model.OKTMOEditorModel',
        'B4.model.OKTMOListModel',
        'B4.view.OKTMOList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'OKTMO_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'OKTMO_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'OKTMO_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'parent_id',
        'modelProperty': 'OKTMO_parent_id',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '08ab56e2-51ac-4690-9998-869c9887e489-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OKTMOEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'margin': '10 5 5 5',
            'modelProperty': 'OKTMO_code',
            'name': 'code',
            'rmsUid': 'e2d50d3a-d448-4811-ad13-2261d830f3d2',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'margin': '10 5 5 5',
            'modelProperty': 'OKTMO_name',
            'name': 'name',
            'rmsUid': '6e0bea42-11d9-4c49-bf99-bdc6eeae4753',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Родительский элемент',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.OKTMOList',
            'listViewCtl': 'B4.controller.OKTMOList',
            'margin': '5 5 5 5',
            'maximizable': true,
            'model': 'B4.model.OKTMOListModel',
            'modelProperty': 'OKTMO_parent_id',
            'name': 'parent_id',
            'rmsUid': 'b6fe7a48-cc77-448b-9b8e-0f0981802306',
            'textProperty': 'code',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Родительский элемент',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'OKTMO_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});