Ext.define('B4.view.ReestrNaimenovanieObEktovNadzora', {
    'alias': 'widget.rms-reestrnaimenovanieobektovnadzora',
    'dataSourceUid': 'd41afb95-b52e-4013-b714-2fc08404a9f9',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': false,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.ReestrNaimenovanieObEktovNadzoraModel',
    'stateful': true,
    'title': 'Перечень хозяйствующих субъектов надзора  ',
    requires: [
        'B4.model.ReestrNaimenovanieObEktovNadzoraModel',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-delete-png',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-add-png',
        'itemId': 'Addition-FormaRedaktirovanijaObEktov-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-edit-png',
        'itemId': 'Editing-FormaRedaktirovanijaObEktov-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'naimennadzor',
                'dataIndexAbsoluteUid': 'FieldPath://d41afb95-b52e-4013-b714-2fc08404a9f9$65de9155-a350-4206-be94-ec95bbf9ef90',
                'dataIndexRelativePath': 'Element1507203167462',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '4f4465e6-741d-4ea4-876f-9fa57e89ee2a',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }]
        });
        me.callParent(arguments);
    }
});