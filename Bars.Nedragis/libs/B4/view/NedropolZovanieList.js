Ext.define('B4.view.NedropolZovanieList', {
    'alias': 'widget.rms-nedropolzovanielist',
    'dataSourceUid': '319ddc80-7273-4a3b-b12c-91ad4b942e52',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.NedropolZovanieListModel',
    'stateful': true,
    'title': 'Реестр Недропользования',
    requires: [
        'B4.model.NedropolZovanieListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-NedropolZovanieEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-NedropolZovanieEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b23148cd-a81f-4926-9182-410cb7bd8e3f',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name_n_NAME_SP',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$85d731d7-2fd6-4257-83e7-8c14153422ef$c9cfc806-3d3e-4aa7-b12c-d9218426a9d7',
                'dataIndexRelativePath': 'name_n.NAME_SP',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '26a52bdd-fd6e-41b1-af5d-97059e01c186',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование отдела',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'ObjectCreateDate',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$Bars.B4.DataAccess.BaseEntity, Bars.B4.Core/ObjectCreateDate',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'e59b642f-28ed-4a7f-bdbc-5e401197d9f7',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата создания',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'vid_n',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$2f0d9e92-8733-41b0-8aa7-508fc47215c7',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd85b0fea-aa1a-415b-a86b-09489f04d4e8',
                'sortable': true,
                'text': 'Вид работ',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'podvid',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$3275a278-cd6d-4885-acdd-9596c6041639',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd7061f73-2202-4237-a0ed-04eeb6e4582d',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Подвид работ',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'col_n',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$fea2db08-2b52-4d62-a77e-4a32d15fd69b',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '036a30e0-d064-4eb8-82b2-d1dec01b496e',
                'sortable': true,
                'text': 'Количество',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'opis_n',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$12269dd0-f08e-4865-a4e4-9ef1ed483088',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '07122633-b8de-4612-8f6a-e72024d549be',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дополнительное описание',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'obr_n',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$8f552098-4039-4a44-aca7-3c8e5b084a1b',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '1fe8d249-5395-404e-9314-6364721d26fd',
                'sortable': true,
                'text': 'Безвозмездное обращение',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'zakazchik',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$08bf8f5c-3dd0-4382-883a-2fedff98e501',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2046d649-0c81-4748-a1f8-6a4856d12f08',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Заказчик',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'zakaz_n',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$fbdaae74-2f64-471f-a689-3b4103e7c4ba',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '989aa829-7acd-4541-a62c-f53d5777112f',
                'sortable': true,
                'text': 'Дополнительная информация о заказчике',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'organ',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$04ebe600-fec0-47a3-8112-2dfc3ade4fdd',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '52f24412-a8db-4ab4-a941-93261df49098',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Орган госвласти',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'harakter_n',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$1ca35e91-ad00-404b-90a2-cdacaa5f0774',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '35c81fdd-7fe5-4bc8-96c9-d412131f0318',
                'sortable': true,
                'text': 'Характер обращения',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'sotr_n_Element1474352136232',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$882a5873-c777-474a-8969-22014fc971df$5c9e0c3e-7dcb-483d-89fd-a12dacaa7279',
                'dataIndexRelativePath': 'sotr_n.Element1474352136232',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'c9d904fb-c5a3-4380-9a02-1ff063dbe9f6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Исполнитель',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'chas_n',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$1563d613-528a-4e83-88e8-5d5cf712717d',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'bc1be98f-4bd6-462d-ab10-750614921bdf',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Количество затраченных человека часов',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'stoumost',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$7740a407-eb76-438c-8d5a-c317222f00f3',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a6feaa72-3662-4cca-989d-6af10f7c4b88',
                'sortable': true,
                'text': 'Стоимость по калькуляции руб.',
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'oplata_n',
                'dataIndexAbsoluteUid': 'FieldPath://319ddc80-7273-4a3b-b12c-91ad4b942e52$57026230-4bb4-4e28-a890-df4dfa9b7c0a',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'f634ad4b-0c24-452d-a80d-5b740472e452',
                'sortable': true,
                'text': 'Оплачено',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});