Ext.define('B4.view.ZURedaktirovanieNavigacija', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-zuredaktirovanienavigacija',
    title: 'ЗУ редактирование - навигация',
    rmsUid: 'aecb5a82-7da6-4a28-b7ea-0539c124ebf0',
    requires: [
        'B4.view.NavigationPanel'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'ZemelNyemUchastki_Id',
        'type': 'hidden'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'aecb5a82-7da6-4a28-b7ea-0539c124ebf0-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ZURedaktirovanieNavigacija-container',
        'layout': {
            'type': 'fit'
        },
        items: [{
            'border': false,
            'fieldLabel': 'Панель навигации',
            'items': [],
            'rmsUid': '1b976d4c-8d77-4a42-8a2e-6a919a934a8b',
            'rootKey': '1b976d4c-8d77-4a42-8a2e-6a919a934a8b',
            'title': null,
            'treeWidth': 250,
            'xtype': 'rms-navigationpanel'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'ZemelNyemUchastki_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});