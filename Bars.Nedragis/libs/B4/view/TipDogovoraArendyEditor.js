Ext.define('B4.view.TipDogovoraArendyEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-tipdogovoraarendyeditor',
    title: 'Форма редактирования Тип договора аренды',
    rmsUid: '640fab8a-79e9-4ed6-91be-47f36f8209e9',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'TipDogovoraArendy_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'TipDogovoraArendy_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'TipDogovoraArendy_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '640fab8a-79e9-4ed6-91be-47f36f8209e9-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'TipDogovoraArendyEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'TipDogovoraArendy_code',
            'name': 'code',
            'rmsUid': '8bcf0296-0d67-432a-ad36-a4387ae807cf',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'TipDogovoraArendy_name',
            'name': 'name',
            'rmsUid': 'c991a72f-33e1-4968-b61d-7736d8328cd1',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'TipDogovoraArendy_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});