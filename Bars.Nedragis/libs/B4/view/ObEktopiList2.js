Ext.define('B4.view.ObEktopiList2', {
    'alias': 'widget.rms-obektopilist2',
    'dataSourceUid': '2fd9e43e-bd97-4446-a65e-77f22758149b',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.ObEktopiList2Model',
    'stateful': true,
    'title': 'Реестр объектопи2',
    requires: [
        'B4.model.ObEktopiList2Model',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-ObEktopiEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-ObEktopiEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'objectopi17',
                'dataIndexAbsoluteUid': 'FieldPath://2fd9e43e-bd97-4446-a65e-77f22758149b$77e911b2-1933-408c-ace6-faa249142519$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'dataIndexRelativePath': 'Element1508136885094.Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '415f4c7f-49d5-4a7b-8073-8c55f915f41c',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Объект.Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'opinamber17',
                'dataIndexAbsoluteUid': 'FieldPath://2fd9e43e-bd97-4446-a65e-77f22758149b$8e828051-8b23-447a-abc8-e0828173b132$57067dcd-fbdc-4de0-b6d0-0c2c63bd14e4',
                'dataIndexRelativePath': 'Element1508137003934.Element1507635501632',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd8a09f31-5661-4c82-8163-fa55682ebdfe',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер лицензии',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'opidatereest17',
                'dataIndexAbsoluteUid': 'FieldPath://2fd9e43e-bd97-4446-a65e-77f22758149b$8e828051-8b23-447a-abc8-e0828173b132$0832b728-0baf-41a0-80c5-d3afa04fee3e',
                'dataIndexRelativePath': 'Element1508137003934.Element1507635537755',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'c97fdabb-e317-41f3-9402-319f9cee900e',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата регистрации',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'opidateend17',
                'dataIndexAbsoluteUid': 'FieldPath://2fd9e43e-bd97-4446-a65e-77f22758149b$8e828051-8b23-447a-abc8-e0828173b132$d2b08b92-00bc-45ae-850d-6ec56e5820e1',
                'dataIndexRelativePath': 'Element1508137003934.Element1507635559781',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '33c89907-10ac-4076-8b85-cbd6468b7457',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Окончание срока действия',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'opimesto17',
                'dataIndexAbsoluteUid': 'FieldPath://2fd9e43e-bd97-4446-a65e-77f22758149b$8e828051-8b23-447a-abc8-e0828173b132$ede82dfa-23a3-4bb4-808b-03b818537dbc',
                'dataIndexRelativePath': 'Element1508137003934.Element1507635586209',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '7fea4d67-6abf-4bcc-9db7-8e5c9c4ff576',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Местонаходжение',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});