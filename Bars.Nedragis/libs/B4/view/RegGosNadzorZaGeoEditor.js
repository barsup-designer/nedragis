Ext.define('B4.view.RegGosNadzorZaGeoEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-reggosnadzorzageoeditor',
    title: 'Форма редактирования геология',
    rmsUid: 'cb78b2fc-1f56-4337-a67a-98d0120034a5',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'RegGosNadzorZaGeo_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1507635501632',
        'modelProperty': 'RegGosNadzorZaGeo_Element1507635501632',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507635537755',
        'modelProperty': 'RegGosNadzorZaGeo_Element1507635537755',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1507635559781',
        'modelProperty': 'RegGosNadzorZaGeo_Element1507635559781',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1507635586209',
        'modelProperty': 'RegGosNadzorZaGeo_Element1507635586209',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'cb78b2fc-1f56-4337-a67a-98d0120034a5-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'RegGosNadzorZaGeoEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Номер лицензии',
            'margin': '0 10 0 10',
            'modelProperty': 'RegGosNadzorZaGeo_Element1507635501632',
            'name': 'Element1507635501632',
            'rmsUid': 'ca23be16-3cd5-4592-9f4c-e15ea2ee11bd',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата регистрации',
            'format': 'd.m.Y',
            'margin': '0 10 0 10',
            'modelProperty': 'RegGosNadzorZaGeo_Element1507635537755',
            'name': 'Element1507635537755',
            'rmsUid': '9eeefd2a-cc56-4d04-8d35-4bbcafcdfcbd',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Окончание срока действия',
            'format': 'd.m.Y',
            'margin': '0 10 0 10',
            'modelProperty': 'RegGosNadzorZaGeo_Element1507635559781',
            'name': 'Element1507635559781',
            'rmsUid': '443d4eda-1f0b-4e91-95c9-9e1899d5dce3',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Местонаходжение',
            'margin': '0 10 0 10',
            'modelProperty': 'RegGosNadzorZaGeo_Element1507635586209',
            'name': 'Element1507635586209',
            'rmsUid': '727b766c-9198-4d99-b0b7-c351ca8d463e',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'RegGosNadzorZaGeo_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1507635501632'));
        } else {}
        return res;
    },
});