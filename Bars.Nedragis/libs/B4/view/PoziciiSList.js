Ext.define('B4.view.PoziciiSList', {
    'alias': 'widget.rms-poziciislist',
    'dataSourceUid': '6a80d697-d2cc-4fc4-ae2f-12a4cb52d3c4',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.PoziciiSListModel',
    'stateful': true,
    'title': 'Справочник Позиции спецификации',
    requires: [
        'B4.model.PoziciiSListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-PoziciiSEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-PoziciiSEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://6a80d697-d2cc-4fc4-ae2f-12a4cb52d3c4$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'cfb26aae-4e4d-4be7-a976-2c2f406954ca',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://6a80d697-d2cc-4fc4-ae2f-12a4cb52d3c4$3609fe40-219f-463a-a3c6-a85f30719b3a',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '00551d88-e094-4182-a47c-21deca1cf88d',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 's_pod',
                'dataIndexAbsoluteUid': 'FieldPath://6a80d697-d2cc-4fc4-ae2f-12a4cb52d3c4$78aa5961-b7bc-4b11-ab7b-36ab39105b82',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '51e152ef-7e27-49b3-b899-1983b2f938e7',
                'sortable': true,
                'text': 'Стоимость подготовки',
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 's_hran',
                'dataIndexAbsoluteUid': 'FieldPath://6a80d697-d2cc-4fc4-ae2f-12a4cb52d3c4$4f42ff2b-85ef-462e-a0e4-7641d9eee9ab',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'fcdb9908-a841-44df-a259-cad6e15c98bc',
                'sortable': true,
                'text': 'Стоимость хранения',
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'naklad',
                'dataIndexAbsoluteUid': 'FieldPath://6a80d697-d2cc-4fc4-ae2f-12a4cb52d3c4$d3fa4905-3e0c-4ab7-b621-d6869cc55b0f',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '4b530708-2128-427c-a83e-b95e699d2895',
                'sortable': true,
                'text': 'Накладные расходы',
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'nds',
                'dataIndexAbsoluteUid': 'FieldPath://6a80d697-d2cc-4fc4-ae2f-12a4cb52d3c4$67bf7cc3-093e-4ec1-9fd3-726d1029cce3',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'f92b4dec-59fd-4224-92ac-9c34f95bd5df',
                'sortable': true,
                'text': 'НДС',
                'xtype': 'numbercolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});