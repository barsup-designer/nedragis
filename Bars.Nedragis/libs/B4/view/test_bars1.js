Ext.define('B4.view.test_bars1', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-test_bars1',
    title: 'test_bars1',
    rmsUid: 'f6c40bc9-ec41-427d-8ae3-df43cc30d3f9',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'test_bars_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1481264972872',
        'modelProperty': 'test_bars_Element1481264972872',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1496214131857',
        'modelProperty': 'test_bars_Element1496214131857',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'f6c40bc9-ec41-427d-8ae3-df43cc30d3f9-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'test_bars1-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'decimalPrecision': 4,
            'fieldLabel': 'площадь',
            'minValue': 0,
            'modelProperty': 'test_bars_Element1481264972872',
            'name': 'Element1481264972872',
            'rmsUid': 'afbff5cd-1002-4bda-9e0e-c0f04181978f',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Test',
            'modelProperty': 'test_bars_Element1496214131857',
            'name': 'Element1496214131857',
            'rmsUid': 'c9097bd5-ad7f-4d1a-91d3-f967270a2c61',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'test_bars_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});