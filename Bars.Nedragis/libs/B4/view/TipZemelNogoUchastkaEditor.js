Ext.define('B4.view.TipZemelNogoUchastkaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-tipzemelnogouchastkaeditor',
    title: 'Форма редактирования Тип земельного участка',
    rmsUid: '44456141-fd11-4749-825e-91eab8dcc9ef',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'TipZemelNogoUchastka_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'TipZemelNogoUchastka_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'TipZemelNogoUchastka_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '44456141-fd11-4749-825e-91eab8dcc9ef-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'TipZemelNogoUchastkaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'TipZemelNogoUchastka_code',
            'name': 'code',
            'rmsUid': 'c84fead3-47bf-4dd1-92bd-a80b9a085519',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'TipZemelNogoUchastka_name',
            'name': 'name',
            'rmsUid': '0449a0d0-739a-43ee-871e-99f899c0fcb5',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'TipZemelNogoUchastka_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});