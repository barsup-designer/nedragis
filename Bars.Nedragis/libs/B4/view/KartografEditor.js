Ext.define('B4.view.KartografEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-kartografeditor',
    title: 'Форма редактирования Картография',
    rmsUid: 'e7c7b9ec-acc7-43ac-9fc2-112e8fafdda0',
    requires: [
        'B4.form.PickerField',
        'B4.model.Otdel1EditorModel',
        'B4.model.Otdel1ListModel',
        'B4.model.Sotrudniki1EditorModel',
        'B4.model.Sotrudniki1ListModel',
        'B4.model.VidyRabotEditorModel',
        'B4.model.VidyRabotListModel',
        'B4.view.Otdel1List',
        'B4.view.Sotrudniki1List',
        'B4.view.VidyRabotList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Kartograf_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'Kartograf_name',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'ObjectCreateDate',
        'modelProperty': 'Kartograf_ObjectCreateDate',
        'type': 'DateField'
    }, {
        'dataIndex': 'vid',
        'modelProperty': 'Kartograf_vid',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'podvid',
        'modelProperty': 'Kartograf_podvid',
        'type': 'TextField'
    }, {
        'dataIndex': 'col',
        'modelProperty': 'Kartograf_col',
        'type': 'NumberField'
    }, {
        'dataIndex': 'opis',
        'modelProperty': 'Kartograf_opis',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1474351766399',
        'modelProperty': 'Kartograf_Element1474351766399',
        'type': 'TextField'
    }, {
        'dataIndex': 'zakazchik',
        'modelProperty': 'Kartograf_zakazchik',
        'type': 'TextField'
    }, {
        'dataIndex': 'zakaz1',
        'modelProperty': 'Kartograf_zakaz1',
        'type': 'TextField'
    }, {
        'dataIndex': 'harakter',
        'modelProperty': 'Kartograf_harakter',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1474352034381',
        'modelProperty': 'Kartograf_Element1474352034381',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'organ',
        'modelProperty': 'Kartograf_organ',
        'type': 'TextField'
    }, {
        'dataIndex': 'chas',
        'modelProperty': 'Kartograf_chas',
        'type': 'NumberField'
    }, {
        'dataIndex': 'stoumost',
        'modelProperty': 'Kartograf_stoumost',
        'type': 'NumberField'
    }, {
        'dataIndex': 'oplata',
        'modelProperty': 'Kartograf_oplata',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'e7c7b9ec-acc7-43ac-9fc2-112e8fafdda0-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'KartografEditor-container',
        'autoScroll': true,
        'bodyPadding': '5 5 5 5',
        'flex': 500,
        'height': 650,
        'layout': {
            'type': 'anchor'
        },
        'margin': '5 5 5 5',
        'padding': '10 10 0 10',
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Наименование отдела',
            'idProperty': 'Id',
            'isContextAware': true,
            'labelAlign': 'top',
            'listView': 'B4.view.Otdel1List',
            'listViewCtl': 'B4.controller.Otdel1List',
            'maximizable': true,
            'model': 'B4.model.Otdel1ListModel',
            'modelProperty': 'Kartograf_name',
            'name': 'name',
            'rmsUid': 'a5b28966-a850-45fd-80cc-9907a3668dfb',
            'textProperty': 'NAME_SP',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Наименование отдела',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': false,
            'anchor': '100%',
            'fieldLabel': 'Дата занесения информации',
            'format': 'd.m.Y',
            'modelProperty': 'Kartograf_ObjectCreateDate',
            'name': 'ObjectCreateDate',
            'rmsUid': 'eef1c74b-3c96-413b-879c-c79fd8964170',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Вид работ',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.VidyRabotList',
            'listViewCtl': 'B4.controller.VidyRabotList',
            'model': 'B4.model.VidyRabotListModel',
            'modelProperty': 'Kartograf_vid',
            'name': 'vid',
            'rmsUid': '32999804-77e2-49a3-ace0-9cf706eb287b',
            'textProperty': 'Element1474020299386',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Вид работ',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Подвид работ',
            'modelProperty': 'Kartograf_podvid',
            'name': 'podvid',
            'rmsUid': '22b2e055-bd0b-412a-8c9c-28b0f980a684',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'Количество',
            'modelProperty': 'Kartograf_col',
            'name': 'col',
            'rmsUid': '46653b81-efd5-42a1-8369-5cb9448da37f',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дополнительное описание',
            'modelProperty': 'Kartograf_opis',
            'name': 'opis',
            'rmsUid': '91db7a75-d30d-4029-b3ad-b3b8e1a4b289',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Безвозмездное обращение',
            'modelProperty': 'Kartograf_Element1474351766399',
            'name': 'Element1474351766399',
            'rmsUid': '6a4d50d3-c035-422c-a655-e4093e10db89',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Заказчик',
            'modelProperty': 'Kartograf_zakazchik',
            'name': 'zakazchik',
            'rmsUid': '92ed231f-7292-4199-9fe0-c6b9c174240f',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дополнительная информация о заказчике',
            'modelProperty': 'Kartograf_zakaz1',
            'name': 'zakaz1',
            'rmsUid': 'b0abf829-486a-46e8-8d69-63fcd5901f67',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Характер обращения',
            'modelProperty': 'Kartograf_harakter',
            'name': 'harakter',
            'rmsUid': '5d1cd9fd-6202-4abe-836d-0606582d3a09',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Исполнитель',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.Sotrudniki1List',
            'listViewCtl': 'B4.controller.Sotrudniki1List',
            'maximizable': true,
            'model': 'B4.model.Sotrudniki1ListModel',
            'modelProperty': 'Kartograf_Element1474352034381',
            'name': 'Element1474352034381',
            'rmsUid': '72fbbb20-522b-48a6-b279-b72c7f5771e3',
            'textProperty': 'Element1474352136232',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Исполнитель',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Орган госвласти',
            'modelProperty': 'Kartograf_organ',
            'name': 'organ',
            'rmsUid': 'f689512e-dbbc-4d8e-bf1f-85c68aac5941',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'Количество затраченных человека часов',
            'modelProperty': 'Kartograf_chas',
            'name': 'chas',
            'rmsUid': '95268d2f-2395-46ef-9e91-6af6bd7bd8dd',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Стоимость по калькуляции руб.',
            'labelAlign': 'left',
            'margin': '10 0 0 0',
            'minValue': 0,
            'modelProperty': 'Kartograf_stoumost',
            'name': 'stoumost',
            'rmsUid': '2d71353f-9ded-4a1b-95db-21b1c877f107',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Оплачено',
            'modelProperty': 'Kartograf_oplata',
            'name': 'oplata',
            'rmsUid': '3219b336-97c3-401c-9436-bbbe0629533e',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Kartograf_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});