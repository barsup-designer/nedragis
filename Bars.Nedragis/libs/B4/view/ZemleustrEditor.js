Ext.define('B4.view.ZemleustrEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-zemleustreditor',
    title: 'Форма редактирования Землеустройство',
    rmsUid: '4dd905da-08f4-43d3-a5e1-a61dea0c71bc',
    requires: [
        'B4.form.PickerField',
        'B4.model.Otdel1EditorModel',
        'B4.model.Otdel1ListModel',
        'B4.model.RajjonyJaNAOEditorModel',
        'B4.model.RajjonyJaNAOListModel',
        'B4.model.Sotrudniki1EditorModel',
        'B4.model.Sotrudniki1ListModel',
        'B4.view.Otdel1List',
        'B4.view.RajjonyJaNAOList',
        'B4.view.Sotrudniki1List'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Zemleustr_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name_z',
        'modelProperty': 'Zemleustr_name_z',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'NDelo',
        'modelProperty': 'Zemleustr_NDelo',
        'type': 'TextField'
    }, {
        'dataIndex': 'vid_rabot',
        'modelProperty': 'Zemleustr_vid_rabot',
        'type': 'TextField'
    }, {
        'dataIndex': 'komu',
        'modelProperty': 'Zemleustr_komu',
        'type': 'TextField'
    }, {
        'dataIndex': 'company',
        'modelProperty': 'Zemleustr_company',
        'type': 'TextField'
    }, {
        'dataIndex': 'name_obj',
        'modelProperty': 'Zemleustr_name_obj',
        'type': 'TextField'
    }, {
        'dataIndex': 'kad_nom',
        'modelProperty': 'Zemleustr_kad_nom',
        'type': 'TextField'
    }, {
        'dataIndex': 'kateg_zem',
        'modelProperty': 'Zemleustr_kateg_zem',
        'type': 'TextField'
    }, {
        'dataIndex': 'date_vh_doc',
        'modelProperty': 'Zemleustr_date_vh_doc',
        'type': 'DateField'
    }, {
        'dataIndex': 'konech_kategor',
        'modelProperty': 'Zemleustr_konech_kategor',
        'type': 'TextField'
    }, {
        'dataIndex': 'square',
        'modelProperty': 'Zemleustr_square',
        'type': 'NumberField'
    }, {
        'dataIndex': 'region',
        'modelProperty': 'Zemleustr_region',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'date_postupl',
        'modelProperty': 'Zemleustr_date_postupl',
        'type': 'DateField'
    }, {
        'dataIndex': 'Etap',
        'modelProperty': 'Zemleustr_Etap',
        'type': 'TextField'
    }, {
        'dataIndex': 'nom_vh_doc',
        'modelProperty': 'Zemleustr_nom_vh_doc',
        'type': 'TextField'
    }, {
        'dataIndex': 'autor',
        'modelProperty': 'Zemleustr_autor',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'nomer_pisma',
        'modelProperty': 'Zemleustr_nomer_pisma',
        'type': 'NumberField'
    }, {
        'dataIndex': 'data_usp',
        'modelProperty': 'Zemleustr_data_usp',
        'type': 'DateField'
    }, {
        'dataIndex': 'ispol',
        'modelProperty': 'Zemleustr_ispol',
        'type': 'TextField'
    }, {
        'dataIndex': 'format',
        'modelProperty': 'Zemleustr_format',
        'type': 'TextField'
    }, {
        'dataIndex': 'kolvo_ed',
        'modelProperty': 'Zemleustr_kolvo_ed',
        'type': 'NumberField'
    }, {
        'dataIndex': 'prim',
        'modelProperty': 'Zemleustr_prim',
        'type': 'TextField'
    }, {
        'dataIndex': 'sum_kalk',
        'modelProperty': 'Zemleustr_sum_kalk',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '4dd905da-08f4-43d3-a5e1-a61dea0c71bc-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ZemleustrEditor-container',
        'autoScroll': true,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Наименование отдела',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.Otdel1List',
            'listViewCtl': 'B4.controller.Otdel1List',
            'maximizable': true,
            'model': 'B4.model.Otdel1ListModel',
            'modelProperty': 'Zemleustr_name_z',
            'name': 'name_z',
            'rmsUid': '15566e63-c494-4134-93cd-d1f909d3c91b',
            'textProperty': 'NAME_SP',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Наименование отдела',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Номер дела/проекта',
            'margin': '10 200 10 10',
            'modelProperty': 'Zemleustr_NDelo',
            'name': 'NDelo',
            'rmsUid': '955f89e0-a1f2-40b9-b75c-ee7bbb8bba6d',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Вид работ',
            'margin': '10 10 10 10',
            'modelProperty': 'Zemleustr_vid_rabot',
            'name': 'vid_rabot',
            'rmsUid': '04898b40-90c5-4c02-be4b-e870f57be1c3',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Кому',
            'margin': '10 10 10 10',
            'modelProperty': 'Zemleustr_komu',
            'name': 'komu',
            'rmsUid': '04aa4a44-2ab7-49d0-afed-54e58423d33e',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Компания',
            'margin': '10 10 10 10',
            'modelProperty': 'Zemleustr_company',
            'name': 'company',
            'rmsUid': 'b4d3bc6f-1179-41dd-b21b-aab9c3086f1b',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Название объекта',
            'margin': '10 10 10 10',
            'modelProperty': 'Zemleustr_name_obj',
            'name': 'name_obj',
            'rmsUid': '6b3a5bf6-01b7-425f-9253-f964d477b6c2',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Кадастровый номер',
            'margin': '10 200 10 10',
            'modelProperty': 'Zemleustr_kad_nom',
            'name': 'kad_nom',
            'rmsUid': '99d7b389-159c-4505-bc2a-6b8e0894fa93',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Исходная категория земель',
            'margin': '10 10 10 10',
            'modelProperty': 'Zemleustr_kateg_zem',
            'name': 'kateg_zem',
            'rmsUid': 'ae242339-6051-49d4-829a-6e73420e2cfc',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата входящего документа',
            'format': 'd.m.Y',
            'margin': '10 200 10 10',
            'modelProperty': 'Zemleustr_date_vh_doc',
            'name': 'date_vh_doc',
            'rmsUid': '39f40dfc-38a0-463f-aff0-e157dc1917f2',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Конечная категория земель',
            'margin': '10 10 10 10',
            'modelProperty': 'Zemleustr_konech_kategor',
            'name': 'konech_kategor',
            'rmsUid': '1c68e19c-cd2e-4b35-ba4e-63dd710777b6',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'decimalPrecision': 4,
            'fieldLabel': 'Площадь',
            'hideTrigger': true,
            'margin': '10 200 10 10',
            'minValue': 0,
            'modelProperty': 'Zemleustr_square',
            'name': 'square',
            'rmsUid': '7f5757f3-e498-4d0d-87a9-633594eb8372',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Район',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.RajjonyJaNAOList',
            'listViewCtl': 'B4.controller.RajjonyJaNAOList',
            'margin': '10 10 10 10',
            'maximizable': true,
            'model': 'B4.model.RajjonyJaNAOListModel',
            'modelProperty': 'Zemleustr_region',
            'name': 'region',
            'rmsUid': '1b020f42-d6c7-4686-92c4-87182c5eb0a9',
            'textProperty': 'name',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Район',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата поступления в ГКУ Ресурсы Ямала',
            'format': 'd.m.Y',
            'margin': '10 200 10 10',
            'modelProperty': 'Zemleustr_date_postupl',
            'name': 'date_postupl',
            'rmsUid': 'e9f18c91-5e00-4adf-8834-79eb4441aedb',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Этап',
            'margin': '10 10 10 10',
            'modelProperty': 'Zemleustr_Etap',
            'name': 'Etap',
            'rmsUid': 'de8caac8-a47f-455c-9ddf-74062e9ddaa8',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Номер входящего документа',
            'margin': '10 200 10 10',
            'modelProperty': 'Zemleustr_nom_vh_doc',
            'name': 'nom_vh_doc',
            'rmsUid': 'b383b1cd-dd52-4d9e-b238-21f298eb854c',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Автор(исполнитель)',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.Sotrudniki1List',
            'listViewCtl': 'B4.controller.Sotrudniki1List',
            'maximizable': true,
            'model': 'B4.model.Sotrudniki1ListModel',
            'modelProperty': 'Zemleustr_autor',
            'name': 'autor',
            'rmsUid': '636d3b92-31e2-43bb-90d7-e7df4c2a0ec8',
            'textProperty': 'Element1474352136232',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Автор(исполнитель)',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Номер письма заявителя',
            'minValue': 0,
            'modelProperty': 'Zemleustr_nomer_pisma',
            'name': 'nomer_pisma',
            'rmsUid': '2c078cc1-fce3-41ff-a1d5-fdc71c8ca1a9',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата исполнения',
            'format': 'd.m.Y',
            'modelProperty': 'Zemleustr_data_usp',
            'name': 'data_usp',
            'rmsUid': 'b30c74c9-9791-4fa7-a970-839e975fdbdf',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Исполнитель',
            'modelProperty': 'Zemleustr_ispol',
            'name': 'ispol',
            'rmsUid': '5ca4d982-21bb-46c9-a058-e4e0b0ddba7c',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Формат данных',
            'margin': '10 200 10 10',
            'modelProperty': 'Zemleustr_format',
            'name': 'format',
            'rmsUid': '9313d0fd-6a29-4ffc-af05-f84c8d68e66d',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'decimalPrecision': 0,
            'fieldLabel': 'Количество единиц',
            'hideTrigger': true,
            'margin': '10 200 10 10',
            'minValue': 0,
            'modelProperty': 'Zemleustr_kolvo_ed',
            'name': 'kolvo_ed',
            'rmsUid': 'c479e45b-a80b-4167-bc31-5b250c950e59',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Примечание',
            'margin': '10 10 10 10',
            'modelProperty': 'Zemleustr_prim',
            'name': 'prim',
            'rmsUid': '09b1ba3e-1cad-4ca4-a9d2-2db369c8172e',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Сумма калькуляции',
            'hideTrigger': true,
            'margin': '10 200 10 10',
            'minValue': 0,
            'modelProperty': 'Zemleustr_sum_kalk',
            'name': 'sum_kalk',
            'rmsUid': 'cf296c77-f388-410f-a9e7-902f3dc3e435',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Zemleustr_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('NDelo'));
        } else {}
        return res;
    },
});