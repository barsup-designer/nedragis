Ext.define('B4.view.ReshenieEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-reshenieeditor',
    title: 'Форма редактирования Решение по праву приобретения информации',
    rmsUid: 'baf672b3-bc97-44f0-a2c2-5487b20e01a0',
    requires: [
        'B4.form.PickerField',
        'B4.model.SpecifikacijaEditor2Model',
        'B4.model.SpecifikacijaListModel',
        'B4.view.SpecifikacijaList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Reshenie_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1478752620339',
        'modelProperty': 'Reshenie_Element1478752620339',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478752748169',
        'modelProperty': 'Reshenie_Element1478752748169',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1478752768753',
        'modelProperty': 'Reshenie_Element1478752768753',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478752791585',
        'modelProperty': 'Reshenie_Element1478752791585',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1478752831192',
        'modelProperty': 'Reshenie_Element1478752831192',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1478752856240',
        'modelProperty': 'Reshenie_Element1478752856240',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478753246124',
        'modelProperty': 'Reshenie_Element1478753246124',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1478752884224',
        'modelProperty': 'Reshenie_Element1478752884224',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478753305771',
        'modelProperty': 'Reshenie_Element1478753305771',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478753329859',
        'modelProperty': 'Reshenie_Element1478753329859',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478753351594',
        'modelProperty': 'Reshenie_Element1478753351594',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'baf672b3-bc97-44f0-a2c2-5487b20e01a0-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ReshenieEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Контейнер',
                'items': [{
                    'allowBlank': true,
                    'fieldLabel': 'Номер решения',
                    'margin': '5 5 5 5',
                    'modelProperty': 'Reshenie_Element1478752620339',
                    'name': 'Element1478752620339',
                    'rmsUid': '44b3f87d-d4f7-422f-bfb4-ca885d6865a8',
                    'xtype': 'textfield'
                }, {
                    'allowBlank': true,
                    'fieldLabel': 'Дата решения',
                    'format': 'd.m.Y',
                    'margin': '5 5 5 5',
                    'modelProperty': 'Reshenie_Element1478752748169',
                    'name': 'Element1478752748169',
                    'rmsUid': '2592730d-6dae-42da-bb30-9ad3c104c3ca',
                    'xtype': 'datefield'
                }],
                'layout': {
                    'type': 'hbox'
                },
                'rmsUid': '22b5306a-97ac-4eec-a641-a7dd67b4be9b',
                'title': 'Контейнер',
                'xtype': 'container'
            }, {
                'allowBlank': true,
                'anchor': '100%',
                'fieldLabel': 'Текст',
                'margin': '5 5 5 5',
                'modelProperty': 'Reshenie_Element1478752768753',
                'name': 'Element1478752768753',
                'rmsUid': '7f607293-bb5e-4d14-98c5-93ddbb0fb2ea',
                'xtype': 'textfield'
            }],
            'rmsUid': 'f46b1537-00a0-4aca-b4ec-d4a0b4b213ed',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Контейнер',
                'items': [{
                    'allowBlank': true,
                    'allowDecimals': false,
                    'decimalPrecision': 2,
                    'fieldLabel': 'Номер запроса в фед. агенство',
                    'margin': '5 5 5 5',
                    'minValue': 0,
                    'modelProperty': 'Reshenie_Element1478752791585',
                    'name': 'Element1478752791585',
                    'rmsUid': 'f2835ccb-c767-4b4f-ba32-585aa4d56294',
                    'step': 1,
                    'xtype': 'numberfield'
                }, {
                    'allowBlank': true,
                    'fieldLabel': 'Дата запроса в фед агенство',
                    'format': 'd.m.Y',
                    'margin': '5 5 5 5',
                    'modelProperty': 'Reshenie_Element1478752831192',
                    'name': 'Element1478752831192',
                    'rmsUid': '4bc3cb87-f43b-4677-8e49-d82e6a0ba9aa',
                    'xtype': 'datefield'
                }],
                'layout': {
                    'type': 'hbox'
                },
                'rmsUid': 'b948e560-e46d-419a-87a1-7f682401dea4',
                'title': 'Контейнер',
                'xtype': 'container'
            }, {
                'allowBlank': true,
                'anchor': '100%',
                'fieldLabel': 'Текст запроса в фед. агенство',
                'margin': '5 5 5 5',
                'modelProperty': 'Reshenie_Element1478752856240',
                'name': 'Element1478752856240',
                'rmsUid': '52818127-2fd0-4ed2-a3a3-73c478047181',
                'xtype': 'textfield'
            }],
            'rmsUid': 'aa8d1ad4-9704-4a98-a55a-6c9baca9afa1',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Контейнер',
                'items': [{
                    'allowBlank': true,
                    'fieldLabel': 'Дата решения фед агенства',
                    'format': 'd.m.Y',
                    'margin': '5 5 5 5',
                    'modelProperty': 'Reshenie_Element1478753246124',
                    'name': 'Element1478753246124',
                    'rmsUid': 'f163d9cc-a2a4-4e75-b788-ab35670a648a',
                    'xtype': 'datefield'
                }, {
                    'allowBlank': true,
                    'fieldLabel': 'Номер решения фед.агенства',
                    'margin': '5 5 5 5',
                    'modelProperty': 'Reshenie_Element1478752884224',
                    'name': 'Element1478752884224',
                    'rmsUid': '715cf89e-5156-4a9a-880d-99a99e0b05a1',
                    'xtype': 'textfield'
                }],
                'layout': {
                    'type': 'hbox'
                },
                'rmsUid': 'df138c25-6e83-4931-8197-22699a228f25',
                'title': 'Контейнер',
                'xtype': 'container'
            }, {
                'allowBlank': true,
                'anchor': '100%',
                'fieldLabel': 'Текст решения фед агенства',
                'margin': '5 5 5 5',
                'modelProperty': 'Reshenie_Element1478753305771',
                'name': 'Element1478753305771',
                'rmsUid': 'e5cfd5c2-e941-4fda-ac40-601c1f3fce41',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'anchor': '100%',
                'fieldLabel': 'Руководитель фед. агенства',
                'margin': '5 400 5 5',
                'modelProperty': 'Reshenie_Element1478753329859',
                'name': 'Element1478753329859',
                'rmsUid': 'cfda3dbb-35cd-4f99-a800-f143b9a2ec75',
                'xtype': 'textfield'
            }],
            'rmsUid': '0eca32ef-7abb-444a-a090-6ce89f0fc2ba',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Номер спецификации по заявке',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.SpecifikacijaList',
            'listViewCtl': 'B4.controller.SpecifikacijaList',
            'margin': '5 600 5 5',
            'maximizable': true,
            'model': 'B4.model.SpecifikacijaListModel',
            'modelProperty': 'Reshenie_Element1478753351594',
            'name': 'Element1478753351594',
            'rmsUid': '8946e4b8-e208-44ba-a59e-03d8a7276490',
            'textProperty': 'nomsp',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Номер спецификации по заявке',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'anchor': '100%',
            'fieldLabel': 'Посмотреть',
            'margin': '10 10 10 10',
            'rmsUid': '7182acad-b5c2-439a-a3e5-c72a6cd51260',
            'text': 'Посмотреть',
            'xtype': 'button'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Reshenie_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1478752620339'));
        } else {}
        return res;
    },
});