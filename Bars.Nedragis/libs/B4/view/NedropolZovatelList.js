Ext.define('B4.view.NedropolZovatelList', {
    'alias': 'widget.rms-nedropolzovatellist',
    'dataSourceUid': '0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.NedropolZovatelListModel',
    'stateful': true,
    'title': 'Справочник Недропользователь',
    requires: [
        'B4.model.NedropolZovatelListModel',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-NedropolZovatelEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-NedropolZovatelEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '803b0f41-2551-49b4-979e-a3a78de41bc3',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$a89e07c3-c289-45c4-8389-931826ce22ab',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '85cba6c3-afdc-4fcf-9932-548eddac365b',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'adres',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$24a10b32-e511-4396-925b-ad1fbb6e3482',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '7510541d-7fb4-4db0-8534-0dab3cdf68fb',
                'sortable': true,
                'text': 'Адрес',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'tel',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$4fe69fb9-ffcc-4db7-823e-f8f3a8860834',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'dcce271a-1f08-4bd0-86e1-92ac9cdf8c90',
                'sortable': true,
                'text': 'Телефон',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'mail',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$359a580a-717e-496a-8c5c-b0388c7ceff9',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '48b6e82c-3a22-47a8-9e90-fadeadb9e21b',
                'sortable': true,
                'text': 'e-mail',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'fax',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$5191b69b-a48b-460d-b6eb-db4b64dd8042',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '205ef239-dedc-43f4-a190-fdac03a304c3',
                'sortable': true,
                'text': 'Факс',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'member',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$443d11dd-2ca9-4432-86c4-802676f4d0f2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'edc4db82-31f5-480c-913c-5c931268d8ee',
                'sortable': true,
                'text': 'Руководитель',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'inn',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$1de99098-c379-46bc-8d3a-6b2b13873c95',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'ef3227c2-f0f9-4e52-a292-ec5d473b75f3',
                'sortable': true,
                'text': 'ИНН',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'kpp',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$95777287-2d75-44a3-b8c5-af6cf8e05fa2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '89d8f4d1-f7c9-4753-a215-a336c43ed415',
                'sortable': true,
                'text': 'КПП',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'rs',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$0a357ad8-a6fc-426f-a775-50a030141637',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '28abd6df-4953-4adf-a7c6-13262e53b31c',
                'sortable': true,
                'text': 'Расчетный счет',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'bik',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$154766bb-ce6a-4dd3-a18c-f1cf06aaf9d3',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '220d7196-c44c-47f5-9610-fa873ce16993',
                'sortable': true,
                'text': 'БИК',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'kors',
                'dataIndexAbsoluteUid': 'FieldPath://0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb$9fe34937-d8a5-4d56-aabc-ea818ac5cc20',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '16adfc29-c297-4ff7-8072-c03b1d3884cb',
                'sortable': true,
                'text': 'Корресп. счет',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }]
        });
        me.callParent(arguments);
    }
});