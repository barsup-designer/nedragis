Ext.define('B4.view.SpecifikacijaEditor2', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-specifikacijaeditor2',
    title: 'Форма редактирования Спецификация2',
    rmsUid: '00a7d75c-27b2-4769-a0e7-8df1068feadb',
    requires: [
        'B4.form.PickerField',
        'B4.model.SotrudnikiEditorModel',
        'B4.model.SotrudnikiListModel',
        'B4.view.SotrudnikiList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Specifikacija_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'nomsp',
        'modelProperty': 'Specifikacija_nomsp',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1478601989143',
        'modelProperty': 'Specifikacija_Element1478601989143',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1478602041541',
        'modelProperty': 'Specifikacija_Element1478602041541',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1478602079636',
        'modelProperty': 'Specifikacija_Element1478602079636',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1478602280592',
        'modelProperty': 'Specifikacija_Element1478602280592',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1478607128570',
        'modelProperty': 'Specifikacija_Element1478607128570',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478607204176',
        'modelProperty': 'Specifikacija_Element1478607204176',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '00a7d75c-27b2-4769-a0e7-8df1068feadb-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'SpecifikacijaEditor2-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'Номер спецификации',
            'modelProperty': 'Specifikacija_nomsp',
            'name': 'nomsp',
            'rmsUid': '75740c45-ea06-4f40-bc20-8455d5a00f15',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата спецификации',
            'format': 'd.m.Y',
            'modelProperty': 'Specifikacija_Element1478601989143',
            'name': 'Element1478601989143',
            'rmsUid': '188b172f-bb4d-429d-937d-ebfce54109e7',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'Количество',
            'modelProperty': 'Specifikacija_Element1478602041541',
            'name': 'Element1478602041541',
            'rmsUid': '19bb9045-ae22-4a9d-9a60-b05172b696a9',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Итоговая сумма',
            'modelProperty': 'Specifikacija_Element1478602079636',
            'name': 'Element1478602079636',
            'rmsUid': '3693e7da-a823-400b-acb9-aa542a053154',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'Номер позиции спецификации',
            'modelProperty': 'Specifikacija_Element1478602280592',
            'name': 'Element1478602280592',
            'rmsUid': 'e2da8124-7661-4e44-8f00-38191675d135',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Номер заявки',
            'modelProperty': 'Specifikacija_Element1478607128570',
            'name': 'Element1478607128570',
            'rmsUid': 'f060f8b3-b611-4913-8e29-02f35a970873',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Сотрудник',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.SotrudnikiList',
            'listViewCtl': 'B4.controller.SotrudnikiList',
            'model': 'B4.model.SotrudnikiListModel',
            'modelProperty': 'Specifikacija_Element1478607204176',
            'name': 'Element1478607204176',
            'rmsUid': 'f252d555-fe02-4e21-9f1b-2dfabd06b800',
            'textProperty': 'Id',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Сотрудник',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Specifikacija_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('nomsp'));
        } else {}
        return res;
    },
});