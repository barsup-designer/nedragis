Ext.define('B4.view.PodvidRabotPrirodaList', {
    'alias': 'widget.rms-podvidrabotprirodalist',
    'dataSourceUid': 'bbf0e8a9-5ab2-491d-899f-4f2e652309b1',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.PodvidRabotPrirodaListModel',
    'stateful': true,
    'title': 'Реестр Подвид работ природа',
    requires: [
        'B4.model.PodvidRabotPrirodaListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-PodvidRabotPrirodaEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-PodvidRabotPrirodaEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Podvid_rab_pr',
                'dataIndexAbsoluteUid': 'FieldPath://bbf0e8a9-5ab2-491d-899f-4f2e652309b1$618375fc-f98a-45b1-b759-96866251edc2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '57ff2761-1b26-4552-9cac-1e70785d35b0',
                'sortable': true,
                'text': 'Подвид работ ',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://bbf0e8a9-5ab2-491d-899f-4f2e652309b1$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2c3a7aec-49b0-4108-9163-e279670ce191',
                'sortable': true,
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});