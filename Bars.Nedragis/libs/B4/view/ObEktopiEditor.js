Ext.define('B4.view.ObEktopiEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-obektopieditor',
    title: 'Форма редактирования объектопи',
    rmsUid: '414ffe01-f268-40bb-9261-9efc56a2c442',
    requires: [
        'B4.form.PickerField',
        'B4.model.ReestrObEktovEditorModel',
        'B4.model.ReestrObEktovListModel',
        'B4.model.RegGosNadzorZaGeoEditorModel',
        'B4.model.RegGosNadzorZaGeoListModel',
        'B4.view.ReestrObEktovList',
        'B4.view.RegGosNadzorZaGeoList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'ObEktopi_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1508136885094',
        'modelProperty': 'ObEktopi_Element1508136885094',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Element1508137003934',
        'modelProperty': 'ObEktopi_Element1508137003934',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '414ffe01-f268-40bb-9261-9efc56a2c442-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ObEktopiEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Объект',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.ReestrObEktovList',
            'listViewCtl': 'B4.controller.ReestrObEktovList',
            'model': 'B4.model.ReestrObEktovListModel',
            'modelProperty': 'ObEktopi_Element1508136885094',
            'name': 'Element1508136885094',
            'rmsUid': '8d036332-5713-4a27-97c5-9e89201e4c60',
            'textProperty': 'Element1507784366035',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Объект',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'опи',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.RegGosNadzorZaGeoList',
            'listViewCtl': 'B4.controller.RegGosNadzorZaGeoList',
            'model': 'B4.model.RegGosNadzorZaGeoListModel',
            'modelProperty': 'ObEktopi_Element1508137003934',
            'name': 'Element1508137003934',
            'rmsUid': '1082c1bc-f3e7-4e5e-bf8e-cffd1bb46782',
            'textProperty': 'Element1507635501632',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'опи',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'ObEktopi_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});