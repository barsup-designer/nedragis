Ext.define('B4.view.ReestrNadzor', {
    'alias': 'widget.rms-reestrnadzor',
    'dataSourceUid': 'ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': false,
    'extend': 'B4.base.registry.View',
    'groupField': 'subjectnadzorann',
    'model': 'B4.model.ReestrNadzorModel',
    'stateful': true,
    'title': 'Реестр объектов регионального надзора ДПРР ЯНАО',
    requires: [
        'B4.model.ReestrNadzorModel',
        'B4.ux.grid.plugin.ExportExcel',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-add-png',
        'itemId': 'Addition-FormaNadzor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-edit-png',
        'itemId': 'Editing-FormaNadzor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-remove-png',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'nomerpoprc',
                'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$b0cb6807-9191-4f9a-9af3-58b5140f5b80',
                'dataIndexRelativePath': 'Element1507619429093',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '09e95fcd-d8c0-4659-897e-afa933737c6b',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': '№',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'subjectnadzorann',
                'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$26d536f1-dc05-4d6f-a7e5-98e87be26490$65de9155-a350-4206-be94-ec95bbf9ef90',
                'dataIndexRelativePath': 'Element1507271571833.Element1507203167462',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'cb3263b4-5f81-48ca-b6e9-b8faab87fc20',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование субъекта надзора',
                'width': 200,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'reestrkodsv',
                'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$bd254f04-6ed8-4f8f-b98a-8e073a42a86b',
                'dataIndexRelativePath': 'Element1507611045539',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '4f19e24f-41e7-420c-bc86-207c5f13e79a',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Код свидетельства',
                'width': 100,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'innr',
                'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$6b23037f-c1ec-41fd-9325-e83fda6bb667',
                'dataIndexRelativePath': 'inn',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'da041158-661a-4bcd-8254-7e39f565aa3c',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'ИНН',
                'width': 100,
                'xtype': 'gridcolumn'
            }, {
                'columns': [{
                    'columns': [{
                        'dataIndex': 'licenseNumber',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$724a1ace-9cc8-4eaa-b93c-a3ebab18ed40',
                        'dataIndexRelativePath': 'Element1507010532348',
                        'filter': {
                            'xtype': 'b4-filter-field-list-of-string-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'rmsUid': '30fb93f8-07ca-4020-834d-b17e5be0b42e',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': '№ лицензии',
                        'xtype': 'gridcolumn'
                    }, {
                        'dataIndex': 'dater',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$d44caab2-6388-43f9-9b24-7cf5ed19c4ac',
                        'dataIndexRelativePath': 'date',
                        'filter': {
                            'type': 'utcdatetime',
                            'useDays': true,
                            'useQuarters': true,
                            'xtype': 'b4-filter-field-list-of-date-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'renderer': B4.grid.Renderers.date,
                        'rmsUid': '84c389c7-a5df-4e03-8a92-4170e18c40c5',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Дата регистации',
                        'xtype': 'gridcolumn'
                    }, {
                        'dataIndex': 'srok_endr',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$fe470564-0ffd-462f-b0d1-2e72f2babd67',
                        'dataIndexRelativePath': 'srok_end',
                        'filter': {
                            'type': 'utcdatetime',
                            'useDays': true,
                            'useQuarters': true,
                            'xtype': 'b4-filter-field-list-of-date-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'renderer': B4.grid.Renderers.date,
                        'rmsUid': 'b95e04fc-a28e-442e-bc56-62c46bde490f',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Окончание срока действия',
                        'xtype': 'gridcolumn'
                    }, {
                        'dataIndex': 'mestor',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$6378e4a9-1904-497b-9f2c-77be3c3a56ba',
                        'dataIndexRelativePath': 'mesto',
                        'filter': {
                            'xtype': 'b4-filter-field-list-of-string-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'rmsUid': 'e34cbb4a-e319-47b9-b142-cc3178ac6e7d',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Местонахождение',
                        'xtype': 'gridcolumn'
                    }],
                    'flex': 1,
                    'menuDisabled': true,
                    'multisortable': false,
                    'rmsUid': 'c4a6d35a-d3c4-4193-8a5b-16da161ef6e7',
                    'sortable': false,
                    'text': 'Региональный государственный надзор за геологическим изучением, рациональным использованием и охраной недр',
                    'xtype': 'gridcolumn'
                }, {
                    'columns': [{
                        'dataIndex': 'reesrdogvod1',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$9c3d7d3e-8176-4a13-a464-95f00cc83d42',
                        'dataIndexRelativePath': 'Element1507703919662',
                        'filter': {
                            'xtype': 'b4-filter-field-list-of-string-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'rmsUid': '90ebf995-81b4-4aa8-8456-388361cb442f',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Договор водопользования',
                        'xtype': 'gridcolumn'
                    }, {
                        'dataIndex': 'dogovorDataregisrt',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$31893987-a9d1-442b-81e0-9417baff4a65',
                        'dataIndexRelativePath': 'Element1507262033494',
                        'filter': {
                            'type': 'utcdatetime',
                            'useDays': true,
                            'useQuarters': true,
                            'xtype': 'b4-filter-field-list-of-date-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'renderer': B4.grid.Renderers.date,
                        'rmsUid': '3274d752-f6e5-4b4a-b411-508fd5922166',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Дата регистрации договора',
                        'xtype': 'gridcolumn'
                    }, {
                        'dataIndex': 'dogovorDataend',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$d49378a8-7478-4ba4-962d-19a02ba50403',
                        'dataIndexRelativePath': 'Element1507262072161',
                        'filter': {
                            'type': 'utcdatetime',
                            'useDays': true,
                            'useQuarters': true,
                            'xtype': 'b4-filter-field-list-of-date-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'renderer': B4.grid.Renderers.date,
                        'rmsUid': 'dca2c752-da9a-47e3-8710-7dfa4b4243c6',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Окончание срока действия договора',
                        'xtype': 'gridcolumn'
                    }, {
                        'dataIndex': 'dogovorMestopol',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$ac975412-32d2-45f8-b41a-87ae55ef2984',
                        'dataIndexRelativePath': 'Element1507262105889',
                        'filter': {
                            'xtype': 'b4-filter-field-list-of-string-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'rmsUid': 'ec6e542f-8e57-4570-b0f2-35f32b98fac5',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Местоположение',
                        'xtype': 'gridcolumn'
                    }],
                    'flex': 1,
                    'menuDisabled': true,
                    'multisortable': false,
                    'rmsUid': '5af0a447-711c-40b7-8409-35c2d8b0aafa',
                    'sortable': false,
                    'text': 'Региональный государственный надзор в области использования и охраны водных объектов',
                    'xtype': 'gridcolumn'
                }, {
                    'columns': [{
                        'dataIndex': 'istochnikzag',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$1ad85c9f-1b43-48fa-8aee-870883896309',
                        'dataIndexRelativePath': 'Element1507262162260',
                        'filter': {
                            'xtype': 'b4-filter-field-list-of-string-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'rmsUid': 'b847688d-23d3-4774-8d85-97faf7370c4f',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Наименование источника загрязнения',
                        'xtype': 'gridcolumn'
                    }, {
                        'dataIndex': 'istochnikMestopol',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$0b083d72-bdab-46c8-ac2f-fe420d69b6f2',
                        'dataIndexRelativePath': 'Element1507262266009',
                        'filter': {
                            'xtype': 'b4-filter-field-list-of-string-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'rmsUid': '6e368061-89bc-4a4b-a5d5-85fdb3614ac8',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Местонахождение',
                        'xtype': 'gridcolumn'
                    }, {
                        'dataIndex': 'istochnikTom',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$91e1e0ab-b9da-4c29-8cd0-f86678e88e1c',
                        'dataIndexRelativePath': 'Element1507262301335',
                        'filter': {
                            'xtype': 'b4-filter-field-list-of-string-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'rmsUid': 'ccdb5ecc-3718-48db-803d-244a9fc3286b',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Наличие тома ПДВ',
                        'xtype': 'gridcolumn'
                    }, {
                        'dataIndex': 'istochnukRaz',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$e0b4ead1-2dea-4027-8fe7-31fb183c1cbb',
                        'dataIndexRelativePath': 'Element1507262339156',
                        'filter': {
                            'xtype': 'b4-filter-field-list-of-string-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'rmsUid': '2fbedc22-45d4-4d2b-a844-196401d95f30',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Разрешение на выброс в атмосферу',
                        'xtype': 'gridcolumn'
                    }],
                    'flex': 1,
                    'menuDisabled': true,
                    'multisortable': false,
                    'rmsUid': '3a8ba664-5e62-4359-8e3e-82a1bd8fdb4b',
                    'sortable': false,
                    'text': 'Государственный надзор в области охраны атмосферного воздуха ',
                    'xtype': 'gridcolumn'
                }, {
                    'columns': [{
                        'dataIndex': 'otxName',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$4647ced5-c421-4bde-bc8b-29cb2374ec4e',
                        'dataIndexRelativePath': 'Element1507262379380',
                        'filter': {
                            'xtype': 'b4-filter-field-list-of-string-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'rmsUid': 'a208d848-5102-44e3-af15-216a0bf65d5f',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Наименование объекта образования отходов',
                        'xtype': 'gridcolumn'
                    }, {
                        'dataIndex': 'otxMestopol',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$a9adbe34-7d92-4a53-a6d1-15e78f9a9830',
                        'dataIndexRelativePath': 'Element1507262414231',
                        'filter': {
                            'xtype': 'b4-filter-field-list-of-string-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'rmsUid': '37657b8b-68ff-495e-b029-fe9d35b2d2fd',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Местонахождение',
                        'xtype': 'gridcolumn'
                    }, {
                        'dataIndex': 'otxProject',
                        'dataIndexAbsoluteUid': 'FieldPath://ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b$10c8749a-0faa-4c82-92b9-660d008b0b34',
                        'dataIndexRelativePath': 'Element1507262455649',
                        'filter': {
                            'xtype': 'b4-filter-field-list-of-string-property'
                        },
                        'flex': 1,
                        'menuDisabled': false,
                        'multisortable': false,
                        'rmsUid': '0878794c-dba6-49c8-a50d-0c04a43d758d',
                        'sortable': true,
                        'summaryRenderer': function(value) {
                            return Ext.String.format('{0}', value);
                        },
                        'summaryType': 'none',
                        'text': 'Проект нормативов образования отходов и лимитов на их размещение',
                        'xtype': 'gridcolumn'
                    }],
                    'flex': 1,
                    'menuDisabled': true,
                    'multisortable': false,
                    'rmsUid': 'b3e03958-19c0-4d5d-a4ac-c676ecd7aa6a',
                    'sortable': false,
                    'text': 'Государственный надзор в области обращения с отходами',
                    'xtype': 'gridcolumn'
                }],
                'flex': 1,
                'menuDisabled': true,
                'multisortable': false,
                'rmsUid': '8b3e4ffa-6a6d-4848-bb08-378a908631f9',
                'sortable': false,
                'text': 'Объекты регионального надзора',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'ptype': 'gridExportExcel'
            }]
        });
        me.callParent(arguments);
    }
});