Ext.define('B4.view.ZemelNyemUchastkiEditor_Copy_2', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-zemelnyemuchastkieditor_copy_2',
    title: 'Форма редактирования Земельные участки - Копия - 2',
    rmsUid: '76f1064a-31af-47ca-acac-4c57b4e9e46e',
    requires: [
        'B4.form.PickerField',
        'B4.model.KategoriiZemelEditorModel',
        'B4.model.KategoriiZemelListModel',
        'B4.model.ReestryEditorModel',
        'B4.model.ReestryListModel',
        'B4.model.SubEktyPrava',
        'B4.model.VidPravaNaZemljuEditorModel',
        'B4.model.VidPravaNaZemljuListModel',
        'B4.model.VRIZUEditorModel',
        'B4.model.VRIZUListModel',
        'B4.view.KategoriiZemelList',
        'B4.view.ReestryList',
        'B4.view.VidPravaNaZemljuList',
        'B4.view.VRIZUList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'ZemelNyemUchastki_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'ZemelNyemUchastki_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'type',
        'modelProperty': 'ZemelNyemUchastki_type',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'vid_prava',
        'modelProperty': 'ZemelNyemUchastki_vid_prava',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'category',
        'modelProperty': 'ZemelNyemUchastki_category',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'vid_use',
        'modelProperty': 'ZemelNyemUchastki_vid_use',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'owner_id',
        'modelProperty': 'ZemelNyemUchastki_owner_id',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'holder_id',
        'modelProperty': 'ZemelNyemUchastki_holder_id',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'area',
        'modelProperty': 'ZemelNyemUchastki_area',
        'type': 'NumberField'
    }, {
        'dataIndex': 'naznach',
        'modelProperty': 'ZemelNyemUchastki_naznach',
        'type': 'TextField'
    }, {
        'dataIndex': 'MapID',
        'modelProperty': 'ZemelNyemUchastki_MapID',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '76f1064a-31af-47ca-acac-4c57b4e9e46e-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ZemelNyemUchastkiEditor_Copy_2-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 2,
                'items': [{
                    'anchor': '100%',
                    'fieldLabel': 'Просмотр на карте',
                    'margin': '9 0 0 0',
                    'rmsUid': '7858648c-c063-4add-8397-6003ca565e84',
                    'text': 'Просмотр на карте',
                    'xtype': 'button'
                }],
                'layout': 'anchor',
                'rmsUid': 'a72c8188-5926-49f7-9210-c357c6ffb756',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'cc16ae9b-815c-4a41-a02c-22397f1909c9',
            'xtype': 'container'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'margin': '5 5 5 5',
            'modelProperty': 'ZemelNyemUchastki_name',
            'name': 'name',
            'rmsUid': '1600298e-e3b3-4eb1-a949-6c312d45c8c7',
            'xtype': 'textfield'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Панель закладок',
            'items': [{
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Основные сведения',
                'height': 300,
                'items': [{
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Контейнер',
                    'flex': 2,
                    'margin': '5 5 5 5',
                    'rmsUid': 'cbc0e309-beda-43ef-bcf5-8299e84a4fbe',
                    'title': 'Контейнер',
                    'xtype': 'container'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Контейнер',
                    'flex': 1,
                    'items': [{
                        'allowBlank': true,
                        'editable': false,
                        'fieldLabel': 'Тип объекта права',
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'listView': 'B4.view.ReestryList',
                        'listViewCtl': 'B4.controller.ReestryList',
                        'maximizable': true,
                        'model': 'B4.model.ReestryListModel',
                        'modelProperty': 'ZemelNyemUchastki_type',
                        'name': 'type',
                        'rmsUid': 'fe9c8b5d-f4f8-4cad-88a5-fdde9bba26b4',
                        'textProperty': 'code',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Тип объекта права',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }, {
                        'allowBlank': true,
                        'editable': false,
                        'fieldLabel': 'Вид права на землю',
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'listView': 'B4.view.VidPravaNaZemljuList',
                        'listViewCtl': 'B4.controller.VidPravaNaZemljuList',
                        'margin': '0 0 0 10',
                        'maximizable': true,
                        'model': 'B4.model.VidPravaNaZemljuListModel',
                        'modelProperty': 'ZemelNyemUchastki_vid_prava',
                        'name': 'vid_prava',
                        'rmsUid': '6fad93ad-4b23-4027-a246-bff66d3f1ebe',
                        'textProperty': 'code',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Вид права на землю',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }, {
                        'allowBlank': true,
                        'editable': false,
                        'fieldLabel': 'Категория земель',
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'listView': 'B4.view.KategoriiZemelList',
                        'listViewCtl': 'B4.controller.KategoriiZemelList',
                        'maximizable': true,
                        'model': 'B4.model.KategoriiZemelListModel',
                        'modelProperty': 'ZemelNyemUchastki_category',
                        'name': 'category',
                        'rmsUid': 'e03ecb19-2d53-4936-ba14-ba5dc8168ecf',
                        'textProperty': 'code',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Категория земель',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }, {
                        'allowBlank': true,
                        'editable': false,
                        'fieldLabel': 'Вид разрешенного использования',
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'listView': 'B4.view.VRIZUList',
                        'listViewCtl': 'B4.controller.VRIZUList',
                        'margin': '0 0 0 10',
                        'maximizable': true,
                        'model': 'B4.model.VRIZUListModel',
                        'modelProperty': 'ZemelNyemUchastki_vid_use',
                        'name': 'vid_use',
                        'rmsUid': '0f81f76d-cf10-410f-9d45-e306895e2c8b',
                        'textProperty': 'code',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Вид разрешенного использования',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }, {
                        'allowBlank': true,
                        'decimalPrecision': 2,
                        'fieldLabel': 'Общая площадь, кв. м',
                        'minValue': 0,
                        'modelProperty': 'ZemelNyemUchastki_area',
                        'name': 'area',
                        'rmsUid': 'ccaa5c61-c3a3-4cba-989a-4c8c926051e5',
                        'step': 1,
                        'width': 200,
                        'xtype': 'numberfield'
                    }],
                    'layout': {
                        'type': 'column'
                    },
                    'margin': '5 5 5 5',
                    'rmsUid': 'ffc434a6-019a-4282-9709-ccb4af3d5f8c',
                    'title': 'Контейнер',
                    'xtype': 'container'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Контейнер',
                    'items': [{
                        'allowBlank': true,
                        'fieldLabel': 'Назначение',
                        'modelProperty': 'ZemelNyemUchastki_naznach',
                        'name': 'naznach',
                        'rmsUid': '3f7d93c4-f331-440e-95b4-f7ea85c3c465',
                        'xtype': 'textfield'
                    }],
                    'layout': {
                        'type': 'fit'
                    },
                    'margin': '15 5 5 5',
                    'rmsUid': '55ec9340-ad2f-4bff-8dec-97035344f293',
                    'title': 'Контейнер',
                    'xtype': 'container'
                }, {
                    'allowBlank': true,
                    'allowDecimals': false,
                    'anchor': '100%',
                    'decimalPrecision': 2,
                    'fieldLabel': 'Идентификатор ГИС',
                    'minValue': 0,
                    'modelProperty': 'ZemelNyemUchastki_MapID',
                    'name': 'MapID',
                    'rmsUid': 'fa8bf04b-6c7a-456e-8f37-a832e2a302e9',
                    'step': 1,
                    'xtype': 'numberfield'
                }],
                'rmsUid': '7b17b25e-2488-4b69-bd4a-2c80fddd8b38',
                'title': 'Основные сведения',
                'xtype': 'container'
            }],
            'rmsUid': '06abd874-6aef-43c1-9840-7c12f6a37d08',
            'xtype': 'tabpanel'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'ZemelNyemUchastki_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});