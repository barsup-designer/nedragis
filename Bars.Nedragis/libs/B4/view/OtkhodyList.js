Ext.define('B4.view.OtkhodyList', {
    'alias': 'widget.rms-otkhodylist',
    'dataSourceUid': '07fadf5a-6010-4fa1-8cf6-39f3e5cf7590',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.OtkhodyListModel',
    'stateful': true,
    'title': 'Реестр Отходы',
    requires: [
        'B4.model.OtkhodyListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-OtkhodyEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-OtkhodyEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Element1507790021761',
                'dataIndexAbsoluteUid': 'FieldPath://07fadf5a-6010-4fa1-8cf6-39f3e5cf7590$f446c90f-b549-4d26-bbf4-82f8cc2ee71e',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2b331272-2065-4a0c-a9db-b74c467bf5f6',
                'sortable': true,
                'text': 'Наименование объекта образования отходов',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1507791662904',
                'dataIndexAbsoluteUid': 'FieldPath://07fadf5a-6010-4fa1-8cf6-39f3e5cf7590$5a616d20-8796-493e-b547-c004f80861d6',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0c17553a-ab41-4c28-9e60-b7f0d570600a',
                'sortable': true,
                'text': 'Местонахождение',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1507791696742',
                'dataIndexAbsoluteUid': 'FieldPath://07fadf5a-6010-4fa1-8cf6-39f3e5cf7590$2efd4c92-2535-447d-83cd-4d18c3c6a031',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'fa5ab7d5-6662-49ed-9fea-f952ee1305e7',
                'sortable': true,
                'text': 'Проект нормативов образования отходов и лимитов на их размещение',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});