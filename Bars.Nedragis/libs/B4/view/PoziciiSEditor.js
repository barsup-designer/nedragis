Ext.define('B4.view.PoziciiSEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-poziciiseditor',
    title: 'Редактирование Позиции спецификации',
    rmsUid: '77d87dcf-5697-47e8-bbfc-4eb054b9e7ae',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'PoziciiS_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'PoziciiS_name',
        'type': 'TextField'
    }, {
        'dataIndex': 's_pod',
        'modelProperty': 'PoziciiS_s_pod',
        'type': 'NumberField'
    }, {
        'dataIndex': 's_hran',
        'modelProperty': 'PoziciiS_s_hran',
        'type': 'NumberField'
    }, {
        'dataIndex': 'naklad',
        'modelProperty': 'PoziciiS_naklad',
        'type': 'NumberField'
    }, {
        'dataIndex': 'nds',
        'modelProperty': 'PoziciiS_nds',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '77d87dcf-5697-47e8-bbfc-4eb054b9e7ae-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'PoziciiSEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Идентификатор',
            'margin': '10 250 10 10',
            'modelProperty': 'f02dc9a6-e3af-4088-8e9f-21071ee2bbc2',
            'readOnly': true,
            'rmsUid': 'f02dc9a6-e3af-4088-8e9f-21071ee2bbc2',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'margin': '10 50 10 10',
            'modelProperty': 'PoziciiS_name',
            'name': 'name',
            'rmsUid': '24c5852e-6b66-42bc-84ea-58654626324f',
            'xtype': 'textfield'
        }, {
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Количество',
            'modelProperty': '797e2d99-574a-4d6c-99b2-74f088cc2c0c',
            'readOnly': true,
            'rmsUid': '797e2d99-574a-4d6c-99b2-74f088cc2c0c',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Стоимость подготовки',
            'hideTrigger': true,
            'margin': '10 280 10 10',
            'minValue': 0,
            'modelProperty': 'PoziciiS_s_pod',
            'name': 's_pod',
            'rmsUid': '5371c486-6d78-4158-8bda-66d23b3f4d86',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Стоимость хранения',
            'hideTrigger': true,
            'margin': '10 280 10 10',
            'minValue': 0,
            'modelProperty': 'PoziciiS_s_hran',
            'name': 's_hran',
            'rmsUid': '9bee8bee-9dbc-44a8-8e80-aaa9cf46dcb6',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Накладные расходы',
            'hideTrigger': true,
            'margin': '10 280 10 10',
            'minValue': 0,
            'modelProperty': 'PoziciiS_naklad',
            'name': 'naklad',
            'rmsUid': 'e336855a-3007-4724-a5c9-d25a90e4276a',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'НДС',
            'hideTrigger': true,
            'margin': '10 280 10 10',
            'minValue': 0,
            'modelProperty': 'PoziciiS_nds',
            'name': 'nds',
            'rmsUid': '85f559cb-37e1-4ebb-a35f-6cf9bb9ce56a',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'PoziciiS_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});