Ext.define('B4.view.VidOgranichenijaList', {
    'alias': 'widget.rms-vidogranichenijalist',
    'dataSourceUid': '819d41cc-49ef-46e6-8c2d-b426986e0144',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.VidOgranichenijaListModel',
    'stateful': true,
    'title': 'Реестр Вид ограничения',
    requires: [
        'B4.model.VidOgranichenijaListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-VidOgranichenijaEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-VidOgranichenijaEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://819d41cc-49ef-46e6-8c2d-b426986e0144$a98c533d-9824-443f-a9b9-a60baff91fcc',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a5c90aea-3f1f-4234-955f-844b3ffad210',
                'sortable': true,
                'text': 'Код',
                'width': 100,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name1',
                'dataIndexAbsoluteUid': 'FieldPath://819d41cc-49ef-46e6-8c2d-b426986e0144$67cfc38b-90d4-40ab-8144-1201f1dcaa54',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'deec6ecf-da61-4218-99e4-e491f13517c5',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});