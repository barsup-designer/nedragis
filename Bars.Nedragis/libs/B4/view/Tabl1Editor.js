Ext.define('B4.view.Tabl1Editor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-tabl1editor',
    title: 'Форма редактирования табл1',
    rmsUid: '3ebdf055-7783-45ef-9cf0-fa4ba05c2b6e',
    requires: [
        'B4.enums.VidRabot',
        'B4.form.EnumCombo',
        'B4.form.PickerField',
        'B4.model.VidPravaEditorModel',
        'B4.model.VidPravaListModel',
        'B4.view.VidPravaList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Tabl1_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'vid_r',
        'modelProperty': 'Tabl1_vid_r',
        'type': 'DropdownField'
    }, {
        'dataIndex': 'count',
        'modelProperty': 'Tabl1_count',
        'type': 'NumberField'
    }, {
        'dataIndex': 'rowid',
        'modelProperty': 'Tabl1_rowid',
        'type': 'NumberField'
    }, {
        'dataIndex': 'sum_DS',
        'modelProperty': 'Tabl1_sum_DS',
        'type': 'NumberField'
    }, {
        'dataIndex': 'kok',
        'modelProperty': 'Tabl1_kok',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '3ebdf055-7783-45ef-9cf0-fa4ba05c2b6e-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'Tabl1Editor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': false,
            'anchor': '100%',
            'enumName': 'B4.enums.VidRabot',
            'fieldLabel': 'Вид работ',
            'modelProperty': 'Tabl1_vid_r',
            'name': 'vid_r',
            'queryMode': 'local',
            'rmsUid': 'f6fac313-b74e-41d4-a21b-6393f0390350',
            'xtype': 'b4enumcombo'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'count',
            'hideTrigger': true,
            'jjj': '20',
            'minValue': 0,
            'modelProperty': 'Tabl1_count',
            'name': 'count',
            'P1453877335765': null,
            'rmsUid': '6cb9d256-9031-4f1e-ad68-9e4e38116768',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'rowid',
            'hideTrigger': true,
            'minValue': 0,
            'modelProperty': 'Tabl1_rowid',
            'name': 'rowid',
            'rmsUid': '238348b3-3a2c-4c6e-b1ad-1cb9da5b6d9a',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'sum_DS',
            'hideTrigger': true,
            'minValue': 0,
            'modelProperty': 'Tabl1_sum_DS',
            'name': 'sum_DS',
            'rmsUid': '59266e1e-aba1-47ac-a104-d1dd8d1359c3',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Идентификатор',
            'modelProperty': '2d591211-b17d-4d36-86db-31c4b306be4d',
            'readOnly': true,
            'rmsUid': '2d591211-b17d-4d36-86db-31c4b306be4d',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'kok',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.VidPravaList',
            'listViewCtl': 'B4.controller.VidPravaList',
            'maximizable': true,
            'model': 'B4.model.VidPravaListModel',
            'modelProperty': 'Tabl1_kok',
            'name': 'kok',
            'rmsUid': '4156e5e2-e453-4e82-958e-5b8dffdd0422',
            'textProperty': 'code',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'kok',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Tabl1_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});