Ext.define('B4.view.RukovoditeliList', {
    'alias': 'widget.rms-rukovoditelilist',
    'dataSourceUid': 'cf26c7ab-6cf7-49ad-888a-7fb500a92d49',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.RukovoditeliListModel',
    'stateful': true,
    'title': 'Реестр Руководители',
    requires: [
        'B4.model.RukovoditeliListModel',
        'B4.ux.grid.plugin.DataExport',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-RukovoditeliEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-RukovoditeliEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }, {
        'handler': function() {
            me = this.up('rms-rukovoditelilist'); /*br*/
            if (!me.getSelectionModel().lastSelected) /*br*/
            return; /*br*/
            var formData = new FormData; /*br*/
            formData.append('id', me.getSelectionModel().lastSelected.raw.Id); /*br*/
            formData.append('registry', 'Руководители'); /*br*/
            $.ajax({ /*br*/
                url: 'Copy/CopyRaw',
                /*br*/
                data: formData,
                /*br*/
                processData: false,
                /*br*/
                contentType: false,
                /*br*/
                type: 'POST',
                /*br*/
                success: function(respData) { /*br*/
                    me.store.reload(); /*br*/
                },
                /*br*/
                error: function(xhr, status, data) { /*br*/
                    /*br*/
                } /*br*/
            });
        },
        'hidden': false,
        'iconCls': 'content-img-icons-page_copy-png',
        'itemId': 'Custom',
        'rmsUid': null,
        'text': 'Копировать строку'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Surname',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$8b8bb5a0-c6eb-4d45-a0c7-9dd5a8e7e78b',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'fe4bd458-1449-4a8e-b301-497015e534e6',
                'sortable': true,
                'text': 'Фамилия',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'FirstName',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$79d3cf0f-cffa-4856-80b6-405056ed5837',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '26c5338a-5129-4e1f-a215-7496fdd8e738',
                'sortable': true,
                'text': 'Имя',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Patronymic',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$9a9a3d4b-9609-4819-9d7c-4bdefbee63be',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'c622bf44-542f-4fbd-a798-14749cba1800',
                'sortable': true,
                'text': 'Отчество',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'PostType_name1',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$27df2a9e-b98e-4c89-8744-3bae756f275f$9b116f06-db34-48a5-a6ea-feb1c59effc8',
                'dataIndexRelativePath': 'PostType.name1',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b2610f51-b373-4994-b2f7-f82e6f394e0b',
                'sortable': true,
                'text': 'Тип должности',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'PostText',
                'dataIndexAbsoluteUid': 'FieldPath://cf26c7ab-6cf7-49ad-888a-7fb500a92d49$f892df1d-562c-42a9-8f14-3a153fd3f9d5',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '3c62f88d-8722-4763-bd17-830eeee56b11',
                'sortable': true,
                'text': 'Должность',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }, {
                'ptype': 'gridDataExport',
                'reportTitle': 'Реестр Руководители'
            }]
        });
        me.callParent(arguments);
    }
});