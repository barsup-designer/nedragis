Ext.define('B4.view.LicUchastkiEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-licuchastkieditor',
    title: 'Редактирование Лицензионные участки',
    rmsUid: '420d1621-1a7f-404d-99b6-fa3b4a6b0347',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'LicUchastki_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'LicUchastki_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478588881912',
        'modelProperty': 'LicUchastki_Element1478588881912',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478589271306',
        'modelProperty': 'LicUchastki_Element1478589271306',
        'type': 'TextAreaField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '420d1621-1a7f-404d-99b6-fa3b4a6b0347-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'LicUchastkiEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Идентификатор',
            'margin': '10 250 10 10',
            'modelProperty': '063a338d-4b50-4477-b9b8-e07d770080fa',
            'readOnly': true,
            'rmsUid': '063a338d-4b50-4477-b9b8-e07d770080fa',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'margin': '10 10 10 10',
            'modelProperty': 'LicUchastki_name',
            'name': 'name',
            'rmsUid': '6c50e5c7-b546-4b2a-b9a0-e648de888252',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Номер лицензии',
            'margin': '10 200 10 10',
            'modelProperty': 'LicUchastki_Element1478588881912',
            'name': 'Element1478588881912',
            'rmsUid': '9ff263a0-5e66-41c0-bcd2-04b44fd1f827',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Описание расположение участка',
            'labelAlign': 'top',
            'margin': '10 10 10 10',
            'modelProperty': 'LicUchastki_Element1478589271306',
            'name': 'Element1478589271306',
            'rmsUid': '68b898cd-db15-4f1a-8a54-10ab6b615f16',
            'xtype': 'textarea'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'LicUchastki_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});