Ext.define('B4.view.TipyObEktaPravaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-tipyobektapravaeditor',
    title: 'Форма редактирования Типы объекта права',
    rmsUid: '0f659b89-1171-4d59-9ee0-edcda09c5044',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'TipyObEktaPrava_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'TipyObEktaPrava_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'TipyObEktaPrava_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '0f659b89-1171-4d59-9ee0-edcda09c5044-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'TipyObEktaPravaEditor-container',
        'flex': 1,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'margin': '10 10 10 10',
            'modelProperty': 'TipyObEktaPrava_code',
            'name': 'code',
            'rmsUid': '74d701da-ea66-4f91-8e57-9d58b12e2b7a',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'margin': '10 10 10 10',
            'modelProperty': 'TipyObEktaPrava_name',
            'name': 'name',
            'rmsUid': '695b00ee-a611-4a42-a3d0-fdb8a881b22e',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'TipyObEktaPrava_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});