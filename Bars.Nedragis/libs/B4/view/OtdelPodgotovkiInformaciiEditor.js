Ext.define('B4.view.OtdelPodgotovkiInformaciiEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-otdelpodgotovkiinformaciieditor',
    title: 'Форма редактирования Отдел подготовки информации',
    rmsUid: 'dc3b457d-7646-4047-979a-9d37d1e8d355',
    requires: [
        'B4.form.PickerField',
        'B4.model.Sotrudniki1EditorModel',
        'B4.model.Sotrudniki1ListModel',
        'B4.model.VidRabotOPINFEditorModel',
        'B4.model.VidRabotOPINFListModel',
        'B4.view.Sotrudniki1List',
        'B4.view.VidRabotOPINFList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'OtdelPodgotovkiInformacii_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1510134896283',
        'modelProperty': 'OtdelPodgotovkiInformacii_Element1510134896283',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1510135000178',
        'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135000178',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Element1510135042374',
        'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135042374',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Element1510135074618',
        'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135074618',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1510135121705',
        'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135121705',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Element1510135249843',
        'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135249843',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1510135325790',
        'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135325790',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1510135360701',
        'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135360701',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1510135484012',
        'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135484012',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1510135527140',
        'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135527140',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'dc3b457d-7646-4047-979a-9d37d1e8d355-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OtdelPodgotovkiInformaciiEditor-container',
        'layout': {
            'type': 'anchor'
        },
        'margin': '10 10 0 10',
        'padding': '20 0 0 0',
        items: [{
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '50%',
            'decimalPrecision': 2,
            'fieldLabel': 'Номер п/п',
            'margin': '0 0 20 0',
            'minValue': 0,
            'modelProperty': 'OtdelPodgotovkiInformacii_Element1510134896283',
            'name': 'Element1510134896283',
            'rmsUid': '83e05413-f63c-4a62-9802-8cb0e6f793b5',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Вид работ',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.VidRabotOPINFList',
            'listViewCtl': 'B4.controller.VidRabotOPINFList',
            'margin': '0 0 20 0',
            'maximizable': true,
            'model': 'B4.model.VidRabotOPINFListModel',
            'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135000178',
            'name': 'Element1510135000178',
            'rmsUid': 'f0219ff1-7c11-4bdf-8741-305b62aac65b',
            'textProperty': 'VidRabotOPINF222',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Вид работ',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Подвид работ',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.VidRabotOPINFList',
            'listViewCtl': 'B4.controller.VidRabotOPINFList',
            'maximizable': true,
            'model': 'B4.model.VidRabotOPINFListModel',
            'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135042374',
            'name': 'Element1510135042374',
            'rmsUid': '3baf6263-59b3-427b-a07b-a2036442bbc3',
            'textProperty': 'VidRabotOPINF222',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Подвид работ',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Источник поступления информации',
            'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135074618',
            'name': 'Element1510135074618',
            'rmsUid': '6e605d94-436c-494b-b388-52ee1ddc59ba',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'editable': false,
            'fieldLabel': 'Исполнитель',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.Sotrudniki1List',
            'listViewCtl': 'B4.controller.Sotrudniki1List',
            'margin': '0 0 15 0',
            'maximizable': true,
            'model': 'B4.model.Sotrudniki1ListModel',
            'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135121705',
            'name': 'Element1510135121705',
            'rmsUid': 'c6349342-e06a-40c9-9713-424da4cf61ff',
            'textProperty': 'Element1474352136232',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Исполнитель',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '50%',
            'decimalPrecision': 2,
            'fieldLabel': 'Количество заявок',
            'minValue': 0,
            'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135249843',
            'name': 'Element1510135249843',
            'rmsUid': 'f93dfa7e-92aa-4fe1-a1e7-2b19079dc3d5',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '50%',
            'decimalPrecision': 2,
            'fieldLabel': 'Количество листов',
            'minValue': 0,
            'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135325790',
            'name': 'Element1510135325790',
            'rmsUid': '1046c2c4-9b43-4832-ad34-80c5e3509ffb',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'fieldLabel': 'Номер документа',
            'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135360701',
            'name': 'Element1510135360701',
            'rmsUid': '33ca0a25-54b8-4260-ac5d-c94a44aa56c5',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '50%',
            'decimalPrecision': 2,
            'fieldLabel': 'Количество папок',
            'minValue': 0,
            'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135484012',
            'name': 'Element1510135484012',
            'rmsUid': '83719061-0f1b-4d77-bd3b-72e136666f97',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '50%',
            'decimalPrecision': 2,
            'fieldLabel': 'Количество файлов',
            'minValue': 0,
            'modelProperty': 'OtdelPodgotovkiInformacii_Element1510135527140',
            'name': 'Element1510135527140',
            'rmsUid': '6ebad3f5-57c1-4a86-b66a-0338d1f731cc',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'OtdelPodgotovkiInformacii_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1510134896283'));
        } else {}
        return res;
    },
});