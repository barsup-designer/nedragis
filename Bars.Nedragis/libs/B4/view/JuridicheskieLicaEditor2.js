Ext.define('B4.view.JuridicheskieLicaEditor2', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-juridicheskielicaeditor2',
    title: 'ЮЛ Добавление',
    rmsUid: 'a0c97a59-b5de-4236-9d1f-49b5f005deab',
    requires: [
        'B4.form.FiasSelectAddress',
        'B4.form.PickerField',
        'B4.model.OtraslEditorModel',
        'B4.model.OtraslListModel',
        'B4.view.OtraslList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'JuridicheskieLica_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'INN1',
        'modelProperty': 'JuridicheskieLica_INN1',
        'type': 'TextField'
    }, {
        'dataIndex': 'KPP1',
        'modelProperty': 'JuridicheskieLica_KPP1',
        'type': 'TextField'
    }, {
        'dataIndex': 'Representation',
        'modelProperty': 'JuridicheskieLica_Representation',
        'type': 'TextField'
    }, {
        'dataIndex': 'Fullname',
        'modelProperty': 'JuridicheskieLica_Fullname',
        'type': 'TextField'
    }, {
        'dataIndex': 'Sphere',
        'modelProperty': 'JuridicheskieLica_Sphere',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'LegalAddress',
        'isAddress': true,
        'modelProperty': 'JuridicheskieLica_LegalAddress',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'a0c97a59-b5de-4236-9d1f-49b5f005deab-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'JuridicheskieLicaEditor2-container',
        'flex': 1,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'fieldLabel': 'ИНН',
                'flex': 1,
                'margin': '10 10 0 10',
                'maxLength': 10,
                'modelProperty': 'JuridicheskieLica_INN1',
                'name': 'INN1',
                'rmsUid': 'ee093447-7d50-459c-9419-e5c195fec1df',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'КПП',
                'flex': 1,
                'margin': '10 10 0 10',
                'maxLength': 9,
                'modelProperty': 'JuridicheskieLica_KPP1',
                'name': 'KPP1',
                'rmsUid': 'ee06b6ca-f1f7-498d-9161-ef32f8a05538',
                'xtype': 'textfield'
            }],
            'layout': {
                'align': 'stretch',
                'type': 'hbox'
            },
            'margin': '10 0 10 0',
            'region': 'North',
            'rmsUid': 'eec0ccb0-a995-41a5-9c4e-f7a572d75240',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'fieldLabel': 'Краткое наименование',
                'margin': '10 10 0 10',
                'modelProperty': 'JuridicheskieLica_Representation',
                'name': 'Representation',
                'rmsUid': 'e0a03cbc-809a-45d0-ac6a-451ea1f16e00',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Полное наименование',
                'height': 45,
                'margin': '10 10 10 10',
                'modelProperty': 'JuridicheskieLica_Fullname',
                'name': 'Fullname',
                'rmsUid': '058b1cb7-d4eb-4c12-bc7b-bbe80dbfd0b6',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Отрасль',
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.OtraslList',
                'listViewCtl': 'B4.controller.OtraslList',
                'margin': '10 10 0 10',
                'maximizable': true,
                'model': 'B4.model.OtraslListModel',
                'modelProperty': 'JuridicheskieLica_Sphere',
                'name': 'Sphere',
                'rmsUid': 'a1e595f4-eca6-4cf3-b6e1-248c1535a803',
                'textProperty': 'code',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Отрасль',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }, {
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Юридический адрес',
                'idProperty': 'Id',
                'marginLeft': 10,
                'marginRight': 10,
                'marginTop': 10,
                'maximizable': true,
                'model': 'B4.model.FiasAddress',
                'modelProperty': 'JuridicheskieLica_LegalAddress',
                'name': 'LegalAddress',
                'rmsUid': 'fca0fdea-ce27-4e2d-b2dd-103bf93c7ccf',
                'typeAhead': false,
                'xtype': 'b4fiasselectaddress'
            }],
            'layout': {
                'type': 'fit'
            },
            'rmsUid': '12bbc3d5-b63a-4e27-8c55-f7893862da87',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'JuridicheskieLica_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {
            me.setTitle('Добавление юридического лица');
        }
        return res;
    },
});