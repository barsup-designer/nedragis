Ext.define('B4.view.FormaRedaktirovanijaObEktov', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-formaredaktirovanijaobektov',
    title: 'Форма редактирования объектов',
    rmsUid: '7b9141d5-2d9e-4baf-b298-144af3ad4e90',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'NaimenovanieObEktovNadzora_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1507203167462',
        'modelProperty': 'NaimenovanieObEktovNadzora_Element1507203167462',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '7b9141d5-2d9e-4baf-b298-144af3ad4e90-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'FormaRedaktirovanijaObEktov-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'modelProperty': 'NaimenovanieObEktovNadzora_Element1507203167462',
            'name': 'Element1507203167462',
            'rmsUid': 'f2e3a876-3f28-4696-afce-e84fa55df772',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'NaimenovanieObEktovNadzora_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});