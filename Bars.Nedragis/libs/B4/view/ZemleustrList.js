Ext.define('B4.view.ZemleustrList', {
    'alias': 'widget.rms-zemleustrlist',
    'dataSourceUid': 'f43e2a1e-85d1-44ff-8cb6-87595813d7ad',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.ZemleustrListModel',
    'stateful': true,
    'title': 'Реестр Землеустройство',
    requires: [
        'B4.model.ZemleustrListModel',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-ZemleustrEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-ZemleustrEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'name_z_NAME_SP',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$f0c7fc6d-bb89-4362-ba18-51ab9d589ccf$c9cfc806-3d3e-4aa7-b12c-d9218426a9d7',
                'dataIndexRelativePath': 'name_z.NAME_SP',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0cc30b7b-524a-418a-ac67-ae8599f8cc84',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование отдела.Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'NDelo',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$faa81b6d-44b1-4aa9-85e7-c855125d9094',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '740c2dbb-1a61-4421-a334-c121c9ee05bb',
                'sortable': true,
                'text': 'Номер дела/проекта',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'vid_rabot',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$c378de4a-8343-4bc9-be6e-fa791eca7e8e',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0212807e-c452-4566-af99-68ae71090020',
                'sortable': true,
                'text': 'Вид работ',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'komu',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$75df8dc4-d003-4f45-b9e7-1ee24f23357c',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '622bfdcb-1048-4e76-8682-7c8c496ee013',
                'sortable': true,
                'text': 'Кому',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'company',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$6b25f7c0-c1eb-4c77-abe0-8f98b8a78c42',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2c28d7c1-e254-42fd-8b28-fcd814ff37bb',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Компания',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name_obj',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$260f0832-e300-4378-8428-c84e39ac3ce5',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a1a06697-1004-4cef-823a-3270f217434a',
                'sortable': true,
                'text': 'Название объекта',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'kad_nom',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$7cd74c40-7379-49e9-86fe-74230954e494',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '57ea18e6-862a-4704-bb4b-606f4a3956d7',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Кадастровый номер',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'kateg_zem',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$ae229ccf-f963-4bae-80c6-d1770c88814e',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'cdd430f2-6e2e-4063-b007-b597f669094a',
                'sortable': true,
                'text': 'Исходная категория земель',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'konech_kategor',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$5b06f3cc-ddf8-427c-aaab-10e3ed9911be',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0e68e16c-be2d-4d95-8957-5fa0aeba9cf1',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Конечная категория земель',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'square',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$44abd96b-8d29-4d0a-9fc2-74fef0ae2f5b',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0.000',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '785eacd1-4390-4959-9ce7-3bcbc4236bd7',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Площадь',
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'region_name',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$0fd97ee2-7dff-4b7e-8f14-ae335f2ee16f$8808232e-7586-4c64-b5ce-c8e0d8fa30c2',
                'dataIndexRelativePath': 'region.name',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'ac26815a-2f65-4ef3-b53a-159dab538849',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Район.Наименование района',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'date_postupl',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$b8d2eb9c-e90e-4f6b-adc1-34cb4dacb0cc',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'fcb2362d-ad0f-4868-9b53-3703f52ac6c2',
                'sortable': true,
                'text': 'Дата поступления в ГКУ Ресурсы Ямала',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Etap',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$36988660-bfcb-4925-9395-92cc696d745a',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '84212c18-5adb-4f7d-8786-ca5b71ad2f03',
                'sortable': true,
                'text': 'Этап',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'date_vh_doc',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$3a98587e-b435-4545-a235-379ea3682772',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '94e6859f-0459-4c63-bb9a-776066c291a3',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата входящего документа',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'nom_vh_doc',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$651adb48-4570-4819-bd23-290913a02d1d',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2508b416-f63f-44ec-9ec1-4c949be7c0d4',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер входящего документа',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'autor_Element1474352136232',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$4d8b55f7-d0da-46b3-aabc-6ef35c082650$5c9e0c3e-7dcb-483d-89fd-a12dacaa7279',
                'dataIndexRelativePath': 'autor.Element1474352136232',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '126e09ab-a2ad-4a33-a0a8-3f172a6d9b75',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Автор(исполнитель).ФИО',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'nomer_pisma',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$ea1a36fe-5c60-4beb-b944-fe17156ffb31',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'c4b86861-70ee-46ff-a529-10ea9da527e7',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер письма заявителя',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'data_usp',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$8c776cd0-27a8-4d66-adcb-852c69c869bf',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '1098161c-9529-4e5b-949d-edc0b6fbbec6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата исполнения',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'ispol',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$09a0f52c-82e4-4d7a-8427-8a654fea8d1e',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'e22db2c1-cfa6-44dc-b540-81264d09f0b5',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Исполнитель',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'format',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$4dcb9882-85e7-483a-b9b5-cb4963db8138',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2dffe708-3ba9-48f6-bc32-bc8863077c1a',
                'sortable': true,
                'text': 'Формат данных',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'kolvo_ed',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$e38204d2-f9c5-4003-8c4a-af275a04388b',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '907c6041-e98d-44a1-86b9-99fb77710992',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Количество единиц',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'prim',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$41bb7d7b-97ee-4fcb-8b76-0a406c35644c',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a50d5528-2f57-4b95-8180-063b2b02c9cf',
                'sortable': true,
                'text': 'Примечание',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'sum_kalk',
                'dataIndexAbsoluteUid': 'FieldPath://f43e2a1e-85d1-44ff-8cb6-87595813d7ad$eda1f75f-1955-4812-add0-edd6f7653c9a',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0.000',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'fdf34899-193a-47ef-8450-7955f1a4290d',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Сумма калькуляции',
                'xtype': 'numbercolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }]
        });
        me.callParent(arguments);
    }
});