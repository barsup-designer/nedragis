Ext.define('B4.view.NedropolZovanieEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-nedropolzovanieeditor',
    title: 'Форма редактирования Недропользование',
    rmsUid: 'f6940730-dfdf-4248-9827-e0aa0837dc71',
    requires: [
        'B4.form.PickerField',
        'B4.model.Otdel1EditorModel',
        'B4.model.Otdel1ListModel',
        'B4.model.Sotrudniki1EditorModel',
        'B4.model.Sotrudniki1ListModel',
        'B4.view.Otdel1List',
        'B4.view.Sotrudniki1List'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'NedropolZovanie_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name_n',
        'modelProperty': 'NedropolZovanie_name_n',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'ObjectCreateDate',
        'modelProperty': 'NedropolZovanie_ObjectCreateDate',
        'type': 'DateField'
    }, {
        'dataIndex': 'vid_n',
        'modelProperty': 'NedropolZovanie_vid_n',
        'type': 'TextField'
    }, {
        'dataIndex': 'podvid',
        'modelProperty': 'NedropolZovanie_podvid',
        'type': 'TextField'
    }, {
        'dataIndex': 'col_n',
        'modelProperty': 'NedropolZovanie_col_n',
        'type': 'NumberField'
    }, {
        'dataIndex': 'opis_n',
        'modelProperty': 'NedropolZovanie_opis_n',
        'type': 'TextField'
    }, {
        'dataIndex': 'obr_n',
        'modelProperty': 'NedropolZovanie_obr_n',
        'type': 'TextField'
    }, {
        'dataIndex': 'zakazchik',
        'modelProperty': 'NedropolZovanie_zakazchik',
        'type': 'TextField'
    }, {
        'dataIndex': 'zakaz_n',
        'modelProperty': 'NedropolZovanie_zakaz_n',
        'type': 'TextField'
    }, {
        'dataIndex': 'harakter_n',
        'modelProperty': 'NedropolZovanie_harakter_n',
        'type': 'TextField'
    }, {
        'dataIndex': 'sotr_n',
        'modelProperty': 'NedropolZovanie_sotr_n',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'organ',
        'modelProperty': 'NedropolZovanie_organ',
        'type': 'TextField'
    }, {
        'dataIndex': 'chas_n',
        'modelProperty': 'NedropolZovanie_chas_n',
        'type': 'NumberField'
    }, {
        'dataIndex': 'stoumost',
        'modelProperty': 'NedropolZovanie_stoumost',
        'type': 'NumberField'
    }, {
        'dataIndex': 'oplata_n',
        'modelProperty': 'NedropolZovanie_oplata_n',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'f6940730-dfdf-4248-9827-e0aa0837dc71-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'NedropolZovanieEditor-container',
        'autoScroll': true,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Наименование отдела',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.Otdel1List',
            'listViewCtl': 'B4.controller.Otdel1List',
            'maximizable': true,
            'model': 'B4.model.Otdel1ListModel',
            'modelProperty': 'NedropolZovanie_name_n',
            'name': 'name_n',
            'rmsUid': 'cb51a4dd-f192-4f22-b74a-bec2ba54d1a5',
            'textProperty': 'NAME_SP',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Наименование отдела',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': false,
            'anchor': '100%',
            'fieldLabel': 'Дата занесения информации',
            'format': 'd.m.Y',
            'modelProperty': 'NedropolZovanie_ObjectCreateDate',
            'name': 'ObjectCreateDate',
            'rmsUid': '684927e5-9a22-422e-b710-4b06e1fba2d4',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Вид работ',
            'modelProperty': 'NedropolZovanie_vid_n',
            'name': 'vid_n',
            'rmsUid': 'b5c93108-5fa8-4ade-93f8-e20cd6890b27',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Подвид работ',
            'modelProperty': 'NedropolZovanie_podvid',
            'name': 'podvid',
            'rmsUid': '2bfac98e-cf67-48e0-828c-07cb83ed983a',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'Количество',
            'modelProperty': 'NedropolZovanie_col_n',
            'name': 'col_n',
            'rmsUid': '584f049e-ea3c-40c3-9d72-1d3360487d7f',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дополнительное описание',
            'modelProperty': 'NedropolZovanie_opis_n',
            'name': 'opis_n',
            'rmsUid': 'b27d2fde-3f79-4282-b928-6f2637490616',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Безвозмездное обращение',
            'modelProperty': 'NedropolZovanie_obr_n',
            'name': 'obr_n',
            'rmsUid': 'eaaa31af-cd31-4bac-8793-846d561c6cab',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Заказчик',
            'modelProperty': 'NedropolZovanie_zakazchik',
            'name': 'zakazchik',
            'rmsUid': 'cfab6d33-da56-456c-a527-090157df07d6',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дополнительная информация о заказчике',
            'modelProperty': 'NedropolZovanie_zakaz_n',
            'name': 'zakaz_n',
            'rmsUid': 'b048e57a-d007-428b-9360-6c01264d36a0',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Характер обращения',
            'modelProperty': 'NedropolZovanie_harakter_n',
            'name': 'harakter_n',
            'rmsUid': '080062b3-ef62-470c-b0f9-ee57369c8d34',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Исполнитель',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.Sotrudniki1List',
            'listViewCtl': 'B4.controller.Sotrudniki1List',
            'maximizable': true,
            'model': 'B4.model.Sotrudniki1ListModel',
            'modelProperty': 'NedropolZovanie_sotr_n',
            'name': 'sotr_n',
            'rmsUid': 'fd4b6919-0fad-408b-b6ad-403f0d105728',
            'textProperty': 'Element1474352136232',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Исполнитель',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Орган госвласти',
            'modelProperty': 'NedropolZovanie_organ',
            'name': 'organ',
            'rmsUid': '53097677-a096-483d-9856-a0526a2b0db9',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'fieldLabel': 'Количество затраченных человека часов',
            'modelProperty': 'NedropolZovanie_chas_n',
            'name': 'chas_n',
            'rmsUid': '4ae97157-9cd4-46ac-8762-ba229461d11e',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Стоимость по калькуляции руб.',
            'modelProperty': 'NedropolZovanie_stoumost',
            'name': 'stoumost',
            'rmsUid': 'e3562885-8dce-42c1-a0a3-83e959dcc1dc',
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Оплачено',
            'modelProperty': 'NedropolZovanie_oplata_n',
            'name': 'oplata_n',
            'rmsUid': '35c0bcdb-c697-44fa-9935-ffac08e10344',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'NedropolZovanie_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});