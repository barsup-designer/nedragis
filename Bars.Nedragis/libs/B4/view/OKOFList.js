Ext.define('B4.view.OKOFList', {
    'alias': 'widget.rms-okoflist',
    'dataSourceUid': '594403ac-d85f-4ef6-8815-bf4c01f3b7f9',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.OKOFListModel',
    'stateful': true,
    'title': 'Реестр ОКОФ',
    requires: [
        'B4.model.OKOFListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-OKOFEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-OKOFEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://594403ac-d85f-4ef6-8815-bf4c01f3b7f9$68c5f8ed-cccf-4b7a-9b3e-2ca04e22eb5f',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'cf61a6de-6ddb-4380-905f-17df526a103f',
                'sortable': true,
                'text': 'Код',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://594403ac-d85f-4ef6-8815-bf4c01f3b7f9$c50ac8dc-07e1-43a6-a3e2-43c64c9567c4',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '802894c0-020a-42d3-9e01-a9d0fc6fc4f0',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'check_number',
                'dataIndexAbsoluteUid': 'FieldPath://594403ac-d85f-4ef6-8815-bf4c01f3b7f9$dcf91b29-5256-4fd7-97e1-ecdb0a8a626d',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '50393dad-b698-4511-9105-6349ab9871c7',
                'sortable': true,
                'text': 'Контрольное число',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'clasifier_name',
                'dataIndexAbsoluteUid': 'FieldPath://594403ac-d85f-4ef6-8815-bf4c01f3b7f9$e267b6bd-c137-496e-87f7-295177a8b7ca$6cc00b9e-2a55-4b59-b182-8af607b62fa3',
                'dataIndexRelativePath': 'clasifier.name',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'cd73df12-2edf-42df-bb36-d39c0fd71728',
                'sortable': true,
                'text': 'Классификация.Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'parent_id_name',
                'dataIndexAbsoluteUid': 'FieldPath://594403ac-d85f-4ef6-8815-bf4c01f3b7f9$83cb6be4-8b49-4019-b660-32060ffeec63$c50ac8dc-07e1-43a6-a3e2-43c64c9567c4',
                'dataIndexRelativePath': 'parent_id.name',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a6920602-2be9-4731-b3c7-9b9ed1bfcb4c',
                'sortable': true,
                'text': 'Родительский элемент.Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});