Ext.define('B4.view.RegionalNyjjNadzorVodnykhObEktovList', {
    'alias': 'widget.rms-regionalnyjjnadzorvodnykhobektovlist',
    'dataSourceUid': '8d4cd756-d656-4caa-805b-242b484000f7',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.RegionalNyjjNadzorVodnykhObEktovListModel',
    'stateful': true,
    'title': 'Реестр Региональный надзор водных объектов',
    requires: [
        'B4.model.RegionalNyjjNadzorVodnykhObEktovListModel',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-RegionalNyjjNadzorVodnykhObEktovEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-RegionalNyjjNadzorVodnykhObEktovEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'reestrid13',
                'dataIndexAbsoluteUid': 'FieldPath://8d4cd756-d656-4caa-805b-242b484000f7$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'dataIndexRelativePath': 'Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b2cbf6a4-6bcd-4219-8c93-8bcb2c3f2f9a',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'dognumber13',
                'dataIndexAbsoluteUid': 'FieldPath://8d4cd756-d656-4caa-805b-242b484000f7$242c25a8-da1c-46f3-8483-03b694abeac9',
                'dataIndexRelativePath': 'Element1507787714216',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '36ab2186-5aed-4424-85c4-fcf00c7d8ff2',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер договора',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'dogdate13',
                'dataIndexAbsoluteUid': 'FieldPath://8d4cd756-d656-4caa-805b-242b484000f7$aefa3f0d-955d-44a4-9595-5cf81ebbbb9c',
                'dataIndexRelativePath': 'Element1507787771917',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'c4e980c2-44b1-494b-bfba-3da4c72662b5',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата регистрации',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'dogdateend13',
                'dataIndexAbsoluteUid': 'FieldPath://8d4cd756-d656-4caa-805b-242b484000f7$feab30c0-ce5b-4340-a322-854df625398b',
                'dataIndexRelativePath': 'Element1507787816822',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '62ac8c77-8e24-4e38-aa99-6112ed328f90',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Окончание срока действия',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'dogmesto13',
                'dataIndexAbsoluteUid': 'FieldPath://8d4cd756-d656-4caa-805b-242b484000f7$b9f05a6e-fd13-4cad-8a22-e517b56ec08f',
                'dataIndexRelativePath': 'Element1507787841624',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '4d195bd7-d123-44e7-b000-5be194aafd5d',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Местонахождение',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }]
        });
        me.callParent(arguments);
    }
});