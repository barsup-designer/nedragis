Ext.define('B4.view.TipyDokumentovEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-tipydokumentoveditor',
    title: 'Форма редактирования Типы документов',
    rmsUid: '60b4dc7f-e8b7-4707-9bbc-24467f8d612a',
    requires: [
        'B4.form.PickerField',
        'B4.model.TipyDokumentovEditorModel',
        'B4.model.TipyDokumentovListModel',
        'B4.view.TipyDokumentovList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'TipyDokumentov_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'TipyDokumentov_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'TipyDokumentov_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'parent_id',
        'modelProperty': 'TipyDokumentov_parent_id',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '60b4dc7f-e8b7-4707-9bbc-24467f8d612a-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'TipyDokumentovEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'TipyDokumentov_code',
            'name': 'code',
            'rmsUid': '6152f3c7-b38a-4e5e-a32c-c8a4dbd7ff64',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'TipyDokumentov_name',
            'name': 'name',
            'rmsUid': '4607ea52-599f-4d3c-bca2-4563bb02df1a',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'editable': false,
            'fieldLabel': 'Родительский элемент',
            'flex': 1,
            'height': 20,
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.TipyDokumentovList',
            'listViewCtl': 'B4.controller.TipyDokumentovList',
            'margin': '10 10 10 10',
            'maximizable': true,
            'model': 'B4.model.TipyDokumentovListModel',
            'modelProperty': 'TipyDokumentov_parent_id',
            'name': 'parent_id',
            'rmsUid': '56d4c204-1fcc-4741-9b6f-6628d48313e6',
            'textProperty': 'code',
            'typeAhead': false,
            'width': 10,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Родительский элемент',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'TipyDokumentov_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});