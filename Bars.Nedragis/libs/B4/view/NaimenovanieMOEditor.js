Ext.define('B4.view.NaimenovanieMOEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-naimenovaniemoeditor',
    title: 'Форма редактирования Наименование МО',
    rmsUid: '8f98ff79-ba93-4391-be7a-2d77977fccae',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'NaimenovanieMO_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1455707802552',
        'modelProperty': 'NaimenovanieMO_Element1455707802552',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '8f98ff79-ba93-4391-be7a-2d77977fccae-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'NaimenovanieMOEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'modelProperty': 'NaimenovanieMO_Element1455707802552',
            'name': 'Element1455707802552',
            'rmsUid': 'b063af24-173c-4e14-ab94-444be4ff1a39',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'NaimenovanieMO_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1455707802552'));
        } else {}
        return res;
    },
});