Ext.define('B4.view.PlatezhPoruchenieList', {
    'alias': 'widget.rms-platezhporuchenielist',
    'dataSourceUid': 'c719553b-685c-415f-a312-0e6fb3bba679',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.PlatezhPoruchenieListModel',
    'stateful': true,
    'title': 'Справочник Платежное поручение',
    requires: [
        'B4.model.PlatezhPoruchenieListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-PlatezhPoruchenieEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-PlatezhPoruchenieEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://c719553b-685c-415f-a312-0e6fb3bba679$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'df7f7a6a-ce46-4480-a4ef-8eef4b3b2e0e',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'dattr',
                'dataIndexAbsoluteUid': 'FieldPath://c719553b-685c-415f-a312-0e6fb3bba679$9ba36e14-63f3-40f1-8207-93ae85089637',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '4c962909-f92e-438d-bc72-8d30d95bb067',
                'sortable': true,
                'text': 'Дата',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'suma',
                'dataIndexAbsoluteUid': 'FieldPath://c719553b-685c-415f-a312-0e6fb3bba679$ced0b693-0832-47ad-bbfb-5ee88113e126',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '9a6cec4a-d0d5-4532-9b06-f5977e3f4b0f',
                'sortable': true,
                'text': 'Сумма',
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'dataa',
                'dataIndexAbsoluteUid': 'FieldPath://c719553b-685c-415f-a312-0e6fb3bba679$a4303c99-a5bc-40e0-a537-021123a343bd',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'c736f255-c150-4022-9fc8-d21e421a77c5',
                'sortable': true,
                'text': 'Дата договора',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1478594361589',
                'dataIndexAbsoluteUid': 'FieldPath://c719553b-685c-415f-a312-0e6fb3bba679$fa34704c-ab4e-4b72-9618-dca72a5da222',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '655a3e42-c13a-4da2-8280-32589e752923',
                'sortable': true,
                'text': 'Номер договора',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});