Ext.define('B4.view.VRIZUEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-vrizueditor',
    title: 'Форма редактирования ВРИ ЗУ',
    rmsUid: '9cf46c22-57af-4c74-b5ec-d2816ab28b67',
    requires: [
        'B4.form.PickerField',
        'B4.model.VRIZUEditorModel',
        'B4.model.VRIZUIerarkhijaModel',
        'B4.view.VRIZUIerarkhija'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'VRIZU_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'parent',
        'modelProperty': 'VRIZU_parent',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'VRIZU_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name1',
        'modelProperty': 'VRIZU_name1',
        'type': 'TextField'
    }, {
        'dataIndex': 'description',
        'modelProperty': 'VRIZU_description',
        'type': 'TextAreaField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '9cf46c22-57af-4c74-b5ec-d2816ab28b67-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'VRIZUEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Родительский элемент',
            'flex': 1,
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.VRIZUIerarkhija',
            'listViewCtl': 'B4.controller.VRIZUIerarkhija',
            'margin': '10 10 10 10',
            'maximizable': true,
            'model': 'B4.model.VRIZUIerarkhijaModel',
            'modelProperty': 'VRIZU_parent',
            'name': 'parent',
            'rmsUid': 'c4442e25-e9af-4569-86ac-374a6d0f4acc',
            'textProperty': 'code',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Родительский элемент',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'VRIZU_code',
            'name': 'code',
            'rmsUid': 'f5b8f26a-32ad-4aa4-a150-f61a5980c79c',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'VRIZU_name1',
            'name': 'name1',
            'rmsUid': '00fda78a-7d27-454e-8d49-9f8ae4fa3b82',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Деятельность правообладателя',
            'height': 85,
            'labelAlign': 'left',
            'margin': '10 10 10 10',
            'modelProperty': 'VRIZU_description',
            'name': 'description',
            'rmsUid': '75eb3dcf-9edf-4257-a52c-1f6b9ea1bd6c',
            'xtype': 'textarea'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'VRIZU_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle('Изменение ВРИ');
        } else {
            me.setTitle('Создание ВРИ');
        }
        return res;
    },
});