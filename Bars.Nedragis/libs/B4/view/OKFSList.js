Ext.define('B4.view.OKFSList', {
    'alias': 'widget.rms-okfslist',
    'dataSourceUid': 'c2b0994f-1f3c-411c-9f98-f03071f14265',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.OKFSListModel',
    'stateful': true,
    'title': 'Реестр ОКФС',
    requires: [
        'B4.model.OKFSListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-OKFSEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-OKFSEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://c2b0994f-1f3c-411c-9f98-f03071f14265$8c54f47d-9f25-42e8-ba8c-2d7f8eca023a',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'ce0b3bb1-b3be-400b-94b4-e80212ade101',
                'sortable': true,
                'text': 'Код',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://c2b0994f-1f3c-411c-9f98-f03071f14265$6f904d04-8b48-4c89-ae50-0bed3d2b32fb',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 3,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'cf656acc-48a7-4678-a943-2e2b41aa0353',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'check_algorithm',
                'dataIndexAbsoluteUid': 'FieldPath://c2b0994f-1f3c-411c-9f98-f03071f14265$d1410cf8-8e6c-4e00-a451-1b2012e31470',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '1ee5cb6a-775f-4901-a966-7a899d07a69d',
                'sortable': true,
                'text': 'Алгоритм сбора',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});