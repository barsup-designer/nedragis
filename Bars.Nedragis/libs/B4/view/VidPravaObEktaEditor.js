Ext.define('B4.view.VidPravaObEktaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-vidpravaobektaeditor',
    title: 'Форма редактирования Вид права объекта',
    rmsUid: '02d4e467-6c09-461d-ad03-be32a4146d75',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'VidPravaObEkta_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'VidPravaObEkta_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name1',
        'modelProperty': 'VidPravaObEkta_name1',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '02d4e467-6c09-461d-ad03-be32a4146d75-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'VidPravaObEktaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'VidPravaObEkta_code',
            'name': 'code',
            'rmsUid': '2fc21518-3057-4f1c-b240-4244d6f5dc5a',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'VidPravaObEkta_name1',
            'name': 'name1',
            'rmsUid': '693d0464-b809-463f-bf97-a37b6e827bf6',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'VidPravaObEkta_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name1'));
        } else {}
        return res;
    },
});