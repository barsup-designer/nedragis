Ext.define('B4.view.FajjlPrivjazannyjjKDokumentuList', {
    'alias': 'widget.rms-fajjlprivjazannyjjkdokumentulist',
    'dataSourceUid': '55caa4f7-4f96-47d2-a66a-4d16b044a7cf',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.FajjlPrivjazannyjjKDokumentuListModel',
    'stateful': true,
    'title': 'Реестр Файл, привязанный к документу',
    requires: [
        'B4.model.FajjlPrivjazannyjjKDokumentuListModel',
        'B4.ux.grid.plugin.DataExport',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-FajjlPrivjazannyjjKDokumentuEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-FajjlPrivjazannyjjKDokumentuEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'FileInfo_Name',
                'dataIndexAbsoluteUid': 'FieldPath://55caa4f7-4f96-47d2-a66a-4d16b044a7cf$33d87bcc-3c11-4abc-847e-845dc3c99e8c$Bars.B4.Modules.FileStorage.FileInfo, Bars.B4.Modules.FileStorage/Name',
                'dataIndexRelativePath': 'FileInfo.Name',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'f9367297-53a4-49a3-9cfa-d354f56ad85d',
                'sortable': true,
                'text': 'Наименование файла',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Comment',
                'dataIndexAbsoluteUid': 'FieldPath://55caa4f7-4f96-47d2-a66a-4d16b044a7cf$0f87c4c9-82e2-4c41-b4b7-ff5fdcfc469d',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'bda32d7c-59bd-4aae-95da-4214a5d96e51',
                'sortable': true,
                'text': 'Комментарий',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'FileInfo_Size',
                'dataIndexAbsoluteUid': 'FieldPath://55caa4f7-4f96-47d2-a66a-4d16b044a7cf$33d87bcc-3c11-4abc-847e-845dc3c99e8c$Bars.B4.Modules.FileStorage.FileInfo, Bars.B4.Modules.FileStorage/Size',
                'dataIndexRelativePath': 'FileInfo.Size',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '14fdc9ff-9908-426f-8102-50138a8c56a8',
                'sortable': true,
                'text': 'Размер',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'FileInfo_Content',
                'dataIndexAbsoluteUid': 'FieldPath://55caa4f7-4f96-47d2-a66a-4d16b044a7cf$33d87bcc-3c11-4abc-847e-845dc3c99e8c$Bars.B4.Modules.FileStorage.FileInfo, Bars.B4.Modules.FileStorage/Content',
                'dataIndexRelativePath': 'FileInfo.Content',
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0892416e-8ae5-48c1-9b8d-4f82d5d95b81',
                'sortable': true,
                'text': 'Скачать файл',
                'xtype': 'b4filecolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }, {
                'ptype': 'gridDataExport',
                'reportTitle': 'Реестр Файл, привязанный к документу'
            }]
        });
        me.callParent(arguments);
    }
});