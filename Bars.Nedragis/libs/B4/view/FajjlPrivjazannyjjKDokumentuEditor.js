Ext.define('B4.view.FajjlPrivjazannyjjKDokumentuEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-fajjlprivjazannyjjkdokumentueditor',
    title: 'Форма редактирования Файл, привязанный к документу',
    rmsUid: '0cde53c9-41ae-4e83-857e-98b6537f660a',
    requires: [
        'B4.form.FileField',
        'B4.form.PickerField',
        'B4.model.DokumentEditorModel',
        'B4.model.DokumentListModel',
        'B4.model.FileInfo',
        'B4.view.DokumentList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'FajjlPrivjazannyjjKDokumentu_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'FileInfo',
        'modelProperty': 'FajjlPrivjazannyjjKDokumentu_FileInfo',
        'type': 'FileField'
    }, {
        'dataIndex': 'Comment',
        'modelProperty': 'FajjlPrivjazannyjjKDokumentu_Comment',
        'type': 'TextAreaField'
    }, {
        'dataIndex': 'Document',
        'modelProperty': 'FajjlPrivjazannyjjKDokumentu_Document',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '0cde53c9-41ae-4e83-857e-98b6537f660a-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'FajjlPrivjazannyjjKDokumentuEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Запись файла',
                'flex': 1,
                'margin': '12 10 0 10',
                'modelProperty': 'FajjlPrivjazannyjjKDokumentu_FileInfo',
                'name': 'FileInfo',
                'rmsUid': '9de0b6c3-9434-48ac-bd7f-4d4e0756d788',
                'typeAhead': false,
                'xtype': 'b4filefield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Комментарий',
                'flex': 1,
                'labelAlign': 'top',
                'margin': '10 10 10 10',
                'modelProperty': 'FajjlPrivjazannyjjKDokumentu_Comment',
                'name': 'Comment',
                'rmsUid': 'a1c2dfa6-d4e0-4d75-b6a9-4ae8de64ad6b',
                'xtype': 'textarea'
            }, {
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Документ',
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.DokumentList',
                'listViewCtl': 'B4.controller.DokumentList',
                'margin': '5 10 10 10',
                'maximizable': true,
                'model': 'B4.model.DokumentListModel',
                'modelProperty': 'FajjlPrivjazannyjjKDokumentu_Document',
                'name': 'Document',
                'rmsUid': '58e76208-de70-4264-aa76-ccfee9c9b2c8',
                'textProperty': 'Number',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Документ',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }],
            'layout': {
                'type': 'fit'
            },
            'rmsUid': 'cbc1fbf7-d4ce-4e8e-9493-a6f9b96fe425',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'FajjlPrivjazannyjjKDokumentu_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle('Изменение файла');
        } else {
            me.setTitle('Создание файла');
        }
        return res;
    },
});