Ext.define('B4.view.DogovorIPrilozhenijaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-dogovoriprilozhenijaeditor',
    title: 'Договор и приложения',
    rmsUid: 'fce3486c-df02-4cb2-8e6e-63282f28e933',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'DogovorIPrilozhenija_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1478683765504',
        'modelProperty': 'DogovorIPrilozhenija_Element1478683765504',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1478683945974',
        'modelProperty': 'DogovorIPrilozhenija_Element1478683945974',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478683081624',
        'modelProperty': 'DogovorIPrilozhenija_Element1478683081624',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1478683584922',
        'modelProperty': 'DogovorIPrilozhenija_Element1478683584922',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1478683617170',
        'modelProperty': 'DogovorIPrilozhenija_Element1478683617170',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478683683665',
        'modelProperty': 'DogovorIPrilozhenija_Element1478683683665',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478683916351',
        'modelProperty': 'DogovorIPrilozhenija_Element1478683916351',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478684021941',
        'modelProperty': 'DogovorIPrilozhenija_Element1478684021941',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478684113244',
        'modelProperty': 'DogovorIPrilozhenija_Element1478684113244',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1478684143764',
        'modelProperty': 'DogovorIPrilozhenija_Element1478684143764',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478683832391',
        'modelProperty': 'DogovorIPrilozhenija_Element1478683832391',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1478683872055',
        'modelProperty': 'DogovorIPrilozhenija_Element1478683872055',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'fce3486c-df02-4cb2-8e6e-63282f28e933-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'DogovorIPrilozhenijaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Панель закладок',
            'items': [{
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Договор',
                'items': [{
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Контейнер',
                    'layout': {
                        'type': 'hbox'
                    },
                    'rmsUid': '20701606-302c-4828-b60a-af09734a5be4',
                    'title': 'Контейнер',
                    'xtype': 'container'
                }, {
                    'allowBlank': true,
                    'allowDecimals': false,
                    'anchor': '100%',
                    'decimalPrecision': 2,
                    'fieldLabel': 'Номер заявки',
                    'margin': '10 300 10 5',
                    'minValue': 0,
                    'modelProperty': 'DogovorIPrilozhenija_Element1478683765504',
                    'name': 'Element1478683765504',
                    'rmsUid': 'a7335e03-76ee-4964-a87a-927a50f14d0f',
                    'step': 1,
                    'xtype': 'numberfield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'ФИО Руководителя ГКУ ',
                    'margin': '5 100 5 5',
                    'modelProperty': 'DogovorIPrilozhenija_Element1478683945974',
                    'name': 'Element1478683945974',
                    'rmsUid': '3eecb384-0c92-46a5-83e6-d15fee96f2fb',
                    'xtype': 'textfield'
                }],
                'rmsUid': 'e960b6fa-32d8-40b0-ba50-167d2634f916',
                'title': 'Договор',
                'xtype': 'container'
            }, {
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Спецификация по договору',
                'items': [{
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Контейнер',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Номер договора',
                        'margin': '5 5 5 5',
                        'minValue': 0,
                        'modelProperty': 'DogovorIPrilozhenija_Element1478683081624',
                        'name': 'Element1478683081624',
                        'rmsUid': '1984e94a-40a3-405a-8c02-549b7b26b378',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'fieldLabel': 'Дата договора',
                        'format': 'd.m.Y',
                        'margin': '5 5 5 5',
                        'modelProperty': 'DogovorIPrilozhenija_Element1478683584922',
                        'name': 'Element1478683584922',
                        'rmsUid': '2e520f55-4b38-4f01-a50c-b46d88a8e3c6',
                        'xtype': 'datefield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'fieldLabel': 'Номер спецификации',
                        'modelProperty': 'DogovorIPrilozhenija_Element1478683617170',
                        'name': 'Element1478683617170',
                        'rmsUid': '5b7f080c-71fd-46b5-a979-d530c17f79e2',
                        'xtype': 'textfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'fieldLabel': 'Дата спецификации',
                        'modelProperty': 'DogovorIPrilozhenija_Element1478683683665',
                        'name': 'Element1478683683665',
                        'rmsUid': 'fa8f6d75-080c-4b78-b10e-4db9358758d4',
                        'xtype': 'textfield'
                    }],
                    'rmsUid': '66d94632-c568-4651-831f-9c88d8bfb0e5',
                    'title': 'Контейнер',
                    'xtype': 'container'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Панель',
                    'rmsUid': 'c1c38027-81fe-4375-9036-830eb3f4957a',
                    'title': 'Панель',
                    'xtype': 'panel'
                }],
                'rmsUid': 'f34853d3-d661-4ce1-891e-848dbb4c7da7',
                'title': 'Спецификация по договору',
                'xtype': 'container'
            }, {
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Соглашение о договорной цене',
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Сумма на естеств языке',
                    'modelProperty': 'DogovorIPrilozhenija_Element1478683916351',
                    'name': 'Element1478683916351',
                    'rmsUid': '80e17734-ca2e-4c5c-82ce-493fc770c721',
                    'xtype': 'textfield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Номер акта сдачи-приемки',
                    'modelProperty': 'DogovorIPrilozhenija_Element1478684021941',
                    'name': 'Element1478684021941',
                    'rmsUid': 'cb26ca10-935a-473a-9712-4ee9e724225d',
                    'xtype': 'textfield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Дата акта сдачи-приемки',
                    'format': 'd.m.Y',
                    'modelProperty': 'DogovorIPrilozhenija_Element1478684113244',
                    'name': 'Element1478684113244',
                    'rmsUid': 'd5083b71-a7a7-4fc4-a9ce-acc29ce6359d',
                    'xtype': 'datefield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Номер спецификации',
                    'modelProperty': 'DogovorIPrilozhenija_Element1478684143764',
                    'name': 'Element1478684143764',
                    'rmsUid': '51884743-7252-4818-b967-54f55cb89d82',
                    'xtype': 'textfield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Дата соглашения',
                    'format': 'd.m.Y',
                    'modelProperty': 'DogovorIPrilozhenija_Element1478683832391',
                    'name': 'Element1478683832391',
                    'rmsUid': 'cb2f1a78-d99c-4ad2-97cb-0964f6d3114e',
                    'xtype': 'datefield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'НДС на естеств языке',
                    'modelProperty': 'DogovorIPrilozhenija_Element1478683872055',
                    'name': 'Element1478683872055',
                    'rmsUid': '860ac778-95a7-4ca7-ba95-0b9851900557',
                    'xtype': 'textfield'
                }],
                'rmsUid': '36fc45f3-f6b4-4638-9d7e-c89a76c4dee4',
                'title': 'Соглашение о договорной цене',
                'xtype': 'container'
            }],
            'rmsUid': 'd4de6b23-9f04-4901-b54d-34538256e5b5',
            'xtype': 'tabpanel'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'DogovorIPrilozhenija_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1478683081624'));
        } else {}
        return res;
    },
});