Ext.define('B4.view.FailyEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-failyeditor',
    title: 'Редактирование Файлы',
    rmsUid: '9a7aac91-3084-45d5-98e5-556c2690ccfc',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Faily_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'Faily_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'obem',
        'modelProperty': 'Faily_obem',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '9a7aac91-3084-45d5-98e5-556c2690ccfc-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'FailyEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Идентификатор',
            'margin': '10 250 10 10',
            'modelProperty': '60b90e25-7edd-4e14-b6d8-27d904a3d0b3',
            'readOnly': true,
            'rmsUid': '60b90e25-7edd-4e14-b6d8-27d904a3d0b3',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'margin': '10 50 10 10',
            'modelProperty': 'Faily_name',
            'name': 'name',
            'rmsUid': '9413e29b-0a4e-45b0-8859-30b5600ffe18',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'decimalPrecision': 2,
            'fieldLabel': 'Объем',
            'hideTrigger': true,
            'margin': '10 250 10 10',
            'minValue': 0,
            'modelProperty': 'Faily_obem',
            'name': 'obem',
            'rmsUid': '2c1c4fb3-3379-4ccf-b4ce-2dd2d1b7160a',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Faily_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});