Ext.define('B4.view.OtraslEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-otrasleditor',
    title: 'Форма редактирования Отрасль',
    rmsUid: '7ecc5ddc-2d0e-46f5-a6e9-058b5a79fe72',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Otrasl_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'Otrasl_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'Otrasl_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '7ecc5ddc-2d0e-46f5-a6e9-058b5a79fe72-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OtraslEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'Otrasl_code',
            'name': 'code',
            'rmsUid': '534995dd-fd10-44f9-8e0a-a75951bddf06',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'Otrasl_name',
            'name': 'name',
            'rmsUid': '7c7c5f7c-90cc-4a4a-b875-8808eeede292',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Otrasl_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});