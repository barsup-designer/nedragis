Ext.define('B4.view.PrirodopolZovanie2List', {
    'alias': 'widget.rms-prirodopolzovanie2list',
    'dataSourceUid': '825f8a0a-cc49-42e0-8d04-df96f9f2e519',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'groupField': 'MonthPR2_month_pr222',
    'model': 'B4.model.PrirodopolZovanie2ListModel',
    'stateful': true,
    'title': 'Реестр Природопользование 2',
    requires: [
        'B4.model.PrirodopolZovanie2ListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-PrirodopolZovanie2Editor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-PrirodopolZovanie2Editor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'DateInPR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$a2516249-c410-4d40-902e-08e5d0369bb6',
                'dataIndexRelativePath': 'DateInPR2',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '432f5e1e-f2ca-425c-8106-0649d0c2af21',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата входящего',
                'width': 70,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'NumInDocsPr222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$49c48009-3fc5-40bf-8d24-95eb5de7fa79',
                'dataIndexRelativePath': 'NumInDocsPr2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'db1c9793-7c69-430f-87a3-7fae563340f4',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер входящего',
                'width': 70,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'DateCompletePR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$610bbaee-be1b-4d36-919c-2663c80128cc',
                'dataIndexRelativePath': 'DateCompletePR2',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '6b603598-2fae-42ec-a90e-d13609a0b893',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата исполнения',
                'width': 70,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'CompanyPR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$d697ddd7-4d57-4d7d-96e3-58910dc06743',
                'dataIndexRelativePath': 'CompanyPR2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '48ec3b29-cbb9-4abd-8c85-bdb6288f0f82',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Компания (Заказчик)',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'VidRabotPR2_vid_rabot_prirod_s222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$5b77a419-637f-4b69-82a5-80c3756e9c44$5b610cce-715f-4684-99f3-eb7068b94320',
                'dataIndexRelativePath': 'VidRabotPR2.vid_rabot_prirod_s',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 2,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '1b9b0de7-0166-452c-a3c1-d9d10790749d',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Вид работ',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'QuantityPR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$1fd92fbe-274a-4ca5-b920-9b2449cba2d3',
                'dataIndexRelativePath': 'QuantityPR2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'fab98012-0ad1-474a-ba73-fabb1ce42cea',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Коли-чество',
                'width': 50,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'ObjectPR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$b11561d7-197a-4e13-9128-1fbbb0cc0400',
                'dataIndexRelativePath': 'ObjectPR2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 2,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '20a22d1a-1b46-4aa0-926e-96a9c81bf43d',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Объект',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'NumDogovorPR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$cb1e975c-ff01-4461-a14d-94fe18775ee1',
                'dataIndexRelativePath': 'NumDogovorPR2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '1d9d411a-acbf-411f-8d88-c40648af7cdf',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер договора',
                'width': 60,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'NumZayvkaPR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$0eace844-d207-48e6-b327-e680938246c6',
                'dataIndexRelativePath': 'NumZayvkaPR2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5092b7b7-5c47-49d5-975b-e2b01b23dab6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер заявки ',
                'width': 60,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'CodePR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$5fb58b91-57ba-4489-8e52-2dda9782cd78',
                'dataIndexRelativePath': 'CodePR2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '35730e75-a4fc-458c-85bc-812d10dc3534',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Код',
                'width': 60,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'SummToPayPR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$2411d741-27e1-4d00-a1cc-3fff960aa7ae',
                'dataIndexRelativePath': 'SummToPayPR2',
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'format': '0.000',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '95bdce7b-478e-4711-b1f9-a39d6b98d8a8',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Сумма к оплате',
                'width': 70,
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'IdentNumderPR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$ff4e643d-7f7f-4475-94c7-0e6752be8b2e',
                'dataIndexRelativePath': 'IdentNumderPR2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5f64b0b2-ca75-4be4-adeb-6b9339e61162',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Идентификационный номер',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'IspolnitelPR2_Element1474352136232PR2',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$ace26669-298f-42bb-86a3-85b45d0665d9$5c9e0c3e-7dcb-483d-89fd-a12dacaa7279',
                'dataIndexRelativePath': 'IspolnitelPR2.Element1474352136232',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd1c7d594-60af-446f-bf5f-c486b40077b2',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Исполнитель',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'SoispolnitelPR22222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$83377ef3-bbf0-4218-a843-123c59f4ad60$5c9e0c3e-7dcb-483d-89fd-a12dacaa7279',
                'dataIndexRelativePath': 'SoispolnitelPR2.Element1474352136232',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5ad0737b-9ce2-4bae-b0bc-7478fdc1f8f8',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Соисполнитель',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'MonthPR2_month_pr222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$839608b1-d5e6-47f9-ab37-611fc6140b01$6f3bdf0a-69b1-44b0-87bb-6c627aa28f92',
                'dataIndexRelativePath': 'MonthPR2.month_pr',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0d824992-9b82-47fa-96f9-f9c55fcaec9b',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Отчетный месяц',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'FIOZakazchikaPR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$3be09518-41c0-4b19-bdc8-a17aab9bc9e3',
                'dataIndexRelativePath': 'FIOZakazchikaPR2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '7f5a90d7-aa86-4bd9-bc08-0c0097ab3538',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'ФИО заказчика',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'PrimechaniePR222',
                'dataIndexAbsoluteUid': 'FieldPath://825f8a0a-cc49-42e0-8d04-df96f9f2e519$9e8cacdb-e604-4ff6-add7-48be233b46ac',
                'dataIndexRelativePath': 'PrimechaniePR2',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '7f5f7517-0e14-477b-bcbc-a54e36a0f5bc',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Примечание',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});