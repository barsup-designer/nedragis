Ext.define('B4.view.DokumentyZUList2', {
    'alias': 'widget.rms-dokumentyzulist2',
    'dataSourceUid': '4996ce7a-3313-46fc-979a-fe45071d30e9',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.DokumentyZUList2Model',
    'stateful': true,
    'title': 'Реестр документов (ДокументыЗУ)',
    requires: [
        'B4.model.DokumentyZUList2Model',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-DokumentyZUEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-DokumentyZUEditor3-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'doc1_Type_name',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$b0627770-34ec-4c22-8112-78c21088ef09$9c89dcca-5979-4614-8413-7afafab6ae59$4de0c197-d1b9-4da2-90d6-320d9c727e26',
                'dataIndexRelativePath': 'doc1.Type.name',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a8a5d184-12f9-4920-8602-af15f41db161',
                'sortable': true,
                'text': 'Тип документа',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'doc1_Number',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$b0627770-34ec-4c22-8112-78c21088ef09$a786d3f3-7c06-42e9-aca6-8d86b2ff7b68',
                'dataIndexRelativePath': 'doc1.Number',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd0a544e2-8919-4d70-93e2-40db45ae5abd',
                'sortable': true,
                'text': 'Номер документа',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'doc1_Date',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$b0627770-34ec-4c22-8112-78c21088ef09$b04912a6-57a3-4cbf-9877-2c47c960c735',
                'dataIndexRelativePath': 'doc1.Date',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '8d6d40b1-54fe-457a-a0e3-ffc5665d9cab',
                'sortable': true,
                'text': 'Дата документа',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'doc1_Subject_Representation',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$b0627770-34ec-4c22-8112-78c21088ef09$4265a9b2-1918-4248-b839-42bac455b5a0$6f8b2489-4290-4747-93ba-ff199586d3bb',
                'dataIndexRelativePath': 'doc1.Subject.Representation',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '883ccc5e-e831-4c64-9399-3b5e235d8b95',
                'sortable': true,
                'text': 'Субъект',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'zemuch_Id',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$bac8dff6-5e22-4e24-8a69-c82f1802f8e9$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'dataIndexRelativePath': 'zemuch.Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a975f0d7-9c67-46a4-a964-d3c2ae0ebeed',
                'sortable': true,
                'text': 'Земельный участок.Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'doc1_Signatory_Representation',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$b0627770-34ec-4c22-8112-78c21088ef09$6dc50ef8-c0c5-4d09-b077-202a53e27303$6f8b2489-4290-4747-93ba-ff199586d3bb',
                'dataIndexRelativePath': 'doc1.Signatory.Representation',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5d1a39d9-331b-464d-a347-3823d1e09bed',
                'sortable': true,
                'text': 'Орган подписавший документ',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'doc1_ActualDate',
                'dataIndexAbsoluteUid': 'FieldPath://4996ce7a-3313-46fc-979a-fe45071d30e9$b0627770-34ec-4c22-8112-78c21088ef09$87563068-b9c1-4bd9-a886-ef395b1c0417',
                'dataIndexRelativePath': 'doc1.ActualDate',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'b5ad08cf-9fe7-4201-a3f2-3a8e2925228a',
                'sortable': true,
                'text': 'Дата вступления в силу',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});