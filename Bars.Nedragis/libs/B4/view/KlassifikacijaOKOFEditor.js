Ext.define('B4.view.KlassifikacijaOKOFEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-klassifikacijaokofeditor',
    title: 'Форма редактирования Классификация ОКОФ',
    rmsUid: '42f494a8-8d1b-40c9-a55d-10978a409c0a',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'KlassifikacijaOKOF_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'KlassifikacijaOKOF_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'KlassifikacijaOKOF_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '42f494a8-8d1b-40c9-a55d-10978a409c0a-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'KlassifikacijaOKOFEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'KlassifikacijaOKOF_code',
            'name': 'code',
            'rmsUid': '66ffb858-8282-4816-830c-b2f71ebd41ac',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'KlassifikacijaOKOF_name',
            'name': 'name',
            'rmsUid': 'ebc613f9-3adc-44d8-a809-49f78b8be4c6',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'KlassifikacijaOKOF_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});