Ext.define('B4.view.VidPravaObEktaList', {
    'alias': 'widget.rms-vidpravaobektalist',
    'dataSourceUid': 'bc0928cd-24af-4ec6-9445-ba8b0e7367f7',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.VidPravaObEktaListModel',
    'stateful': true,
    'title': 'Реестр Вид права объекта',
    requires: [
        'B4.model.VidPravaObEktaListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-VidPravaObEktaEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-VidPravaObEktaEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://bc0928cd-24af-4ec6-9445-ba8b0e7367f7$788096e4-9847-430d-a9e9-b7aeb4a7a31e',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '62ad5f34-5a47-4f36-8902-6e2a874324d8',
                'sortable': true,
                'text': 'Код',
                'width': 55,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'name1',
                'dataIndexAbsoluteUid': 'FieldPath://bc0928cd-24af-4ec6-9445-ba8b0e7367f7$53a0c652-b83f-4495-abdf-9e22694d7d4f',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b11fffe3-a611-47a7-b4c5-a1482c318121',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});