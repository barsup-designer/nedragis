Ext.define('B4.view.SpecifikacijaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-specifikacijaeditor',
    title: 'Форма редактирования Спецификация',
    rmsUid: 'baa5124e-1891-4bec-8daa-56dd3e48f578',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Specifikacija_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1478601989143',
        'modelProperty': 'Specifikacija_Element1478601989143',
        'type': 'DateField'
    }, {
        'dataIndex': 'nomsp',
        'modelProperty': 'Specifikacija_nomsp',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1478607128570',
        'modelProperty': 'Specifikacija_Element1478607128570',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1478602079636',
        'modelProperty': 'Specifikacija_Element1478602079636',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'baa5124e-1891-4bec-8daa-56dd3e48f578-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'SpecifikacijaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'allowBlank': true,
                'fieldLabel': 'Дата спецификации',
                'format': 'd.m.Y',
                'margin': '5 5 5 5',
                'modelProperty': 'Specifikacija_Element1478601989143',
                'name': 'Element1478601989143',
                'rmsUid': '2bd09937-e609-4ebe-bda3-8ffc67c7b31b',
                'xtype': 'datefield'
            }, {
                'allowBlank': true,
                'allowDecimals': false,
                'decimalPrecision': 2,
                'fieldLabel': 'Номер спецификации',
                'margin': '5 5 5 5',
                'minValue': 0,
                'modelProperty': 'Specifikacija_nomsp',
                'name': 'nomsp',
                'rmsUid': 'f0f6316e-d032-4545-9efb-c66773ec904d',
                'step': 1,
                'xtype': 'numberfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Номер заявки',
                'margin': '10 5 5 5',
                'modelProperty': 'Specifikacija_Element1478607128570',
                'name': 'Element1478607128570',
                'rmsUid': '5cb2d79e-6e97-4ce9-bd17-819b944a9332',
                'xtype': 'textfield'
            }],
            'layout': {
                'type': 'hbox'
            },
            'rmsUid': '248f60ca-855e-4a67-b05c-1db6e0c8db30',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'layout': {
                'type': 'hbox'
            },
            'rmsUid': '0557a582-031f-4441-8914-738766e23cae',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Панель',
                'rmsUid': 'bc1a2e16-fb19-4d5e-a551-02cb9df3f102',
                'title': 'Панель',
                'xtype': 'panel'
            }],
            'rmsUid': '335d6a4f-a283-4918-b02b-bcfe72738f86',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'allowBlank': true,
                'anchor': '100%',
                'decimalPrecision': 2,
                'fieldLabel': 'Итоговая сумма',
                'margin': '10 600 10 10',
                'minValue': 0,
                'modelProperty': 'Specifikacija_Element1478602079636',
                'name': 'Element1478602079636',
                'rmsUid': '0008273d-85f5-49b4-bc9d-b7b981bae2e4',
                'step': 1,
                'xtype': 'numberfield'
            }],
            'rmsUid': '6b29cef4-7dd6-42ac-a417-e981a8da7886',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'fieldLabel': 'Посмотреть заявку',
            'margin': '10 300 10 10',
            'rmsUid': '33f8c524-a33d-4908-8e29-7dafad8742af',
            'text': 'Посмотреть заявку',
            'xtype': 'button'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Составить',
            'height': 50,
            'items': [{
                'fieldLabel': 'Договор',
                'margin': '5 10 5 10',
                'rmsUid': '4ad7a97b-2c79-4aa5-b622-1ccec4397a81',
                'text': 'Договор',
                'xtype': 'button'
            }, {
                'fieldLabel': 'Спецификацию по договору',
                'margin': '5 10 5 10',
                'rmsUid': '6a4d5c8c-9dd9-4280-9824-0dc31f0c139f',
                'text': 'Спецификацию по договору',
                'xtype': 'button'
            }, {
                'fieldLabel': 'Соглашение',
                'margin': '5 10 5 10',
                'rmsUid': 'a7d1e6d0-63ff-4726-ab78-72e0aa7114a0',
                'text': 'Соглашение',
                'xtype': 'button'
            }],
            'layout': {
                'type': 'vbox'
            },
            'margin': '10 10 10 10',
            'rmsUid': 'a0674d97-47cd-4b34-8f62-d0097f06285e',
            'title': 'Составить',
            'xtype': 'fieldset'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Specifikacija_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('nomsp'));
        } else {}
        return res;
    },
});