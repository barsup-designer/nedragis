Ext.define('B4.view.TipySubEktaPravaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-tipysubektapravaeditor',
    title: 'Форма редактирования Типы субъекта права',
    rmsUid: 'cb5ff098-eac9-4e70-ad96-759a2a0681a4',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'TipySubEktaPrava_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'TipySubEktaPrava_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'TipySubEktaPrava_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'cb5ff098-eac9-4e70-ad96-759a2a0681a4-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'TipySubEktaPravaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'labelWidth': 60,
            'margin': '10 10 10 10',
            'modelProperty': 'TipySubEktaPrava_code',
            'name': 'code',
            'rmsUid': '39abcc11-8b28-4428-96d0-790bbe11050e',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'margin': '10 10 10 10',
            'modelProperty': 'TipySubEktaPrava_name',
            'name': 'name',
            'rmsUid': 'db654413-bcf7-4cd8-a911-7db8b9359fde',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'TipySubEktaPrava_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});