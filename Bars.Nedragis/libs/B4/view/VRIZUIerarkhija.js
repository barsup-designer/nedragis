Ext.define('B4.view.VRIZUIerarkhija', {
    'alias': 'widget.rms-vrizuierarkhija',
    'dataSourceUid': '5db62416-3df3-4972-a540-4abd50af9c5c',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': false,
    'extend': 'B4.base.registry.TreeView',
    'model': 'B4.model.VRIZUIerarkhijaModel',
    'parentFieldName': 'parent',
    'stateful': true,
    'title': 'Виды разрешенного использования',
    requires: [
        'B4.model.VRIZUIerarkhijaModel',
        'B4.ux.grid.plugin.DataExport',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'content-img-icons-reload-png',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-add-png',
        'itemId': 'Addition-VRIZUEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-edit-png',
        'itemId': 'Editing-VRIZUEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-delete-png',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://5db62416-3df3-4972-a540-4abd50af9c5c$d96612dd-5047-4630-ab93-03c58f1eab71',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'fe667ae0-be47-4e80-b1c7-f340e3f48994',
                'sortable': true,
                'text': 'Код',
                'xtype': 'treecolumn'
            }, {
                'dataIndex': 'name1',
                'dataIndexAbsoluteUid': 'FieldPath://5db62416-3df3-4972-a540-4abd50af9c5c$868835cf-0e3b-47aa-be83-eb3e66045386',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 2,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd14562f1-f156-4105-85ce-842d4d5cf59f',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'description',
                'dataIndexAbsoluteUid': 'FieldPath://5db62416-3df3-4972-a540-4abd50af9c5c$5672c027-5051-4296-9cdd-8ac839de220e',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 3,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '34c2c868-a814-48e4-a26a-36a03c9d0e6e',
                'sortable': true,
                'text': 'Деятельность правообладателя',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://5db62416-3df3-4972-a540-4abd50af9c5c$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '35db3872-56cd-4a18-a8fa-2ab52fed4080',
                'sortable': true,
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'ptype': 'gridDataExport',
                'reportTitle': 'Виды разрешенного использования'
            }]
        });
        me.callParent(arguments);
    }
});