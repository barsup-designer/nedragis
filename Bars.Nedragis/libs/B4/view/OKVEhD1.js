Ext.define('B4.view.OKVEhD1', {
    'alias': 'widget.rms-okvehd1',
    'dataSourceUid': 'c6f1d936-9d21-451f-b3c5-8c562fc1cff7',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': false,
    'extend': 'B4.base.registry.TreeView',
    'model': 'B4.model.OKVEhD1Model',
    'parentFieldName': 'parent_id',
    'stateful': true,
    'title': 'ОКВЭД',
    requires: [
        'B4.model.OKVEhD1Model',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'content-img-icons-reload-png',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-add-png',
        'itemId': 'Addition-OKVEhDEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-edit-png',
        'itemId': 'Editing-OKVEhDEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-delete-png',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'code',
                'dataIndexAbsoluteUid': 'FieldPath://c6f1d936-9d21-451f-b3c5-8c562fc1cff7$12db5f89-3ecd-4d5b-87c4-60b76bd51438',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '3ed6a810-50a9-422f-b640-7efb2fbbfae6',
                'sortable': true,
                'text': 'Код',
                'xtype': 'treecolumn'
            }, {
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://c6f1d936-9d21-451f-b3c5-8c562fc1cff7$2e4c719a-5963-490f-999e-2e5cbc07b9b8',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 2,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '7add43e8-3dd8-4a85-87c0-d02596d86785',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://c6f1d936-9d21-451f-b3c5-8c562fc1cff7$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '66dab981-7ead-4130-9b72-2549e176efb8',
                'sortable': true,
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }]
        });
        me.callParent(arguments);
    }
});