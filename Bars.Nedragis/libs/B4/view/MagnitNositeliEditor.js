Ext.define('B4.view.MagnitNositeliEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-magnitnositelieditor',
    title: 'Редактирование Магнитные носители',
    rmsUid: 'e235f18e-ccdd-403c-8a6f-0a2f3542ad8f',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'MagnitNositeli_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'MagnitNositeli_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'barcod',
        'modelProperty': 'MagnitNositeli_barcod',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'e235f18e-ccdd-403c-8a6f-0a2f3542ad8f-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'MagnitNositeliEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Идентификатор',
            'margin': '10 200 10 10',
            'modelProperty': '46af6563-4121-4a65-9944-554f90352190',
            'readOnly': true,
            'rmsUid': '46af6563-4121-4a65-9944-554f90352190',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'margin': '10 10 10 10',
            'modelProperty': 'MagnitNositeli_name',
            'name': 'name',
            'rmsUid': '2970fcb5-f8f7-4ef4-be99-4b410cb92dfa',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Бар-код',
            'margin': '10 200 10 10',
            'modelProperty': 'MagnitNositeli_barcod',
            'name': 'barcod',
            'rmsUid': 'a5503ad1-6dbe-4a74-badb-56b932e9577e',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'MagnitNositeli_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});