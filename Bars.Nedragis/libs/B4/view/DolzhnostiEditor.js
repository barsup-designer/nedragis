Ext.define('B4.view.DolzhnostiEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-dolzhnostieditor',
    title: 'Форма редактирования Должности',
    rmsUid: '63691359-e50a-418a-b0fe-9804ec7ac373',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Dolzhnosti_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'Dolzhnosti_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name1',
        'modelProperty': 'Dolzhnosti_name1',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '63691359-e50a-418a-b0fe-9804ec7ac373-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'DolzhnostiEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'Dolzhnosti_code',
            'name': 'code',
            'rmsUid': '261eb75d-eed9-4f13-ae2c-9ba0ae7dfcf5',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'Dolzhnosti_name1',
            'name': 'name1',
            'rmsUid': '31edd7d9-e2ef-4c1b-a249-4bc128e5e1a0',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Dolzhnosti_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name1'));
        } else {}
        return res;
    },
});