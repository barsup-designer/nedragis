Ext.define('B4.view.OKATOEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-okatoeditor',
    title: 'Форма редактирования ОКАТО',
    rmsUid: '125aabc1-a228-4220-a185-855d4c0041d7',
    requires: [
        'B4.form.PickerField',
        'B4.model.OKATO1Model',
        'B4.model.OKATOEditorModel',
        'B4.view.OKATO1'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'OKATO_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'parent_id',
        'modelProperty': 'OKATO_parent_id',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'OKATO_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name1',
        'modelProperty': 'OKATO_name1',
        'type': 'TextField'
    }, {
        'dataIndex': 'center_name',
        'modelProperty': 'OKATO_center_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '125aabc1-a228-4220-a185-855d4c0041d7-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OKATOEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Родительский элемент',
            'flex': 1,
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.OKATO1',
            'listViewCtl': 'B4.controller.OKATO1',
            'margin': '10 10 10 10',
            'maximizable': true,
            'model': 'B4.model.OKATO1Model',
            'modelProperty': 'OKATO_parent_id',
            'name': 'parent_id',
            'rmsUid': '7353dd9b-b1c4-4dca-b206-c040e8ed6efb',
            'textProperty': 'code',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Родительский элемент',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'OKATO_code',
            'name': 'code',
            'rmsUid': 'adea1d86-edaf-4c28-bd86-4ec05b4e064f',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'OKATO_name1',
            'name': 'name1',
            'rmsUid': '2df383e0-0b85-4eb9-b3fb-b1c5305df031',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Районный центр',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'OKATO_center_name',
            'name': 'center_name',
            'rmsUid': 'd7ee9e74-9029-447e-bdad-da513a888e9d',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'OKATO_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name1'));
        } else {}
        return res;
    },
});