Ext.define('B4.view.KategoriiZemelEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-kategoriizemeleditor',
    title: 'Форма редактирования Категории земель',
    rmsUid: '7854d448-c1e5-41dc-b616-58a6650ffc03',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'KategoriiZemel_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'KategoriiZemel_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name1',
        'modelProperty': 'KategoriiZemel_name1',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '7854d448-c1e5-41dc-b616-58a6650ffc03-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'KategoriiZemelEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'KategoriiZemel_code',
            'name': 'code',
            'rmsUid': '47f0692a-602d-4ce0-8139-8f02c57116b3',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'KategoriiZemel_name1',
            'name': 'name1',
            'rmsUid': '618db8b4-7efd-4cb1-8df6-05b909b7a815',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'KategoriiZemel_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name1'));
        } else {}
        return res;
    },
});