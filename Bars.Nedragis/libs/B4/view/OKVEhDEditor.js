Ext.define('B4.view.OKVEhDEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-okvehdeditor',
    title: 'Форма редактирования ОКВЭД',
    rmsUid: '4641dbd1-c764-43d5-8a95-aef8c0ae76b8',
    requires: [
        'B4.form.PickerField',
        'B4.model.OKVEhD1Model',
        'B4.model.OKVEhDEditorModel',
        'B4.view.OKVEhD1'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'OKVEhD_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'OKVEhD_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'OKVEhD_name',
        'type': 'TextAreaField'
    }, {
        'dataIndex': 'parent_id',
        'modelProperty': 'OKVEhD_parent_id',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '4641dbd1-c764-43d5-8a95-aef8c0ae76b8-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OKVEhDEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 10 10',
            'modelProperty': 'OKVEhD_code',
            'name': 'code',
            'rmsUid': '635f8d2c-7546-47c2-bc7a-602ecfdb264b',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'flex': 1,
            'labelAlign': 'left',
            'margin': '10 10 0 10',
            'modelProperty': 'OKVEhD_name',
            'name': 'name',
            'rmsUid': '569bb180-936b-43fa-acee-c911c607a167',
            'xtype': 'textarea'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Родительский элемент',
            'flex': 1,
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.OKVEhD1',
            'listViewCtl': 'B4.controller.OKVEhD1',
            'margin': '10 10 10 10',
            'maximizable': true,
            'model': 'B4.model.OKVEhD1Model',
            'modelProperty': 'OKVEhD_parent_id',
            'name': 'parent_id',
            'rmsUid': '3e8a9fbe-4355-4c24-b0b3-1cde0c3d5fff',
            'textProperty': 'code',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Родительский элемент',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'OKVEhD_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});