Ext.define('B4.view.PrirodopolZovanie2Editor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-prirodopolzovanie2editor',
    title: 'Форма редактирования Природопользование 2',
    rmsUid: '77996565-8563-4a00-b4b6-5f7691a289cd',
    requires: [
        'B4.form.PickerField',
        'B4.model.MesjacPrirodaEditorModel',
        'B4.model.MesjacPrirodaListModel',
        'B4.model.Sotrudniki1EditorModel',
        'B4.model.Sotrudniki1ListModel',
        'B4.model.VidRabotPrirEditorModel',
        'B4.model.VidRabotPrirListModel',
        'B4.view.MesjacPrirodaList',
        'B4.view.Sotrudniki1List',
        'B4.view.VidRabotPrirList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'PrirodopolZovanie2_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'DateInPR2',
        'modelProperty': 'PrirodopolZovanie2_DateInPR2',
        'type': 'DateField'
    }, {
        'dataIndex': 'NumInDocsPr2',
        'modelProperty': 'PrirodopolZovanie2_NumInDocsPr2',
        'type': 'TextField'
    }, {
        'dataIndex': 'DateCompletePR2',
        'modelProperty': 'PrirodopolZovanie2_DateCompletePR2',
        'type': 'DateField'
    }, {
        'dataIndex': 'CompanyPR2',
        'modelProperty': 'PrirodopolZovanie2_CompanyPR2',
        'type': 'TextField'
    }, {
        'dataIndex': 'VidRabotPR2',
        'modelProperty': 'PrirodopolZovanie2_VidRabotPR2',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'QuantityPR2',
        'modelProperty': 'PrirodopolZovanie2_QuantityPR2',
        'type': 'NumberField'
    }, {
        'dataIndex': 'ObjectPR2',
        'modelProperty': 'PrirodopolZovanie2_ObjectPR2',
        'type': 'TextField'
    }, {
        'dataIndex': 'NumDogovorPR2',
        'modelProperty': 'PrirodopolZovanie2_NumDogovorPR2',
        'type': 'TextField'
    }, {
        'dataIndex': 'NumZayvkaPR2',
        'modelProperty': 'PrirodopolZovanie2_NumZayvkaPR2',
        'type': 'NumberField'
    }, {
        'dataIndex': 'CodePR2',
        'modelProperty': 'PrirodopolZovanie2_CodePR2',
        'type': 'TextField'
    }, {
        'dataIndex': 'SummToPayPR2',
        'modelProperty': 'PrirodopolZovanie2_SummToPayPR2',
        'type': 'NumberField'
    }, {
        'dataIndex': 'IdentNumderPR2',
        'modelProperty': 'PrirodopolZovanie2_IdentNumderPR2',
        'type': 'TextField'
    }, {
        'dataIndex': 'IspolnitelPR2',
        'modelProperty': 'PrirodopolZovanie2_IspolnitelPR2',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'SoispolnitelPR2',
        'modelProperty': 'PrirodopolZovanie2_SoispolnitelPR2',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'MonthPR2',
        'modelProperty': 'PrirodopolZovanie2_MonthPR2',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'FIOZakazchikaPR2',
        'modelProperty': 'PrirodopolZovanie2_FIOZakazchikaPR2',
        'type': 'TextField'
    }, {
        'dataIndex': 'PrimechaniePR2',
        'modelProperty': 'PrirodopolZovanie2_PrimechaniePR2',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '77996565-8563-4a00-b4b6-5f7691a289cd-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'PrirodopolZovanie2Editor-container',
        'layout': {
            'type': 'anchor'
        },
        'padding': '10 10 10 10',
        items: [{
            'allowBlank': true,
            'anchor': '50%',
            'fieldLabel': 'Дата входящего',
            'format': 'd.m.Y',
            'modelProperty': 'PrirodopolZovanie2_DateInPR2',
            'name': 'DateInPR2',
            'rmsUid': '4fe7011e-f87e-461d-af1a-53ae5758691c',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'fieldLabel': 'Номер входящего',
            'modelProperty': 'PrirodopolZovanie2_NumInDocsPr2',
            'name': 'NumInDocsPr2',
            'rmsUid': '8f0c7a7a-5951-4704-b7df-a3fc3825c583',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'fieldLabel': 'Дата исполнения',
            'format': 'd.m.Y',
            'modelProperty': 'PrirodopolZovanie2_DateCompletePR2',
            'name': 'DateCompletePR2',
            'rmsUid': '74c71a89-f9b8-4239-9d0e-2913ea6c329d',
            'xtype': 'datefield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Компания Заказчик',
            'modelProperty': 'PrirodopolZovanie2_CompanyPR2',
            'name': 'CompanyPR2',
            'rmsUid': '83d8c5aa-189e-4bc7-a10f-babd85688f5d',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Вид работ',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.VidRabotPrirList',
            'listViewCtl': 'B4.controller.VidRabotPrirList',
            'maximizable': true,
            'model': 'B4.model.VidRabotPrirListModel',
            'modelProperty': 'PrirodopolZovanie2_VidRabotPR2',
            'name': 'VidRabotPR2',
            'rmsUid': '38a1db10-c634-418a-9185-3e84e8dbae17',
            'textProperty': 'vid_rabot_prirod_s',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Вид работ',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '50%',
            'decimalPrecision': 2,
            'fieldLabel': 'Количество',
            'minValue': 0,
            'modelProperty': 'PrirodopolZovanie2_QuantityPR2',
            'name': 'QuantityPR2',
            'rmsUid': '0b68cc06-0cfa-4b70-a111-80e63b5be7f5',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Объект',
            'height': 100,
            'modelProperty': 'PrirodopolZovanie2_ObjectPR2',
            'name': 'ObjectPR2',
            'rmsUid': '2c939258-9120-4ed8-a3c9-a1b409db3aaf',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'fieldLabel': 'Номер договора',
            'modelProperty': 'PrirodopolZovanie2_NumDogovorPR2',
            'name': 'NumDogovorPR2',
            'rmsUid': '82ae0b75-46bf-481b-b894-f1755cf5f518',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '50%',
            'decimalPrecision': 2,
            'fieldLabel': 'Номер заявки ',
            'minValue': 0,
            'modelProperty': 'PrirodopolZovanie2_NumZayvkaPR2',
            'name': 'NumZayvkaPR2',
            'rmsUid': '25761307-10b6-4532-9ab9-620f391e38c6',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'fieldLabel': 'Код',
            'modelProperty': 'PrirodopolZovanie2_CodePR2',
            'name': 'CodePR2',
            'rmsUid': 'aea7cdf1-cdf2-4a69-a676-099c8fd6d2f8',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'decimalPrecision': 2,
            'fieldLabel': 'Сумма к оплате',
            'minValue': 0,
            'modelProperty': 'PrirodopolZovanie2_SummToPayPR2',
            'name': 'SummToPayPR2',
            'rmsUid': '0a974463-7b3c-4226-a4c8-eeaa013e58b8',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'fieldLabel': 'Идентифика-ционный номер',
            'modelProperty': 'PrirodopolZovanie2_IdentNumderPR2',
            'name': 'IdentNumderPR2',
            'rmsUid': '948a3c02-651f-49ea-b796-3107c7152b31',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'editable': false,
            'fieldLabel': 'Исполнитель',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.Sotrudniki1List',
            'listViewCtl': 'B4.controller.Sotrudniki1List',
            'maximizable': true,
            'model': 'B4.model.Sotrudniki1ListModel',
            'modelProperty': 'PrirodopolZovanie2_IspolnitelPR2',
            'name': 'IspolnitelPR2',
            'rmsUid': '6c2e8c02-9b1f-411a-b5ff-55ac4e447d50',
            'textProperty': 'Element1474352136232',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Исполнитель',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'editable': false,
            'fieldLabel': 'Соисполнитель',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.Sotrudniki1List',
            'listViewCtl': 'B4.controller.Sotrudniki1List',
            'maximizable': true,
            'model': 'B4.model.Sotrudniki1ListModel',
            'modelProperty': 'PrirodopolZovanie2_SoispolnitelPR2',
            'name': 'SoispolnitelPR2',
            'rmsUid': 'd775a307-6c91-43ed-8b8c-6d8f62737a46',
            'textProperty': 'Element1474352136232',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Соисполнитель',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'editable': false,
            'fieldLabel': 'Месяц',
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.MesjacPrirodaList',
            'listViewCtl': 'B4.controller.MesjacPrirodaList',
            'maximizable': true,
            'model': 'B4.model.MesjacPrirodaListModel',
            'modelProperty': 'PrirodopolZovanie2_MonthPR2',
            'name': 'MonthPR2',
            'rmsUid': '45d8a46f-74e4-4e72-b88f-dac437cf5923',
            'textProperty': 'Id',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Месяц',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '50%',
            'fieldLabel': 'ФИО заказчика',
            'modelProperty': 'PrirodopolZovanie2_FIOZakazchikaPR2',
            'name': 'FIOZakazchikaPR2',
            'rmsUid': '5af7e153-ba45-407b-bf02-95e1398bf89f',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Примечание',
            'modelProperty': 'PrirodopolZovanie2_PrimechaniePR2',
            'name': 'PrimechaniePR2',
            'rmsUid': '69d56300-ff21-4f47-b9cb-8ae20f67bb28',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'PrirodopolZovanie2_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('NumInDocsPr2'));
        } else {}
        return res;
    },
});