Ext.define('B4.view.PodvidRabotPrirodaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-podvidrabotprirodaeditor',
    title: 'Форма редактирования Подвид работ природа',
    rmsUid: '05ad296c-a1b1-4cb3-b207-b98a0b584bc0',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'PodvidRabotPriroda_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Podvid_rab_pr',
        'modelProperty': 'PodvidRabotPriroda_Podvid_rab_pr',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '05ad296c-a1b1-4cb3-b207-b98a0b584bc0-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'PodvidRabotPrirodaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Идентификатор',
            'margin': '10 200 10 10',
            'modelProperty': '1d41a6dd-604b-45b8-b2c7-ac742b8ca951',
            'readOnly': true,
            'rmsUid': '1d41a6dd-604b-45b8-b2c7-ac742b8ca951',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Подвид работ ',
            'margin': '10 10 10 10',
            'modelProperty': 'PodvidRabotPriroda_Podvid_rab_pr',
            'name': 'Podvid_rab_pr',
            'rmsUid': 'e65e3a4c-3b80-46cb-9711-2105107277b1',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'PodvidRabotPriroda_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});