Ext.define('B4.view.StatusySubEktaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-statusysubektaeditor',
    title: 'Форма редактирования Статусы субъекта',
    rmsUid: 'e23e4a32-5a36-4c14-90a7-33b2ede3434e',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'StatusySubEkta_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'StatusySubEkta_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'StatusySubEkta_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'e23e4a32-5a36-4c14-90a7-33b2ede3434e-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'StatusySubEktaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'StatusySubEkta_code',
            'name': 'code',
            'rmsUid': '0e1cd156-5477-4494-9050-f59734de2550',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'StatusySubEkta_name',
            'name': 'name',
            'rmsUid': '0d04d6e5-0ce0-4d38-bb20-8015cd3d452c',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'StatusySubEkta_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});