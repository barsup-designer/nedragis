Ext.define('B4.view.Test123Editor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-test123editor',
    title: 'Форма редактирования test123',
    rmsUid: '290cecfd-e36d-4e7c-b3f1-3df99cb7c7f3',
    requires: [
        'B4.view.SotrudnikiList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Test123_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name1234',
        'modelProperty': 'Test123_name1234',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '290cecfd-e36d-4e7c-b3f1-3df99cb7c7f3-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'Test123Editor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': false,
            'anchor': '100%',
            'fieldLabel': 'ФИО',
            'modelProperty': 'Test123_name1234',
            'name': 'name1234',
            'rmsUid': '78e7df18-6a28-4bbe-9b74-17a798f0e1c5',
            'xtype': 'textfield'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'anchor': '100%',
                'dockedItems': [],
                'fieldLabel': 'Панель',
                'items': [{
                    'closable': false,
                    'header': false,
                    'height': 200,
                    'margin': '0 0 5 0',
                    'name': 'SotrudnikiList',
                    'rmsUid': 'e4d2eea2-df44-4239-9dae-11cfe3ca30a4',
                    'title': null,
                    'xtype': 'rms-sotrudnikilist'
                }],
                'layout': {
                    'type': 'fit'
                },
                'rmsUid': 'c64e2706-868b-4199-9c57-a5d631dddc0b',
                'title': 'Панель',
                'xtype': 'panel'
            }],
            'rmsUid': 'dc902e6e-347c-4ef4-9be7-6308f5722701',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Test123_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
        me.grid_SotrudnikiList = me.down('rms-sotrudnikilist[name=SotrudnikiList]');
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});