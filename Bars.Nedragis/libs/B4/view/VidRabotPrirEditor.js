Ext.define('B4.view.VidRabotPrirEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-vidrabotprireditor',
    title: 'Форма редактирования Вид работ природа',
    rmsUid: '4979193c-53df-4acd-817a-37eccf59f986',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'VidRabotPrir_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'vid_rabot_prirod_s',
        'modelProperty': 'VidRabotPrir_vid_rabot_prirod_s',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '4979193c-53df-4acd-817a-37eccf59f986-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'VidRabotPrirEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'fieldCls': 'x-form-field x-form-field-custom',
            'fieldLabel': 'Идентификатор',
            'margin': '20 20 20 20',
            'modelProperty': '55a7ae42-89b6-48ab-a83b-042fdc1e6c15',
            'readOnly': true,
            'rmsUid': '55a7ae42-89b6-48ab-a83b-042fdc1e6c15',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Вид работ',
            'margin': '20 20 20 20',
            'modelProperty': 'VidRabotPrir_vid_rabot_prirod_s',
            'name': 'vid_rabot_prirod_s',
            'rmsUid': 'b72ba696-c1a1-44f0-b5cd-896dc2888aa4',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'VidRabotPrir_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});