Ext.define('B4.view.RukovoditeliEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-rukovoditelieditor',
    title: 'Форма редактирования Руководители',
    rmsUid: '6edae791-3f32-48d2-9478-ddcb67a712c3',
    requires: [
        'B4.enums.Pol',
        'B4.form.EnumCombo',
        'B4.form.PickerField',
        'B4.model.DokumentyUdostoverjajushhieLichnostEditorModel',
        'B4.model.DokumentyUdostoverjajushhieLichnostListModel',
        'B4.model.DolzhnostiEditorModel',
        'B4.model.DolzhnostiListModel',
        'B4.model.JuLRedaktirovanieModel',
        'B4.model.JuridicheskieLicaListModel',
        'B4.view.DokumentyUdostoverjajushhieLichnostList',
        'B4.view.DolzhnostiList',
        'B4.view.JuridicheskieLicaList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Rukovoditeli_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Surname',
        'modelProperty': 'Rukovoditeli_Surname',
        'type': 'TextField'
    }, {
        'dataIndex': 'FirstName',
        'modelProperty': 'Rukovoditeli_FirstName',
        'type': 'TextField'
    }, {
        'dataIndex': 'Patronymic',
        'modelProperty': 'Rukovoditeli_Patronymic',
        'type': 'TextField'
    }, {
        'dataIndex': 'Sex',
        'modelProperty': 'Rukovoditeli_Sex',
        'type': 'DropdownField'
    }, {
        'dataIndex': 'PostType',
        'modelProperty': 'Rukovoditeli_PostType',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'PostText',
        'modelProperty': 'Rukovoditeli_PostText',
        'type': 'TextField'
    }, {
        'dataIndex': 'Phone',
        'modelProperty': 'Rukovoditeli_Phone',
        'type': 'TextField'
    }, {
        'dataIndex': 'Basis',
        'modelProperty': 'Rukovoditeli_Basis',
        'type': 'TextField'
    }, {
        'dataIndex': 'udostovereniye',
        'modelProperty': 'Rukovoditeli_udostovereniye',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'YurLitco',
        'modelProperty': 'Rukovoditeli_YurLitco',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '6edae791-3f32-48d2-9478-ddcb67a712c3-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'RukovoditeliEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'fieldLabel': 'Фамилия',
                'flex': 2,
                'labelWidth': 100,
                'margin': '10 5 0 10',
                'modelProperty': 'Rukovoditeli_Surname',
                'name': 'Surname',
                'rmsUid': '527f1c4d-258b-472c-95d6-bff9150d890a',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Имя',
                'flex': 1,
                'labelWidth': 25,
                'margin': '10 5 0 5',
                'modelProperty': 'Rukovoditeli_FirstName',
                'name': 'FirstName',
                'rmsUid': '4e6db2a1-d046-42af-8092-656d32947f0a',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Отчество',
                'flex': 1,
                'labelWidth': 55,
                'margin': '10 5 0 5',
                'modelProperty': 'Rukovoditeli_Patronymic',
                'name': 'Patronymic',
                'rmsUid': '347a5f77-031c-43ae-9e60-239fec0a10e1',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'enumName': 'B4.enums.Pol',
                'fieldLabel': 'Пол',
                'flex': 1,
                'labelWidth': 25,
                'marginLeft': 5,
                'marginRight': 5,
                'marginTop': 10,
                'modelProperty': 'Rukovoditeli_Sex',
                'name': 'Sex',
                'queryMode': 'local',
                'rmsUid': '7857d423-8b29-4681-9cab-77b5eb3aa0ad',
                'xtype': 'b4enumcombo'
            }],
            'layout': {
                'type': 'hbox'
            },
            'margin': '0 5 5 0',
            'rmsUid': 'e7c4c5dc-45b3-4997-89e7-7434226cb74d',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Тип должности',
                'flex': 1,
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.DolzhnostiList',
                'listViewCtl': 'B4.controller.DolzhnostiList',
                'margin': '10 10 0 10',
                'maximizable': true,
                'model': 'B4.model.DolzhnostiListModel',
                'modelProperty': 'Rukovoditeli_PostType',
                'name': 'PostType',
                'rmsUid': '2b3af2fd-7c77-4c45-93a2-2a02ca907e0c',
                'textProperty': 'code',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Тип должности',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Должность',
                'flex': 1,
                'margin': '15 10 0 10',
                'modelProperty': 'Rukovoditeli_PostText',
                'name': 'PostText',
                'rmsUid': '78aa727a-04a6-46f9-8f95-36b281c1ab50',
                'xtype': 'textfield'
            }],
            'layout': {
                'type': 'fit'
            },
            'rmsUid': 'aa468462-8f49-4cb1-8243-850ab85c95bc',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Телефон',
                    'flex': 1,
                    'margin': '22 10 0 10',
                    'modelProperty': 'Rukovoditeli_Phone',
                    'name': 'Phone',
                    'rmsUid': '2e695ac8-3f37-426e-ab6b-43464ee5c02d',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': '1e9cc318-aab9-4ca9-b303-4ff8b6fab0e7',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Действует на основании',
                    'flex': 1,
                    'labelWidth': 70,
                    'margin': '10 10 0 10',
                    'modelProperty': 'Rukovoditeli_Basis',
                    'name': 'Basis',
                    'rmsUid': '99ce7793-2a0f-48b1-90a2-d11a240b8385',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'ce3e4fd4-1388-4d54-a8bf-37387d34ba2f',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '97e0af3d-cc68-4512-b514-43e6b4e9c200',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'items': [{
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Документ, удостоверяющий личность',
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.DokumentyUdostoverjajushhieLichnostList',
                'listViewCtl': 'B4.controller.DokumentyUdostoverjajushhieLichnostList',
                'margin': '5 10 0 10',
                'maximizable': true,
                'model': 'B4.model.DokumentyUdostoverjajushhieLichnostListModel',
                'modelProperty': 'Rukovoditeli_udostovereniye',
                'name': 'udostovereniye',
                'rmsUid': 'f811fa78-e8d5-4d3f-9225-13c1ce8096f4',
                'textProperty': 'code',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Документ, удостоверяющий личность',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }, {
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Юридическое лицо',
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.JuridicheskieLicaList',
                'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                'margin': '3 10 0 10',
                'maximizable': true,
                'model': 'B4.model.JuridicheskieLicaListModel',
                'modelProperty': 'Rukovoditeli_YurLitco',
                'name': 'YurLitco',
                'rmsUid': '340be21a-4ff2-4c68-8807-19259c32e9fc',
                'textProperty': 'OKATO_name1',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Юридическое лицо',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }],
            'layout': {
                'type': 'fit'
            },
            'rmsUid': 'b50975fd-3a21-4c51-8733-3eb23f11d642',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Rukovoditeli_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle('Редактирование руководителя');
        } else {
            me.setTitle('Добавление руководителя');
        }
        return res;
    },
});