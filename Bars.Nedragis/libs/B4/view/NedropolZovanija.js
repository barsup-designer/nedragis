Ext.define('B4.view.NedropolZovanija', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-nedropolzovanija',
    title: 'Форма отдела "Недропользования"',
    rmsUid: 'bd53db70-595b-41a0-98d7-2636aa6605bd',
    requires: [
        'B4.form.PickerField',
        'B4.model.Otdel1EditorModel',
        'B4.model.Otdel1ListModel',
        'B4.view.Otdel1List'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'monitor_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'otdel',
        'modelProperty': 'monitor_otdel',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Dela_o',
        'modelProperty': 'monitor_Dela_o',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Dela_od',
        'modelProperty': 'monitor_Dela_od',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Karotazs_o',
        'modelProperty': 'monitor_Karotazs_o',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Karotazs_d',
        'modelProperty': 'monitor_Karotazs_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Karotazc',
        'modelProperty': 'monitor_Karotazc',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Karotazc_d',
        'modelProperty': 'monitor_Karotazc_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'geolog_otchet',
        'modelProperty': 'monitor_geolog_otchet',
        'type': 'NumberField'
    }, {
        'dataIndex': 'geolog_otchet_d',
        'modelProperty': 'monitor_geolog_otchet_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1462359767016',
        'modelProperty': 'monitor_Element1462359767016',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Element1462359769527',
        'modelProperty': 'monitor_Element1462359769527',
        'type': 'NumberField'
    }, {
        'dataIndex': 'seismika_otchet',
        'modelProperty': 'monitor_seismika_otchet',
        'type': 'NumberField'
    }, {
        'dataIndex': 'seismika_otchet_d',
        'modelProperty': 'monitor_seismika_otchet_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'seismika',
        'modelProperty': 'monitor_seismika',
        'type': 'NumberField'
    }, {
        'dataIndex': 'seismika_d',
        'modelProperty': 'monitor_seismika_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'zapas',
        'modelProperty': 'monitor_zapas',
        'type': 'NumberField'
    }, {
        'dataIndex': 'zapas_d',
        'modelProperty': 'monitor_zapas_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'koor',
        'modelProperty': 'monitor_koor',
        'type': 'NumberField'
    }, {
        'dataIndex': 'koor_d',
        'modelProperty': 'monitor_koor_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'kub',
        'modelProperty': 'monitor_kub',
        'type': 'NumberField'
    }, {
        'dataIndex': 'kub_d',
        'modelProperty': 'monitor_kub_d',
        'type': 'NumberField'
    }, {
        'dataIndex': 'karta820',
        'modelProperty': 'monitor_karta820',
        'type': 'NumberField'
    }, {
        'dataIndex': 'karta_d',
        'modelProperty': 'monitor_karta_d',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'bd53db70-595b-41a0-98d7-2636aa6605bd-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'NedropolZovanija-container',
        'autoScroll': true,
        'layout': {
            'type': 'anchor'
        },
        'width': 200,
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Наименование отдела',
            'idProperty': 'Id',
            'isContextAware': true,
            'labelWidth': 150,
            'listView': 'B4.view.Otdel1List',
            'listViewCtl': 'B4.controller.Otdel1List',
            'margin': '10 35 0 35',
            'maximizable': true,
            'model': 'B4.model.Otdel1ListModel',
            'modelProperty': 'monitor_otdel',
            'name': 'otdel',
            'rmsUid': 'dc699826-6dfa-41ea-8be7-7765f6889497',
            'textProperty': 'NAME_SP',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Наименование отдела',
                'width': 450
            },
            'xtype': 'b4pickerfield'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'autoScroll': true,
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Дела скважин',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'columnWidth': '0.20',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelAlign': 'left',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_Dela_o',
                        'name': 'Dela_o',
                        'rmsUid': '65234051-bc16-40bf-9a5f-5688892a8311',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_Dela_od',
                        'name': 'Dela_od',
                        'rmsUid': '94705789-fdeb-4b32-a05a-2cfbe610a649',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': '66cd3d1b-11ac-4e9b-ad92-5e144b0344a0',
                    'title': 'Дела скважин',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Каротаж (сканобраз)',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_Karotazs_o',
                        'name': 'Karotazs_o',
                        'rmsUid': 'e1cf3259-c6cc-468e-b864-e5b84f7a407e',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_Karotazs_d',
                        'name': 'Karotazs_d',
                        'rmsUid': 'b3011697-7234-412a-a447-fe8c26713107',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': '49dcd953-c399-4709-9b96-43c8f5c7d355',
                    'title': 'Каротаж (сканобраз)',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Каротаж (оцифрованный формат LAS)',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_Karotazc',
                        'name': 'Karotazc',
                        'rmsUid': 'b8b32a23-8c1c-4f58-a5c1-69e9b937a088',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_Karotazc_d',
                        'name': 'Karotazc_d',
                        'rmsUid': '808a4673-5778-4622-9fcc-bc453b247b5b',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': 'b62245de-82c2-44f6-a65e-6fc9b6111e36',
                    'title': 'Каротаж (оцифрованный формат LAS)',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Геологические отчеты',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_geolog_otchet',
                        'name': 'geolog_otchet',
                        'rmsUid': '827c8e43-147a-413f-920a-a6050f2fdf27',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_geolog_otchet_d',
                        'name': 'geolog_otchet_d',
                        'rmsUid': '5c903261-9d93-4437-94fb-f7f88ab18a67',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': 'e1bf22b9-39a6-4cf0-8cf1-7120a3658f46',
                    'title': 'Геологические отчеты',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Сейсмика: исходный полевой материал',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_Element1462359767016',
                        'name': 'Element1462359767016',
                        'rmsUid': 'decc52eb-66cf-4c7f-a571-5d6a94a7e98d',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_Element1462359769527',
                        'name': 'Element1462359769527',
                        'rmsUid': '6cb1bf1f-7992-4bca-bea8-9c5be5ba7a11',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': '6dd6c57a-901a-4373-bdae-debd46f098bb',
                    'title': 'Сейсмика: исходный полевой материал',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Сейсмические отчеты с графическим материалом',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_seismika_otchet',
                        'name': 'seismika_otchet',
                        'rmsUid': '3344683c-886b-4a84-823e-0a38d9b3d60b',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_seismika_otchet_d',
                        'name': 'seismika_otchet_d',
                        'rmsUid': '91b3802b-aba4-4252-8254-24e08a54e0b2',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': '76acf645-5e82-4445-bff3-33e5da58318b',
                    'title': 'Сейсмические отчеты с графическим материалом',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Сейсмические отчеты без графического материала',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_seismika',
                        'name': 'seismika',
                        'rmsUid': '563dc628-6a2f-41b9-9fba-80be74382357',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_seismika_d',
                        'name': 'seismika_d',
                        'rmsUid': '400642da-2188-4e14-8822-27e1b814c418',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': '734181b7-b276-4c74-ada1-58ce1ea7a954',
                    'title': 'Сейсмические отчеты без графического материала',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Планы подсчетов запасов нефти, газа и конденсата',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_zapas',
                        'name': 'zapas',
                        'rmsUid': '07183dc6-5e78-4693-b501-b0426319da32',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_zapas_d',
                        'name': 'zapas_d',
                        'rmsUid': '18efe426-6709-4fac-8960-d4b1efbf0611',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': '311b2c49-ad35-45be-b332-2c235338a064',
                    'title': 'Планы подсчетов запасов нефти, газа и конденсата',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Координаты скважины и альтитуды',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_koor',
                        'name': 'koor',
                        'rmsUid': '02affdb7-960b-4d32-af04-594826a56505',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_koor_d',
                        'name': 'koor_d',
                        'rmsUid': 'ef59cad2-85ee-4bbd-8949-ea389eaeed42',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': '5d616bcd-a56a-4ae6-886f-14fb5304f82a',
                    'title': 'Координаты скважины и альтитуды',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Сейсмика: сейсмические   временные разрезы/кубы',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_kub',
                        'name': 'kub',
                        'rmsUid': '27bbd992-319e-4614-b241-c6fb24831683',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_kub_d',
                        'name': 'kub_d',
                        'rmsUid': '37c3736e-cef7-4403-99bf-ba1b0229684f',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': '1cdd3ac3-761d-4f12-90ee-0849e6d06ef1',
                    'title': 'Сейсмика: сейсмические   временные разрезы/кубы',
                    'xtype': 'fieldset'
                }, {
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Региональные структурные карты',
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Количество обращений',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_karta820',
                        'name': 'karta820',
                        'rmsUid': '95d79017-e8e6-458f-884c-62c616b6b72f',
                        'step': 1,
                        'xtype': 'numberfield'
                    }, {
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Денежные средства',
                        'labelWidth': 150,
                        'margin': '0 60 0 60',
                        'minValue': 0,
                        'modelProperty': 'monitor_karta_d',
                        'name': 'karta_d',
                        'rmsUid': 'fe8b5216-1395-4f91-8ce8-e085185b29f7',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'rmsUid': 'fd921153-fc49-4fa4-b7b7-1f3227d9de9b',
                    'title': 'Региональные структурные карты',
                    'xtype': 'fieldset'
                }],
                'layout': 'anchor',
                'margin': '0 10 0 10',
                'rmsUid': '4aefbc57-60b4-4cf9-af06-f804c1026f68',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '10 10 0 10',
            'rmsUid': '296d9b8c-f38b-46de-a6c4-99bdcb990cb8',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'monitor_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});