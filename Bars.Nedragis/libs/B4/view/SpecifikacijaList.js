Ext.define('B4.view.SpecifikacijaList', {
    'alias': 'widget.rms-specifikacijalist',
    'dataSourceUid': '640f4644-e8f4-4163-978a-3065538260cc',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.SpecifikacijaListModel',
    'stateful': true,
    'title': 'Реестр Спецификация',
    requires: [
        'B4.model.SpecifikacijaListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': '065d0881-79a1-4677-a182-27f4897d7ddc',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-SpecifikacijaEditor-InWindow',
        'rmsUid': 'aebfbbb2-54b1-4848-b4d9-b95fa0c74ec0',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-SpecifikacijaEditor-InWindow',
        'rmsUid': '4c6fce91-0614-498f-9658-efa0ca608465',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': '46499bc4-fde0-43c3-892b-d9f2dbf06591',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Id',
                'dataIndexAbsoluteUid': 'FieldPath://640f4644-e8f4-4163-978a-3065538260cc$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '15ddb2d8-f408-4606-8790-a38bc0609712',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'nomsp',
                'dataIndexAbsoluteUid': 'FieldPath://640f4644-e8f4-4163-978a-3065538260cc$6908ec42-aacc-41a3-b10f-81a6105a3679',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '8b055112-beec-4108-851a-cb455aff8597',
                'sortable': true,
                'text': 'Номер спецификации',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1478601989143',
                'dataIndexAbsoluteUid': 'FieldPath://640f4644-e8f4-4163-978a-3065538260cc$f3ffae9c-bc3b-4c33-b690-feb28490986d',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '8b6ededc-8527-45dd-8d92-daaadceae350',
                'sortable': true,
                'text': 'Дата спецификации',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1478607128570',
                'dataIndexAbsoluteUid': 'FieldPath://640f4644-e8f4-4163-978a-3065538260cc$01006813-3c42-4c54-b53e-45c39e432b18',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'bff2855f-bfad-4a3c-b599-82e12c94d6d2',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Номер заявки',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1478602079636',
                'dataIndexAbsoluteUid': 'FieldPath://640f4644-e8f4-4163-978a-3065538260cc$0e196576-a92d-4f2b-adc7-bd34847f8563',
                'decimalPrecision': 2,
                'filter': {
                    'defaults': {
                        'allowDecimals': true,
                        'xtype': 'numberfield'
                    },
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'format': '0,000.00',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '9f4eab64-0b69-4688-992f-ba7de197e9b9',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Итоговая сумма',
                'xtype': 'numbercolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});