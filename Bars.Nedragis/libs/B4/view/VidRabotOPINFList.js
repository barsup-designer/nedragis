Ext.define('B4.view.VidRabotOPINFList', {
    'alias': 'widget.rms-vidrabotopinflist',
    'dataSourceUid': 'd718884f-b8e6-4ddd-a77f-109da91c10ae',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.VidRabotOPINFListModel',
    'stateful': true,
    'title': 'Реестр Вид работ ОПИНФ',
    requires: [
        'B4.model.VidRabotOPINFListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': null,
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-VidRabotOPINFEditor-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-VidRabotOPINFEditor-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'VidRabotOPINF222',
                'dataIndexAbsoluteUid': 'FieldPath://d718884f-b8e6-4ddd-a77f-109da91c10ae$c2c721fc-08af-4176-a058-c8add2eb2efa',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '083c1a7c-a2c9-4363-a2a4-ca1e1c363856',
                'sortable': true,
                'text': 'Вид работ',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Element1510134562537',
                'dataIndexAbsoluteUid': 'FieldPath://d718884f-b8e6-4ddd-a77f-109da91c10ae$824011ee-c2df-41ef-b2d0-a051aba5c1cd',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '86c55e25-54f0-4dec-a5b6-3fe90f02f19b',
                'sortable': true,
                'text': 'Подвид работ',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});