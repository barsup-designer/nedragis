Ext.define('B4.view.StatusyObEktaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-statusyobektaeditor',
    title: 'Форма редактирования Статусы объекта',
    rmsUid: '384cb7d4-0f1b-41f7-8a5d-45c24c12b689',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'StatusyObEkta_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'StatusyObEkta_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'StatusyObEkta_name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '384cb7d4-0f1b-41f7-8a5d-45c24c12b689-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'StatusyObEktaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'fieldLabel': 'Код',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'StatusyObEkta_code',
            'name': 'code',
            'rmsUid': 'f73bac42-91a0-4e7e-a25c-b8b3c47279aa',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование',
            'flex': 1,
            'height': 20,
            'margin': '10 10 10 10',
            'modelProperty': 'StatusyObEkta_name',
            'name': 'name',
            'rmsUid': '6086f7ed-bd73-491a-85f1-46c8da2958dc',
            'width': 10,
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'StatusyObEkta_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});