Ext.define('B4.view.OkhnanaOtmosfernogoVozdukhaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-okhnanaotmosfernogovozdukhaeditor',
    title: 'Форма редактирования Охрана атмосферного воздуха',
    rmsUid: '1c7d632c-b5b4-4dd3-aef1-5b2f68c8dfee',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1507788951680',
        'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Element1507788951680',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507791810720',
        'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Element1507791810720',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507788999087',
        'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Element1507788999087',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507789018715',
        'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Element1507789018715',
        'type': 'TextField'
    }, {
        'dataIndex': 'Element1507789044428',
        'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Element1507789044428',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '1c7d632c-b5b4-4dd3-aef1-5b2f68c8dfee-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OkhnanaOtmosfernogoVozdukhaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Название стационарного источника загрязнения',
            'margin': '10 15 0 15',
            'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Element1507788951680',
            'name': 'Element1507788951680',
            'rmsUid': '599da98d-0d67-473d-91cd-1f4fae31ec6f',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код свидетельства',
            'margin': '0 15 0 15',
            'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Element1507791810720',
            'name': 'Element1507791810720',
            'rmsUid': '677e52fd-ab57-4f7a-a9bc-ca9676d6b551',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Местонахождение',
            'margin': '0 15 0 15',
            'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Element1507788999087',
            'name': 'Element1507788999087',
            'rmsUid': '0f022cd0-2a82-424d-a9b2-3b31dabab0e5',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наличие тома ПДВ',
            'margin': '0 15 0 15',
            'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Element1507789018715',
            'name': 'Element1507789018715',
            'rmsUid': 'd3eefb82-f495-47f3-9ca9-2833824d8e08',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Разрешение на выброс в атмосферу',
            'margin': '0 15 0 15',
            'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Element1507789044428',
            'name': 'Element1507789044428',
            'rmsUid': 'cae99da1-fcbe-4d8c-b184-1c84a5132070',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'OkhnanaOtmosfernogoVozdukha_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Element1507788951680'));
        } else {}
        return res;
    },
});