Ext.define('B4.view.DokumentEditor_Copy_1', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-dokumenteditor_copy_1',
    title: 'Основные сведения Документ ',
    rmsUid: '67034bbd-85ba-45eb-85ad-3ebdfc5ebd8b',
    requires: [
        'B4.form.PickerField',
        'B4.model.JuLRedaktirovanieModel',
        'B4.model.JuridicheskieLicaListModel',
        'B4.model.TipyDokumentovEditorModel',
        'B4.model.TipyDokumentovListModel',
        'B4.view.JuridicheskieLicaList',
        'B4.view.TipyDokumentovList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Dokument_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Type',
        'modelProperty': 'Dokument_Type',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Date',
        'modelProperty': 'Dokument_Date',
        'type': 'DateField'
    }, {
        'dataIndex': 'Number',
        'modelProperty': 'Dokument_Number',
        'type': 'TextField'
    }, {
        'dataIndex': 'ActualDate',
        'modelProperty': 'Dokument_ActualDate',
        'type': 'DateField'
    }, {
        'dataIndex': 'Basis',
        'modelProperty': 'Dokument_Basis',
        'type': 'TextField'
    }, {
        'dataIndex': 'Signatory',
        'modelProperty': 'Dokument_Signatory',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Subject',
        'modelProperty': 'Dokument_Subject',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '67034bbd-85ba-45eb-85ad-3ebdfc5ebd8b-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'DokumentEditor_Copy_1-container',
        'layout': {
            'type': 'anchor'
        },
        'region': 'Center',
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Тип документа',
                    'flex': 1,
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'listView': 'B4.view.TipyDokumentovList',
                    'listViewCtl': 'B4.controller.TipyDokumentovList',
                    'margin': '10 10 10 0',
                    'maximizable': true,
                    'model': 'B4.model.TipyDokumentovListModel',
                    'modelProperty': 'Dokument_Type',
                    'name': 'Type',
                    'rmsUid': 'a8749fcd-8b0d-4655-b9f5-e3c0af2fa223',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Тип документа',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Дата документа',
                    'flex': 1,
                    'format': 'd.m.Y',
                    'margin': '0 10 0 0',
                    'modelProperty': 'Dokument_Date',
                    'name': 'Date',
                    'rmsUid': 'c16e779d-305c-4e06-a140-82b4df0f3bac',
                    'xtype': 'datefield'
                }],
                'layout': 'anchor',
                'margin': '0 0 0 10',
                'rmsUid': '14c2db52-e850-43eb-be81-a6ac000615d8',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Номер документа',
                    'flex': 1,
                    'margin': '5 0 5 0',
                    'modelProperty': 'Dokument_Number',
                    'name': 'Number',
                    'rmsUid': 'cb3b87ed-7513-4975-bc75-153917085536',
                    'xtype': 'textfield'
                }, {
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Дата вступления в силу',
                    'format': 'd.m.Y',
                    'modelProperty': 'Dokument_ActualDate',
                    'name': 'ActualDate',
                    'rmsUid': '2d263061-3ad0-45d6-b64b-9edffe3d3fb5',
                    'xtype': 'datefield'
                }],
                'layout': 'anchor',
                'margin': '0 10 0 10',
                'rmsUid': 'b9b1721c-3c95-4f32-8023-175b3774769f',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '0 0 10 0',
            'rmsUid': '7d539ca7-4060-4a85-9e67-61010c9c2f82',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'fieldLabel': 'Основание',
                'flex': 1,
                'margin': '10 10 10 10',
                'modelProperty': 'Dokument_Basis',
                'name': 'Basis',
                'rmsUid': '6a23a366-39e9-40b4-bdd5-d8bcfd6ee444',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Орган, подписавший документ',
                'flex': 1,
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.JuridicheskieLicaList',
                'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                'margin': '10 10 0 10',
                'maximizable': true,
                'model': 'B4.model.JuridicheskieLicaListModel',
                'modelProperty': 'Dokument_Signatory',
                'name': 'Signatory',
                'rmsUid': '43033e48-e34e-4f21-b674-2c4d4d9e86f4',
                'textProperty': 'Representation',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Орган, подписавший документ',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }, {
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Юридическое лицо',
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.JuridicheskieLicaList',
                'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                'margin': '10 10 10 10',
                'maximizable': true,
                'model': 'B4.model.JuridicheskieLicaListModel',
                'modelProperty': 'Dokument_Subject',
                'name': 'Subject',
                'rmsUid': '12844f70-0076-4410-8fcf-54b6c75c7e32',
                'textProperty': 'OKATO_name1',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Юридическое лицо',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }],
            'layout': {
                'type': 'fit'
            },
            'margin': '0 0 5 0',
            'rmsUid': '8205ebb1-34d7-452b-8a0f-471f12999e18',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Dokument_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle('Изменение документа');
        } else {
            me.setTitle('Создание документа');
        }
        return res;
    },
});