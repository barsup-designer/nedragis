Ext.define('B4.view.JuLRedaktirovanie', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-julredaktirovanie',
    title: 'ЮЛ редактирование - навигация',
    rmsUid: '7e363d6f-723a-4ba0-9ef6-5ccdb915a46c',
    requires: [
        'B4.view.NavigationPanel'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'JuridicheskieLica_Id',
        'type': 'hidden'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '7e363d6f-723a-4ba0-9ef6-5ccdb915a46c-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'JuLRedaktirovanie-container',
        'autoScroll': true,
        'layout': {
            'type': 'fit'
        },
        items: [{
            'border': false,
            'fieldLabel': 'Панель навигации',
            'items': [],
            'rmsUid': '509910ca-f47b-4029-b06a-8a5b91d4cdc0',
            'rootKey': '509910ca-f47b-4029-b06a-8a5b91d4cdc0',
            'title': null,
            'treeWidth': 250,
            'xtype': 'rms-navigationpanel'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'JuridicheskieLica_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Representation'));
        } else {}
        return res;
    },
});