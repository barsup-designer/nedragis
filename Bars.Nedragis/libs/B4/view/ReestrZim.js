Ext.define('B4.view.ReestrZim', {
    'alias': 'widget.rms-reestrzim',
    'dataSourceUid': '65ab9d8b-bf41-47db-9196-b2c564e8e49f',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': false,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.ReestrZimModel',
    'stateful': true,
    'title': 'Reestr zim',
    requires: [
        'B4.model.ReestrZimModel',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'add',
        'hidden': false,
        'iconCls': null,
        'itemId': 'Addition-FormaZim-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': null,
        'itemId': 'Editing-FormaZim-InWindow',
        'rmsUid': null,
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': null,
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'zzz',
                'dataIndexAbsoluteUid': 'FieldPath://65ab9d8b-bf41-47db-9196-b2c564e8e49f$cd7d2787-5464-4350-b280-7dc2ef45e80b$faa39da7-bb2a-496f-ab65-c8d1b7068ceb',
                'dataIndexRelativePath': 'Element1517482864074.Element1517294546876',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5353a183-02e0-4a46-ad44-e8425d8cd64a',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'наименование.zima2',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }]
        });
        me.callParent(arguments);
    }
});