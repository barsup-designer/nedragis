Ext.define('B4.view.monitorList', {
    'alias': 'widget.rms-monitorlist',
    'dataSourceUid': '293381ae-d0ca-4bbf-8b8c-0e2c31bd13c4',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.monitorListModel',
    'stateful': true,
    'title': 'Реестр Мониторинг',
    requires: [
        'B4.model.monitorListModel',
        'B4.ux.grid.plugin.ExportExcel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'content-filter-bar-images-add-png',
        'itemId': 'Addition-TestForma-InWindow',
        'rmsUid': null,
        'text': 'Добавить'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': null,
        'text': 'Удалить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-edit-png',
        'itemId': 'Editing-NedropolZovanija-InWindow',
        'rmsUid': null,
        'text': 'Редактировать Недропользование'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-edit-png',
        'itemId': 'Editing-PrirodopolZovanie-InWindow',
        'rmsUid': null,
        'text': 'Редактировать Природопользование'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-edit-png',
        'itemId': 'Editing-Zemleustrojjstva-InWindow',
        'rmsUid': null,
        'text': 'Редактировать Землеустройства'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-olap-themes-default-img-icons-edit-png',
        'itemId': 'Editing-Kartografija-InWindow',
        'rmsUid': null,
        'text': 'Редактировать Картография'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'otdel_NAME_SP',
                'dataIndexAbsoluteUid': 'FieldPath://293381ae-d0ca-4bbf-8b8c-0e2c31bd13c4$398295bc-f5eb-4ed2-b4de-d5f97e28d9da$c9cfc806-3d3e-4aa7-b12c-d9218426a9d7',
                'dataIndexRelativePath': 'otdel.NAME_SP',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '3b6408ca-d871-4305-9750-71b8ea890ac6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование отдела',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'ObjectCreateDate',
                'dataIndexAbsoluteUid': 'FieldPath://293381ae-d0ca-4bbf-8b8c-0e2c31bd13c4$Bars.B4.DataAccess.BaseEntity, Bars.B4.Core/ObjectCreateDate',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'd857d306-9230-4c39-85eb-d96a7e4314b0',
                'sortable': true,
                'text': 'Дата создания',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'ObjectEditDate',
                'dataIndexAbsoluteUid': 'FieldPath://293381ae-d0ca-4bbf-8b8c-0e2c31bd13c4$Bars.B4.DataAccess.BaseEntity, Bars.B4.Core/ObjectEditDate',
                'filter': {
                    'type': 'utcdatetime',
                    'useDays': true,
                    'useQuarters': true,
                    'xtype': 'b4-filter-field-list-of-date-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '29231b6a-b81a-46bc-9eae-09a54f0b8ad8',
                'sortable': true,
                'text': 'Дата изменения',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'gridExportExcel'
            }]
        });
        me.callParent(arguments);
    }
});