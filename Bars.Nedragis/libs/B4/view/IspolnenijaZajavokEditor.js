Ext.define('B4.view.IspolnenijaZajavokEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-ispolnenijazajavokeditor',
    title: 'Отдел приема и контроля исполнения заявок',
    rmsUid: '2a6776c4-56fe-4d20-9a51-20db8e151e80',
    requires: [
        'B4.form.PickerField',
        'B4.model.NaimenovanieMOEditorModel',
        'B4.model.NaimenovanieMOListModel',
        'B4.model.NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel',
        'B4.model.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel',
        'B4.model.OrganizaciiEditorModel',
        'B4.model.OrganizaciiListModel',
        'B4.view.NaimenovanieMOList',
        'B4.view.NaimenovanieNapravlenijaRaskhodovanijaSredstvList',
        'B4.view.OrganizaciiList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'IspolnenijaZajavok_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Element1511781998051',
        'modelProperty': 'IspolnenijaZajavok_Element1511781998051',
        'type': 'DateField'
    }, {
        'dataIndex': 'Element1511506083922',
        'modelProperty': 'IspolnenijaZajavok_Element1511506083922',
        'type': 'CheckboxField'
    }, {
        'dataIndex': 'Element1511506316909',
        'modelProperty': 'IspolnenijaZajavok_Element1511506316909',
        'type': 'CheckboxField'
    }, {
        'dataIndex': 'Element1511506563562',
        'modelProperty': 'IspolnenijaZajavok_Element1511506563562',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Element1511506733063',
        'modelProperty': 'IspolnenijaZajavok_Element1511506733063',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Element1511762285082',
        'modelProperty': 'IspolnenijaZajavok_Element1511762285082',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '2a6776c4-56fe-4d20-9a51-20db8e151e80-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'IspolnenijaZajavokEditor-container',
        'flex': 100,
        'height': 80,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Дата заявки',
            'format': 'd.m.Y',
            'hidden': true,
            'modelProperty': 'IspolnenijaZajavok_Element1511781998051',
            'name': 'Element1511781998051',
            'rmsUid': '0b7ff67f-0886-40ca-adbd-8d312d7cf160',
            'xtype': 'datefield'
        }, {
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'boxLabel': 'Консультация по телефону',
                    'boxLabelAlign': 'before',
                    'labelWidth': 100,
                    'margin': '20 0 0 20',
                    'modelProperty': 'IspolnenijaZajavok_Element1511506083922',
                    'name': 'Element1511506083922',
                    'rmsUid': 'c9fdd6fd-6c40-4c7b-855a-0d1c1a70b9f0',
                    'uncheckedValue': false,
                    'xtype': 'checkbox'
                }],
                'layout': 'anchor',
                'margin': '0 0 10 0',
                'rmsUid': '7af37a92-88dc-4d15-8db8-c939e232350b',
                'width': 200,
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'boxLabel': 'Инцидент(заявка)',
                    'boxLabelAlign': 'before',
                    'margin': '10 0 0 20',
                    'modelProperty': 'IspolnenijaZajavok_Element1511506316909',
                    'name': 'Element1511506316909',
                    'rmsUid': '54134471-bcab-4567-91ff-7ba5c81431b5',
                    'uncheckedValue': false,
                    'xtype': 'checkbox'
                }],
                'layout': 'anchor',
                'margin': '10 0 10 0',
                'rmsUid': '694b201f-75b0-4f48-81d4-dcff870d8ce0',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '804643d9-1236-49b4-bf8f-d52914638047',
            'width': 400,
            'xtype': 'container'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Муниципальное образование',
            'idProperty': 'Id',
            'isContextAware': true,
            'labelAlign': 'top',
            'listView': 'B4.view.NaimenovanieMOList',
            'listViewCtl': 'B4.controller.NaimenovanieMOList',
            'margin': '5 20 0 20',
            'maximizable': true,
            'model': 'B4.model.NaimenovanieMOListModel',
            'modelProperty': 'IspolnenijaZajavok_Element1511506563562',
            'name': 'Element1511506563562',
            'rmsUid': '4b448f0b-7292-451c-bd92-27768d849dce',
            'textProperty': 'Element1455707802552',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Муниципальное образование',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Наименование организации',
            'idProperty': 'Id',
            'isContextAware': true,
            'labelAlign': 'top',
            'listView': 'B4.view.OrganizaciiList',
            'listViewCtl': 'B4.controller.OrganizaciiList',
            'margin': '5 20 0 20',
            'maximizable': true,
            'model': 'B4.model.OrganizaciiListModel',
            'modelProperty': 'IspolnenijaZajavok_Element1511506733063',
            'name': 'Element1511506733063',
            'rmsUid': 'e5b84860-b11a-47f4-a3e5-a6254ca604c5',
            'textProperty': 'Element1511331964541',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Наименование организации',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Проект',
            'idProperty': 'Id',
            'isContextAware': true,
            'labelAlign': 'top',
            'listView': 'B4.view.NaimenovanieNapravlenijaRaskhodovanijaSredstvList',
            'listViewCtl': 'B4.controller.NaimenovanieNapravlenijaRaskhodovanijaSredstvList',
            'margin': '5 20 0 20',
            'maximizable': true,
            'model': 'B4.model.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel',
            'modelProperty': 'IspolnenijaZajavok_Element1511762285082',
            'name': 'Element1511762285082',
            'rmsUid': 'c392cc2e-32ba-4282-b846-b6854f352f26',
            'textProperty': 'Element1455708457805',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Проект',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'IspolnenijaZajavok_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});