Ext.define('B4.view.ZUFormaVvoda', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-zuformavvoda',
    title: 'ЗУ форма ввода',
    rmsUid: '3cbbcbbc-d8b8-4098-9b0f-7df6d4fee84d',
    requires: [
        'B4.form.PickerField',
        'B4.model.JuridicheskieLicaListModel',
        'B4.view.JuridicheskieLicaList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'ZemelNyemUchastki_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'ZemelNyemUchastki_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'area',
        'modelProperty': 'ZemelNyemUchastki_area',
        'type': 'NumberField'
    }, {
        'dataIndex': 'owner_id',
        'modelProperty': 'ZemelNyemUchastki_owner_id',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'holder_id',
        'modelProperty': 'ZemelNyemUchastki_holder_id',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'cad_okrug',
        'modelProperty': 'ZemelNyemUchastki_cad_okrug',
        'type': 'NumberField'
    }, {
        'dataIndex': 'cad_raion',
        'modelProperty': 'ZemelNyemUchastki_cad_raion',
        'type': 'NumberField'
    }, {
        'dataIndex': 'cad_block',
        'modelProperty': 'ZemelNyemUchastki_cad_block',
        'type': 'NumberField'
    }, {
        'dataIndex': 'cad_massiv',
        'modelProperty': 'ZemelNyemUchastki_cad_massiv',
        'type': 'NumberField'
    }, {
        'dataIndex': 'cad_kvartal',
        'modelProperty': 'ZemelNyemUchastki_cad_kvartal',
        'type': 'NumberField'
    }, {
        'dataIndex': 'number_ZU',
        'modelProperty': 'ZemelNyemUchastki_number_ZU',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '3cbbcbbc-d8b8-4098-9b0f-7df6d4fee84d-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ZUFormaVvoda-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'dockedItems': [],
                'items': [{
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 3,
                    'items': [{
                        'allowBlank': true,
                        'anchor': '100%',
                        'fieldLabel': 'Наименование',
                        'flex': 1,
                        'labelAlign': 'top',
                        'margin': '10 10 0 10',
                        'modelProperty': 'ZemelNyemUchastki_name',
                        'name': 'name',
                        'rmsUid': '4acb9dd3-3d84-4544-a921-7d06306be462',
                        'xtype': 'textfield'
                    }],
                    'layout': 'anchor',
                    'rmsUid': '4708a682-9f5e-4644-9539-ff040417c738',
                    'xtype': 'container'
                }, {
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'allowBlank': true,
                        'anchor': '100%',
                        'decimalPrecision': 2,
                        'fieldLabel': 'Общая площадь, кв. м',
                        'flex': 1,
                        'labelAlign': 'top',
                        'margin': '10 10 10 10',
                        'minValue': 0,
                        'modelProperty': 'ZemelNyemUchastki_area',
                        'name': 'area',
                        'rmsUid': '9b12332c-48e8-4506-a283-752fba4d889a',
                        'step': 1,
                        'xtype': 'numberfield'
                    }],
                    'layout': 'anchor',
                    'rmsUid': 'afe12fbf-af93-4342-8d64-18907a00a8e9',
                    'xtype': 'container'
                }],
                'layout': 'hbox',
                'rmsUid': '0b248b22-c457-4645-a272-9b161156fd11',
                'xtype': 'container'
            }, {
                'dockedItems': [],
                'items': [{
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'allowBlank': true,
                        'anchor': '100%',
                        'editable': false,
                        'fieldLabel': 'Собственник',
                        'flex': 1,
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'labelAlign': 'top',
                        'listView': 'B4.view.JuridicheskieLicaList',
                        'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                        'margin': '10 10 0 10',
                        'maximizable': true,
                        'model': 'B4.model.JuridicheskieLicaListModel',
                        'modelProperty': 'ZemelNyemUchastki_owner_id',
                        'name': 'owner_id',
                        'rmsUid': 'ca65d585-c415-4115-a93f-b3e39bd4952f',
                        'textProperty': 'OKATO_name1',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Собственник',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }],
                    'layout': 'anchor',
                    'rmsUid': '33fe16bd-23bb-4d3a-9be7-54461419fa81',
                    'xtype': 'container'
                }, {
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'allowBlank': true,
                        'anchor': '100%',
                        'editable': false,
                        'fieldLabel': 'Правообладатель',
                        'flex': 1,
                        'idProperty': 'Id',
                        'isContextAware': true,
                        'labelAlign': 'top',
                        'listView': 'B4.view.JuridicheskieLicaList',
                        'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                        'margin': '10 10 0 10',
                        'maximizable': true,
                        'model': 'B4.model.JuridicheskieLicaListModel',
                        'modelProperty': 'ZemelNyemUchastki_holder_id',
                        'name': 'holder_id',
                        'rmsUid': '18317267-b7d3-4a07-8d4a-5d9fc13680f7',
                        'textProperty': 'OKATO_name1',
                        'typeAhead': false,
                        'windowCfg': {
                            'border': false,
                            'height': 550,
                            'maximizable': true,
                            'title': 'Правообладатель',
                            'width': 600
                        },
                        'xtype': 'b4pickerfield'
                    }],
                    'layout': 'anchor',
                    'rmsUid': '0ea93522-ad2d-42da-ad63-da1fb12a4371',
                    'xtype': 'container'
                }],
                'layout': 'hbox',
                'rmsUid': 'c23f3930-cf33-4741-aacb-72b0d18a5473',
                'xtype': 'container'
            }, {
                'dockedItems': [],
                'items': [{
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'decimalPrecision': 0,
                        'fieldLabel': 'Кадастровый округ',
                        'flex': 1,
                        'hideTrigger': true,
                        'labelAlign': 'top',
                        'labelWidth': 50,
                        'maxValue': 91,
                        'minValue': 1,
                        'modelProperty': 'ZemelNyemUchastki_cad_okrug',
                        'name': 'cad_okrug',
                        'rmsUid': '54e66d9c-1c11-4a94-9851-1c88f4de89c4',
                        'step': 1,
                        'width': 20,
                        'xtype': 'numberfield'
                    }],
                    'layout': 'anchor',
                    'rmsUid': '881d8068-1ea0-49a9-a4a9-fcc2aa22eb5a',
                    'xtype': 'container'
                }, {
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'decimalPrecision': 0,
                        'fieldLabel': 'Кадастровый район',
                        'hideTrigger': true,
                        'labelAlign': 'top',
                        'labelWidth': 50,
                        'maxValue': 99,
                        'minValue': 1,
                        'modelProperty': 'ZemelNyemUchastki_cad_raion',
                        'name': 'cad_raion',
                        'rmsUid': 'ac073682-ff08-472a-8d12-d0369ef5e10f',
                        'step': 1,
                        'width': 20,
                        'xtype': 'numberfield'
                    }],
                    'layout': 'anchor',
                    'rmsUid': '4fba1652-0e76-43c4-9055-aa1bc7dc87f2',
                    'xtype': 'container'
                }, {
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'decimalPrecision': 0,
                        'fieldLabel': 'Кадастровый блок',
                        'hideTrigger': true,
                        'labelAlign': 'top',
                        'labelWidth': 50,
                        'maxValue': 99,
                        'minValue': 1,
                        'modelProperty': 'ZemelNyemUchastki_cad_block',
                        'name': 'cad_block',
                        'rmsUid': '7be5efdd-10bd-4865-93ca-6d9f41687aef',
                        'step': 1,
                        'width': 20,
                        'xtype': 'numberfield'
                    }],
                    'layout': 'anchor',
                    'rmsUid': '3280e7ef-95ac-4c39-a16d-78697c398f06',
                    'xtype': 'container'
                }, {
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'decimalPrecision': 0,
                        'fieldLabel': 'Кадастровый массив',
                        'hideTrigger': true,
                        'labelAlign': 'top',
                        'maxValue': 99,
                        'minValue': 1,
                        'modelProperty': 'ZemelNyemUchastki_cad_massiv',
                        'name': 'cad_massiv',
                        'rmsUid': 'b6f995e4-c641-45d2-828a-8c6cf672ddfc',
                        'step': 1,
                        'width': 20,
                        'xtype': 'numberfield'
                    }],
                    'layout': 'anchor',
                    'rmsUid': '88084b09-bf29-48b5-b535-463f241d2487',
                    'xtype': 'container'
                }, {
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'decimalPrecision': 2,
                        'fieldLabel': 'Кадастровый квартал',
                        'hideTrigger': true,
                        'labelAlign': 'top',
                        'minValue': 1,
                        'modelProperty': 'ZemelNyemUchastki_cad_kvartal',
                        'name': 'cad_kvartal',
                        'rmsUid': '1a6ddb3b-13a5-41cd-9cff-8a1227370dbf',
                        'step': 1,
                        'width': 40,
                        'xtype': 'numberfield'
                    }],
                    'layout': 'anchor',
                    'rmsUid': '137f0250-8831-497e-a5d5-87cf208d5493',
                    'xtype': 'container'
                }, {
                    'defaults': {
                        'margin': '5 5 5 5'
                    },
                    'dockedItems': [],
                    'flex': 1,
                    'items': [{
                        'allowBlank': true,
                        'allowDecimals': false,
                        'decimalPrecision': 0,
                        'fieldLabel': 'Номер зем. участка',
                        'hideTrigger': true,
                        'labelAlign': 'top',
                        'maxValue': 9999,
                        'minValue': 1,
                        'modelProperty': 'ZemelNyemUchastki_number_ZU',
                        'name': 'number_ZU',
                        'rmsUid': 'a71ed8c8-1323-4a30-b5df-5a359c02f0d5',
                        'step': 1,
                        'width': 40,
                        'xtype': 'numberfield'
                    }],
                    'layout': 'anchor',
                    'rmsUid': '1cb41161-cae5-41a2-aa49-643f09cc6504',
                    'xtype': 'container'
                }],
                'layout': 'hbox',
                'margin': '10 0 0 5',
                'rmsUid': '7a0caadd-c7af-4f3f-8de2-961682cc4591',
                'xtype': 'container'
            }],
            'layout': {
                'type': 'fit'
            },
            'rmsUid': '018cd000-e3b1-44e2-b0e2-51f20d10fef9',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'ZemelNyemUchastki_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});