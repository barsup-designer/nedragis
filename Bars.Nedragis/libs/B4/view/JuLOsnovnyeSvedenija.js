Ext.define('B4.view.JuLOsnovnyeSvedenija', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-julosnovnyesvedenija',
    title: 'ЮЛ основные сведения',
    rmsUid: 'f4138b7a-c735-4f73-8c3e-6c2edd5c8683',
    requires: [
        'B4.form.FiasSelectAddress',
        'B4.form.PickerField',
        'B4.model.JuLRedaktirovanieModel',
        'B4.model.JuridicheskieLicaListModel',
        'B4.model.OKATO1Model',
        'B4.model.OKATOEditorModel',
        'B4.model.OKFSEditorModel',
        'B4.model.OKFSListModel',
        'B4.model.OKOGU1Model',
        'B4.model.OKOGUEditorModel',
        'B4.model.OKOPFEditorModel',
        'B4.model.OKOPFListModel',
        'B4.model.OKVEhD1Model',
        'B4.model.OKVEhDEditorModel',
        'B4.model.OtraslEditorModel',
        'B4.model.OtraslListModel',
        'B4.model.StatusySubEktaEditorModel',
        'B4.model.StatusySubEktaListModel',
        'B4.view.JuridicheskieLicaList',
        'B4.view.OKATO1',
        'B4.view.OKFSList',
        'B4.view.OKOGU1',
        'B4.view.OKOPFList',
        'B4.view.OKVEhD1',
        'B4.view.OtraslList',
        'B4.view.StatusySubEktaList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'JuridicheskieLica_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Representation',
        'modelProperty': 'JuridicheskieLica_Representation',
        'type': 'TextField'
    }, {
        'dataIndex': 'Status',
        'modelProperty': 'JuridicheskieLica_Status',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Fullname',
        'modelProperty': 'JuridicheskieLica_Fullname',
        'type': 'TextAreaField'
    }, {
        'dataIndex': 'INN1',
        'modelProperty': 'JuridicheskieLica_INN1',
        'type': 'TextField'
    }, {
        'dataIndex': 'KPP1',
        'modelProperty': 'JuridicheskieLica_KPP1',
        'type': 'TextField'
    }, {
        'dataIndex': 'ogrn',
        'modelProperty': 'JuridicheskieLica_ogrn',
        'type': 'TextField'
    }, {
        'dataIndex': 'Okpo',
        'modelProperty': 'JuridicheskieLica_Okpo',
        'type': 'TextField'
    }, {
        'dataIndex': 'Okopf',
        'modelProperty': 'JuridicheskieLica_Okopf',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Okved',
        'modelProperty': 'JuridicheskieLica_Okved',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Okogu',
        'modelProperty': 'JuridicheskieLica_Okogu',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Okfs',
        'modelProperty': 'JuridicheskieLica_Okfs',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'OKATO',
        'modelProperty': 'JuridicheskieLica_OKATO',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'DateReg',
        'modelProperty': 'JuridicheskieLica_DateReg',
        'type': 'DateField'
    }, {
        'dataIndex': 'CapitalStock',
        'modelProperty': 'JuridicheskieLica_CapitalStock',
        'type': 'NumberField'
    }, {
        'dataIndex': 'CapitalPeople',
        'modelProperty': 'JuridicheskieLica_CapitalPeople',
        'type': 'NumberField'
    }, {
        'dataIndex': 'LegalAddress',
        'isAddress': true,
        'modelProperty': 'JuridicheskieLica_LegalAddress',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'FactAddress',
        'isAddress': true,
        'modelProperty': 'JuridicheskieLica_FactAddress',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Phone',
        'modelProperty': 'JuridicheskieLica_Phone',
        'type': 'TextField'
    }, {
        'dataIndex': 'Fax',
        'modelProperty': 'JuridicheskieLica_Fax',
        'type': 'TextField'
    }, {
        'dataIndex': 'Email',
        'modelProperty': 'JuridicheskieLica_Email',
        'type': 'TextField'
    }, {
        'dataIndex': 'Sphere',
        'modelProperty': 'JuridicheskieLica_Sphere',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'HeadSubject',
        'modelProperty': 'JuridicheskieLica_HeadSubject',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'f4138b7a-c735-4f73-8c3e-6c2edd5c8683-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'JuLOsnovnyeSvedenija-container',
        'autoScroll': true,
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'fieldLabel': 'Наименование',
                'flex': 1,
                'labelAlign': 'left',
                'margin': '10 10 0 10',
                'modelProperty': 'JuridicheskieLica_Representation',
                'name': 'Representation',
                'rmsUid': 'e40902fd-b8e1-4e61-bfad-250d2eae191d',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Состояние',
                'flex': 1,
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.StatusySubEktaList',
                'listViewCtl': 'B4.controller.StatusySubEktaList',
                'margin': '10 10 0 10',
                'maximizable': true,
                'model': 'B4.model.StatusySubEktaListModel',
                'modelProperty': 'JuridicheskieLica_Status',
                'name': 'Status',
                'rmsUid': 'a587138d-9851-4e57-8add-04bfd0ce69d6',
                'textProperty': 'code',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Состояние',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }],
            'layout': {
                'type': 'hbox'
            },
            'rmsUid': 'ea8399b1-6cae-4fac-a338-166f33e165f1',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'fieldLabel': 'Полное наименование',
                'flex': 1,
                'labelAlign': 'left',
                'margin': '10 10 0 0',
                'modelProperty': 'JuridicheskieLica_Fullname',
                'name': 'Fullname',
                'rmsUid': 'd2cee3fe-c557-41d3-9c01-e3460419cfe4',
                'xtype': 'textarea'
            }],
            'layout': {
                'type': 'fit'
            },
            'margin': '10 0 10 10',
            'rmsUid': '1b84f040-a092-4538-b04c-98434606b853',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'ИНН',
                    'flex': 1,
                    'margin': '10 10 0 10',
                    'maxLength': 10,
                    'maxLengthText': 'Превышена максимальная длина',
                    'minLength': 10,
                    'minLengthText': 'Текст короче минимальной длины',
                    'modelProperty': 'JuridicheskieLica_INN1',
                    'name': 'INN1',
                    'rmsUid': '226151b7-eab2-4c34-9d59-2eeab6836aab',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': '969ed4e3-a199-49d8-9d34-f969cb3556d8',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'КПП',
                    'flex': 1,
                    'margin': '10 10 0 10',
                    'maxLength': 9,
                    'modelProperty': 'JuridicheskieLica_KPP1',
                    'name': 'KPP1',
                    'rmsUid': '5e7a9850-72d0-4d23-bbc9-efa74c104494',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'abd2e9ac-d7e3-4a13-bed1-89ce94e309b2',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'ОГРН',
                    'flex': 1,
                    'labelAlign': 'left',
                    'margin': '10 10 0 10',
                    'modelProperty': 'JuridicheskieLica_ogrn',
                    'name': 'ogrn',
                    'rmsUid': '01dc7136-3473-4345-ad96-a178dc156c61',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'c48f0ac3-893d-4a1e-863a-461d07c5733e',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '15 0 0 0',
            'rmsUid': '7c07b746-262c-4931-b2f5-fcb9363d7a4b',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'ОКПО',
                    'flex': 1,
                    'labelAlign': 'left',
                    'margin': '5 0 0 10',
                    'modelProperty': 'JuridicheskieLica_Okpo',
                    'name': 'Okpo',
                    'rmsUid': '0d0a8528-9ae0-414a-bd18-6a3690aeab4c',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'bdc482cc-200c-434c-81b5-0253d9868350',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 2,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'ОКОПФ',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelWidth': 45,
                    'listView': 'B4.view.OKOPFList',
                    'listViewCtl': 'B4.controller.OKOPFList',
                    'margin': '5 10 0 20',
                    'maximizable': true,
                    'model': 'B4.model.OKOPFListModel',
                    'modelProperty': 'JuridicheskieLica_Okopf',
                    'name': 'Okopf',
                    'rmsUid': 'c2771835-75f9-4ed0-ba1c-ce9580638df0',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'ОКОПФ',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '12d463de-3016-46cc-9ca4-068506df2ad0',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '10 0 0 0',
            'rmsUid': '9184bb94-09c6-4a93-9aa4-54b946f7f3a4',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'ОКВЭД',
                    'flex': 1,
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'left',
                    'listView': 'B4.view.OKVEhD1',
                    'listViewCtl': 'B4.controller.OKVEhD1',
                    'margin': '10 10 0 10',
                    'maximizable': true,
                    'model': 'B4.model.OKVEhD1Model',
                    'modelProperty': 'JuridicheskieLica_Okved',
                    'name': 'Okved',
                    'rmsUid': '361585fc-af88-41b7-82b9-f08dea077e54',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'ОКВЭД',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '6e312866-4e25-4d45-9c62-103970c79008',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'ОКОГУ',
                    'flex': 1,
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'left',
                    'listView': 'B4.view.OKOGU1',
                    'listViewCtl': 'B4.controller.OKOGU1',
                    'margin': '10 10 0 10',
                    'maximizable': true,
                    'model': 'B4.model.OKOGU1Model',
                    'modelProperty': 'JuridicheskieLica_Okogu',
                    'name': 'Okogu',
                    'rmsUid': 'faf39e28-2488-4b6c-a05f-008f6cc07357',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'ОКОГУ',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '0c66a168-9e85-40a2-b164-ad16b7f92cd5',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '10 0 0 0',
            'rmsUid': 'a2d13b5b-ac7e-4374-be3e-a6586ec70668',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'ОКФС',
                    'flex': 1,
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'left',
                    'listView': 'B4.view.OKFSList',
                    'listViewCtl': 'B4.controller.OKFSList',
                    'margin': '10 10 0 10',
                    'maximizable': true,
                    'model': 'B4.model.OKFSListModel',
                    'modelProperty': 'JuridicheskieLica_Okfs',
                    'name': 'Okfs',
                    'rmsUid': '43878213-0b39-409f-9467-2958bf88baed',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'ОКФС',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '45d91931-bca8-4550-ba2d-42cc81e52dd4',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'ОКАТО',
                    'flex': 1,
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'left',
                    'listView': 'B4.view.OKATO1',
                    'listViewCtl': 'B4.controller.OKATO1',
                    'margin': '10 10 0 10',
                    'maximizable': true,
                    'model': 'B4.model.OKATO1Model',
                    'modelProperty': 'JuridicheskieLica_OKATO',
                    'name': 'OKATO',
                    'rmsUid': '1534fb85-e40c-49d6-90a0-6283f65db42e',
                    'textProperty': 'code',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'ОКАТО',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'b1624f9d-2b61-4763-ad1c-3ac8765de32d',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'margin': '10 0 0 0',
            'rmsUid': '34a93f4b-581d-4cc1-9baf-405383c7f8d1',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'fieldLabel': 'Дата гос. регистрации',
                'flex': 1,
                'format': 'd.m.Y',
                'margin': '10 10 0 10',
                'modelProperty': 'JuridicheskieLica_DateReg',
                'name': 'DateReg',
                'rmsUid': 'f940a7be-044b-4a73-af97-cf3ef679d303',
                'xtype': 'datefield'
            }, {
                'allowBlank': true,
                'decimalPrecision': 2,
                'fieldLabel': 'Уставной капитал, руб.',
                'flex': 1,
                'margin': '10 10 0 10',
                'minValue': 0,
                'modelProperty': 'JuridicheskieLica_CapitalStock',
                'name': 'CapitalStock',
                'rmsUid': '382274ab-a29f-4fed-8a9b-64d08f206f0e',
                'step': 1,
                'xtype': 'numberfield'
            }, {
                'allowBlank': true,
                'allowDecimals': false,
                'decimalPrecision': 2,
                'fieldLabel': 'Среднесписочный состав, чел.',
                'flex': 1,
                'margin': '10 10 0 10',
                'minValue': 0,
                'modelProperty': 'JuridicheskieLica_CapitalPeople',
                'name': 'CapitalPeople',
                'rmsUid': '116562d3-0c7b-40d0-9d51-0670ee9bfb21',
                'step': 1,
                'xtype': 'numberfield'
            }],
            'layout': {
                'type': 'hbox'
            },
            'margin': '10 0 10 0',
            'rmsUid': '73a39307-665c-4532-919c-ae15d3e93ce2',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Юридический адрес',
                'flex': 1,
                'idProperty': 'Id',
                'marginLeft': 10,
                'marginRight': 10,
                'marginTop': 10,
                'maximizable': true,
                'model': 'B4.model.FiasAddress',
                'modelProperty': 'JuridicheskieLica_LegalAddress',
                'name': 'LegalAddress',
                'rmsUid': 'bd88de7c-183e-408e-ae3d-4bf300282f44',
                'typeAhead': false,
                'xtype': 'b4fiasselectaddress'
            }, {
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Фактический адрес',
                'flex': 1,
                'idProperty': 'Id',
                'marginLeft': 10,
                'marginRight': 10,
                'marginTop': 10,
                'maximizable': true,
                'model': 'B4.model.FiasAddress',
                'modelProperty': 'JuridicheskieLica_FactAddress',
                'name': 'FactAddress',
                'rmsUid': 'd4a2a77a-1a2e-493b-99aa-65d18a196a70',
                'typeAhead': false,
                'xtype': 'b4fiasselectaddress'
            }],
            'layout': {
                'type': 'fit'
            },
            'rmsUid': '67523cce-4d43-4771-a562-f0269394ed2b',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'fieldLabel': 'Телефон',
                'flex': 1,
                'margin': '10 10 0 10',
                'modelProperty': 'JuridicheskieLica_Phone',
                'name': 'Phone',
                'rmsUid': '1f5749ee-74c8-4c98-8e9f-b9b32ade6252',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Факс',
                'flex': 1,
                'margin': '10 10 0 10',
                'modelProperty': 'JuridicheskieLica_Fax',
                'name': 'Fax',
                'rmsUid': '6b240d10-e2e4-4e64-a80e-583c2f0bc9a9',
                'xtype': 'textfield'
            }, {
                'allowBlank': true,
                'fieldLabel': 'Email',
                'flex': 1,
                'margin': '10 10 0 10',
                'modelProperty': 'JuridicheskieLica_Email',
                'name': 'Email',
                'rmsUid': 'e9f42fed-096c-45bb-b9e1-71156c074ee3',
                'vtype': 'email',
                'xtype': 'textfield'
            }],
            'layout': {
                'type': 'hbox'
            },
            'rmsUid': 'e4bc406f-28d0-453a-b458-08a68700f169',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Контейнер',
            'flex': 1,
            'items': [{
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Отрасль',
                'flex': 1,
                'idProperty': 'Id',
                'isContextAware': true,
                'labelAlign': 'left',
                'listView': 'B4.view.OtraslList',
                'listViewCtl': 'B4.controller.OtraslList',
                'margin': '10 10 0 10',
                'maximizable': true,
                'model': 'B4.model.OtraslListModel',
                'modelProperty': 'JuridicheskieLica_Sphere',
                'name': 'Sphere',
                'rmsUid': 'be7206b9-2d4f-4509-80c9-35b4cc86b3e4',
                'textProperty': 'code',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Отрасль',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }, {
                'allowBlank': true,
                'editable': false,
                'fieldLabel': 'Вышестоящая организация',
                'flex': 1,
                'idProperty': 'Id',
                'isContextAware': true,
                'listView': 'B4.view.JuridicheskieLicaList',
                'listViewCtl': 'B4.controller.JuridicheskieLicaList',
                'margin': '10 10 0 10',
                'maximizable': true,
                'model': 'B4.model.JuridicheskieLicaListModel',
                'modelProperty': 'JuridicheskieLica_HeadSubject',
                'name': 'HeadSubject',
                'rmsUid': 'c7f3c133-7236-4dd4-927e-448c93af59fc',
                'textProperty': 'OKATO_name1',
                'typeAhead': false,
                'windowCfg': {
                    'border': false,
                    'height': 550,
                    'maximizable': true,
                    'title': 'Вышестоящая организация',
                    'width': 600
                },
                'xtype': 'b4pickerfield'
            }],
            'layout': {
                'type': 'fit'
            },
            'margin': '5 0 10 0',
            'rmsUid': '4d4eff0c-988d-4ede-9bc6-f092f3b3f790',
            'title': 'Контейнер',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'JuridicheskieLica_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle('Основные сведения');
        } else {}
        return res;
    },
});