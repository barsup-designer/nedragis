Ext.define('B4.view.OKOFEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-okofeditor',
    title: 'Форма редактирования ОКОФ',
    rmsUid: '6e4451b4-f9a0-45c5-8282-ea74f43b7628',
    requires: [
        'B4.form.PickerField',
        'B4.model.KlassifikacijaOKOFEditorModel',
        'B4.model.KlassifikacijaOKOFListModel',
        'B4.model.OKOF1Model',
        'B4.model.OKOFEditorModel',
        'B4.view.KlassifikacijaOKOFList',
        'B4.view.OKOF1'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'OKOF_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'code',
        'modelProperty': 'OKOF_code',
        'type': 'TextField'
    }, {
        'dataIndex': 'clasifier',
        'modelProperty': 'OKOF_clasifier',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'OKOF_name',
        'type': 'TextAreaField'
    }, {
        'dataIndex': 'parent_id',
        'modelProperty': 'OKOF_parent_id',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'check_number',
        'modelProperty': 'OKOF_check_number',
        'type': 'NumberField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '6e4451b4-f9a0-45c5-8282-ea74f43b7628-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'OKOFEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Код',
            'flex': 1,
            'margin': '10 10 10 10',
            'maxLength': 9,
            'modelProperty': 'OKOF_code',
            'name': 'code',
            'rmsUid': '317120c7-3936-4343-a9d4-cef24005984c',
            'xtype': 'textfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Классификация',
            'flex': 1,
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.KlassifikacijaOKOFList',
            'listViewCtl': 'B4.controller.KlassifikacijaOKOFList',
            'margin': '10 10 0 10',
            'maximizable': true,
            'model': 'B4.model.KlassifikacijaOKOFListModel',
            'modelProperty': 'OKOF_clasifier',
            'name': 'clasifier',
            'rmsUid': '49c3118f-2273-45e9-a800-4346393c464d',
            'textProperty': 'code',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Классификация',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'fieldLabel': 'Наименование',
            'labelAlign': 'left',
            'margin': '10 10 0 10',
            'modelProperty': 'OKOF_name',
            'name': 'name',
            'rmsUid': '52aaf887-958c-4769-bcb7-a5fda548285a',
            'xtype': 'textarea'
        }, {
            'allowBlank': true,
            'editable': false,
            'fieldLabel': 'Родительский элемент',
            'flex': 1,
            'height': 20,
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.OKOF1',
            'listViewCtl': 'B4.controller.OKOF1',
            'margin': '10 10 0 10',
            'maximizable': true,
            'model': 'B4.model.OKOF1Model',
            'modelProperty': 'OKOF_parent_id',
            'name': 'parent_id',
            'rmsUid': '259ec901-d5b7-4208-94d0-52d219a90a41',
            'textProperty': 'code',
            'typeAhead': false,
            'width': 10,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Родительский элемент',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'allowDecimals': false,
            'anchor': '100%',
            'decimalPrecision': 0,
            'fieldLabel': 'Контрольное число',
            'flex': 1,
            'margin': '5 10 10 10',
            'maxValue': 9,
            'minValue': 0,
            'modelProperty': 'OKOF_check_number',
            'name': 'check_number',
            'rmsUid': '554bf167-33c0-4b30-aa77-0f109e527154',
            'step': 1,
            'xtype': 'numberfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'OKOF_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});