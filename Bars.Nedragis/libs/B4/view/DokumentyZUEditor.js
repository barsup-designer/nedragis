Ext.define('B4.view.DokumentyZUEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-dokumentyzueditor',
    title: 'Форма ввода ДокументыЗУ',
    rmsUid: '1ae324f2-320e-42e8-9df3-df8484def876',
    requires: [
        'B4.form.PickerField',
        'B4.model.DokumentEditorModel',
        'B4.model.DokumentListModel',
        'B4.model.ZemelNyemUchastkiListModel',
        'B4.model.ZURedaktirovanieNavigacijaModel',
        'B4.view.DokumentList',
        'B4.view.ZemelNyemUchastkiList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'DokumentyZU_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'zemuch',
        'modelProperty': 'DokumentyZU_zemuch',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'doc1',
        'modelProperty': 'DokumentyZU_doc1',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '1ae324f2-320e-42e8-9df3-df8484def876-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'DokumentyZUEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Земельный участок',
            'flex': 1,
            'idProperty': 'Id',
            'isContextAware': true,
            'listView': 'B4.view.ZemelNyemUchastkiList',
            'listViewCtl': 'B4.controller.ZemelNyemUchastkiList',
            'margin': '10 10 0 10',
            'maximizable': true,
            'model': 'B4.model.ZemelNyemUchastkiListModel',
            'modelProperty': 'DokumentyZU_zemuch',
            'name': 'zemuch',
            'rmsUid': 'ec7a225a-e3b8-43fd-9b38-2f718e01dd48',
            'textProperty': 'name',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 550,
                'maximizable': true,
                'title': 'Земельный участок',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'allowBlank': true,
            'anchor': '100%',
            'editable': false,
            'fieldLabel': 'Документ',
            'flex': 1,
            'idProperty': 'Id',
            'listView': 'B4.view.DokumentList',
            'listViewCtl': 'B4.controller.DokumentList',
            'margin': '10 10 10 10',
            'maximizable': true,
            'model': 'B4.model.DokumentListModel',
            'modelProperty': 'DokumentyZU_doc1',
            'name': 'doc1',
            'rmsUid': '6ead281c-dfcd-46ff-8e78-59be23cbafc8',
            'textProperty': 'Number',
            'typeAhead': false,
            'windowCfg': {
                'border': false,
                'height': 350,
                'maximizable': true,
                'title': 'Документ',
                'width': 600
            },
            'xtype': 'b4pickerfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'DokumentyZU_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});