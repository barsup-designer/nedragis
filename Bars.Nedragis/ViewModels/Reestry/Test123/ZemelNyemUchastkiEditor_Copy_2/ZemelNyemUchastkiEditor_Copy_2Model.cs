namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Земельные участки - Копия - 2' для отдачи на клиент
    /// </summary>
    public class ZemelNyemUchastkiEditor_Copy_2Model : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public ZemelNyemUchastkiEditor_Copy_2Model()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("1600298e-e3b3-4eb1-a949-6c312d45c8c7")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Тип реестра
        /// </summary>
        [Bars.B4.Utils.Display("Тип реестра")]
        [Bars.Rms.Core.Attributes.Uid("fe9c8b5d-f4f8-4cad-88a5-fdde9bba26b4")]
        public virtual Bars.Nedragis.ReestryListModel type
        {
            get;
            set;
        }

        /// <summary>
        /// Вид права на землю
        /// </summary>
        [Bars.B4.Utils.Display("Вид права на землю")]
        [Bars.Rms.Core.Attributes.Uid("6fad93ad-4b23-4027-a246-bff66d3f1ebe")]
        public virtual Bars.Nedragis.VidPravaNaZemljuListModel vid_prava
        {
            get;
            set;
        }

        /// <summary>
        /// Категория земли
        /// </summary>
        [Bars.B4.Utils.Display("Категория земли")]
        [Bars.Rms.Core.Attributes.Uid("e03ecb19-2d53-4936-ba14-ba5dc8168ecf")]
        public virtual Bars.Nedragis.KategoriiZemelListModel category
        {
            get;
            set;
        }

        /// <summary>
        /// Вид разрешенного использования
        /// </summary>
        [Bars.B4.Utils.Display("Вид разрешенного использования")]
        [Bars.Rms.Core.Attributes.Uid("0f81f76d-cf10-410f-9d45-e306895e2c8b")]
        public virtual Bars.Nedragis.VRIZUListModel vid_use
        {
            get;
            set;
        }

        /// <summary>
        /// Собственник
        /// </summary>
        [Bars.B4.Utils.Display("Собственник")]
        [Bars.Rms.Core.Attributes.Uid("0b112d8e-c30f-4153-89f5-fecd99ca5f8c")]
        public virtual Bars.Nedragis.ZemelNyemUchastkiEditor_Copy_2owner_idControlModel owner_id
        {
            get;
            set;
        }

        /// <summary>
        /// Правообладатель
        /// </summary>
        [Bars.B4.Utils.Display("Правообладатель")]
        [Bars.Rms.Core.Attributes.Uid("eb3982e4-5cb4-442e-a610-eb9ca479e8d9")]
        public virtual Bars.Nedragis.ZemelNyemUchastkiEditor_Copy_2holder_idControlModel holder_id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Общая площадь, кв. м'
        /// </summary>
        [Bars.B4.Utils.Display("Общая площадь, кв. м")]
        [Bars.Rms.Core.Attributes.Uid("ccaa5c61-c3a3-4cba-989a-4c8c926051e5")]
        public virtual System.Decimal? area
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Назначение'
        /// </summary>
        [Bars.B4.Utils.Display("Назначение")]
        [Bars.Rms.Core.Attributes.Uid("3f7d93c4-f331-440e-95b4-f7ea85c3c465")]
        public virtual System.String naznach
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Идентификатор ГИС'
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор ГИС")]
        [Bars.Rms.Core.Attributes.Uid("fa8bf04b-6c7a-456e-8f37-a832e2a302e9")]
        public virtual System.Int64? MapID
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Модель для отображения свойства 'Собственник'
    /// </summary>
    public class ZemelNyemUchastkiEditor_Copy_2owner_idControlModel
    {
        /// <summary>
        /// Свойство 'Земельные участки.Собственник.Идентификатор'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Земельные участки.Собственник.Идентификатор")]
        public System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Земельные участки.Собственник.Название субъекта'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Земельные участки.Собственник.Название субъекта")]
        public System.String Representation
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Модель для отображения свойства 'Правообладатель'
    /// </summary>
    public class ZemelNyemUchastkiEditor_Copy_2holder_idControlModel
    {
        /// <summary>
        /// Свойство 'Земельные участки.Правообладатель.Идентификатор'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Земельные участки.Правообладатель.Идентификатор")]
        public System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Земельные участки.Правообладатель.Название субъекта'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Земельные участки.Правообладатель.Название субъекта")]
        public System.String Representation
        {
            get;
            set;
        }
    }
}