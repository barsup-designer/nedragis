namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Земельные участки - Копия - 2'
    /// </summary>
    public interface IZemelNyemUchastkiEditor_Copy_2ViewModel : IEntityEditorViewModel<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditor_Copy_2Model>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Земельные участки - Копия - 2'
    /// </summary>
    public interface IZemelNyemUchastkiEditor_Copy_2Validator : IEditorModelValidator<ZemelNyemUchastkiEditor_Copy_2Model>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Земельные участки - Копия - 2'
    /// </summary>
    public interface IZemelNyemUchastkiEditor_Copy_2Handler : IEntityEditorViewModelHandler<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditor_Copy_2Model>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Земельные участки - Копия - 2'
    /// </summary>
    public abstract class AbstractZemelNyemUchastkiEditor_Copy_2Handler : EntityEditorViewModelHandler<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditor_Copy_2Model>, IZemelNyemUchastkiEditor_Copy_2Handler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Земельные участки - Копия - 2'
    /// </summary>
    public class ZemelNyemUchastkiEditor_Copy_2ViewModel : BaseEditorViewModel<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditor_Copy_2Model>, IZemelNyemUchastkiEditor_Copy_2ViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ZemelNyemUchastkiEditor_Copy_2Model CreateModelInternal(BaseParams @params)
        {
            var model = new ZemelNyemUchastkiEditor_Copy_2Model();
            var vartypeId = @params.Params.GetAs<long>("type_Id", 0);
            if (vartypeId > 0)
            {
                model.type = Container.Resolve<Bars.Nedragis.IReestryListQuery>().GetById(vartypeId);
            }

            var varvid_pravaId = @params.Params.GetAs<long>("vid_prava_Id", 0);
            if (varvid_pravaId > 0)
            {
                model.vid_prava = Container.Resolve<Bars.Nedragis.IVidPravaNaZemljuListQuery>().GetById(varvid_pravaId);
            }

            var varcategoryId = @params.Params.GetAs<long>("category_Id", 0);
            if (varcategoryId > 0)
            {
                model.category = Container.Resolve<Bars.Nedragis.IKategoriiZemelListQuery>().GetById(varcategoryId);
            }

            var varvid_useId = @params.Params.GetAs<long>("vid_use_Id", 0);
            if (varvid_useId > 0)
            {
                model.vid_use = Container.Resolve<Bars.Nedragis.IVRIZUListQuery>().GetById(varvid_useId);
            }

            var varowner_idId = @params.Params.GetAs<long>("owner_id_Id", 0);
            if (varowner_idId > 0)
            {
                var queryowner_id = Container.ResolveDomain<Bars.Nedragis.SubEktyPrava>().GetAll().Where(x => x.Id == varowner_idId);
                model.owner_id = queryowner_id.Select(x => new ZemelNyemUchastkiEditor_Copy_2owner_idControlModel{Id = x.Id, Representation = x.Representation, }).FirstOrDefault();
            }

            var varholder_idId = @params.Params.GetAs<long>("holder_id_Id", 0);
            if (varholder_idId > 0)
            {
                var queryholder_id = Container.ResolveDomain<Bars.Nedragis.SubEktyPrava>().GetAll().Where(x => x.Id == varholder_idId);
                model.holder_id = queryholder_id.Select(x => new ZemelNyemUchastkiEditor_Copy_2holder_idControlModel{Id = x.Id, Representation = x.Representation, }).FirstOrDefault();
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Земельные участки' в модель представления
        /// </summary>
        protected override ZemelNyemUchastkiEditor_Copy_2Model MapEntityInternal(Bars.Nedragis.ZemelNyemUchastki entity)
        {
            // создаем экзепляр модели
            var model = new ZemelNyemUchastkiEditor_Copy_2Model();
            model.Id = entity.Id;
            model.name = (System.String)(entity.name);
            if (entity.type.IsNotNull())
            {
                var queryReestryList = Container.Resolve<Bars.Nedragis.IReestryListQuery>();
                model.type = queryReestryList.GetById(entity.type.Id);
            }

            if (entity.vid_prava.IsNotNull())
            {
                var queryVidPravaNaZemljuList = Container.Resolve<Bars.Nedragis.IVidPravaNaZemljuListQuery>();
                model.vid_prava = queryVidPravaNaZemljuList.GetById(entity.vid_prava.Id);
            }

            if (entity.category.IsNotNull())
            {
                var queryKategoriiZemelList = Container.Resolve<Bars.Nedragis.IKategoriiZemelListQuery>();
                model.category = queryKategoriiZemelList.GetById(entity.category.Id);
            }

            if (entity.vid_use.IsNotNull())
            {
                var queryVRIZUList = Container.Resolve<Bars.Nedragis.IVRIZUListQuery>();
                model.vid_use = queryVRIZUList.GetById(entity.vid_use.Id);
            }

            model.owner_id = entity.owner_id == null ? null : new ZemelNyemUchastkiEditor_Copy_2owner_idControlModel{Id = (System.Int64? )(entity.Return(p => p.owner_id).Return(p => p.Id)), Representation = entity.Return(p => p.owner_id).Return(p => p.Representation), };
            model.holder_id = entity.holder_id == null ? null : new ZemelNyemUchastkiEditor_Copy_2holder_idControlModel{Id = (System.Int64? )(entity.Return(p => p.holder_id).Return(p => p.Id)), Representation = entity.Return(p => p.holder_id).Return(p => p.Representation), };
            model.area = (System.Decimal? )(entity.area);
            model.naznach = (System.String)(entity.naznach);
            model.MapID = (System.Int64? )(entity.MapID);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Земельные участки' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.ZemelNyemUchastki entity, ZemelNyemUchastkiEditor_Copy_2Model model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name = model.name;
            entity.type = TryLoadEntityById<Bars.Nedragis.Reestry>(model.type?.Id);
            entity.vid_prava = TryLoadEntityById<Bars.Nedragis.VidPravaNaZemlju>(model.vid_prava?.Id);
            entity.category = TryLoadEntityById<Bars.Nedragis.KategoriiZemel>(model.category?.Id);
            entity.vid_use = TryLoadEntityById<Bars.Nedragis.VRIZU>(model.vid_use?.Id);
            entity.owner_id = TryLoadEntityById<Bars.Nedragis.SubEktyPrava>(model.owner_id?.Id);
            entity.holder_id = TryLoadEntityById<Bars.Nedragis.SubEktyPrava>(model.holder_id?.Id);
            if (model.area.HasValue)
            {
                entity.area = model.area.Value;
            }

            entity.naznach = model.naznach;
            if (model.MapID.HasValue)
            {
                entity.MapID = model.MapID.Value;
            }
        }
    }
}