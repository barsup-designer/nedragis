namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Решение по праву приобретения информации' для отдачи на клиент
    /// </summary>
    public class ReshenieEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public ReshenieEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер решения'
        /// </summary>
        [Bars.B4.Utils.Display("Номер решения")]
        [Bars.Rms.Core.Attributes.Uid("44b3f87d-d4f7-422f-bfb4-ca885d6865a8")]
        public virtual System.String Element1478752620339
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата решения'
        /// </summary>
        [Bars.B4.Utils.Display("Дата решения")]
        [Bars.Rms.Core.Attributes.Uid("2592730d-6dae-42da-bb30-9ad3c104c3ca")]
        public virtual System.DateTime? Element1478752748169
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Текст'
        /// </summary>
        [Bars.B4.Utils.Display("Текст")]
        [Bars.Rms.Core.Attributes.Uid("7f607293-bb5e-4d14-98c5-93ddbb0fb2ea")]
        public virtual System.String Element1478752768753
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер запроса в фед. агенство'
        /// </summary>
        [Bars.B4.Utils.Display("Номер запроса в фед. агенство")]
        [Bars.Rms.Core.Attributes.Uid("f2835ccb-c767-4b4f-ba32-585aa4d56294")]
        public virtual System.Int64? Element1478752791585
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата запроса в фед агенство'
        /// </summary>
        [Bars.B4.Utils.Display("Дата запроса в фед агенство")]
        [Bars.Rms.Core.Attributes.Uid("4bc3cb87-f43b-4677-8e49-d82e6a0ba9aa")]
        public virtual System.DateTime? Element1478752831192
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Текст запроса в фед. агенство'
        /// </summary>
        [Bars.B4.Utils.Display("Текст запроса в фед. агенство")]
        [Bars.Rms.Core.Attributes.Uid("52818127-2fd0-4ed2-a3a3-73c478047181")]
        public virtual System.String Element1478752856240
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата решения фед агенства'
        /// </summary>
        [Bars.B4.Utils.Display("Дата решения фед агенства")]
        [Bars.Rms.Core.Attributes.Uid("f163d9cc-a2a4-4e75-b788-ab35670a648a")]
        public virtual System.DateTime? Element1478753246124
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер решения фед.агенства'
        /// </summary>
        [Bars.B4.Utils.Display("Номер решения фед.агенства")]
        [Bars.Rms.Core.Attributes.Uid("715cf89e-5156-4a9a-880d-99a99e0b05a1")]
        public virtual System.String Element1478752884224
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Текст решения фед агенства'
        /// </summary>
        [Bars.B4.Utils.Display("Текст решения фед агенства")]
        [Bars.Rms.Core.Attributes.Uid("e5cfd5c2-e941-4fda-ac40-601c1f3fce41")]
        public virtual System.String Element1478753305771
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Руководитель фед. агенства'
        /// </summary>
        [Bars.B4.Utils.Display("Руководитель фед. агенства")]
        [Bars.Rms.Core.Attributes.Uid("cfda3dbb-35cd-4f99-a800-f143b9a2ec75")]
        public virtual System.String Element1478753329859
        {
            get;
            set;
        }

        /// <summary>
        /// Номер спецификации по заявке
        /// </summary>
        [Bars.B4.Utils.Display("Номер спецификации по заявке")]
        [Bars.Rms.Core.Attributes.Uid("8946e4b8-e208-44ba-a59e-03d8a7276490")]
        public virtual Bars.Nedragis.SpecifikacijaListModel Element1478753351594
        {
            get;
            set;
        }
    }
}