namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Решение по праву приобретения информации'
    /// </summary>
    public interface IReshenieEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Reshenie, ReshenieEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Решение по праву приобретения информации'
    /// </summary>
    public interface IReshenieEditorValidator : IEditorModelValidator<ReshenieEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Решение по праву приобретения информации'
    /// </summary>
    public interface IReshenieEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Reshenie, ReshenieEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Решение по праву приобретения информации'
    /// </summary>
    public abstract class AbstractReshenieEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Reshenie, ReshenieEditorModel>, IReshenieEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Решение по праву приобретения информации'
    /// </summary>
    public class ReshenieEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Reshenie, ReshenieEditorModel>, IReshenieEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ReshenieEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new ReshenieEditorModel();
            var varElement1478753351594Id = @params.Params.GetAs<long>("Element1478753351594_Id", 0);
            if (varElement1478753351594Id > 0)
            {
                model.Element1478753351594 = Container.Resolve<Bars.Nedragis.ISpecifikacijaListQuery>().GetById(varElement1478753351594Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Решение по праву приобретения информации' в модель представления
        /// </summary>
        protected override ReshenieEditorModel MapEntityInternal(Bars.Nedragis.Reshenie entity)
        {
            // создаем экзепляр модели
            var model = new ReshenieEditorModel();
            model.Id = entity.Id;
            model.Element1478752620339 = (System.String)(entity.Element1478752620339);
            model.Element1478752748169 = (System.DateTime? )(entity.Element1478752748169);
            model.Element1478752768753 = (System.String)(entity.Element1478752768753);
            model.Element1478752791585 = (System.Int64? )(entity.Element1478752791585);
            model.Element1478752831192 = (System.DateTime? )(entity.Element1478752831192);
            model.Element1478752856240 = (System.String)(entity.Element1478752856240);
            model.Element1478753246124 = (System.DateTime? )(entity.Element1478753246124);
            model.Element1478752884224 = (System.String)(entity.Element1478752884224);
            model.Element1478753305771 = (System.String)(entity.Element1478753305771);
            model.Element1478753329859 = (System.String)(entity.Element1478753329859);
            if (entity.Element1478753351594.IsNotNull())
            {
                var querySpecifikacijaList = Container.Resolve<Bars.Nedragis.ISpecifikacijaListQuery>();
                model.Element1478753351594 = querySpecifikacijaList.GetById(entity.Element1478753351594.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Решение по праву приобретения информации' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Reshenie entity, ReshenieEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1478752620339 = model.Element1478752620339;
            entity.Element1478752748169 = model.Element1478752748169.GetValueOrDefault();
            entity.Element1478752768753 = model.Element1478752768753;
            if (model.Element1478752791585.HasValue)
            {
                entity.Element1478752791585 = model.Element1478752791585.Value;
            }

            entity.Element1478752831192 = model.Element1478752831192.GetValueOrDefault();
            entity.Element1478752856240 = model.Element1478752856240;
            entity.Element1478753246124 = model.Element1478753246124.GetValueOrDefault();
            entity.Element1478752884224 = model.Element1478752884224;
            entity.Element1478753305771 = model.Element1478753305771;
            entity.Element1478753329859 = model.Element1478753329859;
            entity.Element1478753351594 = TryLoadEntityById<Bars.Nedragis.Specifikacija>(model.Element1478753351594?.Id);
        }
    }
}