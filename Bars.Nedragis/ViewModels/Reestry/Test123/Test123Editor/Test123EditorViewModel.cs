namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования test123'
    /// </summary>
    public interface ITest123EditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Test123, Test123EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования test123'
    /// </summary>
    public interface ITest123EditorValidator : IEditorModelValidator<Test123EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования test123'
    /// </summary>
    public interface ITest123EditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Test123, Test123EditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования test123'
    /// </summary>
    public abstract class AbstractTest123EditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Test123, Test123EditorModel>, ITest123EditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования test123'
    /// </summary>
    public class Test123EditorViewModel : BaseEditorViewModel<Bars.Nedragis.Test123, Test123EditorModel>, ITest123EditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override Test123EditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new Test123EditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'test123' в модель представления
        /// </summary>
        protected override Test123EditorModel MapEntityInternal(Bars.Nedragis.Test123 entity)
        {
            // создаем экзепляр модели
            var model = new Test123EditorModel();
            model.Id = entity.Id;
            model.name1234 = (System.String)(entity.name1234);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'test123' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Test123 entity, Test123EditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name1234 = model.name1234;
        }
    }
}