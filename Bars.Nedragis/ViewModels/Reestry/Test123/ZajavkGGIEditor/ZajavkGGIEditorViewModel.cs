namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Заявка на приобретение геолого-геофизической информации'
    /// </summary>
    public interface IZajavkGGIEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.ZajavkGGI, ZajavkGGIEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Заявка на приобретение геолого-геофизической информации'
    /// </summary>
    public interface IZajavkGGIEditorValidator : IEditorModelValidator<ZajavkGGIEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Заявка на приобретение геолого-геофизической информации'
    /// </summary>
    public interface IZajavkGGIEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.ZajavkGGI, ZajavkGGIEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Заявка на приобретение геолого-геофизической информации'
    /// </summary>
    public abstract class AbstractZajavkGGIEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.ZajavkGGI, ZajavkGGIEditorModel>, IZajavkGGIEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Заявка на приобретение геолого-геофизической информации'
    /// </summary>
    public class ZajavkGGIEditorViewModel : BaseEditorViewModel<Bars.Nedragis.ZajavkGGI, ZajavkGGIEditorModel>, IZajavkGGIEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ZajavkGGIEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new ZajavkGGIEditorModel();
            var varNedropolZovatelId = @params.Params.GetAs<long>("NedropolZovatel_Id", 0);
            if (varNedropolZovatelId > 0)
            {
                model.NedropolZovatel = Container.Resolve<Bars.Nedragis.INedropolZovatelListQuery>().GetById(varNedropolZovatelId);
            }

            var varluchId = @params.Params.GetAs<long>("luch_Id", 0);
            if (varluchId > 0)
            {
                model.luch = Container.Resolve<Bars.Nedragis.ILicUchastkiListQuery>().GetById(varluchId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Заявка на приобретение ГГИ' в модель представления
        /// </summary>
        protected override ZajavkGGIEditorModel MapEntityInternal(Bars.Nedragis.ZajavkGGI entity)
        {
            // создаем экзепляр модели
            var model = new ZajavkGGIEditorModel();
            model.Id = entity.Id;
            model.nomz = (System.Int32? )(entity.nomz);
            model.dataz = (System.DateTime? )(entity.dataz);
            model.dati = (System.DateTime? )(entity.dati);
            if (entity.NedropolZovatel.IsNotNull())
            {
                var queryNedropolZovatelList = Container.Resolve<Bars.Nedragis.INedropolZovatelListQuery>();
                model.NedropolZovatel = queryNedropolZovatelList.GetById(entity.NedropolZovatel.Id);
            }

            if (entity.luch.IsNotNull())
            {
                var queryLicUchastkiList = Container.Resolve<Bars.Nedragis.ILicUchastkiListQuery>();
                model.luch = queryLicUchastkiList.GetById(entity.luch.Id);
            }

            model.texto = (System.String)(entity.texto);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Заявка на приобретение ГГИ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.ZajavkGGI entity, ZajavkGGIEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.nomz.HasValue)
            {
                entity.nomz = model.nomz.Value;
            }

            entity.dataz = model.dataz.GetValueOrDefault();
            entity.dati = model.dati.GetValueOrDefault();
            entity.NedropolZovatel = TryLoadEntityById<Bars.Nedragis.NedropolZovatel>(model.NedropolZovatel?.Id);
            entity.luch = TryLoadEntityById<Bars.Nedragis.LicUchastki>(model.luch?.Id);
            entity.texto = model.texto;
        }
    }
}