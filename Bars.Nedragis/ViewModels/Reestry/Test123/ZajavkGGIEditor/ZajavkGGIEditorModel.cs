namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Заявка на приобретение геолого-геофизической информации' для отдачи на клиент
    /// </summary>
    public class ZajavkGGIEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public ZajavkGGIEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер заявки'
        /// </summary>
        [Bars.B4.Utils.Display("Номер заявки")]
        [Bars.Rms.Core.Attributes.Uid("44c54402-dd63-4523-bfa0-35da1cd4838c")]
        public virtual System.Int32? nomz
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата заявки'
        /// </summary>
        [Bars.B4.Utils.Display("Дата заявки")]
        [Bars.Rms.Core.Attributes.Uid("6459d64e-5001-4a57-910f-da99b3d27f24")]
        public virtual System.DateTime? dataz
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата исполнения'
        /// </summary>
        [Bars.B4.Utils.Display("Дата исполнения")]
        [Bars.Rms.Core.Attributes.Uid("e0d68586-5c04-4403-98b5-2e2d8924396c")]
        public virtual System.DateTime? dati
        {
            get;
            set;
        }

        /// <summary>
        /// Недропользователь
        /// </summary>
        [Bars.B4.Utils.Display("Недропользователь")]
        [Bars.Rms.Core.Attributes.Uid("e807e052-e339-4a4e-81cb-8471681d6b60")]
        public virtual Bars.Nedragis.NedropolZovatelListModel NedropolZovatel
        {
            get;
            set;
        }

        /// <summary>
        /// Лицензионный участок
        /// </summary>
        [Bars.B4.Utils.Display("Лицензионный участок")]
        [Bars.Rms.Core.Attributes.Uid("7528d83f-6e39-41df-8f53-de5905f6f128")]
        public virtual Bars.Nedragis.LicUchastkiListModel luch
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Текст'
        /// </summary>
        [Bars.B4.Utils.Display("Текст")]
        [Bars.Rms.Core.Attributes.Uid("68e53a60-0d38-4fec-ba18-f99c144d0fed")]
        public virtual System.String texto
        {
            get;
            set;
        }
    }
}