namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Спецификация2'
    /// </summary>
    public interface ISpecifikacijaEditor2ViewModel : IEntityEditorViewModel<Bars.Nedragis.Specifikacija, SpecifikacijaEditor2Model>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Спецификация2'
    /// </summary>
    public interface ISpecifikacijaEditor2Validator : IEditorModelValidator<SpecifikacijaEditor2Model>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Спецификация2'
    /// </summary>
    public interface ISpecifikacijaEditor2Handler : IEntityEditorViewModelHandler<Bars.Nedragis.Specifikacija, SpecifikacijaEditor2Model>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Спецификация2'
    /// </summary>
    public abstract class AbstractSpecifikacijaEditor2Handler : EntityEditorViewModelHandler<Bars.Nedragis.Specifikacija, SpecifikacijaEditor2Model>, ISpecifikacijaEditor2Handler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Спецификация2'
    /// </summary>
    public class SpecifikacijaEditor2ViewModel : BaseEditorViewModel<Bars.Nedragis.Specifikacija, SpecifikacijaEditor2Model>, ISpecifikacijaEditor2ViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override SpecifikacijaEditor2Model CreateModelInternal(BaseParams @params)
        {
            var model = new SpecifikacijaEditor2Model();
            var varElement1478607204176Id = @params.Params.GetAs<long>("Element1478607204176_Id", 0);
            if (varElement1478607204176Id > 0)
            {
                model.Element1478607204176 = Container.Resolve<Bars.Nedragis.ISotrudnikiListQuery>().GetById(varElement1478607204176Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Спецификация' в модель представления
        /// </summary>
        protected override SpecifikacijaEditor2Model MapEntityInternal(Bars.Nedragis.Specifikacija entity)
        {
            // создаем экзепляр модели
            var model = new SpecifikacijaEditor2Model();
            model.Id = entity.Id;
            model.nomsp = (System.Int32? )(entity.nomsp);
            model.Element1478601989143 = (System.DateTime? )(entity.Element1478601989143);
            model.Element1478602041541 = (System.Int32? )(entity.Element1478602041541);
            model.Element1478602079636 = (System.Decimal? )(entity.Element1478602079636);
            model.Element1478602280592 = (System.Int32? )(entity.Element1478602280592);
            model.Element1478607128570 = (System.String)(entity.Element1478607128570);
            if (entity.Element1478607204176.IsNotNull())
            {
                var querySotrudnikiList = Container.Resolve<Bars.Nedragis.ISotrudnikiListQuery>();
                model.Element1478607204176 = querySotrudnikiList.GetById(entity.Element1478607204176.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Спецификация' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Specifikacija entity, SpecifikacijaEditor2Model model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.nomsp.HasValue)
            {
                entity.nomsp = model.nomsp.Value;
            }

            entity.Element1478601989143 = model.Element1478601989143.GetValueOrDefault();
            if (model.Element1478602041541.HasValue)
            {
                entity.Element1478602041541 = model.Element1478602041541.Value;
            }

            if (model.Element1478602079636.HasValue)
            {
                entity.Element1478602079636 = model.Element1478602079636.Value;
            }

            if (model.Element1478602280592.HasValue)
            {
                entity.Element1478602280592 = model.Element1478602280592.Value;
            }

            entity.Element1478607128570 = model.Element1478607128570;
            entity.Element1478607204176 = TryLoadEntityById<Bars.Nedragis.Sotrudniki>(model.Element1478607204176?.Id);
        }
    }
}