namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Спецификация2' для отдачи на клиент
    /// </summary>
    public class SpecifikacijaEditor2Model : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public SpecifikacijaEditor2Model()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер спецификации'
        /// </summary>
        [Bars.B4.Utils.Display("Номер спецификации")]
        [Bars.Rms.Core.Attributes.Uid("75740c45-ea06-4f40-bc20-8455d5a00f15")]
        public virtual System.Int32? nomsp
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата спецификации'
        /// </summary>
        [Bars.B4.Utils.Display("Дата спецификации")]
        [Bars.Rms.Core.Attributes.Uid("188b172f-bb4d-429d-937d-ebfce54109e7")]
        public virtual System.DateTime? Element1478601989143
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Количество'
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("19bb9045-ae22-4a9d-9a60-b05172b696a9")]
        public virtual System.Int32? Element1478602041541
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Итоговая сумма'
        /// </summary>
        [Bars.B4.Utils.Display("Итоговая сумма")]
        [Bars.Rms.Core.Attributes.Uid("3693e7da-a823-400b-acb9-aa542a053154")]
        public virtual System.Decimal? Element1478602079636
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер позиции спецификации'
        /// </summary>
        [Bars.B4.Utils.Display("Номер позиции спецификации")]
        [Bars.Rms.Core.Attributes.Uid("e2da8124-7661-4e44-8f00-38191675d135")]
        public virtual System.Int32? Element1478602280592
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер заявки'
        /// </summary>
        [Bars.B4.Utils.Display("Номер заявки")]
        [Bars.Rms.Core.Attributes.Uid("f060f8b3-b611-4913-8e29-02f35a970873")]
        public virtual System.String Element1478607128570
        {
            get;
            set;
        }

        /// <summary>
        /// Сотрудник
        /// </summary>
        [Bars.B4.Utils.Display("Сотрудник")]
        [Bars.Rms.Core.Attributes.Uid("f252d555-fe02-4e21-9f1b-2dfabd06b800")]
        public virtual Bars.Nedragis.SotrudnikiListModel Element1478607204176
        {
            get;
            set;
        }
    }
}