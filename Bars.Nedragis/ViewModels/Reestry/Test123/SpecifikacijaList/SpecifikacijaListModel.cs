namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Спецификация'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Спецификация")]
    public class SpecifikacijaListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер спецификации' (псевдоним: nomsp)
        /// </summary>
        [Bars.B4.Utils.Display("Номер спецификации")]
        [Bars.Rms.Core.Attributes.Uid("8b055112-beec-4108-851a-cb455aff8597")]
        public virtual System.Int32? nomsp
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата спецификации' (псевдоним: Element1478601989143)
        /// </summary>
        [Bars.B4.Utils.Display("Дата спецификации")]
        [Bars.Rms.Core.Attributes.Uid("8b6ededc-8527-45dd-8d92-daaadceae350")]
        public virtual System.DateTime? Element1478601989143
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер заявки' (псевдоним: Element1478607128570)
        /// </summary>
        [Bars.B4.Utils.Display("Номер заявки")]
        [Bars.Rms.Core.Attributes.Uid("bff2855f-bfad-4a3c-b599-82e12c94d6d2")]
        public virtual System.String Element1478607128570
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Итоговая сумма' (псевдоним: Element1478602079636)
        /// </summary>
        [Bars.B4.Utils.Display("Итоговая сумма")]
        [Bars.Rms.Core.Attributes.Uid("9f4eab64-0b69-4688-992f-ba7de197e9b9")]
        public virtual System.Decimal? Element1478602079636
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}