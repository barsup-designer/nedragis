namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Спецификация'
    /// </summary>
    public interface ISpecifikacijaListQuery : IQueryOperation<Bars.Nedragis.Specifikacija, Bars.Nedragis.SpecifikacijaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Спецификация'
    /// </summary>
    public interface ISpecifikacijaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Specifikacija, Bars.Nedragis.SpecifikacijaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Спецификация'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Спецификация")]
    [ApiAccessible("Реестр Спецификация")]
    public class SpecifikacijaListQuery : RmsEntityQueryOperation<Bars.Nedragis.Specifikacija, Bars.Nedragis.SpecifikacijaListModel>, ISpecifikacijaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "SpecifikacijaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public SpecifikacijaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Specifikacija> Filter(IQueryable<Bars.Nedragis.Specifikacija> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.SpecifikacijaListModel> Map(IQueryable<Bars.Nedragis.Specifikacija> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Specifikacija>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.SpecifikacijaListModel{Id = x.Id, _TypeUid = "640f4644-e8f4-4163-978a-3065538260cc", nomsp = (System.Int32? )(x.nomsp), Element1478601989143 = (System.DateTime? )(x.Element1478601989143), Element1478607128570 = (System.String)(x.Element1478607128570), Element1478602079636 = (System.Decimal? )(x.Element1478602079636), });
        }
    }
}