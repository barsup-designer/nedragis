namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Спецификация'
    /// </summary>
    public interface ISpecifikacijaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Specifikacija, SpecifikacijaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Спецификация'
    /// </summary>
    public interface ISpecifikacijaEditorValidator : IEditorModelValidator<SpecifikacijaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Спецификация'
    /// </summary>
    public interface ISpecifikacijaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Specifikacija, SpecifikacijaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Спецификация'
    /// </summary>
    public abstract class AbstractSpecifikacijaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Specifikacija, SpecifikacijaEditorModel>, ISpecifikacijaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Спецификация'
    /// </summary>
    public class SpecifikacijaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Specifikacija, SpecifikacijaEditorModel>, ISpecifikacijaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override SpecifikacijaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new SpecifikacijaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Спецификация' в модель представления
        /// </summary>
        protected override SpecifikacijaEditorModel MapEntityInternal(Bars.Nedragis.Specifikacija entity)
        {
            // создаем экзепляр модели
            var model = new SpecifikacijaEditorModel();
            model.Id = entity.Id;
            model.Element1478601989143 = (System.DateTime? )(entity.Element1478601989143);
            model.nomsp = (System.Int32? )(entity.nomsp);
            model.Element1478607128570 = (System.String)(entity.Element1478607128570);
            model.Element1478602079636 = (System.Decimal? )(entity.Element1478602079636);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Спецификация' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Specifikacija entity, SpecifikacijaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1478601989143 = model.Element1478601989143.GetValueOrDefault();
            if (model.nomsp.HasValue)
            {
                entity.nomsp = model.nomsp.Value;
            }

            entity.Element1478607128570 = model.Element1478607128570;
            if (model.Element1478602079636.HasValue)
            {
                entity.Element1478602079636 = model.Element1478602079636.Value;
            }
        }
    }
}