namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Спецификация' для отдачи на клиент
    /// </summary>
    public class SpecifikacijaEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public SpecifikacijaEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата спецификации'
        /// </summary>
        [Bars.B4.Utils.Display("Дата спецификации")]
        [Bars.Rms.Core.Attributes.Uid("2bd09937-e609-4ebe-bda3-8ffc67c7b31b")]
        public virtual System.DateTime? Element1478601989143
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер спецификации'
        /// </summary>
        [Bars.B4.Utils.Display("Номер спецификации")]
        [Bars.Rms.Core.Attributes.Uid("f0f6316e-d032-4545-9efb-c66773ec904d")]
        public virtual System.Int32? nomsp
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер заявки'
        /// </summary>
        [Bars.B4.Utils.Display("Номер заявки")]
        [Bars.Rms.Core.Attributes.Uid("5cb2d79e-6e97-4ce9-bd17-819b944a9332")]
        public virtual System.String Element1478607128570
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Итоговая сумма'
        /// </summary>
        [Bars.B4.Utils.Display("Итоговая сумма")]
        [Bars.Rms.Core.Attributes.Uid("0008273d-85f5-49b4-bc9d-b7b981bae2e4")]
        public virtual System.Decimal? Element1478602079636
        {
            get;
            set;
        }
    }
}