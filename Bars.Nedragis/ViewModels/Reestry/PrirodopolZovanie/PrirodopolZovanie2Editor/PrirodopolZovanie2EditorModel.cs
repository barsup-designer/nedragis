namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Природопользование 2' для отдачи на клиент
    /// </summary>
    public class PrirodopolZovanie2EditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public PrirodopolZovanie2EditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата входящего'
        /// </summary>
        [Bars.B4.Utils.Display("Дата входящего")]
        [Bars.Rms.Core.Attributes.Uid("4fe7011e-f87e-461d-af1a-53ae5758691c")]
        public virtual System.DateTime? DateInPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер входящего'
        /// </summary>
        [Bars.B4.Utils.Display("Номер входящего")]
        [Bars.Rms.Core.Attributes.Uid("8f0c7a7a-5951-4704-b7df-a3fc3825c583")]
        public virtual System.String NumInDocsPr2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата исполнения'
        /// </summary>
        [Bars.B4.Utils.Display("Дата исполнения")]
        [Bars.Rms.Core.Attributes.Uid("74c71a89-f9b8-4239-9d0e-2913ea6c329d")]
        public virtual System.DateTime? DateCompletePR2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Компания Заказчик'
        /// </summary>
        [Bars.B4.Utils.Display("Компания Заказчик")]
        [Bars.Rms.Core.Attributes.Uid("83d8c5aa-189e-4bc7-a10f-babd85688f5d")]
        public virtual System.String CompanyPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("38a1db10-c634-418a-9185-3e84e8dbae17")]
        public virtual Bars.Nedragis.VidRabotPrirListModel VidRabotPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Количество'
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("0b68cc06-0cfa-4b70-a111-80e63b5be7f5")]
        public virtual System.Int32? QuantityPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Объект'
        /// </summary>
        [Bars.B4.Utils.Display("Объект")]
        [Bars.Rms.Core.Attributes.Uid("2c939258-9120-4ed8-a3c9-a1b409db3aaf")]
        public virtual System.String ObjectPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер договора'
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("82ae0b75-46bf-481b-b894-f1755cf5f518")]
        public virtual System.String NumDogovorPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер заявки '
        /// </summary>
        [Bars.B4.Utils.Display("Номер заявки ")]
        [Bars.Rms.Core.Attributes.Uid("25761307-10b6-4532-9ab9-620f391e38c6")]
        public virtual System.Int32? NumZayvkaPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Код'
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("aea7cdf1-cdf2-4a69-a676-099c8fd6d2f8")]
        public virtual System.String CodePR2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сумма к оплате'
        /// </summary>
        [Bars.B4.Utils.Display("Сумма к оплате")]
        [Bars.Rms.Core.Attributes.Uid("0a974463-7b3c-4226-a4c8-eeaa013e58b8")]
        public virtual System.Double? SummToPayPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Идентификационный номер'
        /// </summary>
        [Bars.B4.Utils.Display("Идентификационный номер")]
        [Bars.Rms.Core.Attributes.Uid("948a3c02-651f-49ea-b796-3107c7152b31")]
        public virtual System.String IdentNumderPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Исполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("6c2e8c02-9b1f-411a-b5ff-55ac4e447d50")]
        public virtual Bars.Nedragis.Sotrudniki1ListModel IspolnitelPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Соисполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Соисполнитель")]
        [Bars.Rms.Core.Attributes.Uid("d775a307-6c91-43ed-8b8c-6d8f62737a46")]
        public virtual Bars.Nedragis.Sotrudniki1ListModel SoispolnitelPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Месяц
        /// </summary>
        [Bars.B4.Utils.Display("Месяц")]
        [Bars.Rms.Core.Attributes.Uid("45d8a46f-74e4-4e72-b88f-dac437cf5923")]
        public virtual Bars.Nedragis.MesjacPrirodaListModel MonthPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'ФИО заказчика'
        /// </summary>
        [Bars.B4.Utils.Display("ФИО заказчика")]
        [Bars.Rms.Core.Attributes.Uid("5af7e153-ba45-407b-bf02-95e1398bf89f")]
        public virtual System.String FIOZakazchikaPR2
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Примечание'
        /// </summary>
        [Bars.B4.Utils.Display("Примечание")]
        [Bars.Rms.Core.Attributes.Uid("69d56300-ff21-4f47-b9cb-8ae20f67bb28")]
        public virtual System.String PrimechaniePR2
        {
            get;
            set;
        }
    }
}