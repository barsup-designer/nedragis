namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Природопользование 2'
    /// </summary>
    public interface IPrirodopolZovanie2EditorViewModel : IEntityEditorViewModel<Bars.Nedragis.PrirodopolZovanie2, PrirodopolZovanie2EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Природопользование 2'
    /// </summary>
    public interface IPrirodopolZovanie2EditorValidator : IEditorModelValidator<PrirodopolZovanie2EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Природопользование 2'
    /// </summary>
    public interface IPrirodopolZovanie2EditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.PrirodopolZovanie2, PrirodopolZovanie2EditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Природопользование 2'
    /// </summary>
    public abstract class AbstractPrirodopolZovanie2EditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.PrirodopolZovanie2, PrirodopolZovanie2EditorModel>, IPrirodopolZovanie2EditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Природопользование 2'
    /// </summary>
    public class PrirodopolZovanie2EditorViewModel : BaseEditorViewModel<Bars.Nedragis.PrirodopolZovanie2, PrirodopolZovanie2EditorModel>, IPrirodopolZovanie2EditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override PrirodopolZovanie2EditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new PrirodopolZovanie2EditorModel();
            var varVidRabotPR2Id = @params.Params.GetAs<long>("VidRabotPR2_Id", 0);
            if (varVidRabotPR2Id > 0)
            {
                model.VidRabotPR2 = Container.Resolve<Bars.Nedragis.IVidRabotPrirListQuery>().GetById(varVidRabotPR2Id);
            }

            var varIspolnitelPR2Id = @params.Params.GetAs<long>("IspolnitelPR2_Id", 0);
            if (varIspolnitelPR2Id > 0)
            {
                model.IspolnitelPR2 = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>().GetById(varIspolnitelPR2Id);
            }

            var varSoispolnitelPR2Id = @params.Params.GetAs<long>("SoispolnitelPR2_Id", 0);
            if (varSoispolnitelPR2Id > 0)
            {
                model.SoispolnitelPR2 = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>().GetById(varSoispolnitelPR2Id);
            }

            var varMonthPR2Id = @params.Params.GetAs<long>("MonthPR2_Id", 0);
            if (varMonthPR2Id > 0)
            {
                model.MonthPR2 = Container.Resolve<Bars.Nedragis.IMesjacPrirodaListQuery>().GetById(varMonthPR2Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Природопользование 2' в модель представления
        /// </summary>
        protected override PrirodopolZovanie2EditorModel MapEntityInternal(Bars.Nedragis.PrirodopolZovanie2 entity)
        {
            // создаем экзепляр модели
            var model = new PrirodopolZovanie2EditorModel();
            model.Id = entity.Id;
            model.DateInPR2 = (System.DateTime? )(entity.DateInPR2);
            model.NumInDocsPr2 = (System.String)(entity.NumInDocsPr2);
            model.DateCompletePR2 = (System.DateTime? )(entity.DateCompletePR2);
            model.CompanyPR2 = (System.String)(entity.CompanyPR2);
            if (entity.VidRabotPR2.IsNotNull())
            {
                var queryVidRabotPrirList = Container.Resolve<Bars.Nedragis.IVidRabotPrirListQuery>();
                model.VidRabotPR2 = queryVidRabotPrirList.GetById(entity.VidRabotPR2.Id);
            }

            model.QuantityPR2 = (System.Int32? )(entity.QuantityPR2);
            model.ObjectPR2 = (System.String)(entity.ObjectPR2);
            model.NumDogovorPR2 = (System.String)(entity.NumDogovorPR2);
            model.NumZayvkaPR2 = (System.Int32? )(entity.NumZayvkaPR2);
            model.CodePR2 = (System.String)(entity.CodePR2);
            model.SummToPayPR2 = (System.Double? )(entity.SummToPayPR2);
            model.IdentNumderPR2 = (System.String)(entity.IdentNumderPR2);
            if (entity.IspolnitelPR2.IsNotNull())
            {
                var querySotrudniki1List = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>();
                model.IspolnitelPR2 = querySotrudniki1List.GetById(entity.IspolnitelPR2.Id);
            }

            if (entity.SoispolnitelPR2.IsNotNull())
            {
                var querySotrudniki1List = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>();
                model.SoispolnitelPR2 = querySotrudniki1List.GetById(entity.SoispolnitelPR2.Id);
            }

            if (entity.MonthPR2.IsNotNull())
            {
                var queryMesjacPrirodaList = Container.Resolve<Bars.Nedragis.IMesjacPrirodaListQuery>();
                model.MonthPR2 = queryMesjacPrirodaList.GetById(entity.MonthPR2.Id);
            }

            model.FIOZakazchikaPR2 = (System.String)(entity.FIOZakazchikaPR2);
            model.PrimechaniePR2 = (System.String)(entity.PrimechaniePR2);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Природопользование 2' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.PrirodopolZovanie2 entity, PrirodopolZovanie2EditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.DateInPR2 = model.DateInPR2.GetValueOrDefault();
            entity.NumInDocsPr2 = model.NumInDocsPr2;
            entity.DateCompletePR2 = model.DateCompletePR2.GetValueOrDefault();
            entity.CompanyPR2 = model.CompanyPR2;
            entity.VidRabotPR2 = TryLoadEntityById<Bars.Nedragis.VidRabotPrir>(model.VidRabotPR2?.Id);
            if (model.QuantityPR2.HasValue)
            {
                entity.QuantityPR2 = model.QuantityPR2.Value;
            }

            entity.ObjectPR2 = model.ObjectPR2;
            entity.NumDogovorPR2 = model.NumDogovorPR2;
            if (model.NumZayvkaPR2.HasValue)
            {
                entity.NumZayvkaPR2 = model.NumZayvkaPR2.Value;
            }

            entity.CodePR2 = model.CodePR2;
            if (model.SummToPayPR2.HasValue)
            {
                entity.SummToPayPR2 = model.SummToPayPR2.Value;
            }

            entity.IdentNumderPR2 = model.IdentNumderPR2;
            entity.IspolnitelPR2 = TryLoadEntityById<Bars.Nedragis.Sotrudniki1>(model.IspolnitelPR2?.Id);
            entity.SoispolnitelPR2 = TryLoadEntityById<Bars.Nedragis.Sotrudniki1>(model.SoispolnitelPR2?.Id);
            entity.MonthPR2 = TryLoadEntityById<Bars.Nedragis.MesjacPriroda>(model.MonthPR2?.Id);
            entity.FIOZakazchikaPR2 = model.FIOZakazchikaPR2;
            entity.PrimechaniePR2 = model.PrimechaniePR2;
        }
    }
}