namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Природопользование 2'
    /// </summary>
    public interface IPrirodopolZovanie2ListQuery : IQueryOperation<Bars.Nedragis.PrirodopolZovanie2, Bars.Nedragis.PrirodopolZovanie2ListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Природопользование 2'
    /// </summary>
    public interface IPrirodopolZovanie2ListQueryFilter : IQueryOperationFilter<Bars.Nedragis.PrirodopolZovanie2, Bars.Nedragis.PrirodopolZovanie2ListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Природопользование 2'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Природопользование 2")]
    public class PrirodopolZovanie2ListQuery : RmsEntityQueryOperation<Bars.Nedragis.PrirodopolZovanie2, Bars.Nedragis.PrirodopolZovanie2ListModel>, IPrirodopolZovanie2ListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "PrirodopolZovanie2ListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public PrirodopolZovanie2ListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.PrirodopolZovanie2> Filter(IQueryable<Bars.Nedragis.PrirodopolZovanie2> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.PrirodopolZovanie2ListModel> Map(IQueryable<Bars.Nedragis.PrirodopolZovanie2> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.PrirodopolZovanie2>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.PrirodopolZovanie2ListModel{Id = x.Id, _TypeUid = "825f8a0a-cc49-42e0-8d04-df96f9f2e519", DateInPR222 = (System.DateTime? )(x.DateInPR2), NumInDocsPr222 = (System.String)(x.NumInDocsPr2), DateCompletePR222 = (System.DateTime? )(x.DateCompletePR2), CompanyPR222 = (System.String)(x.CompanyPR2), VidRabotPR2_vid_rabot_prirod_s222 = (System.String)(x.VidRabotPR2.vid_rabot_prirod_s), QuantityPR222 = (System.Int32? )(x.QuantityPR2), ObjectPR222 = (System.String)(x.ObjectPR2), NumDogovorPR222 = (System.String)(x.NumDogovorPR2), NumZayvkaPR222 = (System.Int32? )(x.NumZayvkaPR2), CodePR222 = (System.String)(x.CodePR2), SummToPayPR222 = (System.Double? )(x.SummToPayPR2), IdentNumderPR222 = (System.String)(x.IdentNumderPR2), IspolnitelPR2_Element1474352136232PR2 = (System.String)(x.IspolnitelPR2.Element1474352136232), SoispolnitelPR22222 = (System.String)(x.SoispolnitelPR2.Element1474352136232), MonthPR2_month_pr222 = (System.String)(x.MonthPR2.month_pr), FIOZakazchikaPR222 = (System.String)(x.FIOZakazchikaPR2), PrimechaniePR222 = (System.String)(x.PrimechaniePR2), });
        }
    }
}