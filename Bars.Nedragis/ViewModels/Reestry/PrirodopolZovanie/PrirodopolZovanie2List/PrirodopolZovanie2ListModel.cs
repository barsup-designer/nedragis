namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Природопользование 2'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Природопользование 2")]
    public class PrirodopolZovanie2ListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата входящего' (псевдоним: DateInPR222)
        /// </summary>
        [Bars.B4.Utils.Display("Дата входящего")]
        [Bars.Rms.Core.Attributes.Uid("432f5e1e-f2ca-425c-8106-0649d0c2af21")]
        public virtual System.DateTime? DateInPR222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер входящего' (псевдоним: NumInDocsPr222)
        /// </summary>
        [Bars.B4.Utils.Display("Номер входящего")]
        [Bars.Rms.Core.Attributes.Uid("db1c9793-7c69-430f-87a3-7fae563340f4")]
        public virtual System.String NumInDocsPr222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата исполнения' (псевдоним: DateCompletePR222)
        /// </summary>
        [Bars.B4.Utils.Display("Дата исполнения")]
        [Bars.Rms.Core.Attributes.Uid("6b603598-2fae-42ec-a90e-d13609a0b893")]
        public virtual System.DateTime? DateCompletePR222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Компания Заказчик' (псевдоним: CompanyPR222)
        /// </summary>
        [Bars.B4.Utils.Display("Компания Заказчик")]
        [Bars.Rms.Core.Attributes.Uid("48ec3b29-cbb9-4abd-8c85-bdb6288f0f82")]
        public virtual System.String CompanyPR222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Вид работ.Вид работ' (псевдоним: VidRabotPR2_vid_rabot_prirod_s222)
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ.Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("1b9b0de7-0166-452c-a3c1-d9d10790749d")]
        public virtual System.String VidRabotPR2_vid_rabot_prirod_s222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Количество' (псевдоним: QuantityPR222)
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("fab98012-0ad1-474a-ba73-fabb1ce42cea")]
        public virtual System.Int32? QuantityPR222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Объект' (псевдоним: ObjectPR222)
        /// </summary>
        [Bars.B4.Utils.Display("Объект")]
        [Bars.Rms.Core.Attributes.Uid("20a22d1a-1b46-4aa0-926e-96a9c81bf43d")]
        public virtual System.String ObjectPR222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер договора' (псевдоним: NumDogovorPR222)
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("1d9d411a-acbf-411f-8d88-c40648af7cdf")]
        public virtual System.String NumDogovorPR222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер заявки ' (псевдоним: NumZayvkaPR222)
        /// </summary>
        [Bars.B4.Utils.Display("Номер заявки ")]
        [Bars.Rms.Core.Attributes.Uid("5092b7b7-5c47-49d5-975b-e2b01b23dab6")]
        public virtual System.Int32? NumZayvkaPR222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Код' (псевдоним: CodePR222)
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("35730e75-a4fc-458c-85bc-812d10dc3534")]
        public virtual System.String CodePR222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Сумма к оплате' (псевдоним: SummToPayPR222)
        /// </summary>
        [Bars.B4.Utils.Display("Сумма к оплате")]
        [Bars.Rms.Core.Attributes.Uid("95bdce7b-478e-4711-b1f9-a39d6b98d8a8")]
        public virtual System.Double? SummToPayPR222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Идентификационный номер' (псевдоним: IdentNumderPR222)
        /// </summary>
        [Bars.B4.Utils.Display("Идентификационный номер")]
        [Bars.Rms.Core.Attributes.Uid("5f64b0b2-ca75-4be4-adeb-6b9339e61162")]
        public virtual System.String IdentNumderPR222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Исполнитель.ФИО' (псевдоним: IspolnitelPR2_Element1474352136232PR2)
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель.ФИО")]
        [Bars.Rms.Core.Attributes.Uid("d1c7d594-60af-446f-bf5f-c486b40077b2")]
        public virtual System.String IspolnitelPR2_Element1474352136232PR2
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Соисполнитель.ФИО' (псевдоним: SoispolnitelPR22222)
        /// </summary>
        [Bars.B4.Utils.Display("Соисполнитель.ФИО")]
        [Bars.Rms.Core.Attributes.Uid("5ad0737b-9ce2-4bae-b0bc-7478fdc1f8f8")]
        public virtual System.String SoispolnitelPR22222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Месяц.Отчетный месяц' (псевдоним: MonthPR2_month_pr222)
        /// </summary>
        [Bars.B4.Utils.Display("Месяц.Отчетный месяц")]
        [Bars.Rms.Core.Attributes.Uid("0d824992-9b82-47fa-96f9-f9c55fcaec9b")]
        public virtual System.String MonthPR2_month_pr222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'ФИО заказчика' (псевдоним: FIOZakazchikaPR222)
        /// </summary>
        [Bars.B4.Utils.Display("ФИО заказчика")]
        [Bars.Rms.Core.Attributes.Uid("7f5a90d7-aa86-4bd9-bc08-0c0097ab3538")]
        public virtual System.String FIOZakazchikaPR222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Примечание' (псевдоним: PrimechaniePR222)
        /// </summary>
        [Bars.B4.Utils.Display("Примечание")]
        [Bars.Rms.Core.Attributes.Uid("7f5f7517-0e14-477b-bcbc-a54e36a0f5bc")]
        public virtual System.String PrimechaniePR222
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}