namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Землеустройство'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Землеустройство")]
    public class ZemleustrListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование отдела.Наименование' (псевдоним: name_z_NAME_SP)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("0cc30b7b-524a-418a-ac67-ae8599f8cc84")]
        public virtual System.String name_z_NAME_SP
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер дела/проекта' (псевдоним: NDelo)
        /// </summary>
        [Bars.B4.Utils.Display("Номер дела/проекта")]
        [Bars.Rms.Core.Attributes.Uid("740c2dbb-1a61-4421-a334-c121c9ee05bb")]
        public virtual System.String NDelo
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Вид работ' (псевдоним: vid_rabot)
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("0212807e-c452-4566-af99-68ae71090020")]
        public virtual System.String vid_rabot
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Кому' (псевдоним: komu)
        /// </summary>
        [Bars.B4.Utils.Display("Кому")]
        [Bars.Rms.Core.Attributes.Uid("622bfdcb-1048-4e76-8682-7c8c496ee013")]
        public virtual System.String komu
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Компания' (псевдоним: company)
        /// </summary>
        [Bars.B4.Utils.Display("Компания")]
        [Bars.Rms.Core.Attributes.Uid("2c28d7c1-e254-42fd-8b28-fcd814ff37bb")]
        public virtual System.String company
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Название объекта' (псевдоним: name_obj)
        /// </summary>
        [Bars.B4.Utils.Display("Название объекта")]
        [Bars.Rms.Core.Attributes.Uid("a1a06697-1004-4cef-823a-3270f217434a")]
        public virtual System.String name_obj
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Кадастровый номер' (псевдоним: kad_nom)
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый номер")]
        [Bars.Rms.Core.Attributes.Uid("57ea18e6-862a-4704-bb4b-606f4a3956d7")]
        public virtual System.String kad_nom
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Исходная категория земель' (псевдоним: kateg_zem)
        /// </summary>
        [Bars.B4.Utils.Display("Исходная категория земель")]
        [Bars.Rms.Core.Attributes.Uid("cdd430f2-6e2e-4063-b007-b597f669094a")]
        public virtual System.String kateg_zem
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Конечная категория земель' (псевдоним: konech_kategor)
        /// </summary>
        [Bars.B4.Utils.Display("Конечная категория земель")]
        [Bars.Rms.Core.Attributes.Uid("0e68e16c-be2d-4d95-8957-5fa0aeba9cf1")]
        public virtual System.String konech_kategor
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Площадь' (псевдоним: square)
        /// </summary>
        [Bars.B4.Utils.Display("Площадь")]
        [Bars.Rms.Core.Attributes.Uid("785eacd1-4390-4959-9ce7-3bcbc4236bd7")]
        public virtual System.Double? square
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Район.Наименование района' (псевдоним: region_name)
        /// </summary>
        [Bars.B4.Utils.Display("Район.Наименование района")]
        [Bars.Rms.Core.Attributes.Uid("ac26815a-2f65-4ef3-b53a-159dab538849")]
        public virtual System.String region_name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата поступления в ГКУ "Ресурсы Ямала"' (псевдоним: date_postupl)
        /// </summary>
        [Bars.B4.Utils.Display("Дата поступления в ГКУ \"Ресурсы Ямала\"")]
        [Bars.Rms.Core.Attributes.Uid("fcb2362d-ad0f-4868-9b53-3703f52ac6c2")]
        public virtual System.DateTime? date_postupl
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Этап' (псевдоним: Etap)
        /// </summary>
        [Bars.B4.Utils.Display("Этап")]
        [Bars.Rms.Core.Attributes.Uid("84212c18-5adb-4f7d-8786-ca5b71ad2f03")]
        public virtual System.String Etap
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата входящего документа' (псевдоним: date_vh_doc)
        /// </summary>
        [Bars.B4.Utils.Display("Дата входящего документа")]
        [Bars.Rms.Core.Attributes.Uid("94e6859f-0459-4c63-bb9a-776066c291a3")]
        public virtual System.DateTime? date_vh_doc
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер входящего документа' (псевдоним: nom_vh_doc)
        /// </summary>
        [Bars.B4.Utils.Display("Номер входящего документа")]
        [Bars.Rms.Core.Attributes.Uid("2508b416-f63f-44ec-9ec1-4c949be7c0d4")]
        public virtual System.String nom_vh_doc
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Автор(исполнитель).ФИО' (псевдоним: autor_Element1474352136232)
        /// </summary>
        [Bars.B4.Utils.Display("Автор(исполнитель).ФИО")]
        [Bars.Rms.Core.Attributes.Uid("126e09ab-a2ad-4a33-a0a8-3f172a6d9b75")]
        public virtual System.String autor_Element1474352136232
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер письма заявителя' (псевдоним: nomer_pisma)
        /// </summary>
        [Bars.B4.Utils.Display("Номер письма заявителя")]
        [Bars.Rms.Core.Attributes.Uid("c4b86861-70ee-46ff-a529-10ea9da527e7")]
        public virtual System.Int32? nomer_pisma
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата исполнения' (псевдоним: data_usp)
        /// </summary>
        [Bars.B4.Utils.Display("Дата исполнения")]
        [Bars.Rms.Core.Attributes.Uid("1098161c-9529-4e5b-949d-edc0b6fbbec6")]
        public virtual System.DateTime? data_usp
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Исполнитель' (псевдоним: ispol)
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("e22db2c1-cfa6-44dc-b540-81264d09f0b5")]
        public virtual System.String ispol
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Формат данных' (псевдоним: format)
        /// </summary>
        [Bars.B4.Utils.Display("Формат данных")]
        [Bars.Rms.Core.Attributes.Uid("2dffe708-3ba9-48f6-bc32-bc8863077c1a")]
        public virtual System.String format
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Количество единиц' (псевдоним: kolvo_ed)
        /// </summary>
        [Bars.B4.Utils.Display("Количество единиц")]
        [Bars.Rms.Core.Attributes.Uid("907c6041-e98d-44a1-86b9-99fb77710992")]
        public virtual System.Int32? kolvo_ed
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Примечание' (псевдоним: prim)
        /// </summary>
        [Bars.B4.Utils.Display("Примечание")]
        [Bars.Rms.Core.Attributes.Uid("a50d5528-2f57-4b95-8180-063b2b02c9cf")]
        public virtual System.String prim
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Сумма калькуляции' (псевдоним: sum_kalk)
        /// </summary>
        [Bars.B4.Utils.Display("Сумма калькуляции")]
        [Bars.Rms.Core.Attributes.Uid("fdf34899-193a-47ef-8450-7955f1a4290d")]
        public virtual System.Double? sum_kalk
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}