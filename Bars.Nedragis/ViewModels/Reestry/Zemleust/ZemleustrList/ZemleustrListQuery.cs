namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Землеустройство'
    /// </summary>
    public interface IZemleustrListQuery : IQueryOperation<Bars.Nedragis.Zemleustr, Bars.Nedragis.ZemleustrListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Землеустройство'
    /// </summary>
    public interface IZemleustrListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Zemleustr, Bars.Nedragis.ZemleustrListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Землеустройство'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Землеустройство")]
    public class ZemleustrListQuery : RmsEntityQueryOperation<Bars.Nedragis.Zemleustr, Bars.Nedragis.ZemleustrListModel>, IZemleustrListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "ZemleustrListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public ZemleustrListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Zemleustr> Filter(IQueryable<Bars.Nedragis.Zemleustr> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.ZemleustrListModel> Map(IQueryable<Bars.Nedragis.Zemleustr> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Zemleustr>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.ZemleustrListModel{Id = x.Id, _TypeUid = "f43e2a1e-85d1-44ff-8cb6-87595813d7ad", name_z_NAME_SP = (System.String)(x.name_z.NAME_SP), NDelo = (System.String)(x.NDelo), vid_rabot = (System.String)(x.vid_rabot), komu = (System.String)(x.komu), company = (System.String)(x.company), name_obj = (System.String)(x.name_obj), kad_nom = (System.String)(x.kad_nom), kateg_zem = (System.String)(x.kateg_zem), konech_kategor = (System.String)(x.konech_kategor), square = (System.Double? )(x.square), region_name = (System.String)(x.region.name), date_postupl = (System.DateTime? )(x.date_postupl), Etap = (System.String)(x.Etap), date_vh_doc = (System.DateTime? )(x.date_vh_doc), nom_vh_doc = (System.String)(x.nom_vh_doc), autor_Element1474352136232 = (System.String)(x.autor.Element1474352136232), nomer_pisma = (System.Int32? )(x.nomer_pisma), data_usp = (System.DateTime? )(x.data_usp), ispol = (System.String)(x.ispol), format = (System.String)(x.format), kolvo_ed = (System.Int32? )(x.kolvo_ed), prim = (System.String)(x.prim), sum_kalk = (System.Double? )(x.sum_kalk), });
        }
    }
}