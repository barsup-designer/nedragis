namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Землеустройство' для отдачи на клиент
    /// </summary>
    public class ZemleustrEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public ZemleustrEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("15566e63-c494-4134-93cd-d1f909d3c91b")]
        public virtual Bars.Nedragis.Otdel1ListModel name_z
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер дела/проекта'
        /// </summary>
        [Bars.B4.Utils.Display("Номер дела/проекта")]
        [Bars.Rms.Core.Attributes.Uid("955f89e0-a1f2-40b9-b75c-ee7bbb8bba6d")]
        public virtual System.String NDelo
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Вид работ'
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("04898b40-90c5-4c02-be4b-e870f57be1c3")]
        public virtual System.String vid_rabot
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кому'
        /// </summary>
        [Bars.B4.Utils.Display("Кому")]
        [Bars.Rms.Core.Attributes.Uid("04aa4a44-2ab7-49d0-afed-54e58423d33e")]
        public virtual System.String komu
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Компания'
        /// </summary>
        [Bars.B4.Utils.Display("Компания")]
        [Bars.Rms.Core.Attributes.Uid("b4d3bc6f-1179-41dd-b21b-aab9c3086f1b")]
        public virtual System.String company
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Название объекта'
        /// </summary>
        [Bars.B4.Utils.Display("Название объекта")]
        [Bars.Rms.Core.Attributes.Uid("6b3a5bf6-01b7-425f-9253-f964d477b6c2")]
        public virtual System.String name_obj
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый номер'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый номер")]
        [Bars.Rms.Core.Attributes.Uid("99d7b389-159c-4505-bc2a-6b8e0894fa93")]
        public virtual System.String kad_nom
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Исходная категория земель'
        /// </summary>
        [Bars.B4.Utils.Display("Исходная категория земель")]
        [Bars.Rms.Core.Attributes.Uid("ae242339-6051-49d4-829a-6e73420e2cfc")]
        public virtual System.String kateg_zem
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата входящего документа'
        /// </summary>
        [Bars.B4.Utils.Display("Дата входящего документа")]
        [Bars.Rms.Core.Attributes.Uid("39f40dfc-38a0-463f-aff0-e157dc1917f2")]
        public virtual System.DateTime? date_vh_doc
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Конечная категория земель'
        /// </summary>
        [Bars.B4.Utils.Display("Конечная категория земель")]
        [Bars.Rms.Core.Attributes.Uid("1c68e19c-cd2e-4b35-ba4e-63dd710777b6")]
        public virtual System.String konech_kategor
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Площадь'
        /// </summary>
        [Bars.B4.Utils.Display("Площадь")]
        [Bars.Rms.Core.Attributes.Uid("7f5757f3-e498-4d0d-87a9-633594eb8372")]
        public virtual System.Double? square
        {
            get;
            set;
        }

        /// <summary>
        /// Район
        /// </summary>
        [Bars.B4.Utils.Display("Район")]
        [Bars.Rms.Core.Attributes.Uid("1b020f42-d6c7-4686-92c4-87182c5eb0a9")]
        public virtual Bars.Nedragis.RajjonyJaNAOListModel region
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата поступления в ГКУ "Ресурсы Ямала"'
        /// </summary>
        [Bars.B4.Utils.Display("Дата поступления в ГКУ \"Ресурсы Ямала\"")]
        [Bars.Rms.Core.Attributes.Uid("e9f18c91-5e00-4adf-8834-79eb4441aedb")]
        public virtual System.DateTime? date_postupl
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Этап'
        /// </summary>
        [Bars.B4.Utils.Display("Этап")]
        [Bars.Rms.Core.Attributes.Uid("de8caac8-a47f-455c-9ddf-74062e9ddaa8")]
        public virtual System.String Etap
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер входящего документа'
        /// </summary>
        [Bars.B4.Utils.Display("Номер входящего документа")]
        [Bars.Rms.Core.Attributes.Uid("b383b1cd-dd52-4d9e-b238-21f298eb854c")]
        public virtual System.String nom_vh_doc
        {
            get;
            set;
        }

        /// <summary>
        /// Автор(исполнитель)
        /// </summary>
        [Bars.B4.Utils.Display("Автор(исполнитель)")]
        [Bars.Rms.Core.Attributes.Uid("636d3b92-31e2-43bb-90d7-e7df4c2a0ec8")]
        public virtual Bars.Nedragis.Sotrudniki1ListModel autor
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер письма заявителя'
        /// </summary>
        [Bars.B4.Utils.Display("Номер письма заявителя")]
        [Bars.Rms.Core.Attributes.Uid("2c078cc1-fce3-41ff-a1d5-fdc71c8ca1a9")]
        public virtual System.Int32? nomer_pisma
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата исполнения'
        /// </summary>
        [Bars.B4.Utils.Display("Дата исполнения")]
        [Bars.Rms.Core.Attributes.Uid("b30c74c9-9791-4fa7-a970-839e975fdbdf")]
        public virtual System.DateTime? data_usp
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Исполнитель'
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("5ca4d982-21bb-46c9-a058-e4e0b0ddba7c")]
        public virtual System.String ispol
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Формат данных'
        /// </summary>
        [Bars.B4.Utils.Display("Формат данных")]
        [Bars.Rms.Core.Attributes.Uid("9313d0fd-6a29-4ffc-af05-f84c8d68e66d")]
        public virtual System.String format
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Количество единиц'
        /// </summary>
        [Bars.B4.Utils.Display("Количество единиц")]
        [Bars.Rms.Core.Attributes.Uid("c479e45b-a80b-4167-bc31-5b250c950e59")]
        public virtual System.Int32? kolvo_ed
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Примечание'
        /// </summary>
        [Bars.B4.Utils.Display("Примечание")]
        [Bars.Rms.Core.Attributes.Uid("09b1ba3e-1cad-4ca4-a9d2-2db369c8172e")]
        public virtual System.String prim
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сумма калькуляции'
        /// </summary>
        [Bars.B4.Utils.Display("Сумма калькуляции")]
        [Bars.Rms.Core.Attributes.Uid("cf296c77-f388-410f-a9e7-902f3dc3e435")]
        public virtual System.Double? sum_kalk
        {
            get;
            set;
        }
    }
}