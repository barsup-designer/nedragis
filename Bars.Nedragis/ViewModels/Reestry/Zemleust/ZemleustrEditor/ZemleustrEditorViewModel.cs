namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Землеустройство'
    /// </summary>
    public interface IZemleustrEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Zemleustr, ZemleustrEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Землеустройство'
    /// </summary>
    public interface IZemleustrEditorValidator : IEditorModelValidator<ZemleustrEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Землеустройство'
    /// </summary>
    public interface IZemleustrEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Zemleustr, ZemleustrEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Землеустройство'
    /// </summary>
    public abstract class AbstractZemleustrEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Zemleustr, ZemleustrEditorModel>, IZemleustrEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Землеустройство'
    /// </summary>
    public class ZemleustrEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Zemleustr, ZemleustrEditorModel>, IZemleustrEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ZemleustrEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new ZemleustrEditorModel();
            var varname_zId = @params.Params.GetAs<long>("name_z_Id", 0);
            if (varname_zId > 0)
            {
                model.name_z = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>().GetById(varname_zId);
            }

            var varregionId = @params.Params.GetAs<long>("region_Id", 0);
            if (varregionId > 0)
            {
                model.region = Container.Resolve<Bars.Nedragis.IRajjonyJaNAOListQuery>().GetById(varregionId);
            }

            var varautorId = @params.Params.GetAs<long>("autor_Id", 0);
            if (varautorId > 0)
            {
                model.autor = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>().GetById(varautorId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Землеустройство' в модель представления
        /// </summary>
        protected override ZemleustrEditorModel MapEntityInternal(Bars.Nedragis.Zemleustr entity)
        {
            // создаем экзепляр модели
            var model = new ZemleustrEditorModel();
            model.Id = entity.Id;
            if (entity.name_z.IsNotNull())
            {
                var queryOtdel1List = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>();
                model.name_z = queryOtdel1List.GetById(entity.name_z.Id);
            }

            model.NDelo = (System.String)(entity.NDelo);
            model.vid_rabot = (System.String)(entity.vid_rabot);
            model.komu = (System.String)(entity.komu);
            model.company = (System.String)(entity.company);
            model.name_obj = (System.String)(entity.name_obj);
            model.kad_nom = (System.String)(entity.kad_nom);
            model.kateg_zem = (System.String)(entity.kateg_zem);
            model.date_vh_doc = (System.DateTime? )(entity.date_vh_doc);
            model.konech_kategor = (System.String)(entity.konech_kategor);
            model.square = (System.Double? )(entity.square);
            if (entity.region.IsNotNull())
            {
                var queryRajjonyJaNAOList = Container.Resolve<Bars.Nedragis.IRajjonyJaNAOListQuery>();
                model.region = queryRajjonyJaNAOList.GetById(entity.region.Id);
            }

            model.date_postupl = (System.DateTime? )(entity.date_postupl);
            model.Etap = (System.String)(entity.Etap);
            model.nom_vh_doc = (System.String)(entity.nom_vh_doc);
            if (entity.autor.IsNotNull())
            {
                var querySotrudniki1List = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>();
                model.autor = querySotrudniki1List.GetById(entity.autor.Id);
            }

            model.nomer_pisma = (System.Int32? )(entity.nomer_pisma);
            model.data_usp = (System.DateTime? )(entity.data_usp);
            model.ispol = (System.String)(entity.ispol);
            model.format = (System.String)(entity.format);
            model.kolvo_ed = (System.Int32? )(entity.kolvo_ed);
            model.prim = (System.String)(entity.prim);
            model.sum_kalk = (System.Double? )(entity.sum_kalk);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Землеустройство' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Zemleustr entity, ZemleustrEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name_z = TryLoadEntityById<Bars.Nedragis.Otdel1>(model.name_z?.Id);
            entity.NDelo = model.NDelo;
            entity.vid_rabot = model.vid_rabot;
            entity.komu = model.komu;
            entity.company = model.company;
            entity.name_obj = model.name_obj;
            entity.kad_nom = model.kad_nom;
            entity.kateg_zem = model.kateg_zem;
            entity.date_vh_doc = model.date_vh_doc.GetValueOrDefault();
            entity.konech_kategor = model.konech_kategor;
            if (model.square.HasValue)
            {
                entity.square = model.square.Value;
            }

            entity.region = TryLoadEntityById<Bars.Nedragis.RajjonyJaNAO>(model.region?.Id);
            entity.date_postupl = model.date_postupl.GetValueOrDefault();
            entity.Etap = model.Etap;
            entity.nom_vh_doc = model.nom_vh_doc;
            entity.autor = TryLoadEntityById<Bars.Nedragis.Sotrudniki1>(model.autor?.Id);
            if (model.nomer_pisma.HasValue)
            {
                entity.nomer_pisma = model.nomer_pisma.Value;
            }

            entity.data_usp = model.data_usp.GetValueOrDefault();
            entity.ispol = model.ispol;
            entity.format = model.format;
            if (model.kolvo_ed.HasValue)
            {
                entity.kolvo_ed = model.kolvo_ed.Value;
            }

            entity.prim = model.prim;
            if (model.sum_kalk.HasValue)
            {
                entity.sum_kalk = model.sum_kalk.Value;
            }
        }
    }
}