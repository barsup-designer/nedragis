namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Отдел приема и контроля исполнения заявок'
    /// </summary>
    public interface IIspolnenijaZajavokEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.IspolnenijaZajavok, IspolnenijaZajavokEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Отдел приема и контроля исполнения заявок'
    /// </summary>
    public interface IIspolnenijaZajavokEditorValidator : IEditorModelValidator<IspolnenijaZajavokEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Отдел приема и контроля исполнения заявок'
    /// </summary>
    public interface IIspolnenijaZajavokEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.IspolnenijaZajavok, IspolnenijaZajavokEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Отдел приема и контроля исполнения заявок'
    /// </summary>
    public abstract class AbstractIspolnenijaZajavokEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.IspolnenijaZajavok, IspolnenijaZajavokEditorModel>, IIspolnenijaZajavokEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Отдел приема и контроля исполнения заявок'
    /// </summary>
    public class IspolnenijaZajavokEditorViewModel : BaseEditorViewModel<Bars.Nedragis.IspolnenijaZajavok, IspolnenijaZajavokEditorModel>, IIspolnenijaZajavokEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override IspolnenijaZajavokEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new IspolnenijaZajavokEditorModel();
            var varElement1511506563562Id = @params.Params.GetAs<long>("Element1511506563562_Id", 0);
            if (varElement1511506563562Id > 0)
            {
                model.Element1511506563562 = Container.Resolve<Bars.Nedragis.INaimenovanieMOListQuery>().GetById(varElement1511506563562Id);
            }

            var varElement1511506733063Id = @params.Params.GetAs<long>("Element1511506733063_Id", 0);
            if (varElement1511506733063Id > 0)
            {
                model.Element1511506733063 = Container.Resolve<Bars.Nedragis.IOrganizaciiListQuery>().GetById(varElement1511506733063Id);
            }

            var varElement1511762285082Id = @params.Params.GetAs<long>("Element1511762285082_Id", 0);
            if (varElement1511762285082Id > 0)
            {
                model.Element1511762285082 = Container.Resolve<Bars.Nedragis.INaimenovanieNapravlenijaRaskhodovanijaSredstvListQuery>().GetById(varElement1511762285082Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Приема и контроля исполнения заявок' в модель представления
        /// </summary>
        protected override IspolnenijaZajavokEditorModel MapEntityInternal(Bars.Nedragis.IspolnenijaZajavok entity)
        {
            // создаем экзепляр модели
            var model = new IspolnenijaZajavokEditorModel();
            model.Id = entity.Id;
            model.Element1511781998051 = (System.DateTime? )(entity.Element1511781998051);
            model.Element1511506083922 = (System.Boolean? )(entity.Element1511506083922);
            model.Element1511506316909 = (System.Boolean? )(entity.Element1511506316909);
            if (entity.Element1511506563562.IsNotNull())
            {
                var queryNaimenovanieMOList = Container.Resolve<Bars.Nedragis.INaimenovanieMOListQuery>();
                model.Element1511506563562 = queryNaimenovanieMOList.GetById(entity.Element1511506563562.Id);
            }

            if (entity.Element1511506733063.IsNotNull())
            {
                var queryOrganizaciiList = Container.Resolve<Bars.Nedragis.IOrganizaciiListQuery>();
                model.Element1511506733063 = queryOrganizaciiList.GetById(entity.Element1511506733063.Id);
            }

            if (entity.Element1511762285082.IsNotNull())
            {
                var queryNaimenovanieNapravlenijaRaskhodovanijaSredstvList = Container.Resolve<Bars.Nedragis.INaimenovanieNapravlenijaRaskhodovanijaSredstvListQuery>();
                model.Element1511762285082 = queryNaimenovanieNapravlenijaRaskhodovanijaSredstvList.GetById(entity.Element1511762285082.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Приема и контроля исполнения заявок' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.IspolnenijaZajavok entity, IspolnenijaZajavokEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1511781998051 = model.Element1511781998051.GetValueOrDefault();
            if (model.Element1511506083922.HasValue)
            {
                entity.Element1511506083922 = model.Element1511506083922.Value;
            }

            if (model.Element1511506316909.HasValue)
            {
                entity.Element1511506316909 = model.Element1511506316909.Value;
            }

            entity.Element1511506563562 = TryLoadEntityById<Bars.Nedragis.NaimenovanieMO>(model.Element1511506563562?.Id);
            entity.Element1511506733063 = TryLoadEntityById<Bars.Nedragis.Organizacii>(model.Element1511506733063?.Id);
            entity.Element1511762285082 = TryLoadEntityById<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv>(model.Element1511762285082?.Id);
        }
    }
}