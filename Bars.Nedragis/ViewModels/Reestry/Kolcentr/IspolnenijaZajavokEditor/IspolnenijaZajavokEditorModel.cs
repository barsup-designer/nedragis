namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Отдел приема и контроля исполнения заявок' для отдачи на клиент
    /// </summary>
    public class IspolnenijaZajavokEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public IspolnenijaZajavokEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата заявки'
        /// </summary>
        [Bars.B4.Utils.Display("Дата заявки")]
        [Bars.Rms.Core.Attributes.Uid("0b7ff67f-0886-40ca-adbd-8d312d7cf160")]
        public virtual System.DateTime? Element1511781998051
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Консультация по телефону'
        /// </summary>
        [Bars.B4.Utils.Display("Консультация по телефону")]
        [Bars.Rms.Core.Attributes.Uid("c9fdd6fd-6c40-4c7b-855a-0d1c1a70b9f0")]
        public virtual System.Boolean? Element1511506083922
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Инцидент(заявка)'
        /// </summary>
        [Bars.B4.Utils.Display("Инцидент(заявка)")]
        [Bars.Rms.Core.Attributes.Uid("54134471-bcab-4567-91ff-7ba5c81431b5")]
        public virtual System.Boolean? Element1511506316909
        {
            get;
            set;
        }

        /// <summary>
        /// Муниципальное образование
        /// </summary>
        [Bars.B4.Utils.Display("Муниципальное образование")]
        [Bars.Rms.Core.Attributes.Uid("4b448f0b-7292-451c-bd92-27768d849dce")]
        public virtual Bars.Nedragis.NaimenovanieMOListModel Element1511506563562
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование организации
        /// </summary>
        [Bars.B4.Utils.Display("Наименование организации")]
        [Bars.Rms.Core.Attributes.Uid("e5b84860-b11a-47f4-a3e5-a6254ca604c5")]
        public virtual Bars.Nedragis.OrganizaciiListModel Element1511506733063
        {
            get;
            set;
        }

        /// <summary>
        /// проект
        /// </summary>
        [Bars.B4.Utils.Display("проект")]
        [Bars.Rms.Core.Attributes.Uid("c392cc2e-32ba-4282-b846-b6854f352f26")]
        public virtual Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel Element1511762285082
        {
            get;
            set;
        }
    }
}