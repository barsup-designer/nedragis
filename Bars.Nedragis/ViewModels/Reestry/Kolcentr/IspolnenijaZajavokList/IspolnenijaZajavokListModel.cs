namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Приема и контроля исполнения заявок'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Приема и контроля исполнения заявок")]
    public class IspolnenijaZajavokListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата создания' (псевдоним: Date456789)
        /// </summary>
        [Bars.B4.Utils.Display("Дата создания")]
        [Bars.Rms.Core.Attributes.Uid("f8b7092b-1914-48cc-8c99-88ec160c1a1a")]
        public virtual System.DateTime? Date456789
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата заявки' (псевдоним: reestr_date)
        /// </summary>
        [Bars.B4.Utils.Display("Дата заявки")]
        [Bars.Rms.Core.Attributes.Uid("1f6b11a5-4833-4ad5-b8b7-007038ff916f")]
        public virtual System.DateTime? reestr_date
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Консультация по телефону' (псевдоним: kons456)
        /// </summary>
        [Bars.B4.Utils.Display("Консультация по телефону")]
        [Bars.Rms.Core.Attributes.Uid("c909b69f-24b1-4692-b0f0-c0c9814afac1")]
        public virtual System.Boolean? kons456
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Инцидент(заявка)' (псевдоним: ins456)
        /// </summary>
        [Bars.B4.Utils.Display("Инцидент(заявка)")]
        [Bars.Rms.Core.Attributes.Uid("5b82459a-3784-41a5-a9ee-49a4fbec3b34")]
        public virtual System.Boolean? ins456
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Муниципальное образование.Наименование' (псевдоним: reestrmo456)
        /// </summary>
        [Bars.B4.Utils.Display("Муниципальное образование.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("a3270e8b-a8fd-4fda-b752-ce36de95eec4")]
        public virtual System.String reestrmo456
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование организации.Наименование' (псевдоним: reestrname456)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование организации.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("16884788-f00b-486c-acfe-1af4d0eaae67")]
        public virtual System.String reestrname456
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'проект.Наименование' (псевдоним: reestrpr)
        /// </summary>
        [Bars.B4.Utils.Display("проект.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("41f14168-8779-4bb8-9c14-4261f672777b")]
        public virtual System.String reestrpr
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}