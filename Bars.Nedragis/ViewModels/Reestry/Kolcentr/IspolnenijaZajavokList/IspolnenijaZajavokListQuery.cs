namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Приема и контроля исполнения заявок'
    /// </summary>
    public interface IIspolnenijaZajavokListQuery : IQueryOperation<Bars.Nedragis.IspolnenijaZajavok, Bars.Nedragis.IspolnenijaZajavokListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Приема и контроля исполнения заявок'
    /// </summary>
    public interface IIspolnenijaZajavokListQueryFilter : IQueryOperationFilter<Bars.Nedragis.IspolnenijaZajavok, Bars.Nedragis.IspolnenijaZajavokListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Приема и контроля исполнения заявок'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Приема и контроля исполнения заявок")]
    public class IspolnenijaZajavokListQuery : RmsEntityQueryOperation<Bars.Nedragis.IspolnenijaZajavok, Bars.Nedragis.IspolnenijaZajavokListModel>, IIspolnenijaZajavokListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "IspolnenijaZajavokListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public IspolnenijaZajavokListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.IspolnenijaZajavok> Filter(IQueryable<Bars.Nedragis.IspolnenijaZajavok> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.IspolnenijaZajavokListModel> Map(IQueryable<Bars.Nedragis.IspolnenijaZajavok> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.IspolnenijaZajavok>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.IspolnenijaZajavokListModel{Id = x.Id, _TypeUid = "570c3381-31ef-424d-b058-c74c0a28ed56", Date456789 = (System.DateTime? )(x.ObjectCreateDate), reestr_date = (System.DateTime? )(x.Element1511781998051), kons456 = (System.Boolean? )(x.Element1511506083922), ins456 = (System.Boolean? )(x.Element1511506316909), reestrmo456 = (System.String)(x.Element1511506563562.Element1455707802552), reestrname456 = (System.String)(x.Element1511506733063.Element1511331964541), reestrpr = (System.String)(x.Element1511762285082.Element1455708457805), });
        }
    }
}