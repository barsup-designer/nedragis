namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр документов (ДокументыЗУ)'
    /// </summary>
    public interface IDokumentyZUList2Query : IQueryOperation<Bars.Nedragis.DokumentyZU, Bars.Nedragis.DokumentyZUList2Model, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр документов (ДокументыЗУ)'
    /// </summary>
    public interface IDokumentyZUList2QueryFilter : IQueryOperationFilter<Bars.Nedragis.DokumentyZU, Bars.Nedragis.DokumentyZUList2Model, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр документов (ДокументыЗУ)'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр документов (ДокументыЗУ)")]
    public class DokumentyZUList2Query : RmsEntityQueryOperation<Bars.Nedragis.DokumentyZU, Bars.Nedragis.DokumentyZUList2Model>, IDokumentyZUList2Query
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "DokumentyZUList2Query"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public DokumentyZUList2Query(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.DokumentyZU> Filter(IQueryable<Bars.Nedragis.DokumentyZU> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.DokumentyZUList2Model> Map(IQueryable<Bars.Nedragis.DokumentyZU> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.DokumentyZU>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.DokumentyZUList2Model{Id = x.Id, _TypeUid = "4996ce7a-3313-46fc-979a-fe45071d30e9", doc1_Type_name = (System.String)(x.doc1.Type.name), doc1_Number = (System.String)(x.doc1.Number), doc1_Date = (System.DateTime? )(x.doc1.Date), doc1_Subject_Representation = (System.String)(x.doc1.Subject.Representation), zemuch_Id = (System.Int64? )(x.zemuch.Id), doc1_Signatory_Representation = (System.String)(x.doc1.Signatory.Representation), doc1_ActualDate = (System.DateTime? )(x.doc1.ActualDate), });
        }
    }
}