namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр документов (ДокументыЗУ)'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр документов (ДокументыЗУ)")]
    public class DokumentyZUList2Model
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Документ.Тип документа.Наименование' (псевдоним: doc1_Type_name)
        /// </summary>
        [Bars.B4.Utils.Display("Документ.Тип документа.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("a8a5d184-12f9-4920-8602-af15f41db161")]
        public virtual System.String doc1_Type_name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Документ.Номер документа' (псевдоним: doc1_Number)
        /// </summary>
        [Bars.B4.Utils.Display("Документ.Номер документа")]
        [Bars.Rms.Core.Attributes.Uid("d0a544e2-8919-4d70-93e2-40db45ae5abd")]
        public virtual System.String doc1_Number
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Документ.Дата документа' (псевдоним: doc1_Date)
        /// </summary>
        [Bars.B4.Utils.Display("Документ.Дата документа")]
        [Bars.Rms.Core.Attributes.Uid("8d6d40b1-54fe-457a-a0e3-ffc5665d9cab")]
        public virtual System.DateTime? doc1_Date
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Документ.Субъект.Название субъекта' (псевдоним: doc1_Subject_Representation)
        /// </summary>
        [Bars.B4.Utils.Display("Документ.Субъект.Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("883ccc5e-e831-4c64-9399-3b5e235d8b95")]
        public virtual System.String doc1_Subject_Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Земельный участок.Идентификатор' (псевдоним: zemuch_Id)
        /// </summary>
        [Bars.B4.Utils.Display("Земельный участок.Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("a975f0d7-9c67-46a4-a964-d3c2ae0ebeed")]
        public virtual System.Int64? zemuch_Id
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Документ.Орган подписавший документ.Название субъекта' (псевдоним: doc1_Signatory_Representation)
        /// </summary>
        [Bars.B4.Utils.Display("Документ.Орган подписавший документ.Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("5d1a39d9-331b-464d-a347-3823d1e09bed")]
        public virtual System.String doc1_Signatory_Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Документ.Дата вступления в силу' (псевдоним: doc1_ActualDate)
        /// </summary>
        [Bars.B4.Utils.Display("Документ.Дата вступления в силу")]
        [Bars.Rms.Core.Attributes.Uid("b5ad08cf-9fe7-4201-a3f2-3a8e2925228a")]
        public virtual System.DateTime? doc1_ActualDate
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}