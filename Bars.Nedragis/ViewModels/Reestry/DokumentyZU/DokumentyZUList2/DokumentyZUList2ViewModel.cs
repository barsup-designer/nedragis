namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Commons;
    using Bars.Rms.GeneratedApp.Lists;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Linq;

    /// <summary>
    /// Контракт модели представления 'Реестр документов (ДокументыЗУ)'
    /// </summary>
    public interface IDokumentyZUList2ViewModel : IViewModel<Bars.Nedragis.DokumentyZU, DokumentyZUList2Model>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Реестр документов (ДокументыЗУ)'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр документов (ДокументыЗУ)")]
    public class DokumentyZUList2ViewModel : BaseListViewModel<Bars.Nedragis.DokumentyZU, DokumentyZUList2Model, IDokumentyZUList2Query>, IDokumentyZUList2ViewModel
    {
        /// <summary>
        /// Конструктор модели
        /// </summary>
        /// <param name = "query">Запрос данных</param>    
        public DokumentyZUList2ViewModel(IDataStore datastore, IDokumentyZUList2Query query): base (datastore, query)
        {
        }

        /// <summary>
        /// Получение и упаковка в модель списка сущностей
        /// </summary>
        protected override IDataResult List(IQueryable<DokumentyZUList2Model> query, LoadParam loadParams, BaseParams baseParams)
        {
            IList<DokumentyZUList2Model> map;
            var sorters = (loadParams.Order ?? Enumerable.Empty<OrderField>()).ToArray();
            var typeOrderFields = sorters.Where(x => x.Name.IsOneOf("_TypeDisplay", "_TypeUid")).ToArray();
            if (typeOrderFields.Any())
                loadParams.Order = sorters.Except(typeOrderFields).ToArray();
            int totalCount = 0;
            var summaryData = DynamicDictionary.Create();
            if (typeof (NhQueryable<>).IsAssignableFrom(query.GetType().GetGenericTypeDefinition()))
            {
                var totalCountFuture = query.ToFutureValue(x => x.Count());
                map = query.Order(loadParams).Paging(loadParams).ToFuture().ToList();
                totalCount = totalCountFuture.Value;
            }
            else
            {
                totalCount = query.Count();
                map = query.Order(loadParams).Paging(loadParams).ToList();
            }

            if (typeOrderFields.Any())
                map = map.OrderBy(x => x._TypeUid).ToList();
            // устанавливаем контроллер редактора для каждой из сущностей
            foreach (var itm in map)
            {
                switch (itm._TypeUid)
                {
                    case "4996ce7a-3313-46fc-979a-fe45071d30e9":
                        itm._EditorName = "DokumentyZUEditor";
                        break;
                }
            }

            // формирование результата
            return new ListDataResultWithSummary(map, totalCount, summaryData);
        }
    }
}