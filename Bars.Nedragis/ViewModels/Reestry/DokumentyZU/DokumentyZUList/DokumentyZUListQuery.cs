namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр ЗУ (ДокументыЗУ)'
    /// </summary>
    public interface IDokumentyZUListQuery : IQueryOperation<Bars.Nedragis.DokumentyZU, Bars.Nedragis.DokumentyZUListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр ЗУ (ДокументыЗУ)'
    /// </summary>
    public interface IDokumentyZUListQueryFilter : IQueryOperationFilter<Bars.Nedragis.DokumentyZU, Bars.Nedragis.DokumentyZUListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр ЗУ (ДокументыЗУ)'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр ЗУ (ДокументыЗУ)")]
    public class DokumentyZUListQuery : RmsEntityQueryOperation<Bars.Nedragis.DokumentyZU, Bars.Nedragis.DokumentyZUListModel>, IDokumentyZUListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "DokumentyZUListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public DokumentyZUListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.DokumentyZU> Filter(IQueryable<Bars.Nedragis.DokumentyZU> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.DokumentyZUListModel> Map(IQueryable<Bars.Nedragis.DokumentyZU> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.DokumentyZU>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.DokumentyZUListModel{Id = x.Id, _TypeUid = "4996ce7a-3313-46fc-979a-fe45071d30e9", zemuch_cadnum1 = (System.String)(x.zemuch.cadnum1), zemuch_name = (System.String)(x.zemuch.name), zemuch_owner_id_Representation = (System.String)(x.zemuch.owner_id.Representation), doc1_Id = (System.Int64? )(x.doc1.Id), zemuch_vid_prava_name1 = (System.String)(x.zemuch.vid_prava.name1), zemuch_category_name1 = (System.String)(x.zemuch.category.name1), zemuch_vid_use_name1 = (System.String)(x.zemuch.vid_use.name1), });
        }
    }
}