namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр ЗУ (ДокументыЗУ)'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр ЗУ (ДокументыЗУ)")]
    public class DokumentyZUListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Земельный участок.Кадастровый номер' (псевдоним: zemuch_cadnum1)
        /// </summary>
        [Bars.B4.Utils.Display("Земельный участок.Кадастровый номер")]
        [Bars.Rms.Core.Attributes.Uid("561443fe-c5c5-4cf4-8d63-f9389223052c")]
        public virtual System.String zemuch_cadnum1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Земельный участок.Наименование' (псевдоним: zemuch_name)
        /// </summary>
        [Bars.B4.Utils.Display("Земельный участок.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("4d29f3ec-c6cb-44eb-8c8e-3a1b00439e3e")]
        public virtual System.String zemuch_name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Земельный участок.Собственник.Название субъекта' (псевдоним: zemuch_owner_id_Representation)
        /// </summary>
        [Bars.B4.Utils.Display("Земельный участок.Собственник.Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("c208d106-12c4-418e-b88a-6efbc4b6d4ab")]
        public virtual System.String zemuch_owner_id_Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Документ.Идентификатор' (псевдоним: doc1_Id)
        /// </summary>
        [Bars.B4.Utils.Display("Документ.Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("d96883f9-211b-48ee-8749-2d052764e550")]
        public virtual System.Int64? doc1_Id
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Земельный участок.Вид права на землю.Наименование' (псевдоним: zemuch_vid_prava_name1)
        /// </summary>
        [Bars.B4.Utils.Display("Земельный участок.Вид права на землю.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("9ce92f87-cd8e-4858-9cd1-aa44f736d3ff")]
        public virtual System.String zemuch_vid_prava_name1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Земельный участок.Категория земли.Наименование' (псевдоним: zemuch_category_name1)
        /// </summary>
        [Bars.B4.Utils.Display("Земельный участок.Категория земли.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("ef1b20d1-07bf-409e-bc34-70170e8fed86")]
        public virtual System.String zemuch_category_name1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Земельный участок.Вид разрешенного использования.Наименование' (псевдоним: zemuch_vid_use_name1)
        /// </summary>
        [Bars.B4.Utils.Display("Земельный участок.Вид разрешенного использования.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("5cb0686c-d64a-4ae6-88df-f91df26f0696")]
        public virtual System.String zemuch_vid_use_name1
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}