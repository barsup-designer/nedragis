namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования ДокументыЗУ3' для отдачи на клиент
    /// </summary>
    public class DokumentyZUEditor3Model : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public DokumentyZUEditor3Model()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Земельный участок
        /// </summary>
        [Bars.B4.Utils.Display("Земельный участок")]
        [Bars.Rms.Core.Attributes.Uid("69ff3ed9-3f6e-45ee-a5ac-bcc5f016c041")]
        public virtual Bars.Nedragis.ZemelNyemUchastkiListModel zemuch
        {
            get;
            set;
        }

        /// <summary>
        /// Документ
        /// </summary>
        [Bars.B4.Utils.Display("Документ")]
        [Bars.Rms.Core.Attributes.Uid("0175eda3-2737-4fe9-a482-4229737acd93")]
        public virtual Bars.Nedragis.DokumentListModel doc1
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'DokumentEditor'
        /// </summary>
        [Bars.B4.Utils.Display("DokumentEditor")]
        [Bars.Rms.Core.Attributes.Uid("DokumentEditor")]
        public virtual Bars.Nedragis.DokumentEditorModel DokumentEditor
        {
            get;
            set;
        }
    }
}