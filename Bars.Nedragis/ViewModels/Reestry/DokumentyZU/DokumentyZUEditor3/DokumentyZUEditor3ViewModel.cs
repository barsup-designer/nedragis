namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования ДокументыЗУ3'
    /// </summary>
    public interface IDokumentyZUEditor3ViewModel : IEntityEditorViewModel<Bars.Nedragis.DokumentyZU, DokumentyZUEditor3Model>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования ДокументыЗУ3'
    /// </summary>
    public interface IDokumentyZUEditor3Validator : IEditorModelValidator<DokumentyZUEditor3Model>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования ДокументыЗУ3'
    /// </summary>
    public interface IDokumentyZUEditor3Handler : IEntityEditorViewModelHandler<Bars.Nedragis.DokumentyZU, DokumentyZUEditor3Model>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования ДокументыЗУ3'
    /// </summary>
    public abstract class AbstractDokumentyZUEditor3Handler : EntityEditorViewModelHandler<Bars.Nedragis.DokumentyZU, DokumentyZUEditor3Model>, IDokumentyZUEditor3Handler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования ДокументыЗУ3'
    /// </summary>
    public class DokumentyZUEditor3ViewModel : BaseEditorViewModel<Bars.Nedragis.DokumentyZU, DokumentyZUEditor3Model>, IDokumentyZUEditor3ViewModel
    {
        /// <summary>
        /// Модель редактора 'Форма редактирования Документ' (DokumentEditor) 
        /// </summary>
        public virtual Bars.Nedragis.IDokumentEditorViewModel DokumentEditorModel
        {
            get;
            set;
        }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override DokumentyZUEditor3Model CreateModelInternal(BaseParams @params)
        {
            var model = new DokumentyZUEditor3Model();
            var varzemuchId = @params.Params.GetAs<long>("zemuch_Id", 0);
            if (varzemuchId > 0)
            {
                model.zemuch = Container.Resolve<Bars.Nedragis.IZemelNyemUchastkiListQuery>().GetById(varzemuchId);
            }

            var vardoc1Id = @params.Params.GetAs<long>("doc1_Id", 0);
            if (vardoc1Id > 0)
            {
                model.doc1 = Container.Resolve<Bars.Nedragis.IDokumentListQuery>().GetById(vardoc1Id);
            }

            model.DokumentEditor = DokumentEditorModel.GetModel(new BaseParams());
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'ДокументыЗУ' в модель представления
        /// </summary>
        protected override DokumentyZUEditor3Model MapEntityInternal(Bars.Nedragis.DokumentyZU entity)
        {
            // создаем экзепляр модели
            var model = new DokumentyZUEditor3Model();
            model.Id = entity.Id;
            if (entity.zemuch.IsNotNull())
            {
                var queryZemelNyemUchastkiList = Container.Resolve<Bars.Nedragis.IZemelNyemUchastkiListQuery>();
                model.zemuch = queryZemelNyemUchastkiList.GetById(entity.zemuch.Id);
            }

            if (entity.doc1.IsNotNull())
            {
                var queryDokumentList = Container.Resolve<Bars.Nedragis.IDokumentListQuery>();
                model.doc1 = queryDokumentList.GetById(entity.doc1.Id);
            }

            var DokumentEditorQuery = Container.ResolveDomain<Bars.Nedragis.Dokument>().GetAll();
            const string DokumentEditorFilterString = "{'Group':3,'Operand':0,'DataIndex':null,'DataIndexType':null,'Value':null,'Filters':[{'Group':0,'Operand':0,'DataIndex':'FieldPath://bb2ade91-5fc2-4846-8ee8-8b717abf46fb$Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id','DataIndexType':null,'Value':'@DokumentyZU_doc1_Id','Filters':null}]}";
            var DokumentEditorDataFilter = JsonNetConvert.DeserializeObject<DataFilter>(DokumentEditorFilterString);
            var DokumentEditorCtx = DynamicDictionary.Create();
            DokumentEditorCtx["@DokumentyZU_doc1_Id"] = entity.Return(x => x.doc1).Return(x => x.Id);
            DokumentEditorDataFilter.PrepareDataFilter(DokumentEditorCtx);
            DokumentEditorQuery = DokumentEditorQuery.ApplyDataFilter(DokumentEditorDataFilter);
            Bars.Nedragis.Dokument DokumentEditorResult = null;
            DokumentEditorResult = DokumentEditorQuery.FirstOrDefault();
            model.DokumentEditor = DokumentEditorResult == null ? DokumentEditorModel.CreateModel(new BaseParams()) : DokumentEditorModel.MapEntity(DokumentEditorResult);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'ДокументыЗУ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.DokumentyZU entity, DokumentyZUEditor3Model model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.zemuch = TryLoadEntityById<Bars.Nedragis.ZemelNyemUchastki>(model.zemuch?.Id);
            entity.doc1 = TryLoadEntityById<Bars.Nedragis.Dokument>(model.doc1?.Id);
            if (model.DokumentEditor != null)
            {
                var eDokumentEditor = DokumentEditorModel.UnmapEntity(model.DokumentEditor, requestFiles, filesToDelete, true);
                DokumentEditorModel.SaveOrUpdate(eDokumentEditor);
            }
        }
    }
}