namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма ввода ДокументыЗУ' для отдачи на клиент
    /// </summary>
    public class DokumentyZUEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public DokumentyZUEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Земельный участок
        /// </summary>
        [Bars.B4.Utils.Display("Земельный участок")]
        [Bars.Rms.Core.Attributes.Uid("ec7a225a-e3b8-43fd-9b38-2f718e01dd48")]
        public virtual Bars.Nedragis.ZemelNyemUchastkiListModel zemuch
        {
            get;
            set;
        }

        /// <summary>
        /// Документ
        /// </summary>
        [Bars.B4.Utils.Display("Документ")]
        [Bars.Rms.Core.Attributes.Uid("6ead281c-dfcd-46ff-8e78-59be23cbafc8")]
        public virtual Bars.Nedragis.DokumentListModel doc1
        {
            get;
            set;
        }
    }
}