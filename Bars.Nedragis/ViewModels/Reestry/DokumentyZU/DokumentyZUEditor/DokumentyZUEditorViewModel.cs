namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма ввода ДокументыЗУ'
    /// </summary>
    public interface IDokumentyZUEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.DokumentyZU, DokumentyZUEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма ввода ДокументыЗУ'
    /// </summary>
    public interface IDokumentyZUEditorValidator : IEditorModelValidator<DokumentyZUEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма ввода ДокументыЗУ'
    /// </summary>
    public interface IDokumentyZUEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.DokumentyZU, DokumentyZUEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма ввода ДокументыЗУ'
    /// </summary>
    public abstract class AbstractDokumentyZUEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.DokumentyZU, DokumentyZUEditorModel>, IDokumentyZUEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма ввода ДокументыЗУ'
    /// </summary>
    public class DokumentyZUEditorViewModel : BaseEditorViewModel<Bars.Nedragis.DokumentyZU, DokumentyZUEditorModel>, IDokumentyZUEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override DokumentyZUEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new DokumentyZUEditorModel();
            var varzemuchId = @params.Params.GetAs<long>("zemuch_Id", 0);
            if (varzemuchId > 0)
            {
                model.zemuch = Container.Resolve<Bars.Nedragis.IZemelNyemUchastkiListQuery>().GetById(varzemuchId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'ДокументыЗУ' в модель представления
        /// </summary>
        protected override DokumentyZUEditorModel MapEntityInternal(Bars.Nedragis.DokumentyZU entity)
        {
            // создаем экзепляр модели
            var model = new DokumentyZUEditorModel();
            model.Id = entity.Id;
            if (entity.zemuch.IsNotNull())
            {
                var queryZemelNyemUchastkiList = Container.Resolve<Bars.Nedragis.IZemelNyemUchastkiListQuery>();
                model.zemuch = queryZemelNyemUchastkiList.GetById(entity.zemuch.Id);
            }

            if (entity.doc1.IsNotNull())
            {
                var queryDokumentList = Container.Resolve<Bars.Nedragis.IDokumentListQuery>();
                model.doc1 = queryDokumentList.GetById(entity.doc1.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'ДокументыЗУ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.DokumentyZU entity, DokumentyZUEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.zemuch = TryLoadEntityById<Bars.Nedragis.ZemelNyemUchastki>(model.zemuch?.Id);
            entity.doc1 = TryLoadEntityById<Bars.Nedragis.Dokument>(model.doc1?.Id);
        }
    }
}