namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Земельные участки'
    /// </summary>
    public interface IZemelNyemUchastkiListQuery : IQueryOperation<Bars.Nedragis.ZemelNyemUchastki, Bars.Nedragis.ZemelNyemUchastkiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Земельные участки'
    /// </summary>
    public interface IZemelNyemUchastkiListQueryFilter : IQueryOperationFilter<Bars.Nedragis.ZemelNyemUchastki, Bars.Nedragis.ZemelNyemUchastkiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Земельные участки'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Земельные участки")]
    public class ZemelNyemUchastkiListQuery : RmsEntityQueryOperation<Bars.Nedragis.ZemelNyemUchastki, Bars.Nedragis.ZemelNyemUchastkiListModel>, IZemelNyemUchastkiListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "ZemelNyemUchastkiListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public ZemelNyemUchastkiListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.ZemelNyemUchastki> Filter(IQueryable<Bars.Nedragis.ZemelNyemUchastki> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.ZemelNyemUchastkiListModel> Map(IQueryable<Bars.Nedragis.ZemelNyemUchastki> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.ZemelNyemUchastki>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.ZemelNyemUchastkiListModel{Id = x.Id, _TypeUid = "6885fdfb-7ec7-4566-8395-5542b58c6305", cadnum1 = (System.String)(x.cadnum1), name = (System.String)(x.name), area = (System.Decimal? )(x.area), naznach = (System.String)(x.naznach), category_name1 = (System.String)(x.category.name1), vid_use_name1 = (System.String)(x.vid_use.name1), vid_prava_name1 = (System.String)(x.vid_prava.name1), holder_id_Representation = (System.String)(x.holder_id.Representation), owner_id_Representation = (System.String)(x.owner_id.Representation), property_manager_id_Representation = (System.String)(x.property_manager_id.Representation), MapID = (System.Int64? )(x.MapID), typeobj_name = (System.String)(x.typeobj.name), });
        }
    }
}