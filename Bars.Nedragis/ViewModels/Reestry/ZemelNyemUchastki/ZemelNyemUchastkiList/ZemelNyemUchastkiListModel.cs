namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Земельные участки'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Земельные участки")]
    public class ZemelNyemUchastkiListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Кадастровый номер' (псевдоним: cadnum1)
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый номер")]
        [Bars.Rms.Core.Attributes.Uid("0692e90c-28b5-4c95-a886-0b6b8815a2bd")]
        public virtual System.String cadnum1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: name)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("a8c7e280-caea-4a5e-a982-472457f04984")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Общая площадь, кв. м' (псевдоним: area)
        /// </summary>
        [Bars.B4.Utils.Display("Общая площадь, кв. м")]
        [Bars.Rms.Core.Attributes.Uid("f26b6e3c-10f2-4519-88e8-7776a0a02b64")]
        public virtual System.Decimal? area
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Назначение' (псевдоним: naznach)
        /// </summary>
        [Bars.B4.Utils.Display("Назначение")]
        [Bars.Rms.Core.Attributes.Uid("813b1596-5ad6-4988-abda-f7667183bbbe")]
        public virtual System.String naznach
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Категория земли.Наименование' (псевдоним: category_name1)
        /// </summary>
        [Bars.B4.Utils.Display("Категория земли.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("cea25bb5-e119-461a-8de0-2dfbf4f2a7b0")]
        public virtual System.String category_name1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Вид разрешенного использования.Наименование' (псевдоним: vid_use_name1)
        /// </summary>
        [Bars.B4.Utils.Display("Вид разрешенного использования.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("2620d922-8c45-439a-97eb-60553fc7d187")]
        public virtual System.String vid_use_name1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Вид права на землю.Наименование' (псевдоним: vid_prava_name1)
        /// </summary>
        [Bars.B4.Utils.Display("Вид права на землю.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("d10fae9a-0ba0-4e37-a83f-6ce6a7810251")]
        public virtual System.String vid_prava_name1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Правообладатель.Название субъекта' (псевдоним: holder_id_Representation)
        /// </summary>
        [Bars.B4.Utils.Display("Правообладатель.Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("382b156a-e663-4e7d-9af8-74439c78ca97")]
        public virtual System.String holder_id_Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Собственник.Название субъекта' (псевдоним: owner_id_Representation)
        /// </summary>
        [Bars.B4.Utils.Display("Собственник.Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("a15d93fb-a101-4103-8222-fb26adc477ae")]
        public virtual System.String owner_id_Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Уполномоченный орган.Название субъекта' (псевдоним: property_manager_id_Representation)
        /// </summary>
        [Bars.B4.Utils.Display("Уполномоченный орган.Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("f96e4f3a-a1ce-4110-ad72-37ba2fd34e8f")]
        public virtual System.String property_manager_id_Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Идентификатор ГИС' (псевдоним: MapID)
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор ГИС")]
        [Bars.Rms.Core.Attributes.Uid("2f6ec407-16f0-42ce-93f0-7df04a0293d9")]
        public virtual System.Int64? MapID
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Тип объекта права.Наименование' (псевдоним: typeobj_name)
        /// </summary>
        [Bars.B4.Utils.Display("Тип объекта права.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("dea362d9-641d-41b6-80a8-2fc44c8f91e3")]
        public virtual System.String typeobj_name
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}