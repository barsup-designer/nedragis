namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'ЗУ Карта'
    /// </summary>
    public interface IZUKartaViewModel : IEntityEditorViewModel<Bars.Nedragis.ZemelNyemUchastki, ZUKartaModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'ЗУ Карта'
    /// </summary>
    public interface IZUKartaValidator : IEditorModelValidator<ZUKartaModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'ЗУ Карта'
    /// </summary>
    public interface IZUKartaHandler : IEntityEditorViewModelHandler<Bars.Nedragis.ZemelNyemUchastki, ZUKartaModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'ЗУ Карта'
    /// </summary>
    public abstract class AbstractZUKartaHandler : EntityEditorViewModelHandler<Bars.Nedragis.ZemelNyemUchastki, ZUKartaModel>, IZUKartaHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'ЗУ Карта'
    /// </summary>
    public class ZUKartaViewModel : BaseEditorViewModel<Bars.Nedragis.ZemelNyemUchastki, ZUKartaModel>, IZUKartaViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ZUKartaModel CreateModelInternal(BaseParams @params)
        {
            var model = new ZUKartaModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Земельные участки' в модель представления
        /// </summary>
        protected override ZUKartaModel MapEntityInternal(Bars.Nedragis.ZemelNyemUchastki entity)
        {
            // создаем экзепляр модели
            var model = new ZUKartaModel();
            model.Id = entity.Id;
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Земельные участки' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.ZemelNyemUchastki entity, ZUKartaModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.Id.HasValue)
            {
                entity.Id = model.Id.Value;
            }
        }
    }
}