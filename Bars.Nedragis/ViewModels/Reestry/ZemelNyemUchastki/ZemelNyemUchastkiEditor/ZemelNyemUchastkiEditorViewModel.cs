namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Земельные участки'
    /// </summary>
    public interface IZemelNyemUchastkiEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Земельные участки'
    /// </summary>
    public interface IZemelNyemUchastkiEditorValidator : IEditorModelValidator<ZemelNyemUchastkiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Земельные участки'
    /// </summary>
    public interface IZemelNyemUchastkiEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Земельные участки'
    /// </summary>
    public abstract class AbstractZemelNyemUchastkiEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditorModel>, IZemelNyemUchastkiEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Земельные участки'
    /// </summary>
    public class ZemelNyemUchastkiEditorViewModel : BaseEditorViewModel<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditorModel>, IZemelNyemUchastkiEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ZemelNyemUchastkiEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new ZemelNyemUchastkiEditorModel();
            var vartypeId = @params.Params.GetAs<long>("type_Id", 0);
            if (vartypeId > 0)
            {
                model.type = Container.Resolve<Bars.Nedragis.IReestryListQuery>().GetById(vartypeId);
            }

            var varvid_pravaId = @params.Params.GetAs<long>("vid_prava_Id", 0);
            if (varvid_pravaId > 0)
            {
                model.vid_prava = Container.Resolve<Bars.Nedragis.IVidPravaNaZemljuListQuery>().GetById(varvid_pravaId);
            }

            var varcategoryId = @params.Params.GetAs<long>("category_Id", 0);
            if (varcategoryId > 0)
            {
                model.category = Container.Resolve<Bars.Nedragis.IKategoriiZemelListQuery>().GetById(varcategoryId);
            }

            var varvid_useId = @params.Params.GetAs<long>("vid_use_Id", 0);
            if (varvid_useId > 0)
            {
                model.vid_use = Container.Resolve<Bars.Nedragis.IVRIZUListQuery>().GetById(varvid_useId);
            }

            var varowner_idId = @params.Params.GetAs<long>("owner_id_Id", 0);
            if (varowner_idId > 0)
            {
                model.owner_id = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>().GetById(varowner_idId);
            }

            var varholder_idId = @params.Params.GetAs<long>("holder_id_Id", 0);
            if (varholder_idId > 0)
            {
                model.holder_id = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>().GetById(varholder_idId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Земельные участки' в модель представления
        /// </summary>
        protected override ZemelNyemUchastkiEditorModel MapEntityInternal(Bars.Nedragis.ZemelNyemUchastki entity)
        {
            // создаем экзепляр модели
            var model = new ZemelNyemUchastkiEditorModel();
            model.Id = entity.Id;
            model.name = (System.String)(entity.name);
            if (entity.type.IsNotNull())
            {
                var queryReestryList = Container.Resolve<Bars.Nedragis.IReestryListQuery>();
                model.type = queryReestryList.GetById(entity.type.Id);
            }

            if (entity.vid_prava.IsNotNull())
            {
                var queryVidPravaNaZemljuList = Container.Resolve<Bars.Nedragis.IVidPravaNaZemljuListQuery>();
                model.vid_prava = queryVidPravaNaZemljuList.GetById(entity.vid_prava.Id);
            }

            if (entity.category.IsNotNull())
            {
                var queryKategoriiZemelList = Container.Resolve<Bars.Nedragis.IKategoriiZemelListQuery>();
                model.category = queryKategoriiZemelList.GetById(entity.category.Id);
            }

            if (entity.vid_use.IsNotNull())
            {
                var queryVRIZUList = Container.Resolve<Bars.Nedragis.IVRIZUListQuery>();
                model.vid_use = queryVRIZUList.GetById(entity.vid_use.Id);
            }

            if (entity.owner_id.IsNotNull())
            {
                var queryJuridicheskieLicaList = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>();
                model.owner_id = queryJuridicheskieLicaList.GetById(entity.owner_id.Id);
            }

            if (entity.holder_id.IsNotNull())
            {
                var queryJuridicheskieLicaList = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>();
                model.holder_id = queryJuridicheskieLicaList.GetById(entity.holder_id.Id);
            }

            model.area = (System.Decimal? )(entity.area);
            model.naznach = (System.String)(entity.naznach);
            model.MapID = (System.Int64? )(entity.MapID);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Земельные участки' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.ZemelNyemUchastki entity, ZemelNyemUchastkiEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name = model.name;
            entity.type = TryLoadEntityById<Bars.Nedragis.Reestry>(model.type?.Id);
            entity.vid_prava = TryLoadEntityById<Bars.Nedragis.VidPravaNaZemlju>(model.vid_prava?.Id);
            entity.category = TryLoadEntityById<Bars.Nedragis.KategoriiZemel>(model.category?.Id);
            entity.vid_use = TryLoadEntityById<Bars.Nedragis.VRIZU>(model.vid_use?.Id);
            entity.owner_id = TryLoadEntityById<Bars.Nedragis.SubEktyPrava>(model.owner_id?.Id);
            entity.holder_id = TryLoadEntityById<Bars.Nedragis.SubEktyPrava>(model.holder_id?.Id);
            if (model.area.HasValue)
            {
                entity.area = model.area.Value;
            }

            entity.naznach = model.naznach;
            if (model.MapID.HasValue)
            {
                entity.MapID = model.MapID.Value;
            }
        }
    }
}