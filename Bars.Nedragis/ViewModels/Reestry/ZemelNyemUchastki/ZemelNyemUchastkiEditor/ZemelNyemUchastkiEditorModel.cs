namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Земельные участки' для отдачи на клиент
    /// </summary>
    public class ZemelNyemUchastkiEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public ZemelNyemUchastkiEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("01cca3c6-dcc2-466d-99d6-421c77f816a9")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Тип реестра
        /// </summary>
        [Bars.B4.Utils.Display("Тип реестра")]
        [Bars.Rms.Core.Attributes.Uid("03039803-cb89-4535-83db-49527b8a175a")]
        public virtual Bars.Nedragis.ReestryListModel type
        {
            get;
            set;
        }

        /// <summary>
        /// Вид права на землю
        /// </summary>
        [Bars.B4.Utils.Display("Вид права на землю")]
        [Bars.Rms.Core.Attributes.Uid("1ba4ca11-8c8b-4bcd-856b-f82f28cb5067")]
        public virtual Bars.Nedragis.VidPravaNaZemljuListModel vid_prava
        {
            get;
            set;
        }

        /// <summary>
        /// Категория земли
        /// </summary>
        [Bars.B4.Utils.Display("Категория земли")]
        [Bars.Rms.Core.Attributes.Uid("46a2b822-c72a-4273-adde-17c98f842374")]
        public virtual Bars.Nedragis.KategoriiZemelListModel category
        {
            get;
            set;
        }

        /// <summary>
        /// Вид разрешенного использования
        /// </summary>
        [Bars.B4.Utils.Display("Вид разрешенного использования")]
        [Bars.Rms.Core.Attributes.Uid("5c1e37b5-7e89-46e3-9be7-c7bf95477646")]
        public virtual Bars.Nedragis.VRIZUListModel vid_use
        {
            get;
            set;
        }

        /// <summary>
        /// Собственник
        /// </summary>
        [Bars.B4.Utils.Display("Собственник")]
        [Bars.Rms.Core.Attributes.Uid("e2ac23e4-fcc5-4cc3-bcf5-79c3aa11302c")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel owner_id
        {
            get;
            set;
        }

        /// <summary>
        /// Правообладатель
        /// </summary>
        [Bars.B4.Utils.Display("Правообладатель")]
        [Bars.Rms.Core.Attributes.Uid("6eb1e1bb-5806-4a8b-a658-e91b45f5031d")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel holder_id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Общая площадь, кв. м'
        /// </summary>
        [Bars.B4.Utils.Display("Общая площадь, кв. м")]
        [Bars.Rms.Core.Attributes.Uid("61c717a3-6a98-47db-8d67-189f094470ea")]
        public virtual System.Decimal? area
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Назначение'
        /// </summary>
        [Bars.B4.Utils.Display("Назначение")]
        [Bars.Rms.Core.Attributes.Uid("3ef4790b-f6ac-4b7f-8591-7618882626da")]
        public virtual System.String naznach
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Идентификатор ГИС'
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор ГИС")]
        [Bars.Rms.Core.Attributes.Uid("79eff5c5-6995-409a-b9bf-f4a2f879bb9a")]
        public virtual System.Int64? MapID
        {
            get;
            set;
        }
    }
}