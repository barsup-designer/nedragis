namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'ЗУ Основные сведения' для отдачи на клиент
    /// </summary>
    public class ZemelNyemUchastkiEditor_Copy_1Model : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public ZemelNyemUchastkiEditor_Copy_1Model()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Тип объекта права
        /// </summary>
        [Bars.B4.Utils.Display("Тип объекта права")]
        [Bars.Rms.Core.Attributes.Uid("f45dd4d8-3e0a-464b-910e-6ecf8f7b3a66")]
        public virtual Bars.Nedragis.TipyObEktaPravaListModel typeobj
        {
            get;
            set;
        }

        /// <summary>
        /// Тип земельного участка
        /// </summary>
        [Bars.B4.Utils.Display("Тип земельного участка")]
        [Bars.Rms.Core.Attributes.Uid("de3b7de1-5e32-4e8a-a778-4c57e178492c")]
        public virtual Bars.Nedragis.TipZemelNogoUchastkaListModel TypeZU
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый округ'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый округ")]
        [Bars.Rms.Core.Attributes.Uid("98cab5b3-10af-4147-a3d8-3c25639eceae")]
        public virtual System.Int32? cad_okrug
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый район'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый район")]
        [Bars.Rms.Core.Attributes.Uid("2b41b5cd-e104-40d6-9ac5-176c6f20bdd1")]
        public virtual System.Int32? cad_raion
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый блок'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый блок")]
        [Bars.Rms.Core.Attributes.Uid("cceee588-c86b-4cad-b33d-6968d4a33f7d")]
        public virtual System.Int32? cad_block
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый массив'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый массив")]
        [Bars.Rms.Core.Attributes.Uid("9114bbeb-07b7-45b4-92be-851083ae3151")]
        public virtual System.Int32? cad_massiv
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый квартал'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый квартал")]
        [Bars.Rms.Core.Attributes.Uid("f17c362e-2a96-40dd-8a98-4aadbe7c186e")]
        public virtual System.Int32? cad_kvartal
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер ЗУ'
        /// </summary>
        [Bars.B4.Utils.Display("Номер ЗУ")]
        [Bars.Rms.Core.Attributes.Uid("45aa6ef9-23b9-41d4-a1ba-fad9cc61da48")]
        public virtual System.Int32? number_ZU
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый номер'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый номер")]
        [Bars.Rms.Core.Attributes.Uid("e7329ddb-1276-49c6-ba0a-821ea948ad72")]
        public virtual System.String cadnum1
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("ec2e5b64-7591-4af1-a53b-434c1f263ae3")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Назначение'
        /// </summary>
        [Bars.B4.Utils.Display("Назначение")]
        [Bars.Rms.Core.Attributes.Uid("90696a1e-c508-41d6-a586-32216ab1688f")]
        public virtual System.String naznach
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Общая площадь, кв. м'
        /// </summary>
        [Bars.B4.Utils.Display("Общая площадь, кв. м")]
        [Bars.Rms.Core.Attributes.Uid("684a8903-5e4a-4811-9af6-453a5e522145")]
        public virtual System.Decimal? area
        {
            get;
            set;
        }

        /// <summary>
        /// Статус объекта
        /// </summary>
        [Bars.B4.Utils.Display("Статус объекта")]
        [Bars.Rms.Core.Attributes.Uid("2fd14bb8-a655-49cc-8848-dadefa4e5a7f")]
        public virtual Bars.Nedragis.StatusyObEktaListModel Status_obj
        {
            get;
            set;
        }

        /// <summary>
        /// Категория земли
        /// </summary>
        [Bars.B4.Utils.Display("Категория земли")]
        [Bars.Rms.Core.Attributes.Uid("8eef49a7-bcf0-479e-82bf-171993dc6998")]
        public virtual Bars.Nedragis.KategoriiZemelListModel category
        {
            get;
            set;
        }

        /// <summary>
        /// Вид права на землю
        /// </summary>
        [Bars.B4.Utils.Display("Вид права на землю")]
        [Bars.Rms.Core.Attributes.Uid("e6b82795-fd0c-4e0d-8e39-0091102d1c94")]
        public virtual Bars.Nedragis.VidPravaNaZemljuListModel vid_prava
        {
            get;
            set;
        }

        /// <summary>
        /// Вид разрешенного использования
        /// </summary>
        [Bars.B4.Utils.Display("Вид разрешенного использования")]
        [Bars.Rms.Core.Attributes.Uid("b4335fef-0f01-42ec-9da3-0d49177474d0")]
        public virtual Bars.Nedragis.VRIZUIerarkhijaModel vid_use
        {
            get;
            set;
        }

        /// <summary>
        /// Правообладатель
        /// </summary>
        [Bars.B4.Utils.Display("Правообладатель")]
        [Bars.Rms.Core.Attributes.Uid("6d4b7379-2e15-4d1f-893d-8f631e4e8608")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel holder_id
        {
            get;
            set;
        }

        /// <summary>
        /// Обременения
        /// </summary>
        [Bars.B4.Utils.Display("Обременения")]
        [Bars.Rms.Core.Attributes.Uid("9181e440-1e37-44df-9cc7-4c9751787065")]
        public virtual Bars.Nedragis.VidOgranichenijaListModel bremya
        {
            get;
            set;
        }

        /// <summary>
        /// Собственник
        /// </summary>
        [Bars.B4.Utils.Display("Собственник")]
        [Bars.Rms.Core.Attributes.Uid("75060ae0-0e43-49df-90ce-7dd8cb1bc9cd")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel owner_id
        {
            get;
            set;
        }

        /// <summary>
        /// ОКАТО
        /// </summary>
        [Bars.B4.Utils.Display("ОКАТО")]
        [Bars.Rms.Core.Attributes.Uid("2f67ca2d-2709-450c-b9df-fb6193c1b27d")]
        public virtual Bars.Nedragis.OKATO1Model okato
        {
            get;
            set;
        }

        /// <summary>
        /// ОКТМО
        /// </summary>
        [Bars.B4.Utils.Display("ОКТМО")]
        [Bars.Rms.Core.Attributes.Uid("6908a298-30aa-407c-a735-eaac3f30d0b0")]
        public virtual Bars.Nedragis.OKTMO1Model oktmo
        {
            get;
            set;
        }

        /// <summary>
        /// Уполномоченный орган
        /// </summary>
        [Bars.B4.Utils.Display("Уполномоченный орган")]
        [Bars.Rms.Core.Attributes.Uid("5d33ff7c-a2c9-4696-959b-4270d3dd886f")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel property_manager_id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Описание месторасположения границ'
        /// </summary>
        [Bars.B4.Utils.Display("Описание месторасположения границ")]
        [Bars.Rms.Core.Attributes.Uid("a94f8dc5-cc96-411f-bc62-141402bb5dd7")]
        public virtual System.String Borders
        {
            get;
            set;
        }
    }
}