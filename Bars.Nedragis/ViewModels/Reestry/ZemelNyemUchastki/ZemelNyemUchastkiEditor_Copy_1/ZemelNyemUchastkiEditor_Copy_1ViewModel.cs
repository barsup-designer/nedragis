namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'ЗУ Основные сведения'
    /// </summary>
    public interface IZemelNyemUchastkiEditor_Copy_1ViewModel : IEntityEditorViewModel<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditor_Copy_1Model>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'ЗУ Основные сведения'
    /// </summary>
    public interface IZemelNyemUchastkiEditor_Copy_1Validator : IEditorModelValidator<ZemelNyemUchastkiEditor_Copy_1Model>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'ЗУ Основные сведения'
    /// </summary>
    public interface IZemelNyemUchastkiEditor_Copy_1Handler : IEntityEditorViewModelHandler<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditor_Copy_1Model>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'ЗУ Основные сведения'
    /// </summary>
    public abstract class AbstractZemelNyemUchastkiEditor_Copy_1Handler : EntityEditorViewModelHandler<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditor_Copy_1Model>, IZemelNyemUchastkiEditor_Copy_1Handler
    {
    }

    /// <summary>
    /// Реализация модели представления 'ЗУ Основные сведения'
    /// </summary>
    public class ZemelNyemUchastkiEditor_Copy_1ViewModel : BaseEditorViewModel<Bars.Nedragis.ZemelNyemUchastki, ZemelNyemUchastkiEditor_Copy_1Model>, IZemelNyemUchastkiEditor_Copy_1ViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ZemelNyemUchastkiEditor_Copy_1Model CreateModelInternal(BaseParams @params)
        {
            var model = new ZemelNyemUchastkiEditor_Copy_1Model();
            var vartypeobjId = @params.Params.GetAs<long>("typeobj_Id", 0);
            if (vartypeobjId > 0)
            {
                model.typeobj = Container.Resolve<Bars.Nedragis.ITipyObEktaPravaListQuery>().GetById(vartypeobjId);
            }

            var varTypeZUId = @params.Params.GetAs<long>("TypeZU_Id", 0);
            if (varTypeZUId > 0)
            {
                model.TypeZU = Container.Resolve<Bars.Nedragis.ITipZemelNogoUchastkaListQuery>().GetById(varTypeZUId);
            }

            var varStatus_objId = @params.Params.GetAs<long>("Status_obj_Id", 0);
            if (varStatus_objId > 0)
            {
                model.Status_obj = Container.Resolve<Bars.Nedragis.IStatusyObEktaListQuery>().GetById(varStatus_objId);
            }

            var varcategoryId = @params.Params.GetAs<long>("category_Id", 0);
            if (varcategoryId > 0)
            {
                model.category = Container.Resolve<Bars.Nedragis.IKategoriiZemelListQuery>().GetById(varcategoryId);
            }

            var varvid_pravaId = @params.Params.GetAs<long>("vid_prava_Id", 0);
            if (varvid_pravaId > 0)
            {
                model.vid_prava = Container.Resolve<Bars.Nedragis.IVidPravaNaZemljuListQuery>().GetById(varvid_pravaId);
            }

            var varvid_useId = @params.Params.GetAs<long>("vid_use_Id", 0);
            if (varvid_useId > 0)
            {
                model.vid_use = Container.Resolve<Bars.Nedragis.IVRIZUIerarkhijaQuery>().GetById(varvid_useId);
            }

            var varholder_idId = @params.Params.GetAs<long>("holder_id_Id", 0);
            if (varholder_idId > 0)
            {
                model.holder_id = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>().GetById(varholder_idId);
            }

            var varbremyaId = @params.Params.GetAs<long>("bremya_Id", 0);
            if (varbremyaId > 0)
            {
                model.bremya = Container.Resolve<Bars.Nedragis.IVidOgranichenijaListQuery>().GetById(varbremyaId);
            }

            var varowner_idId = @params.Params.GetAs<long>("owner_id_Id", 0);
            if (varowner_idId > 0)
            {
                model.owner_id = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>().GetById(varowner_idId);
            }

            var varokatoId = @params.Params.GetAs<long>("okato_Id", 0);
            if (varokatoId > 0)
            {
                model.okato = Container.Resolve<Bars.Nedragis.IOKATO1Query>().GetById(varokatoId);
            }

            var varoktmoId = @params.Params.GetAs<long>("oktmo_Id", 0);
            if (varoktmoId > 0)
            {
                model.oktmo = Container.Resolve<Bars.Nedragis.IOKTMO1Query>().GetById(varoktmoId);
            }

            var varproperty_manager_idId = @params.Params.GetAs<long>("property_manager_id_Id", 0);
            if (varproperty_manager_idId > 0)
            {
                model.property_manager_id = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>().GetById(varproperty_manager_idId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Земельные участки' в модель представления
        /// </summary>
        protected override ZemelNyemUchastkiEditor_Copy_1Model MapEntityInternal(Bars.Nedragis.ZemelNyemUchastki entity)
        {
            // создаем экзепляр модели
            var model = new ZemelNyemUchastkiEditor_Copy_1Model();
            model.Id = entity.Id;
            if (entity.typeobj.IsNotNull())
            {
                var queryTipyObEktaPravaList = Container.Resolve<Bars.Nedragis.ITipyObEktaPravaListQuery>();
                model.typeobj = queryTipyObEktaPravaList.GetById(entity.typeobj.Id);
            }

            if (entity.TypeZU.IsNotNull())
            {
                var queryTipZemelNogoUchastkaList = Container.Resolve<Bars.Nedragis.ITipZemelNogoUchastkaListQuery>();
                model.TypeZU = queryTipZemelNogoUchastkaList.GetById(entity.TypeZU.Id);
            }

            model.cad_okrug = (System.Int32? )(entity.cad_okrug);
            model.cad_raion = (System.Int32? )(entity.cad_raion);
            model.cad_block = (System.Int32? )(entity.cad_block);
            model.cad_massiv = (System.Int32? )(entity.cad_massiv);
            model.cad_kvartal = (System.Int32? )(entity.cad_kvartal);
            model.number_ZU = (System.Int32? )(entity.number_ZU);
            model.cadnum1 = (System.String)(entity.cadnum1);
            model.name = (System.String)(entity.name);
            model.naznach = (System.String)(entity.naznach);
            model.area = (System.Decimal? )(entity.area);
            if (entity.Status_obj.IsNotNull())
            {
                var queryStatusyObEktaList = Container.Resolve<Bars.Nedragis.IStatusyObEktaListQuery>();
                model.Status_obj = queryStatusyObEktaList.GetById(entity.Status_obj.Id);
            }

            if (entity.category.IsNotNull())
            {
                var queryKategoriiZemelList = Container.Resolve<Bars.Nedragis.IKategoriiZemelListQuery>();
                model.category = queryKategoriiZemelList.GetById(entity.category.Id);
            }

            if (entity.vid_prava.IsNotNull())
            {
                var queryVidPravaNaZemljuList = Container.Resolve<Bars.Nedragis.IVidPravaNaZemljuListQuery>();
                model.vid_prava = queryVidPravaNaZemljuList.GetById(entity.vid_prava.Id);
            }

            if (entity.vid_use.IsNotNull())
            {
                var queryVRIZUIerarkhija = Container.Resolve<Bars.Nedragis.IVRIZUIerarkhijaQuery>();
                model.vid_use = queryVRIZUIerarkhija.GetById(entity.vid_use.Id);
            }

            if (entity.holder_id.IsNotNull())
            {
                var queryJuridicheskieLicaList = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>();
                model.holder_id = queryJuridicheskieLicaList.GetById(entity.holder_id.Id);
            }

            if (entity.bremya.IsNotNull())
            {
                var queryVidOgranichenijaList = Container.Resolve<Bars.Nedragis.IVidOgranichenijaListQuery>();
                model.bremya = queryVidOgranichenijaList.GetById(entity.bremya.Id);
            }

            if (entity.owner_id.IsNotNull())
            {
                var queryJuridicheskieLicaList = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>();
                model.owner_id = queryJuridicheskieLicaList.GetById(entity.owner_id.Id);
            }

            if (entity.okato.IsNotNull())
            {
                var queryOKATO1 = Container.Resolve<Bars.Nedragis.IOKATO1Query>();
                model.okato = queryOKATO1.GetById(entity.okato.Id);
            }

            if (entity.oktmo.IsNotNull())
            {
                var queryOKTMO1 = Container.Resolve<Bars.Nedragis.IOKTMO1Query>();
                model.oktmo = queryOKTMO1.GetById(entity.oktmo.Id);
            }

            if (entity.property_manager_id.IsNotNull())
            {
                var queryJuridicheskieLicaList = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>();
                model.property_manager_id = queryJuridicheskieLicaList.GetById(entity.property_manager_id.Id);
            }

            model.Borders = (System.String)(entity.Borders);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Земельные участки' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.ZemelNyemUchastki entity, ZemelNyemUchastkiEditor_Copy_1Model model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.typeobj = TryLoadEntityById<Bars.Nedragis.TipyObEktaPrava>(model.typeobj?.Id);
            entity.TypeZU = TryLoadEntityById<Bars.Nedragis.TipZemelNogoUchastka>(model.TypeZU?.Id);
            if (model.cad_okrug.HasValue)
            {
                entity.cad_okrug = model.cad_okrug.Value;
            }

            if (model.cad_raion.HasValue)
            {
                entity.cad_raion = model.cad_raion.Value;
            }

            if (model.cad_block.HasValue)
            {
                entity.cad_block = model.cad_block.Value;
            }

            if (model.cad_massiv.HasValue)
            {
                entity.cad_massiv = model.cad_massiv.Value;
            }

            if (model.cad_kvartal.HasValue)
            {
                entity.cad_kvartal = model.cad_kvartal.Value;
            }

            if (model.number_ZU.HasValue)
            {
                entity.number_ZU = model.number_ZU.Value;
            }

            entity.name = model.name;
            entity.naznach = model.naznach;
            if (model.area.HasValue)
            {
                entity.area = model.area.Value;
            }

            entity.Status_obj = TryLoadEntityById<Bars.Nedragis.StatusyObEkta>(model.Status_obj?.Id);
            entity.category = TryLoadEntityById<Bars.Nedragis.KategoriiZemel>(model.category?.Id);
            entity.vid_prava = TryLoadEntityById<Bars.Nedragis.VidPravaNaZemlju>(model.vid_prava?.Id);
            entity.vid_use = TryLoadEntityById<Bars.Nedragis.VRIZU>(model.vid_use?.Id);
            entity.holder_id = TryLoadEntityById<Bars.Nedragis.SubEktyPrava>(model.holder_id?.Id);
            entity.bremya = TryLoadEntityById<Bars.Nedragis.VidOgranichenija>(model.bremya?.Id);
            entity.owner_id = TryLoadEntityById<Bars.Nedragis.SubEktyPrava>(model.owner_id?.Id);
            entity.okato = TryLoadEntityById<Bars.Nedragis.OKATO>(model.okato?.Id);
            entity.oktmo = TryLoadEntityById<Bars.Nedragis.OKTMO>(model.oktmo?.Id);
            entity.property_manager_id = TryLoadEntityById<Bars.Nedragis.JuridicheskieLica>(model.property_manager_id?.Id);
            entity.Borders = model.Borders;
        }
    }
}