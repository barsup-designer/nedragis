namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'ЗУ форма ввода'
    /// </summary>
    public interface IZUFormaVvodaViewModel : IEntityEditorViewModel<Bars.Nedragis.ZemelNyemUchastki, ZUFormaVvodaModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'ЗУ форма ввода'
    /// </summary>
    public interface IZUFormaVvodaValidator : IEditorModelValidator<ZUFormaVvodaModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'ЗУ форма ввода'
    /// </summary>
    public interface IZUFormaVvodaHandler : IEntityEditorViewModelHandler<Bars.Nedragis.ZemelNyemUchastki, ZUFormaVvodaModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'ЗУ форма ввода'
    /// </summary>
    public abstract class AbstractZUFormaVvodaHandler : EntityEditorViewModelHandler<Bars.Nedragis.ZemelNyemUchastki, ZUFormaVvodaModel>, IZUFormaVvodaHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'ЗУ форма ввода'
    /// </summary>
    public class ZUFormaVvodaViewModel : BaseEditorViewModel<Bars.Nedragis.ZemelNyemUchastki, ZUFormaVvodaModel>, IZUFormaVvodaViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ZUFormaVvodaModel CreateModelInternal(BaseParams @params)
        {
            var model = new ZUFormaVvodaModel();
            var varowner_idId = @params.Params.GetAs<long>("owner_id_Id", 0);
            if (varowner_idId > 0)
            {
                model.owner_id = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>().GetById(varowner_idId);
            }

            var varholder_idId = @params.Params.GetAs<long>("holder_id_Id", 0);
            if (varholder_idId > 0)
            {
                model.holder_id = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>().GetById(varholder_idId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Земельные участки' в модель представления
        /// </summary>
        protected override ZUFormaVvodaModel MapEntityInternal(Bars.Nedragis.ZemelNyemUchastki entity)
        {
            // создаем экзепляр модели
            var model = new ZUFormaVvodaModel();
            model.Id = entity.Id;
            model.name = (System.String)(entity.name);
            model.area = (System.Decimal? )(entity.area);
            if (entity.owner_id.IsNotNull())
            {
                var queryJuridicheskieLicaList = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>();
                model.owner_id = queryJuridicheskieLicaList.GetById(entity.owner_id.Id);
            }

            if (entity.holder_id.IsNotNull())
            {
                var queryJuridicheskieLicaList = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>();
                model.holder_id = queryJuridicheskieLicaList.GetById(entity.holder_id.Id);
            }

            model.cad_okrug = (System.Int32? )(entity.cad_okrug);
            model.cad_raion = (System.Int32? )(entity.cad_raion);
            model.cad_block = (System.Int32? )(entity.cad_block);
            model.cad_massiv = (System.Int32? )(entity.cad_massiv);
            model.cad_kvartal = (System.Int32? )(entity.cad_kvartal);
            model.number_ZU = (System.Int32? )(entity.number_ZU);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Земельные участки' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.ZemelNyemUchastki entity, ZUFormaVvodaModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name = model.name;
            if (model.area.HasValue)
            {
                entity.area = model.area.Value;
            }

            entity.owner_id = TryLoadEntityById<Bars.Nedragis.SubEktyPrava>(model.owner_id?.Id);
            entity.holder_id = TryLoadEntityById<Bars.Nedragis.SubEktyPrava>(model.holder_id?.Id);
            if (model.cad_okrug.HasValue)
            {
                entity.cad_okrug = model.cad_okrug.Value;
            }

            if (model.cad_raion.HasValue)
            {
                entity.cad_raion = model.cad_raion.Value;
            }

            if (model.cad_block.HasValue)
            {
                entity.cad_block = model.cad_block.Value;
            }

            if (model.cad_massiv.HasValue)
            {
                entity.cad_massiv = model.cad_massiv.Value;
            }

            if (model.cad_kvartal.HasValue)
            {
                entity.cad_kvartal = model.cad_kvartal.Value;
            }

            if (model.number_ZU.HasValue)
            {
                entity.number_ZU = model.number_ZU.Value;
            }
        }
    }
}