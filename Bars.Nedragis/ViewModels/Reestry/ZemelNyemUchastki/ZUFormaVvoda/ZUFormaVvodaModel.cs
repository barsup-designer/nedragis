namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'ЗУ форма ввода' для отдачи на клиент
    /// </summary>
    public class ZUFormaVvodaModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public ZUFormaVvodaModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("4acb9dd3-3d84-4544-a921-7d06306be462")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Общая площадь, кв. м'
        /// </summary>
        [Bars.B4.Utils.Display("Общая площадь, кв. м")]
        [Bars.Rms.Core.Attributes.Uid("9b12332c-48e8-4506-a283-752fba4d889a")]
        public virtual System.Decimal? area
        {
            get;
            set;
        }

        /// <summary>
        /// Собственник
        /// </summary>
        [Bars.B4.Utils.Display("Собственник")]
        [Bars.Rms.Core.Attributes.Uid("ca65d585-c415-4115-a93f-b3e39bd4952f")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel owner_id
        {
            get;
            set;
        }

        /// <summary>
        /// Правообладатель
        /// </summary>
        [Bars.B4.Utils.Display("Правообладатель")]
        [Bars.Rms.Core.Attributes.Uid("18317267-b7d3-4a07-8d4a-5d9fc13680f7")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel holder_id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый округ'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый округ")]
        [Bars.Rms.Core.Attributes.Uid("54e66d9c-1c11-4a94-9851-1c88f4de89c4")]
        public virtual System.Int32? cad_okrug
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый район'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый район")]
        [Bars.Rms.Core.Attributes.Uid("ac073682-ff08-472a-8d12-d0369ef5e10f")]
        public virtual System.Int32? cad_raion
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый блок'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый блок")]
        [Bars.Rms.Core.Attributes.Uid("7be5efdd-10bd-4865-93ca-6d9f41687aef")]
        public virtual System.Int32? cad_block
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый массив'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый массив")]
        [Bars.Rms.Core.Attributes.Uid("b6f995e4-c641-45d2-828a-8c6cf672ddfc")]
        public virtual System.Int32? cad_massiv
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Кадастровый квартал'
        /// </summary>
        [Bars.B4.Utils.Display("Кадастровый квартал")]
        [Bars.Rms.Core.Attributes.Uid("1a6ddb3b-13a5-41cd-9cff-8a1227370dbf")]
        public virtual System.Int32? cad_kvartal
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер ЗУ'
        /// </summary>
        [Bars.B4.Utils.Display("Номер ЗУ")]
        [Bars.Rms.Core.Attributes.Uid("a71ed8c8-1323-4a30-b5df-5a359c02f0d5")]
        public virtual System.Int32? number_ZU
        {
            get;
            set;
        }
    }
}