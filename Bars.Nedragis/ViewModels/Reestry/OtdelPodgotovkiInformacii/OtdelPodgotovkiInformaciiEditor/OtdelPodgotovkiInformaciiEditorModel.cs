namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Отдел подготовки информации' для отдачи на клиент
    /// </summary>
    public class OtdelPodgotovkiInformaciiEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public OtdelPodgotovkiInformaciiEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер п/п'
        /// </summary>
        [Bars.B4.Utils.Display("Номер п/п")]
        [Bars.Rms.Core.Attributes.Uid("83e05413-f63c-4a62-9802-8cb0e6f793b5")]
        public virtual System.Int32? Element1510134896283
        {
            get;
            set;
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("f0219ff1-7c11-4bdf-8741-305b62aac65b")]
        public virtual Bars.Nedragis.VidRabotOPINFListModel Element1510135000178
        {
            get;
            set;
        }

        /// <summary>
        /// Подвид работ
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("3baf6263-59b3-427b-a07b-a2036442bbc3")]
        public virtual Bars.Nedragis.VidRabotOPINFListModel Element1510135042374
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Источник поступления информации'
        /// </summary>
        [Bars.B4.Utils.Display("Источник поступления информации")]
        [Bars.Rms.Core.Attributes.Uid("6e605d94-436c-494b-b388-52ee1ddc59ba")]
        public virtual System.String Element1510135074618
        {
            get;
            set;
        }

        /// <summary>
        /// Исполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("c6349342-e06a-40c9-9713-424da4cf61ff")]
        public virtual Bars.Nedragis.Sotrudniki1ListModel Element1510135121705
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Колчество заявок'
        /// </summary>
        [Bars.B4.Utils.Display("Колчество заявок")]
        [Bars.Rms.Core.Attributes.Uid("f93dfa7e-92aa-4fe1-a1e7-2b19079dc3d5")]
        public virtual System.Int32? Element1510135249843
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Количество листов'
        /// </summary>
        [Bars.B4.Utils.Display("Количество листов")]
        [Bars.Rms.Core.Attributes.Uid("1046c2c4-9b43-4832-ad34-80c5e3509ffb")]
        public virtual System.Int32? Element1510135325790
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер документа'
        /// </summary>
        [Bars.B4.Utils.Display("Номер документа")]
        [Bars.Rms.Core.Attributes.Uid("33ca0a25-54b8-4260-ac5d-c94a44aa56c5")]
        public virtual System.String Element1510135360701
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Количество папок'
        /// </summary>
        [Bars.B4.Utils.Display("Количество папок")]
        [Bars.Rms.Core.Attributes.Uid("83719061-0f1b-4d77-bd3b-72e136666f97")]
        public virtual System.Int32? Element1510135484012
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Количество файлов'
        /// </summary>
        [Bars.B4.Utils.Display("Количество файлов")]
        [Bars.Rms.Core.Attributes.Uid("6ebad3f5-57c1-4a86-b66a-0338d1f731cc")]
        public virtual System.Int32? Element1510135527140
        {
            get;
            set;
        }
    }
}