namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Отдел подготовки информации'
    /// </summary>
    public interface IOtdelPodgotovkiInformaciiEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.OtdelPodgotovkiInformacii, OtdelPodgotovkiInformaciiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Отдел подготовки информации'
    /// </summary>
    public interface IOtdelPodgotovkiInformaciiEditorValidator : IEditorModelValidator<OtdelPodgotovkiInformaciiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Отдел подготовки информации'
    /// </summary>
    public interface IOtdelPodgotovkiInformaciiEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.OtdelPodgotovkiInformacii, OtdelPodgotovkiInformaciiEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Отдел подготовки информации'
    /// </summary>
    public abstract class AbstractOtdelPodgotovkiInformaciiEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.OtdelPodgotovkiInformacii, OtdelPodgotovkiInformaciiEditorModel>, IOtdelPodgotovkiInformaciiEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Отдел подготовки информации'
    /// </summary>
    public class OtdelPodgotovkiInformaciiEditorViewModel : BaseEditorViewModel<Bars.Nedragis.OtdelPodgotovkiInformacii, OtdelPodgotovkiInformaciiEditorModel>, IOtdelPodgotovkiInformaciiEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OtdelPodgotovkiInformaciiEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OtdelPodgotovkiInformaciiEditorModel();
            var varElement1510135000178Id = @params.Params.GetAs<long>("Element1510135000178_Id", 0);
            if (varElement1510135000178Id > 0)
            {
                model.Element1510135000178 = Container.Resolve<Bars.Nedragis.IVidRabotOPINFListQuery>().GetById(varElement1510135000178Id);
            }

            var varElement1510135042374Id = @params.Params.GetAs<long>("Element1510135042374_Id", 0);
            if (varElement1510135042374Id > 0)
            {
                model.Element1510135042374 = Container.Resolve<Bars.Nedragis.IVidRabotOPINFListQuery>().GetById(varElement1510135042374Id);
            }

            var varElement1510135121705Id = @params.Params.GetAs<long>("Element1510135121705_Id", 0);
            if (varElement1510135121705Id > 0)
            {
                model.Element1510135121705 = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>().GetById(varElement1510135121705Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Отдел подготовки информации' в модель представления
        /// </summary>
        protected override OtdelPodgotovkiInformaciiEditorModel MapEntityInternal(Bars.Nedragis.OtdelPodgotovkiInformacii entity)
        {
            // создаем экзепляр модели
            var model = new OtdelPodgotovkiInformaciiEditorModel();
            model.Id = entity.Id;
            model.Element1510134896283 = (System.Int32? )(entity.Element1510134896283);
            if (entity.Element1510135000178.IsNotNull())
            {
                var queryVidRabotOPINFList = Container.Resolve<Bars.Nedragis.IVidRabotOPINFListQuery>();
                model.Element1510135000178 = queryVidRabotOPINFList.GetById(entity.Element1510135000178.Id);
            }

            if (entity.Element1510135042374.IsNotNull())
            {
                var queryVidRabotOPINFList = Container.Resolve<Bars.Nedragis.IVidRabotOPINFListQuery>();
                model.Element1510135042374 = queryVidRabotOPINFList.GetById(entity.Element1510135042374.Id);
            }

            model.Element1510135074618 = (System.String)(entity.Element1510135074618);
            if (entity.Element1510135121705.IsNotNull())
            {
                var querySotrudniki1List = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>();
                model.Element1510135121705 = querySotrudniki1List.GetById(entity.Element1510135121705.Id);
            }

            model.Element1510135249843 = (System.Int32? )(entity.Element1510135249843);
            model.Element1510135325790 = (System.Int32? )(entity.Element1510135325790);
            model.Element1510135360701 = (System.String)(entity.Element1510135360701);
            model.Element1510135484012 = (System.Int32? )(entity.Element1510135484012);
            model.Element1510135527140 = (System.Int32? )(entity.Element1510135527140);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Отдел подготовки информации' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.OtdelPodgotovkiInformacii entity, OtdelPodgotovkiInformaciiEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.Element1510134896283.HasValue)
            {
                entity.Element1510134896283 = model.Element1510134896283.Value;
            }

            entity.Element1510135000178 = TryLoadEntityById<Bars.Nedragis.VidRabotOPINF>(model.Element1510135000178?.Id);
            entity.Element1510135042374 = TryLoadEntityById<Bars.Nedragis.VidRabotOPINF>(model.Element1510135042374?.Id);
            entity.Element1510135074618 = model.Element1510135074618;
            entity.Element1510135121705 = TryLoadEntityById<Bars.Nedragis.Sotrudniki1>(model.Element1510135121705?.Id);
            if (model.Element1510135249843.HasValue)
            {
                entity.Element1510135249843 = model.Element1510135249843.Value;
            }

            if (model.Element1510135325790.HasValue)
            {
                entity.Element1510135325790 = model.Element1510135325790.Value;
            }

            entity.Element1510135360701 = model.Element1510135360701;
            if (model.Element1510135484012.HasValue)
            {
                entity.Element1510135484012 = model.Element1510135484012.Value;
            }

            if (model.Element1510135527140.HasValue)
            {
                entity.Element1510135527140 = model.Element1510135527140.Value;
            }
        }
    }
}