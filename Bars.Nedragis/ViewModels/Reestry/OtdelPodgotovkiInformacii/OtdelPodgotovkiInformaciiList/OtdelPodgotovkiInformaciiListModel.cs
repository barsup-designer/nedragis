namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Отдел подготовки информации'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Отдел подготовки информации")]
    public class OtdelPodgotovkiInformaciiListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер п/п' (псевдоним: Element1510134896283)
        /// </summary>
        [Bars.B4.Utils.Display("Номер п/п")]
        [Bars.Rms.Core.Attributes.Uid("0e7ccd86-897b-459a-bab0-d9591c561c50")]
        public virtual System.Int32? Element1510134896283
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Вид работ.Вид работ' (псевдоним: Element1510135000178_VidRabotOPINF222)
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ.Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("c0f2bf3b-4a68-4392-a51e-3f807a69ebbd")]
        public virtual System.String Element1510135000178_VidRabotOPINF222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Подвид работ.Подвид работ' (псевдоним: Element1510135042374_Element1510134562537)
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ.Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("eb88f1e4-a728-4416-9383-e3acdfe919aa")]
        public virtual System.String Element1510135042374_Element1510134562537
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Источник поступления информации' (псевдоним: Element1510135074618)
        /// </summary>
        [Bars.B4.Utils.Display("Источник поступления информации")]
        [Bars.Rms.Core.Attributes.Uid("e90a021e-e14d-415e-8ca4-b2f7523ef799")]
        public virtual System.String Element1510135074618
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Исполнитель.ФИО' (псевдоним: Element1510135121705_Element1474352136232)
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель.ФИО")]
        [Bars.Rms.Core.Attributes.Uid("0824bd71-6d9b-46c5-a502-952e5b2e638e")]
        public virtual System.String Element1510135121705_Element1474352136232
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Колчество заявок' (псевдоним: Element1510135249843)
        /// </summary>
        [Bars.B4.Utils.Display("Колчество заявок")]
        [Bars.Rms.Core.Attributes.Uid("b5397769-406a-4306-a295-0d00535db478")]
        public virtual System.Int32? Element1510135249843
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Количество листов' (псевдоним: Element1510135325790)
        /// </summary>
        [Bars.B4.Utils.Display("Количество листов")]
        [Bars.Rms.Core.Attributes.Uid("cfdc357c-0665-4601-98f4-d37c88677a6c")]
        public virtual System.Int32? Element1510135325790
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер документа' (псевдоним: Element1510135360701)
        /// </summary>
        [Bars.B4.Utils.Display("Номер документа")]
        [Bars.Rms.Core.Attributes.Uid("b2bb34b5-07ac-4407-99db-594e38ec8a7b")]
        public virtual System.String Element1510135360701
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Количество папок' (псевдоним: Element1510135484012)
        /// </summary>
        [Bars.B4.Utils.Display("Количество папок")]
        [Bars.Rms.Core.Attributes.Uid("608be445-a165-40a9-9cc7-dd7ec18f51e6")]
        public virtual System.Int32? Element1510135484012
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Количество файлов' (псевдоним: Element1510135527140)
        /// </summary>
        [Bars.B4.Utils.Display("Количество файлов")]
        [Bars.Rms.Core.Attributes.Uid("5a253ced-6ebd-4a7a-b757-722ddc56ce1f")]
        public virtual System.Int32? Element1510135527140
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}