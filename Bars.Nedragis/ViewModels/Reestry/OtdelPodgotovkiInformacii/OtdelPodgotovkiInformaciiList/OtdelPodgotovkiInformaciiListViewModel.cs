namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Commons;
    using Bars.Rms.GeneratedApp.Lists;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Linq;

    /// <summary>
    /// Контракт модели представления 'Реестр Отдел подготовки информации'
    /// </summary>
    public interface IOtdelPodgotovkiInformaciiListViewModel : IViewModel<Bars.Nedragis.OtdelPodgotovkiInformacii, OtdelPodgotovkiInformaciiListModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Реестр Отдел подготовки информации'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Отдел подготовки информации")]
    public class OtdelPodgotovkiInformaciiListViewModel : BaseListViewModel<Bars.Nedragis.OtdelPodgotovkiInformacii, OtdelPodgotovkiInformaciiListModel, IOtdelPodgotovkiInformaciiListQuery>, IOtdelPodgotovkiInformaciiListViewModel
    {
        /// <summary>
        /// Конструктор модели
        /// </summary>
        /// <param name = "query">Запрос данных</param>    
        public OtdelPodgotovkiInformaciiListViewModel(IDataStore datastore, IOtdelPodgotovkiInformaciiListQuery query): base (datastore, query)
        {
        }

        /// <summary>
        /// Получение и упаковка в модель списка сущностей
        /// </summary>
        protected override IDataResult List(IQueryable<OtdelPodgotovkiInformaciiListModel> query, LoadParam loadParams, BaseParams baseParams)
        {
            IList<OtdelPodgotovkiInformaciiListModel> map;
            var sorters = (loadParams.Order ?? Enumerable.Empty<OrderField>()).ToArray();
            var typeOrderFields = sorters.Where(x => x.Name.IsOneOf("_TypeDisplay", "_TypeUid")).ToArray();
            if (typeOrderFields.Any())
                loadParams.Order = sorters.Except(typeOrderFields).ToArray();
            int totalCount = 0;
            var summaryData = DynamicDictionary.Create();
            if (typeof (NhQueryable<>).IsAssignableFrom(query.GetType().GetGenericTypeDefinition()))
            {
                var totalCountFuture = query.ToFutureValue(x => x.Count());
                map = query.Order(loadParams).Paging(loadParams).ToFuture().ToList();
                totalCount = totalCountFuture.Value;
            }
            else
            {
                totalCount = query.Count();
                map = query.Order(loadParams).Paging(loadParams).ToList();
            }

            if (typeOrderFields.Any())
                map = map.OrderBy(x => x._TypeUid).ToList();
            // формирование результата
            return new ListDataResultWithSummary(map, totalCount, summaryData);
        }
    }
}