namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Отдел подготовки информации'
    /// </summary>
    public interface IOtdelPodgotovkiInformaciiListQuery : IQueryOperation<Bars.Nedragis.OtdelPodgotovkiInformacii, Bars.Nedragis.OtdelPodgotovkiInformaciiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Отдел подготовки информации'
    /// </summary>
    public interface IOtdelPodgotovkiInformaciiListQueryFilter : IQueryOperationFilter<Bars.Nedragis.OtdelPodgotovkiInformacii, Bars.Nedragis.OtdelPodgotovkiInformaciiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Отдел подготовки информации'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Отдел подготовки информации")]
    public class OtdelPodgotovkiInformaciiListQuery : RmsEntityQueryOperation<Bars.Nedragis.OtdelPodgotovkiInformacii, Bars.Nedragis.OtdelPodgotovkiInformaciiListModel>, IOtdelPodgotovkiInformaciiListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OtdelPodgotovkiInformaciiListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OtdelPodgotovkiInformaciiListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OtdelPodgotovkiInformacii> Filter(IQueryable<Bars.Nedragis.OtdelPodgotovkiInformacii> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OtdelPodgotovkiInformaciiListModel> Map(IQueryable<Bars.Nedragis.OtdelPodgotovkiInformacii> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OtdelPodgotovkiInformacii>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OtdelPodgotovkiInformaciiListModel{Id = x.Id, _TypeUid = "ef5acf69-1403-413d-ac6e-11caae67cfa8", Element1510134896283 = (System.Int32? )(x.Element1510134896283), Element1510135000178_VidRabotOPINF222 = (System.String)(x.Element1510135000178.VidRabotOPINF222), Element1510135042374_Element1510134562537 = (System.String)(x.Element1510135042374.Element1510134562537), Element1510135074618 = (System.String)(x.Element1510135074618), Element1510135121705_Element1474352136232 = (System.String)(x.Element1510135121705.Element1474352136232), Element1510135249843 = (System.Int32? )(x.Element1510135249843), Element1510135325790 = (System.Int32? )(x.Element1510135325790), Element1510135360701 = (System.String)(x.Element1510135360701), Element1510135484012 = (System.Int32? )(x.Element1510135484012), Element1510135527140 = (System.Int32? )(x.Element1510135527140), });
        }
    }
}