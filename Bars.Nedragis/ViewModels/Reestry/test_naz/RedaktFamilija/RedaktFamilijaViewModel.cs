namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Редакт_Фамилия'
    /// </summary>
    public interface IRedaktFamilijaViewModel : IEntityEditorViewModel<Bars.Nedragis.Familija, RedaktFamilijaModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Редакт_Фамилия'
    /// </summary>
    public interface IRedaktFamilijaValidator : IEditorModelValidator<RedaktFamilijaModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Редакт_Фамилия'
    /// </summary>
    public interface IRedaktFamilijaHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Familija, RedaktFamilijaModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Редакт_Фамилия'
    /// </summary>
    public abstract class AbstractRedaktFamilijaHandler : EntityEditorViewModelHandler<Bars.Nedragis.Familija, RedaktFamilijaModel>, IRedaktFamilijaHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Редакт_Фамилия'
    /// </summary>
    public class RedaktFamilijaViewModel : BaseEditorViewModel<Bars.Nedragis.Familija, RedaktFamilijaModel>, IRedaktFamilijaViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override RedaktFamilijaModel CreateModelInternal(BaseParams @params)
        {
            var model = new RedaktFamilijaModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Фамилия' в модель представления
        /// </summary>
        protected override RedaktFamilijaModel MapEntityInternal(Bars.Nedragis.Familija entity)
        {
            // создаем экзепляр модели
            var model = new RedaktFamilijaModel();
            model.Id = entity.Id;
            model.Surname = (System.String)(entity.Surname);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Фамилия' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Familija entity, RedaktFamilijaModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Surname = model.Surname;
        }
    }
}