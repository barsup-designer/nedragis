namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования табл1' для отдачи на клиент
    /// </summary>
    public class Tabl1EditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public Tabl1EditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Вид работ'
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("f6fac313-b74e-41d4-a21b-6393f0390350")]
        public virtual Bars.Nedragis.VidRabot? vid_r
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'count'
        /// </summary>
        [Bars.B4.Utils.Display("count")]
        [Bars.Rms.Core.Attributes.Uid("6cb9d256-9031-4f1e-ad68-9e4e38116768")]
        public virtual System.Int32? count
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'rowid'
        /// </summary>
        [Bars.B4.Utils.Display("rowid")]
        [Bars.Rms.Core.Attributes.Uid("238348b3-3a2c-4c6e-b1ad-1cb9da5b6d9a")]
        public virtual System.Int64? rowid
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'sum_DS'
        /// </summary>
        [Bars.B4.Utils.Display("sum_DS")]
        [Bars.Rms.Core.Attributes.Uid("59266e1e-aba1-47ac-a104-d1dd8d1359c3")]
        public virtual System.Decimal? sum_DS
        {
            get;
            set;
        }

        /// <summary>
        /// kok
        /// </summary>
        [Bars.B4.Utils.Display("kok")]
        [Bars.Rms.Core.Attributes.Uid("4156e5e2-e453-4e82-958e-5b8dffdd0422")]
        public virtual Bars.Nedragis.VidPravaListModel kok
        {
            get;
            set;
        }
    }
}