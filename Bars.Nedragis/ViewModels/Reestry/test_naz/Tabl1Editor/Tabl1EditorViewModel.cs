namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования табл1'
    /// </summary>
    public interface ITabl1EditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Tabl1, Tabl1EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования табл1'
    /// </summary>
    public interface ITabl1EditorValidator : IEditorModelValidator<Tabl1EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования табл1'
    /// </summary>
    public interface ITabl1EditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Tabl1, Tabl1EditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования табл1'
    /// </summary>
    public abstract class AbstractTabl1EditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Tabl1, Tabl1EditorModel>, ITabl1EditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования табл1'
    /// </summary>
    public class Tabl1EditorViewModel : BaseEditorViewModel<Bars.Nedragis.Tabl1, Tabl1EditorModel>, ITabl1EditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override Tabl1EditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new Tabl1EditorModel();
            model.vid_r = (Bars.Nedragis.VidRabot)1;
            var varkokId = @params.Params.GetAs<long>("kok_Id", 0);
            if (varkokId > 0)
            {
                model.kok = Container.Resolve<Bars.Nedragis.IVidPravaListQuery>().GetById(varkokId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'табл1' в модель представления
        /// </summary>
        protected override Tabl1EditorModel MapEntityInternal(Bars.Nedragis.Tabl1 entity)
        {
            // создаем экзепляр модели
            var model = new Tabl1EditorModel();
            model.Id = entity.Id;
            model.vid_r = (Bars.Nedragis.VidRabot? )(entity.vid_r);
            model.count = (System.Int32? )(entity.count);
            model.rowid = (System.Int64? )(entity.rowid);
            model.sum_DS = (System.Decimal? )(entity.sum_DS);
            if (entity.kok.IsNotNull())
            {
                var queryVidPravaList = Container.Resolve<Bars.Nedragis.IVidPravaListQuery>();
                model.kok = queryVidPravaList.GetById(entity.kok.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'табл1' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Tabl1 entity, Tabl1EditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.vid_r.HasValue)
            {
                entity.vid_r = model.vid_r.Value;
            }
            else
            {
                entity.vid_r = null;
            }

            if (model.count.HasValue)
            {
                entity.count = model.count.Value;
            }

            entity.rowid = model.rowid;
            if (model.sum_DS.HasValue)
            {
                entity.sum_DS = model.sum_DS.Value;
            }

            entity.kok = TryLoadEntityById<Bars.Nedragis.VidPrava>(model.kok?.Id);
        }
    }
}