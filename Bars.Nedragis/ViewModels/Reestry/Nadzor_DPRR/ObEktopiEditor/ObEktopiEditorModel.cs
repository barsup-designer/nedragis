namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования объектопи' для отдачи на клиент
    /// </summary>
    public class ObEktopiEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public ObEktopiEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Объект
        /// </summary>
        [Bars.B4.Utils.Display("Объект")]
        [Bars.Rms.Core.Attributes.Uid("8d036332-5713-4a27-97c5-9e89201e4c60")]
        public virtual Bars.Nedragis.ReestrObEktovListModel Element1508136885094
        {
            get;
            set;
        }

        /// <summary>
        /// опи
        /// </summary>
        [Bars.B4.Utils.Display("опи")]
        [Bars.Rms.Core.Attributes.Uid("1082c1bc-f3e7-4e5e-bf8e-cffd1bb46782")]
        public virtual Bars.Nedragis.RegGosNadzorZaGeoListModel Element1508137003934
        {
            get;
            set;
        }
    }
}