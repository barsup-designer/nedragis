namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования объектопи'
    /// </summary>
    public interface IObEktopiEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.ObEktopi, ObEktopiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования объектопи'
    /// </summary>
    public interface IObEktopiEditorValidator : IEditorModelValidator<ObEktopiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования объектопи'
    /// </summary>
    public interface IObEktopiEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.ObEktopi, ObEktopiEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования объектопи'
    /// </summary>
    public abstract class AbstractObEktopiEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.ObEktopi, ObEktopiEditorModel>, IObEktopiEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования объектопи'
    /// </summary>
    public class ObEktopiEditorViewModel : BaseEditorViewModel<Bars.Nedragis.ObEktopi, ObEktopiEditorModel>, IObEktopiEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ObEktopiEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new ObEktopiEditorModel();
            var varElement1508136885094Id = @params.Params.GetAs<long>("Element1508136885094_Id", 0);
            if (varElement1508136885094Id > 0)
            {
                model.Element1508136885094 = Container.Resolve<Bars.Nedragis.IReestrObEktovListQuery>().GetById(varElement1508136885094Id);
            }

            var varElement1508137003934Id = @params.Params.GetAs<long>("Element1508137003934_Id", 0);
            if (varElement1508137003934Id > 0)
            {
                model.Element1508137003934 = Container.Resolve<Bars.Nedragis.IRegGosNadzorZaGeoListQuery>().GetById(varElement1508137003934Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'объектопи' в модель представления
        /// </summary>
        protected override ObEktopiEditorModel MapEntityInternal(Bars.Nedragis.ObEktopi entity)
        {
            // создаем экзепляр модели
            var model = new ObEktopiEditorModel();
            model.Id = entity.Id;
            if (entity.Element1508136885094.IsNotNull())
            {
                var queryReestrObEktovList = Container.Resolve<Bars.Nedragis.IReestrObEktovListQuery>();
                model.Element1508136885094 = queryReestrObEktovList.GetById(entity.Element1508136885094.Id);
            }

            if (entity.Element1508137003934.IsNotNull())
            {
                var queryRegGosNadzorZaGeoList = Container.Resolve<Bars.Nedragis.IRegGosNadzorZaGeoListQuery>();
                model.Element1508137003934 = queryRegGosNadzorZaGeoList.GetById(entity.Element1508137003934.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'объектопи' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.ObEktopi entity, ObEktopiEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1508136885094 = TryLoadEntityById<Bars.Nedragis.ReestrObEktov>(model.Element1508136885094?.Id);
            entity.Element1508137003934 = TryLoadEntityById<Bars.Nedragis.RegGosNadzorZaGeo>(model.Element1508137003934?.Id);
        }
    }
}