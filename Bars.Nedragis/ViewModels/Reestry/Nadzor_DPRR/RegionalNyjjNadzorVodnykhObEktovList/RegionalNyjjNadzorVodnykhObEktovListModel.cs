namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Региональный надзор водных объектов'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Региональный надзор водных объектов")]
    public class RegionalNyjjNadzorVodnykhObEktovListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер договора' (псевдоним: dognumber13)
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("36ab2186-5aed-4424-85c4-fcf00c7d8ff2")]
        public virtual System.String dognumber13
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата регистрации' (псевдоним: dogdate13)
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистрации")]
        [Bars.Rms.Core.Attributes.Uid("c4e980c2-44b1-494b-bfba-3da4c72662b5")]
        public virtual System.DateTime? dogdate13
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Окончание срока действия' (псевдоним: dogdateend13)
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия")]
        [Bars.Rms.Core.Attributes.Uid("62ac8c77-8e24-4e38-aa99-6112ed328f90")]
        public virtual System.DateTime? dogdateend13
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Местонахождение' (псевдоним: dogmesto13)
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("4d195bd7-d123-44e7-b000-5be194aafd5d")]
        public virtual System.String dogmesto13
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}