namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Региональный надзор водных объектов'
    /// </summary>
    public interface IRegionalNyjjNadzorVodnykhObEktovListQuery : IQueryOperation<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov, Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktovListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Региональный надзор водных объектов'
    /// </summary>
    public interface IRegionalNyjjNadzorVodnykhObEktovListQueryFilter : IQueryOperationFilter<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov, Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktovListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Региональный надзор водных объектов'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Региональный надзор водных объектов")]
    public class RegionalNyjjNadzorVodnykhObEktovListQuery : RmsEntityQueryOperation<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov, Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktovListModel>, IRegionalNyjjNadzorVodnykhObEktovListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "RegionalNyjjNadzorVodnykhObEktovListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public RegionalNyjjNadzorVodnykhObEktovListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov> Filter(IQueryable<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktovListModel> Map(IQueryable<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktovListModel{Id = x.Id, _TypeUid = "8d4cd756-d656-4caa-805b-242b484000f7", dognumber13 = (System.String)(x.Element1507787714216), dogdate13 = (System.DateTime? )(x.Element1507787771917), dogdateend13 = (System.DateTime? )(x.Element1507787816822), dogmesto13 = (System.String)(x.Element1507787841624), });
        }
    }
}