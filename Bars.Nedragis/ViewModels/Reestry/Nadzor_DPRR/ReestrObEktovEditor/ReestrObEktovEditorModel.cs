namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Реестр объектов' для отдачи на клиент
    /// </summary>
    public class ReestrObEktovEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public ReestrObEktovEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер по порядку'
        /// </summary>
        [Bars.B4.Utils.Display("Номер по порядку")]
        [Bars.Rms.Core.Attributes.Uid("17fcb539-7acf-4f48-8cfb-12b62076e1cf")]
        public virtual System.Int32? Element1507784366035
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование субъектов надзора
        /// </summary>
        [Bars.B4.Utils.Display("Наименование субъектов надзора")]
        [Bars.Rms.Core.Attributes.Uid("1cc7b218-b70e-4c21-b94f-658d4e6cd515")]
        public virtual Bars.Nedragis.ReestrNaimenovanieObEktovNadzoraModel Element1507784253669
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'ИНН'
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("158bc50f-a333-43fd-91df-572dd65090e8")]
        public virtual System.Int32? Element1507784343465
        {
            get;
            set;
        }
    }
}