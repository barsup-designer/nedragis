namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Реестр объектов'
    /// </summary>
    public interface IReestrObEktovEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.ReestrObEktov, ReestrObEktovEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Реестр объектов'
    /// </summary>
    public interface IReestrObEktovEditorValidator : IEditorModelValidator<ReestrObEktovEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Реестр объектов'
    /// </summary>
    public interface IReestrObEktovEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.ReestrObEktov, ReestrObEktovEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Реестр объектов'
    /// </summary>
    public abstract class AbstractReestrObEktovEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.ReestrObEktov, ReestrObEktovEditorModel>, IReestrObEktovEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Реестр объектов'
    /// </summary>
    public class ReestrObEktovEditorViewModel : BaseEditorViewModel<Bars.Nedragis.ReestrObEktov, ReestrObEktovEditorModel>, IReestrObEktovEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ReestrObEktovEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new ReestrObEktovEditorModel();
            var varElement1507784253669Id = @params.Params.GetAs<long>("Element1507784253669_Id", 0);
            if (varElement1507784253669Id > 0)
            {
                model.Element1507784253669 = Container.Resolve<Bars.Nedragis.IReestrNaimenovanieObEktovNadzoraQuery>().GetById(varElement1507784253669Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Реестр объектов' в модель представления
        /// </summary>
        protected override ReestrObEktovEditorModel MapEntityInternal(Bars.Nedragis.ReestrObEktov entity)
        {
            // создаем экзепляр модели
            var model = new ReestrObEktovEditorModel();
            model.Id = entity.Id;
            model.Element1507784366035 = (System.Int32? )(entity.Element1507784366035);
            if (entity.Element1507784253669.IsNotNull())
            {
                var queryReestrNaimenovanieObEktovNadzora = Container.Resolve<Bars.Nedragis.IReestrNaimenovanieObEktovNadzoraQuery>();
                model.Element1507784253669 = queryReestrNaimenovanieObEktovNadzora.GetById(entity.Element1507784253669.Id);
            }

            model.Element1507784343465 = (System.Int32? )(entity.Element1507784343465);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Реестр объектов' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.ReestrObEktov entity, ReestrObEktovEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.Element1507784366035.HasValue)
            {
                entity.Element1507784366035 = model.Element1507784366035.Value;
            }

            entity.Element1507784253669 = TryLoadEntityById<Bars.Nedragis.NaimenovanieObEktovNadzora>(model.Element1507784253669?.Id);
            if (model.Element1507784343465.HasValue)
            {
                entity.Element1507784343465 = model.Element1507784343465.Value;
            }
        }
    }
}