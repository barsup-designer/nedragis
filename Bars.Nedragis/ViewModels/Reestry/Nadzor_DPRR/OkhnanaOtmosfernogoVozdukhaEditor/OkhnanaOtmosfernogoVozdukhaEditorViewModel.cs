namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Охрана атмосферного воздуха'
    /// </summary>
    public interface IOkhnanaOtmosfernogoVozdukhaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.OkhnanaOtmosfernogoVozdukha, OkhnanaOtmosfernogoVozdukhaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Охрана атмосферного воздуха'
    /// </summary>
    public interface IOkhnanaOtmosfernogoVozdukhaEditorValidator : IEditorModelValidator<OkhnanaOtmosfernogoVozdukhaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Охрана атмосферного воздуха'
    /// </summary>
    public interface IOkhnanaOtmosfernogoVozdukhaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.OkhnanaOtmosfernogoVozdukha, OkhnanaOtmosfernogoVozdukhaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Охрана атмосферного воздуха'
    /// </summary>
    public abstract class AbstractOkhnanaOtmosfernogoVozdukhaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.OkhnanaOtmosfernogoVozdukha, OkhnanaOtmosfernogoVozdukhaEditorModel>, IOkhnanaOtmosfernogoVozdukhaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Охрана атмосферного воздуха'
    /// </summary>
    public class OkhnanaOtmosfernogoVozdukhaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.OkhnanaOtmosfernogoVozdukha, OkhnanaOtmosfernogoVozdukhaEditorModel>, IOkhnanaOtmosfernogoVozdukhaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OkhnanaOtmosfernogoVozdukhaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OkhnanaOtmosfernogoVozdukhaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Воздух' в модель представления
        /// </summary>
        protected override OkhnanaOtmosfernogoVozdukhaEditorModel MapEntityInternal(Bars.Nedragis.OkhnanaOtmosfernogoVozdukha entity)
        {
            // создаем экзепляр модели
            var model = new OkhnanaOtmosfernogoVozdukhaEditorModel();
            model.Id = entity.Id;
            model.Element1507788951680 = (System.String)(entity.Element1507788951680);
            model.Element1507791810720 = (System.String)(entity.Element1507791810720);
            model.Element1507788999087 = (System.String)(entity.Element1507788999087);
            model.Element1507789018715 = (System.String)(entity.Element1507789018715);
            model.Element1507789044428 = (System.String)(entity.Element1507789044428);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Воздух' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.OkhnanaOtmosfernogoVozdukha entity, OkhnanaOtmosfernogoVozdukhaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1507788951680 = model.Element1507788951680;
            entity.Element1507791810720 = model.Element1507791810720;
            entity.Element1507788999087 = model.Element1507788999087;
            entity.Element1507789018715 = model.Element1507789018715;
            entity.Element1507789044428 = model.Element1507789044428;
        }
    }
}