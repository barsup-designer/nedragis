namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Охрана атмосферного воздуха' для отдачи на клиент
    /// </summary>
    public class OkhnanaOtmosfernogoVozdukhaEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public OkhnanaOtmosfernogoVozdukhaEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Название стационарного источника загрязнения'
        /// </summary>
        [Bars.B4.Utils.Display("Название стационарного источника загрязнения")]
        [Bars.Rms.Core.Attributes.Uid("599da98d-0d67-473d-91cd-1f4fae31ec6f")]
        public virtual System.String Element1507788951680
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Код свидетельства'
        /// </summary>
        [Bars.B4.Utils.Display("Код свидетельства")]
        [Bars.Rms.Core.Attributes.Uid("677e52fd-ab57-4f7a-a9bc-ca9676d6b551")]
        public virtual System.String Element1507791810720
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Местонахождение'
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("0f022cd0-2a82-424d-a9b2-3b31dabab0e5")]
        public virtual System.String Element1507788999087
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наличие тома ПДВ'
        /// </summary>
        [Bars.B4.Utils.Display("Наличие тома ПДВ")]
        [Bars.Rms.Core.Attributes.Uid("d3eefb82-f495-47f3-9ca9-2833824d8e08")]
        public virtual System.String Element1507789018715
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Разрешение на выброс в атмосферу'
        /// </summary>
        [Bars.B4.Utils.Display("Разрешение на выброс в атмосферу")]
        [Bars.Rms.Core.Attributes.Uid("cae99da1-fcbe-4d8c-b184-1c84a5132070")]
        public virtual System.String Element1507789044428
        {
            get;
            set;
        }
    }
}