namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Отходы'
    /// </summary>
    public interface IOtkhodyListQuery : IQueryOperation<Bars.Nedragis.Otkhody, Bars.Nedragis.OtkhodyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Отходы'
    /// </summary>
    public interface IOtkhodyListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Otkhody, Bars.Nedragis.OtkhodyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Отходы'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Отходы")]
    public class OtkhodyListQuery : RmsEntityQueryOperation<Bars.Nedragis.Otkhody, Bars.Nedragis.OtkhodyListModel>, IOtkhodyListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OtkhodyListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OtkhodyListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Otkhody> Filter(IQueryable<Bars.Nedragis.Otkhody> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OtkhodyListModel> Map(IQueryable<Bars.Nedragis.Otkhody> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Otkhody>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OtkhodyListModel{Id = x.Id, _TypeUid = "07fadf5a-6010-4fa1-8cf6-39f3e5cf7590", Element1507790021761 = (System.String)(x.Element1507790021761), Element1507791662904 = (System.String)(x.Element1507791662904), Element1507791696742 = (System.String)(x.Element1507791696742), });
        }
    }
}