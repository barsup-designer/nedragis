namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Реестр объектов'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Реестр объектов")]
    public class ReestrObEktovListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер по порядку' (псевдоним: objectnamber12)
        /// </summary>
        [Bars.B4.Utils.Display("Номер по порядку")]
        [Bars.Rms.Core.Attributes.Uid("cac6456b-ef00-4e4d-8ad0-892ac1966491")]
        public virtual System.Int32? objectnamber12
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование субъектов надзора.Наименование' (псевдоним: objectname12)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование субъектов надзора.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("5d94bbb3-a0b4-43cd-852d-f67452ed1759")]
        public virtual System.String objectname12
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'ИНН' (псевдоним: objectinn12)
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("fc085bba-d168-441d-be1c-0ee5bdff63a3")]
        public virtual System.Int32? objectinn12
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'NumberTest' (псевдоним: NumberTest24adcf55-df18-4f4b-bfc8-952c7687d0ad)
        /// </summary>
        [Bars.B4.Utils.Display("NumberTest")]
        public virtual System.Int64 NumberTest24adcf55df184f4bbfc8952c7687d0ad
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}