namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Реестр объектов'
    /// </summary>
    public interface IReestrObEktovListQuery : IQueryOperation<Bars.Nedragis.ReestrObEktov, Bars.Nedragis.ReestrObEktovListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Реестр объектов'
    /// </summary>
    public interface IReestrObEktovListQueryFilter : IQueryOperationFilter<Bars.Nedragis.ReestrObEktov, Bars.Nedragis.ReestrObEktovListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Реестр объектов'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Реестр объектов")]
    public class ReestrObEktovListQuery : RmsEntityQueryOperation<Bars.Nedragis.ReestrObEktov, Bars.Nedragis.ReestrObEktovListModel>, IReestrObEktovListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "ReestrObEktovListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public ReestrObEktovListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.ReestrObEktov> Filter(IQueryable<Bars.Nedragis.ReestrObEktov> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.ReestrObEktovListModel> Map(IQueryable<Bars.Nedragis.ReestrObEktov> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.ReestrObEktov>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.ReestrObEktovListModel{Id = x.Id, _TypeUid = "69f498ef-df00-409a-a699-c742d843e803", objectnamber12 = (System.Int32? )(x.Element1507784366035), objectname12 = (System.String)(x.Element1507784253669.Element1507203167462), objectinn12 = (System.Int32? )(x.Element1507784343465), NumberTest24adcf55df184f4bbfc8952c7687d0ad = SqlFunctions.NumberTest(x.Element1507784366035), });
        }
    }
}