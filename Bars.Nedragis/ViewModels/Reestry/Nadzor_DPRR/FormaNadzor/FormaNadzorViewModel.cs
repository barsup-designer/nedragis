namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Надзор'
    /// </summary>
    public interface IFormaNadzorViewModel : IEntityEditorViewModel<Bars.Nedragis.Nadzor, FormaNadzorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Надзор'
    /// </summary>
    public interface IFormaNadzorValidator : IEditorModelValidator<FormaNadzorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Надзор'
    /// </summary>
    public interface IFormaNadzorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Nadzor, FormaNadzorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Надзор'
    /// </summary>
    public abstract class AbstractFormaNadzorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Nadzor, FormaNadzorModel>, IFormaNadzorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Надзор'
    /// </summary>
    public class FormaNadzorViewModel : BaseEditorViewModel<Bars.Nedragis.Nadzor, FormaNadzorModel>, IFormaNadzorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override FormaNadzorModel CreateModelInternal(BaseParams @params)
        {
            var model = new FormaNadzorModel();
            var varElement1507271571833Id = @params.Params.GetAs<long>("Element1507271571833_Id", 0);
            if (varElement1507271571833Id > 0)
            {
                model.Element1507271571833 = Container.Resolve<Bars.Nedragis.IReestrNaimenovanieObEktovNadzoraQuery>().GetById(varElement1507271571833Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Объекты регионального надзора' в модель представления
        /// </summary>
        protected override FormaNadzorModel MapEntityInternal(Bars.Nedragis.Nadzor entity)
        {
            // создаем экзепляр модели
            var model = new FormaNadzorModel();
            model.Id = entity.Id;
            model.Element1507619429093 = (System.Int32? )(entity.Element1507619429093);
            if (entity.Element1507271571833.IsNotNull())
            {
                var queryReestrNaimenovanieObEktovNadzora = Container.Resolve<Bars.Nedragis.IReestrNaimenovanieObEktovNadzoraQuery>();
                model.Element1507271571833 = queryReestrNaimenovanieObEktovNadzora.GetById(entity.Element1507271571833.Id);
            }

            model.Element1507611045539 = (System.String)(entity.Element1507611045539);
            model.inn = (System.Int64? )(entity.inn);
            model.Element1507010532348 = (System.String)(entity.Element1507010532348);
            model.date = (System.DateTime? )(entity.date);
            model.srok_end = (System.DateTime? )(entity.srok_end);
            model.mesto = (System.String)(entity.mesto);
            model.Element1507703919662 = (System.String)(entity.Element1507703919662);
            model.Element1507262033494 = (System.DateTime? )(entity.Element1507262033494);
            model.Element1507262072161 = (System.DateTime? )(entity.Element1507262072161);
            model.Element1507262105889 = (System.String)(entity.Element1507262105889);
            model.Element1507262162260 = (System.String)(entity.Element1507262162260);
            model.Element1507262266009 = (System.String)(entity.Element1507262266009);
            model.Element1507262301335 = (System.String)(entity.Element1507262301335);
            model.Element1507262339156 = (System.String)(entity.Element1507262339156);
            model.Element1507262414231 = (System.String)(entity.Element1507262414231);
            model.Element1507262455649 = (System.String)(entity.Element1507262455649);
            model.Element1507262379380 = (System.String)(entity.Element1507262379380);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Объекты регионального надзора' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Nadzor entity, FormaNadzorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.Element1507619429093.HasValue)
            {
                entity.Element1507619429093 = model.Element1507619429093.Value;
            }

            entity.Element1507271571833 = TryLoadEntityById<Bars.Nedragis.NaimenovanieObEktovNadzora>(model.Element1507271571833?.Id);
            entity.Element1507611045539 = model.Element1507611045539;
            if (model.inn.HasValue)
            {
                entity.inn = model.inn.Value;
            }

            entity.Element1507010532348 = model.Element1507010532348;
            entity.date = model.date.GetValueOrDefault();
            entity.srok_end = model.srok_end;
            entity.mesto = model.mesto;
            entity.Element1507703919662 = model.Element1507703919662;
            entity.Element1507262033494 = model.Element1507262033494.GetValueOrDefault();
            entity.Element1507262072161 = model.Element1507262072161.GetValueOrDefault();
            entity.Element1507262105889 = model.Element1507262105889;
            entity.Element1507262162260 = model.Element1507262162260;
            entity.Element1507262266009 = model.Element1507262266009;
            entity.Element1507262301335 = model.Element1507262301335;
            entity.Element1507262339156 = model.Element1507262339156;
            entity.Element1507262414231 = model.Element1507262414231;
            entity.Element1507262455649 = model.Element1507262455649;
            entity.Element1507262379380 = model.Element1507262379380;
        }
    }
}