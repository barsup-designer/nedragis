namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Надзор' для отдачи на клиент
    /// </summary>
    public class FormaNadzorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public FormaNadzorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер'
        /// </summary>
        [Bars.B4.Utils.Display("Номер")]
        [Bars.Rms.Core.Attributes.Uid("18ecbcd9-eb50-4402-99aa-6f7ab18dc74a")]
        public virtual System.Int32? Element1507619429093
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование субъекта надзора
        /// </summary>
        [Bars.B4.Utils.Display("Наименование субъекта надзора")]
        [Bars.Rms.Core.Attributes.Uid("4ccdc9b2-4cf2-4b4a-ba22-d4bacb9275b2")]
        public virtual Bars.Nedragis.ReestrNaimenovanieObEktovNadzoraModel Element1507271571833
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Код свидетельства'
        /// </summary>
        [Bars.B4.Utils.Display("Код свидетельства")]
        [Bars.Rms.Core.Attributes.Uid("d27a9356-6d24-4d36-9baa-b53dfede45b5")]
        public virtual System.String Element1507611045539
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'ИНН'
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("5a3dfdfa-b02c-43ee-b1ac-d31c397c02ba")]
        public virtual System.Int64? inn
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер Лицензии'
        /// </summary>
        [Bars.B4.Utils.Display("Номер Лицензии")]
        [Bars.Rms.Core.Attributes.Uid("3768cab9-df03-47ab-b0b5-e54043749f48")]
        public virtual System.String Element1507010532348
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата регистации'
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистации")]
        [Bars.Rms.Core.Attributes.Uid("74f99050-3f36-4c8b-a29d-428dd519f0b1")]
        public virtual System.DateTime? date
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Окончание срока действия'
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия")]
        [Bars.Rms.Core.Attributes.Uid("dd296446-ee41-4714-a88b-8193bee998de")]
        public virtual System.DateTime? srok_end
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Местонахождение'
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("08b32b21-d6a6-4c2c-83f6-0dce5bf37d47")]
        public virtual System.String mesto
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Договор водопользования'
        /// </summary>
        [Bars.B4.Utils.Display("Договор водопользования")]
        [Bars.Rms.Core.Attributes.Uid("0b9988ea-2187-4186-b0af-75592f39aadc")]
        public virtual System.String Element1507703919662
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата регистрации договора'
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистрации договора")]
        [Bars.Rms.Core.Attributes.Uid("70c56e25-c4b1-466c-8be4-e6f2f9cdf31c")]
        public virtual System.DateTime? Element1507262033494
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Окончание срока действия договора'
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия договора")]
        [Bars.Rms.Core.Attributes.Uid("e3396cb2-bca6-4d42-9563-71947f16b74c")]
        public virtual System.DateTime? Element1507262072161
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Местоположение'
        /// </summary>
        [Bars.B4.Utils.Display("Местоположение")]
        [Bars.Rms.Core.Attributes.Uid("e31cb04c-6b63-4e91-bd5f-825014e10788")]
        public virtual System.String Element1507262105889
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование стационарного источника загрязнения'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование стационарного источника загрязнения")]
        [Bars.Rms.Core.Attributes.Uid("f6b15c5c-a3ef-4d8b-baf3-697993883611")]
        public virtual System.String Element1507262162260
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Местонахождение'
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("3316e8da-b65d-4fa5-bde7-f3f10c186d70")]
        public virtual System.String Element1507262266009
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наличие тома ПДВ'
        /// </summary>
        [Bars.B4.Utils.Display("Наличие тома ПДВ")]
        [Bars.Rms.Core.Attributes.Uid("4ce5111e-eaa2-40ac-a796-f61a953b110f")]
        public virtual System.String Element1507262301335
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Разрешение на выброс в атмосферу'
        /// </summary>
        [Bars.B4.Utils.Display("Разрешение на выброс в атмосферу")]
        [Bars.Rms.Core.Attributes.Uid("eebc0a22-deb5-4577-9bf9-d89ce5e94fe1")]
        public virtual System.String Element1507262339156
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Местонахождение'
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("0d71b970-884d-4ff6-85be-a385521e3684")]
        public virtual System.String Element1507262414231
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Проект нормативов образования отходов и лимитов на их размещение'
        /// </summary>
        [Bars.B4.Utils.Display("Проект нормативов образования отходов и лимитов на их размещение")]
        [Bars.Rms.Core.Attributes.Uid("0fd09144-47c6-4995-9cfc-7e7298487558")]
        public virtual System.String Element1507262455649
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование объекта образования отходов'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование объекта образования отходов")]
        [Bars.Rms.Core.Attributes.Uid("7f90303d-a0cf-4fbe-aa0a-944bac64b849")]
        public virtual System.String Element1507262379380
        {
            get;
            set;
        }
    }
}