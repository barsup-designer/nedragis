namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования ОПИ2' для отдачи на клиент
    /// </summary>
    public class RegGosNadzorZaGeoEditor2Model : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public RegGosNadzorZaGeoEditor2Model()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер лицензии'
        /// </summary>
        [Bars.B4.Utils.Display("Номер лицензии")]
        [Bars.Rms.Core.Attributes.Uid("8c484782-6b37-4a14-8f9e-d41975e8a0df")]
        public virtual System.String Element1507635501632
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата регистрации'
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистрации")]
        [Bars.Rms.Core.Attributes.Uid("87176540-1bac-454f-a424-667fb657dbb6")]
        public virtual System.DateTime? Element1507635537755
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Окончание срока действия'
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия")]
        [Bars.Rms.Core.Attributes.Uid("8542b1b3-2992-4916-8401-fa81eb76b58a")]
        public virtual System.DateTime? Element1507635559781
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Местонаходжение'
        /// </summary>
        [Bars.B4.Utils.Display("Местонаходжение")]
        [Bars.Rms.Core.Attributes.Uid("ac8e46cc-ecbd-4b0c-b969-f40cca78f534")]
        public virtual System.String Element1507635586209
        {
            get;
            set;
        }
    }
}