namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр объектопи'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр объектопи")]
    public class ObEktopiListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Объект.Наименование субъектов надзора.Наименование' (псевдоним: objectnamereest16)
        /// </summary>
        [Bars.B4.Utils.Display("Объект.Наименование субъектов надзора.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("f42441ed-59c2-4f9d-9bbf-80e0d18f9473")]
        public virtual System.String objectnamereest16
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Объект.ИНН' (псевдоним: objectinnreestr16)
        /// </summary>
        [Bars.B4.Utils.Display("Объект.ИНН")]
        [Bars.Rms.Core.Attributes.Uid("3328c592-98b4-487e-8945-1fb5384f29a9")]
        public virtual System.Int32? objectinnreestr16
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Объект.Номер по порядку' (псевдоним: objectn16)
        /// </summary>
        [Bars.B4.Utils.Display("Объект.Номер по порядку")]
        [Bars.Rms.Core.Attributes.Uid("adab0974-cf2c-4f49-8ddb-441c4696ba6c")]
        public virtual System.Int32? objectn16
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'опи.Идентификатор' (псевдоним: opireestr16)
        /// </summary>
        [Bars.B4.Utils.Display("опи.Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("579ff6d6-98db-4349-b2b3-d25d2aa3eb6c")]
        public virtual System.Int64? opireestr16
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}