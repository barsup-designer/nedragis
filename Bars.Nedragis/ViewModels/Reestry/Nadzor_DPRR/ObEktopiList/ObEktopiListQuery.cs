namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр объектопи'
    /// </summary>
    public interface IObEktopiListQuery : IQueryOperation<Bars.Nedragis.ObEktopi, Bars.Nedragis.ObEktopiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр объектопи'
    /// </summary>
    public interface IObEktopiListQueryFilter : IQueryOperationFilter<Bars.Nedragis.ObEktopi, Bars.Nedragis.ObEktopiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр объектопи'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр объектопи")]
    public class ObEktopiListQuery : RmsEntityQueryOperation<Bars.Nedragis.ObEktopi, Bars.Nedragis.ObEktopiListModel>, IObEktopiListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "ObEktopiListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public ObEktopiListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.ObEktopi> Filter(IQueryable<Bars.Nedragis.ObEktopi> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.ObEktopiListModel> Map(IQueryable<Bars.Nedragis.ObEktopi> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.ObEktopi>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.ObEktopiListModel{Id = x.Id, _TypeUid = "2fd9e43e-bd97-4446-a65e-77f22758149b", objectnamereest16 = (System.String)(x.Element1508136885094.Element1507784253669.Element1507203167462), objectinnreestr16 = (System.Int32? )(x.Element1508136885094.Element1507784343465), objectn16 = (System.Int32? )(x.Element1508136885094.Element1507784366035), opireestr16 = (System.Int64? )(x.Element1508137003934.Id), });
        }
    }
}