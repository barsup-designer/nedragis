namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр объектов регионального надзора ДПРР ЯНАО'
    /// </summary>
    public interface IReestrNadzorQuery : IQueryOperation<Bars.Nedragis.Nadzor, Bars.Nedragis.ReestrNadzorModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр объектов регионального надзора ДПРР ЯНАО'
    /// </summary>
    public interface IReestrNadzorQueryFilter : IQueryOperationFilter<Bars.Nedragis.Nadzor, Bars.Nedragis.ReestrNadzorModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр объектов регионального надзора ДПРР ЯНАО'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр объектов регионального надзора ДПРР ЯНАО")]
    public class ReestrNadzorQuery : RmsEntityQueryOperation<Bars.Nedragis.Nadzor, Bars.Nedragis.ReestrNadzorModel>, IReestrNadzorQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "ReestrNadzorQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public ReestrNadzorQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Nadzor> Filter(IQueryable<Bars.Nedragis.Nadzor> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.ReestrNadzorModel> Map(IQueryable<Bars.Nedragis.Nadzor> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Nadzor>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.ReestrNadzorModel{Id = x.Id, _TypeUid = "ad4d820c-37fc-4e0a-98a5-ea89b5edcc9b", nomerpoprc = (System.Int32? )(x.Element1507619429093), subjectnadzorann = (System.String)(x.Element1507271571833.Element1507203167462), reestrkodsv = (System.String)(x.Element1507611045539), innr = (System.Int64? )(x.inn), licenseNumber = (System.String)(x.Element1507010532348), dater = (System.DateTime? )(x.date), srok_endr = (System.DateTime? )(x.srok_end), mestor = (System.String)(x.mesto), reesrdogvod1 = (System.String)(x.Element1507703919662), dogovorDataregisrt = (System.DateTime? )(x.Element1507262033494), dogovorDataend = (System.DateTime? )(x.Element1507262072161), dogovorMestopol = (System.String)(x.Element1507262105889), istochnikzag = (System.String)(x.Element1507262162260), istochnikMestopol = (System.String)(x.Element1507262266009), istochnikTom = (System.String)(x.Element1507262301335), istochnukRaz = (System.String)(x.Element1507262339156), otxName = (System.String)(x.Element1507262379380), otxMestopol = (System.String)(x.Element1507262414231), otxProject = (System.String)(x.Element1507262455649), });
        }
    }
}