namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр объектов регионального надзора ДПРР ЯНАО'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр объектов регионального надзора ДПРР ЯНАО")]
    public class ReestrNadzorModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер' (псевдоним: nomerpoprc)
        /// </summary>
        [Bars.B4.Utils.Display("Номер")]
        [Bars.Rms.Core.Attributes.Uid("09e95fcd-d8c0-4659-897e-afa933737c6b")]
        public virtual System.Int32? nomerpoprc
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование субъекта надзора.Наименование' (псевдоним: subjectnadzorann)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование субъекта надзора.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("cb3263b4-5f81-48ca-b6e9-b8faab87fc20")]
        public virtual System.String subjectnadzorann
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Код свидетельства' (псевдоним: reestrkodsv)
        /// </summary>
        [Bars.B4.Utils.Display("Код свидетельства")]
        [Bars.Rms.Core.Attributes.Uid("4f19e24f-41e7-420c-bc86-207c5f13e79a")]
        public virtual System.String reestrkodsv
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'ИНН' (псевдоним: innr)
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("da041158-661a-4bcd-8254-7e39f565aa3c")]
        public virtual System.Int64? innr
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер Лицензии' (псевдоним: licenseNumber)
        /// </summary>
        [Bars.B4.Utils.Display("Номер Лицензии")]
        [Bars.Rms.Core.Attributes.Uid("30fb93f8-07ca-4020-834d-b17e5be0b42e")]
        public virtual System.String licenseNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата регистации' (псевдоним: dater)
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистации")]
        [Bars.Rms.Core.Attributes.Uid("84c389c7-a5df-4e03-8a92-4170e18c40c5")]
        public virtual System.DateTime? dater
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Окончание срока действия' (псевдоним: srok_endr)
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия")]
        [Bars.Rms.Core.Attributes.Uid("b95e04fc-a28e-442e-bc56-62c46bde490f")]
        public virtual System.DateTime? srok_endr
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Местонахождение' (псевдоним: mestor)
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("e34cbb4a-e319-47b9-b142-cc3178ac6e7d")]
        public virtual System.String mestor
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Договор водопользования' (псевдоним: reesrdogvod1)
        /// </summary>
        [Bars.B4.Utils.Display("Договор водопользования")]
        [Bars.Rms.Core.Attributes.Uid("90ebf995-81b4-4aa8-8456-388361cb442f")]
        public virtual System.String reesrdogvod1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата регистрации договора' (псевдоним: dogovorDataregisrt)
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистрации договора")]
        [Bars.Rms.Core.Attributes.Uid("3274d752-f6e5-4b4a-b411-508fd5922166")]
        public virtual System.DateTime? dogovorDataregisrt
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Окончание срока действия договора' (псевдоним: dogovorDataend)
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия договора")]
        [Bars.Rms.Core.Attributes.Uid("dca2c752-da9a-47e3-8710-7dfa4b4243c6")]
        public virtual System.DateTime? dogovorDataend
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Местоположение' (псевдоним: dogovorMestopol)
        /// </summary>
        [Bars.B4.Utils.Display("Местоположение")]
        [Bars.Rms.Core.Attributes.Uid("ec6e542f-8e57-4570-b0f2-35f32b98fac5")]
        public virtual System.String dogovorMestopol
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование стационарного источника загрязнения' (псевдоним: istochnikzag)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование стационарного источника загрязнения")]
        [Bars.Rms.Core.Attributes.Uid("b847688d-23d3-4774-8d85-97faf7370c4f")]
        public virtual System.String istochnikzag
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Местонахождение' (псевдоним: istochnikMestopol)
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("6e368061-89bc-4a4b-a5d5-85fdb3614ac8")]
        public virtual System.String istochnikMestopol
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наличие тома ПДВ' (псевдоним: istochnikTom)
        /// </summary>
        [Bars.B4.Utils.Display("Наличие тома ПДВ")]
        [Bars.Rms.Core.Attributes.Uid("ccdb5ecc-3718-48db-803d-244a9fc3286b")]
        public virtual System.String istochnikTom
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Разрешение на выброс в атмосферу' (псевдоним: istochnukRaz)
        /// </summary>
        [Bars.B4.Utils.Display("Разрешение на выброс в атмосферу")]
        [Bars.Rms.Core.Attributes.Uid("2fbedc22-45d4-4d2b-a844-196401d95f30")]
        public virtual System.String istochnukRaz
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование объекта образования отходов' (псевдоним: otxName)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование объекта образования отходов")]
        [Bars.Rms.Core.Attributes.Uid("a208d848-5102-44e3-af15-216a0bf65d5f")]
        public virtual System.String otxName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Местонахождение' (псевдоним: otxMestopol)
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("37657b8b-68ff-495e-b029-fe9d35b2d2fd")]
        public virtual System.String otxMestopol
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Проект нормативов образования отходов и лимитов на их размещение' (псевдоним: otxProject)
        /// </summary>
        [Bars.B4.Utils.Display("Проект нормативов образования отходов и лимитов на их размещение")]
        [Bars.Rms.Core.Attributes.Uid("0878794c-dba6-49c8-a50d-0c04a43d758d")]
        public virtual System.String otxProject
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}