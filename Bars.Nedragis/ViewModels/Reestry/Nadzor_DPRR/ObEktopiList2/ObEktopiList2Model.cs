namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр объектопи2'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр объектопи2")]
    public class ObEktopiList2Model
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Объект.Идентификатор' (псевдоним: objectopi17)
        /// </summary>
        [Bars.B4.Utils.Display("Объект.Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("415f4c7f-49d5-4a7b-8073-8c55f915f41c")]
        public virtual System.Int64? objectopi17
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'опи.Номер лицензии' (псевдоним: opinamber17)
        /// </summary>
        [Bars.B4.Utils.Display("опи.Номер лицензии")]
        [Bars.Rms.Core.Attributes.Uid("d8a09f31-5661-4c82-8163-fa55682ebdfe")]
        public virtual System.String opinamber17
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'опи.Дата регистрации' (псевдоним: opidatereest17)
        /// </summary>
        [Bars.B4.Utils.Display("опи.Дата регистрации")]
        [Bars.Rms.Core.Attributes.Uid("c97fdabb-e317-41f3-9402-319f9cee900e")]
        public virtual System.DateTime? opidatereest17
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'опи.Окончание срока действия' (псевдоним: opidateend17)
        /// </summary>
        [Bars.B4.Utils.Display("опи.Окончание срока действия")]
        [Bars.Rms.Core.Attributes.Uid("33c89907-10ac-4076-8b85-cbd6468b7457")]
        public virtual System.DateTime? opidateend17
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'опи.Местонаходжение' (псевдоним: opimesto17)
        /// </summary>
        [Bars.B4.Utils.Display("опи.Местонаходжение")]
        [Bars.Rms.Core.Attributes.Uid("7fea4d67-6abf-4bcc-9db7-8e5c9c4ff576")]
        public virtual System.String opimesto17
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}