namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Региональный надзор водных объектов' для отдачи на клиент
    /// </summary>
    public class RegionalNyjjNadzorVodnykhObEktovEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public RegionalNyjjNadzorVodnykhObEktovEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер договора'
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("c697962c-ac13-4069-a233-adc2146fa259")]
        public virtual System.String Element1507787714216
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата регистрации'
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистрации")]
        [Bars.Rms.Core.Attributes.Uid("7bc2ceeb-670b-4504-8987-b42bf7a017f0")]
        public virtual System.DateTime? Element1507787771917
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Окончание срока действия'
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия")]
        [Bars.Rms.Core.Attributes.Uid("aa813e95-8d47-4812-a841-33cb414b88a4")]
        public virtual System.DateTime? Element1507787816822
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Местонахождение'
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("ad18f575-c9ea-4f29-aa6d-5e96accfa8f3")]
        public virtual System.String Element1507787841624
        {
            get;
            set;
        }
    }
}