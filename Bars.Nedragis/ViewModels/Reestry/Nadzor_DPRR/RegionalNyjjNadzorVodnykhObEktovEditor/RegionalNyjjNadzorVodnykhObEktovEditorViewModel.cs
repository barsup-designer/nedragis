namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Региональный надзор водных объектов'
    /// </summary>
    public interface IRegionalNyjjNadzorVodnykhObEktovEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov, RegionalNyjjNadzorVodnykhObEktovEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Региональный надзор водных объектов'
    /// </summary>
    public interface IRegionalNyjjNadzorVodnykhObEktovEditorValidator : IEditorModelValidator<RegionalNyjjNadzorVodnykhObEktovEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Региональный надзор водных объектов'
    /// </summary>
    public interface IRegionalNyjjNadzorVodnykhObEktovEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov, RegionalNyjjNadzorVodnykhObEktovEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Региональный надзор водных объектов'
    /// </summary>
    public abstract class AbstractRegionalNyjjNadzorVodnykhObEktovEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov, RegionalNyjjNadzorVodnykhObEktovEditorModel>, IRegionalNyjjNadzorVodnykhObEktovEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Региональный надзор водных объектов'
    /// </summary>
    public class RegionalNyjjNadzorVodnykhObEktovEditorViewModel : BaseEditorViewModel<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov, RegionalNyjjNadzorVodnykhObEktovEditorModel>, IRegionalNyjjNadzorVodnykhObEktovEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override RegionalNyjjNadzorVodnykhObEktovEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new RegionalNyjjNadzorVodnykhObEktovEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Вода' в модель представления
        /// </summary>
        protected override RegionalNyjjNadzorVodnykhObEktovEditorModel MapEntityInternal(Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov entity)
        {
            // создаем экзепляр модели
            var model = new RegionalNyjjNadzorVodnykhObEktovEditorModel();
            model.Id = entity.Id;
            model.Element1507787714216 = (System.String)(entity.Element1507787714216);
            model.Element1507787771917 = (System.DateTime? )(entity.Element1507787771917);
            model.Element1507787816822 = (System.DateTime? )(entity.Element1507787816822);
            model.Element1507787841624 = (System.String)(entity.Element1507787841624);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Вода' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov entity, RegionalNyjjNadzorVodnykhObEktovEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1507787714216 = model.Element1507787714216;
            entity.Element1507787771917 = model.Element1507787771917.GetValueOrDefault();
            entity.Element1507787816822 = model.Element1507787816822.GetValueOrDefault();
            entity.Element1507787841624 = model.Element1507787841624;
        }
    }
}