namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Отходы' для отдачи на клиент
    /// </summary>
    public class OtkhodyEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public OtkhodyEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование объекта образования отходов'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование объекта образования отходов")]
        [Bars.Rms.Core.Attributes.Uid("4dcd4047-cb4c-4fd1-8bca-3b0fe52016ec")]
        public virtual System.String Element1507790021761
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Местонахождение'
        /// </summary>
        [Bars.B4.Utils.Display("Местонахождение")]
        [Bars.Rms.Core.Attributes.Uid("bd24bf5b-5952-4747-890d-d085c1715ac7")]
        public virtual System.String Element1507791662904
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Проект нормативов образования отходов и лимитов на их размещение'
        /// </summary>
        [Bars.B4.Utils.Display("Проект нормативов образования отходов и лимитов на их размещение")]
        [Bars.Rms.Core.Attributes.Uid("2c23a81b-7b71-4a50-8a7d-4b27bff96aac")]
        public virtual System.String Element1507791696742
        {
            get;
            set;
        }
    }
}