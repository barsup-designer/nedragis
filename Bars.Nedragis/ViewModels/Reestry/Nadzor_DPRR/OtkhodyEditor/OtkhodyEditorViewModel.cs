namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Отходы'
    /// </summary>
    public interface IOtkhodyEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Otkhody, OtkhodyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Отходы'
    /// </summary>
    public interface IOtkhodyEditorValidator : IEditorModelValidator<OtkhodyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Отходы'
    /// </summary>
    public interface IOtkhodyEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Otkhody, OtkhodyEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Отходы'
    /// </summary>
    public abstract class AbstractOtkhodyEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Otkhody, OtkhodyEditorModel>, IOtkhodyEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Отходы'
    /// </summary>
    public class OtkhodyEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Otkhody, OtkhodyEditorModel>, IOtkhodyEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OtkhodyEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OtkhodyEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Отходы' в модель представления
        /// </summary>
        protected override OtkhodyEditorModel MapEntityInternal(Bars.Nedragis.Otkhody entity)
        {
            // создаем экзепляр модели
            var model = new OtkhodyEditorModel();
            model.Id = entity.Id;
            model.Element1507790021761 = (System.String)(entity.Element1507790021761);
            model.Element1507791662904 = (System.String)(entity.Element1507791662904);
            model.Element1507791696742 = (System.String)(entity.Element1507791696742);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Отходы' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Otkhody entity, OtkhodyEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1507790021761 = model.Element1507790021761;
            entity.Element1507791662904 = model.Element1507791662904;
            entity.Element1507791696742 = model.Element1507791696742;
        }
    }
}