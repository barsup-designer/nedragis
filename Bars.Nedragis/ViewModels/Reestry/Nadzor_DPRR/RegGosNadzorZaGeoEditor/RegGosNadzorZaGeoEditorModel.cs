namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования геология' для отдачи на клиент
    /// </summary>
    public class RegGosNadzorZaGeoEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public RegGosNadzorZaGeoEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер лицензии'
        /// </summary>
        [Bars.B4.Utils.Display("Номер лицензии")]
        [Bars.Rms.Core.Attributes.Uid("ca23be16-3cd5-4592-9f4c-e15ea2ee11bd")]
        public virtual System.String Element1507635501632
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата регистрации'
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистрации")]
        [Bars.Rms.Core.Attributes.Uid("9eeefd2a-cc56-4d04-8d35-4bbcafcdfcbd")]
        public virtual System.DateTime? Element1507635537755
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Окончание срока действия'
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия")]
        [Bars.Rms.Core.Attributes.Uid("443d4eda-1f0b-4e91-95c9-9e1899d5dce3")]
        public virtual System.DateTime? Element1507635559781
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Местонаходжение'
        /// </summary>
        [Bars.B4.Utils.Display("Местонаходжение")]
        [Bars.Rms.Core.Attributes.Uid("727b766c-9198-4d99-b0b7-c351ca8d463e")]
        public virtual System.String Element1507635586209
        {
            get;
            set;
        }
    }
}