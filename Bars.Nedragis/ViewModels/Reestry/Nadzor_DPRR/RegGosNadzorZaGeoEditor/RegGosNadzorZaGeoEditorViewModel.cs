namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования геология'
    /// </summary>
    public interface IRegGosNadzorZaGeoEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.RegGosNadzorZaGeo, RegGosNadzorZaGeoEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования геология'
    /// </summary>
    public interface IRegGosNadzorZaGeoEditorValidator : IEditorModelValidator<RegGosNadzorZaGeoEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования геология'
    /// </summary>
    public interface IRegGosNadzorZaGeoEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.RegGosNadzorZaGeo, RegGosNadzorZaGeoEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования геология'
    /// </summary>
    public abstract class AbstractRegGosNadzorZaGeoEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.RegGosNadzorZaGeo, RegGosNadzorZaGeoEditorModel>, IRegGosNadzorZaGeoEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования геология'
    /// </summary>
    public class RegGosNadzorZaGeoEditorViewModel : BaseEditorViewModel<Bars.Nedragis.RegGosNadzorZaGeo, RegGosNadzorZaGeoEditorModel>, IRegGosNadzorZaGeoEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override RegGosNadzorZaGeoEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new RegGosNadzorZaGeoEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'ОПИ' в модель представления
        /// </summary>
        protected override RegGosNadzorZaGeoEditorModel MapEntityInternal(Bars.Nedragis.RegGosNadzorZaGeo entity)
        {
            // создаем экзепляр модели
            var model = new RegGosNadzorZaGeoEditorModel();
            model.Id = entity.Id;
            model.Element1507635501632 = (System.String)(entity.Element1507635501632);
            model.Element1507635537755 = (System.DateTime? )(entity.Element1507635537755);
            model.Element1507635559781 = (System.DateTime? )(entity.Element1507635559781);
            model.Element1507635586209 = (System.String)(entity.Element1507635586209);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'ОПИ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.RegGosNadzorZaGeo entity, RegGosNadzorZaGeoEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1507635501632 = model.Element1507635501632;
            entity.Element1507635537755 = model.Element1507635537755.GetValueOrDefault();
            entity.Element1507635559781 = model.Element1507635559781.GetValueOrDefault();
            entity.Element1507635586209 = model.Element1507635586209;
        }
    }
}