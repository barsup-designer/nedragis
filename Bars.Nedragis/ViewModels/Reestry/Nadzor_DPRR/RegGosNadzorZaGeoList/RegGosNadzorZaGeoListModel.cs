namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Региональный государственный надзор за геологическим изучением'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Региональный государственный надзор за геологическим изучением")]
    public class RegGosNadzorZaGeoListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер лицензии' (псевдоним: licn11)
        /// </summary>
        [Bars.B4.Utils.Display("Номер лицензии")]
        [Bars.Rms.Core.Attributes.Uid("48d36341-df91-440c-8c33-fefc303a383d")]
        public virtual System.String licn11
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата регистрации' (псевдоним: licdate11)
        /// </summary>
        [Bars.B4.Utils.Display("Дата регистрации")]
        [Bars.Rms.Core.Attributes.Uid("473c0c39-9bf3-49e9-a54d-6110406f6f7f")]
        public virtual System.DateTime? licdate11
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Окончание срока действия' (псевдоним: licdateend11)
        /// </summary>
        [Bars.B4.Utils.Display("Окончание срока действия")]
        [Bars.Rms.Core.Attributes.Uid("22c70618-15bb-40c8-948d-8a70f0661227")]
        public virtual System.DateTime? licdateend11
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Местонаходжение' (псевдоним: licmesto11)
        /// </summary>
        [Bars.B4.Utils.Display("Местонаходжение")]
        [Bars.Rms.Core.Attributes.Uid("a09bb209-52f4-4799-a626-07c8bdb4c58e")]
        public virtual System.String licmesto11
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}