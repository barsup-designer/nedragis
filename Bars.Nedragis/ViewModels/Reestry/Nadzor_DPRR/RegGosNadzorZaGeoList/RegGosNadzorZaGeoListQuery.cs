namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Региональный государственный надзор за геологическим изучением'
    /// </summary>
    public interface IRegGosNadzorZaGeoListQuery : IQueryOperation<Bars.Nedragis.RegGosNadzorZaGeo, Bars.Nedragis.RegGosNadzorZaGeoListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Региональный государственный надзор за геологическим изучением'
    /// </summary>
    public interface IRegGosNadzorZaGeoListQueryFilter : IQueryOperationFilter<Bars.Nedragis.RegGosNadzorZaGeo, Bars.Nedragis.RegGosNadzorZaGeoListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Региональный государственный надзор за геологическим изучением'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Региональный государственный надзор за геологическим изучением")]
    public class RegGosNadzorZaGeoListQuery : RmsEntityQueryOperation<Bars.Nedragis.RegGosNadzorZaGeo, Bars.Nedragis.RegGosNadzorZaGeoListModel>, IRegGosNadzorZaGeoListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "RegGosNadzorZaGeoListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public RegGosNadzorZaGeoListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.RegGosNadzorZaGeo> Filter(IQueryable<Bars.Nedragis.RegGosNadzorZaGeo> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.RegGosNadzorZaGeoListModel> Map(IQueryable<Bars.Nedragis.RegGosNadzorZaGeo> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.RegGosNadzorZaGeo>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.RegGosNadzorZaGeoListModel{Id = x.Id, _TypeUid = "d14687ca-1601-4731-a8ab-e4e2311cdb65", licn11 = (System.String)(x.Element1507635501632), licdate11 = (System.DateTime? )(x.Element1507635537755), licdateend11 = (System.DateTime? )(x.Element1507635559781), licmesto11 = (System.String)(x.Element1507635586209), });
        }
    }
}