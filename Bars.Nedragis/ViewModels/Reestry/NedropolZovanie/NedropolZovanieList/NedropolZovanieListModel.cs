namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Недропользования'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Недропользования")]
    public class NedropolZovanieListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование отдела.Наименование' (псевдоним: name_n_NAME_SP)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("26a52bdd-fd6e-41b1-af5d-97059e01c186")]
        public virtual System.String name_n_NAME_SP
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата создания' (псевдоним: ObjectCreateDate)
        /// </summary>
        [Bars.B4.Utils.Display("Дата создания")]
        [Bars.Rms.Core.Attributes.Uid("e59b642f-28ed-4a7f-bdbc-5e401197d9f7")]
        public virtual System.DateTime? ObjectCreateDate
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Вид работ' (псевдоним: vid_n)
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("d85b0fea-aa1a-415b-a86b-09489f04d4e8")]
        public virtual System.String vid_n
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Подвид работ' (псевдоним: podvid)
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("d7061f73-2202-4237-a0ed-04eeb6e4582d")]
        public virtual System.String podvid
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Количество' (псевдоним: col_n)
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("036a30e0-d064-4eb8-82b2-d1dec01b496e")]
        public virtual System.Int32? col_n
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дополнительное описание' (псевдоним: opis_n)
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительное описание")]
        [Bars.Rms.Core.Attributes.Uid("07122633-b8de-4612-8f6a-e72024d549be")]
        public virtual System.String opis_n
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Безвозмездное обращение' (псевдоним: obr_n)
        /// </summary>
        [Bars.B4.Utils.Display("Безвозмездное обращение")]
        [Bars.Rms.Core.Attributes.Uid("1fe8d249-5395-404e-9314-6364721d26fd")]
        public virtual System.String obr_n
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Заказчик' (псевдоним: zakazchik)
        /// </summary>
        [Bars.B4.Utils.Display("Заказчик")]
        [Bars.Rms.Core.Attributes.Uid("2046d649-0c81-4748-a1f8-6a4856d12f08")]
        public virtual System.String zakazchik
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дополнительная информация о заказчике' (псевдоним: zakaz_n)
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительная информация о заказчике")]
        [Bars.Rms.Core.Attributes.Uid("989aa829-7acd-4541-a62c-f53d5777112f")]
        public virtual System.String zakaz_n
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Орган госвласти' (псевдоним: organ)
        /// </summary>
        [Bars.B4.Utils.Display("Орган госвласти")]
        [Bars.Rms.Core.Attributes.Uid("52f24412-a8db-4ab4-a941-93261df49098")]
        public virtual System.String organ
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Характер обращения' (псевдоним: harakter_n)
        /// </summary>
        [Bars.B4.Utils.Display("Характер обращения")]
        [Bars.Rms.Core.Attributes.Uid("35c81fdd-7fe5-4bc8-96c9-d412131f0318")]
        public virtual System.String harakter_n
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Исполнитель.ФИО' (псевдоним: sotr_n_Element1474352136232)
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель.ФИО")]
        [Bars.Rms.Core.Attributes.Uid("c9d904fb-c5a3-4380-9a02-1ff063dbe9f6")]
        public virtual System.String sotr_n_Element1474352136232
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Количество затраченных человека часов' (псевдоним: chas_n)
        /// </summary>
        [Bars.B4.Utils.Display("Количество затраченных человека часов")]
        [Bars.Rms.Core.Attributes.Uid("bc1be98f-4bd6-462d-ab10-750614921bdf")]
        public virtual System.Int32? chas_n
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Стоимость по калькуляции руб.' (псевдоним: stoumost)
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость по калькуляции руб.")]
        [Bars.Rms.Core.Attributes.Uid("a6feaa72-3662-4cca-989d-6af10f7c4b88")]
        public virtual System.Decimal? stoumost
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Оплачено' (псевдоним: oplata_n)
        /// </summary>
        [Bars.B4.Utils.Display("Оплачено")]
        [Bars.Rms.Core.Attributes.Uid("f634ad4b-0c24-452d-a80d-5b740472e452")]
        public virtual System.String oplata_n
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}