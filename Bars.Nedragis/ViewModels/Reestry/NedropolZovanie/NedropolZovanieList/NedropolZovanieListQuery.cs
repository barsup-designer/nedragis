namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Недропользования'
    /// </summary>
    public interface INedropolZovanieListQuery : IQueryOperation<Bars.Nedragis.NedropolZovanie, Bars.Nedragis.NedropolZovanieListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Недропользования'
    /// </summary>
    public interface INedropolZovanieListQueryFilter : IQueryOperationFilter<Bars.Nedragis.NedropolZovanie, Bars.Nedragis.NedropolZovanieListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Недропользования'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Недропользования")]
    public class NedropolZovanieListQuery : RmsEntityQueryOperation<Bars.Nedragis.NedropolZovanie, Bars.Nedragis.NedropolZovanieListModel>, INedropolZovanieListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "NedropolZovanieListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public NedropolZovanieListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.NedropolZovanie> Filter(IQueryable<Bars.Nedragis.NedropolZovanie> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.NedropolZovanieListModel> Map(IQueryable<Bars.Nedragis.NedropolZovanie> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.NedropolZovanie>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.NedropolZovanieListModel{Id = x.Id, _TypeUid = "319ddc80-7273-4a3b-b12c-91ad4b942e52", name_n_NAME_SP = (System.String)(x.name_n.NAME_SP), ObjectCreateDate = (System.DateTime? )(x.ObjectCreateDate), vid_n = (System.String)(x.vid_n), podvid = (System.String)(x.podvid), col_n = (System.Int32? )(x.col_n), opis_n = (System.String)(x.opis_n), obr_n = (System.String)(x.obr_n), zakazchik = (System.String)(x.zakazchik), zakaz_n = (System.String)(x.zakaz_n), organ = (System.String)(x.organ), harakter_n = (System.String)(x.harakter_n), sotr_n_Element1474352136232 = (System.String)(x.sotr_n.Element1474352136232), chas_n = (System.Int32? )(x.chas_n), stoumost = (System.Decimal? )(x.stoumost), oplata_n = (System.String)(x.oplata_n), });
        }
    }
}