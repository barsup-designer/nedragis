namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Недропользование' для отдачи на клиент
    /// </summary>
    public class NedropolZovanieEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public NedropolZovanieEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("cb51a4dd-f192-4f22-b74a-bec2ba54d1a5")]
        public virtual Bars.Nedragis.Otdel1ListModel name_n
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата создания'
        /// </summary>
        [Bars.B4.Utils.Display("Дата создания")]
        [Bars.Rms.Core.Attributes.Uid("684927e5-9a22-422e-b710-4b06e1fba2d4")]
        public virtual System.DateTime? ObjectCreateDate
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Вид работ'
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("b5c93108-5fa8-4ade-93f8-e20cd6890b27")]
        public virtual System.String vid_n
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Подвид работ'
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("2bfac98e-cf67-48e0-828c-07cb83ed983a")]
        public virtual System.String podvid
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Количество'
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("584f049e-ea3c-40c3-9d72-1d3360487d7f")]
        public virtual System.Int32? col_n
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дополнительное описание'
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительное описание")]
        [Bars.Rms.Core.Attributes.Uid("b27d2fde-3f79-4282-b928-6f2637490616")]
        public virtual System.String opis_n
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Безвозмездное обращение'
        /// </summary>
        [Bars.B4.Utils.Display("Безвозмездное обращение")]
        [Bars.Rms.Core.Attributes.Uid("eaaa31af-cd31-4bac-8793-846d561c6cab")]
        public virtual System.String obr_n
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Заказчик'
        /// </summary>
        [Bars.B4.Utils.Display("Заказчик")]
        [Bars.Rms.Core.Attributes.Uid("cfab6d33-da56-456c-a527-090157df07d6")]
        public virtual System.String zakazchik
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дополнительная информация о заказчике'
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительная информация о заказчике")]
        [Bars.Rms.Core.Attributes.Uid("b048e57a-d007-428b-9360-6c01264d36a0")]
        public virtual System.String zakaz_n
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Характер обращения'
        /// </summary>
        [Bars.B4.Utils.Display("Характер обращения")]
        [Bars.Rms.Core.Attributes.Uid("080062b3-ef62-470c-b0f9-ee57369c8d34")]
        public virtual System.String harakter_n
        {
            get;
            set;
        }

        /// <summary>
        /// Исполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("fd4b6919-0fad-408b-b6ad-403f0d105728")]
        public virtual Bars.Nedragis.Sotrudniki1ListModel sotr_n
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Орган госвласти'
        /// </summary>
        [Bars.B4.Utils.Display("Орган госвласти")]
        [Bars.Rms.Core.Attributes.Uid("53097677-a096-483d-9856-a0526a2b0db9")]
        public virtual System.String organ
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Количество затраченных человека часов'
        /// </summary>
        [Bars.B4.Utils.Display("Количество затраченных человека часов")]
        [Bars.Rms.Core.Attributes.Uid("4ae97157-9cd4-46ac-8762-ba229461d11e")]
        public virtual System.Int32? chas_n
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Стоимость по калькуляции руб.'
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость по калькуляции руб.")]
        [Bars.Rms.Core.Attributes.Uid("e3562885-8dce-42c1-a0a3-83e959dcc1dc")]
        public virtual System.Decimal? stoumost
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Оплачено'
        /// </summary>
        [Bars.B4.Utils.Display("Оплачено")]
        [Bars.Rms.Core.Attributes.Uid("35c0bcdb-c697-44fa-9935-ffac08e10344")]
        public virtual System.String oplata_n
        {
            get;
            set;
        }
    }
}