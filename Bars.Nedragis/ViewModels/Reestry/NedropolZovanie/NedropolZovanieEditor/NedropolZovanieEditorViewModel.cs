namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Недропользование'
    /// </summary>
    public interface INedropolZovanieEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.NedropolZovanie, NedropolZovanieEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Недропользование'
    /// </summary>
    public interface INedropolZovanieEditorValidator : IEditorModelValidator<NedropolZovanieEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Недропользование'
    /// </summary>
    public interface INedropolZovanieEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.NedropolZovanie, NedropolZovanieEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Недропользование'
    /// </summary>
    public abstract class AbstractNedropolZovanieEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.NedropolZovanie, NedropolZovanieEditorModel>, INedropolZovanieEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Недропользование'
    /// </summary>
    public class NedropolZovanieEditorViewModel : BaseEditorViewModel<Bars.Nedragis.NedropolZovanie, NedropolZovanieEditorModel>, INedropolZovanieEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override NedropolZovanieEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new NedropolZovanieEditorModel();
            var varname_nId = @params.Params.GetAs<long>("name_n_Id", 0);
            if (varname_nId > 0)
            {
                model.name_n = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>().GetById(varname_nId);
            }

            var varsotr_nId = @params.Params.GetAs<long>("sotr_n_Id", 0);
            if (varsotr_nId > 0)
            {
                model.sotr_n = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>().GetById(varsotr_nId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Недропользования' в модель представления
        /// </summary>
        protected override NedropolZovanieEditorModel MapEntityInternal(Bars.Nedragis.NedropolZovanie entity)
        {
            // создаем экзепляр модели
            var model = new NedropolZovanieEditorModel();
            model.Id = entity.Id;
            if (entity.name_n.IsNotNull())
            {
                var queryOtdel1List = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>();
                model.name_n = queryOtdel1List.GetById(entity.name_n.Id);
            }

            model.ObjectCreateDate = (System.DateTime? )(entity.ObjectCreateDate);
            model.vid_n = (System.String)(entity.vid_n);
            model.podvid = (System.String)(entity.podvid);
            model.col_n = (System.Int32? )(entity.col_n);
            model.opis_n = (System.String)(entity.opis_n);
            model.obr_n = (System.String)(entity.obr_n);
            model.zakazchik = (System.String)(entity.zakazchik);
            model.zakaz_n = (System.String)(entity.zakaz_n);
            model.harakter_n = (System.String)(entity.harakter_n);
            if (entity.sotr_n.IsNotNull())
            {
                var querySotrudniki1List = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>();
                model.sotr_n = querySotrudniki1List.GetById(entity.sotr_n.Id);
            }

            model.organ = (System.String)(entity.organ);
            model.chas_n = (System.Int32? )(entity.chas_n);
            model.stoumost = (System.Decimal? )(entity.stoumost);
            model.oplata_n = (System.String)(entity.oplata_n);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Недропользования' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.NedropolZovanie entity, NedropolZovanieEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name_n = TryLoadEntityById<Bars.Nedragis.Otdel1>(model.name_n?.Id);
            entity.ObjectCreateDate = model.ObjectCreateDate.GetValueOrDefault();
            entity.vid_n = model.vid_n;
            entity.podvid = model.podvid;
            if (model.col_n.HasValue)
            {
                entity.col_n = model.col_n.Value;
            }

            entity.opis_n = model.opis_n;
            entity.obr_n = model.obr_n;
            entity.zakazchik = model.zakazchik;
            entity.zakaz_n = model.zakaz_n;
            entity.harakter_n = model.harakter_n;
            entity.sotr_n = TryLoadEntityById<Bars.Nedragis.Sotrudniki1>(model.sotr_n?.Id);
            entity.organ = model.organ;
            if (model.chas_n.HasValue)
            {
                entity.chas_n = model.chas_n.Value;
            }

            if (model.stoumost.HasValue)
            {
                entity.stoumost = model.stoumost.Value;
            }

            entity.oplata_n = model.oplata_n;
        }
    }
}