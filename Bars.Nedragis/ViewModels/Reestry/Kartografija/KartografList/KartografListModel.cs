namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Картография'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Картография")]
    public class KartografListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование отдела.Наименование' (псевдоним: name_NAME_SP)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("3602d578-775d-4c6a-ba83-790116b99cb6")]
        public virtual System.String name_NAME_SP
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата создания' (псевдоним: ObjectCreateDate)
        /// </summary>
        [Bars.B4.Utils.Display("Дата создания")]
        [Bars.Rms.Core.Attributes.Uid("29440dfb-32d1-49cd-ba05-ae06d0c51d2c")]
        public virtual System.DateTime? ObjectCreateDate
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Вид работ.Наименование' (псевдоним: Element1474278116576_Element1474020299386)
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("6f16a007-cb29-47e5-976f-81ca18a9fb66")]
        public virtual System.String Element1474278116576_Element1474020299386
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Подвид работ' (псевдоним: podvid)
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("5c24101b-211c-48a7-87bc-d77f53a8b877")]
        public virtual System.String podvid
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Количество' (псевдоним: col)
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("af7015fc-153c-447e-a6a0-2e82dd0e661f")]
        public virtual System.Int32? col
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дополнительное описание' (псевдоним: opis)
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительное описание")]
        [Bars.Rms.Core.Attributes.Uid("4c9d2f60-612f-4c7b-919e-0ee2c033dcb6")]
        public virtual System.String opis
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Безвозмездное обращение' (псевдоним: Element1474351766399)
        /// </summary>
        [Bars.B4.Utils.Display("Безвозмездное обращение")]
        [Bars.Rms.Core.Attributes.Uid("1bb6a143-4634-4837-882a-5113bfd05c3e")]
        public virtual System.String Element1474351766399
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Заказчик' (псевдоним: zakazchik)
        /// </summary>
        [Bars.B4.Utils.Display("Заказчик")]
        [Bars.Rms.Core.Attributes.Uid("9fc20532-ed28-4e2f-a97a-3e53a1b3acbd")]
        public virtual System.String zakazchik
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дополнительная информация о заказчике' (псевдоним: zakaz1)
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительная информация о заказчике")]
        [Bars.Rms.Core.Attributes.Uid("0dfd12bd-c9e5-4e71-b284-67f65924398f")]
        public virtual System.String zakaz1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Орган госвласти' (псевдоним: organ)
        /// </summary>
        [Bars.B4.Utils.Display("Орган госвласти")]
        [Bars.Rms.Core.Attributes.Uid("82ba0519-4d17-4c2c-9701-142cd74f4244")]
        public virtual System.String organ
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Характер обращения' (псевдоним: harakter)
        /// </summary>
        [Bars.B4.Utils.Display("Характер обращения")]
        [Bars.Rms.Core.Attributes.Uid("11b8c3e5-4787-4e44-8379-83a8577ccbf8")]
        public virtual System.String harakter
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Исполнитель.ФИО' (псевдоним: Element1474352034381_Element1474352136232)
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель.ФИО")]
        [Bars.Rms.Core.Attributes.Uid("c367d55b-47ff-4410-b346-9327723472eb")]
        public virtual System.String Element1474352034381_Element1474352136232
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Количество затраченных человека часов' (псевдоним: chas)
        /// </summary>
        [Bars.B4.Utils.Display("Количество затраченных человека часов")]
        [Bars.Rms.Core.Attributes.Uid("c3d0de13-d118-4439-8c65-41836c3a59c5")]
        public virtual System.Int32? chas
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Стоимость по калькуляции руб.' (псевдоним: stoumost)
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость по калькуляции руб.")]
        [Bars.Rms.Core.Attributes.Uid("e2cf3b7c-b6de-4f11-aa27-8caba662d661")]
        public virtual System.Decimal? stoumost
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Оплачено' (псевдоним: oplata)
        /// </summary>
        [Bars.B4.Utils.Display("Оплачено")]
        [Bars.Rms.Core.Attributes.Uid("c39a69ba-5916-40c4-87a8-de075dbecd49")]
        public virtual System.String oplata
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}