namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Картография'
    /// </summary>
    public interface IKartografListQuery : IQueryOperation<Bars.Nedragis.Kartograf, Bars.Nedragis.KartografListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Картография'
    /// </summary>
    public interface IKartografListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Kartograf, Bars.Nedragis.KartografListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Картография'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Картография")]
    public class KartografListQuery : RmsEntityQueryOperation<Bars.Nedragis.Kartograf, Bars.Nedragis.KartografListModel>, IKartografListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "KartografListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public KartografListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Kartograf> Filter(IQueryable<Bars.Nedragis.Kartograf> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.KartografListModel> Map(IQueryable<Bars.Nedragis.Kartograf> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Kartograf>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.KartografListModel{Id = x.Id, _TypeUid = "cdbf139a-d2ac-4731-87f9-40f673f96b6c", name_NAME_SP = (System.String)(x.name.NAME_SP), ObjectCreateDate = (System.DateTime? )(x.ObjectCreateDate), Element1474278116576_Element1474020299386 = (System.String)(x.vid.Element1474020299386), podvid = (System.String)(x.podvid), col = (System.Int32? )(x.col), opis = (System.String)(x.opis), Element1474351766399 = (System.String)(x.Element1474351766399), zakazchik = (System.String)(x.zakazchik), zakaz1 = (System.String)(x.zakaz1), organ = (System.String)(x.organ), harakter = (System.String)(x.harakter), Element1474352034381_Element1474352136232 = (System.String)(x.Element1474352034381.Element1474352136232), chas = (System.Int32? )(x.chas), stoumost = (System.Decimal? )(x.stoumost), oplata = (System.String)(x.oplata), });
        }
    }
}