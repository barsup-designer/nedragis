namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Картография'
    /// </summary>
    public interface IKartografEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Kartograf, KartografEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Картография'
    /// </summary>
    public interface IKartografEditorValidator : IEditorModelValidator<KartografEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Картография'
    /// </summary>
    public interface IKartografEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Kartograf, KartografEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Картография'
    /// </summary>
    public abstract class AbstractKartografEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Kartograf, KartografEditorModel>, IKartografEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Картография'
    /// </summary>
    public class KartografEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Kartograf, KartografEditorModel>, IKartografEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override KartografEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new KartografEditorModel();
            var varnameId = @params.Params.GetAs<long>("name_Id", 0);
            if (varnameId > 0)
            {
                model.name = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>().GetById(varnameId);
            }

            var varvidId = @params.Params.GetAs<long>("vid_Id", 0);
            if (varvidId > 0)
            {
                model.vid = Container.Resolve<Bars.Nedragis.IVidyRabotListQuery>().GetById(varvidId);
            }

            var varElement1474352034381Id = @params.Params.GetAs<long>("Element1474352034381_Id", 0);
            if (varElement1474352034381Id > 0)
            {
                model.Element1474352034381 = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>().GetById(varElement1474352034381Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Картография' в модель представления
        /// </summary>
        protected override KartografEditorModel MapEntityInternal(Bars.Nedragis.Kartograf entity)
        {
            // создаем экзепляр модели
            var model = new KartografEditorModel();
            model.Id = entity.Id;
            if (entity.name.IsNotNull())
            {
                var queryOtdel1List = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>();
                model.name = queryOtdel1List.GetById(entity.name.Id);
            }

            model.ObjectCreateDate = (System.DateTime? )(entity.ObjectCreateDate);
            if (entity.vid.IsNotNull())
            {
                var queryVidyRabotList = Container.Resolve<Bars.Nedragis.IVidyRabotListQuery>();
                model.vid = queryVidyRabotList.GetById(entity.vid.Id);
            }

            model.podvid = (System.String)(entity.podvid);
            model.col = (System.Int32? )(entity.col);
            model.opis = (System.String)(entity.opis);
            model.Element1474351766399 = (System.String)(entity.Element1474351766399);
            model.zakazchik = (System.String)(entity.zakazchik);
            model.zakaz1 = (System.String)(entity.zakaz1);
            model.harakter = (System.String)(entity.harakter);
            if (entity.Element1474352034381.IsNotNull())
            {
                var querySotrudniki1List = Container.Resolve<Bars.Nedragis.ISotrudniki1ListQuery>();
                model.Element1474352034381 = querySotrudniki1List.GetById(entity.Element1474352034381.Id);
            }

            model.organ = (System.String)(entity.organ);
            model.chas = (System.Int32? )(entity.chas);
            model.stoumost = (System.Decimal? )(entity.stoumost);
            model.oplata = (System.String)(entity.oplata);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Картография' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Kartograf entity, KartografEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name = TryLoadEntityById<Bars.Nedragis.Otdel1>(model.name?.Id);
            entity.ObjectCreateDate = model.ObjectCreateDate.GetValueOrDefault();
            entity.vid = TryLoadEntityById<Bars.Nedragis.VidyRabot>(model.vid?.Id);
            entity.podvid = model.podvid;
            if (model.col.HasValue)
            {
                entity.col = model.col.Value;
            }

            entity.opis = model.opis;
            entity.Element1474351766399 = model.Element1474351766399;
            entity.zakazchik = model.zakazchik;
            entity.zakaz1 = model.zakaz1;
            entity.harakter = model.harakter;
            entity.Element1474352034381 = TryLoadEntityById<Bars.Nedragis.Sotrudniki1>(model.Element1474352034381?.Id);
            entity.organ = model.organ;
            if (model.chas.HasValue)
            {
                entity.chas = model.chas.Value;
            }

            if (model.stoumost.HasValue)
            {
                entity.stoumost = model.stoumost.Value;
            }

            entity.oplata = model.oplata;
        }
    }
}