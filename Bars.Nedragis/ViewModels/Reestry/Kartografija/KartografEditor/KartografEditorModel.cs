namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Картография' для отдачи на клиент
    /// </summary>
    public class KartografEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public KartografEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("a5b28966-a850-45fd-80cc-9907a3668dfb")]
        public virtual Bars.Nedragis.Otdel1ListModel name
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата создания'
        /// </summary>
        [Bars.B4.Utils.Display("Дата создания")]
        [Bars.Rms.Core.Attributes.Uid("eef1c74b-3c96-413b-879c-c79fd8964170")]
        public virtual System.DateTime? ObjectCreateDate
        {
            get;
            set;
        }

        /// <summary>
        /// Вид работ
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("32999804-77e2-49a3-ace0-9cf706eb287b")]
        public virtual Bars.Nedragis.VidyRabotListModel vid
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Подвид работ'
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("22b2e055-bd0b-412a-8c9c-28b0f980a684")]
        public virtual System.String podvid
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Количество'
        /// </summary>
        [Bars.B4.Utils.Display("Количество")]
        [Bars.Rms.Core.Attributes.Uid("46653b81-efd5-42a1-8369-5cb9448da37f")]
        public virtual System.Int32? col
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дополнительное описание'
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительное описание")]
        [Bars.Rms.Core.Attributes.Uid("91db7a75-d30d-4029-b3ad-b3b8e1a4b289")]
        public virtual System.String opis
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Безвозмездное обращение'
        /// </summary>
        [Bars.B4.Utils.Display("Безвозмездное обращение")]
        [Bars.Rms.Core.Attributes.Uid("6a4d50d3-c035-422c-a655-e4093e10db89")]
        public virtual System.String Element1474351766399
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Заказчик'
        /// </summary>
        [Bars.B4.Utils.Display("Заказчик")]
        [Bars.Rms.Core.Attributes.Uid("92ed231f-7292-4199-9fe0-c6b9c174240f")]
        public virtual System.String zakazchik
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дополнительная информация о заказчике'
        /// </summary>
        [Bars.B4.Utils.Display("Дополнительная информация о заказчике")]
        [Bars.Rms.Core.Attributes.Uid("b0abf829-486a-46e8-8d69-63fcd5901f67")]
        public virtual System.String zakaz1
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Характер обращения'
        /// </summary>
        [Bars.B4.Utils.Display("Характер обращения")]
        [Bars.Rms.Core.Attributes.Uid("5d1cd9fd-6202-4abe-836d-0606582d3a09")]
        public virtual System.String harakter
        {
            get;
            set;
        }

        /// <summary>
        /// Исполнитель
        /// </summary>
        [Bars.B4.Utils.Display("Исполнитель")]
        [Bars.Rms.Core.Attributes.Uid("72fbbb20-522b-48a6-b279-b72c7f5771e3")]
        public virtual Bars.Nedragis.Sotrudniki1ListModel Element1474352034381
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Орган госвласти'
        /// </summary>
        [Bars.B4.Utils.Display("Орган госвласти")]
        [Bars.Rms.Core.Attributes.Uid("f689512e-dbbc-4d8e-bf1f-85c68aac5941")]
        public virtual System.String organ
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Количество затраченных человека часов'
        /// </summary>
        [Bars.B4.Utils.Display("Количество затраченных человека часов")]
        [Bars.Rms.Core.Attributes.Uid("95268d2f-2395-46ef-9e91-6af6bd7bd8dd")]
        public virtual System.Int32? chas
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Стоимость по калькуляции руб.'
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость по калькуляции руб.")]
        [Bars.Rms.Core.Attributes.Uid("2d71353f-9ded-4a1b-95db-21b1c877f107")]
        public virtual System.Decimal? stoumost
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Оплачено'
        /// </summary>
        [Bars.B4.Utils.Display("Оплачено")]
        [Bars.Rms.Core.Attributes.Uid("3219b336-97c3-401c-9436-bbbe0629533e")]
        public virtual System.String oplata
        {
            get;
            set;
        }
    }
}