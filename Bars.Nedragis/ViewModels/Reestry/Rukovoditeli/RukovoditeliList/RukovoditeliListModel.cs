namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Руководители'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Руководители")]
    public class RukovoditeliListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Фамилия' (псевдоним: Surname)
        /// </summary>
        [Bars.B4.Utils.Display("Фамилия")]
        [Bars.Rms.Core.Attributes.Uid("fe4bd458-1449-4a8e-b301-497015e534e6")]
        public virtual System.String Surname
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Имя' (псевдоним: FirstName)
        /// </summary>
        [Bars.B4.Utils.Display("Имя")]
        [Bars.Rms.Core.Attributes.Uid("26c5338a-5129-4e1f-a215-7496fdd8e738")]
        public virtual System.String FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Отчество' (псевдоним: Patronymic)
        /// </summary>
        [Bars.B4.Utils.Display("Отчество")]
        [Bars.Rms.Core.Attributes.Uid("c622bf44-542f-4fbd-a798-14749cba1800")]
        public virtual System.String Patronymic
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Тип должности.Наименование' (псевдоним: PostType_name1)
        /// </summary>
        [Bars.B4.Utils.Display("Тип должности.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("b2610f51-b373-4994-b2f7-f82e6f394e0b")]
        public virtual System.String PostType_name1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Должность' (псевдоним: PostText)
        /// </summary>
        [Bars.B4.Utils.Display("Должность")]
        [Bars.Rms.Core.Attributes.Uid("3c62f88d-8722-4763-bd17-830eeee56b11")]
        public virtual System.String PostText
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}