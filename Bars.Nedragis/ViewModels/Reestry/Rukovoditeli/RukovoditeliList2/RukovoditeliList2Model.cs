namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Руководители'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Руководители")]
    public class RukovoditeliList2Model
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Юридическое лицо.Название субъекта' (псевдоним: YurLitco_Representation)
        /// </summary>
        [Bars.B4.Utils.Display("Юридическое лицо.Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("c1d2903d-89a0-4ba2-b6a7-a8de32ab844e")]
        public virtual System.String YurLitco_Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Фамилия' (псевдоним: Surname)
        /// </summary>
        [Bars.B4.Utils.Display("Фамилия")]
        [Bars.Rms.Core.Attributes.Uid("e86463fe-7967-4e30-ab79-88c5f9208fc6")]
        public virtual System.String Surname
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Имя' (псевдоним: FirstName)
        /// </summary>
        [Bars.B4.Utils.Display("Имя")]
        [Bars.Rms.Core.Attributes.Uid("71f62d64-0174-495f-a9f1-3157d1e86e4f")]
        public virtual System.String FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Отчество' (псевдоним: Patronymic)
        /// </summary>
        [Bars.B4.Utils.Display("Отчество")]
        [Bars.Rms.Core.Attributes.Uid("4f244150-2d37-4370-99d7-20dc136d355e")]
        public virtual System.String Patronymic
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Пол' (псевдоним: Sex)
        /// </summary>
        [Bars.B4.Utils.Display("Пол")]
        [Bars.Rms.Core.Attributes.Uid("3061c757-ccca-4d81-9ac2-1e51c1edbca6")]
        public virtual Bars.Nedragis.Pol? Sex
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Тип должности.Наименование' (псевдоним: PostType_name1)
        /// </summary>
        [Bars.B4.Utils.Display("Тип должности.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("b98e6b21-81b4-4e91-8cb8-e86b7a0ce7a5")]
        public virtual System.String PostType_name1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Должность' (псевдоним: PostText)
        /// </summary>
        [Bars.B4.Utils.Display("Должность")]
        [Bars.Rms.Core.Attributes.Uid("4c083c9c-7e3e-446e-86bf-2e9e3bc50397")]
        public virtual System.String PostText
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Действует на основании' (псевдоним: Basis)
        /// </summary>
        [Bars.B4.Utils.Display("Действует на основании")]
        [Bars.Rms.Core.Attributes.Uid("58c5e8e5-f005-4186-b77b-b01e82f95922")]
        public virtual System.String Basis
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Телефон' (псевдоним: Phone)
        /// </summary>
        [Bars.B4.Utils.Display("Телефон")]
        [Bars.Rms.Core.Attributes.Uid("be2b24c3-e3b2-4608-9a9f-df369d8e5994")]
        public virtual System.String Phone
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}