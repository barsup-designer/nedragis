namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Руководители'
    /// </summary>
    public interface IRukovoditeliList2Query : IQueryOperation<Bars.Nedragis.Rukovoditeli, Bars.Nedragis.RukovoditeliList2Model, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Руководители'
    /// </summary>
    public interface IRukovoditeliList2QueryFilter : IQueryOperationFilter<Bars.Nedragis.Rukovoditeli, Bars.Nedragis.RukovoditeliList2Model, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Руководители'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Руководители")]
    public class RukovoditeliList2Query : RmsEntityQueryOperation<Bars.Nedragis.Rukovoditeli, Bars.Nedragis.RukovoditeliList2Model>, IRukovoditeliList2Query
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "RukovoditeliList2Query"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public RukovoditeliList2Query(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Rukovoditeli> Filter(IQueryable<Bars.Nedragis.Rukovoditeli> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
                // обработка значения поля 'Пол'
                var fldSex = @params.Params.GetAs<Bars.Nedragis.Pol? >("Sex");
                if (fldSex.HasValue)
                {
                    query = query.Where(x => x.Sex == fldSex.Value);
                }
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.RukovoditeliList2Model> Map(IQueryable<Bars.Nedragis.Rukovoditeli> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Rukovoditeli>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.RukovoditeliList2Model{Id = x.Id, _TypeUid = "cf26c7ab-6cf7-49ad-888a-7fb500a92d49", YurLitco_Representation = (System.String)(x.YurLitco.Representation), Surname = (System.String)(x.Surname), FirstName = (System.String)(x.FirstName), Patronymic = (System.String)(x.Patronymic), Sex = (Bars.Nedragis.Pol? )(x.Sex), PostType_name1 = (System.String)(x.PostType.name1), PostText = (System.String)(x.PostText), Basis = (System.String)(x.Basis), Phone = (System.String)(x.Phone), });
        }
    }
}