namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Руководители'
    /// </summary>
    public interface IRukovoditeliEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Rukovoditeli, RukovoditeliEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Руководители'
    /// </summary>
    public interface IRukovoditeliEditorValidator : IEditorModelValidator<RukovoditeliEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Руководители'
    /// </summary>
    public interface IRukovoditeliEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Rukovoditeli, RukovoditeliEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Руководители'
    /// </summary>
    public abstract class AbstractRukovoditeliEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Rukovoditeli, RukovoditeliEditorModel>, IRukovoditeliEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Руководители'
    /// </summary>
    public class RukovoditeliEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Rukovoditeli, RukovoditeliEditorModel>, IRukovoditeliEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override RukovoditeliEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new RukovoditeliEditorModel();
            model.Sex = (Bars.Nedragis.Pol)1;
            var varPostTypeId = @params.Params.GetAs<long>("PostType_Id", 0);
            if (varPostTypeId > 0)
            {
                model.PostType = Container.Resolve<Bars.Nedragis.IDolzhnostiListQuery>().GetById(varPostTypeId);
            }

            var varudostovereniyeId = @params.Params.GetAs<long>("udostovereniye_Id", 0);
            if (varudostovereniyeId > 0)
            {
                model.udostovereniye = Container.Resolve<Bars.Nedragis.IDokumentyUdostoverjajushhieLichnostListQuery>().GetById(varudostovereniyeId);
            }

            var varYurLitcoId = @params.Params.GetAs<long>("YurLitco_Id", 0);
            if (varYurLitcoId > 0)
            {
                model.YurLitco = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>().GetById(varYurLitcoId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Руководители' в модель представления
        /// </summary>
        protected override RukovoditeliEditorModel MapEntityInternal(Bars.Nedragis.Rukovoditeli entity)
        {
            // создаем экзепляр модели
            var model = new RukovoditeliEditorModel();
            model.Id = entity.Id;
            model.Surname = (System.String)(entity.Surname);
            model.FirstName = (System.String)(entity.FirstName);
            model.Patronymic = (System.String)(entity.Patronymic);
            model.Sex = (Bars.Nedragis.Pol? )(entity.Sex);
            if (entity.PostType.IsNotNull())
            {
                var queryDolzhnostiList = Container.Resolve<Bars.Nedragis.IDolzhnostiListQuery>();
                model.PostType = queryDolzhnostiList.GetById(entity.PostType.Id);
            }

            model.PostText = (System.String)(entity.PostText);
            model.Phone = (System.String)(entity.Phone);
            model.Basis = (System.String)(entity.Basis);
            if (entity.udostovereniye.IsNotNull())
            {
                var queryDokumentyUdostoverjajushhieLichnostList = Container.Resolve<Bars.Nedragis.IDokumentyUdostoverjajushhieLichnostListQuery>();
                model.udostovereniye = queryDokumentyUdostoverjajushhieLichnostList.GetById(entity.udostovereniye.Id);
            }

            if (entity.YurLitco.IsNotNull())
            {
                var queryJuridicheskieLicaList = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>();
                model.YurLitco = queryJuridicheskieLicaList.GetById(entity.YurLitco.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Руководители' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Rukovoditeli entity, RukovoditeliEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Surname = model.Surname;
            entity.FirstName = model.FirstName;
            entity.Patronymic = model.Patronymic;
            if (model.Sex.HasValue)
            {
                entity.Sex = model.Sex.Value;
            }
            else
            {
                entity.Sex = null;
            }

            entity.PostType = TryLoadEntityById<Bars.Nedragis.Dolzhnosti>(model.PostType?.Id);
            entity.PostText = model.PostText;
            entity.Phone = model.Phone;
            entity.Basis = model.Basis;
            entity.udostovereniye = TryLoadEntityById<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost>(model.udostovereniye?.Id);
            entity.YurLitco = TryLoadEntityById<Bars.Nedragis.JuridicheskieLica>(model.YurLitco?.Id);
        }
    }
}