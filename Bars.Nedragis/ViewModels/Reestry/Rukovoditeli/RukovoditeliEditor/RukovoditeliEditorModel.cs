namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Руководители' для отдачи на клиент
    /// </summary>
    public class RukovoditeliEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public RukovoditeliEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Фамилия'
        /// </summary>
        [Bars.B4.Utils.Display("Фамилия")]
        [Bars.Rms.Core.Attributes.Uid("527f1c4d-258b-472c-95d6-bff9150d890a")]
        public virtual System.String Surname
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Имя'
        /// </summary>
        [Bars.B4.Utils.Display("Имя")]
        [Bars.Rms.Core.Attributes.Uid("4e6db2a1-d046-42af-8092-656d32947f0a")]
        public virtual System.String FirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Отчество'
        /// </summary>
        [Bars.B4.Utils.Display("Отчество")]
        [Bars.Rms.Core.Attributes.Uid("347a5f77-031c-43ae-9e60-239fec0a10e1")]
        public virtual System.String Patronymic
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Пол'
        /// </summary>
        [Bars.B4.Utils.Display("Пол")]
        [Bars.Rms.Core.Attributes.Uid("7857d423-8b29-4681-9cab-77b5eb3aa0ad")]
        public virtual Bars.Nedragis.Pol? Sex
        {
            get;
            set;
        }

        /// <summary>
        /// Тип должности
        /// </summary>
        [Bars.B4.Utils.Display("Тип должности")]
        [Bars.Rms.Core.Attributes.Uid("2b3af2fd-7c77-4c45-93a2-2a02ca907e0c")]
        public virtual Bars.Nedragis.DolzhnostiListModel PostType
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Должность'
        /// </summary>
        [Bars.B4.Utils.Display("Должность")]
        [Bars.Rms.Core.Attributes.Uid("78aa727a-04a6-46f9-8f95-36b281c1ab50")]
        public virtual System.String PostText
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Телефон'
        /// </summary>
        [Bars.B4.Utils.Display("Телефон")]
        [Bars.Rms.Core.Attributes.Uid("2e695ac8-3f37-426e-ab6b-43464ee5c02d")]
        public virtual System.String Phone
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Действует на основании'
        /// </summary>
        [Bars.B4.Utils.Display("Действует на основании")]
        [Bars.Rms.Core.Attributes.Uid("99ce7793-2a0f-48b1-90a2-d11a240b8385")]
        public virtual System.String Basis
        {
            get;
            set;
        }

        /// <summary>
        /// Документ, удостоверяющий личность
        /// </summary>
        [Bars.B4.Utils.Display("Документ, удостоверяющий личность")]
        [Bars.Rms.Core.Attributes.Uid("f811fa78-e8d5-4d3f-9225-13c1ce8096f4")]
        public virtual Bars.Nedragis.DokumentyUdostoverjajushhieLichnostListModel udostovereniye
        {
            get;
            set;
        }

        /// <summary>
        /// Юридическое лицо
        /// </summary>
        [Bars.B4.Utils.Display("Юридическое лицо")]
        [Bars.Rms.Core.Attributes.Uid("340be21a-4ff2-4c68-8807-19259c32e9fc")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel YurLitco
        {
            get;
            set;
        }
    }
}