namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Юридические лица'
    /// </summary>
    public interface IJuridicheskieLicaListQuery : IQueryOperation<Bars.Nedragis.JuridicheskieLica, Bars.Nedragis.JuridicheskieLicaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Юридические лица'
    /// </summary>
    public interface IJuridicheskieLicaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.JuridicheskieLica, Bars.Nedragis.JuridicheskieLicaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Юридические лица'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Юридические лица")]
    public class JuridicheskieLicaListQuery : RmsEntityQueryOperation<Bars.Nedragis.JuridicheskieLica, Bars.Nedragis.JuridicheskieLicaListModel>, IJuridicheskieLicaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "JuridicheskieLicaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public JuridicheskieLicaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.JuridicheskieLica> Filter(IQueryable<Bars.Nedragis.JuridicheskieLica> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.JuridicheskieLicaListModel> Map(IQueryable<Bars.Nedragis.JuridicheskieLica> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.JuridicheskieLica>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.JuridicheskieLicaListModel{Id = x.Id, _TypeUid = "252ed4f8-3150-4624-b6aa-d2a0fb5f8f5f", OKATO_name1 = (System.String)(x.OKATO.name1), Representation = (System.String)(x.Representation), Fullname = (System.String)(x.Fullname), ogrn = (System.String)(x.ogrn), INN1 = (System.String)(x.INN1), KPP1 = (System.String)(x.KPP1), DateReg = (System.DateTime? )(x.DateReg), CapitalStock = (System.Decimal? )(x.CapitalStock), CapitalPeople = (System.Int32? )(x.CapitalPeople), });
        }
    }
}