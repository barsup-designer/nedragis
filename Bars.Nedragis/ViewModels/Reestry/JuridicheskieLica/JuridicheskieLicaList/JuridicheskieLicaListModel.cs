namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Юридические лица'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Юридические лица")]
    public class JuridicheskieLicaListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'ОКАТО.Наименование' (псевдоним: OKATO_name1)
        /// </summary>
        [Bars.B4.Utils.Display("ОКАТО.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("77ccee4a-58d8-4034-a91d-c6c550f40ee3")]
        public virtual System.String OKATO_name1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Название субъекта' (псевдоним: Representation)
        /// </summary>
        [Bars.B4.Utils.Display("Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("5a337cde-3ed5-4c5a-9844-0b17274e5581")]
        public virtual System.String Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Полное наименование' (псевдоним: Fullname)
        /// </summary>
        [Bars.B4.Utils.Display("Полное наименование")]
        [Bars.Rms.Core.Attributes.Uid("32ed9836-2282-4109-93dd-a4e5a466b0a0")]
        public virtual System.String Fullname
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'ОГРН' (псевдоним: ogrn)
        /// </summary>
        [Bars.B4.Utils.Display("ОГРН")]
        [Bars.Rms.Core.Attributes.Uid("73f4608b-1bde-4482-851d-6b760f1c4b9d")]
        public virtual System.String ogrn
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'ИНН' (псевдоним: INN1)
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("e58468ce-2ec1-42ac-b4ec-c421d72c18ee")]
        public virtual System.String INN1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'КПП' (псевдоним: KPP1)
        /// </summary>
        [Bars.B4.Utils.Display("КПП")]
        [Bars.Rms.Core.Attributes.Uid("8d47ad74-16f9-4e90-8d90-bfc7e99df6ab")]
        public virtual System.String KPP1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата гос. регистрации' (псевдоним: DateReg)
        /// </summary>
        [Bars.B4.Utils.Display("Дата гос. регистрации")]
        [Bars.Rms.Core.Attributes.Uid("d2e22fe3-1112-44c1-bab8-c9a593ba79c1")]
        public virtual System.DateTime? DateReg
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Размер уставного фонда организации' (псевдоним: CapitalStock)
        /// </summary>
        [Bars.B4.Utils.Display("Размер уставного фонда организации")]
        [Bars.Rms.Core.Attributes.Uid("a95e3ef7-c786-4ebf-9e70-2f94f5463e4c")]
        public virtual System.Decimal? CapitalStock
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Среднесписочный состав' (псевдоним: CapitalPeople)
        /// </summary>
        [Bars.B4.Utils.Display("Среднесписочный состав")]
        [Bars.Rms.Core.Attributes.Uid("76b4d8c9-798b-4a08-9533-7d5f02b33032")]
        public virtual System.Int32? CapitalPeople
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}