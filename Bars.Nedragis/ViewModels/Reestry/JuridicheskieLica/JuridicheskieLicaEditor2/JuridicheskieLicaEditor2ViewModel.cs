namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'ЮЛ Добавление'
    /// </summary>
    public interface IJuridicheskieLicaEditor2ViewModel : IEntityEditorViewModel<Bars.Nedragis.JuridicheskieLica, JuridicheskieLicaEditor2Model>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'ЮЛ Добавление'
    /// </summary>
    public interface IJuridicheskieLicaEditor2Validator : IEditorModelValidator<JuridicheskieLicaEditor2Model>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'ЮЛ Добавление'
    /// </summary>
    public interface IJuridicheskieLicaEditor2Handler : IEntityEditorViewModelHandler<Bars.Nedragis.JuridicheskieLica, JuridicheskieLicaEditor2Model>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'ЮЛ Добавление'
    /// </summary>
    public abstract class AbstractJuridicheskieLicaEditor2Handler : EntityEditorViewModelHandler<Bars.Nedragis.JuridicheskieLica, JuridicheskieLicaEditor2Model>, IJuridicheskieLicaEditor2Handler
    {
    }

    /// <summary>
    /// Реализация модели представления 'ЮЛ Добавление'
    /// </summary>
    public class JuridicheskieLicaEditor2ViewModel : BaseEditorViewModel<Bars.Nedragis.JuridicheskieLica, JuridicheskieLicaEditor2Model>, IJuridicheskieLicaEditor2ViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override JuridicheskieLicaEditor2Model CreateModelInternal(BaseParams @params)
        {
            var model = new JuridicheskieLicaEditor2Model();
            var varSphereId = @params.Params.GetAs<long>("Sphere_Id", 0);
            if (varSphereId > 0)
            {
                model.Sphere = Container.Resolve<Bars.Nedragis.IOtraslListQuery>().GetById(varSphereId);
            }

            // получение адреса из свойства Юридический адрес
            var varLegalAddressId = @params.Params.GetAs<long ? >("LegalAddress_Id");
            model.LegalAddress = TryGetEntityById<Bars.B4.Modules.FIAS.FiasAddress>(varLegalAddressId);
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Юридические лица' в модель представления
        /// </summary>
        protected override JuridicheskieLicaEditor2Model MapEntityInternal(Bars.Nedragis.JuridicheskieLica entity)
        {
            // создаем экзепляр модели
            var model = new JuridicheskieLicaEditor2Model();
            model.Id = entity.Id;
            model.INN1 = (System.String)(entity.INN1);
            model.KPP1 = (System.String)(entity.KPP1);
            model.Representation = (System.String)(entity.Representation);
            model.Fullname = (System.String)(entity.Fullname);
            if (entity.Sphere.IsNotNull())
            {
                var queryOtraslList = Container.Resolve<Bars.Nedragis.IOtraslListQuery>();
                model.Sphere = queryOtraslList.GetById(entity.Sphere.Id);
            }

            model.LegalAddress = entity.LegalAddress;
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Юридические лица' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.JuridicheskieLica entity, JuridicheskieLicaEditor2Model model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.INN1 = model.INN1;
            entity.KPP1 = model.KPP1;
            entity.Representation = model.Representation;
            entity.Fullname = model.Fullname;
            entity.Sphere = TryLoadEntityById<Bars.Nedragis.Otrasl>(model.Sphere?.Id);
            // считываем адрес для свойства Юридический адрес
            entity.LegalAddress = RestoreAddress(model.LegalAddress);
        }
    }
}