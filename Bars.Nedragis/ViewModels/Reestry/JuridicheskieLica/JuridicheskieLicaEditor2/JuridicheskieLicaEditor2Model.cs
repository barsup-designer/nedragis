namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'ЮЛ Добавление' для отдачи на клиент
    /// </summary>
    public class JuridicheskieLicaEditor2Model : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public JuridicheskieLicaEditor2Model()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'ИНН'
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("ee093447-7d50-459c-9419-e5c195fec1df")]
        public virtual System.String INN1
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'КПП'
        /// </summary>
        [Bars.B4.Utils.Display("КПП")]
        [Bars.Rms.Core.Attributes.Uid("ee06b6ca-f1f7-498d-9161-ef32f8a05538")]
        public virtual System.String KPP1
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Название субъекта'
        /// </summary>
        [Bars.B4.Utils.Display("Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("e0a03cbc-809a-45d0-ac6a-451ea1f16e00")]
        public virtual System.String Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Полное наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Полное наименование")]
        [Bars.Rms.Core.Attributes.Uid("058b1cb7-d4eb-4c12-bc7b-bbe80dbfd0b6")]
        public virtual System.String Fullname
        {
            get;
            set;
        }

        /// <summary>
        /// Отрасль
        /// </summary>
        [Bars.B4.Utils.Display("Отрасль")]
        [Bars.Rms.Core.Attributes.Uid("a1e595f4-eca6-4cf3-b6e1-248c1535a803")]
        public virtual Bars.Nedragis.OtraslListModel Sphere
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Юридический адрес'
        /// </summary>
        [Bars.B4.Utils.Display("Юридический адрес")]
        [Bars.Rms.Core.Attributes.Uid("2cc25b53-5924-4f99-800e-f2d2b10086d5")]
        public virtual Bars.B4.Modules.FIAS.FiasAddress LegalAddress
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Модель для отображения свойства 'Юридический адрес'
    /// </summary>
    public class JuridicheskieLicaEditor2LegalAddressControlModel
    {
        /// <summary>
        /// Свойство 'Юридические лица.Юридический адрес.Идентификатор'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Юридические лица.Юридический адрес.Идентификатор")]
        public System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Юридические лица.Юридический адрес.AddressName'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Юридические лица.Юридический адрес.AddressName")]
        public System.String AddressName
        {
            get;
            set;
        }
    }
}