namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'ЮЛ редактирование - навигация'
    /// </summary>
    public interface IJuLRedaktirovanieViewModel : IEntityEditorViewModel<Bars.Nedragis.JuridicheskieLica, JuLRedaktirovanieModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'ЮЛ редактирование - навигация'
    /// </summary>
    public interface IJuLRedaktirovanieValidator : IEditorModelValidator<JuLRedaktirovanieModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'ЮЛ редактирование - навигация'
    /// </summary>
    public interface IJuLRedaktirovanieHandler : IEntityEditorViewModelHandler<Bars.Nedragis.JuridicheskieLica, JuLRedaktirovanieModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'ЮЛ редактирование - навигация'
    /// </summary>
    public abstract class AbstractJuLRedaktirovanieHandler : EntityEditorViewModelHandler<Bars.Nedragis.JuridicheskieLica, JuLRedaktirovanieModel>, IJuLRedaktirovanieHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'ЮЛ редактирование - навигация'
    /// </summary>
    public class JuLRedaktirovanieViewModel : BaseEditorViewModel<Bars.Nedragis.JuridicheskieLica, JuLRedaktirovanieModel>, IJuLRedaktirovanieViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override JuLRedaktirovanieModel CreateModelInternal(BaseParams @params)
        {
            var model = new JuLRedaktirovanieModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Юридические лица' в модель представления
        /// </summary>
        protected override JuLRedaktirovanieModel MapEntityInternal(Bars.Nedragis.JuridicheskieLica entity)
        {
            // создаем экзепляр модели
            var model = new JuLRedaktirovanieModel();
            model.Id = entity.Id;
            model.Representation = entity.Representation;
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Юридические лица' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.JuridicheskieLica entity, JuLRedaktirovanieModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
        }
    }
}