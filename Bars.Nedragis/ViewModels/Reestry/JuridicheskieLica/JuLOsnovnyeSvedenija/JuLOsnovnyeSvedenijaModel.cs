namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'ЮЛ основные сведения' для отдачи на клиент
    /// </summary>
    public class JuLOsnovnyeSvedenijaModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public JuLOsnovnyeSvedenijaModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Название субъекта'
        /// </summary>
        [Bars.B4.Utils.Display("Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("e40902fd-b8e1-4e61-bfad-250d2eae191d")]
        public virtual System.String Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Состояние
        /// </summary>
        [Bars.B4.Utils.Display("Состояние")]
        [Bars.Rms.Core.Attributes.Uid("a587138d-9851-4e57-8add-04bfd0ce69d6")]
        public virtual Bars.Nedragis.StatusySubEktaListModel Status
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Полное наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Полное наименование")]
        [Bars.Rms.Core.Attributes.Uid("d2cee3fe-c557-41d3-9c01-e3460419cfe4")]
        public virtual System.String Fullname
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'ИНН'
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("226151b7-eab2-4c34-9d59-2eeab6836aab")]
        public virtual System.String INN1
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'КПП'
        /// </summary>
        [Bars.B4.Utils.Display("КПП")]
        [Bars.Rms.Core.Attributes.Uid("5e7a9850-72d0-4d23-bbc9-efa74c104494")]
        public virtual System.String KPP1
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'ОГРН'
        /// </summary>
        [Bars.B4.Utils.Display("ОГРН")]
        [Bars.Rms.Core.Attributes.Uid("01dc7136-3473-4345-ad96-a178dc156c61")]
        public virtual System.String ogrn
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'ОКПО'
        /// </summary>
        [Bars.B4.Utils.Display("ОКПО")]
        [Bars.Rms.Core.Attributes.Uid("0d0a8528-9ae0-414a-bd18-6a3690aeab4c")]
        public virtual System.String Okpo
        {
            get;
            set;
        }

        /// <summary>
        /// ОКОПФ
        /// </summary>
        [Bars.B4.Utils.Display("ОКОПФ")]
        [Bars.Rms.Core.Attributes.Uid("c2771835-75f9-4ed0-ba1c-ce9580638df0")]
        public virtual Bars.Nedragis.OKOPFListModel Okopf
        {
            get;
            set;
        }

        /// <summary>
        /// ОКВЭД
        /// </summary>
        [Bars.B4.Utils.Display("ОКВЭД")]
        [Bars.Rms.Core.Attributes.Uid("361585fc-af88-41b7-82b9-f08dea077e54")]
        public virtual Bars.Nedragis.OKVEhD1Model Okved
        {
            get;
            set;
        }

        /// <summary>
        /// ОКОГУ
        /// </summary>
        [Bars.B4.Utils.Display("ОКОГУ")]
        [Bars.Rms.Core.Attributes.Uid("faf39e28-2488-4b6c-a05f-008f6cc07357")]
        public virtual Bars.Nedragis.OKOGU1Model Okogu
        {
            get;
            set;
        }

        /// <summary>
        /// ОКФС
        /// </summary>
        [Bars.B4.Utils.Display("ОКФС")]
        [Bars.Rms.Core.Attributes.Uid("43878213-0b39-409f-9467-2958bf88baed")]
        public virtual Bars.Nedragis.OKFSListModel Okfs
        {
            get;
            set;
        }

        /// <summary>
        /// ОКАТО
        /// </summary>
        [Bars.B4.Utils.Display("ОКАТО")]
        [Bars.Rms.Core.Attributes.Uid("1534fb85-e40c-49d6-90a0-6283f65db42e")]
        public virtual Bars.Nedragis.OKATO1Model OKATO
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата гос. регистрации'
        /// </summary>
        [Bars.B4.Utils.Display("Дата гос. регистрации")]
        [Bars.Rms.Core.Attributes.Uid("f940a7be-044b-4a73-af97-cf3ef679d303")]
        public virtual System.DateTime? DateReg
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Размер уставного фонда организации'
        /// </summary>
        [Bars.B4.Utils.Display("Размер уставного фонда организации")]
        [Bars.Rms.Core.Attributes.Uid("382274ab-a29f-4fed-8a9b-64d08f206f0e")]
        public virtual System.Decimal? CapitalStock
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Среднесписочный состав'
        /// </summary>
        [Bars.B4.Utils.Display("Среднесписочный состав")]
        [Bars.Rms.Core.Attributes.Uid("116562d3-0c7b-40d0-9d51-0670ee9bfb21")]
        public virtual System.Int32? CapitalPeople
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Юридический адрес'
        /// </summary>
        [Bars.B4.Utils.Display("Юридический адрес")]
        [Bars.Rms.Core.Attributes.Uid("2cc25b53-5924-4f99-800e-f2d2b10086d5")]
        public virtual Bars.B4.Modules.FIAS.FiasAddress LegalAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Фактический адрес'
        /// </summary>
        [Bars.B4.Utils.Display("Фактический адрес")]
        [Bars.Rms.Core.Attributes.Uid("a932425e-fa61-4da0-9baa-2057bf1741c6")]
        public virtual Bars.B4.Modules.FIAS.FiasAddress FactAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Телефон'
        /// </summary>
        [Bars.B4.Utils.Display("Телефон")]
        [Bars.Rms.Core.Attributes.Uid("1f5749ee-74c8-4c98-8e9f-b9b32ade6252")]
        public virtual System.String Phone
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Факс'
        /// </summary>
        [Bars.B4.Utils.Display("Факс")]
        [Bars.Rms.Core.Attributes.Uid("6b240d10-e2e4-4e64-a80e-583c2f0bc9a9")]
        public virtual System.String Fax
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Email'
        /// </summary>
        [Bars.B4.Utils.Display("Email")]
        [Bars.Rms.Core.Attributes.Uid("e9f42fed-096c-45bb-b9e1-71156c074ee3")]
        public virtual System.String Email
        {
            get;
            set;
        }

        /// <summary>
        /// Отрасль
        /// </summary>
        [Bars.B4.Utils.Display("Отрасль")]
        [Bars.Rms.Core.Attributes.Uid("be7206b9-2d4f-4509-80c9-35b4cc86b3e4")]
        public virtual Bars.Nedragis.OtraslListModel Sphere
        {
            get;
            set;
        }

        /// <summary>
        /// Вышестоящая организация
        /// </summary>
        [Bars.B4.Utils.Display("Вышестоящая организация")]
        [Bars.Rms.Core.Attributes.Uid("c7f3c133-7236-4dd4-927e-448c93af59fc")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel HeadSubject
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Модель для отображения свойства 'Юридический адрес'
    /// </summary>
    public class JuLOsnovnyeSvedenijaLegalAddressControlModel
    {
        /// <summary>
        /// Свойство 'Юридические лица.Юридический адрес.Идентификатор'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Юридические лица.Юридический адрес.Идентификатор")]
        public System.Int64? Id
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Модель для отображения свойства 'Фактический адрес'
    /// </summary>
    public class JuLOsnovnyeSvedenijaFactAddressControlModel
    {
        /// <summary>
        /// Свойство 'Юридические лица.Фактический адрес.Идентификатор'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Юридические лица.Фактический адрес.Идентификатор")]
        public System.Int64? Id
        {
            get;
            set;
        }
    }
}