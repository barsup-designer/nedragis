namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'ЮЛ основные сведения'
    /// </summary>
    public interface IJuLOsnovnyeSvedenijaViewModel : IEntityEditorViewModel<Bars.Nedragis.JuridicheskieLica, JuLOsnovnyeSvedenijaModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'ЮЛ основные сведения'
    /// </summary>
    public interface IJuLOsnovnyeSvedenijaValidator : IEditorModelValidator<JuLOsnovnyeSvedenijaModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'ЮЛ основные сведения'
    /// </summary>
    public interface IJuLOsnovnyeSvedenijaHandler : IEntityEditorViewModelHandler<Bars.Nedragis.JuridicheskieLica, JuLOsnovnyeSvedenijaModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'ЮЛ основные сведения'
    /// </summary>
    public abstract class AbstractJuLOsnovnyeSvedenijaHandler : EntityEditorViewModelHandler<Bars.Nedragis.JuridicheskieLica, JuLOsnovnyeSvedenijaModel>, IJuLOsnovnyeSvedenijaHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'ЮЛ основные сведения'
    /// </summary>
    public class JuLOsnovnyeSvedenijaViewModel : BaseEditorViewModel<Bars.Nedragis.JuridicheskieLica, JuLOsnovnyeSvedenijaModel>, IJuLOsnovnyeSvedenijaViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override JuLOsnovnyeSvedenijaModel CreateModelInternal(BaseParams @params)
        {
            var model = new JuLOsnovnyeSvedenijaModel();
            var varStatusId = @params.Params.GetAs<long>("Status_Id", 0);
            if (varStatusId > 0)
            {
                model.Status = Container.Resolve<Bars.Nedragis.IStatusySubEktaListQuery>().GetById(varStatusId);
            }

            var varOkopfId = @params.Params.GetAs<long>("Okopf_Id", 0);
            if (varOkopfId > 0)
            {
                model.Okopf = Container.Resolve<Bars.Nedragis.IOKOPFListQuery>().GetById(varOkopfId);
            }

            var varOkvedId = @params.Params.GetAs<long>("Okved_Id", 0);
            if (varOkvedId > 0)
            {
                model.Okved = Container.Resolve<Bars.Nedragis.IOKVEhD1Query>().GetById(varOkvedId);
            }

            var varOkoguId = @params.Params.GetAs<long>("Okogu_Id", 0);
            if (varOkoguId > 0)
            {
                model.Okogu = Container.Resolve<Bars.Nedragis.IOKOGU1Query>().GetById(varOkoguId);
            }

            var varOkfsId = @params.Params.GetAs<long>("Okfs_Id", 0);
            if (varOkfsId > 0)
            {
                model.Okfs = Container.Resolve<Bars.Nedragis.IOKFSListQuery>().GetById(varOkfsId);
            }

            var varOKATOId = @params.Params.GetAs<long>("OKATO_Id", 0);
            if (varOKATOId > 0)
            {
                model.OKATO = Container.Resolve<Bars.Nedragis.IOKATO1Query>().GetById(varOKATOId);
            }

            // получение адреса из свойства Юридический адрес
            var varLegalAddressId = @params.Params.GetAs<long ? >("LegalAddress_Id");
            model.LegalAddress = TryGetEntityById<Bars.B4.Modules.FIAS.FiasAddress>(varLegalAddressId);
            // получение адреса из свойства Фактический адрес
            var varFactAddressId = @params.Params.GetAs<long ? >("FactAddress_Id");
            model.FactAddress = TryGetEntityById<Bars.B4.Modules.FIAS.FiasAddress>(varFactAddressId);
            var varSphereId = @params.Params.GetAs<long>("Sphere_Id", 0);
            if (varSphereId > 0)
            {
                model.Sphere = Container.Resolve<Bars.Nedragis.IOtraslListQuery>().GetById(varSphereId);
            }

            var varHeadSubjectId = @params.Params.GetAs<long>("HeadSubject_Id", 0);
            if (varHeadSubjectId > 0)
            {
                model.HeadSubject = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>().GetById(varHeadSubjectId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Юридические лица' в модель представления
        /// </summary>
        protected override JuLOsnovnyeSvedenijaModel MapEntityInternal(Bars.Nedragis.JuridicheskieLica entity)
        {
            // создаем экзепляр модели
            var model = new JuLOsnovnyeSvedenijaModel();
            model.Id = entity.Id;
            model.Representation = (System.String)(entity.Representation);
            if (entity.Status.IsNotNull())
            {
                var queryStatusySubEktaList = Container.Resolve<Bars.Nedragis.IStatusySubEktaListQuery>();
                model.Status = queryStatusySubEktaList.GetById(entity.Status.Id);
            }

            model.Fullname = (System.String)(entity.Fullname);
            model.INN1 = (System.String)(entity.INN1);
            model.KPP1 = (System.String)(entity.KPP1);
            model.ogrn = (System.String)(entity.ogrn);
            model.Okpo = (System.String)(entity.Okpo);
            if (entity.Okopf.IsNotNull())
            {
                var queryOKOPFList = Container.Resolve<Bars.Nedragis.IOKOPFListQuery>();
                model.Okopf = queryOKOPFList.GetById(entity.Okopf.Id);
            }

            if (entity.Okved.IsNotNull())
            {
                var queryOKVEhD1 = Container.Resolve<Bars.Nedragis.IOKVEhD1Query>();
                model.Okved = queryOKVEhD1.GetById(entity.Okved.Id);
            }

            if (entity.Okogu.IsNotNull())
            {
                var queryOKOGU1 = Container.Resolve<Bars.Nedragis.IOKOGU1Query>();
                model.Okogu = queryOKOGU1.GetById(entity.Okogu.Id);
            }

            if (entity.Okfs.IsNotNull())
            {
                var queryOKFSList = Container.Resolve<Bars.Nedragis.IOKFSListQuery>();
                model.Okfs = queryOKFSList.GetById(entity.Okfs.Id);
            }

            if (entity.OKATO.IsNotNull())
            {
                var queryOKATO1 = Container.Resolve<Bars.Nedragis.IOKATO1Query>();
                model.OKATO = queryOKATO1.GetById(entity.OKATO.Id);
            }

            model.DateReg = (System.DateTime? )(entity.DateReg);
            model.CapitalStock = (System.Decimal? )(entity.CapitalStock);
            model.CapitalPeople = (System.Int32? )(entity.CapitalPeople);
            model.LegalAddress = entity.LegalAddress;
            model.FactAddress = entity.FactAddress;
            model.Phone = (System.String)(entity.Phone);
            model.Fax = (System.String)(entity.Fax);
            model.Email = (System.String)(entity.Email);
            if (entity.Sphere.IsNotNull())
            {
                var queryOtraslList = Container.Resolve<Bars.Nedragis.IOtraslListQuery>();
                model.Sphere = queryOtraslList.GetById(entity.Sphere.Id);
            }

            if (entity.HeadSubject.IsNotNull())
            {
                var queryJuridicheskieLicaList = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>();
                model.HeadSubject = queryJuridicheskieLicaList.GetById(entity.HeadSubject.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Юридические лица' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.JuridicheskieLica entity, JuLOsnovnyeSvedenijaModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Representation = model.Representation;
            entity.Status = TryLoadEntityById<Bars.Nedragis.StatusySubEkta>(model.Status?.Id);
            entity.Fullname = model.Fullname;
            entity.INN1 = model.INN1;
            entity.KPP1 = model.KPP1;
            entity.ogrn = model.ogrn;
            entity.Okpo = model.Okpo;
            entity.Okopf = TryLoadEntityById<Bars.Nedragis.OKOPF>(model.Okopf?.Id);
            entity.Okved = TryLoadEntityById<Bars.Nedragis.OKVEhD>(model.Okved?.Id);
            entity.Okogu = TryLoadEntityById<Bars.Nedragis.OKOGU>(model.Okogu?.Id);
            entity.Okfs = TryLoadEntityById<Bars.Nedragis.OKFS>(model.Okfs?.Id);
            entity.OKATO = TryLoadEntityById<Bars.Nedragis.OKATO>(model.OKATO?.Id);
            entity.DateReg = model.DateReg.GetValueOrDefault();
            if (model.CapitalStock.HasValue)
            {
                entity.CapitalStock = model.CapitalStock.Value;
            }

            if (model.CapitalPeople.HasValue)
            {
                entity.CapitalPeople = model.CapitalPeople.Value;
            }

            // считываем адрес для свойства Юридический адрес
            entity.LegalAddress = RestoreAddress(model.LegalAddress);
            // считываем адрес для свойства Фактический адрес
            entity.FactAddress = RestoreAddress(model.FactAddress);
            entity.Phone = model.Phone;
            entity.Fax = model.Fax;
            entity.Email = model.Email;
            entity.Sphere = TryLoadEntityById<Bars.Nedragis.Otrasl>(model.Sphere?.Id);
            entity.HeadSubject = TryLoadEntityById<Bars.Nedragis.JuridicheskieLica>(model.HeadSubject?.Id);
        }
    }
}