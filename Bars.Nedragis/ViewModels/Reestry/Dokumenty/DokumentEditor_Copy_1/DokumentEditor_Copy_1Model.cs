namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Основные сведения Документ ' для отдачи на клиент
    /// </summary>
    public class DokumentEditor_Copy_1Model : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public DokumentEditor_Copy_1Model()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Тип документа
        /// </summary>
        [Bars.B4.Utils.Display("Тип документа")]
        [Bars.Rms.Core.Attributes.Uid("a8749fcd-8b0d-4655-b9f5-e3c0af2fa223")]
        public virtual Bars.Nedragis.TipyDokumentovListModel Type
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата документа'
        /// </summary>
        [Bars.B4.Utils.Display("Дата документа")]
        [Bars.Rms.Core.Attributes.Uid("c16e779d-305c-4e06-a140-82b4df0f3bac")]
        public virtual System.DateTime? Date
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер документа'
        /// </summary>
        [Bars.B4.Utils.Display("Номер документа")]
        [Bars.Rms.Core.Attributes.Uid("cb3b87ed-7513-4975-bc75-153917085536")]
        public virtual System.String Number
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата вступления в силу'
        /// </summary>
        [Bars.B4.Utils.Display("Дата вступления в силу")]
        [Bars.Rms.Core.Attributes.Uid("2d263061-3ad0-45d6-b64b-9edffe3d3fb5")]
        public virtual System.DateTime? ActualDate
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Основание'
        /// </summary>
        [Bars.B4.Utils.Display("Основание")]
        [Bars.Rms.Core.Attributes.Uid("6a23a366-39e9-40b4-bdd5-d8bcfd6ee444")]
        public virtual System.String Basis
        {
            get;
            set;
        }

        /// <summary>
        /// Орган подписавший документ
        /// </summary>
        [Bars.B4.Utils.Display("Орган подписавший документ")]
        [Bars.Rms.Core.Attributes.Uid("43033e48-e34e-4f21-b674-2c4d4d9e86f4")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel Signatory
        {
            get;
            set;
        }

        /// <summary>
        /// Субъект
        /// </summary>
        [Bars.B4.Utils.Display("Субъект")]
        [Bars.Rms.Core.Attributes.Uid("12844f70-0076-4410-8fcf-54b6c75c7e32")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel Subject
        {
            get;
            set;
        }
    }
}