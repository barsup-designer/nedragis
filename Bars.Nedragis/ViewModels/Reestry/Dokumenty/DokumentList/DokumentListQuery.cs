namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Документ'
    /// </summary>
    public interface IDokumentListQuery : IQueryOperation<Bars.Nedragis.Dokument, Bars.Nedragis.DokumentListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Документ'
    /// </summary>
    public interface IDokumentListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Dokument, Bars.Nedragis.DokumentListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Документ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Документ")]
    public class DokumentListQuery : RmsEntityQueryOperation<Bars.Nedragis.Dokument, Bars.Nedragis.DokumentListModel>, IDokumentListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "DokumentListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public DokumentListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Dokument> Filter(IQueryable<Bars.Nedragis.Dokument> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.DokumentListModel> Map(IQueryable<Bars.Nedragis.Dokument> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Dokument>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.DokumentListModel{Id = x.Id, _TypeUid = "bb2ade91-5fc2-4846-8ee8-8b717abf46fb", Type_name = (System.String)(x.Type.name), Number = (System.String)(x.Number), Date = (System.DateTime? )(x.Date), Basis = (System.String)(x.Basis), Subject_Representation = (System.String)(x.Subject.Representation), });
        }
    }
}