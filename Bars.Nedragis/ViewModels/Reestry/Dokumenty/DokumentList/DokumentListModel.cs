namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Документ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Документ")]
    public class DokumentListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Тип документа.Наименование' (псевдоним: Type_name)
        /// </summary>
        [Bars.B4.Utils.Display("Тип документа.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("45f37bd3-781b-41da-b783-d73673f11fb7")]
        public virtual System.String Type_name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер документа' (псевдоним: Number)
        /// </summary>
        [Bars.B4.Utils.Display("Номер документа")]
        [Bars.Rms.Core.Attributes.Uid("2bf6e712-a570-4af8-8537-c6d929fd6fe6")]
        public virtual System.String Number
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата документа' (псевдоним: Date)
        /// </summary>
        [Bars.B4.Utils.Display("Дата документа")]
        [Bars.Rms.Core.Attributes.Uid("982a8c78-5222-42b7-bb6b-b2f80847c813")]
        public virtual System.DateTime? Date
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Основание' (псевдоним: Basis)
        /// </summary>
        [Bars.B4.Utils.Display("Основание")]
        [Bars.Rms.Core.Attributes.Uid("8010b5fc-fadc-4004-ad72-5bc4f10f0825")]
        public virtual System.String Basis
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Субъект.Название субъекта' (псевдоним: Subject_Representation)
        /// </summary>
        [Bars.B4.Utils.Display("Субъект.Название субъекта")]
        [Bars.Rms.Core.Attributes.Uid("f6ee4a03-7641-485d-93be-3571d6e50d4c")]
        public virtual System.String Subject_Representation
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}