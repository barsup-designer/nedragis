namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Документы - навигация'
    /// </summary>
    public interface IDokumentyNavigacijaViewModel : IEntityEditorViewModel<Bars.Nedragis.Dokument, DokumentyNavigacijaModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Документы - навигация'
    /// </summary>
    public interface IDokumentyNavigacijaValidator : IEditorModelValidator<DokumentyNavigacijaModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Документы - навигация'
    /// </summary>
    public interface IDokumentyNavigacijaHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Dokument, DokumentyNavigacijaModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Документы - навигация'
    /// </summary>
    public abstract class AbstractDokumentyNavigacijaHandler : EntityEditorViewModelHandler<Bars.Nedragis.Dokument, DokumentyNavigacijaModel>, IDokumentyNavigacijaHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Документы - навигация'
    /// </summary>
    public class DokumentyNavigacijaViewModel : BaseEditorViewModel<Bars.Nedragis.Dokument, DokumentyNavigacijaModel>, IDokumentyNavigacijaViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override DokumentyNavigacijaModel CreateModelInternal(BaseParams @params)
        {
            var model = new DokumentyNavigacijaModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Документ' в модель представления
        /// </summary>
        protected override DokumentyNavigacijaModel MapEntityInternal(Bars.Nedragis.Dokument entity)
        {
            // создаем экзепляр модели
            var model = new DokumentyNavigacijaModel();
            model.Id = entity.Id;
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Документ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Dokument entity, DokumentyNavigacijaModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
        }
    }
}