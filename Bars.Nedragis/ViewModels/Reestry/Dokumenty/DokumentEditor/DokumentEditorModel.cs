namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Документ' для отдачи на клиент
    /// </summary>
    public class DokumentEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public DokumentEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Тип документа
        /// </summary>
        [Bars.B4.Utils.Display("Тип документа")]
        [Bars.Rms.Core.Attributes.Uid("1fca0c02-f035-4319-bc15-2a827b29d4c0")]
        public virtual Bars.Nedragis.TipyDokumentovListModel Type
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата документа'
        /// </summary>
        [Bars.B4.Utils.Display("Дата документа")]
        [Bars.Rms.Core.Attributes.Uid("f1c147fa-42d5-4f2f-8467-90be2c023460")]
        public virtual System.DateTime? Date
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер документа'
        /// </summary>
        [Bars.B4.Utils.Display("Номер документа")]
        [Bars.Rms.Core.Attributes.Uid("90d69d7f-7054-4979-b389-acf15aba9cb6")]
        public virtual System.String Number
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата вступления в силу'
        /// </summary>
        [Bars.B4.Utils.Display("Дата вступления в силу")]
        [Bars.Rms.Core.Attributes.Uid("6748bec2-b33d-42eb-8e3a-cbb13af12b17")]
        public virtual System.DateTime? ActualDate
        {
            get;
            set;
        }

        /// <summary>
        /// Орган подписавший документ
        /// </summary>
        [Bars.B4.Utils.Display("Орган подписавший документ")]
        [Bars.Rms.Core.Attributes.Uid("68dcb983-1063-42e5-8119-8637636c62c8")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel Signatory
        {
            get;
            set;
        }

        /// <summary>
        /// Субъект
        /// </summary>
        [Bars.B4.Utils.Display("Субъект")]
        [Bars.Rms.Core.Attributes.Uid("f5febecb-309b-45e1-a77c-5e3dcebdd4df")]
        public virtual Bars.Nedragis.JuridicheskieLicaListModel Subject
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Основание'
        /// </summary>
        [Bars.B4.Utils.Display("Основание")]
        [Bars.Rms.Core.Attributes.Uid("2883f083-cff4-4ccc-8dbb-0c5c189a38ba")]
        public virtual System.String Basis
        {
            get;
            set;
        }
    }
}