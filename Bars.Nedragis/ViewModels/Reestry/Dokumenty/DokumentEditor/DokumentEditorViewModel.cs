namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Документ'
    /// </summary>
    public interface IDokumentEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Dokument, DokumentEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Документ'
    /// </summary>
    public interface IDokumentEditorValidator : IEditorModelValidator<DokumentEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Документ'
    /// </summary>
    public interface IDokumentEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Dokument, DokumentEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Документ'
    /// </summary>
    public abstract class AbstractDokumentEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Dokument, DokumentEditorModel>, IDokumentEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Документ'
    /// </summary>
    public class DokumentEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Dokument, DokumentEditorModel>, IDokumentEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override DokumentEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new DokumentEditorModel();
            var varTypeId = @params.Params.GetAs<long>("Type_Id", 0);
            if (varTypeId > 0)
            {
                model.Type = Container.Resolve<Bars.Nedragis.ITipyDokumentovListQuery>().GetById(varTypeId);
            }

            var varSignatoryId = @params.Params.GetAs<long>("Signatory_Id", 0);
            if (varSignatoryId > 0)
            {
                model.Signatory = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>().GetById(varSignatoryId);
            }

            var varSubjectId = @params.Params.GetAs<long>("Subject_Id", 0);
            if (varSubjectId > 0)
            {
                model.Subject = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>().GetById(varSubjectId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Документ' в модель представления
        /// </summary>
        protected override DokumentEditorModel MapEntityInternal(Bars.Nedragis.Dokument entity)
        {
            // создаем экзепляр модели
            var model = new DokumentEditorModel();
            model.Id = entity.Id;
            if (entity.Type.IsNotNull())
            {
                var queryTipyDokumentovList = Container.Resolve<Bars.Nedragis.ITipyDokumentovListQuery>();
                model.Type = queryTipyDokumentovList.GetById(entity.Type.Id);
            }

            model.Date = (System.DateTime? )(entity.Date);
            model.Number = (System.String)(entity.Number);
            model.ActualDate = (System.DateTime? )(entity.ActualDate);
            if (entity.Signatory.IsNotNull())
            {
                var queryJuridicheskieLicaList = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>();
                model.Signatory = queryJuridicheskieLicaList.GetById(entity.Signatory.Id);
            }

            if (entity.Subject.IsNotNull())
            {
                var queryJuridicheskieLicaList = Container.Resolve<Bars.Nedragis.IJuridicheskieLicaListQuery>();
                model.Subject = queryJuridicheskieLicaList.GetById(entity.Subject.Id);
            }

            model.Basis = (System.String)(entity.Basis);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Документ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Dokument entity, DokumentEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Type = TryLoadEntityById<Bars.Nedragis.TipyDokumentov>(model.Type?.Id);
            entity.Date = model.Date.GetValueOrDefault();
            entity.Number = model.Number;
            entity.ActualDate = model.ActualDate.GetValueOrDefault();
            entity.Signatory = TryLoadEntityById<Bars.Nedragis.SubEktyPrava>(model.Signatory?.Id);
            entity.Subject = TryLoadEntityById<Bars.Nedragis.JuridicheskieLica>(model.Subject?.Id);
            entity.Basis = model.Basis;
        }
    }
}