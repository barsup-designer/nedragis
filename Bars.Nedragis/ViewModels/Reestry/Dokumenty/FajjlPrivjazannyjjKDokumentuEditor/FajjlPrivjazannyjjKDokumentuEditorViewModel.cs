namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Файл, привязанный к документу'
    /// </summary>
    public interface IFajjlPrivjazannyjjKDokumentuEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu, FajjlPrivjazannyjjKDokumentuEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Файл, привязанный к документу'
    /// </summary>
    public interface IFajjlPrivjazannyjjKDokumentuEditorValidator : IEditorModelValidator<FajjlPrivjazannyjjKDokumentuEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Файл, привязанный к документу'
    /// </summary>
    public interface IFajjlPrivjazannyjjKDokumentuEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu, FajjlPrivjazannyjjKDokumentuEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Файл, привязанный к документу'
    /// </summary>
    public abstract class AbstractFajjlPrivjazannyjjKDokumentuEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu, FajjlPrivjazannyjjKDokumentuEditorModel>, IFajjlPrivjazannyjjKDokumentuEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Файл, привязанный к документу'
    /// </summary>
    public class FajjlPrivjazannyjjKDokumentuEditorViewModel : BaseEditorViewModel<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu, FajjlPrivjazannyjjKDokumentuEditorModel>, IFajjlPrivjazannyjjKDokumentuEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override FajjlPrivjazannyjjKDokumentuEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new FajjlPrivjazannyjjKDokumentuEditorModel();
            // получение параметров файла из свойства Запись файла
            model.FileInfo = TryGetEntityById<Bars.B4.Modules.FileStorage.FileInfo>(@params.Params.GetAs<long ? >("FileInfo_Id"));
            var varDocumentId = @params.Params.GetAs<long>("Document_Id", 0);
            if (varDocumentId > 0)
            {
                model.Document = Container.Resolve<Bars.Nedragis.IDokumentListQuery>().GetById(varDocumentId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Файл, привязанный к документу' в модель представления
        /// </summary>
        protected override FajjlPrivjazannyjjKDokumentuEditorModel MapEntityInternal(Bars.Nedragis.FajjlPrivjazannyjjKDokumentu entity)
        {
            // создаем экзепляр модели
            var model = new FajjlPrivjazannyjjKDokumentuEditorModel();
            model.Id = entity.Id;
            model.FileInfo = entity.FileInfo;
            model.Comment = (System.String)(entity.Comment);
            if (entity.Document.IsNotNull())
            {
                var queryDokumentList = Container.Resolve<Bars.Nedragis.IDokumentListQuery>();
                model.Document = queryDokumentList.GetById(entity.Document.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Файл, привязанный к документу' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.FajjlPrivjazannyjjKDokumentu entity, FajjlPrivjazannyjjKDokumentuEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Comment = model.Comment;
            entity.Document = TryLoadEntityById<Bars.Nedragis.Dokument>(model.Document?.Id);
            // считываем файл Запись файла
            entity.FileInfo = RestoreFile(model.FileInfo, entity.FileInfo, requestFiles.Get("FileInfo"), filesToDelete);
        }
    }
}