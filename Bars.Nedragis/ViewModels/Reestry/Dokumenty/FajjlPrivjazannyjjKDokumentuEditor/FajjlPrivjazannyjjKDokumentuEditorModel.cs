namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Файл, привязанный к документу' для отдачи на клиент
    /// </summary>
    public class FajjlPrivjazannyjjKDokumentuEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public FajjlPrivjazannyjjKDokumentuEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Запись файла
        /// </summary>
        [Bars.B4.Utils.Display("Запись файла")]
        [Bars.Rms.Core.Attributes.Uid("FileInfo")]
        public virtual Bars.B4.Modules.FileStorage.FileInfo FileInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Комментарий'
        /// </summary>
        [Bars.B4.Utils.Display("Комментарий")]
        [Bars.Rms.Core.Attributes.Uid("a1c2dfa6-d4e0-4d75-b6a9-4ae8de64ad6b")]
        public virtual System.String Comment
        {
            get;
            set;
        }

        /// <summary>
        /// Документ
        /// </summary>
        [Bars.B4.Utils.Display("Документ")]
        [Bars.Rms.Core.Attributes.Uid("58e76208-de70-4264-aa76-ccfee9c9b2c8")]
        public virtual Bars.Nedragis.DokumentListModel Document
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Модель для отображения свойства 'Запись файла'
    /// </summary>
    public class FajjlPrivjazannyjjKDokumentuEditorFileInfoControlModel
    {
        /// <summary>
        /// Свойство 'Файл, привязанный к документу.Запись файла.Идентификатор'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Файл, привязанный к документу.Запись файла.Идентификатор")]
        public System.Int64? Id
        {
            get;
            set;
        }
    }
}