namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Файл, привязанный к документу'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Файл, привязанный к документу")]
    public class FajjlPrivjazannyjjKDokumentuListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Запись файла.Наименование' (псевдоним: FileInfo_Name)
        /// </summary>
        [Bars.B4.Utils.Display("Запись файла.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("f9367297-53a4-49a3-9cfa-d354f56ad85d")]
        public virtual System.String FileInfo_Name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Комментарий' (псевдоним: Comment)
        /// </summary>
        [Bars.B4.Utils.Display("Комментарий")]
        [Bars.Rms.Core.Attributes.Uid("bda32d7c-59bd-4aae-95da-4214a5d96e51")]
        public virtual System.String Comment
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Запись файла.Размер' (псевдоним: FileInfo_Size)
        /// </summary>
        [Bars.B4.Utils.Display("Запись файла.Размер")]
        [Bars.Rms.Core.Attributes.Uid("14fdc9ff-9908-426f-8102-50138a8c56a8")]
        public virtual System.Int64? FileInfo_Size
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Запись файла.Контент' (псевдоним: FileInfo_Content)
        /// </summary>
        [Bars.B4.Utils.Display("Скачать файл")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.Modules.FileStorage.FileInfo, Bars.B4.Modules.FileStorage/Content")]
        public virtual System.Int64? FileInfo_Content
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}