namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Файл, привязанный к документу'
    /// </summary>
    public interface IFajjlPrivjazannyjjKDokumentuListQuery : IQueryOperation<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu, Bars.Nedragis.FajjlPrivjazannyjjKDokumentuListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Файл, привязанный к документу'
    /// </summary>
    public interface IFajjlPrivjazannyjjKDokumentuListQueryFilter : IQueryOperationFilter<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu, Bars.Nedragis.FajjlPrivjazannyjjKDokumentuListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Файл, привязанный к документу'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Файл, привязанный к документу")]
    public class FajjlPrivjazannyjjKDokumentuListQuery : RmsEntityQueryOperation<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu, Bars.Nedragis.FajjlPrivjazannyjjKDokumentuListModel>, IFajjlPrivjazannyjjKDokumentuListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "FajjlPrivjazannyjjKDokumentuListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public FajjlPrivjazannyjjKDokumentuListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu> Filter(IQueryable<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.FajjlPrivjazannyjjKDokumentuListModel> Map(IQueryable<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.FajjlPrivjazannyjjKDokumentuListModel{Id = x.Id, _TypeUid = "55caa4f7-4f96-47d2-a66a-4d16b044a7cf", FileInfo_Name = (System.String)(x.FileInfo.Name), Comment = (System.String)(x.Comment), FileInfo_Size = (System.Int64? )(x.FileInfo.Size), FileInfo_Content = (System.Int64)(x.FileInfo.Id), });
        }
    }
}