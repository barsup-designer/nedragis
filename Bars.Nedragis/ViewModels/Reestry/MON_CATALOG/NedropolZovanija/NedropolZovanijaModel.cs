namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма отдела "Недропользования"' для отдачи на клиент
    /// </summary>
    public class NedropolZovanijaModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public NedropolZovanijaModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("dc699826-6dfa-41ea-8be7-7765f6889497")]
        public virtual Bars.Nedragis.Otdel1ListModel otdel
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дело скважин(кол.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Дело скважин(кол.об.)")]
        [Bars.Rms.Core.Attributes.Uid("65234051-bc16-40bf-9a5f-5688892a8311")]
        public virtual System.Int32? Dela_o
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дела скважин (д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Дела скважин (д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("94705789-fdeb-4b32-a05a-2cfbe610a649")]
        public virtual System.Decimal? Dela_od
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Каротаж - сканобраз (количество обращений)'
        /// </summary>
        [Bars.B4.Utils.Display("Каротаж - сканобраз (количество обращений)")]
        [Bars.Rms.Core.Attributes.Uid("e1cf3259-c6cc-468e-b864-e5b84f7a407e")]
        public virtual System.Int32? Karotazs_o
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Каротаж-сканобраз (денежные средства)'
        /// </summary>
        [Bars.B4.Utils.Display("Каротаж-сканобраз (денежные средства)")]
        [Bars.Rms.Core.Attributes.Uid("b3011697-7234-412a-a447-fe8c26713107")]
        public virtual System.Decimal? Karotazs_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Каротаж (оцифрованный формат LAS)_к.об.'
        /// </summary>
        [Bars.B4.Utils.Display("Каротаж (оцифрованный формат LAS)_к.об.")]
        [Bars.Rms.Core.Attributes.Uid("b8b32a23-8c1c-4f58-a5c1-69e9b937a088")]
        public virtual System.Int32? Karotazc
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Каротаж (оцифрованный формат LAS)_д.ср.'
        /// </summary>
        [Bars.B4.Utils.Display("Каротаж (оцифрованный формат LAS)_д.ср.")]
        [Bars.Rms.Core.Attributes.Uid("808a4673-5778-4622-9fcc-bc453b247b5b")]
        public virtual System.Decimal? Karotazc_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Геологические отчеты(количество обращений)'
        /// </summary>
        [Bars.B4.Utils.Display("Геологические отчеты(количество обращений)")]
        [Bars.Rms.Core.Attributes.Uid("827c8e43-147a-413f-920a-a6050f2fdf27")]
        public virtual System.Int32? geolog_otchet
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Геологические отчеты(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Геологические отчеты(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("5c903261-9d93-4437-94fb-f7f88ab18a67")]
        public virtual System.Decimal? geolog_otchet_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сейсмика: исходный полевой материал (к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмика: исходный полевой материал (к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("decc52eb-66cf-4c7f-a571-5d6a94a7e98d")]
        public virtual System.Int32? Element1462359767016
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сейсмика: исходный полевой материал (д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмика: исходный полевой материал (д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("6cb1bf1f-7992-4bca-bea8-9c5be5ba7a11")]
        public virtual System.Decimal? Element1462359769527
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сейсмические отчеты(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмические отчеты(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("3344683c-886b-4a84-823e-0a38d9b3d60b")]
        public virtual System.Int32? seismika_otchet
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сейсмические отчеты(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмические отчеты(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("91b3802b-aba4-4252-8254-24e08a54e0b2")]
        public virtual System.Decimal? seismika_otchet_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сейсмические отчеты без графического материала(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмические отчеты без графического материала(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("563dc628-6a2f-41b9-9fba-80be74382357")]
        public virtual System.Int32? seismika
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сейсмические отчеты без графического материала(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмические отчеты без графического материала(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("400642da-2188-4e14-8822-27e1b814c418")]
        public virtual System.Decimal? seismika_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Планы подсчетов запасов(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Планы подсчетов запасов(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("07183dc6-5e78-4693-b501-b0426319da32")]
        public virtual System.Int32? zapas
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Планы подсчетов запасов(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Планы подсчетов запасов(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("18efe426-6709-4fac-8960-d4b1efbf0611")]
        public virtual System.Decimal? zapas_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Координаты скважины и альтитуды(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Координаты скважины и альтитуды(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("02affdb7-960b-4d32-af04-594826a56505")]
        public virtual System.Int32? koor
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Координаты скважины и альтитуды(д.ж.)'
        /// </summary>
        [Bars.B4.Utils.Display("Координаты скважины и альтитуды(д.ж.)")]
        [Bars.Rms.Core.Attributes.Uid("ef59cad2-85ee-4bbd-8949-ea389eaeed42")]
        public virtual System.Decimal? koor_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сейсмика_кубы(кол.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмика_кубы(кол.об.)")]
        [Bars.Rms.Core.Attributes.Uid("27bbd992-319e-4614-b241-c6fb24831683")]
        public virtual System.Int32? kub
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сейсмика_кубы(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Сейсмика_кубы(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("37c3736e-cef7-4403-99bf-ba1b0229684f")]
        public virtual System.Decimal? kub_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Карты_н(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Карты_н(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("95d79017-e8e6-458f-884c-62c616b6b72f")]
        public virtual System.Int32? karta820
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Карты_н(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Карты_н(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("fe8b5216-1395-4f91-8ce8-e085185b29f7")]
        public virtual System.Decimal? karta_d
        {
            get;
            set;
        }
    }
}