namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма отдела "Недропользования"'
    /// </summary>
    public interface INedropolZovanijaViewModel : IEntityEditorViewModel<Bars.Nedragis.monitor, NedropolZovanijaModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма отдела "Недропользования"'
    /// </summary>
    public interface INedropolZovanijaValidator : IEditorModelValidator<NedropolZovanijaModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма отдела "Недропользования"'
    /// </summary>
    public interface INedropolZovanijaHandler : IEntityEditorViewModelHandler<Bars.Nedragis.monitor, NedropolZovanijaModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма отдела "Недропользования"'
    /// </summary>
    public abstract class AbstractNedropolZovanijaHandler : EntityEditorViewModelHandler<Bars.Nedragis.monitor, NedropolZovanijaModel>, INedropolZovanijaHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма отдела "Недропользования"'
    /// </summary>
    public class NedropolZovanijaViewModel : BaseEditorViewModel<Bars.Nedragis.monitor, NedropolZovanijaModel>, INedropolZovanijaViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override NedropolZovanijaModel CreateModelInternal(BaseParams @params)
        {
            var model = new NedropolZovanijaModel();
            var varotdelId = @params.Params.GetAs<long>("otdel_Id", 0);
            if (varotdelId > 0)
            {
                model.otdel = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>().GetById(varotdelId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Мониторинг' в модель представления
        /// </summary>
        protected override NedropolZovanijaModel MapEntityInternal(Bars.Nedragis.monitor entity)
        {
            // создаем экзепляр модели
            var model = new NedropolZovanijaModel();
            model.Id = entity.Id;
            if (entity.otdel.IsNotNull())
            {
                var queryOtdel1List = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>();
                model.otdel = queryOtdel1List.GetById(entity.otdel.Id);
            }

            model.Dela_o = (System.Int32? )(entity.Dela_o);
            model.Dela_od = (System.Decimal? )(entity.Dela_od);
            model.Karotazs_o = (System.Int32? )(entity.Karotazs_o);
            model.Karotazs_d = (System.Decimal? )(entity.Karotazs_d);
            model.Karotazc = (System.Int32? )(entity.Karotazc);
            model.Karotazc_d = (System.Decimal? )(entity.Karotazc_d);
            model.geolog_otchet = (System.Int32? )(entity.geolog_otchet);
            model.geolog_otchet_d = (System.Decimal? )(entity.geolog_otchet_d);
            model.Element1462359767016 = (System.Int32? )(entity.Element1462359767016);
            model.Element1462359769527 = (System.Decimal? )(entity.Element1462359769527);
            model.seismika_otchet = (System.Int32? )(entity.seismika_otchet);
            model.seismika_otchet_d = (System.Decimal? )(entity.seismika_otchet_d);
            model.seismika = (System.Int32? )(entity.seismika);
            model.seismika_d = (System.Decimal? )(entity.seismika_d);
            model.zapas = (System.Int32? )(entity.zapas);
            model.zapas_d = (System.Decimal? )(entity.zapas_d);
            model.koor = (System.Int32? )(entity.koor);
            model.koor_d = (System.Decimal? )(entity.koor_d);
            model.kub = (System.Int32? )(entity.kub);
            model.kub_d = (System.Decimal? )(entity.kub_d);
            model.karta820 = (System.Int32? )(entity.karta820);
            model.karta_d = (System.Decimal? )(entity.karta_d);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Мониторинг' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.monitor entity, NedropolZovanijaModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.otdel = TryLoadEntityById<Bars.Nedragis.Otdel1>(model.otdel?.Id);
            if (model.Dela_o.HasValue)
            {
                entity.Dela_o = model.Dela_o.Value;
            }

            if (model.Dela_od.HasValue)
            {
                entity.Dela_od = model.Dela_od.Value;
            }

            if (model.Karotazs_o.HasValue)
            {
                entity.Karotazs_o = model.Karotazs_o.Value;
            }

            if (model.Karotazs_d.HasValue)
            {
                entity.Karotazs_d = model.Karotazs_d.Value;
            }

            if (model.Karotazc.HasValue)
            {
                entity.Karotazc = model.Karotazc.Value;
            }

            if (model.Karotazc_d.HasValue)
            {
                entity.Karotazc_d = model.Karotazc_d.Value;
            }

            if (model.geolog_otchet.HasValue)
            {
                entity.geolog_otchet = model.geolog_otchet.Value;
            }

            if (model.geolog_otchet_d.HasValue)
            {
                entity.geolog_otchet_d = model.geolog_otchet_d.Value;
            }

            if (model.Element1462359767016.HasValue)
            {
                entity.Element1462359767016 = model.Element1462359767016.Value;
            }

            if (model.Element1462359769527.HasValue)
            {
                entity.Element1462359769527 = model.Element1462359769527.Value;
            }

            if (model.seismika_otchet.HasValue)
            {
                entity.seismika_otchet = model.seismika_otchet.Value;
            }

            if (model.seismika_otchet_d.HasValue)
            {
                entity.seismika_otchet_d = model.seismika_otchet_d.Value;
            }

            if (model.seismika.HasValue)
            {
                entity.seismika = model.seismika.Value;
            }

            if (model.seismika_d.HasValue)
            {
                entity.seismika_d = model.seismika_d.Value;
            }

            if (model.zapas.HasValue)
            {
                entity.zapas = model.zapas.Value;
            }

            if (model.zapas_d.HasValue)
            {
                entity.zapas_d = model.zapas_d.Value;
            }

            if (model.koor.HasValue)
            {
                entity.koor = model.koor.Value;
            }

            if (model.koor_d.HasValue)
            {
                entity.koor_d = model.koor_d.Value;
            }

            if (model.kub.HasValue)
            {
                entity.kub = model.kub.Value;
            }

            if (model.kub_d.HasValue)
            {
                entity.kub_d = model.kub_d.Value;
            }

            if (model.karta820.HasValue)
            {
                entity.karta820 = model.karta820.Value;
            }

            if (model.karta_d.HasValue)
            {
                entity.karta_d = model.karta_d.Value;
            }
        }
    }
}