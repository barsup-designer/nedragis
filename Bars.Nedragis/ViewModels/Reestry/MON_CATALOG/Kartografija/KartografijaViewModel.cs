namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма отдела "Картографии"'
    /// </summary>
    public interface IKartografijaViewModel : IEntityEditorViewModel<Bars.Nedragis.monitor, KartografijaModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма отдела "Картографии"'
    /// </summary>
    public interface IKartografijaValidator : IEditorModelValidator<KartografijaModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма отдела "Картографии"'
    /// </summary>
    public interface IKartografijaHandler : IEntityEditorViewModelHandler<Bars.Nedragis.monitor, KartografijaModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма отдела "Картографии"'
    /// </summary>
    public abstract class AbstractKartografijaHandler : EntityEditorViewModelHandler<Bars.Nedragis.monitor, KartografijaModel>, IKartografijaHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма отдела "Картографии"'
    /// </summary>
    public class KartografijaViewModel : BaseEditorViewModel<Bars.Nedragis.monitor, KartografijaModel>, IKartografijaViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override KartografijaModel CreateModelInternal(BaseParams @params)
        {
            var model = new KartografijaModel();
            var varotdelId = @params.Params.GetAs<long>("otdel_Id", 0);
            if (varotdelId > 0)
            {
                model.otdel = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>().GetById(varotdelId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Мониторинг' в модель представления
        /// </summary>
        protected override KartografijaModel MapEntityInternal(Bars.Nedragis.monitor entity)
        {
            // создаем экзепляр модели
            var model = new KartografijaModel();
            model.Id = entity.Id;
            if (entity.otdel.IsNotNull())
            {
                var queryOtdel1List = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>();
                model.otdel = queryOtdel1List.GetById(entity.otdel.Id);
            }

            model.Element1462359853756 = (System.Int32? )(entity.Element1462359853756);
            model.Element1462359855708 = (System.Decimal? )(entity.Element1462359855708);
            model.karta_raion = (System.Int32? )(entity.karta_raion);
            model.karta_raion_d = (System.Decimal? )(entity.karta_raion_d);
            model.karta_nedra = (System.Int32? )(entity.karta_nedra);
            model.karta_nedra_d = (System.Decimal? )(entity.karta_nedra_d);
            model.karta_n_p9622 = (System.Int32? )(entity.karta_n_p9622);
            model.karta_n_p6180 = (System.Decimal? )(entity.karta_n_p6180);
            model.shema = (System.Int32? )(entity.shema);
            model.shema_d = (System.Decimal? )(entity.shema_d);
            model.pecat_g_m = (System.Int32? )(entity.pecat_g_m);
            model.pecat_g_m_d = (System.Decimal? )(entity.pecat_g_m_d);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Мониторинг' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.monitor entity, KartografijaModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.otdel = TryLoadEntityById<Bars.Nedragis.Otdel1>(model.otdel?.Id);
            if (model.Element1462359853756.HasValue)
            {
                entity.Element1462359853756 = model.Element1462359853756.Value;
            }

            if (model.Element1462359855708.HasValue)
            {
                entity.Element1462359855708 = model.Element1462359855708.Value;
            }

            if (model.karta_raion.HasValue)
            {
                entity.karta_raion = model.karta_raion.Value;
            }

            if (model.karta_raion_d.HasValue)
            {
                entity.karta_raion_d = model.karta_raion_d.Value;
            }

            if (model.karta_nedra.HasValue)
            {
                entity.karta_nedra = model.karta_nedra.Value;
            }

            if (model.karta_nedra_d.HasValue)
            {
                entity.karta_nedra_d = model.karta_nedra_d.Value;
            }

            if (model.karta_n_p9622.HasValue)
            {
                entity.karta_n_p9622 = model.karta_n_p9622.Value;
            }

            if (model.karta_n_p6180.HasValue)
            {
                entity.karta_n_p6180 = model.karta_n_p6180.Value;
            }

            if (model.shema.HasValue)
            {
                entity.shema = model.shema.Value;
            }

            if (model.shema_d.HasValue)
            {
                entity.shema_d = model.shema_d.Value;
            }

            if (model.pecat_g_m.HasValue)
            {
                entity.pecat_g_m = model.pecat_g_m.Value;
            }

            if (model.pecat_g_m_d.HasValue)
            {
                entity.pecat_g_m_d = model.pecat_g_m_d.Value;
            }
        }
    }
}