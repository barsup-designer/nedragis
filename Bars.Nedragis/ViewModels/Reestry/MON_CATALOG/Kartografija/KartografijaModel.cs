namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма отдела "Картографии"' для отдачи на клиент
    /// </summary>
    public class KartografijaModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public KartografijaModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("1e94f171-abf5-481d-b7c8-fb0696336526")]
        public virtual Bars.Nedragis.Otdel1ListModel otdel
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Обзорная карта ЯНАО(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта ЯНАО(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("70fdfa54-5e4e-4150-aa58-02ef7bdb6e30")]
        public virtual System.Int32? Element1462359853756
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Обзорная карта ЯНАО(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта ЯНАО(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("e63e2ab8-becc-4bbd-a739-74f183cf581a")]
        public virtual System.Decimal? Element1462359855708
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Обзорная карта района ЯНАО(к.об.))'
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта района ЯНАО(к.об.))")]
        [Bars.Rms.Core.Attributes.Uid("c5a5a233-b007-4a11-a8b7-9fe84f3d7508")]
        public virtual System.Int32? karta_raion
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Обзорная карта района ЯНАО(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта района ЯНАО(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("1cd567c5-928d-4729-9b3c-7e97046c1db5")]
        public virtual System.Decimal? karta_raion_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Обзорная карта недропользования ЯНАО(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта недропользования ЯНАО(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("1c5085eb-f79f-449a-b303-b9de08bf2c0b")]
        public virtual System.Int32? karta_nedra
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Обзорная карта недропользования ЯНАО(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Обзорная карта недропользования ЯНАО(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("d2801af6-06fa-42ca-bd10-bfa18ebd8c6a")]
        public virtual System.Decimal? karta_nedra_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Карта населенного пункта ЯНАО(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Карта населенного пункта ЯНАО(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("ea2f5e71-dbd4-495c-bff5-514160961dda")]
        public virtual System.Int32? karta_n_p9622
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Карта населенного пункта ЯНАО(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Карта населенного пункта ЯНАО(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("2fcb70df-99ab-4608-8b47-4d8bc5d0629d")]
        public virtual System.Decimal? karta_n_p6180
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Схемы различного формата(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Схемы различного формата(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("ed0acc7c-1680-4831-9d08-374ea79987b7")]
        public virtual System.Int32? shema
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Схемы различного формата(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Схемы различного формата(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("f4246854-0539-46b8-9472-02dc8f5d7ad1")]
        public virtual System.Decimal? shema_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Печать графического материала(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Печать графического материала(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("ddfc43cb-bdc7-41b1-a3a1-dcc73835d1c3")]
        public virtual System.Int32? pecat_g_m
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Печать графического материала(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Печать графического материала(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("db3ef534-e3b7-42d8-aa6b-1357dbfa00cf")]
        public virtual System.Decimal? pecat_g_m_d
        {
            get;
            set;
        }
    }
}