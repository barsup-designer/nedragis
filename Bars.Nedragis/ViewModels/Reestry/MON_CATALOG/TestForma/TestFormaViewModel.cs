namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Отделы'
    /// </summary>
    public interface ITestFormaViewModel : IEntityEditorViewModel<Bars.Nedragis.monitor, TestFormaModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Отделы'
    /// </summary>
    public interface ITestFormaValidator : IEditorModelValidator<TestFormaModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Отделы'
    /// </summary>
    public interface ITestFormaHandler : IEntityEditorViewModelHandler<Bars.Nedragis.monitor, TestFormaModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Отделы'
    /// </summary>
    public abstract class AbstractTestFormaHandler : EntityEditorViewModelHandler<Bars.Nedragis.monitor, TestFormaModel>, ITestFormaHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Отделы'
    /// </summary>
    public class TestFormaViewModel : BaseEditorViewModel<Bars.Nedragis.monitor, TestFormaModel>, ITestFormaViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override TestFormaModel CreateModelInternal(BaseParams @params)
        {
            var model = new TestFormaModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Мониторинг' в модель представления
        /// </summary>
        protected override TestFormaModel MapEntityInternal(Bars.Nedragis.monitor entity)
        {
            // создаем экзепляр модели
            var model = new TestFormaModel();
            model.Id = entity.Id;
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Мониторинг' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.monitor entity, TestFormaModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
        }
    }
}