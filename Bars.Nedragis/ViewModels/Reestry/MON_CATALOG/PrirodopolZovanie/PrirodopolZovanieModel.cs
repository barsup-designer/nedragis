namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма отдела "Природопользования"' для отдачи на клиент
    /// </summary>
    public class PrirodopolZovanieModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public PrirodopolZovanieModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("4666233e-ad74-489d-a628-c65295d96eb7")]
        public virtual Bars.Nedragis.Otdel1ListModel otdel
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Оценка вреда и исчисление размера ущерба(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Оценка вреда и исчисление размера ущерба(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("a4bba18c-28c4-492c-8057-68c9fbaca7e0")]
        public virtual System.Int32? ocenka_vreda
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Оценка вреда и исчисление размера ущерба(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Оценка вреда и исчисление размера ущерба(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("73040d03-17ba-4c4a-8002-d6d295ebf4cd")]
        public virtual System.Decimal? ocenka_vreda_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Расчет убытков землепользователей(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Расчет убытков землепользователей(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("7e894002-c22c-43af-a66a-bd11e5451522")]
        public virtual System.Int32? raschet_ubytkov
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Расчет убытков землепользователей(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Расчет убытков землепользователей(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("824de177-0da4-4486-bced-192c6d731368")]
        public virtual System.Decimal? Element1462360050013
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Проверка координат и подготовка схем_к.о.'
        /// </summary>
        [Bars.B4.Utils.Display("Проверка координат и подготовка схем_к.о.")]
        [Bars.Rms.Core.Attributes.Uid("23b73f05-8761-4cf0-be95-af48e63341f6")]
        public virtual System.Int32? priroda_rpoverka_coor
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Проверка координат и подготовка схем_д.с.'
        /// </summary>
        [Bars.B4.Utils.Display("Проверка координат и подготовка схем_д.с.")]
        [Bars.Rms.Core.Attributes.Uid("f5cef265-ced8-4fab-96cd-aa471ed8a8be")]
        public virtual System.Decimal? priroda_rpoverka_coor_d
        {
            get;
            set;
        }
    }
}