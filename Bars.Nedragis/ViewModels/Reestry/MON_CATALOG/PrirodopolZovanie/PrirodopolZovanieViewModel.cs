namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма отдела "Природопользования"'
    /// </summary>
    public interface IPrirodopolZovanieViewModel : IEntityEditorViewModel<Bars.Nedragis.monitor, PrirodopolZovanieModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма отдела "Природопользования"'
    /// </summary>
    public interface IPrirodopolZovanieValidator : IEditorModelValidator<PrirodopolZovanieModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма отдела "Природопользования"'
    /// </summary>
    public interface IPrirodopolZovanieHandler : IEntityEditorViewModelHandler<Bars.Nedragis.monitor, PrirodopolZovanieModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма отдела "Природопользования"'
    /// </summary>
    public abstract class AbstractPrirodopolZovanieHandler : EntityEditorViewModelHandler<Bars.Nedragis.monitor, PrirodopolZovanieModel>, IPrirodopolZovanieHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма отдела "Природопользования"'
    /// </summary>
    public class PrirodopolZovanieViewModel : BaseEditorViewModel<Bars.Nedragis.monitor, PrirodopolZovanieModel>, IPrirodopolZovanieViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override PrirodopolZovanieModel CreateModelInternal(BaseParams @params)
        {
            var model = new PrirodopolZovanieModel();
            var varotdelId = @params.Params.GetAs<long>("otdel_Id", 0);
            if (varotdelId > 0)
            {
                model.otdel = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>().GetById(varotdelId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Мониторинг' в модель представления
        /// </summary>
        protected override PrirodopolZovanieModel MapEntityInternal(Bars.Nedragis.monitor entity)
        {
            // создаем экзепляр модели
            var model = new PrirodopolZovanieModel();
            model.Id = entity.Id;
            if (entity.otdel.IsNotNull())
            {
                var queryOtdel1List = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>();
                model.otdel = queryOtdel1List.GetById(entity.otdel.Id);
            }

            model.ocenka_vreda = (System.Int32? )(entity.ocenka_vreda);
            model.ocenka_vreda_d = (System.Decimal? )(entity.ocenka_vreda_d);
            model.raschet_ubytkov = (System.Int32? )(entity.raschet_ubytkov);
            model.Element1462360050013 = (System.Decimal? )(entity.Element1462360050013);
            model.priroda_rpoverka_coor = (System.Int32? )(entity.priroda_rpoverka_coor);
            model.priroda_rpoverka_coor_d = (System.Decimal? )(entity.priroda_rpoverka_coor_d);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Мониторинг' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.monitor entity, PrirodopolZovanieModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.otdel = TryLoadEntityById<Bars.Nedragis.Otdel1>(model.otdel?.Id);
            if (model.ocenka_vreda.HasValue)
            {
                entity.ocenka_vreda = model.ocenka_vreda.Value;
            }

            if (model.ocenka_vreda_d.HasValue)
            {
                entity.ocenka_vreda_d = model.ocenka_vreda_d.Value;
            }

            if (model.raschet_ubytkov.HasValue)
            {
                entity.raschet_ubytkov = model.raschet_ubytkov.Value;
            }

            if (model.Element1462360050013.HasValue)
            {
                entity.Element1462360050013 = model.Element1462360050013.Value;
            }

            if (model.priroda_rpoverka_coor.HasValue)
            {
                entity.priroda_rpoverka_coor = model.priroda_rpoverka_coor.Value;
            }

            if (model.priroda_rpoverka_coor_d.HasValue)
            {
                entity.priroda_rpoverka_coor_d = model.priroda_rpoverka_coor_d.Value;
            }
        }
    }
}