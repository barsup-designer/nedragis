namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма отдела "Землеустройства" ' для отдачи на клиент
    /// </summary>
    public class ZemleustrojjstvaModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public ZemleustrojjstvaModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование отдела
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела")]
        [Bars.Rms.Core.Attributes.Uid("c2d314fc-f5b9-4a51-901d-fd7fd62f61df")]
        public virtual Bars.Nedragis.Otdel1ListModel otdel
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Пространственный анализ размещения проектируемых участков(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Пространственный анализ размещения проектируемых участков(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("513cf552-f48f-4848-85ab-5c6574f79122")]
        public virtual System.Int32? analiz
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Пространственный анализ размещения проектируемых участков(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Пространственный анализ размещения проектируемых участков(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("2c49cddf-dc4d-4b99-a107-dcec1e95a579")]
        public virtual System.Decimal? analiz_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Выполнение обзорных карт-схем(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Выполнение обзорных карт-схем(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("621d83d1-a0c2-482b-98b2-d7a70bd891c3")]
        public virtual System.Int32? karta_chema
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Выполнение обзорных карт-схем(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Выполнение обзорных карт-схем(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("47242321-1d40-4774-b048-82778030cc27")]
        public virtual System.Decimal? karta_shema_d
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Пространственный анализ(к.об.)'
        /// </summary>
        [Bars.B4.Utils.Display("Пространственный анализ(к.об.)")]
        [Bars.Rms.Core.Attributes.Uid("2351d247-fcad-4f60-bd0d-b5a342703ee1")]
        public virtual System.Int32? pr_analiz
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Пространственный анализ(д.ср.)'
        /// </summary>
        [Bars.B4.Utils.Display("Пространственный анализ(д.ср.)")]
        [Bars.Rms.Core.Attributes.Uid("bcc9eec3-dd9b-44bb-8187-565d41c44229")]
        public virtual System.Decimal? pr_analiz_d
        {
            get;
            set;
        }
    }
}