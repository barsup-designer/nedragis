namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма отдела "Землеустройства" '
    /// </summary>
    public interface IZemleustrojjstvaViewModel : IEntityEditorViewModel<Bars.Nedragis.monitor, ZemleustrojjstvaModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма отдела "Землеустройства" '
    /// </summary>
    public interface IZemleustrojjstvaValidator : IEditorModelValidator<ZemleustrojjstvaModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма отдела "Землеустройства" '
    /// </summary>
    public interface IZemleustrojjstvaHandler : IEntityEditorViewModelHandler<Bars.Nedragis.monitor, ZemleustrojjstvaModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма отдела "Землеустройства" '
    /// </summary>
    public abstract class AbstractZemleustrojjstvaHandler : EntityEditorViewModelHandler<Bars.Nedragis.monitor, ZemleustrojjstvaModel>, IZemleustrojjstvaHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма отдела "Землеустройства" '
    /// </summary>
    public class ZemleustrojjstvaViewModel : BaseEditorViewModel<Bars.Nedragis.monitor, ZemleustrojjstvaModel>, IZemleustrojjstvaViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ZemleustrojjstvaModel CreateModelInternal(BaseParams @params)
        {
            var model = new ZemleustrojjstvaModel();
            var varotdelId = @params.Params.GetAs<long>("otdel_Id", 0);
            if (varotdelId > 0)
            {
                model.otdel = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>().GetById(varotdelId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Мониторинг' в модель представления
        /// </summary>
        protected override ZemleustrojjstvaModel MapEntityInternal(Bars.Nedragis.monitor entity)
        {
            // создаем экзепляр модели
            var model = new ZemleustrojjstvaModel();
            model.Id = entity.Id;
            if (entity.otdel.IsNotNull())
            {
                var queryOtdel1List = Container.Resolve<Bars.Nedragis.IOtdel1ListQuery>();
                model.otdel = queryOtdel1List.GetById(entity.otdel.Id);
            }

            model.analiz = (System.Int32? )(entity.analiz);
            model.analiz_d = (System.Decimal? )(entity.analiz_d);
            model.karta_chema = (System.Int32? )(entity.karta_chema);
            model.karta_shema_d = (System.Decimal? )(entity.karta_shema_d);
            model.pr_analiz = (System.Int32? )(entity.pr_analiz);
            model.pr_analiz_d = (System.Decimal? )(entity.pr_analiz_d);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Мониторинг' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.monitor entity, ZemleustrojjstvaModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.otdel = TryLoadEntityById<Bars.Nedragis.Otdel1>(model.otdel?.Id);
            if (model.analiz.HasValue)
            {
                entity.analiz = model.analiz.Value;
            }

            if (model.analiz_d.HasValue)
            {
                entity.analiz_d = model.analiz_d.Value;
            }

            if (model.karta_chema.HasValue)
            {
                entity.karta_chema = model.karta_chema.Value;
            }

            if (model.karta_shema_d.HasValue)
            {
                entity.karta_shema_d = model.karta_shema_d.Value;
            }

            if (model.pr_analiz.HasValue)
            {
                entity.pr_analiz = model.pr_analiz.Value;
            }

            if (model.pr_analiz_d.HasValue)
            {
                entity.pr_analiz_d = model.pr_analiz_d.Value;
            }
        }
    }
}