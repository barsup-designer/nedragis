namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Мониторинг'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Мониторинг")]
    public class monitorListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование отдела.Наименование' (псевдоним: otdel_NAME_SP)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование отдела.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("3b6408ca-d871-4305-9750-71b8ea890ac6")]
        public virtual System.String otdel_NAME_SP
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата создания' (псевдоним: ObjectCreateDate)
        /// </summary>
        [Bars.B4.Utils.Display("Дата создания")]
        [Bars.Rms.Core.Attributes.Uid("d857d306-9230-4c39-85eb-d96a7e4314b0")]
        public virtual System.DateTime? ObjectCreateDate
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата последнего редактирования' (псевдоним: ObjectEditDate)
        /// </summary>
        [Bars.B4.Utils.Display("Дата последнего редактирования")]
        [Bars.Rms.Core.Attributes.Uid("29231b6a-b81a-46bc-9eae-09a54f0b8ad8")]
        public virtual System.DateTime? ObjectEditDate
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}