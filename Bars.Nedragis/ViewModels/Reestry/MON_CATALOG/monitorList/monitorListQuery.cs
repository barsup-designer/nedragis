namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Мониторинг'
    /// </summary>
    public interface ImonitorListQuery : IQueryOperation<Bars.Nedragis.monitor, Bars.Nedragis.monitorListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Мониторинг'
    /// </summary>
    public interface ImonitorListQueryFilter : IQueryOperationFilter<Bars.Nedragis.monitor, Bars.Nedragis.monitorListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Мониторинг'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Мониторинг")]
    public class monitorListQuery : RmsEntityQueryOperation<Bars.Nedragis.monitor, Bars.Nedragis.monitorListModel>, ImonitorListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "monitorListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public monitorListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.monitor> Filter(IQueryable<Bars.Nedragis.monitor> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.monitorListModel> Map(IQueryable<Bars.Nedragis.monitor> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.monitor>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.monitorListModel{Id = x.Id, _TypeUid = "293381ae-d0ca-4bbf-8b8c-0e2c31bd13c4", otdel_NAME_SP = (System.String)(x.otdel.NAME_SP), ObjectCreateDate = (System.DateTime? )(x.ObjectCreateDate), ObjectEditDate = (System.DateTime? )(x.ObjectEditDate), });
        }
    }
}