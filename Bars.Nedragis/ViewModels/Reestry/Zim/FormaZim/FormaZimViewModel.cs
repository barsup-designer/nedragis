namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма зим'
    /// </summary>
    public interface IFormaZimViewModel : IEntityEditorViewModel<Bars.Nedragis.Zim1, FormaZimModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма зим'
    /// </summary>
    public interface IFormaZimValidator : IEditorModelValidator<FormaZimModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма зим'
    /// </summary>
    public interface IFormaZimHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Zim1, FormaZimModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма зим'
    /// </summary>
    public abstract class AbstractFormaZimHandler : EntityEditorViewModelHandler<Bars.Nedragis.Zim1, FormaZimModel>, IFormaZimHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма зим'
    /// </summary>
    public class FormaZimViewModel : BaseEditorViewModel<Bars.Nedragis.Zim1, FormaZimModel>, IFormaZimViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override FormaZimModel CreateModelInternal(BaseParams @params)
        {
            var model = new FormaZimModel();
            var varElement1517482864074Id = @params.Params.GetAs<long>("Element1517482864074_Id", 0);
            if (varElement1517482864074Id > 0)
            {
                model.Element1517482864074 = Container.Resolve<Bars.Nedragis.IZima1ListQuery>().GetById(varElement1517482864074Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'zim1' в модель представления
        /// </summary>
        protected override FormaZimModel MapEntityInternal(Bars.Nedragis.Zim1 entity)
        {
            // создаем экзепляр модели
            var model = new FormaZimModel();
            model.Id = entity.Id;
            if (entity.Element1517482864074.IsNotNull())
            {
                var queryZima1List = Container.Resolve<Bars.Nedragis.IZima1ListQuery>();
                model.Element1517482864074 = queryZima1List.GetById(entity.Element1517482864074.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'zim1' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Zim1 entity, FormaZimModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1517482864074 = TryLoadEntityById<Bars.Nedragis.Zima1>(model.Element1517482864074?.Id);
        }
    }
}