namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Reestr zim'
    /// </summary>
    public interface IReestrZimQuery : IQueryOperation<Bars.Nedragis.Zim1, Bars.Nedragis.ReestrZimModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Reestr zim'
    /// </summary>
    public interface IReestrZimQueryFilter : IQueryOperationFilter<Bars.Nedragis.Zim1, Bars.Nedragis.ReestrZimModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Reestr zim'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Reestr zim")]
    public class ReestrZimQuery : RmsEntityQueryOperation<Bars.Nedragis.Zim1, Bars.Nedragis.ReestrZimModel>, IReestrZimQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "ReestrZimQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public ReestrZimQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Zim1> Filter(IQueryable<Bars.Nedragis.Zim1> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.ReestrZimModel> Map(IQueryable<Bars.Nedragis.Zim1> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Zim1>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.ReestrZimModel{Id = x.Id, _TypeUid = "65ab9d8b-bf41-47db-9196-b2c564e8e49f", zzz = (System.String)(x.Element1517482864074.Element1517294546876), });
        }
    }
}