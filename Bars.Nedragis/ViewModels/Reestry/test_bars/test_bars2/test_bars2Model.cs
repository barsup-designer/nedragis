namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'test_bars2'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("test_bars2")]
    public class test_bars2Model
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'площадь' (псевдоним: Element1481264972872)
        /// </summary>
        [Bars.B4.Utils.Display("площадь")]
        [Bars.Rms.Core.Attributes.Uid("02b161a0-c76a-45eb-adb4-eceb54baf604")]
        public virtual System.Decimal? Element1481264972872
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Test' (псевдоним: Element1496214131857)
        /// </summary>
        [Bars.B4.Utils.Display("Test")]
        [Bars.Rms.Core.Attributes.Uid("023ca7a2-7c0e-4260-a541-51b83e948f1d")]
        public virtual System.String Element1496214131857
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}