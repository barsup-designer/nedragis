namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования test_bars'
    /// </summary>
    public interface Itest_barsEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.test_bars, test_barsEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования test_bars'
    /// </summary>
    public interface Itest_barsEditorValidator : IEditorModelValidator<test_barsEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования test_bars'
    /// </summary>
    public interface Itest_barsEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.test_bars, test_barsEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования test_bars'
    /// </summary>
    public abstract class Abstracttest_barsEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.test_bars, test_barsEditorModel>, Itest_barsEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования test_bars'
    /// </summary>
    public class test_barsEditorViewModel : BaseEditorViewModel<Bars.Nedragis.test_bars, test_barsEditorModel>, Itest_barsEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override test_barsEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new test_barsEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'test_bars' в модель представления
        /// </summary>
        protected override test_barsEditorModel MapEntityInternal(Bars.Nedragis.test_bars entity)
        {
            // создаем экзепляр модели
            var model = new test_barsEditorModel();
            model.Id = entity.Id;
            model.Element1481264972872 = (System.Decimal? )(entity.Element1481264972872);
            model.Element1496214131857 = (System.String)(entity.Element1496214131857);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'test_bars' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.test_bars entity, test_barsEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.Element1481264972872.HasValue)
            {
                entity.Element1481264972872 = model.Element1481264972872.Value;
            }

            entity.Element1496214131857 = model.Element1496214131857;
        }
    }
}