namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования test_bars' для отдачи на клиент
    /// </summary>
    public class test_barsEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public test_barsEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'площадь'
        /// </summary>
        [Bars.B4.Utils.Display("площадь")]
        [Bars.Rms.Core.Attributes.Uid("751519a6-780f-4f5c-af88-aeb5c3891a5a")]
        public virtual System.Decimal? Element1481264972872
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Test'
        /// </summary>
        [Bars.B4.Utils.Display("Test")]
        [Bars.Rms.Core.Attributes.Uid("c2730a8d-b92d-4c9a-a2b5-049354344371")]
        public virtual System.String Element1496214131857
        {
            get;
            set;
        }
    }
}