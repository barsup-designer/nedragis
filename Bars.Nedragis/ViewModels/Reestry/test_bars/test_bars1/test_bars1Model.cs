namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'test_bars1' для отдачи на клиент
    /// </summary>
    public class test_bars1Model : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public test_bars1Model()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'площадь'
        /// </summary>
        [Bars.B4.Utils.Display("площадь")]
        [Bars.Rms.Core.Attributes.Uid("afbff5cd-1002-4bda-9e0e-c0f04181978f")]
        public virtual System.Decimal? Element1481264972872
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Test'
        /// </summary>
        [Bars.B4.Utils.Display("Test")]
        [Bars.Rms.Core.Attributes.Uid("c9097bd5-ad7f-4d1a-91d3-f967270a2c61")]
        public virtual System.String Element1496214131857
        {
            get;
            set;
        }
    }
}