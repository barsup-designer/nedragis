namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'test_bars1'
    /// </summary>
    public interface Itest_bars1ViewModel : IEntityEditorViewModel<Bars.Nedragis.test_bars, test_bars1Model>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'test_bars1'
    /// </summary>
    public interface Itest_bars1Validator : IEditorModelValidator<test_bars1Model>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'test_bars1'
    /// </summary>
    public interface Itest_bars1Handler : IEntityEditorViewModelHandler<Bars.Nedragis.test_bars, test_bars1Model>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'test_bars1'
    /// </summary>
    public abstract class Abstracttest_bars1Handler : EntityEditorViewModelHandler<Bars.Nedragis.test_bars, test_bars1Model>, Itest_bars1Handler
    {
    }

    /// <summary>
    /// Реализация модели представления 'test_bars1'
    /// </summary>
    public class test_bars1ViewModel : BaseEditorViewModel<Bars.Nedragis.test_bars, test_bars1Model>, Itest_bars1ViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override test_bars1Model CreateModelInternal(BaseParams @params)
        {
            var model = new test_bars1Model();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'test_bars' в модель представления
        /// </summary>
        protected override test_bars1Model MapEntityInternal(Bars.Nedragis.test_bars entity)
        {
            // создаем экзепляр модели
            var model = new test_bars1Model();
            model.Id = entity.Id;
            model.Element1481264972872 = (System.Decimal? )(entity.Element1481264972872);
            model.Element1496214131857 = (System.String)(entity.Element1496214131857);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'test_bars' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.test_bars entity, test_bars1Model model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.Element1481264972872.HasValue)
            {
                entity.Element1481264972872 = model.Element1481264972872.Value;
            }

            entity.Element1496214131857 = model.Element1496214131857;
        }
    }
}