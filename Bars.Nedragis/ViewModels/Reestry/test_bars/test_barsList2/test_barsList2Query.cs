namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр test_bars2'
    /// </summary>
    public interface Itest_barsList2Query : IQueryOperation<Bars.Nedragis.test_bars, Bars.Nedragis.test_barsList2Model, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр test_bars2'
    /// </summary>
    public interface Itest_barsList2QueryFilter : IQueryOperationFilter<Bars.Nedragis.test_bars, Bars.Nedragis.test_barsList2Model, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр test_bars2'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр test_bars2")]
    public class test_barsList2Query : RmsEntityQueryOperation<Bars.Nedragis.test_bars, Bars.Nedragis.test_barsList2Model>, Itest_barsList2Query
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "test_barsList2Query"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public test_barsList2Query(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.test_bars> Filter(IQueryable<Bars.Nedragis.test_bars> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.test_barsList2Model> Map(IQueryable<Bars.Nedragis.test_bars> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.test_bars>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.test_barsList2Model{Id = x.Id, _TypeUid = "7dd5ad97-a603-4dbf-acec-30db779b157d", Element1481264972872 = (System.Decimal? )(x.Element1481264972872), Element1496214131857 = (System.String)(x.Element1496214131857), });
        }
    }
}