namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Субсидии'
    /// </summary>
    public interface IsubsidiesEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.subsidies, subsidiesEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Субсидии'
    /// </summary>
    public interface IsubsidiesEditorValidator : IEditorModelValidator<subsidiesEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Субсидии'
    /// </summary>
    public interface IsubsidiesEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.subsidies, subsidiesEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Субсидии'
    /// </summary>
    public abstract class AbstractsubsidiesEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.subsidies, subsidiesEditorModel>, IsubsidiesEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Субсидии'
    /// </summary>
    public class subsidiesEditorViewModel : BaseEditorViewModel<Bars.Nedragis.subsidies, subsidiesEditorModel>, IsubsidiesEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override subsidiesEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new subsidiesEditorModel();
            var varNAME_MOId = @params.Params.GetAs<long>("NAME_MO_Id", 0);
            if (varNAME_MOId > 0)
            {
                model.NAME_MO = Container.Resolve<Bars.Nedragis.INaimenovanieMOListQuery>().GetById(varNAME_MOId);
            }

            var varElement1455708491495Id = @params.Params.GetAs<long>("Element1455708491495_Id", 0);
            if (varElement1455708491495Id > 0)
            {
                model.Element1455708491495 = Container.Resolve<Bars.Nedragis.INaimenovanieNapravlenijaRaskhodovanijaSredstvListQuery>().GetById(varElement1455708491495Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Субсидии' в модель представления
        /// </summary>
        protected override subsidiesEditorModel MapEntityInternal(Bars.Nedragis.subsidies entity)
        {
            // создаем экзепляр модели
            var model = new subsidiesEditorModel();
            model.Id = entity.Id;
            if (entity.NAME_MO.IsNotNull())
            {
                var queryNaimenovanieMOList = Container.Resolve<Bars.Nedragis.INaimenovanieMOListQuery>();
                model.NAME_MO = queryNaimenovanieMOList.GetById(entity.NAME_MO.Id);
            }

            if (entity.Element1455708491495.IsNotNull())
            {
                var queryNaimenovanieNapravlenijaRaskhodovanijaSredstvList = Container.Resolve<Bars.Nedragis.INaimenovanieNapravlenijaRaskhodovanijaSredstvListQuery>();
                model.Element1455708491495 = queryNaimenovanieNapravlenijaRaskhodovanijaSredstvList.GetById(entity.Element1455708491495.Id);
            }

            model.Element1455709095130 = (System.String)(entity.Element1455709095130);
            model.Element1455709437278 = (System.String)(entity.Element1455709437278);
            model.Element1455709476348 = (System.String)(entity.Element1455709476348);
            model.Element1456977571408 = (System.String)(entity.Element1456977571408);
            model.Element1456979244748 = (System.DateTime? )(entity.Element1456979244748);
            model.Element1456979287919 = (System.DateTime? )(entity.Element1456979287919);
            model.Element1456982258068 = (System.Int32? )(entity.Element1456982258068);
            model.Element1456982634223 = (System.Int32? )(entity.Element1456982634223);
            model.Element1456982674444 = (System.Decimal? )(entity.Element1456982674444);
            model.Element1456982760976 = (System.Int32? )(entity.Element1456982760976);
            model.Element1456982777199 = (System.Int32? )(entity.Element1456982777199);
            model.Element1456982822174 = (System.Int32? )(entity.Element1456982822174);
            model.Element1456982887080 = (System.Int32? )(entity.Element1456982887080);
            model.Element1456982966340 = (System.Decimal? )(entity.Element1456982966340);
            model.Element1456983021798 = (System.Decimal? )(entity.Element1456983021798);
            model.Element1456983065031 = (System.Int32? )(entity.Element1456983065031);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Субсидии' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.subsidies entity, subsidiesEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.NAME_MO = TryLoadEntityById<Bars.Nedragis.NaimenovanieMO>(model.NAME_MO?.Id);
            entity.Element1455708491495 = TryLoadEntityById<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv>(model.Element1455708491495?.Id);
            entity.Element1455709095130 = model.Element1455709095130;
            entity.Element1455709437278 = model.Element1455709437278;
            entity.Element1455709476348 = model.Element1455709476348;
            entity.Element1456977571408 = model.Element1456977571408;
            entity.Element1456979244748 = model.Element1456979244748.GetValueOrDefault();
            entity.Element1456979287919 = model.Element1456979287919.GetValueOrDefault();
            if (model.Element1456982258068.HasValue)
            {
                entity.Element1456982258068 = model.Element1456982258068.Value;
            }

            if (model.Element1456982634223.HasValue)
            {
                entity.Element1456982634223 = model.Element1456982634223.Value;
            }

            if (model.Element1456982674444.HasValue)
            {
                entity.Element1456982674444 = model.Element1456982674444.Value;
            }

            if (model.Element1456982760976.HasValue)
            {
                entity.Element1456982760976 = model.Element1456982760976.Value;
            }

            if (model.Element1456982777199.HasValue)
            {
                entity.Element1456982777199 = model.Element1456982777199.Value;
            }

            if (model.Element1456982822174.HasValue)
            {
                entity.Element1456982822174 = model.Element1456982822174.Value;
            }

            if (model.Element1456982887080.HasValue)
            {
                entity.Element1456982887080 = model.Element1456982887080.Value;
            }

            if (model.Element1456982966340.HasValue)
            {
                entity.Element1456982966340 = model.Element1456982966340.Value;
            }

            if (model.Element1456983021798.HasValue)
            {
                entity.Element1456983021798 = model.Element1456983021798.Value;
            }

            if (model.Element1456983065031.HasValue)
            {
                entity.Element1456983065031 = model.Element1456983065031.Value;
            }
        }
    }
}