namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Субсидии' для отдачи на клиент
    /// </summary>
    public class subsidiesEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public subsidiesEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование муниципального образования
        /// </summary>
        [Bars.B4.Utils.Display("Наименование муниципального образования")]
        [Bars.Rms.Core.Attributes.Uid("816b1a2b-9cae-43f9-90cb-94f4862f631b")]
        public virtual Bars.Nedragis.NaimenovanieMOListModel NAME_MO
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование направления расходования средств
        /// </summary>
        [Bars.B4.Utils.Display("Наименование направления расходования средств")]
        [Bars.Rms.Core.Attributes.Uid("ae4dd62f-3002-40e4-9b71-ff6241c2b844")]
        public virtual Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel Element1455708491495
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование объекта, с группировкой по населенным пунктам'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование объекта, с группировкой по населенным пунктам")]
        [Bars.Rms.Core.Attributes.Uid("5fc7de46-db00-4660-b246-e21ac5c450bc")]
        public virtual System.String Element1455709095130
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Реквизиты свидетельства о государственной регистрации'
        /// </summary>
        [Bars.B4.Utils.Display("Реквизиты свидетельства о государственной регистрации")]
        [Bars.Rms.Core.Attributes.Uid("fd3b849e-1f95-4eff-9774-3aa11547313d")]
        public virtual System.String Element1455709437278
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Производственный процесс СМР ПИР'
        /// </summary>
        [Bars.B4.Utils.Display("Производственный процесс СМР ПИР")]
        [Bars.Rms.Core.Attributes.Uid("ca009da3-05cc-4075-92b8-24e3fa341bab")]
        public virtual System.String Element1455709476348
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Вид работ '
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ ")]
        [Bars.Rms.Core.Attributes.Uid("29aad86a-23ac-4d77-aa0f-6a0d0c027a8a")]
        public virtual System.String Element1456977571408
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата начала выполнения работ'
        /// </summary>
        [Bars.B4.Utils.Display("Дата начала выполнения работ")]
        [Bars.Rms.Core.Attributes.Uid("218c0fd8-a4be-4295-8774-9946eaa141ae")]
        public virtual System.DateTime? Element1456979244748
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата окончания выполнения работ'
        /// </summary>
        [Bars.B4.Utils.Display("Дата окончания выполнения работ")]
        [Bars.Rms.Core.Attributes.Uid("cf12ab37-2d9e-4431-a188-f1977b43a59a")]
        public virtual System.DateTime? Element1456979287919
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Мощность(км)'
        /// </summary>
        [Bars.B4.Utils.Display("Мощность(км)")]
        [Bars.Rms.Core.Attributes.Uid("0e88db2c-1395-4747-9e2d-7549dce67304")]
        public virtual System.Int32? Element1456982258068
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Мощность(м2)'
        /// </summary>
        [Bars.B4.Utils.Display("Мощность(м2)")]
        [Bars.Rms.Core.Attributes.Uid("fd14d6c9-334e-4f13-926c-b854bf2a0a0b")]
        public virtual System.Int32? Element1456982634223
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Максимальная цена контракта(руб.)'
        /// </summary>
        [Bars.B4.Utils.Display("Максимальная цена контракта(руб.)")]
        [Bars.Rms.Core.Attributes.Uid("5817b927-7b4a-4421-bddc-67b811c5f223")]
        public virtual System.Decimal? Element1456982674444
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'км'
        /// </summary>
        [Bars.B4.Utils.Display("км")]
        [Bars.Rms.Core.Attributes.Uid("a23d56e3-b2d0-416c-9dc0-f65326d89e61")]
        public virtual System.Int32? Element1456982760976
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'м2'
        /// </summary>
        [Bars.B4.Utils.Display("м2")]
        [Bars.Rms.Core.Attributes.Uid("b4737eec-5ebd-4b0a-b61d-0a24b4a520cb")]
        public virtual System.Int32? Element1456982777199
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Количество объектов с положительным заключением'
        /// </summary>
        [Bars.B4.Utils.Display("Количество объектов с положительным заключением")]
        [Bars.Rms.Core.Attributes.Uid("d28c6035-a566-4842-ba16-b66f7f78aa75")]
        public virtual System.Int32? Element1456982822174
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Доля автомобильных дорог(%)'
        /// </summary>
        [Bars.B4.Utils.Display("Доля автомобильных дорог(%)")]
        [Bars.Rms.Core.Attributes.Uid("7867d610-c8b6-43f7-bc53-b7d33c7e1e2e")]
        public virtual System.Int32? Element1456982887080
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Объем финансирования(Бюджет ЯНАО)'
        /// </summary>
        [Bars.B4.Utils.Display("Объем финансирования(Бюджет ЯНАО)")]
        [Bars.Rms.Core.Attributes.Uid("569630c8-792a-4f15-8202-3a3f9c4a99d8")]
        public virtual System.Decimal? Element1456982966340
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Объем финансирования(Бюджет МО)'
        /// </summary>
        [Bars.B4.Utils.Display("Объем финансирования(Бюджет МО)")]
        [Bars.Rms.Core.Attributes.Uid("9aff1ef1-0ab7-4f7e-bcf7-788b237d04c6")]
        public virtual System.Decimal? Element1456983021798
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Процент долевого участия бюджета МО'
        /// </summary>
        [Bars.B4.Utils.Display("Процент долевого участия бюджета МО")]
        [Bars.Rms.Core.Attributes.Uid("0ba296d2-3775-445c-bcd5-349c70331bb2")]
        public virtual System.Int32? Element1456983065031
        {
            get;
            set;
        }
    }
}