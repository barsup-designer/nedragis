namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Файлы'
    /// </summary>
    public interface IFajjlyEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Fajjly, FajjlyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Файлы'
    /// </summary>
    public interface IFajjlyEditorValidator : IEditorModelValidator<FajjlyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Файлы'
    /// </summary>
    public interface IFajjlyEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Fajjly, FajjlyEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Файлы'
    /// </summary>
    public abstract class AbstractFajjlyEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Fajjly, FajjlyEditorModel>, IFajjlyEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Файлы'
    /// </summary>
    public class FajjlyEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Fajjly, FajjlyEditorModel>, IFajjlyEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override FajjlyEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new FajjlyEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Файлы' в модель представления
        /// </summary>
        protected override FajjlyEditorModel MapEntityInternal(Bars.Nedragis.Fajjly entity)
        {
            // создаем экзепляр модели
            var model = new FajjlyEditorModel();
            model.Id = entity.Id;
            model.ObjectCreateDate = (System.DateTime? )(entity.ObjectCreateDate);
            model.Element1457586389312 = (System.String)(entity.Element1457586389312);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Файлы' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Fajjly entity, FajjlyEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.ObjectCreateDate = model.ObjectCreateDate.GetValueOrDefault();
            entity.Element1457586389312 = model.Element1457586389312;
        }
    }
}