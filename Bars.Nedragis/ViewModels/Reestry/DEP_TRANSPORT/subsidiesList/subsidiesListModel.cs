namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Субсидии'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Субсидии")]
    public class subsidiesListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование муниципального образования.Наименование' (псевдоним: NAME_MO_Element1455707802552)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование муниципального образования.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("b784bb9f-43b4-476c-9fab-027a35c5b645")]
        public virtual System.String NAME_MO_Element1455707802552
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование направления расходования средств.Наименование' (псевдоним: Element1455708491495_Element1455708457805)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование направления расходования средств.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("83fff8dd-690e-4fac-8589-b3cb025e9973")]
        public virtual System.String Element1455708491495_Element1455708457805
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование объекта, с группировкой по населенным пунктам' (псевдоним: Element1455709095130)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование объекта, с группировкой по населенным пунктам")]
        [Bars.Rms.Core.Attributes.Uid("ea01de67-91fe-43bc-b898-93c483cee372")]
        public virtual System.String Element1455709095130
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Реквизиты свидетельства о государственной регистрации' (псевдоним: Element1455709437278)
        /// </summary>
        [Bars.B4.Utils.Display("Реквизиты свидетельства о государственной регистрации")]
        [Bars.Rms.Core.Attributes.Uid("297f12ce-fda3-4382-a506-ebb6cfb18db6")]
        public virtual System.String Element1455709437278
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}