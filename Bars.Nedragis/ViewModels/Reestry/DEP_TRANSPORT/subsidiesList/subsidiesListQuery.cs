namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Субсидии'
    /// </summary>
    public interface IsubsidiesListQuery : IQueryOperation<Bars.Nedragis.subsidies, Bars.Nedragis.subsidiesListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Субсидии'
    /// </summary>
    public interface IsubsidiesListQueryFilter : IQueryOperationFilter<Bars.Nedragis.subsidies, Bars.Nedragis.subsidiesListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Субсидии'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Субсидии")]
    public class subsidiesListQuery : RmsEntityQueryOperation<Bars.Nedragis.subsidies, Bars.Nedragis.subsidiesListModel>, IsubsidiesListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "subsidiesListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public subsidiesListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.subsidies> Filter(IQueryable<Bars.Nedragis.subsidies> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.subsidiesListModel> Map(IQueryable<Bars.Nedragis.subsidies> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.subsidies>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.subsidiesListModel{Id = x.Id, _TypeUid = "59ae1a57-2649-408b-9f30-33fc49b5a7cb", NAME_MO_Element1455707802552 = (System.String)(x.NAME_MO.Element1455707802552), Element1455708491495_Element1455708457805 = (System.String)(x.Element1455708491495.Element1455708457805), Element1455709095130 = (System.String)(x.Element1455709095130), Element1455709437278 = (System.String)(x.Element1455709437278), });
        }
    }
}