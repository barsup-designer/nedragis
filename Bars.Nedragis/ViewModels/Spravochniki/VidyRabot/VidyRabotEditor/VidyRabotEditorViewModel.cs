namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Виды работ'
    /// </summary>
    public interface IVidyRabotEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.VidyRabot, VidyRabotEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Виды работ'
    /// </summary>
    public interface IVidyRabotEditorValidator : IEditorModelValidator<VidyRabotEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Виды работ'
    /// </summary>
    public interface IVidyRabotEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.VidyRabot, VidyRabotEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Виды работ'
    /// </summary>
    public abstract class AbstractVidyRabotEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.VidyRabot, VidyRabotEditorModel>, IVidyRabotEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Виды работ'
    /// </summary>
    public class VidyRabotEditorViewModel : BaseEditorViewModel<Bars.Nedragis.VidyRabot, VidyRabotEditorModel>, IVidyRabotEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override VidyRabotEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new VidyRabotEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Виды работ' в модель представления
        /// </summary>
        protected override VidyRabotEditorModel MapEntityInternal(Bars.Nedragis.VidyRabot entity)
        {
            // создаем экзепляр модели
            var model = new VidyRabotEditorModel();
            model.Id = entity.Id;
            model.Element1474020299386 = (System.String)(entity.Element1474020299386);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Виды работ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.VidyRabot entity, VidyRabotEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.Id.HasValue)
            {
                entity.Id = model.Id.Value;
            }

            entity.Element1474020299386 = model.Element1474020299386;
        }
    }
}