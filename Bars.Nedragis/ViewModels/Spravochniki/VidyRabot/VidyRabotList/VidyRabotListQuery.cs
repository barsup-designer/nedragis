namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Виды работ'
    /// </summary>
    public interface IVidyRabotListQuery : IQueryOperation<Bars.Nedragis.VidyRabot, Bars.Nedragis.VidyRabotListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Виды работ'
    /// </summary>
    public interface IVidyRabotListQueryFilter : IQueryOperationFilter<Bars.Nedragis.VidyRabot, Bars.Nedragis.VidyRabotListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Виды работ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Виды работ")]
    public class VidyRabotListQuery : RmsEntityQueryOperation<Bars.Nedragis.VidyRabot, Bars.Nedragis.VidyRabotListModel>, IVidyRabotListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "VidyRabotListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public VidyRabotListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.VidyRabot> Filter(IQueryable<Bars.Nedragis.VidyRabot> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.VidyRabotListModel> Map(IQueryable<Bars.Nedragis.VidyRabot> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.VidyRabot>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.VidyRabotListModel{Id = x.Id, _TypeUid = "f1b2473f-a514-468d-95fc-58e1fbb40f2b", Element1474020299386 = (System.String)(x.Element1474020299386), });
        }
    }
}