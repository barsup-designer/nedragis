namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Документы, удостоверяющие личность'
    /// </summary>
    public interface IDokumentyUdostoverjajushhieLichnostEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost, DokumentyUdostoverjajushhieLichnostEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Документы, удостоверяющие личность'
    /// </summary>
    public interface IDokumentyUdostoverjajushhieLichnostEditorValidator : IEditorModelValidator<DokumentyUdostoverjajushhieLichnostEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Документы, удостоверяющие личность'
    /// </summary>
    public interface IDokumentyUdostoverjajushhieLichnostEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost, DokumentyUdostoverjajushhieLichnostEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Документы, удостоверяющие личность'
    /// </summary>
    public abstract class AbstractDokumentyUdostoverjajushhieLichnostEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost, DokumentyUdostoverjajushhieLichnostEditorModel>, IDokumentyUdostoverjajushhieLichnostEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Документы, удостоверяющие личность'
    /// </summary>
    public class DokumentyUdostoverjajushhieLichnostEditorViewModel : BaseEditorViewModel<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost, DokumentyUdostoverjajushhieLichnostEditorModel>, IDokumentyUdostoverjajushhieLichnostEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override DokumentyUdostoverjajushhieLichnostEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new DokumentyUdostoverjajushhieLichnostEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Документы удостоверяющие личность' в модель представления
        /// </summary>
        protected override DokumentyUdostoverjajushhieLichnostEditorModel MapEntityInternal(Bars.Nedragis.DokumentyUdostoverjajushhieLichnost entity)
        {
            // создаем экзепляр модели
            var model = new DokumentyUdostoverjajushhieLichnostEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name1 = (System.String)(entity.name1);
            model.description = (System.String)(entity.description);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Документы удостоверяющие личность' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.DokumentyUdostoverjajushhieLichnost entity, DokumentyUdostoverjajushhieLichnostEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name1 = model.name1;
            entity.description = model.description;
        }
    }
}