namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Документы удостоверяющие личность'
    /// </summary>
    public interface IDokumentyUdostoverjajushhieLichnostListQuery : IQueryOperation<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost, Bars.Nedragis.DokumentyUdostoverjajushhieLichnostListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Документы удостоверяющие личность'
    /// </summary>
    public interface IDokumentyUdostoverjajushhieLichnostListQueryFilter : IQueryOperationFilter<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost, Bars.Nedragis.DokumentyUdostoverjajushhieLichnostListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Документы удостоверяющие личность'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Документы удостоверяющие личность")]
    public class DokumentyUdostoverjajushhieLichnostListQuery : RmsEntityQueryOperation<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost, Bars.Nedragis.DokumentyUdostoverjajushhieLichnostListModel>, IDokumentyUdostoverjajushhieLichnostListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "DokumentyUdostoverjajushhieLichnostListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public DokumentyUdostoverjajushhieLichnostListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost> Filter(IQueryable<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.DokumentyUdostoverjajushhieLichnostListModel> Map(IQueryable<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.DokumentyUdostoverjajushhieLichnost>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.DokumentyUdostoverjajushhieLichnostListModel{Id = x.Id, _TypeUid = "3d17ce3e-0019-4ef3-a9cf-ce1304271693", code = (System.String)(x.code), name1 = (System.String)(x.name1), description = (System.String)(x.description), });
        }
    }
}