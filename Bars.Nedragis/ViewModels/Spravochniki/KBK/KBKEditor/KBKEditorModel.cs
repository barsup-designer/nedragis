namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования КБК' для отдачи на клиент
    /// </summary>
    public class KBKEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public KBKEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Код'
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("969731c1-0faf-49ca-af75-765815afdbb5")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("4f9def96-b24b-432d-91cb-82249612b9cb")]
        public virtual System.String name1
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата начала действия'
        /// </summary>
        [Bars.B4.Utils.Display("Дата начала действия")]
        [Bars.Rms.Core.Attributes.Uid("59a6782e-2acd-45f9-b1c8-461771adf6e2")]
        public virtual System.DateTime? date_from
        {
            get;
            set;
        }
    }
}