namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования КБК'
    /// </summary>
    public interface IKBKEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.KBK, KBKEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования КБК'
    /// </summary>
    public interface IKBKEditorValidator : IEditorModelValidator<KBKEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования КБК'
    /// </summary>
    public interface IKBKEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.KBK, KBKEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования КБК'
    /// </summary>
    public abstract class AbstractKBKEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.KBK, KBKEditorModel>, IKBKEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования КБК'
    /// </summary>
    public class KBKEditorViewModel : BaseEditorViewModel<Bars.Nedragis.KBK, KBKEditorModel>, IKBKEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override KBKEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new KBKEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'КБК' в модель представления
        /// </summary>
        protected override KBKEditorModel MapEntityInternal(Bars.Nedragis.KBK entity)
        {
            // создаем экзепляр модели
            var model = new KBKEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name1 = (System.String)(entity.name1);
            model.date_from = (System.DateTime? )(entity.date_from);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'КБК' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.KBK entity, KBKEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name1 = model.name1;
            entity.date_from = model.date_from.GetValueOrDefault();
        }
    }
}