namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр КБК'
    /// </summary>
    public interface IKBKListQuery : IQueryOperation<Bars.Nedragis.KBK, Bars.Nedragis.KBKListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр КБК'
    /// </summary>
    public interface IKBKListQueryFilter : IQueryOperationFilter<Bars.Nedragis.KBK, Bars.Nedragis.KBKListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр КБК'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр КБК")]
    public class KBKListQuery : RmsEntityQueryOperation<Bars.Nedragis.KBK, Bars.Nedragis.KBKListModel>, IKBKListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "KBKListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public KBKListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.KBK> Filter(IQueryable<Bars.Nedragis.KBK> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.KBKListModel> Map(IQueryable<Bars.Nedragis.KBK> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.KBK>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.KBKListModel{Id = x.Id, _TypeUid = "27c717a5-3146-4aff-8401-828c5f8734b5", code = (System.String)(x.code), name1 = (System.String)(x.name1), date_from = (System.DateTime? )(x.date_from), });
        }
    }
}