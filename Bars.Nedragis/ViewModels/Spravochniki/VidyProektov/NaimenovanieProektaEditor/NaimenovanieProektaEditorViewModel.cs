namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Наименование проекта'
    /// </summary>
    public interface INaimenovanieProektaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.NaimenovanieProekta, NaimenovanieProektaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Наименование проекта'
    /// </summary>
    public interface INaimenovanieProektaEditorValidator : IEditorModelValidator<NaimenovanieProektaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Наименование проекта'
    /// </summary>
    public interface INaimenovanieProektaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.NaimenovanieProekta, NaimenovanieProektaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Наименование проекта'
    /// </summary>
    public abstract class AbstractNaimenovanieProektaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.NaimenovanieProekta, NaimenovanieProektaEditorModel>, INaimenovanieProektaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Наименование проекта'
    /// </summary>
    public class NaimenovanieProektaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.NaimenovanieProekta, NaimenovanieProektaEditorModel>, INaimenovanieProektaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override NaimenovanieProektaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new NaimenovanieProektaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Наименование проекта' в модель представления
        /// </summary>
        protected override NaimenovanieProektaEditorModel MapEntityInternal(Bars.Nedragis.NaimenovanieProekta entity)
        {
            // создаем экзепляр модели
            var model = new NaimenovanieProektaEditorModel();
            model.Id = entity.Id;
            model.Element1511755872431 = (System.String)(entity.Element1511755872431);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Наименование проекта' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.NaimenovanieProekta entity, NaimenovanieProektaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1511755872431 = model.Element1511755872431;
        }
    }
}