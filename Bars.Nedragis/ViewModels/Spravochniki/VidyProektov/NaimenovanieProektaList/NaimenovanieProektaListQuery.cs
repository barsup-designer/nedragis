namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Наименование проекта'
    /// </summary>
    public interface INaimenovanieProektaListQuery : IQueryOperation<Bars.Nedragis.NaimenovanieProekta, Bars.Nedragis.NaimenovanieProektaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Наименование проекта'
    /// </summary>
    public interface INaimenovanieProektaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.NaimenovanieProekta, Bars.Nedragis.NaimenovanieProektaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Наименование проекта'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Наименование проекта")]
    public class NaimenovanieProektaListQuery : RmsEntityQueryOperation<Bars.Nedragis.NaimenovanieProekta, Bars.Nedragis.NaimenovanieProektaListModel>, INaimenovanieProektaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "NaimenovanieProektaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public NaimenovanieProektaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.NaimenovanieProekta> Filter(IQueryable<Bars.Nedragis.NaimenovanieProekta> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.NaimenovanieProektaListModel> Map(IQueryable<Bars.Nedragis.NaimenovanieProekta> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.NaimenovanieProekta>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.NaimenovanieProektaListModel{Id = x.Id, _TypeUid = "216f9ac4-1368-4dff-97b0-1c1e19d23db4", nameproject = (System.String)(x.Element1511755872431), });
        }
    }
}