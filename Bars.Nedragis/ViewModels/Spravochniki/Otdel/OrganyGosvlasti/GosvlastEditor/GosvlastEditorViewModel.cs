namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Госвласть'
    /// </summary>
    public interface IGosvlastEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Gosvlast, GosvlastEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Госвласть'
    /// </summary>
    public interface IGosvlastEditorValidator : IEditorModelValidator<GosvlastEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Госвласть'
    /// </summary>
    public interface IGosvlastEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Gosvlast, GosvlastEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Госвласть'
    /// </summary>
    public abstract class AbstractGosvlastEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Gosvlast, GosvlastEditorModel>, IGosvlastEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Госвласть'
    /// </summary>
    public class GosvlastEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Gosvlast, GosvlastEditorModel>, IGosvlastEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override GosvlastEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new GosvlastEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Госвласть' в модель представления
        /// </summary>
        protected override GosvlastEditorModel MapEntityInternal(Bars.Nedragis.Gosvlast entity)
        {
            // создаем экзепляр модели
            var model = new GosvlastEditorModel();
            model.Id = entity.Id;
            model.Element1474350949546 = (System.String)(entity.Element1474350949546);
            model.Element1474350976371 = (System.String)(entity.Element1474350976371);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Госвласть' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Gosvlast entity, GosvlastEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1474350949546 = model.Element1474350949546;
            entity.Element1474350976371 = model.Element1474350976371;
        }
    }
}