namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Госвласть' для отдачи на клиент
    /// </summary>
    public class GosvlastEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public GosvlastEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("03f0f7f0-abdc-42f6-ac34-7c857a5d89ad")]
        public virtual System.String Element1474350949546
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Полное наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Полное наименование")]
        [Bars.Rms.Core.Attributes.Uid("8eff27e8-9762-4bbe-93df-36e614d973cd")]
        public virtual System.String Element1474350976371
        {
            get;
            set;
        }
    }
}