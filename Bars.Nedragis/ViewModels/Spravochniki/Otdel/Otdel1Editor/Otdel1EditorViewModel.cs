namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Отдел'
    /// </summary>
    public interface IOtdel1EditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Otdel1, Otdel1EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Отдел'
    /// </summary>
    public interface IOtdel1EditorValidator : IEditorModelValidator<Otdel1EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Отдел'
    /// </summary>
    public interface IOtdel1EditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Otdel1, Otdel1EditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Отдел'
    /// </summary>
    public abstract class AbstractOtdel1EditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Otdel1, Otdel1EditorModel>, IOtdel1EditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Отдел'
    /// </summary>
    public class Otdel1EditorViewModel : BaseEditorViewModel<Bars.Nedragis.Otdel1, Otdel1EditorModel>, IOtdel1EditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override Otdel1EditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new Otdel1EditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Отдел' в модель представления
        /// </summary>
        protected override Otdel1EditorModel MapEntityInternal(Bars.Nedragis.Otdel1 entity)
        {
            // создаем экзепляр модели
            var model = new Otdel1EditorModel();
            model.Id = entity.Id;
            model.NAME_SP = (System.String)(entity.NAME_SP);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Отдел' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Otdel1 entity, Otdel1EditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.NAME_SP = model.NAME_SP;
        }
    }
}