namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Вид работ природа'
    /// </summary>
    public interface IVidRabotPrirEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.VidRabotPrir, VidRabotPrirEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Вид работ природа'
    /// </summary>
    public interface IVidRabotPrirEditorValidator : IEditorModelValidator<VidRabotPrirEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Вид работ природа'
    /// </summary>
    public interface IVidRabotPrirEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.VidRabotPrir, VidRabotPrirEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Вид работ природа'
    /// </summary>
    public abstract class AbstractVidRabotPrirEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.VidRabotPrir, VidRabotPrirEditorModel>, IVidRabotPrirEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Вид работ природа'
    /// </summary>
    public class VidRabotPrirEditorViewModel : BaseEditorViewModel<Bars.Nedragis.VidRabotPrir, VidRabotPrirEditorModel>, IVidRabotPrirEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override VidRabotPrirEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new VidRabotPrirEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Вид работ природа' в модель представления
        /// </summary>
        protected override VidRabotPrirEditorModel MapEntityInternal(Bars.Nedragis.VidRabotPrir entity)
        {
            // создаем экзепляр модели
            var model = new VidRabotPrirEditorModel();
            model.Id = entity.Id;
            model.vid_rabot_prirod_s = (System.String)(entity.vid_rabot_prirod_s);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Вид работ природа' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.VidRabotPrir entity, VidRabotPrirEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.vid_rabot_prirod_s = model.vid_rabot_prirod_s;
        }
    }
}