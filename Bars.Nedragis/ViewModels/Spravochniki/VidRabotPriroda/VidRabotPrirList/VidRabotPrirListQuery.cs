namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Вид работ природа'
    /// </summary>
    public interface IVidRabotPrirListQuery : IQueryOperation<Bars.Nedragis.VidRabotPrir, Bars.Nedragis.VidRabotPrirListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Вид работ природа'
    /// </summary>
    public interface IVidRabotPrirListQueryFilter : IQueryOperationFilter<Bars.Nedragis.VidRabotPrir, Bars.Nedragis.VidRabotPrirListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Вид работ природа'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Вид работ природа")]
    public class VidRabotPrirListQuery : RmsEntityQueryOperation<Bars.Nedragis.VidRabotPrir, Bars.Nedragis.VidRabotPrirListModel>, IVidRabotPrirListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "VidRabotPrirListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public VidRabotPrirListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.VidRabotPrir> Filter(IQueryable<Bars.Nedragis.VidRabotPrir> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.VidRabotPrirListModel> Map(IQueryable<Bars.Nedragis.VidRabotPrir> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.VidRabotPrir>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.VidRabotPrirListModel{Id = x.Id, _TypeUid = "3d1718cc-c093-46d2-974e-1b0bc0fd51cb", vid_rabot_prirod_s = (System.String)(x.vid_rabot_prirod_s), });
        }
    }
}