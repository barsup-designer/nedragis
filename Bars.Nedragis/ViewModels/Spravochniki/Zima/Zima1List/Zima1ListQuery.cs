namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр zima1'
    /// </summary>
    public interface IZima1ListQuery : IQueryOperation<Bars.Nedragis.Zima1, Bars.Nedragis.Zima1ListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр zima1'
    /// </summary>
    public interface IZima1ListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Zima1, Bars.Nedragis.Zima1ListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр zima1'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр zima1")]
    public class Zima1ListQuery : RmsEntityQueryOperation<Bars.Nedragis.Zima1, Bars.Nedragis.Zima1ListModel>, IZima1ListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "Zima1ListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public Zima1ListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Zima1> Filter(IQueryable<Bars.Nedragis.Zima1> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.Zima1ListModel> Map(IQueryable<Bars.Nedragis.Zima1> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Zima1>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.Zima1ListModel{Id = x.Id, _TypeUid = "346b2315-bb41-485e-b013-fb0a7c9936d8", zima_reestr = (System.String)(x.Element1517294546876), });
        }
    }
}