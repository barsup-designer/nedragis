namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования zima1'
    /// </summary>
    public interface IZima1EditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Zima1, Zima1EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования zima1'
    /// </summary>
    public interface IZima1EditorValidator : IEditorModelValidator<Zima1EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования zima1'
    /// </summary>
    public interface IZima1EditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Zima1, Zima1EditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования zima1'
    /// </summary>
    public abstract class AbstractZima1EditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Zima1, Zima1EditorModel>, IZima1EditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования zima1'
    /// </summary>
    public class Zima1EditorViewModel : BaseEditorViewModel<Bars.Nedragis.Zima1, Zima1EditorModel>, IZima1EditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override Zima1EditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new Zima1EditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'zima1' в модель представления
        /// </summary>
        protected override Zima1EditorModel MapEntityInternal(Bars.Nedragis.Zima1 entity)
        {
            // создаем экзепляр модели
            var model = new Zima1EditorModel();
            model.Id = entity.Id;
            model.Element1517294546876 = (System.String)(entity.Element1517294546876);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'zima1' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Zima1 entity, Zima1EditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1517294546876 = model.Element1517294546876;
        }
    }
}