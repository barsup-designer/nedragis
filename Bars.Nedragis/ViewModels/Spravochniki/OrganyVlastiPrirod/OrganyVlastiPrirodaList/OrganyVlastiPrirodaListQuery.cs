namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Органы власти Природа'
    /// </summary>
    public interface IOrganyVlastiPrirodaListQuery : IQueryOperation<Bars.Nedragis.OrganyVlastiPriroda, Bars.Nedragis.OrganyVlastiPrirodaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Органы власти Природа'
    /// </summary>
    public interface IOrganyVlastiPrirodaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.OrganyVlastiPriroda, Bars.Nedragis.OrganyVlastiPrirodaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Органы власти Природа'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Органы власти Природа")]
    public class OrganyVlastiPrirodaListQuery : RmsEntityQueryOperation<Bars.Nedragis.OrganyVlastiPriroda, Bars.Nedragis.OrganyVlastiPrirodaListModel>, IOrganyVlastiPrirodaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OrganyVlastiPrirodaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OrganyVlastiPrirodaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OrganyVlastiPriroda> Filter(IQueryable<Bars.Nedragis.OrganyVlastiPriroda> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OrganyVlastiPrirodaListModel> Map(IQueryable<Bars.Nedragis.OrganyVlastiPriroda> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OrganyVlastiPriroda>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OrganyVlastiPrirodaListModel{Id = x.Id, _TypeUid = "800fae24-3869-4df3-be5d-3f5b9955b5e0", organ_vlasti_pr = (System.String)(x.organ_vlasti_pr), });
        }
    }
}