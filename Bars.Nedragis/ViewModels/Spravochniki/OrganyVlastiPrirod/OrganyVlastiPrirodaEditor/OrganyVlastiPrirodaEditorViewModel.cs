namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Органы власти Природа'
    /// </summary>
    public interface IOrganyVlastiPrirodaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.OrganyVlastiPriroda, OrganyVlastiPrirodaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Органы власти Природа'
    /// </summary>
    public interface IOrganyVlastiPrirodaEditorValidator : IEditorModelValidator<OrganyVlastiPrirodaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Органы власти Природа'
    /// </summary>
    public interface IOrganyVlastiPrirodaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.OrganyVlastiPriroda, OrganyVlastiPrirodaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Органы власти Природа'
    /// </summary>
    public abstract class AbstractOrganyVlastiPrirodaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.OrganyVlastiPriroda, OrganyVlastiPrirodaEditorModel>, IOrganyVlastiPrirodaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Органы власти Природа'
    /// </summary>
    public class OrganyVlastiPrirodaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.OrganyVlastiPriroda, OrganyVlastiPrirodaEditorModel>, IOrganyVlastiPrirodaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OrganyVlastiPrirodaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OrganyVlastiPrirodaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Органы власти Природа' в модель представления
        /// </summary>
        protected override OrganyVlastiPrirodaEditorModel MapEntityInternal(Bars.Nedragis.OrganyVlastiPriroda entity)
        {
            // создаем экзепляр модели
            var model = new OrganyVlastiPrirodaEditorModel();
            model.Id = entity.Id;
            model.organ_vlasti_pr = (System.String)(entity.organ_vlasti_pr);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Органы власти Природа' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.OrganyVlastiPriroda entity, OrganyVlastiPrirodaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.organ_vlasti_pr = model.organ_vlasti_pr;
        }
    }
}