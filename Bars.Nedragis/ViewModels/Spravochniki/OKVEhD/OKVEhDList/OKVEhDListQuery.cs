namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр ОКВЭД'
    /// </summary>
    public interface IOKVEhDListQuery : IQueryOperation<Bars.Nedragis.OKVEhD, Bars.Nedragis.OKVEhDListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр ОКВЭД'
    /// </summary>
    public interface IOKVEhDListQueryFilter : IQueryOperationFilter<Bars.Nedragis.OKVEhD, Bars.Nedragis.OKVEhDListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр ОКВЭД'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр ОКВЭД")]
    public class OKVEhDListQuery : RmsEntityQueryOperation<Bars.Nedragis.OKVEhD, Bars.Nedragis.OKVEhDListModel>, IOKVEhDListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OKVEhDListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OKVEhDListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKVEhD> Filter(IQueryable<Bars.Nedragis.OKVEhD> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OKVEhDListModel> Map(IQueryable<Bars.Nedragis.OKVEhD> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OKVEhD>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OKVEhDListModel{Id = x.Id, _TypeUid = "c6f1d936-9d21-451f-b3c5-8c562fc1cff7", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}