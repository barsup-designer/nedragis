namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования ОКВЭД'
    /// </summary>
    public interface IOKVEhDEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.OKVEhD, OKVEhDEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования ОКВЭД'
    /// </summary>
    public interface IOKVEhDEditorValidator : IEditorModelValidator<OKVEhDEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования ОКВЭД'
    /// </summary>
    public interface IOKVEhDEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.OKVEhD, OKVEhDEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования ОКВЭД'
    /// </summary>
    public abstract class AbstractOKVEhDEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.OKVEhD, OKVEhDEditorModel>, IOKVEhDEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования ОКВЭД'
    /// </summary>
    public class OKVEhDEditorViewModel : BaseEditorViewModel<Bars.Nedragis.OKVEhD, OKVEhDEditorModel>, IOKVEhDEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OKVEhDEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OKVEhDEditorModel();
            var varparent_idId = @params.Params.GetAs<long>("parent_id_Id", 0);
            if (varparent_idId > 0)
            {
                model.parent_id = Container.Resolve<Bars.Nedragis.IOKVEhD1Query>().GetById(varparent_idId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'ОКВЭД' в модель представления
        /// </summary>
        protected override OKVEhDEditorModel MapEntityInternal(Bars.Nedragis.OKVEhD entity)
        {
            // создаем экзепляр модели
            var model = new OKVEhDEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            if (entity.parent_id.IsNotNull())
            {
                var queryOKVEhD1 = Container.Resolve<Bars.Nedragis.IOKVEhD1Query>();
                model.parent_id = queryOKVEhD1.GetById(entity.parent_id.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'ОКВЭД' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.OKVEhD entity, OKVEhDEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
            entity.parent_id = TryLoadEntityById<Bars.Nedragis.OKVEhD>(model.parent_id?.Id);
        }
    }
}