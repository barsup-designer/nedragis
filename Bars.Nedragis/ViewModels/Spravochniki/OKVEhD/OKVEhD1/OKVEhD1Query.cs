namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'ОКВЭД'
    /// </summary>
    public interface IOKVEhD1Query : IQueryOperation<Bars.Nedragis.OKVEhD, Bars.Nedragis.OKVEhD1Model, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'ОКВЭД'
    /// </summary>
    public interface IOKVEhD1QueryFilter : IQueryOperationFilter<Bars.Nedragis.OKVEhD, Bars.Nedragis.OKVEhD1Model, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'ОКВЭД'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("ОКВЭД")]
    public class OKVEhD1Query : RmsTreeQueryOperation<Bars.Nedragis.OKVEhD, Bars.Nedragis.OKVEhD1Model>, IOKVEhD1Query
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OKVEhD1Query"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OKVEhD1Query(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKVEhD> Filter(IQueryable<Bars.Nedragis.OKVEhD> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
                var treeNode = @params.Params.GetAs<long ? >("node");
            }

            return query;
        }

        /// <summary>
        /// Фильтрация в зависимости от идентификатора родительского элемента
        /// </summary>
        /// <param name = "query"></param>
        /// <param name = "parentId"></param>
        /// <param name = "params"></param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKVEhD> FilterByParent(IQueryable<Bars.Nedragis.OKVEhD> query, long ? parentId, BaseParams @params)
        {
            return parentId == null ? query.Where(x => x.parent_id == null) : query.Where(x => x.parent_id != null && x.parent_id.Id == parentId.Value);
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OKVEhD1Model> Map(IQueryable<Bars.Nedragis.OKVEhD> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OKVEhD>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OKVEhD1Model{Id = x.Id, _TypeUid = "c6f1d936-9d21-451f-b3c5-8c562fc1cff7", code = (System.String)(x.code), name = (System.String)(x.name), IsLeaf = !query.Any(c => c.parent_id.Id == x.Id), });
        }
    }
}