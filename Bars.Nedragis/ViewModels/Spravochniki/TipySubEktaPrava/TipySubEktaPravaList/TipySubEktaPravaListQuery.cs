namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Типы субъекта права'
    /// </summary>
    public interface ITipySubEktaPravaListQuery : IQueryOperation<Bars.Nedragis.TipySubEktaPrava, Bars.Nedragis.TipySubEktaPravaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Типы субъекта права'
    /// </summary>
    public interface ITipySubEktaPravaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.TipySubEktaPrava, Bars.Nedragis.TipySubEktaPravaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Типы субъекта права'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Типы субъекта права")]
    public class TipySubEktaPravaListQuery : RmsEntityQueryOperation<Bars.Nedragis.TipySubEktaPrava, Bars.Nedragis.TipySubEktaPravaListModel>, ITipySubEktaPravaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "TipySubEktaPravaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public TipySubEktaPravaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.TipySubEktaPrava> Filter(IQueryable<Bars.Nedragis.TipySubEktaPrava> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.TipySubEktaPravaListModel> Map(IQueryable<Bars.Nedragis.TipySubEktaPrava> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.TipySubEktaPrava>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.TipySubEktaPravaListModel{Id = x.Id, _TypeUid = "f795e28f-f5a6-4099-a88d-c9e75f73f1a1", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}