namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Типы субъекта права'
    /// </summary>
    public interface ITipySubEktaPravaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.TipySubEktaPrava, TipySubEktaPravaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Типы субъекта права'
    /// </summary>
    public interface ITipySubEktaPravaEditorValidator : IEditorModelValidator<TipySubEktaPravaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Типы субъекта права'
    /// </summary>
    public interface ITipySubEktaPravaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.TipySubEktaPrava, TipySubEktaPravaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Типы субъекта права'
    /// </summary>
    public abstract class AbstractTipySubEktaPravaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.TipySubEktaPrava, TipySubEktaPravaEditorModel>, ITipySubEktaPravaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Типы субъекта права'
    /// </summary>
    public class TipySubEktaPravaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.TipySubEktaPrava, TipySubEktaPravaEditorModel>, ITipySubEktaPravaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override TipySubEktaPravaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new TipySubEktaPravaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Типы субъекта права' в модель представления
        /// </summary>
        protected override TipySubEktaPravaEditorModel MapEntityInternal(Bars.Nedragis.TipySubEktaPrava entity)
        {
            // создаем экзепляр модели
            var model = new TipySubEktaPravaEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Типы субъекта права' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.TipySubEktaPrava entity, TipySubEktaPravaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}