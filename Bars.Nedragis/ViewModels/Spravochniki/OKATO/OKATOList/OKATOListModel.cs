namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр ОКАТО'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр ОКАТО")]
    public class OKATOListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Код' (псевдоним: code)
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("927612ca-e3db-4112-90a1-b2ea6254b95d")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: name1)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("8f346b9d-a961-43a7-9db3-8b70f23cdb29")]
        public virtual System.String name1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Районный центр' (псевдоним: center_name)
        /// </summary>
        [Bars.B4.Utils.Display("Районный центр")]
        [Bars.Rms.Core.Attributes.Uid("9df69523-3746-4ee0-bbeb-17c1b5506e8b")]
        public virtual System.String center_name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Родительский элемент.Наименование' (псевдоним: parent_id_name1)
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("d17d6fc4-790a-4204-88cb-71d5a524baa1")]
        public virtual System.String parent_id_name1
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}