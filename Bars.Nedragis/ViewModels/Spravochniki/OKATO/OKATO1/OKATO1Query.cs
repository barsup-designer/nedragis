namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'ОКАТО'
    /// </summary>
    public interface IOKATO1Query : IQueryOperation<Bars.Nedragis.OKATO, Bars.Nedragis.OKATO1Model, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'ОКАТО'
    /// </summary>
    public interface IOKATO1QueryFilter : IQueryOperationFilter<Bars.Nedragis.OKATO, Bars.Nedragis.OKATO1Model, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'ОКАТО'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("ОКАТО")]
    public class OKATO1Query : RmsTreeQueryOperation<Bars.Nedragis.OKATO, Bars.Nedragis.OKATO1Model>, IOKATO1Query
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OKATO1Query"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OKATO1Query(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKATO> Filter(IQueryable<Bars.Nedragis.OKATO> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
                var treeNode = @params.Params.GetAs<long ? >("node");
            }

            return query;
        }

        /// <summary>
        /// Фильтрация в зависимости от идентификатора родительского элемента
        /// </summary>
        /// <param name = "query"></param>
        /// <param name = "parentId"></param>
        /// <param name = "params"></param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKATO> FilterByParent(IQueryable<Bars.Nedragis.OKATO> query, long ? parentId, BaseParams @params)
        {
            return parentId == null ? query.Where(x => x.parent_id == null) : query.Where(x => x.parent_id != null && x.parent_id.Id == parentId.Value);
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OKATO1Model> Map(IQueryable<Bars.Nedragis.OKATO> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OKATO>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OKATO1Model{Id = x.Id, _TypeUid = "209f5156-b2b9-4a80-b96a-100c2cf0083d", code = (System.String)(x.code), name1 = (System.String)(x.name1), center_name = (System.String)(x.center_name), IsLeaf = !query.Any(c => c.parent_id.Id == x.Id), });
        }
    }
}