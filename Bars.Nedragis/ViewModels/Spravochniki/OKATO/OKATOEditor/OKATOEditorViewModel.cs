namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования ОКАТО'
    /// </summary>
    public interface IOKATOEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.OKATO, OKATOEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования ОКАТО'
    /// </summary>
    public interface IOKATOEditorValidator : IEditorModelValidator<OKATOEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования ОКАТО'
    /// </summary>
    public interface IOKATOEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.OKATO, OKATOEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования ОКАТО'
    /// </summary>
    public abstract class AbstractOKATOEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.OKATO, OKATOEditorModel>, IOKATOEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования ОКАТО'
    /// </summary>
    public class OKATOEditorViewModel : BaseEditorViewModel<Bars.Nedragis.OKATO, OKATOEditorModel>, IOKATOEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OKATOEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OKATOEditorModel();
            var varparent_idId = @params.Params.GetAs<long>("parent_id_Id", 0);
            if (varparent_idId > 0)
            {
                model.parent_id = Container.Resolve<Bars.Nedragis.IOKATO1Query>().GetById(varparent_idId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'ОКАТО' в модель представления
        /// </summary>
        protected override OKATOEditorModel MapEntityInternal(Bars.Nedragis.OKATO entity)
        {
            // создаем экзепляр модели
            var model = new OKATOEditorModel();
            model.Id = entity.Id;
            if (entity.parent_id.IsNotNull())
            {
                var queryOKATO1 = Container.Resolve<Bars.Nedragis.IOKATO1Query>();
                model.parent_id = queryOKATO1.GetById(entity.parent_id.Id);
            }

            model.code = (System.String)(entity.code);
            model.name1 = (System.String)(entity.name1);
            model.center_name = (System.String)(entity.center_name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'ОКАТО' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.OKATO entity, OKATOEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.parent_id = TryLoadEntityById<Bars.Nedragis.OKATO>(model.parent_id?.Id);
            entity.code = model.code;
            entity.name1 = model.name1;
            entity.center_name = model.center_name;
        }
    }
}