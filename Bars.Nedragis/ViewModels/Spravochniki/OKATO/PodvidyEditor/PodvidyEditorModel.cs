namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Подвиды' для отдачи на клиент
    /// </summary>
    public class PodvidyEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public PodvidyEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("c45c9a0f-b6ba-49b4-92c9-c945e8d56994")]
        public virtual System.String Element1474267693056
        {
            get;
            set;
        }

        /// <summary>
        /// вид
        /// </summary>
        [Bars.B4.Utils.Display("вид")]
        [Bars.Rms.Core.Attributes.Uid("bd34f7f4-57ec-4eb1-b81b-21c28ea7c9ef")]
        public virtual Bars.Nedragis.VidyRabotListModel Element1474349306046
        {
            get;
            set;
        }
    }
}