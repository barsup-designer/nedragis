namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Подвиды'
    /// </summary>
    public interface IPodvidyEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Podvidy, PodvidyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Подвиды'
    /// </summary>
    public interface IPodvidyEditorValidator : IEditorModelValidator<PodvidyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Подвиды'
    /// </summary>
    public interface IPodvidyEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Podvidy, PodvidyEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Подвиды'
    /// </summary>
    public abstract class AbstractPodvidyEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Podvidy, PodvidyEditorModel>, IPodvidyEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Подвиды'
    /// </summary>
    public class PodvidyEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Podvidy, PodvidyEditorModel>, IPodvidyEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override PodvidyEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new PodvidyEditorModel();
            var varElement1474349306046Id = @params.Params.GetAs<long>("Element1474349306046_Id", 0);
            if (varElement1474349306046Id > 0)
            {
                model.Element1474349306046 = Container.Resolve<Bars.Nedragis.IVidyRabotListQuery>().GetById(varElement1474349306046Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Подвиды' в модель представления
        /// </summary>
        protected override PodvidyEditorModel MapEntityInternal(Bars.Nedragis.Podvidy entity)
        {
            // создаем экзепляр модели
            var model = new PodvidyEditorModel();
            model.Id = entity.Id;
            model.Element1474267693056 = (System.String)(entity.Element1474267693056);
            if (entity.Element1474349306046.IsNotNull())
            {
                var queryVidyRabotList = Container.Resolve<Bars.Nedragis.IVidyRabotListQuery>();
                model.Element1474349306046 = queryVidyRabotList.GetById(entity.Element1474349306046.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Подвиды' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Podvidy entity, PodvidyEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1474267693056 = model.Element1474267693056;
            entity.Element1474349306046 = TryLoadEntityById<Bars.Nedragis.VidyRabot>(model.Element1474349306046?.Id);
        }
    }
}