namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования ОКФС'
    /// </summary>
    public interface IOKFSEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.OKFS, OKFSEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования ОКФС'
    /// </summary>
    public interface IOKFSEditorValidator : IEditorModelValidator<OKFSEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования ОКФС'
    /// </summary>
    public interface IOKFSEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.OKFS, OKFSEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования ОКФС'
    /// </summary>
    public abstract class AbstractOKFSEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.OKFS, OKFSEditorModel>, IOKFSEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования ОКФС'
    /// </summary>
    public class OKFSEditorViewModel : BaseEditorViewModel<Bars.Nedragis.OKFS, OKFSEditorModel>, IOKFSEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OKFSEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OKFSEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'ОКФС' в модель представления
        /// </summary>
        protected override OKFSEditorModel MapEntityInternal(Bars.Nedragis.OKFS entity)
        {
            // создаем экзепляр модели
            var model = new OKFSEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            model.check_algorithm = (System.String)(entity.check_algorithm);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'ОКФС' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.OKFS entity, OKFSEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
            entity.check_algorithm = model.check_algorithm;
        }
    }
}