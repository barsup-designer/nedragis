namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр ОКФС'
    /// </summary>
    public interface IOKFSListQuery : IQueryOperation<Bars.Nedragis.OKFS, Bars.Nedragis.OKFSListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр ОКФС'
    /// </summary>
    public interface IOKFSListQueryFilter : IQueryOperationFilter<Bars.Nedragis.OKFS, Bars.Nedragis.OKFSListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр ОКФС'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр ОКФС")]
    public class OKFSListQuery : RmsEntityQueryOperation<Bars.Nedragis.OKFS, Bars.Nedragis.OKFSListModel>, IOKFSListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OKFSListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OKFSListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKFS> Filter(IQueryable<Bars.Nedragis.OKFS> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OKFSListModel> Map(IQueryable<Bars.Nedragis.OKFS> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OKFS>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OKFSListModel{Id = x.Id, _TypeUid = "c2b0994f-1f3c-411c-9f98-f03071f14265", code = (System.String)(x.code), name = (System.String)(x.name), check_algorithm = (System.String)(x.check_algorithm), });
        }
    }
}