namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Тип договора купли-продажи'
    /// </summary>
    public interface ITipDogovoraKupliProdazhiEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.TipDogovoraKupliProdazhi, TipDogovoraKupliProdazhiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Тип договора купли-продажи'
    /// </summary>
    public interface ITipDogovoraKupliProdazhiEditorValidator : IEditorModelValidator<TipDogovoraKupliProdazhiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Тип договора купли-продажи'
    /// </summary>
    public interface ITipDogovoraKupliProdazhiEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.TipDogovoraKupliProdazhi, TipDogovoraKupliProdazhiEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Тип договора купли-продажи'
    /// </summary>
    public abstract class AbstractTipDogovoraKupliProdazhiEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.TipDogovoraKupliProdazhi, TipDogovoraKupliProdazhiEditorModel>, ITipDogovoraKupliProdazhiEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Тип договора купли-продажи'
    /// </summary>
    public class TipDogovoraKupliProdazhiEditorViewModel : BaseEditorViewModel<Bars.Nedragis.TipDogovoraKupliProdazhi, TipDogovoraKupliProdazhiEditorModel>, ITipDogovoraKupliProdazhiEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override TipDogovoraKupliProdazhiEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new TipDogovoraKupliProdazhiEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Тип договора купли-продажи' в модель представления
        /// </summary>
        protected override TipDogovoraKupliProdazhiEditorModel MapEntityInternal(Bars.Nedragis.TipDogovoraKupliProdazhi entity)
        {
            // создаем экзепляр модели
            var model = new TipDogovoraKupliProdazhiEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Тип договора купли-продажи' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.TipDogovoraKupliProdazhi entity, TipDogovoraKupliProdazhiEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}