namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Тип договора купли-продажи'
    /// </summary>
    public interface ITipDogovoraKupliProdazhiListQuery : IQueryOperation<Bars.Nedragis.TipDogovoraKupliProdazhi, Bars.Nedragis.TipDogovoraKupliProdazhiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Тип договора купли-продажи'
    /// </summary>
    public interface ITipDogovoraKupliProdazhiListQueryFilter : IQueryOperationFilter<Bars.Nedragis.TipDogovoraKupliProdazhi, Bars.Nedragis.TipDogovoraKupliProdazhiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Тип договора купли-продажи'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Тип договора купли-продажи")]
    public class TipDogovoraKupliProdazhiListQuery : RmsEntityQueryOperation<Bars.Nedragis.TipDogovoraKupliProdazhi, Bars.Nedragis.TipDogovoraKupliProdazhiListModel>, ITipDogovoraKupliProdazhiListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "TipDogovoraKupliProdazhiListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public TipDogovoraKupliProdazhiListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.TipDogovoraKupliProdazhi> Filter(IQueryable<Bars.Nedragis.TipDogovoraKupliProdazhi> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.TipDogovoraKupliProdazhiListModel> Map(IQueryable<Bars.Nedragis.TipDogovoraKupliProdazhi> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.TipDogovoraKupliProdazhi>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.TipDogovoraKupliProdazhiListModel{Id = x.Id, _TypeUid = "925276c8-b776-4527-87f9-f859857aa018", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}