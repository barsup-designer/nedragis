namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр ВРИ ЗУ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр ВРИ ЗУ")]
    public class VRIZUListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Код' (псевдоним: code)
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("003e706e-3d8f-4c2f-89fb-911fb655f16c")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: name1)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("e490efb4-b88d-497d-be00-8bd48b42e225")]
        public virtual System.String name1
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Деятельность правообладателя' (псевдоним: description)
        /// </summary>
        [Bars.B4.Utils.Display("Деятельность правообладателя")]
        [Bars.Rms.Core.Attributes.Uid("1e1de963-1f78-47c9-9714-ebe9f9e24446")]
        public virtual System.String description
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Родительский элемент.Наименование' (псевдоним: parent_name1)
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("0e4a390e-011f-45da-9068-7fb303b33cf9")]
        public virtual System.String parent_name1
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}