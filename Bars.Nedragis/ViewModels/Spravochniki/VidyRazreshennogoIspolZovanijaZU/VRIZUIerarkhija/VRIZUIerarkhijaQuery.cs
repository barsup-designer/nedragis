namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Виды разрешенного использования'
    /// </summary>
    public interface IVRIZUIerarkhijaQuery : IQueryOperation<Bars.Nedragis.VRIZU, Bars.Nedragis.VRIZUIerarkhijaModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Виды разрешенного использования'
    /// </summary>
    public interface IVRIZUIerarkhijaQueryFilter : IQueryOperationFilter<Bars.Nedragis.VRIZU, Bars.Nedragis.VRIZUIerarkhijaModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Виды разрешенного использования'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Виды разрешенного использования")]
    public class VRIZUIerarkhijaQuery : RmsTreeQueryOperation<Bars.Nedragis.VRIZU, Bars.Nedragis.VRIZUIerarkhijaModel>, IVRIZUIerarkhijaQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "VRIZUIerarkhijaQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public VRIZUIerarkhijaQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.VRIZU> Filter(IQueryable<Bars.Nedragis.VRIZU> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
                var treeNode = @params.Params.GetAs<long ? >("node");
            }

            return query;
        }

        /// <summary>
        /// Фильтрация в зависимости от идентификатора родительского элемента
        /// </summary>
        /// <param name = "query"></param>
        /// <param name = "parentId"></param>
        /// <param name = "params"></param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.VRIZU> FilterByParent(IQueryable<Bars.Nedragis.VRIZU> query, long ? parentId, BaseParams @params)
        {
            return parentId == null ? query.Where(x => x.parent == null) : query.Where(x => x.parent != null && x.parent.Id == parentId.Value);
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.VRIZUIerarkhijaModel> Map(IQueryable<Bars.Nedragis.VRIZU> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.VRIZU>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.VRIZUIerarkhijaModel{Id = x.Id, _TypeUid = "5db62416-3df3-4972-a540-4abd50af9c5c", code = (System.String)(x.code), name1 = (System.String)(x.name1), description = (System.String)(x.description), IsLeaf = !query.Any(c => c.parent.Id == x.Id), });
        }
    }
}