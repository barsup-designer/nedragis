namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования ВРИ ЗУ' для отдачи на клиент
    /// </summary>
    public class VRIZUEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public VRIZUEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент")]
        [Bars.Rms.Core.Attributes.Uid("c4442e25-e9af-4569-86ac-374a6d0f4acc")]
        public virtual Bars.Nedragis.VRIZUIerarkhijaModel parent
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Код'
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("f5b8f26a-32ad-4aa4-a150-f61a5980c79c")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("00fda78a-7d27-454e-8d49-9f8ae4fa3b82")]
        public virtual System.String name1
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Деятельность правообладателя'
        /// </summary>
        [Bars.B4.Utils.Display("Деятельность правообладателя")]
        [Bars.Rms.Core.Attributes.Uid("75eb3dcf-9edf-4257-a52c-1f6b9ea1bd6c")]
        public virtual System.String description
        {
            get;
            set;
        }
    }
}