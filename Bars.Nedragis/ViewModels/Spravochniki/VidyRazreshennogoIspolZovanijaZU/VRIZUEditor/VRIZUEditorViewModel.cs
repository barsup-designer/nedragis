namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования ВРИ ЗУ'
    /// </summary>
    public interface IVRIZUEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.VRIZU, VRIZUEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования ВРИ ЗУ'
    /// </summary>
    public interface IVRIZUEditorValidator : IEditorModelValidator<VRIZUEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования ВРИ ЗУ'
    /// </summary>
    public interface IVRIZUEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.VRIZU, VRIZUEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования ВРИ ЗУ'
    /// </summary>
    public abstract class AbstractVRIZUEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.VRIZU, VRIZUEditorModel>, IVRIZUEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования ВРИ ЗУ'
    /// </summary>
    public class VRIZUEditorViewModel : BaseEditorViewModel<Bars.Nedragis.VRIZU, VRIZUEditorModel>, IVRIZUEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override VRIZUEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new VRIZUEditorModel();
            var varparentId = @params.Params.GetAs<long>("parent_Id", 0);
            if (varparentId > 0)
            {
                model.parent = Container.Resolve<Bars.Nedragis.IVRIZUIerarkhijaQuery>().GetById(varparentId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'ВРИ ЗУ' в модель представления
        /// </summary>
        protected override VRIZUEditorModel MapEntityInternal(Bars.Nedragis.VRIZU entity)
        {
            // создаем экзепляр модели
            var model = new VRIZUEditorModel();
            model.Id = entity.Id;
            if (entity.parent.IsNotNull())
            {
                var queryVRIZUIerarkhija = Container.Resolve<Bars.Nedragis.IVRIZUIerarkhijaQuery>();
                model.parent = queryVRIZUIerarkhija.GetById(entity.parent.Id);
            }

            model.code = (System.String)(entity.code);
            model.name1 = (System.String)(entity.name1);
            model.description = (System.String)(entity.description);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'ВРИ ЗУ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.VRIZU entity, VRIZUEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.parent = TryLoadEntityById<Bars.Nedragis.VRIZU>(model.parent?.Id);
            entity.code = model.code;
            entity.name1 = model.name1;
            entity.description = model.description;
        }
    }
}