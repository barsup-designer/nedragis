namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Наименование направления работ'
    /// </summary>
    public interface INaimenovanieNapravlenijaRaskhodovanijaSredstvListQuery : IQueryOperation<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv, Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Наименование направления работ'
    /// </summary>
    public interface INaimenovanieNapravlenijaRaskhodovanijaSredstvListQueryFilter : IQueryOperationFilter<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv, Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Наименование направления работ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Наименование направления работ")]
    public class NaimenovanieNapravlenijaRaskhodovanijaSredstvListQuery : RmsEntityQueryOperation<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv, Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel>, INaimenovanieNapravlenijaRaskhodovanijaSredstvListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "NaimenovanieNapravlenijaRaskhodovanijaSredstvListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public NaimenovanieNapravlenijaRaskhodovanijaSredstvListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv> Filter(IQueryable<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel> Map(IQueryable<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel{Id = x.Id, _TypeUid = "fae507ae-27bd-4670-8651-083e8137f7b7", Element1455708457805 = (System.String)(x.Element1455708457805), reestrvp = (System.String)(x.Element1511847414376), });
        }
    }
}