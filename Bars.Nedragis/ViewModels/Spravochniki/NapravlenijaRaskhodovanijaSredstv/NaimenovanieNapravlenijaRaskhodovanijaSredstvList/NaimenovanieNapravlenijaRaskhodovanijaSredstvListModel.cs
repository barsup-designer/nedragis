namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Наименование направления работ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Наименование направления работ")]
    public class NaimenovanieNapravlenijaRaskhodovanijaSredstvListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: Element1455708457805)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("b92c89ff-e21b-4fde-8a86-a055c8336ea0")]
        public virtual System.String Element1455708457805
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Вид' (псевдоним: reestrvp)
        /// </summary>
        [Bars.B4.Utils.Display("Вид")]
        [Bars.Rms.Core.Attributes.Uid("52fbd57f-e150-4b09-8bc5-ea5404376dc9")]
        public virtual System.String reestrvp
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}