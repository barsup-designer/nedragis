namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Вид ошибки' для отдачи на клиент
    /// </summary>
    public class VidOshibkiEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public VidOshibkiEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Вид ошибки'
        /// </summary>
        [Bars.B4.Utils.Display("Вид ошибки")]
        [Bars.Rms.Core.Attributes.Uid("956af6c7-444d-43d5-bc9a-7943c3c65654")]
        public virtual System.String Element1511848489595
        {
            get;
            set;
        }
    }
}