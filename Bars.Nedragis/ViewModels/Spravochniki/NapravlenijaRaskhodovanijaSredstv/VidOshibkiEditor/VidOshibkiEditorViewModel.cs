namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Вид ошибки'
    /// </summary>
    public interface IVidOshibkiEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.VidOshibki, VidOshibkiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Вид ошибки'
    /// </summary>
    public interface IVidOshibkiEditorValidator : IEditorModelValidator<VidOshibkiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Вид ошибки'
    /// </summary>
    public interface IVidOshibkiEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.VidOshibki, VidOshibkiEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Вид ошибки'
    /// </summary>
    public abstract class AbstractVidOshibkiEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.VidOshibki, VidOshibkiEditorModel>, IVidOshibkiEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Вид ошибки'
    /// </summary>
    public class VidOshibkiEditorViewModel : BaseEditorViewModel<Bars.Nedragis.VidOshibki, VidOshibkiEditorModel>, IVidOshibkiEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override VidOshibkiEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new VidOshibkiEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Вид ошибки' в модель представления
        /// </summary>
        protected override VidOshibkiEditorModel MapEntityInternal(Bars.Nedragis.VidOshibki entity)
        {
            // создаем экзепляр модели
            var model = new VidOshibkiEditorModel();
            model.Id = entity.Id;
            model.Element1511848489595 = (System.String)(entity.Element1511848489595);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Вид ошибки' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.VidOshibki entity, VidOshibkiEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1511848489595 = model.Element1511848489595;
        }
    }
}