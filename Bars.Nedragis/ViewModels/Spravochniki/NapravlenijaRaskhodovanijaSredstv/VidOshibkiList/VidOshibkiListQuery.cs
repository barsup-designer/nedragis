namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Вид ошибки'
    /// </summary>
    public interface IVidOshibkiListQuery : IQueryOperation<Bars.Nedragis.VidOshibki, Bars.Nedragis.VidOshibkiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Вид ошибки'
    /// </summary>
    public interface IVidOshibkiListQueryFilter : IQueryOperationFilter<Bars.Nedragis.VidOshibki, Bars.Nedragis.VidOshibkiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Вид ошибки'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Вид ошибки")]
    public class VidOshibkiListQuery : RmsEntityQueryOperation<Bars.Nedragis.VidOshibki, Bars.Nedragis.VidOshibkiListModel>, IVidOshibkiListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "VidOshibkiListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public VidOshibkiListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.VidOshibki> Filter(IQueryable<Bars.Nedragis.VidOshibki> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.VidOshibkiListModel> Map(IQueryable<Bars.Nedragis.VidOshibki> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.VidOshibki>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.VidOshibkiListModel{Id = x.Id, _TypeUid = "3a3bd732-b623-4fde-9f5f-d0420fd87b86", Element1511848489595 = (System.String)(x.Element1511848489595), });
        }
    }
}