namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Наименование направления'
    /// </summary>
    public interface INaimenovanieNapravlenijaRaskhodovanijaSredstvEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv, NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Наименование направления'
    /// </summary>
    public interface INaimenovanieNapravlenijaRaskhodovanijaSredstvEditorValidator : IEditorModelValidator<NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Наименование направления'
    /// </summary>
    public interface INaimenovanieNapravlenijaRaskhodovanijaSredstvEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv, NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Наименование направления'
    /// </summary>
    public abstract class AbstractNaimenovanieNapravlenijaRaskhodovanijaSredstvEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv, NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel>, INaimenovanieNapravlenijaRaskhodovanijaSredstvEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Наименование направления'
    /// </summary>
    public class NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorViewModel : BaseEditorViewModel<Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv, NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel>, INaimenovanieNapravlenijaRaskhodovanijaSredstvEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Наименование проекта' в модель представления
        /// </summary>
        protected override NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel MapEntityInternal(Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv entity)
        {
            // создаем экзепляр модели
            var model = new NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel();
            model.Id = entity.Id;
            model.Element1455708457805 = (System.String)(entity.Element1455708457805);
            model.Element1511847414376 = (System.String)(entity.Element1511847414376);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Наименование проекта' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv entity, NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1455708457805 = model.Element1455708457805;
            entity.Element1511847414376 = model.Element1511847414376;
        }
    }
}