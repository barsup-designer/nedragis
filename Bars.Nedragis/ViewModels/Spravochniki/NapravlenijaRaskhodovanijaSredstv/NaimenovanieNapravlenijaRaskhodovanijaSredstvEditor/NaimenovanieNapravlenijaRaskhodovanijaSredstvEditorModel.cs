namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Наименование направления' для отдачи на клиент
    /// </summary>
    public class NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public NaimenovanieNapravlenijaRaskhodovanijaSredstvEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("fb16cd5b-f7c0-4161-b7ce-42804a2db7af")]
        public virtual System.String Element1455708457805
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Вид'
        /// </summary>
        [Bars.B4.Utils.Display("Вид")]
        [Bars.Rms.Core.Attributes.Uid("627f4239-2e08-4180-b10a-0917bbe6d66a")]
        public virtual System.String Element1511847414376
        {
            get;
            set;
        }
    }
}