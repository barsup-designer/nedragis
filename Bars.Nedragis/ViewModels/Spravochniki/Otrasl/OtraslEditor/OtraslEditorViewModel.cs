namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Отрасль'
    /// </summary>
    public interface IOtraslEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Otrasl, OtraslEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Отрасль'
    /// </summary>
    public interface IOtraslEditorValidator : IEditorModelValidator<OtraslEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Отрасль'
    /// </summary>
    public interface IOtraslEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Otrasl, OtraslEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Отрасль'
    /// </summary>
    public abstract class AbstractOtraslEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Otrasl, OtraslEditorModel>, IOtraslEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Отрасль'
    /// </summary>
    public class OtraslEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Otrasl, OtraslEditorModel>, IOtraslEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OtraslEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OtraslEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Отрасль' в модель представления
        /// </summary>
        protected override OtraslEditorModel MapEntityInternal(Bars.Nedragis.Otrasl entity)
        {
            // создаем экзепляр модели
            var model = new OtraslEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Отрасль' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Otrasl entity, OtraslEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}