namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Отрасль'
    /// </summary>
    public interface IOtraslListQuery : IQueryOperation<Bars.Nedragis.Otrasl, Bars.Nedragis.OtraslListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Отрасль'
    /// </summary>
    public interface IOtraslListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Otrasl, Bars.Nedragis.OtraslListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Отрасль'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Отрасль")]
    public class OtraslListQuery : RmsEntityQueryOperation<Bars.Nedragis.Otrasl, Bars.Nedragis.OtraslListModel>, IOtraslListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OtraslListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OtraslListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Otrasl> Filter(IQueryable<Bars.Nedragis.Otrasl> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OtraslListModel> Map(IQueryable<Bars.Nedragis.Otrasl> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Otrasl>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OtraslListModel{Id = x.Id, _TypeUid = "86d46288-6a31-43c1-93b8-e0320f3c1bc6", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}