namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Проекты'
    /// </summary>
    public interface IProektyEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Proekty, ProektyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Проекты'
    /// </summary>
    public interface IProektyEditorValidator : IEditorModelValidator<ProektyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Проекты'
    /// </summary>
    public interface IProektyEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Proekty, ProektyEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Проекты'
    /// </summary>
    public abstract class AbstractProektyEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Proekty, ProektyEditorModel>, IProektyEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Проекты'
    /// </summary>
    public class ProektyEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Proekty, ProektyEditorModel>, IProektyEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ProektyEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new ProektyEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Проекты' в модель представления
        /// </summary>
        protected override ProektyEditorModel MapEntityInternal(Bars.Nedragis.Proekty entity)
        {
            // создаем экзепляр модели
            var model = new ProektyEditorModel();
            model.Id = entity.Id;
            model.Element1511335229126 = (System.String)(entity.Element1511335229126);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Проекты' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Proekty entity, ProektyEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1511335229126 = model.Element1511335229126;
        }
    }
}