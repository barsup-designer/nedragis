namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Проекты'
    /// </summary>
    public interface IProektyListQuery : IQueryOperation<Bars.Nedragis.Proekty, Bars.Nedragis.ProektyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Проекты'
    /// </summary>
    public interface IProektyListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Proekty, Bars.Nedragis.ProektyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Проекты'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Проекты")]
    public class ProektyListQuery : RmsEntityQueryOperation<Bars.Nedragis.Proekty, Bars.Nedragis.ProektyListModel>, IProektyListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "ProektyListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public ProektyListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Proekty> Filter(IQueryable<Bars.Nedragis.Proekty> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.ProektyListModel> Map(IQueryable<Bars.Nedragis.Proekty> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Proekty>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.ProektyListModel{Id = x.Id, _TypeUid = "4e1f118d-0d5d-441f-97ac-09bbc5021945", nameproject = (System.String)(x.Element1511335229126), });
        }
    }
}