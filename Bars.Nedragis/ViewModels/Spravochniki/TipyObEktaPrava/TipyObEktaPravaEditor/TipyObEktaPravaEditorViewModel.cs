namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Типы объекта права'
    /// </summary>
    public interface ITipyObEktaPravaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.TipyObEktaPrava, TipyObEktaPravaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Типы объекта права'
    /// </summary>
    public interface ITipyObEktaPravaEditorValidator : IEditorModelValidator<TipyObEktaPravaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Типы объекта права'
    /// </summary>
    public interface ITipyObEktaPravaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.TipyObEktaPrava, TipyObEktaPravaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Типы объекта права'
    /// </summary>
    public abstract class AbstractTipyObEktaPravaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.TipyObEktaPrava, TipyObEktaPravaEditorModel>, ITipyObEktaPravaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Типы объекта права'
    /// </summary>
    public class TipyObEktaPravaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.TipyObEktaPrava, TipyObEktaPravaEditorModel>, ITipyObEktaPravaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override TipyObEktaPravaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new TipyObEktaPravaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Типы объекта права' в модель представления
        /// </summary>
        protected override TipyObEktaPravaEditorModel MapEntityInternal(Bars.Nedragis.TipyObEktaPrava entity)
        {
            // создаем экзепляр модели
            var model = new TipyObEktaPravaEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Типы объекта права' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.TipyObEktaPrava entity, TipyObEktaPravaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}