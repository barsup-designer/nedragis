namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Типы объекта права'
    /// </summary>
    public interface ITipyObEktaPravaListQuery : IQueryOperation<Bars.Nedragis.TipyObEktaPrava, Bars.Nedragis.TipyObEktaPravaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Типы объекта права'
    /// </summary>
    public interface ITipyObEktaPravaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.TipyObEktaPrava, Bars.Nedragis.TipyObEktaPravaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Типы объекта права'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Типы объекта права")]
    public class TipyObEktaPravaListQuery : RmsEntityQueryOperation<Bars.Nedragis.TipyObEktaPrava, Bars.Nedragis.TipyObEktaPravaListModel>, ITipyObEktaPravaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "TipyObEktaPravaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public TipyObEktaPravaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.TipyObEktaPrava> Filter(IQueryable<Bars.Nedragis.TipyObEktaPrava> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.TipyObEktaPravaListModel> Map(IQueryable<Bars.Nedragis.TipyObEktaPrava> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.TipyObEktaPrava>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.TipyObEktaPravaListModel{Id = x.Id, _TypeUid = "3ac22ac5-70e7-4061-a191-d95169f8895c", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}