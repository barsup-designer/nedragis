namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'ОКОФ'
    /// </summary>
    public interface IOKOF1Query : IQueryOperation<Bars.Nedragis.OKOF, Bars.Nedragis.OKOF1Model, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'ОКОФ'
    /// </summary>
    public interface IOKOF1QueryFilter : IQueryOperationFilter<Bars.Nedragis.OKOF, Bars.Nedragis.OKOF1Model, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'ОКОФ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("ОКОФ")]
    public class OKOF1Query : RmsTreeQueryOperation<Bars.Nedragis.OKOF, Bars.Nedragis.OKOF1Model>, IOKOF1Query
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OKOF1Query"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OKOF1Query(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKOF> Filter(IQueryable<Bars.Nedragis.OKOF> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
                var treeNode = @params.Params.GetAs<long ? >("node");
            }

            return query;
        }

        /// <summary>
        /// Фильтрация в зависимости от идентификатора родительского элемента
        /// </summary>
        /// <param name = "query"></param>
        /// <param name = "parentId"></param>
        /// <param name = "params"></param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKOF> FilterByParent(IQueryable<Bars.Nedragis.OKOF> query, long ? parentId, BaseParams @params)
        {
            return parentId == null ? query.Where(x => x.parent_id == null) : query.Where(x => x.parent_id != null && x.parent_id.Id == parentId.Value);
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OKOF1Model> Map(IQueryable<Bars.Nedragis.OKOF> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OKOF>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OKOF1Model{Id = x.Id, _TypeUid = "594403ac-d85f-4ef6-8815-bf4c01f3b7f9", clasifier_code = (System.String)(x.clasifier.code), clasifier_name = (System.String)(x.clasifier.name), code = (System.String)(x.code), name = (System.String)(x.name), check_number = (System.Int32? )(x.check_number), IsLeaf = !query.Any(c => c.parent_id.Id == x.Id), });
        }
    }
}