namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'ОКОФ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("ОКОФ")]
    public class OKOF1Model
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Классификация.Код' (псевдоним: clasifier_code)
        /// </summary>
        [Bars.B4.Utils.Display("Классификация.Код")]
        [Bars.Rms.Core.Attributes.Uid("fd6c2de8-d531-4c73-9ed8-d9f5a82d6730")]
        public virtual System.String clasifier_code
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Классификация.Наименование' (псевдоним: clasifier_name)
        /// </summary>
        [Bars.B4.Utils.Display("Классификация.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("0333f746-1fd3-4402-a7c6-0d2fbb56463f")]
        public virtual System.String clasifier_name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Код' (псевдоним: code)
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("ecb679fc-138c-4b56-8405-311529c9900e")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: name)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("b1834d91-8bc2-46a4-8b6f-a9c6d2712d57")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Контрольное число' (псевдоним: check_number)
        /// </summary>
        [Bars.B4.Utils.Display("Контрольное число")]
        [Bars.Rms.Core.Attributes.Uid("39f5a8d3-4a53-4749-ab0b-26e384951a66")]
        public virtual System.Int32? check_number
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}