namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр ОКОФ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр ОКОФ")]
    public class OKOFListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Код' (псевдоним: code)
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("cf61a6de-6ddb-4380-905f-17df526a103f")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: name)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("802894c0-020a-42d3-9e01-a9d0fc6fc4f0")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Контрольное число' (псевдоним: check_number)
        /// </summary>
        [Bars.B4.Utils.Display("Контрольное число")]
        [Bars.Rms.Core.Attributes.Uid("50393dad-b698-4511-9105-6349ab9871c7")]
        public virtual System.Int32? check_number
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Классификация.Наименование' (псевдоним: clasifier_name)
        /// </summary>
        [Bars.B4.Utils.Display("Классификация.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("cd73df12-2edf-42df-bb36-d39c0fd71728")]
        public virtual System.String clasifier_name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Родительский элемент.Наименование' (псевдоним: parent_id_name)
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("a6920602-2be9-4731-b3c7-9b9ed1bfcb4c")]
        public virtual System.String parent_id_name
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}