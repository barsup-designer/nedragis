namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр ОКОФ'
    /// </summary>
    public interface IOKOFListQuery : IQueryOperation<Bars.Nedragis.OKOF, Bars.Nedragis.OKOFListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр ОКОФ'
    /// </summary>
    public interface IOKOFListQueryFilter : IQueryOperationFilter<Bars.Nedragis.OKOF, Bars.Nedragis.OKOFListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр ОКОФ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр ОКОФ")]
    public class OKOFListQuery : RmsEntityQueryOperation<Bars.Nedragis.OKOF, Bars.Nedragis.OKOFListModel>, IOKOFListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OKOFListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OKOFListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKOF> Filter(IQueryable<Bars.Nedragis.OKOF> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OKOFListModel> Map(IQueryable<Bars.Nedragis.OKOF> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OKOF>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OKOFListModel{Id = x.Id, _TypeUid = "594403ac-d85f-4ef6-8815-bf4c01f3b7f9", code = (System.String)(x.code), name = (System.String)(x.name), check_number = (System.Int32? )(x.check_number), clasifier_name = (System.String)(x.clasifier.name), parent_id_name = (System.String)(x.parent_id.name), });
        }
    }
}