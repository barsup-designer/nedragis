namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования ОКОФ' для отдачи на клиент
    /// </summary>
    public class OKOFEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public OKOFEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Код'
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("317120c7-3936-4343-a9d4-cef24005984c")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Классификация
        /// </summary>
        [Bars.B4.Utils.Display("Классификация")]
        [Bars.Rms.Core.Attributes.Uid("49c3118f-2273-45e9-a800-4346393c464d")]
        public virtual Bars.Nedragis.KlassifikacijaOKOFListModel clasifier
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("52aaf887-958c-4769-bcb7-a5fda548285a")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент")]
        [Bars.Rms.Core.Attributes.Uid("259ec901-d5b7-4208-94d0-52d219a90a41")]
        public virtual Bars.Nedragis.OKOF1Model parent_id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Контрольное число'
        /// </summary>
        [Bars.B4.Utils.Display("Контрольное число")]
        [Bars.Rms.Core.Attributes.Uid("554bf167-33c0-4b30-aa77-0f109e527154")]
        public virtual System.Int32? check_number
        {
            get;
            set;
        }
    }
}