namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования ОКОФ'
    /// </summary>
    public interface IOKOFEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.OKOF, OKOFEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования ОКОФ'
    /// </summary>
    public interface IOKOFEditorValidator : IEditorModelValidator<OKOFEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования ОКОФ'
    /// </summary>
    public interface IOKOFEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.OKOF, OKOFEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования ОКОФ'
    /// </summary>
    public abstract class AbstractOKOFEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.OKOF, OKOFEditorModel>, IOKOFEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования ОКОФ'
    /// </summary>
    public class OKOFEditorViewModel : BaseEditorViewModel<Bars.Nedragis.OKOF, OKOFEditorModel>, IOKOFEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OKOFEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OKOFEditorModel();
            var varclasifierId = @params.Params.GetAs<long>("clasifier_Id", 0);
            if (varclasifierId > 0)
            {
                model.clasifier = Container.Resolve<Bars.Nedragis.IKlassifikacijaOKOFListQuery>().GetById(varclasifierId);
            }

            var varparent_idId = @params.Params.GetAs<long>("parent_id_Id", 0);
            if (varparent_idId > 0)
            {
                model.parent_id = Container.Resolve<Bars.Nedragis.IOKOF1Query>().GetById(varparent_idId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'ОКОФ' в модель представления
        /// </summary>
        protected override OKOFEditorModel MapEntityInternal(Bars.Nedragis.OKOF entity)
        {
            // создаем экзепляр модели
            var model = new OKOFEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            if (entity.clasifier.IsNotNull())
            {
                var queryKlassifikacijaOKOFList = Container.Resolve<Bars.Nedragis.IKlassifikacijaOKOFListQuery>();
                model.clasifier = queryKlassifikacijaOKOFList.GetById(entity.clasifier.Id);
            }

            model.name = (System.String)(entity.name);
            if (entity.parent_id.IsNotNull())
            {
                var queryOKOF1 = Container.Resolve<Bars.Nedragis.IOKOF1Query>();
                model.parent_id = queryOKOF1.GetById(entity.parent_id.Id);
            }

            model.check_number = (System.Int32? )(entity.check_number);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'ОКОФ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.OKOF entity, OKOFEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.clasifier = TryLoadEntityById<Bars.Nedragis.KlassifikacijaOKOF>(model.clasifier?.Id);
            entity.name = model.name;
            entity.parent_id = TryLoadEntityById<Bars.Nedragis.OKOF>(model.parent_id?.Id);
            if (model.check_number.HasValue)
            {
                entity.check_number = model.check_number.Value;
            }
        }
    }
}