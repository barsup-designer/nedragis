namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Классификация ОКОФ'
    /// </summary>
    public interface IKlassifikacijaOKOFListQuery : IQueryOperation<Bars.Nedragis.KlassifikacijaOKOF, Bars.Nedragis.KlassifikacijaOKOFListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Классификация ОКОФ'
    /// </summary>
    public interface IKlassifikacijaOKOFListQueryFilter : IQueryOperationFilter<Bars.Nedragis.KlassifikacijaOKOF, Bars.Nedragis.KlassifikacijaOKOFListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Классификация ОКОФ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Классификация ОКОФ")]
    public class KlassifikacijaOKOFListQuery : RmsEntityQueryOperation<Bars.Nedragis.KlassifikacijaOKOF, Bars.Nedragis.KlassifikacijaOKOFListModel>, IKlassifikacijaOKOFListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "KlassifikacijaOKOFListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public KlassifikacijaOKOFListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.KlassifikacijaOKOF> Filter(IQueryable<Bars.Nedragis.KlassifikacijaOKOF> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.KlassifikacijaOKOFListModel> Map(IQueryable<Bars.Nedragis.KlassifikacijaOKOF> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.KlassifikacijaOKOF>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.KlassifikacijaOKOFListModel{Id = x.Id, _TypeUid = "f103b0bd-3d0c-418c-b3cc-f68fd6b5ec2f", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}