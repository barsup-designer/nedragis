namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Классификация ОКОФ'
    /// </summary>
    public interface IKlassifikacijaOKOFEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.KlassifikacijaOKOF, KlassifikacijaOKOFEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Классификация ОКОФ'
    /// </summary>
    public interface IKlassifikacijaOKOFEditorValidator : IEditorModelValidator<KlassifikacijaOKOFEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Классификация ОКОФ'
    /// </summary>
    public interface IKlassifikacijaOKOFEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.KlassifikacijaOKOF, KlassifikacijaOKOFEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Классификация ОКОФ'
    /// </summary>
    public abstract class AbstractKlassifikacijaOKOFEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.KlassifikacijaOKOF, KlassifikacijaOKOFEditorModel>, IKlassifikacijaOKOFEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Классификация ОКОФ'
    /// </summary>
    public class KlassifikacijaOKOFEditorViewModel : BaseEditorViewModel<Bars.Nedragis.KlassifikacijaOKOF, KlassifikacijaOKOFEditorModel>, IKlassifikacijaOKOFEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override KlassifikacijaOKOFEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new KlassifikacijaOKOFEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Классификация ОКОФ' в модель представления
        /// </summary>
        protected override KlassifikacijaOKOFEditorModel MapEntityInternal(Bars.Nedragis.KlassifikacijaOKOF entity)
        {
            // создаем экзепляр модели
            var model = new KlassifikacijaOKOFEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Классификация ОКОФ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.KlassifikacijaOKOF entity, KlassifikacijaOKOFEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}