namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Классификация ОКОФ' для отдачи на клиент
    /// </summary>
    public class KlassifikacijaOKOFEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public KlassifikacijaOKOFEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Код'
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("66ffb858-8282-4816-830c-b2f71ebd41ac")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("ebc613f9-3adc-44d8-a809-49f78b8be4c6")]
        public virtual System.String name
        {
            get;
            set;
        }
    }
}