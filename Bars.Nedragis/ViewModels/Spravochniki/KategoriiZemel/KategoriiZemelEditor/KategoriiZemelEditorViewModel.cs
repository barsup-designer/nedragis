namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Категории земель'
    /// </summary>
    public interface IKategoriiZemelEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.KategoriiZemel, KategoriiZemelEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Категории земель'
    /// </summary>
    public interface IKategoriiZemelEditorValidator : IEditorModelValidator<KategoriiZemelEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Категории земель'
    /// </summary>
    public interface IKategoriiZemelEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.KategoriiZemel, KategoriiZemelEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Категории земель'
    /// </summary>
    public abstract class AbstractKategoriiZemelEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.KategoriiZemel, KategoriiZemelEditorModel>, IKategoriiZemelEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Категории земель'
    /// </summary>
    public class KategoriiZemelEditorViewModel : BaseEditorViewModel<Bars.Nedragis.KategoriiZemel, KategoriiZemelEditorModel>, IKategoriiZemelEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override KategoriiZemelEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new KategoriiZemelEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Категории земель' в модель представления
        /// </summary>
        protected override KategoriiZemelEditorModel MapEntityInternal(Bars.Nedragis.KategoriiZemel entity)
        {
            // создаем экзепляр модели
            var model = new KategoriiZemelEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name1 = (System.String)(entity.name1);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Категории земель' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.KategoriiZemel entity, KategoriiZemelEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name1 = model.name1;
        }
    }
}