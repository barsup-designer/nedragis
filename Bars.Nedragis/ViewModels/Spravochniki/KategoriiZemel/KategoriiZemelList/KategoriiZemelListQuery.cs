namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Категории земель'
    /// </summary>
    public interface IKategoriiZemelListQuery : IQueryOperation<Bars.Nedragis.KategoriiZemel, Bars.Nedragis.KategoriiZemelListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Категории земель'
    /// </summary>
    public interface IKategoriiZemelListQueryFilter : IQueryOperationFilter<Bars.Nedragis.KategoriiZemel, Bars.Nedragis.KategoriiZemelListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Категории земель'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Категории земель")]
    public class KategoriiZemelListQuery : RmsEntityQueryOperation<Bars.Nedragis.KategoriiZemel, Bars.Nedragis.KategoriiZemelListModel>, IKategoriiZemelListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "KategoriiZemelListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public KategoriiZemelListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.KategoriiZemel> Filter(IQueryable<Bars.Nedragis.KategoriiZemel> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.KategoriiZemelListModel> Map(IQueryable<Bars.Nedragis.KategoriiZemel> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.KategoriiZemel>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.KategoriiZemelListModel{Id = x.Id, _TypeUid = "50693b32-18d4-45e9-81f8-9b94eaf76c13", code = (System.String)(x.code), name1 = (System.String)(x.name1), });
        }
    }
}