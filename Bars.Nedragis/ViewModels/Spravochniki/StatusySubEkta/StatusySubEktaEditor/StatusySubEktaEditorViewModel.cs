namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Статусы субъекта'
    /// </summary>
    public interface IStatusySubEktaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.StatusySubEkta, StatusySubEktaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Статусы субъекта'
    /// </summary>
    public interface IStatusySubEktaEditorValidator : IEditorModelValidator<StatusySubEktaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Статусы субъекта'
    /// </summary>
    public interface IStatusySubEktaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.StatusySubEkta, StatusySubEktaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Статусы субъекта'
    /// </summary>
    public abstract class AbstractStatusySubEktaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.StatusySubEkta, StatusySubEktaEditorModel>, IStatusySubEktaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Статусы субъекта'
    /// </summary>
    public class StatusySubEktaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.StatusySubEkta, StatusySubEktaEditorModel>, IStatusySubEktaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override StatusySubEktaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new StatusySubEktaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Статусы субъекта' в модель представления
        /// </summary>
        protected override StatusySubEktaEditorModel MapEntityInternal(Bars.Nedragis.StatusySubEkta entity)
        {
            // создаем экзепляр модели
            var model = new StatusySubEktaEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Статусы субъекта' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.StatusySubEkta entity, StatusySubEktaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}