namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Реестры'
    /// </summary>
    public interface IReestryEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Reestry, ReestryEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Реестры'
    /// </summary>
    public interface IReestryEditorValidator : IEditorModelValidator<ReestryEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Реестры'
    /// </summary>
    public interface IReestryEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Reestry, ReestryEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Реестры'
    /// </summary>
    public abstract class AbstractReestryEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Reestry, ReestryEditorModel>, IReestryEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Реестры'
    /// </summary>
    public class ReestryEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Reestry, ReestryEditorModel>, IReestryEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override ReestryEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new ReestryEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Реестры' в модель представления
        /// </summary>
        protected override ReestryEditorModel MapEntityInternal(Bars.Nedragis.Reestry entity)
        {
            // создаем экзепляр модели
            var model = new ReestryEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Реестры' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Reestry entity, ReestryEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}