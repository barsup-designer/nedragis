namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Вид ограничения'
    /// </summary>
    public interface IVidOgranichenijaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.VidOgranichenija, VidOgranichenijaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Вид ограничения'
    /// </summary>
    public interface IVidOgranichenijaEditorValidator : IEditorModelValidator<VidOgranichenijaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Вид ограничения'
    /// </summary>
    public interface IVidOgranichenijaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.VidOgranichenija, VidOgranichenijaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Вид ограничения'
    /// </summary>
    public abstract class AbstractVidOgranichenijaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.VidOgranichenija, VidOgranichenijaEditorModel>, IVidOgranichenijaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Вид ограничения'
    /// </summary>
    public class VidOgranichenijaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.VidOgranichenija, VidOgranichenijaEditorModel>, IVidOgranichenijaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override VidOgranichenijaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new VidOgranichenijaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Вид ограничения' в модель представления
        /// </summary>
        protected override VidOgranichenijaEditorModel MapEntityInternal(Bars.Nedragis.VidOgranichenija entity)
        {
            // создаем экзепляр модели
            var model = new VidOgranichenijaEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name1 = (System.String)(entity.name1);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Вид ограничения' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.VidOgranichenija entity, VidOgranichenijaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name1 = model.name1;
        }
    }
}