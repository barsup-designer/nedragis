namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Вид ограничения'
    /// </summary>
    public interface IVidOgranichenijaListQuery : IQueryOperation<Bars.Nedragis.VidOgranichenija, Bars.Nedragis.VidOgranichenijaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Вид ограничения'
    /// </summary>
    public interface IVidOgranichenijaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.VidOgranichenija, Bars.Nedragis.VidOgranichenijaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Вид ограничения'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Вид ограничения")]
    public class VidOgranichenijaListQuery : RmsEntityQueryOperation<Bars.Nedragis.VidOgranichenija, Bars.Nedragis.VidOgranichenijaListModel>, IVidOgranichenijaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "VidOgranichenijaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public VidOgranichenijaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.VidOgranichenija> Filter(IQueryable<Bars.Nedragis.VidOgranichenija> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.VidOgranichenijaListModel> Map(IQueryable<Bars.Nedragis.VidOgranichenija> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.VidOgranichenija>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.VidOgranichenijaListModel{Id = x.Id, _TypeUid = "819d41cc-49ef-46e6-8c2d-b426986e0144", code = (System.String)(x.code), name1 = (System.String)(x.name1), });
        }
    }
}