namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Организации' для отдачи на клиент
    /// </summary>
    public class OrganizaciiEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public OrganizaciiEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("64e0b549-f5f9-40b1-9f4c-68dced028de9")]
        public virtual System.String Element1511331964541
        {
            get;
            set;
        }

        /// <summary>
        /// мо
        /// </summary>
        [Bars.B4.Utils.Display("мо")]
        [Bars.Rms.Core.Attributes.Uid("dc883c7b-82b6-49a6-a5f8-153e667a3ccd")]
        public virtual Bars.Nedragis.NaimenovanieMOListModel Element1511331983555
        {
            get;
            set;
        }
    }
}