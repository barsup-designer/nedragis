namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Организации'
    /// </summary>
    public interface IOrganizaciiEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Organizacii, OrganizaciiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Организации'
    /// </summary>
    public interface IOrganizaciiEditorValidator : IEditorModelValidator<OrganizaciiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Организации'
    /// </summary>
    public interface IOrganizaciiEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Organizacii, OrganizaciiEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Организации'
    /// </summary>
    public abstract class AbstractOrganizaciiEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Organizacii, OrganizaciiEditorModel>, IOrganizaciiEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Организации'
    /// </summary>
    public class OrganizaciiEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Organizacii, OrganizaciiEditorModel>, IOrganizaciiEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OrganizaciiEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OrganizaciiEditorModel();
            var varElement1511331983555Id = @params.Params.GetAs<long>("Element1511331983555_Id", 0);
            if (varElement1511331983555Id > 0)
            {
                model.Element1511331983555 = Container.Resolve<Bars.Nedragis.INaimenovanieMOListQuery>().GetById(varElement1511331983555Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Организации' в модель представления
        /// </summary>
        protected override OrganizaciiEditorModel MapEntityInternal(Bars.Nedragis.Organizacii entity)
        {
            // создаем экзепляр модели
            var model = new OrganizaciiEditorModel();
            model.Id = entity.Id;
            model.Element1511331964541 = (System.String)(entity.Element1511331964541);
            if (entity.Element1511331983555.IsNotNull())
            {
                var queryNaimenovanieMOList = Container.Resolve<Bars.Nedragis.INaimenovanieMOListQuery>();
                model.Element1511331983555 = queryNaimenovanieMOList.GetById(entity.Element1511331983555.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Организации' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Organizacii entity, OrganizaciiEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1511331964541 = model.Element1511331964541;
            entity.Element1511331983555 = TryLoadEntityById<Bars.Nedragis.NaimenovanieMO>(model.Element1511331983555?.Id);
        }
    }
}