namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Организации'
    /// </summary>
    public interface IOrganizaciiListQuery : IQueryOperation<Bars.Nedragis.Organizacii, Bars.Nedragis.OrganizaciiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Организации'
    /// </summary>
    public interface IOrganizaciiListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Organizacii, Bars.Nedragis.OrganizaciiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Организации'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Организации")]
    public class OrganizaciiListQuery : RmsEntityQueryOperation<Bars.Nedragis.Organizacii, Bars.Nedragis.OrganizaciiListModel>, IOrganizaciiListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OrganizaciiListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OrganizaciiListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Organizacii> Filter(IQueryable<Bars.Nedragis.Organizacii> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OrganizaciiListModel> Map(IQueryable<Bars.Nedragis.Organizacii> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Organizacii>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OrganizaciiListModel{Id = x.Id, _TypeUid = "fc380625-4f7d-4faf-a793-8060a22ae929", Element1511331964541 = (System.String)(x.Element1511331964541), hgf123 = (System.String)(x.Element1511331983555.Element1455707802552), });
        }
    }
}