namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Вид права на землю'
    /// </summary>
    public interface IVidPravaNaZemljuEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.VidPravaNaZemlju, VidPravaNaZemljuEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Вид права на землю'
    /// </summary>
    public interface IVidPravaNaZemljuEditorValidator : IEditorModelValidator<VidPravaNaZemljuEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Вид права на землю'
    /// </summary>
    public interface IVidPravaNaZemljuEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.VidPravaNaZemlju, VidPravaNaZemljuEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Вид права на землю'
    /// </summary>
    public abstract class AbstractVidPravaNaZemljuEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.VidPravaNaZemlju, VidPravaNaZemljuEditorModel>, IVidPravaNaZemljuEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Вид права на землю'
    /// </summary>
    public class VidPravaNaZemljuEditorViewModel : BaseEditorViewModel<Bars.Nedragis.VidPravaNaZemlju, VidPravaNaZemljuEditorModel>, IVidPravaNaZemljuEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override VidPravaNaZemljuEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new VidPravaNaZemljuEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Вид права на землю' в модель представления
        /// </summary>
        protected override VidPravaNaZemljuEditorModel MapEntityInternal(Bars.Nedragis.VidPravaNaZemlju entity)
        {
            // создаем экзепляр модели
            var model = new VidPravaNaZemljuEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name1 = (System.String)(entity.name1);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Вид права на землю' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.VidPravaNaZemlju entity, VidPravaNaZemljuEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name1 = model.name1;
        }
    }
}