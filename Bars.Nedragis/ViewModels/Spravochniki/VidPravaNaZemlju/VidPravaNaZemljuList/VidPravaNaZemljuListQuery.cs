namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Вид права на землю'
    /// </summary>
    public interface IVidPravaNaZemljuListQuery : IQueryOperation<Bars.Nedragis.VidPravaNaZemlju, Bars.Nedragis.VidPravaNaZemljuListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Вид права на землю'
    /// </summary>
    public interface IVidPravaNaZemljuListQueryFilter : IQueryOperationFilter<Bars.Nedragis.VidPravaNaZemlju, Bars.Nedragis.VidPravaNaZemljuListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Вид права на землю'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Вид права на землю")]
    public class VidPravaNaZemljuListQuery : RmsEntityQueryOperation<Bars.Nedragis.VidPravaNaZemlju, Bars.Nedragis.VidPravaNaZemljuListModel>, IVidPravaNaZemljuListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "VidPravaNaZemljuListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public VidPravaNaZemljuListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.VidPravaNaZemlju> Filter(IQueryable<Bars.Nedragis.VidPravaNaZemlju> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.VidPravaNaZemljuListModel> Map(IQueryable<Bars.Nedragis.VidPravaNaZemlju> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.VidPravaNaZemlju>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.VidPravaNaZemljuListModel{Id = x.Id, _TypeUid = "5a74b33e-dffc-409a-af27-01e29e56ce83", code = (System.String)(x.code), name1 = (System.String)(x.name1), });
        }
    }
}