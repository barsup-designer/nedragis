namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Районы ЯНАО'
    /// </summary>
    public interface IRajjonyJaNAOEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.RajjonyJaNAO, RajjonyJaNAOEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Районы ЯНАО'
    /// </summary>
    public interface IRajjonyJaNAOEditorValidator : IEditorModelValidator<RajjonyJaNAOEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Районы ЯНАО'
    /// </summary>
    public interface IRajjonyJaNAOEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.RajjonyJaNAO, RajjonyJaNAOEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Районы ЯНАО'
    /// </summary>
    public abstract class AbstractRajjonyJaNAOEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.RajjonyJaNAO, RajjonyJaNAOEditorModel>, IRajjonyJaNAOEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Районы ЯНАО'
    /// </summary>
    public class RajjonyJaNAOEditorViewModel : BaseEditorViewModel<Bars.Nedragis.RajjonyJaNAO, RajjonyJaNAOEditorModel>, IRajjonyJaNAOEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override RajjonyJaNAOEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new RajjonyJaNAOEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Районы ЯНАО' в модель представления
        /// </summary>
        protected override RajjonyJaNAOEditorModel MapEntityInternal(Bars.Nedragis.RajjonyJaNAO entity)
        {
            // создаем экзепляр модели
            var model = new RajjonyJaNAOEditorModel();
            model.Id = entity.Id;
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Районы ЯНАО' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.RajjonyJaNAO entity, RajjonyJaNAOEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name = model.name;
        }
    }
}