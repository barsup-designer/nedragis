namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Районы ЯНАО'
    /// </summary>
    public interface IRajjonyJaNAOListQuery : IQueryOperation<Bars.Nedragis.RajjonyJaNAO, Bars.Nedragis.RajjonyJaNAOListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Районы ЯНАО'
    /// </summary>
    public interface IRajjonyJaNAOListQueryFilter : IQueryOperationFilter<Bars.Nedragis.RajjonyJaNAO, Bars.Nedragis.RajjonyJaNAOListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Районы ЯНАО'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Районы ЯНАО")]
    public class RajjonyJaNAOListQuery : RmsEntityQueryOperation<Bars.Nedragis.RajjonyJaNAO, Bars.Nedragis.RajjonyJaNAOListModel>, IRajjonyJaNAOListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "RajjonyJaNAOListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public RajjonyJaNAOListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.RajjonyJaNAO> Filter(IQueryable<Bars.Nedragis.RajjonyJaNAO> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.RajjonyJaNAOListModel> Map(IQueryable<Bars.Nedragis.RajjonyJaNAO> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.RajjonyJaNAO>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.RajjonyJaNAOListModel{Id = x.Id, _TypeUid = "d214293d-e00f-4a6c-a27e-795e72642b89", name = (System.String)(x.name), });
        }
    }
}