namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Вид права объекта'
    /// </summary>
    public interface IVidPravaObEktaListQuery : IQueryOperation<Bars.Nedragis.VidPravaObEkta, Bars.Nedragis.VidPravaObEktaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Вид права объекта'
    /// </summary>
    public interface IVidPravaObEktaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.VidPravaObEkta, Bars.Nedragis.VidPravaObEktaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Вид права объекта'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Вид права объекта")]
    public class VidPravaObEktaListQuery : RmsEntityQueryOperation<Bars.Nedragis.VidPravaObEkta, Bars.Nedragis.VidPravaObEktaListModel>, IVidPravaObEktaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "VidPravaObEktaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public VidPravaObEktaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.VidPravaObEkta> Filter(IQueryable<Bars.Nedragis.VidPravaObEkta> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.VidPravaObEktaListModel> Map(IQueryable<Bars.Nedragis.VidPravaObEkta> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.VidPravaObEkta>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.VidPravaObEktaListModel{Id = x.Id, _TypeUid = "bc0928cd-24af-4ec6-9445-ba8b0e7367f7", code = (System.String)(x.code), name1 = (System.String)(x.name1), });
        }
    }
}