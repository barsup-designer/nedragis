namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Сотрудники'
    /// </summary>
    public interface ISotrudniki1ListQuery : IQueryOperation<Bars.Nedragis.Sotrudniki1, Bars.Nedragis.Sotrudniki1ListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Сотрудники'
    /// </summary>
    public interface ISotrudniki1ListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Sotrudniki1, Bars.Nedragis.Sotrudniki1ListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Сотрудники'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Сотрудники")]
    public class Sotrudniki1ListQuery : RmsEntityQueryOperation<Bars.Nedragis.Sotrudniki1, Bars.Nedragis.Sotrudniki1ListModel>, ISotrudniki1ListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "Sotrudniki1ListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public Sotrudniki1ListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Sotrudniki1> Filter(IQueryable<Bars.Nedragis.Sotrudniki1> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.Sotrudniki1ListModel> Map(IQueryable<Bars.Nedragis.Sotrudniki1> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Sotrudniki1>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.Sotrudniki1ListModel{Id = x.Id, _TypeUid = "3ae04932-5d4f-410a-9da5-985f7dfcb52b", Element1474352136232 = (System.String)(x.Element1474352136232), });
        }
    }
}