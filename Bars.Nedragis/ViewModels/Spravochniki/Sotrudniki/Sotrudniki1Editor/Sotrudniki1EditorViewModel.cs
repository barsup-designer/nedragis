namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Сотрудники'
    /// </summary>
    public interface ISotrudniki1EditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Sotrudniki1, Sotrudniki1EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Сотрудники'
    /// </summary>
    public interface ISotrudniki1EditorValidator : IEditorModelValidator<Sotrudniki1EditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Сотрудники'
    /// </summary>
    public interface ISotrudniki1EditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Sotrudniki1, Sotrudniki1EditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Сотрудники'
    /// </summary>
    public abstract class AbstractSotrudniki1EditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Sotrudniki1, Sotrudniki1EditorModel>, ISotrudniki1EditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Сотрудники'
    /// </summary>
    public class Sotrudniki1EditorViewModel : BaseEditorViewModel<Bars.Nedragis.Sotrudniki1, Sotrudniki1EditorModel>, ISotrudniki1EditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override Sotrudniki1EditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new Sotrudniki1EditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Сотрудники' в модель представления
        /// </summary>
        protected override Sotrudniki1EditorModel MapEntityInternal(Bars.Nedragis.Sotrudniki1 entity)
        {
            // создаем экзепляр модели
            var model = new Sotrudniki1EditorModel();
            model.Id = entity.Id;
            model.Element1474352136232 = (System.String)(entity.Element1474352136232);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Сотрудники' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Sotrudniki1 entity, Sotrudniki1EditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1474352136232 = model.Element1474352136232;
        }
    }
}