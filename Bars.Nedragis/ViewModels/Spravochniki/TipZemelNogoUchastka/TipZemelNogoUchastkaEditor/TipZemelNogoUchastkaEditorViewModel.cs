namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Тип земельного участка'
    /// </summary>
    public interface ITipZemelNogoUchastkaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.TipZemelNogoUchastka, TipZemelNogoUchastkaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Тип земельного участка'
    /// </summary>
    public interface ITipZemelNogoUchastkaEditorValidator : IEditorModelValidator<TipZemelNogoUchastkaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Тип земельного участка'
    /// </summary>
    public interface ITipZemelNogoUchastkaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.TipZemelNogoUchastka, TipZemelNogoUchastkaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Тип земельного участка'
    /// </summary>
    public abstract class AbstractTipZemelNogoUchastkaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.TipZemelNogoUchastka, TipZemelNogoUchastkaEditorModel>, ITipZemelNogoUchastkaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Тип земельного участка'
    /// </summary>
    public class TipZemelNogoUchastkaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.TipZemelNogoUchastka, TipZemelNogoUchastkaEditorModel>, ITipZemelNogoUchastkaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override TipZemelNogoUchastkaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new TipZemelNogoUchastkaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Тип земельного участка' в модель представления
        /// </summary>
        protected override TipZemelNogoUchastkaEditorModel MapEntityInternal(Bars.Nedragis.TipZemelNogoUchastka entity)
        {
            // создаем экзепляр модели
            var model = new TipZemelNogoUchastkaEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Тип земельного участка' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.TipZemelNogoUchastka entity, TipZemelNogoUchastkaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}