namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Тип земельного участка'
    /// </summary>
    public interface ITipZemelNogoUchastkaListQuery : IQueryOperation<Bars.Nedragis.TipZemelNogoUchastka, Bars.Nedragis.TipZemelNogoUchastkaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Тип земельного участка'
    /// </summary>
    public interface ITipZemelNogoUchastkaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.TipZemelNogoUchastka, Bars.Nedragis.TipZemelNogoUchastkaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Тип земельного участка'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Тип земельного участка")]
    public class TipZemelNogoUchastkaListQuery : RmsEntityQueryOperation<Bars.Nedragis.TipZemelNogoUchastka, Bars.Nedragis.TipZemelNogoUchastkaListModel>, ITipZemelNogoUchastkaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "TipZemelNogoUchastkaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public TipZemelNogoUchastkaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.TipZemelNogoUchastka> Filter(IQueryable<Bars.Nedragis.TipZemelNogoUchastka> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.TipZemelNogoUchastkaListModel> Map(IQueryable<Bars.Nedragis.TipZemelNogoUchastka> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.TipZemelNogoUchastka>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.TipZemelNogoUchastkaListModel{Id = x.Id, _TypeUid = "8682edd5-0d38-494b-b3b9-245397ccddcb", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}