namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования МесяцПрирода'
    /// </summary>
    public interface IMesjacPrirodaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.MesjacPriroda, MesjacPrirodaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования МесяцПрирода'
    /// </summary>
    public interface IMesjacPrirodaEditorValidator : IEditorModelValidator<MesjacPrirodaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования МесяцПрирода'
    /// </summary>
    public interface IMesjacPrirodaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.MesjacPriroda, MesjacPrirodaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования МесяцПрирода'
    /// </summary>
    public abstract class AbstractMesjacPrirodaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.MesjacPriroda, MesjacPrirodaEditorModel>, IMesjacPrirodaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования МесяцПрирода'
    /// </summary>
    public class MesjacPrirodaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.MesjacPriroda, MesjacPrirodaEditorModel>, IMesjacPrirodaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override MesjacPrirodaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new MesjacPrirodaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'МесяцПрирода' в модель представления
        /// </summary>
        protected override MesjacPrirodaEditorModel MapEntityInternal(Bars.Nedragis.MesjacPriroda entity)
        {
            // создаем экзепляр модели
            var model = new MesjacPrirodaEditorModel();
            model.Id = entity.Id;
            model.month_pr = (System.String)(entity.month_pr);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'МесяцПрирода' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.MesjacPriroda entity, MesjacPrirodaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.month_pr = model.month_pr;
        }
    }
}