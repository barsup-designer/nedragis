namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр МесяцПрирода'
    /// </summary>
    public interface IMesjacPrirodaListQuery : IQueryOperation<Bars.Nedragis.MesjacPriroda, Bars.Nedragis.MesjacPrirodaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр МесяцПрирода'
    /// </summary>
    public interface IMesjacPrirodaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.MesjacPriroda, Bars.Nedragis.MesjacPrirodaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр МесяцПрирода'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр МесяцПрирода")]
    public class MesjacPrirodaListQuery : RmsEntityQueryOperation<Bars.Nedragis.MesjacPriroda, Bars.Nedragis.MesjacPrirodaListModel>, IMesjacPrirodaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "MesjacPrirodaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public MesjacPrirodaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.MesjacPriroda> Filter(IQueryable<Bars.Nedragis.MesjacPriroda> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.MesjacPrirodaListModel> Map(IQueryable<Bars.Nedragis.MesjacPriroda> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.MesjacPriroda>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.MesjacPrirodaListModel{Id = x.Id, _TypeUid = "706f6c24-2651-4253-b1a9-71d0b627945e", month_pr = (System.String)(x.month_pr), });
        }
    }
}