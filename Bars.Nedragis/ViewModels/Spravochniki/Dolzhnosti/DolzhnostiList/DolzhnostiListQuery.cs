namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Должности'
    /// </summary>
    public interface IDolzhnostiListQuery : IQueryOperation<Bars.Nedragis.Dolzhnosti, Bars.Nedragis.DolzhnostiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Должности'
    /// </summary>
    public interface IDolzhnostiListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Dolzhnosti, Bars.Nedragis.DolzhnostiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Должности'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Должности")]
    public class DolzhnostiListQuery : RmsEntityQueryOperation<Bars.Nedragis.Dolzhnosti, Bars.Nedragis.DolzhnostiListModel>, IDolzhnostiListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "DolzhnostiListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public DolzhnostiListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Dolzhnosti> Filter(IQueryable<Bars.Nedragis.Dolzhnosti> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.DolzhnostiListModel> Map(IQueryable<Bars.Nedragis.Dolzhnosti> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Dolzhnosti>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.DolzhnostiListModel{Id = x.Id, _TypeUid = "c615f281-09eb-4a12-9530-5dcccef6f1ae", code = (System.String)(x.code), name1 = (System.String)(x.name1), });
        }
    }
}