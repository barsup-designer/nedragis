namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Должности'
    /// </summary>
    public interface IDolzhnostiEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Dolzhnosti, DolzhnostiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Должности'
    /// </summary>
    public interface IDolzhnostiEditorValidator : IEditorModelValidator<DolzhnostiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Должности'
    /// </summary>
    public interface IDolzhnostiEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Dolzhnosti, DolzhnostiEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Должности'
    /// </summary>
    public abstract class AbstractDolzhnostiEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Dolzhnosti, DolzhnostiEditorModel>, IDolzhnostiEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Должности'
    /// </summary>
    public class DolzhnostiEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Dolzhnosti, DolzhnostiEditorModel>, IDolzhnostiEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override DolzhnostiEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new DolzhnostiEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Должности' в модель представления
        /// </summary>
        protected override DolzhnostiEditorModel MapEntityInternal(Bars.Nedragis.Dolzhnosti entity)
        {
            // создаем экзепляр модели
            var model = new DolzhnostiEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name1 = (System.String)(entity.name1);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Должности' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Dolzhnosti entity, DolzhnostiEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name1 = model.name1;
        }
    }
}