namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования График платежей'
    /// </summary>
    public interface IGrafikPlatezhejjEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.GrafikPlatezhejj, GrafikPlatezhejjEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования График платежей'
    /// </summary>
    public interface IGrafikPlatezhejjEditorValidator : IEditorModelValidator<GrafikPlatezhejjEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования График платежей'
    /// </summary>
    public interface IGrafikPlatezhejjEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.GrafikPlatezhejj, GrafikPlatezhejjEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования График платежей'
    /// </summary>
    public abstract class AbstractGrafikPlatezhejjEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.GrafikPlatezhejj, GrafikPlatezhejjEditorModel>, IGrafikPlatezhejjEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования График платежей'
    /// </summary>
    public class GrafikPlatezhejjEditorViewModel : BaseEditorViewModel<Bars.Nedragis.GrafikPlatezhejj, GrafikPlatezhejjEditorModel>, IGrafikPlatezhejjEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override GrafikPlatezhejjEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new GrafikPlatezhejjEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'График платежей' в модель представления
        /// </summary>
        protected override GrafikPlatezhejjEditorModel MapEntityInternal(Bars.Nedragis.GrafikPlatezhejj entity)
        {
            // создаем экзепляр модели
            var model = new GrafikPlatezhejjEditorModel();
            model.Id = entity.Id;
            model.code1 = (System.String)(entity.code1);
            model.name1 = (System.String)(entity.name1);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'График платежей' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.GrafikPlatezhejj entity, GrafikPlatezhejjEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code1 = model.code1;
            entity.name1 = model.name1;
        }
    }
}