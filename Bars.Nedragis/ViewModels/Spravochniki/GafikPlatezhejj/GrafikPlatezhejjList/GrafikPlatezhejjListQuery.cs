namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр График платежей'
    /// </summary>
    public interface IGrafikPlatezhejjListQuery : IQueryOperation<Bars.Nedragis.GrafikPlatezhejj, Bars.Nedragis.GrafikPlatezhejjListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр График платежей'
    /// </summary>
    public interface IGrafikPlatezhejjListQueryFilter : IQueryOperationFilter<Bars.Nedragis.GrafikPlatezhejj, Bars.Nedragis.GrafikPlatezhejjListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр График платежей'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр График платежей")]
    public class GrafikPlatezhejjListQuery : RmsEntityQueryOperation<Bars.Nedragis.GrafikPlatezhejj, Bars.Nedragis.GrafikPlatezhejjListModel>, IGrafikPlatezhejjListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "GrafikPlatezhejjListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public GrafikPlatezhejjListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.GrafikPlatezhejj> Filter(IQueryable<Bars.Nedragis.GrafikPlatezhejj> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.GrafikPlatezhejjListModel> Map(IQueryable<Bars.Nedragis.GrafikPlatezhejj> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.GrafikPlatezhejj>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.GrafikPlatezhejjListModel{Id = x.Id, _TypeUid = "533b04e9-5bdd-49b7-bec8-07e684d2ecc2", code1 = (System.String)(x.code1), name1 = (System.String)(x.name1), });
        }
    }
}