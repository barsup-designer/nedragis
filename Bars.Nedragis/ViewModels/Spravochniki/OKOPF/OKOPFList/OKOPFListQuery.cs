namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр ОКОПФ'
    /// </summary>
    public interface IOKOPFListQuery : IQueryOperation<Bars.Nedragis.OKOPF, Bars.Nedragis.OKOPFListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр ОКОПФ'
    /// </summary>
    public interface IOKOPFListQueryFilter : IQueryOperationFilter<Bars.Nedragis.OKOPF, Bars.Nedragis.OKOPFListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр ОКОПФ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр ОКОПФ")]
    public class OKOPFListQuery : RmsEntityQueryOperation<Bars.Nedragis.OKOPF, Bars.Nedragis.OKOPFListModel>, IOKOPFListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OKOPFListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OKOPFListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKOPF> Filter(IQueryable<Bars.Nedragis.OKOPF> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OKOPFListModel> Map(IQueryable<Bars.Nedragis.OKOPF> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OKOPF>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OKOPFListModel{Id = x.Id, _TypeUid = "1f07d59f-1ccc-4e40-8cca-0cf84d26e430", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}