namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования ОКОПФ'
    /// </summary>
    public interface IOKOPFEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.OKOPF, OKOPFEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования ОКОПФ'
    /// </summary>
    public interface IOKOPFEditorValidator : IEditorModelValidator<OKOPFEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования ОКОПФ'
    /// </summary>
    public interface IOKOPFEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.OKOPF, OKOPFEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования ОКОПФ'
    /// </summary>
    public abstract class AbstractOKOPFEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.OKOPF, OKOPFEditorModel>, IOKOPFEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования ОКОПФ'
    /// </summary>
    public class OKOPFEditorViewModel : BaseEditorViewModel<Bars.Nedragis.OKOPF, OKOPFEditorModel>, IOKOPFEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OKOPFEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OKOPFEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'ОКОПФ' в модель представления
        /// </summary>
        protected override OKOPFEditorModel MapEntityInternal(Bars.Nedragis.OKOPF entity)
        {
            // создаем экзепляр модели
            var model = new OKOPFEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'ОКОПФ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.OKOPF entity, OKOPFEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}