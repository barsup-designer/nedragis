namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Вид права'
    /// </summary>
    public interface IVidPravaListQuery : IQueryOperation<Bars.Nedragis.VidPrava, Bars.Nedragis.VidPravaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Вид права'
    /// </summary>
    public interface IVidPravaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.VidPrava, Bars.Nedragis.VidPravaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Вид права'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Вид права")]
    public class VidPravaListQuery : RmsEntityQueryOperation<Bars.Nedragis.VidPrava, Bars.Nedragis.VidPravaListModel>, IVidPravaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "VidPravaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public VidPravaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.VidPrava> Filter(IQueryable<Bars.Nedragis.VidPrava> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.VidPravaListModel> Map(IQueryable<Bars.Nedragis.VidPrava> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.VidPrava>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.VidPravaListModel{Id = x.Id, _TypeUid = "d9f0752c-b27c-4f7f-8667-e2d8dbbe8970", code = (System.String)(x.code), name1 = (System.String)(x.name1), });
        }
    }
}