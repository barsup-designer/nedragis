namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Вид права'
    /// </summary>
    public interface IVidPravaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.VidPrava, VidPravaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Вид права'
    /// </summary>
    public interface IVidPravaEditorValidator : IEditorModelValidator<VidPravaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Вид права'
    /// </summary>
    public interface IVidPravaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.VidPrava, VidPravaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Вид права'
    /// </summary>
    public abstract class AbstractVidPravaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.VidPrava, VidPravaEditorModel>, IVidPravaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Вид права'
    /// </summary>
    public class VidPravaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.VidPrava, VidPravaEditorModel>, IVidPravaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override VidPravaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new VidPravaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Вид права' в модель представления
        /// </summary>
        protected override VidPravaEditorModel MapEntityInternal(Bars.Nedragis.VidPrava entity)
        {
            // создаем экзепляр модели
            var model = new VidPravaEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name1 = (System.String)(entity.name1);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Вид права' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.VidPrava entity, VidPravaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name1 = model.name1;
        }
    }
}