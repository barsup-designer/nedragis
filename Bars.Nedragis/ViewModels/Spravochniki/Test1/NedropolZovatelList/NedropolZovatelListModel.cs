namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Справочник Недропользователь'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник Недропользователь")]
    public class NedropolZovatelListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: name)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("85cba6c3-afdc-4fcf-9932-548eddac365b")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Адрес' (псевдоним: adres)
        /// </summary>
        [Bars.B4.Utils.Display("Адрес")]
        [Bars.Rms.Core.Attributes.Uid("7510541d-7fb4-4db0-8534-0dab3cdf68fb")]
        public virtual System.String adres
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Телефон' (псевдоним: tel)
        /// </summary>
        [Bars.B4.Utils.Display("Телефон")]
        [Bars.Rms.Core.Attributes.Uid("dcce271a-1f08-4bd0-86e1-92ac9cdf8c90")]
        public virtual System.String tel
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'e-mail' (псевдоним: mail)
        /// </summary>
        [Bars.B4.Utils.Display("e-mail")]
        [Bars.Rms.Core.Attributes.Uid("48b6e82c-3a22-47a8-9e90-fadeadb9e21b")]
        public virtual System.String mail
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Факс' (псевдоним: fax)
        /// </summary>
        [Bars.B4.Utils.Display("Факс")]
        [Bars.Rms.Core.Attributes.Uid("205ef239-dedc-43f4-a190-fdac03a304c3")]
        public virtual System.String fax
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Руководитель' (псевдоним: member)
        /// </summary>
        [Bars.B4.Utils.Display("Руководитель")]
        [Bars.Rms.Core.Attributes.Uid("edc4db82-31f5-480c-913c-5c931268d8ee")]
        public virtual System.String member
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'ИНН' (псевдоним: inn)
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("ef3227c2-f0f9-4e52-a292-ec5d473b75f3")]
        public virtual System.String inn
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'КПП' (псевдоним: kpp)
        /// </summary>
        [Bars.B4.Utils.Display("КПП")]
        [Bars.Rms.Core.Attributes.Uid("89d8f4d1-f7c9-4753-a215-a336c43ed415")]
        public virtual System.String kpp
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Расчетный счет' (псевдоним: rs)
        /// </summary>
        [Bars.B4.Utils.Display("Расчетный счет")]
        [Bars.Rms.Core.Attributes.Uid("28abd6df-4953-4adf-a7c6-13262e53b31c")]
        public virtual System.String rs
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'БИК' (псевдоним: bik)
        /// </summary>
        [Bars.B4.Utils.Display("БИК")]
        [Bars.Rms.Core.Attributes.Uid("220d7196-c44c-47f5-9610-fa873ce16993")]
        public virtual System.String bik
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Корресп. счет' (псевдоним: kors)
        /// </summary>
        [Bars.B4.Utils.Display("Корресп. счет")]
        [Bars.Rms.Core.Attributes.Uid("16adfc29-c297-4ff7-8072-c03b1d3884cb")]
        public virtual System.String kors
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}