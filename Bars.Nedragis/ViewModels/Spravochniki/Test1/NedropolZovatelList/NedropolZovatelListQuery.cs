namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Справочник Недропользователь'
    /// </summary>
    public interface INedropolZovatelListQuery : IQueryOperation<Bars.Nedragis.NedropolZovatel, Bars.Nedragis.NedropolZovatelListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Справочник Недропользователь'
    /// </summary>
    public interface INedropolZovatelListQueryFilter : IQueryOperationFilter<Bars.Nedragis.NedropolZovatel, Bars.Nedragis.NedropolZovatelListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Справочник Недропользователь'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник Недропользователь")]
    public class NedropolZovatelListQuery : RmsEntityQueryOperation<Bars.Nedragis.NedropolZovatel, Bars.Nedragis.NedropolZovatelListModel>, INedropolZovatelListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "NedropolZovatelListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public NedropolZovatelListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.NedropolZovatel> Filter(IQueryable<Bars.Nedragis.NedropolZovatel> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.NedropolZovatelListModel> Map(IQueryable<Bars.Nedragis.NedropolZovatel> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.NedropolZovatel>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.NedropolZovatelListModel{Id = x.Id, _TypeUid = "0c62eb0a-8dc6-455a-a0d1-0ad2485a3edb", name = (System.String)(x.name), adres = (System.String)(x.adres), tel = (System.String)(x.tel), mail = (System.String)(x.mail), fax = (System.String)(x.fax), member = (System.String)(x.member), inn = (System.String)(x.inn), kpp = (System.String)(x.kpp), rs = (System.String)(x.rs), bik = (System.String)(x.bik), kors = (System.String)(x.kors), });
        }
    }
}