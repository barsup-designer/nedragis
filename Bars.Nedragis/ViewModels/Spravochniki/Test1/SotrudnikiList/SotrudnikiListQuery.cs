namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Справочник Сотрудники'
    /// </summary>
    public interface ISotrudnikiListQuery : IQueryOperation<Bars.Nedragis.Sotrudniki, Bars.Nedragis.SotrudnikiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Справочник Сотрудники'
    /// </summary>
    public interface ISotrudnikiListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Sotrudniki, Bars.Nedragis.SotrudnikiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Справочник Сотрудники'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник Сотрудники")]
    public class SotrudnikiListQuery : RmsEntityQueryOperation<Bars.Nedragis.Sotrudniki, Bars.Nedragis.SotrudnikiListModel>, ISotrudnikiListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "SotrudnikiListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public SotrudnikiListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Sotrudniki> Filter(IQueryable<Bars.Nedragis.Sotrudniki> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.SotrudnikiListModel> Map(IQueryable<Bars.Nedragis.Sotrudniki> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Sotrudniki>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.SotrudnikiListModel{Id = x.Id, _TypeUid = "7fec6da9-d7ef-4a54-b1c1-53d17443322d", fio = (System.String)(x.fio), dolzh_name = (System.String)(x.dolzh.name), dat = (System.DateTime? )(x.dat), });
        }
    }
}