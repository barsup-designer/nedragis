namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Справочник Лицензионные участки'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник Лицензионные участки")]
    public class LicUchastkiListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: name)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("e58ae88e-d2be-4b32-9fe6-6e76e942a17a")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер лицензии' (псевдоним: Element1478588881912)
        /// </summary>
        [Bars.B4.Utils.Display("Номер лицензии")]
        [Bars.Rms.Core.Attributes.Uid("66ce7c24-98ec-4fb3-97cf-4791c1e9c0d2")]
        public virtual System.String Element1478588881912
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Описание расположение участка' (псевдоним: Element1478589271306)
        /// </summary>
        [Bars.B4.Utils.Display("Описание расположение участка")]
        [Bars.Rms.Core.Attributes.Uid("d9442013-6af5-43fc-8cd8-f0a0248519ea")]
        public virtual System.String Element1478589271306
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}