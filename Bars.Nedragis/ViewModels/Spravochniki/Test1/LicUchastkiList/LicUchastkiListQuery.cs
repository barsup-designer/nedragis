namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Справочник Лицензионные участки'
    /// </summary>
    public interface ILicUchastkiListQuery : IQueryOperation<Bars.Nedragis.LicUchastki, Bars.Nedragis.LicUchastkiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Справочник Лицензионные участки'
    /// </summary>
    public interface ILicUchastkiListQueryFilter : IQueryOperationFilter<Bars.Nedragis.LicUchastki, Bars.Nedragis.LicUchastkiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Справочник Лицензионные участки'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник Лицензионные участки")]
    public class LicUchastkiListQuery : RmsEntityQueryOperation<Bars.Nedragis.LicUchastki, Bars.Nedragis.LicUchastkiListModel>, ILicUchastkiListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "LicUchastkiListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public LicUchastkiListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.LicUchastki> Filter(IQueryable<Bars.Nedragis.LicUchastki> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.LicUchastkiListModel> Map(IQueryable<Bars.Nedragis.LicUchastki> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.LicUchastki>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.LicUchastkiListModel{Id = x.Id, _TypeUid = "2675cadb-dc1f-436f-8d36-cd55fc784aaf", name = (System.String)(x.name), Element1478588881912 = (System.String)(x.Element1478588881912), Element1478589271306 = (System.String)(x.Element1478589271306), });
        }
    }
}