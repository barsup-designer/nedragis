namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Справочник Платежное поручение'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник Платежное поручение")]
    public class PlatezhPoruchenieListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата' (псевдоним: dattr)
        /// </summary>
        [Bars.B4.Utils.Display("Дата")]
        [Bars.Rms.Core.Attributes.Uid("4c962909-f92e-438d-bc72-8d30d95bb067")]
        public virtual System.DateTime? dattr
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Сумма' (псевдоним: suma)
        /// </summary>
        [Bars.B4.Utils.Display("Сумма")]
        [Bars.Rms.Core.Attributes.Uid("9a6cec4a-d0d5-4532-9b06-f5977e3f4b0f")]
        public virtual System.Decimal? suma
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата договора' (псевдоним: dataa)
        /// </summary>
        [Bars.B4.Utils.Display("Дата договора")]
        [Bars.Rms.Core.Attributes.Uid("c736f255-c150-4022-9fc8-d21e421a77c5")]
        public virtual System.DateTime? dataa
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Номер договора' (псевдоним: Element1478594361589)
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("655a3e42-c13a-4da2-8280-32589e752923")]
        public virtual System.Int32? Element1478594361589
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}