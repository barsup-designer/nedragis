namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Справочник Платежное поручение'
    /// </summary>
    public interface IPlatezhPoruchenieListQuery : IQueryOperation<Bars.Nedragis.PlatezhPoruchenie, Bars.Nedragis.PlatezhPoruchenieListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Справочник Платежное поручение'
    /// </summary>
    public interface IPlatezhPoruchenieListQueryFilter : IQueryOperationFilter<Bars.Nedragis.PlatezhPoruchenie, Bars.Nedragis.PlatezhPoruchenieListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Справочник Платежное поручение'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник Платежное поручение")]
    public class PlatezhPoruchenieListQuery : RmsEntityQueryOperation<Bars.Nedragis.PlatezhPoruchenie, Bars.Nedragis.PlatezhPoruchenieListModel>, IPlatezhPoruchenieListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "PlatezhPoruchenieListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public PlatezhPoruchenieListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.PlatezhPoruchenie> Filter(IQueryable<Bars.Nedragis.PlatezhPoruchenie> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.PlatezhPoruchenieListModel> Map(IQueryable<Bars.Nedragis.PlatezhPoruchenie> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.PlatezhPoruchenie>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.PlatezhPoruchenieListModel{Id = x.Id, _TypeUid = "c719553b-685c-415f-a312-0e6fb3bba679", dattr = (System.DateTime? )(x.dattr), suma = (System.Decimal? )(x.suma), dataa = (System.DateTime? )(x.dataa), Element1478594361589 = (System.Int32? )(x.Element1478594361589), });
        }
    }
}