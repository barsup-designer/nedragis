namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Редактирование Файлы'
    /// </summary>
    public interface IFailyEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Faily, FailyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Редактирование Файлы'
    /// </summary>
    public interface IFailyEditorValidator : IEditorModelValidator<FailyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Редактирование Файлы'
    /// </summary>
    public interface IFailyEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Faily, FailyEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Редактирование Файлы'
    /// </summary>
    public abstract class AbstractFailyEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Faily, FailyEditorModel>, IFailyEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Редактирование Файлы'
    /// </summary>
    public class FailyEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Faily, FailyEditorModel>, IFailyEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override FailyEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new FailyEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Файлы' в модель представления
        /// </summary>
        protected override FailyEditorModel MapEntityInternal(Bars.Nedragis.Faily entity)
        {
            // создаем экзепляр модели
            var model = new FailyEditorModel();
            model.Id = entity.Id;
            model.name = (System.String)(entity.name);
            model.obem = (System.Int32? )(entity.obem);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Файлы' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Faily entity, FailyEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name = model.name;
            if (model.obem.HasValue)
            {
                entity.obem = model.obem.Value;
            }
        }
    }
}