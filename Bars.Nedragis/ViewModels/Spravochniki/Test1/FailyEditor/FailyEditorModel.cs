namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Редактирование Файлы' для отдачи на клиент
    /// </summary>
    public class FailyEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public FailyEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("9413e29b-0a4e-45b0-8859-30b5600ffe18")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Объем'
        /// </summary>
        [Bars.B4.Utils.Display("Объем")]
        [Bars.Rms.Core.Attributes.Uid("2c1c4fb3-3379-4ccf-b4ce-2dd2d1b7160a")]
        public virtual System.Int32? obem
        {
            get;
            set;
        }
    }
}