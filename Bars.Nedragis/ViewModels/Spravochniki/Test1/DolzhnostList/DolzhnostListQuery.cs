namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Справочник должность'
    /// </summary>
    public interface IDolzhnostListQuery : IQueryOperation<Bars.Nedragis.Dolzhnost, Bars.Nedragis.DolzhnostListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Справочник должность'
    /// </summary>
    public interface IDolzhnostListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Dolzhnost, Bars.Nedragis.DolzhnostListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Справочник должность'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник должность")]
    public class DolzhnostListQuery : RmsEntityQueryOperation<Bars.Nedragis.Dolzhnost, Bars.Nedragis.DolzhnostListModel>, IDolzhnostListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "DolzhnostListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public DolzhnostListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Dolzhnost> Filter(IQueryable<Bars.Nedragis.Dolzhnost> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.DolzhnostListModel> Map(IQueryable<Bars.Nedragis.Dolzhnost> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Dolzhnost>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.DolzhnostListModel{Id = x.Id, _TypeUid = "c02a3f65-ec3a-44cf-b531-1608eaf5b276", name = (System.String)(x.name), });
        }
    }
}