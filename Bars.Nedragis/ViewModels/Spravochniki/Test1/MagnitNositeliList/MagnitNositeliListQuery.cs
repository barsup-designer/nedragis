namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Справочник Магнитные носители'
    /// </summary>
    public interface IMagnitNositeliListQuery : IQueryOperation<Bars.Nedragis.MagnitNositeli, Bars.Nedragis.MagnitNositeliListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Справочник Магнитные носители'
    /// </summary>
    public interface IMagnitNositeliListQueryFilter : IQueryOperationFilter<Bars.Nedragis.MagnitNositeli, Bars.Nedragis.MagnitNositeliListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Справочник Магнитные носители'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник Магнитные носители")]
    public class MagnitNositeliListQuery : RmsEntityQueryOperation<Bars.Nedragis.MagnitNositeli, Bars.Nedragis.MagnitNositeliListModel>, IMagnitNositeliListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "MagnitNositeliListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public MagnitNositeliListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.MagnitNositeli> Filter(IQueryable<Bars.Nedragis.MagnitNositeli> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.MagnitNositeliListModel> Map(IQueryable<Bars.Nedragis.MagnitNositeli> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.MagnitNositeli>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.MagnitNositeliListModel{Id = x.Id, _TypeUid = "3e627ca5-3cf8-4bec-8d07-a5998d6b28df", name = (System.String)(x.name), barcod = (System.String)(x.barcod), });
        }
    }
}