namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Редактирование справочника Недропользователь'
    /// </summary>
    public interface INedropolZovatelEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.NedropolZovatel, NedropolZovatelEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Редактирование справочника Недропользователь'
    /// </summary>
    public interface INedropolZovatelEditorValidator : IEditorModelValidator<NedropolZovatelEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Редактирование справочника Недропользователь'
    /// </summary>
    public interface INedropolZovatelEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.NedropolZovatel, NedropolZovatelEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Редактирование справочника Недропользователь'
    /// </summary>
    public abstract class AbstractNedropolZovatelEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.NedropolZovatel, NedropolZovatelEditorModel>, INedropolZovatelEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Редактирование справочника Недропользователь'
    /// </summary>
    public class NedropolZovatelEditorViewModel : BaseEditorViewModel<Bars.Nedragis.NedropolZovatel, NedropolZovatelEditorModel>, INedropolZovatelEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override NedropolZovatelEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new NedropolZovatelEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Недропользователь' в модель представления
        /// </summary>
        protected override NedropolZovatelEditorModel MapEntityInternal(Bars.Nedragis.NedropolZovatel entity)
        {
            // создаем экзепляр модели
            var model = new NedropolZovatelEditorModel();
            model.Id = entity.Id;
            model.name = (System.String)(entity.name);
            model.adres = (System.String)(entity.adres);
            model.tel = (System.String)(entity.tel);
            model.mail = (System.String)(entity.mail);
            model.fax = (System.String)(entity.fax);
            model.member = (System.String)(entity.member);
            model.inn = (System.String)(entity.inn);
            model.kpp = (System.String)(entity.kpp);
            model.kors = (System.String)(entity.kors);
            model.rs = (System.String)(entity.rs);
            model.bik = (System.String)(entity.bik);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Недропользователь' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.NedropolZovatel entity, NedropolZovatelEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name = model.name;
            entity.adres = model.adres;
            entity.tel = model.tel;
            entity.mail = model.mail;
            entity.fax = model.fax;
            entity.member = model.member;
            entity.inn = model.inn;
            entity.kpp = model.kpp;
            entity.kors = model.kors;
            entity.rs = model.rs;
            entity.bik = model.bik;
        }
    }
}