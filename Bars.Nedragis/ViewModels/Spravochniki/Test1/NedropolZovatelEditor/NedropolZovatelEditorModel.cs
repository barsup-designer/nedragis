namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Редактирование справочника Недропользователь' для отдачи на клиент
    /// </summary>
    public class NedropolZovatelEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public NedropolZovatelEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("7b0af945-5bb1-4643-b7a3-9bd3467e3f8f")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Адрес'
        /// </summary>
        [Bars.B4.Utils.Display("Адрес")]
        [Bars.Rms.Core.Attributes.Uid("f80f3153-3459-4124-96df-671a06837782")]
        public virtual System.String adres
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Телефон'
        /// </summary>
        [Bars.B4.Utils.Display("Телефон")]
        [Bars.Rms.Core.Attributes.Uid("ff333848-ee84-4735-8905-9a5480f997e0")]
        public virtual System.String tel
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'e-mail'
        /// </summary>
        [Bars.B4.Utils.Display("e-mail")]
        [Bars.Rms.Core.Attributes.Uid("9126b6ca-8dc6-41c8-b12f-eea8306b6248")]
        public virtual System.String mail
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Факс'
        /// </summary>
        [Bars.B4.Utils.Display("Факс")]
        [Bars.Rms.Core.Attributes.Uid("68e3cc2e-2976-40bd-aa20-57de07d39073")]
        public virtual System.String fax
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Руководитель'
        /// </summary>
        [Bars.B4.Utils.Display("Руководитель")]
        [Bars.Rms.Core.Attributes.Uid("6fc03e44-99bb-45d5-b048-4577f43ddd08")]
        public virtual System.String member
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'ИНН'
        /// </summary>
        [Bars.B4.Utils.Display("ИНН")]
        [Bars.Rms.Core.Attributes.Uid("0055a2d0-3718-4e35-a59e-2eb413106f8a")]
        public virtual System.String inn
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'КПП'
        /// </summary>
        [Bars.B4.Utils.Display("КПП")]
        [Bars.Rms.Core.Attributes.Uid("4f11701b-45b7-4b1b-89da-aa8b693dcaec")]
        public virtual System.String kpp
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Корресп. счет'
        /// </summary>
        [Bars.B4.Utils.Display("Корресп. счет")]
        [Bars.Rms.Core.Attributes.Uid("38b9e099-0e55-4b19-94fd-878e59040c79")]
        public virtual System.String kors
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Расчетный счет'
        /// </summary>
        [Bars.B4.Utils.Display("Расчетный счет")]
        [Bars.Rms.Core.Attributes.Uid("2668fd8b-ce2d-4568-bfac-0a37367cc1a5")]
        public virtual System.String rs
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'БИК'
        /// </summary>
        [Bars.B4.Utils.Display("БИК")]
        [Bars.Rms.Core.Attributes.Uid("42060565-5060-4443-bd5d-f0c3fa6349db")]
        public virtual System.String bik
        {
            get;
            set;
        }
    }
}