namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Редактирование Позиции спецификации'
    /// </summary>
    public interface IPoziciiSEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.PoziciiS, PoziciiSEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Редактирование Позиции спецификации'
    /// </summary>
    public interface IPoziciiSEditorValidator : IEditorModelValidator<PoziciiSEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Редактирование Позиции спецификации'
    /// </summary>
    public interface IPoziciiSEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.PoziciiS, PoziciiSEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Редактирование Позиции спецификации'
    /// </summary>
    public abstract class AbstractPoziciiSEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.PoziciiS, PoziciiSEditorModel>, IPoziciiSEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Редактирование Позиции спецификации'
    /// </summary>
    public class PoziciiSEditorViewModel : BaseEditorViewModel<Bars.Nedragis.PoziciiS, PoziciiSEditorModel>, IPoziciiSEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override PoziciiSEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new PoziciiSEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Позиции спецификации' в модель представления
        /// </summary>
        protected override PoziciiSEditorModel MapEntityInternal(Bars.Nedragis.PoziciiS entity)
        {
            // создаем экзепляр модели
            var model = new PoziciiSEditorModel();
            model.Id = entity.Id;
            model.name = (System.String)(entity.name);
            model.s_pod = (System.Decimal? )(entity.s_pod);
            model.s_hran = (System.Decimal? )(entity.s_hran);
            model.naklad = (System.Decimal? )(entity.naklad);
            model.nds = (System.Decimal? )(entity.nds);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Позиции спецификации' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.PoziciiS entity, PoziciiSEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name = model.name;
            if (model.s_pod.HasValue)
            {
                entity.s_pod = model.s_pod.Value;
            }

            if (model.s_hran.HasValue)
            {
                entity.s_hran = model.s_hran.Value;
            }

            if (model.naklad.HasValue)
            {
                entity.naklad = model.naklad.Value;
            }

            if (model.nds.HasValue)
            {
                entity.nds = model.nds.Value;
            }
        }
    }
}