namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Редактирование Позиции спецификации' для отдачи на клиент
    /// </summary>
    public class PoziciiSEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public PoziciiSEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("24c5852e-6b66-42bc-84ea-58654626324f")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Стоимость подготовки'
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость подготовки")]
        [Bars.Rms.Core.Attributes.Uid("5371c486-6d78-4158-8bda-66d23b3f4d86")]
        public virtual System.Decimal? s_pod
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Стоимость хранения'
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость хранения")]
        [Bars.Rms.Core.Attributes.Uid("9bee8bee-9dbc-44a8-8e80-aaa9cf46dcb6")]
        public virtual System.Decimal? s_hran
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Накладные расходы'
        /// </summary>
        [Bars.B4.Utils.Display("Накладные расходы")]
        [Bars.Rms.Core.Attributes.Uid("e336855a-3007-4724-a5c9-d25a90e4276a")]
        public virtual System.Decimal? naklad
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'НДС'
        /// </summary>
        [Bars.B4.Utils.Display("НДС")]
        [Bars.Rms.Core.Attributes.Uid("85f559cb-37e1-4ebb-a35f-6cf9bb9ce56a")]
        public virtual System.Decimal? nds
        {
            get;
            set;
        }
    }
}