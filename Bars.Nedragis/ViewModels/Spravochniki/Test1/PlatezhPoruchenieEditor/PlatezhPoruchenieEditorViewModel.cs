namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Платежное поручение'
    /// </summary>
    public interface IPlatezhPoruchenieEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.PlatezhPoruchenie, PlatezhPoruchenieEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Платежное поручение'
    /// </summary>
    public interface IPlatezhPoruchenieEditorValidator : IEditorModelValidator<PlatezhPoruchenieEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Платежное поручение'
    /// </summary>
    public interface IPlatezhPoruchenieEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.PlatezhPoruchenie, PlatezhPoruchenieEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Платежное поручение'
    /// </summary>
    public abstract class AbstractPlatezhPoruchenieEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.PlatezhPoruchenie, PlatezhPoruchenieEditorModel>, IPlatezhPoruchenieEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Платежное поручение'
    /// </summary>
    public class PlatezhPoruchenieEditorViewModel : BaseEditorViewModel<Bars.Nedragis.PlatezhPoruchenie, PlatezhPoruchenieEditorModel>, IPlatezhPoruchenieEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override PlatezhPoruchenieEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new PlatezhPoruchenieEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Платежное поручение' в модель представления
        /// </summary>
        protected override PlatezhPoruchenieEditorModel MapEntityInternal(Bars.Nedragis.PlatezhPoruchenie entity)
        {
            // создаем экзепляр модели
            var model = new PlatezhPoruchenieEditorModel();
            model.Id = entity.Id;
            model.dattr = (System.DateTime? )(entity.dattr);
            model.suma = (System.Decimal? )(entity.suma);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Платежное поручение' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.PlatezhPoruchenie entity, PlatezhPoruchenieEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.dattr = model.dattr.GetValueOrDefault();
            if (model.suma.HasValue)
            {
                entity.suma = model.suma.Value;
            }
        }
    }
}