namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Платежное поручение' для отдачи на клиент
    /// </summary>
    public class PlatezhPoruchenieEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public PlatezhPoruchenieEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата'
        /// </summary>
        [Bars.B4.Utils.Display("Дата")]
        [Bars.Rms.Core.Attributes.Uid("f4ca64ec-d3a2-4b0d-94fa-97421450a977")]
        public virtual System.DateTime? dattr
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сумма'
        /// </summary>
        [Bars.B4.Utils.Display("Сумма")]
        [Bars.Rms.Core.Attributes.Uid("c7fee6e8-eb1f-45d8-8622-bdbf50fd604f")]
        public virtual System.Decimal? suma
        {
            get;
            set;
        }
    }
}