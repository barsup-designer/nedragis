namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Справочник Файлы'
    /// </summary>
    public interface IFailyListQuery : IQueryOperation<Bars.Nedragis.Faily, Bars.Nedragis.FailyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Справочник Файлы'
    /// </summary>
    public interface IFailyListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Faily, Bars.Nedragis.FailyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Справочник Файлы'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник Файлы")]
    public class FailyListQuery : RmsEntityQueryOperation<Bars.Nedragis.Faily, Bars.Nedragis.FailyListModel>, IFailyListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "FailyListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public FailyListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Faily> Filter(IQueryable<Bars.Nedragis.Faily> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.FailyListModel> Map(IQueryable<Bars.Nedragis.Faily> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Faily>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.FailyListModel{Id = x.Id, _TypeUid = "e2632129-9137-4a44-be60-edee3d85b0eb", name = (System.String)(x.name), obem = (System.Int32? )(x.obem), });
        }
    }
}