namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования должность'
    /// </summary>
    public interface IDolzhnostEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Dolzhnost, DolzhnostEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования должность'
    /// </summary>
    public interface IDolzhnostEditorValidator : IEditorModelValidator<DolzhnostEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования должность'
    /// </summary>
    public interface IDolzhnostEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Dolzhnost, DolzhnostEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования должность'
    /// </summary>
    public abstract class AbstractDolzhnostEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Dolzhnost, DolzhnostEditorModel>, IDolzhnostEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования должность'
    /// </summary>
    public class DolzhnostEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Dolzhnost, DolzhnostEditorModel>, IDolzhnostEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override DolzhnostEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new DolzhnostEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'должность' в модель представления
        /// </summary>
        protected override DolzhnostEditorModel MapEntityInternal(Bars.Nedragis.Dolzhnost entity)
        {
            // создаем экзепляр модели
            var model = new DolzhnostEditorModel();
            model.Id = entity.Id;
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'должность' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Dolzhnost entity, DolzhnostEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name = model.name;
        }
    }
}