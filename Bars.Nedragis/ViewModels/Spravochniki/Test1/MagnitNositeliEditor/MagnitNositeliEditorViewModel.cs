namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Редактирование Магнитные носители'
    /// </summary>
    public interface IMagnitNositeliEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.MagnitNositeli, MagnitNositeliEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Редактирование Магнитные носители'
    /// </summary>
    public interface IMagnitNositeliEditorValidator : IEditorModelValidator<MagnitNositeliEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Редактирование Магнитные носители'
    /// </summary>
    public interface IMagnitNositeliEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.MagnitNositeli, MagnitNositeliEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Редактирование Магнитные носители'
    /// </summary>
    public abstract class AbstractMagnitNositeliEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.MagnitNositeli, MagnitNositeliEditorModel>, IMagnitNositeliEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Редактирование Магнитные носители'
    /// </summary>
    public class MagnitNositeliEditorViewModel : BaseEditorViewModel<Bars.Nedragis.MagnitNositeli, MagnitNositeliEditorModel>, IMagnitNositeliEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override MagnitNositeliEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new MagnitNositeliEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Магнитные носители' в модель представления
        /// </summary>
        protected override MagnitNositeliEditorModel MapEntityInternal(Bars.Nedragis.MagnitNositeli entity)
        {
            // создаем экзепляр модели
            var model = new MagnitNositeliEditorModel();
            model.Id = entity.Id;
            model.name = (System.String)(entity.name);
            model.barcod = (System.String)(entity.barcod);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Магнитные носители' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.MagnitNositeli entity, MagnitNositeliEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name = model.name;
            entity.barcod = model.barcod;
        }
    }
}