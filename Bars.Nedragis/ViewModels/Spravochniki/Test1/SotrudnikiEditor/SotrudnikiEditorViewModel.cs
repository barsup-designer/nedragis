namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Редактирование справочника Сотрудники'
    /// </summary>
    public interface ISotrudnikiEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.Sotrudniki, SotrudnikiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Редактирование справочника Сотрудники'
    /// </summary>
    public interface ISotrudnikiEditorValidator : IEditorModelValidator<SotrudnikiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Редактирование справочника Сотрудники'
    /// </summary>
    public interface ISotrudnikiEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.Sotrudniki, SotrudnikiEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Редактирование справочника Сотрудники'
    /// </summary>
    public abstract class AbstractSotrudnikiEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.Sotrudniki, SotrudnikiEditorModel>, ISotrudnikiEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Редактирование справочника Сотрудники'
    /// </summary>
    public class SotrudnikiEditorViewModel : BaseEditorViewModel<Bars.Nedragis.Sotrudniki, SotrudnikiEditorModel>, ISotrudnikiEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override SotrudnikiEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new SotrudnikiEditorModel();
            var vardolzhId = @params.Params.GetAs<long>("dolzh_Id", 0);
            if (vardolzhId > 0)
            {
                model.dolzh = Container.Resolve<Bars.Nedragis.IDolzhnostListQuery>().GetById(vardolzhId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Сотрудники' в модель представления
        /// </summary>
        protected override SotrudnikiEditorModel MapEntityInternal(Bars.Nedragis.Sotrudniki entity)
        {
            // создаем экзепляр модели
            var model = new SotrudnikiEditorModel();
            model.Id = entity.Id;
            model.fio = (System.String)(entity.fio);
            if (entity.dolzh.IsNotNull())
            {
                var queryDolzhnostList = Container.Resolve<Bars.Nedragis.IDolzhnostListQuery>();
                model.dolzh = queryDolzhnostList.GetById(entity.dolzh.Id);
            }

            model.dat = (System.DateTime? )(entity.dat);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Сотрудники' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Sotrudniki entity, SotrudnikiEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.fio = model.fio;
            entity.dolzh = TryLoadEntityById<Bars.Nedragis.Dolzhnost>(model.dolzh?.Id);
            entity.dat = model.dat.GetValueOrDefault();
        }
    }
}