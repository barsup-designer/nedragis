namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Редактирование справочника Сотрудники' для отдачи на клиент
    /// </summary>
    public class SotrudnikiEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public SotrudnikiEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'ФИО'
        /// </summary>
        [Bars.B4.Utils.Display("ФИО")]
        [Bars.Rms.Core.Attributes.Uid("f1f9b94d-ab38-4fce-9dd5-f8bb3302b983")]
        public virtual System.String fio
        {
            get;
            set;
        }

        /// <summary>
        /// Должность
        /// </summary>
        [Bars.B4.Utils.Display("Должность")]
        [Bars.Rms.Core.Attributes.Uid("04370ea8-e4aa-4946-9e12-9822b3a17e49")]
        public virtual Bars.Nedragis.DolzhnostListModel dolzh
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата'
        /// </summary>
        [Bars.B4.Utils.Display("Дата")]
        [Bars.Rms.Core.Attributes.Uid("c6f441fc-9776-4e05-ae9f-647858cc5a4d")]
        public virtual System.DateTime? dat
        {
            get;
            set;
        }
    }
}