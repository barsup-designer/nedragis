namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Договор и приложения'
    /// </summary>
    public interface IDogovorIPrilozhenijaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.DogovorIPrilozhenija, DogovorIPrilozhenijaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Договор и приложения'
    /// </summary>
    public interface IDogovorIPrilozhenijaEditorValidator : IEditorModelValidator<DogovorIPrilozhenijaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Договор и приложения'
    /// </summary>
    public interface IDogovorIPrilozhenijaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.DogovorIPrilozhenija, DogovorIPrilozhenijaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Договор и приложения'
    /// </summary>
    public abstract class AbstractDogovorIPrilozhenijaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.DogovorIPrilozhenija, DogovorIPrilozhenijaEditorModel>, IDogovorIPrilozhenijaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Договор и приложения'
    /// </summary>
    public class DogovorIPrilozhenijaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.DogovorIPrilozhenija, DogovorIPrilozhenijaEditorModel>, IDogovorIPrilozhenijaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override DogovorIPrilozhenijaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new DogovorIPrilozhenijaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Договор и приложения' в модель представления
        /// </summary>
        protected override DogovorIPrilozhenijaEditorModel MapEntityInternal(Bars.Nedragis.DogovorIPrilozhenija entity)
        {
            // создаем экзепляр модели
            var model = new DogovorIPrilozhenijaEditorModel();
            model.Id = entity.Id;
            model.Element1478683765504 = (System.Int32? )(entity.Element1478683765504);
            model.Element1478683945974 = (System.String)(entity.Element1478683945974);
            model.Element1478683081624 = (System.Int32? )(entity.Element1478683081624);
            model.Element1478683584922 = (System.DateTime? )(entity.Element1478683584922);
            model.Element1478683617170 = (System.String)(entity.Element1478683617170);
            model.Element1478683683665 = (System.String)(entity.Element1478683683665);
            model.Element1478683916351 = (System.String)(entity.Element1478683916351);
            model.Element1478684021941 = (System.String)(entity.Element1478684021941);
            model.Element1478684113244 = (System.DateTime? )(entity.Element1478684113244);
            model.Element1478684143764 = (System.String)(entity.Element1478684143764);
            model.Element1478683832391 = (System.DateTime? )(entity.Element1478683832391);
            model.Element1478683872055 = (System.String)(entity.Element1478683872055);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Договор и приложения' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.DogovorIPrilozhenija entity, DogovorIPrilozhenijaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.Element1478683765504.HasValue)
            {
                entity.Element1478683765504 = model.Element1478683765504.Value;
            }

            entity.Element1478683945974 = model.Element1478683945974;
            if (model.Element1478683081624.HasValue)
            {
                entity.Element1478683081624 = model.Element1478683081624.Value;
            }

            entity.Element1478683584922 = model.Element1478683584922.GetValueOrDefault();
            entity.Element1478683617170 = model.Element1478683617170;
            entity.Element1478683683665 = model.Element1478683683665;
            entity.Element1478683916351 = model.Element1478683916351;
            entity.Element1478684021941 = model.Element1478684021941;
            entity.Element1478684113244 = model.Element1478684113244.GetValueOrDefault();
            entity.Element1478684143764 = model.Element1478684143764;
            entity.Element1478683832391 = model.Element1478683832391.GetValueOrDefault();
            entity.Element1478683872055 = model.Element1478683872055;
        }
    }
}