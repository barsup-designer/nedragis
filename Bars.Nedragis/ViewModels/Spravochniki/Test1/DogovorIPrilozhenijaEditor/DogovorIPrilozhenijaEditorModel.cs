namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Договор и приложения' для отдачи на клиент
    /// </summary>
    public class DogovorIPrilozhenijaEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public DogovorIPrilozhenijaEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер соглашения'
        /// </summary>
        [Bars.B4.Utils.Display("Номер соглашения")]
        [Bars.Rms.Core.Attributes.Uid("a7335e03-76ee-4964-a87a-927a50f14d0f")]
        public virtual System.Int32? Element1478683765504
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'ФИО Руководителя ГКУ '
        /// </summary>
        [Bars.B4.Utils.Display("ФИО Руководителя ГКУ ")]
        [Bars.Rms.Core.Attributes.Uid("3eecb384-0c92-46a5-83e6-d15fee96f2fb")]
        public virtual System.String Element1478683945974
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер договора'
        /// </summary>
        [Bars.B4.Utils.Display("Номер договора")]
        [Bars.Rms.Core.Attributes.Uid("1984e94a-40a3-405a-8c02-549b7b26b378")]
        public virtual System.Int32? Element1478683081624
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата договора'
        /// </summary>
        [Bars.B4.Utils.Display("Дата договора")]
        [Bars.Rms.Core.Attributes.Uid("2e520f55-4b38-4f01-a50c-b46d88a8e3c6")]
        public virtual System.DateTime? Element1478683584922
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер спецификации'
        /// </summary>
        [Bars.B4.Utils.Display("Номер спецификации")]
        [Bars.Rms.Core.Attributes.Uid("5b7f080c-71fd-46b5-a979-d530c17f79e2")]
        public virtual System.String Element1478683617170
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата спецификации'
        /// </summary>
        [Bars.B4.Utils.Display("Дата спецификации")]
        [Bars.Rms.Core.Attributes.Uid("fa8f6d75-080c-4b78-b10e-4db9358758d4")]
        public virtual System.String Element1478683683665
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Сумма на естеств языке'
        /// </summary>
        [Bars.B4.Utils.Display("Сумма на естеств языке")]
        [Bars.Rms.Core.Attributes.Uid("80e17734-ca2e-4c5c-82ce-493fc770c721")]
        public virtual System.String Element1478683916351
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер акта сдачи-приемки'
        /// </summary>
        [Bars.B4.Utils.Display("Номер акта сдачи-приемки")]
        [Bars.Rms.Core.Attributes.Uid("cb26ca10-935a-473a-9712-4ee9e724225d")]
        public virtual System.String Element1478684021941
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата акта сдачи-приемки'
        /// </summary>
        [Bars.B4.Utils.Display("Дата акта сдачи-приемки")]
        [Bars.Rms.Core.Attributes.Uid("d5083b71-a7a7-4fc4-a9ce-acc29ce6359d")]
        public virtual System.DateTime? Element1478684113244
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер спецификации'
        /// </summary>
        [Bars.B4.Utils.Display("Номер спецификации")]
        [Bars.Rms.Core.Attributes.Uid("51884743-7252-4818-b967-54f55cb89d82")]
        public virtual System.String Element1478684143764
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Дата соглашения'
        /// </summary>
        [Bars.B4.Utils.Display("Дата соглашения")]
        [Bars.Rms.Core.Attributes.Uid("cb2f1a78-d99c-4ad2-97cb-0964f6d3114e")]
        public virtual System.DateTime? Element1478683832391
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'НДС на естеств языке'
        /// </summary>
        [Bars.B4.Utils.Display("НДС на естеств языке")]
        [Bars.Rms.Core.Attributes.Uid("860ac778-95a7-4ca7-ba95-0b9851900557")]
        public virtual System.String Element1478683872055
        {
            get;
            set;
        }
    }
}