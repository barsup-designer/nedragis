namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Редактирование Лицензионные участки' для отдачи на клиент
    /// </summary>
    public class LicUchastkiEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public LicUchastkiEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("6c50e5c7-b546-4b2a-b9a0-e648de888252")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Номер лицензии'
        /// </summary>
        [Bars.B4.Utils.Display("Номер лицензии")]
        [Bars.Rms.Core.Attributes.Uid("9ff263a0-5e66-41c0-bcd2-04b44fd1f827")]
        public virtual System.String Element1478588881912
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Описание расположение участка'
        /// </summary>
        [Bars.B4.Utils.Display("Описание расположение участка")]
        [Bars.Rms.Core.Attributes.Uid("68b898cd-db15-4f1a-8a54-10ab6b615f16")]
        public virtual System.String Element1478589271306
        {
            get;
            set;
        }
    }
}