namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Редактирование Лицензионные участки'
    /// </summary>
    public interface ILicUchastkiEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.LicUchastki, LicUchastkiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Редактирование Лицензионные участки'
    /// </summary>
    public interface ILicUchastkiEditorValidator : IEditorModelValidator<LicUchastkiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Редактирование Лицензионные участки'
    /// </summary>
    public interface ILicUchastkiEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.LicUchastki, LicUchastkiEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Редактирование Лицензионные участки'
    /// </summary>
    public abstract class AbstractLicUchastkiEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.LicUchastki, LicUchastkiEditorModel>, ILicUchastkiEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Редактирование Лицензионные участки'
    /// </summary>
    public class LicUchastkiEditorViewModel : BaseEditorViewModel<Bars.Nedragis.LicUchastki, LicUchastkiEditorModel>, ILicUchastkiEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override LicUchastkiEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new LicUchastkiEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Лицензионные участки' в модель представления
        /// </summary>
        protected override LicUchastkiEditorModel MapEntityInternal(Bars.Nedragis.LicUchastki entity)
        {
            // создаем экзепляр модели
            var model = new LicUchastkiEditorModel();
            model.Id = entity.Id;
            model.name = (System.String)(entity.name);
            model.Element1478588881912 = (System.String)(entity.Element1478588881912);
            model.Element1478589271306 = (System.String)(entity.Element1478589271306);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Лицензионные участки' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.LicUchastki entity, LicUchastkiEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.name = model.name;
            entity.Element1478588881912 = model.Element1478588881912;
            entity.Element1478589271306 = model.Element1478589271306;
        }
    }
}