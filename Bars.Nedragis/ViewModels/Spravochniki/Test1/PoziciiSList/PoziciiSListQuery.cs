namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Справочник Позиции спецификации'
    /// </summary>
    public interface IPoziciiSListQuery : IQueryOperation<Bars.Nedragis.PoziciiS, Bars.Nedragis.PoziciiSListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Справочник Позиции спецификации'
    /// </summary>
    public interface IPoziciiSListQueryFilter : IQueryOperationFilter<Bars.Nedragis.PoziciiS, Bars.Nedragis.PoziciiSListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Справочник Позиции спецификации'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник Позиции спецификации")]
    public class PoziciiSListQuery : RmsEntityQueryOperation<Bars.Nedragis.PoziciiS, Bars.Nedragis.PoziciiSListModel>, IPoziciiSListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "PoziciiSListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public PoziciiSListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.PoziciiS> Filter(IQueryable<Bars.Nedragis.PoziciiS> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.PoziciiSListModel> Map(IQueryable<Bars.Nedragis.PoziciiS> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.PoziciiS>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.PoziciiSListModel{Id = x.Id, _TypeUid = "6a80d697-d2cc-4fc4-ae2f-12a4cb52d3c4", name = (System.String)(x.name), s_pod = (System.Decimal? )(x.s_pod), s_hran = (System.Decimal? )(x.s_hran), naklad = (System.Decimal? )(x.naklad), nds = (System.Decimal? )(x.nds), });
        }
    }
}