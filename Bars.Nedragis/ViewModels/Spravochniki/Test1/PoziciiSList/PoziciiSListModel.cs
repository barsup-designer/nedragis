namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Справочник Позиции спецификации'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Справочник Позиции спецификации")]
    public class PoziciiSListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: name)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("00551d88-e094-4182-a47c-21deca1cf88d")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Стоимость подготовки' (псевдоним: s_pod)
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость подготовки")]
        [Bars.Rms.Core.Attributes.Uid("51e152ef-7e27-49b3-b899-1983b2f938e7")]
        public virtual System.Decimal? s_pod
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Стоимость хранения' (псевдоним: s_hran)
        /// </summary>
        [Bars.B4.Utils.Display("Стоимость хранения")]
        [Bars.Rms.Core.Attributes.Uid("fcdb9908-a841-44df-a259-cad6e15c98bc")]
        public virtual System.Decimal? s_hran
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Накладные расходы' (псевдоним: naklad)
        /// </summary>
        [Bars.B4.Utils.Display("Накладные расходы")]
        [Bars.Rms.Core.Attributes.Uid("4b530708-2128-427c-a83e-b95e699d2895")]
        public virtual System.Decimal? naklad
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'НДС' (псевдоним: nds)
        /// </summary>
        [Bars.B4.Utils.Display("НДС")]
        [Bars.Rms.Core.Attributes.Uid("f92b4dec-59fd-4224-92ac-9c34f95bd5df")]
        public virtual System.Decimal? nds
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}