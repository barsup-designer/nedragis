namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Статусы объекта'
    /// </summary>
    public interface IStatusyObEktaListQuery : IQueryOperation<Bars.Nedragis.StatusyObEkta, Bars.Nedragis.StatusyObEktaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Статусы объекта'
    /// </summary>
    public interface IStatusyObEktaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.StatusyObEkta, Bars.Nedragis.StatusyObEktaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Статусы объекта'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Статусы объекта")]
    public class StatusyObEktaListQuery : RmsEntityQueryOperation<Bars.Nedragis.StatusyObEkta, Bars.Nedragis.StatusyObEktaListModel>, IStatusyObEktaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "StatusyObEktaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public StatusyObEktaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.StatusyObEkta> Filter(IQueryable<Bars.Nedragis.StatusyObEkta> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.StatusyObEktaListModel> Map(IQueryable<Bars.Nedragis.StatusyObEkta> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.StatusyObEkta>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.StatusyObEktaListModel{Id = x.Id, _TypeUid = "149b70cd-8d3e-4355-87d7-5562fad24e4e", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}