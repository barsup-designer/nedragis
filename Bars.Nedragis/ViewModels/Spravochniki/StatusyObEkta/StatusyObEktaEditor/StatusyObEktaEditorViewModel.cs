namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Статусы объекта'
    /// </summary>
    public interface IStatusyObEktaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.StatusyObEkta, StatusyObEktaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Статусы объекта'
    /// </summary>
    public interface IStatusyObEktaEditorValidator : IEditorModelValidator<StatusyObEktaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Статусы объекта'
    /// </summary>
    public interface IStatusyObEktaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.StatusyObEkta, StatusyObEktaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Статусы объекта'
    /// </summary>
    public abstract class AbstractStatusyObEktaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.StatusyObEkta, StatusyObEktaEditorModel>, IStatusyObEktaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Статусы объекта'
    /// </summary>
    public class StatusyObEktaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.StatusyObEkta, StatusyObEktaEditorModel>, IStatusyObEktaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override StatusyObEktaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new StatusyObEktaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Статусы объекта' в модель представления
        /// </summary>
        protected override StatusyObEktaEditorModel MapEntityInternal(Bars.Nedragis.StatusyObEkta entity)
        {
            // создаем экзепляр модели
            var model = new StatusyObEktaEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Статусы объекта' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.StatusyObEkta entity, StatusyObEktaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}