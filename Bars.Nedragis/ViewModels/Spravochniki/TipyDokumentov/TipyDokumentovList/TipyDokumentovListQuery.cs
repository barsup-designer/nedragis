namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Типы документов'
    /// </summary>
    public interface ITipyDokumentovListQuery : IQueryOperation<Bars.Nedragis.TipyDokumentov, Bars.Nedragis.TipyDokumentovListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Типы документов'
    /// </summary>
    public interface ITipyDokumentovListQueryFilter : IQueryOperationFilter<Bars.Nedragis.TipyDokumentov, Bars.Nedragis.TipyDokumentovListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Типы документов'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Типы документов")]
    public class TipyDokumentovListQuery : RmsEntityQueryOperation<Bars.Nedragis.TipyDokumentov, Bars.Nedragis.TipyDokumentovListModel>, ITipyDokumentovListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "TipyDokumentovListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public TipyDokumentovListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.TipyDokumentov> Filter(IQueryable<Bars.Nedragis.TipyDokumentov> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.TipyDokumentovListModel> Map(IQueryable<Bars.Nedragis.TipyDokumentov> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.TipyDokumentov>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.TipyDokumentovListModel{Id = x.Id, _TypeUid = "905a597c-579b-4fb5-a6b8-4f88cbdaf279", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}