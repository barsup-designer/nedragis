namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Типы документов' для отдачи на клиент
    /// </summary>
    public class TipyDokumentovEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public TipyDokumentovEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Код'
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.Rms.Core.Attributes.Uid("6152f3c7-b38a-4e5e-a32c-c8a4dbd7ff64")]
        public virtual System.String code
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("4607ea52-599f-4d3c-bca2-4563bb02df1a")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        [Bars.B4.Utils.Display("Родительский элемент")]
        [Bars.Rms.Core.Attributes.Uid("56d4c204-1fcc-4741-9b6f-6628d48313e6")]
        public virtual Bars.Nedragis.TipyDokumentovListModel parent_id
        {
            get;
            set;
        }
    }
}