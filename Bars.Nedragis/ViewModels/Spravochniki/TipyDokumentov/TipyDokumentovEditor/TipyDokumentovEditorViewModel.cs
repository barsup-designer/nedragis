namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Типы документов'
    /// </summary>
    public interface ITipyDokumentovEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.TipyDokumentov, TipyDokumentovEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Типы документов'
    /// </summary>
    public interface ITipyDokumentovEditorValidator : IEditorModelValidator<TipyDokumentovEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Типы документов'
    /// </summary>
    public interface ITipyDokumentovEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.TipyDokumentov, TipyDokumentovEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Типы документов'
    /// </summary>
    public abstract class AbstractTipyDokumentovEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.TipyDokumentov, TipyDokumentovEditorModel>, ITipyDokumentovEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Типы документов'
    /// </summary>
    public class TipyDokumentovEditorViewModel : BaseEditorViewModel<Bars.Nedragis.TipyDokumentov, TipyDokumentovEditorModel>, ITipyDokumentovEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override TipyDokumentovEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new TipyDokumentovEditorModel();
            var varparent_idId = @params.Params.GetAs<long>("parent_id_Id", 0);
            if (varparent_idId > 0)
            {
                model.parent_id = Container.Resolve<Bars.Nedragis.ITipyDokumentovListQuery>().GetById(varparent_idId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Типы документов' в модель представления
        /// </summary>
        protected override TipyDokumentovEditorModel MapEntityInternal(Bars.Nedragis.TipyDokumentov entity)
        {
            // создаем экзепляр модели
            var model = new TipyDokumentovEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            if (entity.parent_id.IsNotNull())
            {
                var queryTipyDokumentovList = Container.Resolve<Bars.Nedragis.ITipyDokumentovListQuery>();
                model.parent_id = queryTipyDokumentovList.GetById(entity.parent_id.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Типы документов' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.TipyDokumentov entity, TipyDokumentovEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
            entity.parent_id = TryLoadEntityById<Bars.Nedragis.TipyDokumentov>(model.parent_id?.Id);
        }
    }
}