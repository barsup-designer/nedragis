namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Наименование МО'
    /// </summary>
    public interface INaimenovanieMOEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.NaimenovanieMO, NaimenovanieMOEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Наименование МО'
    /// </summary>
    public interface INaimenovanieMOEditorValidator : IEditorModelValidator<NaimenovanieMOEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Наименование МО'
    /// </summary>
    public interface INaimenovanieMOEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.NaimenovanieMO, NaimenovanieMOEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Наименование МО'
    /// </summary>
    public abstract class AbstractNaimenovanieMOEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.NaimenovanieMO, NaimenovanieMOEditorModel>, INaimenovanieMOEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Наименование МО'
    /// </summary>
    public class NaimenovanieMOEditorViewModel : BaseEditorViewModel<Bars.Nedragis.NaimenovanieMO, NaimenovanieMOEditorModel>, INaimenovanieMOEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override NaimenovanieMOEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new NaimenovanieMOEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Наименование МО' в модель представления
        /// </summary>
        protected override NaimenovanieMOEditorModel MapEntityInternal(Bars.Nedragis.NaimenovanieMO entity)
        {
            // создаем экзепляр модели
            var model = new NaimenovanieMOEditorModel();
            model.Id = entity.Id;
            model.Element1455707802552 = (System.String)(entity.Element1455707802552);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Наименование МО' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.NaimenovanieMO entity, NaimenovanieMOEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1455707802552 = model.Element1455707802552;
        }
    }
}