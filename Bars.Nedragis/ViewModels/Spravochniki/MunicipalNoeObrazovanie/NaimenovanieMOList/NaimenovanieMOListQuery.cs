namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Наименование МО'
    /// </summary>
    public interface INaimenovanieMOListQuery : IQueryOperation<Bars.Nedragis.NaimenovanieMO, Bars.Nedragis.NaimenovanieMOListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Наименование МО'
    /// </summary>
    public interface INaimenovanieMOListQueryFilter : IQueryOperationFilter<Bars.Nedragis.NaimenovanieMO, Bars.Nedragis.NaimenovanieMOListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Наименование МО'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Наименование МО")]
    public class NaimenovanieMOListQuery : RmsEntityQueryOperation<Bars.Nedragis.NaimenovanieMO, Bars.Nedragis.NaimenovanieMOListModel>, INaimenovanieMOListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "NaimenovanieMOListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public NaimenovanieMOListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.NaimenovanieMO> Filter(IQueryable<Bars.Nedragis.NaimenovanieMO> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.NaimenovanieMOListModel> Map(IQueryable<Bars.Nedragis.NaimenovanieMO> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.NaimenovanieMO>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.NaimenovanieMOListModel{Id = x.Id, _TypeUid = "3195a3ba-40cc-4d49-b2d0-7a9d8153792b", Element1455707802552 = (System.String)(x.Element1455707802552), });
        }
    }
}