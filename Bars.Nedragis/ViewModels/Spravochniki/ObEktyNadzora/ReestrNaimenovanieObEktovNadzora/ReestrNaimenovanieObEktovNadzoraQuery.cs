namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Перечень хозяйствующих субъектов надзора  '
    /// </summary>
    public interface IReestrNaimenovanieObEktovNadzoraQuery : IQueryOperation<Bars.Nedragis.NaimenovanieObEktovNadzora, Bars.Nedragis.ReestrNaimenovanieObEktovNadzoraModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Перечень хозяйствующих субъектов надзора  '
    /// </summary>
    public interface IReestrNaimenovanieObEktovNadzoraQueryFilter : IQueryOperationFilter<Bars.Nedragis.NaimenovanieObEktovNadzora, Bars.Nedragis.ReestrNaimenovanieObEktovNadzoraModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Перечень хозяйствующих субъектов надзора  '
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Перечень хозяйствующих субъектов надзора  ")]
    public class ReestrNaimenovanieObEktovNadzoraQuery : RmsEntityQueryOperation<Bars.Nedragis.NaimenovanieObEktovNadzora, Bars.Nedragis.ReestrNaimenovanieObEktovNadzoraModel>, IReestrNaimenovanieObEktovNadzoraQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "ReestrNaimenovanieObEktovNadzoraQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public ReestrNaimenovanieObEktovNadzoraQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.NaimenovanieObEktovNadzora> Filter(IQueryable<Bars.Nedragis.NaimenovanieObEktovNadzora> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.ReestrNaimenovanieObEktovNadzoraModel> Map(IQueryable<Bars.Nedragis.NaimenovanieObEktovNadzora> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.NaimenovanieObEktovNadzora>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.ReestrNaimenovanieObEktovNadzoraModel{Id = x.Id, _TypeUid = "d41afb95-b52e-4013-b714-2fc08404a9f9", naimennadzor = (System.String)(x.Element1507203167462), });
        }
    }
}