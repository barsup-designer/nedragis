namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования объектов'
    /// </summary>
    public interface IFormaRedaktirovanijaObEktovViewModel : IEntityEditorViewModel<Bars.Nedragis.NaimenovanieObEktovNadzora, FormaRedaktirovanijaObEktovModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования объектов'
    /// </summary>
    public interface IFormaRedaktirovanijaObEktovValidator : IEditorModelValidator<FormaRedaktirovanijaObEktovModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования объектов'
    /// </summary>
    public interface IFormaRedaktirovanijaObEktovHandler : IEntityEditorViewModelHandler<Bars.Nedragis.NaimenovanieObEktovNadzora, FormaRedaktirovanijaObEktovModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования объектов'
    /// </summary>
    public abstract class AbstractFormaRedaktirovanijaObEktovHandler : EntityEditorViewModelHandler<Bars.Nedragis.NaimenovanieObEktovNadzora, FormaRedaktirovanijaObEktovModel>, IFormaRedaktirovanijaObEktovHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования объектов'
    /// </summary>
    public class FormaRedaktirovanijaObEktovViewModel : BaseEditorViewModel<Bars.Nedragis.NaimenovanieObEktovNadzora, FormaRedaktirovanijaObEktovModel>, IFormaRedaktirovanijaObEktovViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override FormaRedaktirovanijaObEktovModel CreateModelInternal(BaseParams @params)
        {
            var model = new FormaRedaktirovanijaObEktovModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Наименование объектов надзора' в модель представления
        /// </summary>
        protected override FormaRedaktirovanijaObEktovModel MapEntityInternal(Bars.Nedragis.NaimenovanieObEktovNadzora entity)
        {
            // создаем экзепляр модели
            var model = new FormaRedaktirovanijaObEktovModel();
            model.Id = entity.Id;
            model.Element1507203167462 = (System.String)(entity.Element1507203167462);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Наименование объектов надзора' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.NaimenovanieObEktovNadzora entity, FormaRedaktirovanijaObEktovModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Element1507203167462 = model.Element1507203167462;
        }
    }
}