namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования ОКТМО'
    /// </summary>
    public interface IOKTMOEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.OKTMO, OKTMOEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования ОКТМО'
    /// </summary>
    public interface IOKTMOEditorValidator : IEditorModelValidator<OKTMOEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования ОКТМО'
    /// </summary>
    public interface IOKTMOEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.OKTMO, OKTMOEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования ОКТМО'
    /// </summary>
    public abstract class AbstractOKTMOEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.OKTMO, OKTMOEditorModel>, IOKTMOEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования ОКТМО'
    /// </summary>
    public class OKTMOEditorViewModel : BaseEditorViewModel<Bars.Nedragis.OKTMO, OKTMOEditorModel>, IOKTMOEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OKTMOEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OKTMOEditorModel();
            var varparent_idId = @params.Params.GetAs<long>("parent_id_Id", 0);
            if (varparent_idId > 0)
            {
                model.parent_id = Container.Resolve<Bars.Nedragis.IOKTMOListQuery>().GetById(varparent_idId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'ОКТМО' в модель представления
        /// </summary>
        protected override OKTMOEditorModel MapEntityInternal(Bars.Nedragis.OKTMO entity)
        {
            // создаем экзепляр модели
            var model = new OKTMOEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            if (entity.parent_id.IsNotNull())
            {
                var queryOKTMOList = Container.Resolve<Bars.Nedragis.IOKTMOListQuery>();
                model.parent_id = queryOKTMOList.GetById(entity.parent_id.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'ОКТМО' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.OKTMO entity, OKTMOEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
            entity.parent_id = TryLoadEntityById<Bars.Nedragis.OKTMO>(model.parent_id?.Id);
        }
    }
}