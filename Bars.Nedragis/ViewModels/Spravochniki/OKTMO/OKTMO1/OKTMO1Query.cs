namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'ОКТМО'
    /// </summary>
    public interface IOKTMO1Query : IQueryOperation<Bars.Nedragis.OKTMO, Bars.Nedragis.OKTMO1Model, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'ОКТМО'
    /// </summary>
    public interface IOKTMO1QueryFilter : IQueryOperationFilter<Bars.Nedragis.OKTMO, Bars.Nedragis.OKTMO1Model, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'ОКТМО'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("ОКТМО")]
    public class OKTMO1Query : RmsTreeQueryOperation<Bars.Nedragis.OKTMO, Bars.Nedragis.OKTMO1Model>, IOKTMO1Query
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OKTMO1Query"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OKTMO1Query(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKTMO> Filter(IQueryable<Bars.Nedragis.OKTMO> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
                var treeNode = @params.Params.GetAs<long ? >("node");
            }

            return query;
        }

        /// <summary>
        /// Фильтрация в зависимости от идентификатора родительского элемента
        /// </summary>
        /// <param name = "query"></param>
        /// <param name = "parentId"></param>
        /// <param name = "params"></param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKTMO> FilterByParent(IQueryable<Bars.Nedragis.OKTMO> query, long ? parentId, BaseParams @params)
        {
            return parentId == null ? query.Where(x => x.parent_id == null) : query.Where(x => x.parent_id != null && x.parent_id.Id == parentId.Value);
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OKTMO1Model> Map(IQueryable<Bars.Nedragis.OKTMO> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OKTMO>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OKTMO1Model{Id = x.Id, _TypeUid = "151d2f6f-fbb5-4684-9f56-92b8ed775121", code = (System.String)(x.code), name = (System.String)(x.name), IsLeaf = !query.Any(c => c.parent_id.Id == x.Id), });
        }
    }
}