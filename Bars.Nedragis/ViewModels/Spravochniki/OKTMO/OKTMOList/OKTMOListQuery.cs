namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр ОКТМО'
    /// </summary>
    public interface IOKTMOListQuery : IQueryOperation<Bars.Nedragis.OKTMO, Bars.Nedragis.OKTMOListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр ОКТМО'
    /// </summary>
    public interface IOKTMOListQueryFilter : IQueryOperationFilter<Bars.Nedragis.OKTMO, Bars.Nedragis.OKTMOListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр ОКТМО'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр ОКТМО")]
    public class OKTMOListQuery : RmsEntityQueryOperation<Bars.Nedragis.OKTMO, Bars.Nedragis.OKTMOListModel>, IOKTMOListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OKTMOListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OKTMOListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKTMO> Filter(IQueryable<Bars.Nedragis.OKTMO> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OKTMOListModel> Map(IQueryable<Bars.Nedragis.OKTMO> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OKTMO>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OKTMOListModel{Id = x.Id, _TypeUid = "151d2f6f-fbb5-4684-9f56-92b8ed775121", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}