namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Подвид работ природа'
    /// </summary>
    public interface IPodvidRabotPrirodaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.PodvidRabotPriroda, PodvidRabotPrirodaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Подвид работ природа'
    /// </summary>
    public interface IPodvidRabotPrirodaEditorValidator : IEditorModelValidator<PodvidRabotPrirodaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Подвид работ природа'
    /// </summary>
    public interface IPodvidRabotPrirodaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.PodvidRabotPriroda, PodvidRabotPrirodaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Подвид работ природа'
    /// </summary>
    public abstract class AbstractPodvidRabotPrirodaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.PodvidRabotPriroda, PodvidRabotPrirodaEditorModel>, IPodvidRabotPrirodaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Подвид работ природа'
    /// </summary>
    public class PodvidRabotPrirodaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.PodvidRabotPriroda, PodvidRabotPrirodaEditorModel>, IPodvidRabotPrirodaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override PodvidRabotPrirodaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new PodvidRabotPrirodaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Подвид работ природа' в модель представления
        /// </summary>
        protected override PodvidRabotPrirodaEditorModel MapEntityInternal(Bars.Nedragis.PodvidRabotPriroda entity)
        {
            // создаем экзепляр модели
            var model = new PodvidRabotPrirodaEditorModel();
            model.Id = entity.Id;
            model.Podvid_rab_pr = (System.String)(entity.Podvid_rab_pr);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Подвид работ природа' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.PodvidRabotPriroda entity, PodvidRabotPrirodaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Podvid_rab_pr = model.Podvid_rab_pr;
        }
    }
}