namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Подвид работ природа'
    /// </summary>
    public interface IPodvidRabotPrirodaListQuery : IQueryOperation<Bars.Nedragis.PodvidRabotPriroda, Bars.Nedragis.PodvidRabotPrirodaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Подвид работ природа'
    /// </summary>
    public interface IPodvidRabotPrirodaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.PodvidRabotPriroda, Bars.Nedragis.PodvidRabotPrirodaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Подвид работ природа'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Подвид работ природа")]
    public class PodvidRabotPrirodaListQuery : RmsEntityQueryOperation<Bars.Nedragis.PodvidRabotPriroda, Bars.Nedragis.PodvidRabotPrirodaListModel>, IPodvidRabotPrirodaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "PodvidRabotPrirodaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public PodvidRabotPrirodaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.PodvidRabotPriroda> Filter(IQueryable<Bars.Nedragis.PodvidRabotPriroda> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.PodvidRabotPrirodaListModel> Map(IQueryable<Bars.Nedragis.PodvidRabotPriroda> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.PodvidRabotPriroda>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.PodvidRabotPrirodaListModel{Id = x.Id, _TypeUid = "bbf0e8a9-5ab2-491d-899f-4f2e652309b1", Podvid_rab_pr = (System.String)(x.Podvid_rab_pr), });
        }
    }
}