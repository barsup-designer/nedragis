namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр ОКОГУ'
    /// </summary>
    public interface IOKOGUListQuery : IQueryOperation<Bars.Nedragis.OKOGU, Bars.Nedragis.OKOGUListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр ОКОГУ'
    /// </summary>
    public interface IOKOGUListQueryFilter : IQueryOperationFilter<Bars.Nedragis.OKOGU, Bars.Nedragis.OKOGUListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр ОКОГУ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр ОКОГУ")]
    public class OKOGUListQuery : RmsEntityQueryOperation<Bars.Nedragis.OKOGU, Bars.Nedragis.OKOGUListModel>, IOKOGUListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OKOGUListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OKOGUListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKOGU> Filter(IQueryable<Bars.Nedragis.OKOGU> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OKOGUListModel> Map(IQueryable<Bars.Nedragis.OKOGU> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OKOGU>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OKOGUListModel{Id = x.Id, _TypeUid = "25e226c5-654b-43a6-b9ff-36d48b8e7d0b", code = (System.String)(x.code), name = (System.String)(x.name), parent_id_name = (System.String)(x.parent_id.name), });
        }
    }
}