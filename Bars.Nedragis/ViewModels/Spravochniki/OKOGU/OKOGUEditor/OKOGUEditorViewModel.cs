namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования ОКОГУ'
    /// </summary>
    public interface IOKOGUEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.OKOGU, OKOGUEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования ОКОГУ'
    /// </summary>
    public interface IOKOGUEditorValidator : IEditorModelValidator<OKOGUEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования ОКОГУ'
    /// </summary>
    public interface IOKOGUEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.OKOGU, OKOGUEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования ОКОГУ'
    /// </summary>
    public abstract class AbstractOKOGUEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.OKOGU, OKOGUEditorModel>, IOKOGUEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования ОКОГУ'
    /// </summary>
    public class OKOGUEditorViewModel : BaseEditorViewModel<Bars.Nedragis.OKOGU, OKOGUEditorModel>, IOKOGUEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override OKOGUEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new OKOGUEditorModel();
            var varparent_idId = @params.Params.GetAs<long>("parent_id_Id", 0);
            if (varparent_idId > 0)
            {
                model.parent_id = Container.Resolve<Bars.Nedragis.IOKOGU1Query>().GetById(varparent_idId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'ОКОГУ' в модель представления
        /// </summary>
        protected override OKOGUEditorModel MapEntityInternal(Bars.Nedragis.OKOGU entity)
        {
            // создаем экзепляр модели
            var model = new OKOGUEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            model.name_short = (System.String)(entity.name_short);
            if (entity.parent_id.IsNotNull())
            {
                var queryOKOGU1 = Container.Resolve<Bars.Nedragis.IOKOGU1Query>();
                model.parent_id = queryOKOGU1.GetById(entity.parent_id.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'ОКОГУ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.OKOGU entity, OKOGUEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
            entity.name_short = model.name_short;
            entity.parent_id = TryLoadEntityById<Bars.Nedragis.OKOGU>(model.parent_id?.Id);
        }
    }
}