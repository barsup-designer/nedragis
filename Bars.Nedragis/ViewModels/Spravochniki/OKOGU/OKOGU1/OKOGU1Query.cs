namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'ОКОГУ'
    /// </summary>
    public interface IOKOGU1Query : IQueryOperation<Bars.Nedragis.OKOGU, Bars.Nedragis.OKOGU1Model, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'ОКОГУ'
    /// </summary>
    public interface IOKOGU1QueryFilter : IQueryOperationFilter<Bars.Nedragis.OKOGU, Bars.Nedragis.OKOGU1Model, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'ОКОГУ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("ОКОГУ")]
    public class OKOGU1Query : RmsTreeQueryOperation<Bars.Nedragis.OKOGU, Bars.Nedragis.OKOGU1Model>, IOKOGU1Query
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "OKOGU1Query"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public OKOGU1Query(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKOGU> Filter(IQueryable<Bars.Nedragis.OKOGU> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
                var treeNode = @params.Params.GetAs<long ? >("node");
            }

            return query;
        }

        /// <summary>
        /// Фильтрация в зависимости от идентификатора родительского элемента
        /// </summary>
        /// <param name = "query"></param>
        /// <param name = "parentId"></param>
        /// <param name = "params"></param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.OKOGU> FilterByParent(IQueryable<Bars.Nedragis.OKOGU> query, long ? parentId, BaseParams @params)
        {
            return parentId == null ? query.Where(x => x.parent_id == null) : query.Where(x => x.parent_id != null && x.parent_id.Id == parentId.Value);
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.OKOGU1Model> Map(IQueryable<Bars.Nedragis.OKOGU> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.OKOGU>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.OKOGU1Model{Id = x.Id, _TypeUid = "25e226c5-654b-43a6-b9ff-36d48b8e7d0b", code = (System.String)(x.code), name = (System.String)(x.name), name_short = (System.String)(x.name_short), IsLeaf = !query.Any(c => c.parent_id.Id == x.Id), });
        }
    }
}