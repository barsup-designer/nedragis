namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Вид работ ОПИНФ'
    /// </summary>
    public interface IVidRabotOPINFListQuery : IQueryOperation<Bars.Nedragis.VidRabotOPINF, Bars.Nedragis.VidRabotOPINFListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Вид работ ОПИНФ'
    /// </summary>
    public interface IVidRabotOPINFListQueryFilter : IQueryOperationFilter<Bars.Nedragis.VidRabotOPINF, Bars.Nedragis.VidRabotOPINFListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Вид работ ОПИНФ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Вид работ ОПИНФ")]
    public class VidRabotOPINFListQuery : RmsEntityQueryOperation<Bars.Nedragis.VidRabotOPINF, Bars.Nedragis.VidRabotOPINFListModel>, IVidRabotOPINFListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "VidRabotOPINFListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public VidRabotOPINFListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.VidRabotOPINF> Filter(IQueryable<Bars.Nedragis.VidRabotOPINF> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.VidRabotOPINFListModel> Map(IQueryable<Bars.Nedragis.VidRabotOPINF> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.VidRabotOPINF>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.VidRabotOPINFListModel{Id = x.Id, _TypeUid = "d718884f-b8e6-4ddd-a77f-109da91c10ae", VidRabotOPINF222 = (System.String)(x.VidRabotOPINF222), Element1510134562537 = (System.String)(x.Element1510134562537), });
        }
    }
}