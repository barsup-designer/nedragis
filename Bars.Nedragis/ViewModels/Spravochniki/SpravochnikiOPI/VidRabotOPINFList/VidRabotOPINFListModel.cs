namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Вид работ ОПИНФ'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Вид работ ОПИНФ")]
    public class VidRabotOPINFListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Вид работ' (псевдоним: VidRabotOPINF222)
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("083c1a7c-a2c9-4363-a2a4-ca1e1c363856")]
        public virtual System.String VidRabotOPINF222
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Подвид работ' (псевдоним: Element1510134562537)
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("86c55e25-54f0-4dec-a5b6-3fe90f02f19b")]
        public virtual System.String Element1510134562537
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}