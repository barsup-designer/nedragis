namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Вид работ ОПИНФ' для отдачи на клиент
    /// </summary>
    public class VidRabotOPINFEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public VidRabotOPINFEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Вид работ'
        /// </summary>
        [Bars.B4.Utils.Display("Вид работ")]
        [Bars.Rms.Core.Attributes.Uid("c731c7b3-967d-49b9-a6c3-fd7548d43550")]
        public virtual System.String VidRabotOPINF222
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Подвид работ'
        /// </summary>
        [Bars.B4.Utils.Display("Подвид работ")]
        [Bars.Rms.Core.Attributes.Uid("b3015764-0612-4ec5-b840-378d7561a476")]
        public virtual System.String Element1510134562537
        {
            get;
            set;
        }
    }
}