namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Вид работ ОПИНФ'
    /// </summary>
    public interface IVidRabotOPINFEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.VidRabotOPINF, VidRabotOPINFEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Вид работ ОПИНФ'
    /// </summary>
    public interface IVidRabotOPINFEditorValidator : IEditorModelValidator<VidRabotOPINFEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Вид работ ОПИНФ'
    /// </summary>
    public interface IVidRabotOPINFEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.VidRabotOPINF, VidRabotOPINFEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Вид работ ОПИНФ'
    /// </summary>
    public abstract class AbstractVidRabotOPINFEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.VidRabotOPINF, VidRabotOPINFEditorModel>, IVidRabotOPINFEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Вид работ ОПИНФ'
    /// </summary>
    public class VidRabotOPINFEditorViewModel : BaseEditorViewModel<Bars.Nedragis.VidRabotOPINF, VidRabotOPINFEditorModel>, IVidRabotOPINFEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override VidRabotOPINFEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new VidRabotOPINFEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Вид работ ОПИНФ' в модель представления
        /// </summary>
        protected override VidRabotOPINFEditorModel MapEntityInternal(Bars.Nedragis.VidRabotOPINF entity)
        {
            // создаем экзепляр модели
            var model = new VidRabotOPINFEditorModel();
            model.Id = entity.Id;
            model.VidRabotOPINF222 = (System.String)(entity.VidRabotOPINF222);
            model.Element1510134562537 = (System.String)(entity.Element1510134562537);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Вид работ ОПИНФ' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.VidRabotOPINF entity, VidRabotOPINFEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.VidRabotOPINF222 = model.VidRabotOPINF222;
            entity.Element1510134562537 = model.Element1510134562537;
        }
    }
}