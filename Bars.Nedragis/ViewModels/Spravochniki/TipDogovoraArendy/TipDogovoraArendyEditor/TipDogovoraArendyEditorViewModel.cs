namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Тип договора аренды'
    /// </summary>
    public interface ITipDogovoraArendyEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.TipDogovoraArendy, TipDogovoraArendyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Тип договора аренды'
    /// </summary>
    public interface ITipDogovoraArendyEditorValidator : IEditorModelValidator<TipDogovoraArendyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Тип договора аренды'
    /// </summary>
    public interface ITipDogovoraArendyEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.TipDogovoraArendy, TipDogovoraArendyEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Тип договора аренды'
    /// </summary>
    public abstract class AbstractTipDogovoraArendyEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.TipDogovoraArendy, TipDogovoraArendyEditorModel>, ITipDogovoraArendyEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Тип договора аренды'
    /// </summary>
    public class TipDogovoraArendyEditorViewModel : BaseEditorViewModel<Bars.Nedragis.TipDogovoraArendy, TipDogovoraArendyEditorModel>, ITipDogovoraArendyEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override TipDogovoraArendyEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new TipDogovoraArendyEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Тип договора аренды' в модель представления
        /// </summary>
        protected override TipDogovoraArendyEditorModel MapEntityInternal(Bars.Nedragis.TipDogovoraArendy entity)
        {
            // создаем экзепляр модели
            var model = new TipDogovoraArendyEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Тип договора аренды' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.TipDogovoraArendy entity, TipDogovoraArendyEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}