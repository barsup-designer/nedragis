namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Тип договора аренды'
    /// </summary>
    public interface ITipDogovoraArendyListQuery : IQueryOperation<Bars.Nedragis.TipDogovoraArendy, Bars.Nedragis.TipDogovoraArendyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Тип договора аренды'
    /// </summary>
    public interface ITipDogovoraArendyListQueryFilter : IQueryOperationFilter<Bars.Nedragis.TipDogovoraArendy, Bars.Nedragis.TipDogovoraArendyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Тип договора аренды'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Тип договора аренды")]
    public class TipDogovoraArendyListQuery : RmsEntityQueryOperation<Bars.Nedragis.TipDogovoraArendy, Bars.Nedragis.TipDogovoraArendyListModel>, ITipDogovoraArendyListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "TipDogovoraArendyListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public TipDogovoraArendyListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.TipDogovoraArendy> Filter(IQueryable<Bars.Nedragis.TipDogovoraArendy> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.TipDogovoraArendyListModel> Map(IQueryable<Bars.Nedragis.TipDogovoraArendy> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.TipDogovoraArendy>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.TipDogovoraArendyListModel{Id = x.Id, _TypeUid = "0e17ebf5-8501-4e52-b1d9-2fc926e2bd3e", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}