namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Состояние объекта'
    /// </summary>
    public interface ISostojanieObEktaListQuery : IQueryOperation<Bars.Nedragis.SostojanieObEkta, Bars.Nedragis.SostojanieObEktaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Состояние объекта'
    /// </summary>
    public interface ISostojanieObEktaListQueryFilter : IQueryOperationFilter<Bars.Nedragis.SostojanieObEkta, Bars.Nedragis.SostojanieObEktaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Состояние объекта'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Состояние объекта")]
    public class SostojanieObEktaListQuery : RmsEntityQueryOperation<Bars.Nedragis.SostojanieObEkta, Bars.Nedragis.SostojanieObEktaListModel>, ISostojanieObEktaListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "SostojanieObEktaListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public SostojanieObEktaListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.SostojanieObEkta> Filter(IQueryable<Bars.Nedragis.SostojanieObEkta> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.SostojanieObEktaListModel> Map(IQueryable<Bars.Nedragis.SostojanieObEkta> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.SostojanieObEkta>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.SostojanieObEktaListModel{Id = x.Id, _TypeUid = "f48fa006-5e53-47ff-8494-db17c0232275", code = (System.String)(x.code), name = (System.String)(x.name), });
        }
    }
}