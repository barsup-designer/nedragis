namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Состояние объекта'
    /// </summary>
    public interface ISostojanieObEktaEditorViewModel : IEntityEditorViewModel<Bars.Nedragis.SostojanieObEkta, SostojanieObEktaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Состояние объекта'
    /// </summary>
    public interface ISostojanieObEktaEditorValidator : IEditorModelValidator<SostojanieObEktaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Состояние объекта'
    /// </summary>
    public interface ISostojanieObEktaEditorHandler : IEntityEditorViewModelHandler<Bars.Nedragis.SostojanieObEkta, SostojanieObEktaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Состояние объекта'
    /// </summary>
    public abstract class AbstractSostojanieObEktaEditorHandler : EntityEditorViewModelHandler<Bars.Nedragis.SostojanieObEkta, SostojanieObEktaEditorModel>, ISostojanieObEktaEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Состояние объекта'
    /// </summary>
    public class SostojanieObEktaEditorViewModel : BaseEditorViewModel<Bars.Nedragis.SostojanieObEkta, SostojanieObEktaEditorModel>, ISostojanieObEktaEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override SostojanieObEktaEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new SostojanieObEktaEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Состояние объекта' в модель представления
        /// </summary>
        protected override SostojanieObEktaEditorModel MapEntityInternal(Bars.Nedragis.SostojanieObEkta entity)
        {
            // создаем экзепляр модели
            var model = new SostojanieObEktaEditorModel();
            model.Id = entity.Id;
            model.code = (System.String)(entity.code);
            model.name = (System.String)(entity.name);
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Состояние объекта' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.SostojanieObEkta entity, SostojanieObEktaEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.code = model.code;
            entity.name = model.name;
        }
    }
}