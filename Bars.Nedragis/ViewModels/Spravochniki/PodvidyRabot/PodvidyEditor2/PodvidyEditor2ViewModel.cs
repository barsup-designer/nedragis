namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Подвиды2'
    /// </summary>
    public interface IPodvidyEditor2ViewModel : IEntityEditorViewModel<Bars.Nedragis.Podvidy, PodvidyEditor2Model>
    {
    }

    /// <summary>
    /// Интерфейс валидатора модели редактора 'Форма редактирования Подвиды2'
    /// </summary>
    public interface IPodvidyEditor2Validator : IEditorModelValidator<PodvidyEditor2Model>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Подвиды2'
    /// </summary>
    public interface IPodvidyEditor2Handler : IEntityEditorViewModelHandler<Bars.Nedragis.Podvidy, PodvidyEditor2Model>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Подвиды2'
    /// </summary>
    public abstract class AbstractPodvidyEditor2Handler : EntityEditorViewModelHandler<Bars.Nedragis.Podvidy, PodvidyEditor2Model>, IPodvidyEditor2Handler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Подвиды2'
    /// </summary>
    public class PodvidyEditor2ViewModel : BaseEditorViewModel<Bars.Nedragis.Podvidy, PodvidyEditor2Model>, IPodvidyEditor2ViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override PodvidyEditor2Model CreateModelInternal(BaseParams @params)
        {
            var model = new PodvidyEditor2Model();
            var varElement1474349306046Id = @params.Params.GetAs<long>("Element1474349306046_Id", 0);
            if (varElement1474349306046Id > 0)
            {
                model.Element1474349306046 = Container.Resolve<Bars.Nedragis.IVidyRabotListQuery>().GetById(varElement1474349306046Id);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Подвиды' в модель представления
        /// </summary>
        protected override PodvidyEditor2Model MapEntityInternal(Bars.Nedragis.Podvidy entity)
        {
            // создаем экзепляр модели
            var model = new PodvidyEditor2Model();
            model.Id = entity.Id;
            model.Element1474267693056 = (System.String)(entity.Element1474267693056);
            if (entity.Element1474349306046.IsNotNull())
            {
                var queryVidyRabotList = Container.Resolve<Bars.Nedragis.IVidyRabotListQuery>();
                model.Element1474349306046 = queryVidyRabotList.GetById(entity.Element1474349306046.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Подвиды' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.Nedragis.Podvidy entity, PodvidyEditor2Model model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.Id.HasValue)
            {
                entity.Id = model.Id.Value;
            }

            entity.Element1474267693056 = model.Element1474267693056;
            entity.Element1474349306046 = TryLoadEntityById<Bars.Nedragis.VidyRabot>(model.Element1474349306046?.Id);
        }
    }
}