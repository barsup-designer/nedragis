namespace Bars.Nedragis
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Подвиды2' для отдачи на клиент
    /// </summary>
    public class PodvidyEditor2Model : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public PodvidyEditor2Model()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("b7f2f2e8-b740-40c3-a7ea-4f706f3a080c")]
        public virtual System.String Element1474267693056
        {
            get;
            set;
        }

        /// <summary>
        /// вид
        /// </summary>
        [Bars.B4.Utils.Display("вид")]
        [Bars.Rms.Core.Attributes.Uid("fc4d3472-7d6d-4a6c-9057-d4b32b443ad0")]
        public virtual Bars.Nedragis.VidyRabotListModel Element1474349306046
        {
            get;
            set;
        }
    }
}