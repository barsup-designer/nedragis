namespace Bars.Nedragis
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Подвиды'
    /// </summary>
    public interface IPodvidyListQuery : IQueryOperation<Bars.Nedragis.Podvidy, Bars.Nedragis.PodvidyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Подвиды'
    /// </summary>
    public interface IPodvidyListQueryFilter : IQueryOperationFilter<Bars.Nedragis.Podvidy, Bars.Nedragis.PodvidyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Подвиды'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Подвиды")]
    public class PodvidyListQuery : RmsEntityQueryOperation<Bars.Nedragis.Podvidy, Bars.Nedragis.PodvidyListModel>, IPodvidyListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "PodvidyListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public PodvidyListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Nedragis.Podvidy> Filter(IQueryable<Bars.Nedragis.Podvidy> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Nedragis.PodvidyListModel> Map(IQueryable<Bars.Nedragis.Podvidy> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.Nedragis.Podvidy>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Nedragis.PodvidyListModel{Id = x.Id, _TypeUid = "71b344e1-9560-43aa-90d5-b5ea06bf434e", Element1474267693056 = (System.String)(x.Element1474267693056), Element1474349306046_Element1474020299386 = (System.String)(x.Element1474349306046.Element1474020299386), });
        }
    }
}