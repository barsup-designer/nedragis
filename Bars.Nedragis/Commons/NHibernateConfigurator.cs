namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.Expressions;
    using Bars.B4.Modules.PostgreSql.DataAccess.Npgsql;
    using Bars.B4.Modules.PostgreSql.DataAccess;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Hql.Ast;
    using NHibernate.Linq.Functions;
    using NHibernate.Linq.Visitors;
    using NHibernate.Linq;
    using System.Collections.ObjectModel;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Reflection;
    using System;
    using NHibernate.Cfg;

    /// <summary>
    /// Конфигуратор NHibernate
    /// </summary>
    internal class NHibernateConfigurator : INhibernateConfigModifier
    {
        public void Apply(Configuration configuration)
        {
            RmsPostgreSqlDialect.RegisterSqlFunction<System.Int32>("NumberTest");
            ExpressionParser.GlobalConfigurators.Add(x => x.Using(typeof (SqlFunctions)));
            HqlGeneratorsRegistry.RegisterGenerator(new SqlFunctionNumberTestHqlGenerator());
        }
    }

    /// <summary>
    /// Статические методы для вызова sql-функций в linq-запросах
    /// </summary>
    public static class SqlFunctions
    {
        /// <summary>
        /// NumberTest
        /// </summary>
        [Bars.Rms.Core.Attributes.Uid("01e2ee7f-8f53-4074-a982-aa428b9948a1")]
        [Bars.B4.Utils.Display("NumberTest")]
        public static System.Int32 NumberTest([Bars.Rms.Core.Attributes.Uid("01e2ee7f-8f53-4074-a982-aa428b9948a1/Arg-1")][Bars.B4.Utils.Display("ARG_1")] System.Int32 argument1)
        {
            throw new NotImplementedException("Метод должен использоваться в linq-запросе");
        }
    }

    /// <summary>
    /// Статические методы расширения сущностей для вызова sql-функций в linq-запросах
    /// </summary>
    public static class SqlFunctionsEntitiesExtensions
    {
    }

    internal class SqlFunctionNumberTestHqlGenerator : BaseHqlGeneratorForMethod
    {
        public SqlFunctionNumberTestHqlGenerator()
        {
            SupportedMethods = new[]{ReflectionHelper.GetMethodDefinition(() => SqlFunctions.NumberTest(default (System.Int32)))};
        }

        public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
        {
            var argExpressions = arguments.Select(x => visitor.Visit(x).AsExpression()).ToArray();
            var expression = treeBuilder.MethodCall("NumberTest", argExpressions);
            var coalesce = treeBuilder.Coalesce(expression, treeBuilder.Constant(default (System.Int32)));
            return coalesce;
        }
    }
}