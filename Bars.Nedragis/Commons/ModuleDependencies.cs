namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4;
    using System.Linq.Expressions;
    using System.Linq;
    using Castle.Windsor;
    using System;

    public class ModuleDependencies : BaseModuleDependencies
    {
        public ModuleDependencies(IWindsorContainer container): base (container)
        {
            RegisterOrganizaciiDependencies();
            RegisterRegGosNadzorZaGeoDependencies();
            RegisterOKOGUDependencies();
            RegisterOrganyVlastiPrirodaDependencies();
            RegisterTipZemelNogoUchastkaDependencies();
            RegisterSotrudnikiDependencies();
            RegisterOKTMODependencies();
            RegisterOKOFDependencies();
            RegisterOKOPFDependencies();
            RegisterDokumentDependencies();
            RegisterStatusySubEktaDependencies();
            RegisterZima1Dependencies();
            RegisterPodvidRabotPrirodaDependencies();
            RegisterJuridicheskieLicaDependencies();
            RegisterKategoriiZemelDependencies();
            RegisterTipyDokumentovDependencies();
            RegisterOKFSDependencies();
            RegisterReestryDependencies();
            RegisterKlassifikacijaOKOFDependencies();
            RegisterDolzhnostiDependencies();
            RegisterDolzhnostDependencies();
            RegisterOKVEhDDependencies();
            RegisterNedropolZovatelDependencies();
            RegisterVidRabotPrirDependencies();
            RegisterOtdel1Dependencies();
            RegisterStatusyObEktaDependencies();
            RegisterVidRabotOPINFDependencies();
            RegisterSotrudniki1Dependencies();
            RegisterLicUchastkiDependencies();
            RegisterTipySubEktaPravaDependencies();
            RegisterRajjonyJaNAODependencies();
            RegisterVidPravaNaZemljuDependencies();
            RegisterNaimenovanieObEktovNadzoraDependencies();
            RegisterNaimenovanieMODependencies();
            RegisterNaimenovanieNapravlenijaRaskhodovanijaSredstvDependencies();
            RegisterVidOgranichenijaDependencies();
            RegisterOKATODependencies();
            RegisterZemelNyemUchastkiDependencies();
            RegisterVidyRabotDependencies();
            RegisterReestrObEktovDependencies();
            RegisterVRIZUDependencies();
            RegisterSpecifikacijaDependencies();
            RegisterVidPravaDependencies();
            RegisterTipyObEktaPravaDependencies();
            RegisterDokumentyUdostoverjajushhieLichnostDependencies();
            RegisterMesjacPrirodaDependencies();
            RegisterObEktyPravaDependencies();
            RegisterOtraslDependencies();
            RegisterSubEktyPravaDependencies();
        }

#region Организации и его наследники
        private void RegisterOrganizaciiDependencies()
        {
#region Организации
            References.Add(new EntityReference{ReferenceName = "Приема и контроля исполнения заявок -> Организации", BaseEntity = typeof (Bars.Nedragis.Organizacii), CheckAnyDependences = id => Any<Bars.Nedragis.IspolnenijaZajavok>(x => x.Element1511506733063.Id == id), GetCountDependences = id => Count<Bars.Nedragis.IspolnenijaZajavok>(x => x.Element1511506733063.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Организации] так как на него есть ссылки"});
#endregion
        }

#endregion
#region ОПИ и его наследники
        private void RegisterRegGosNadzorZaGeoDependencies()
        {
#region ОПИ
            References.Add(new EntityReference{ReferenceName = "объектопи -> ОПИ", BaseEntity = typeof (Bars.Nedragis.RegGosNadzorZaGeo), CheckAnyDependences = id => Any<Bars.Nedragis.ObEktopi>(x => x.Element1508137003934.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ObEktopi>(x => x.Element1508137003934.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОПИ] так как на него есть ссылки"});
#endregion
        }

#endregion
#region ОКОГУ и его наследники
        private void RegisterOKOGUDependencies()
        {
#region ОКОГУ
            References.Add(new EntityReference{ReferenceName = "ОКОГУ -> ОКОГУ", BaseEntity = typeof (Bars.Nedragis.OKOGU), CheckAnyDependences = id => Any<Bars.Nedragis.OKOGU>(x => x.parent_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.OKOGU>(x => x.parent_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКОГУ] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Юридические лица -> ОКОГУ", BaseEntity = typeof (Bars.Nedragis.OKOGU), CheckAnyDependences = id => Any<Bars.Nedragis.JuridicheskieLica>(x => x.Okogu.Id == id), GetCountDependences = id => Count<Bars.Nedragis.JuridicheskieLica>(x => x.Okogu.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКОГУ] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Органы власти Природа и его наследники
        private void RegisterOrganyVlastiPrirodaDependencies()
        {
#region Органы власти Природа
            References.Add(new EntityReference{ReferenceName = "Реестр природопользования -> Органы власти Природа", BaseEntity = typeof (Bars.Nedragis.OrganyVlastiPriroda), CheckAnyDependences = id => Any<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.org_vlast_pr.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.org_vlast_pr.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Органы власти Природа] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Тип земельного участка и его наследники
        private void RegisterTipZemelNogoUchastkaDependencies()
        {
#region Тип земельного участка
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Тип земельного участка", BaseEntity = typeof (Bars.Nedragis.TipZemelNogoUchastka), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.TypeZU.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.TypeZU.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Тип земельного участка] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Сотрудники и его наследники
        private void RegisterSotrudnikiDependencies()
        {
#region Сотрудники
            References.Add(new EntityReference{ReferenceName = "Спецификация -> Сотрудники", BaseEntity = typeof (Bars.Nedragis.Sotrudniki), CheckAnyDependences = id => Any<Bars.Nedragis.Specifikacija>(x => x.Element1478607204176.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Specifikacija>(x => x.Element1478607204176.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Сотрудники] так как на него есть ссылки"});
#endregion
        }

#endregion
#region ОКТМО и его наследники
        private void RegisterOKTMODependencies()
        {
#region ОКТМО
            References.Add(new EntityReference{ReferenceName = "ОКТМО -> ОКТМО", BaseEntity = typeof (Bars.Nedragis.OKTMO), CheckAnyDependences = id => Any<Bars.Nedragis.OKTMO>(x => x.parent_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.OKTMO>(x => x.parent_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКТМО] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> ОКТМО", BaseEntity = typeof (Bars.Nedragis.OKTMO), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.oktmo.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.oktmo.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКТМО] так как на него есть ссылки"});
#endregion
        }

#endregion
#region ОКОФ и его наследники
        private void RegisterOKOFDependencies()
        {
#region ОКОФ
            References.Add(new EntityReference{ReferenceName = "ОКОФ -> ОКОФ", BaseEntity = typeof (Bars.Nedragis.OKOF), CheckAnyDependences = id => Any<Bars.Nedragis.OKOF>(x => x.parent_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.OKOF>(x => x.parent_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКОФ] так как на него есть ссылки"});
#endregion
        }

#endregion
#region ОКОПФ и его наследники
        private void RegisterOKOPFDependencies()
        {
#region ОКОПФ
            References.Add(new EntityReference{ReferenceName = "Юридические лица -> ОКОПФ", BaseEntity = typeof (Bars.Nedragis.OKOPF), CheckAnyDependences = id => Any<Bars.Nedragis.JuridicheskieLica>(x => x.Okopf.Id == id), GetCountDependences = id => Count<Bars.Nedragis.JuridicheskieLica>(x => x.Okopf.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКОПФ] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Документ и его наследники
        private void RegisterDokumentDependencies()
        {
#region Документ
            References.Add(new EntityReference{ReferenceName = "ДокументыЗУ -> Документ", BaseEntity = typeof (Bars.Nedragis.Dokument), CheckAnyDependences = id => Any<Bars.Nedragis.DokumentyZU>(x => x.doc1.Id == id), GetCountDependences = id => Count<Bars.Nedragis.DokumentyZU>(x => x.doc1.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Документ] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Файл, привязанный к документу -> Документ", BaseEntity = typeof (Bars.Nedragis.Dokument), DeleteAnyDependences = id => RemoveRefs<Bars.Nedragis.FajjlPrivjazannyjjKDokumentu>(x => x.Document.Id == id)});
#endregion
        }

#endregion
#region Статусы субъекта и его наследники
        private void RegisterStatusySubEktaDependencies()
        {
#region Статусы субъекта
            References.Add(new EntityReference{ReferenceName = "Юридические лица -> Статусы субъекта", BaseEntity = typeof (Bars.Nedragis.StatusySubEkta), CheckAnyDependences = id => Any<Bars.Nedragis.JuridicheskieLica>(x => x.Status.Id == id), GetCountDependences = id => Count<Bars.Nedragis.JuridicheskieLica>(x => x.Status.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Статусы субъекта] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Субъекты права -> Статусы субъекта", BaseEntity = typeof (Bars.Nedragis.StatusySubEkta), CheckAnyDependences = id => Any<Bars.Nedragis.SubEktyPrava>(x => x.Status.Id == id), GetCountDependences = id => Count<Bars.Nedragis.SubEktyPrava>(x => x.Status.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Статусы субъекта] так как на него есть ссылки"});
#endregion
        }

#endregion
#region zima1 и его наследники
        private void RegisterZima1Dependencies()
        {
#region zima1
            References.Add(new EntityReference{ReferenceName = "zim1 -> zima1", BaseEntity = typeof (Bars.Nedragis.Zima1), CheckAnyDependences = id => Any<Bars.Nedragis.Zim1>(x => x.Element1517482864074.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Zim1>(x => x.Element1517482864074.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [zima1] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Подвид работ природа и его наследники
        private void RegisterPodvidRabotPrirodaDependencies()
        {
#region Подвид работ природа
            References.Add(new EntityReference{ReferenceName = "Реестр природопользования -> Подвид работ природа", BaseEntity = typeof (Bars.Nedragis.PodvidRabotPriroda), CheckAnyDependences = id => Any<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.P_rabot_pr.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.P_rabot_pr.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Подвид работ природа] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Юридические лица и его наследники
        private void RegisterJuridicheskieLicaDependencies()
        {
#region Юридические лица
            References.Add(new EntityReference{ReferenceName = "Документ -> Юридические лица", BaseEntity = typeof (Bars.Nedragis.JuridicheskieLica), CheckAnyDependences = id => Any<Bars.Nedragis.Dokument>(x => x.Subject.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Dokument>(x => x.Subject.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Юридические лица] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Юридические лица -> Юридические лица", BaseEntity = typeof (Bars.Nedragis.JuridicheskieLica), CheckAnyDependences = id => Any<Bars.Nedragis.JuridicheskieLica>(x => x.HeadSubject.Id == id), GetCountDependences = id => Count<Bars.Nedragis.JuridicheskieLica>(x => x.HeadSubject.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Юридические лица] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Юридические лица -> Юридические лица", BaseEntity = typeof (Bars.Nedragis.JuridicheskieLica), CheckAnyDependences = id => Any<Bars.Nedragis.JuridicheskieLica>(x => x.PayeeSubject.Id == id), GetCountDependences = id => Count<Bars.Nedragis.JuridicheskieLica>(x => x.PayeeSubject.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Юридические лица] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Юридические лица", BaseEntity = typeof (Bars.Nedragis.JuridicheskieLica), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.property_manager_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.property_manager_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Юридические лица] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Объекты права -> Юридические лица", BaseEntity = typeof (Bars.Nedragis.JuridicheskieLica), CheckAnyDependences = id => Any<Bars.Nedragis.ObEktyPrava>(x => x.property_manager_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ObEktyPrava>(x => x.property_manager_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Юридические лица] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Руководители -> Юридические лица", BaseEntity = typeof (Bars.Nedragis.JuridicheskieLica), CheckAnyDependences = id => Any<Bars.Nedragis.Rukovoditeli>(x => x.YurLitco.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Rukovoditeli>(x => x.YurLitco.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Юридические лица] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Категории земель и его наследники
        private void RegisterKategoriiZemelDependencies()
        {
#region Категории земель
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Категории земель", BaseEntity = typeof (Bars.Nedragis.KategoriiZemel), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.category.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.category.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Категории земель] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Типы документов и его наследники
        private void RegisterTipyDokumentovDependencies()
        {
#region Типы документов
            References.Add(new EntityReference{ReferenceName = "Документ -> Типы документов", BaseEntity = typeof (Bars.Nedragis.TipyDokumentov), CheckAnyDependences = id => Any<Bars.Nedragis.Dokument>(x => x.Type.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Dokument>(x => x.Type.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Типы документов] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Типы документов -> Типы документов", BaseEntity = typeof (Bars.Nedragis.TipyDokumentov), CheckAnyDependences = id => Any<Bars.Nedragis.TipyDokumentov>(x => x.parent_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.TipyDokumentov>(x => x.parent_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Типы документов] так как на него есть ссылки"});
#endregion
        }

#endregion
#region ОКФС и его наследники
        private void RegisterOKFSDependencies()
        {
#region ОКФС
            References.Add(new EntityReference{ReferenceName = "Юридические лица -> ОКФС", BaseEntity = typeof (Bars.Nedragis.OKFS), CheckAnyDependences = id => Any<Bars.Nedragis.JuridicheskieLica>(x => x.Okfs.Id == id), GetCountDependences = id => Count<Bars.Nedragis.JuridicheskieLica>(x => x.Okfs.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКФС] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Реестры и его наследники
        private void RegisterReestryDependencies()
        {
#region Реестры
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Реестры", BaseEntity = typeof (Bars.Nedragis.Reestry), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.type.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.type.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Реестры] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Объекты права -> Реестры", BaseEntity = typeof (Bars.Nedragis.Reestry), CheckAnyDependences = id => Any<Bars.Nedragis.ObEktyPrava>(x => x.type.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ObEktyPrava>(x => x.type.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Реестры] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Классификация ОКОФ и его наследники
        private void RegisterKlassifikacijaOKOFDependencies()
        {
#region Классификация ОКОФ
            References.Add(new EntityReference{ReferenceName = "ОКОФ -> Классификация ОКОФ", BaseEntity = typeof (Bars.Nedragis.KlassifikacijaOKOF), CheckAnyDependences = id => Any<Bars.Nedragis.OKOF>(x => x.clasifier.Id == id), GetCountDependences = id => Count<Bars.Nedragis.OKOF>(x => x.clasifier.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Классификация ОКОФ] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Должности и его наследники
        private void RegisterDolzhnostiDependencies()
        {
#region Должности
            References.Add(new EntityReference{ReferenceName = "Руководители -> Должности", BaseEntity = typeof (Bars.Nedragis.Dolzhnosti), CheckAnyDependences = id => Any<Bars.Nedragis.Rukovoditeli>(x => x.PostType.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Rukovoditeli>(x => x.PostType.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Должности] так как на него есть ссылки"});
#endregion
        }

#endregion
#region должность и его наследники
        private void RegisterDolzhnostDependencies()
        {
#region должность
            References.Add(new EntityReference{ReferenceName = "Сотрудники -> должность", BaseEntity = typeof (Bars.Nedragis.Dolzhnost), CheckAnyDependences = id => Any<Bars.Nedragis.Sotrudniki>(x => x.dolzh.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Sotrudniki>(x => x.dolzh.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [должность] так как на него есть ссылки"});
#endregion
        }

#endregion
#region ОКВЭД и его наследники
        private void RegisterOKVEhDDependencies()
        {
#region ОКВЭД
            References.Add(new EntityReference{ReferenceName = "Юридические лица -> ОКВЭД", BaseEntity = typeof (Bars.Nedragis.OKVEhD), CheckAnyDependences = id => Any<Bars.Nedragis.JuridicheskieLica>(x => x.Okved.Id == id), GetCountDependences = id => Count<Bars.Nedragis.JuridicheskieLica>(x => x.Okved.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКВЭД] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "ОКВЭД -> ОКВЭД", BaseEntity = typeof (Bars.Nedragis.OKVEhD), CheckAnyDependences = id => Any<Bars.Nedragis.OKVEhD>(x => x.parent_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.OKVEhD>(x => x.parent_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКВЭД] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Недропользователь и его наследники
        private void RegisterNedropolZovatelDependencies()
        {
#region Недропользователь
            References.Add(new EntityReference{ReferenceName = "Заявка на приобретение ГГИ -> Недропользователь", BaseEntity = typeof (Bars.Nedragis.NedropolZovatel), CheckAnyDependences = id => Any<Bars.Nedragis.ZajavkGGI>(x => x.NedropolZovatel.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZajavkGGI>(x => x.NedropolZovatel.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Недропользователь] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Вид работ природа и его наследники
        private void RegisterVidRabotPrirDependencies()
        {
#region Вид работ природа
            References.Add(new EntityReference{ReferenceName = "Природопользование 2 -> Вид работ природа", BaseEntity = typeof (Bars.Nedragis.VidRabotPrir), CheckAnyDependences = id => Any<Bars.Nedragis.PrirodopolZovanie2>(x => x.VidRabotPR2.Id == id), GetCountDependences = id => Count<Bars.Nedragis.PrirodopolZovanie2>(x => x.VidRabotPR2.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Вид работ природа] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Реестр природопользования -> Вид работ природа", BaseEntity = typeof (Bars.Nedragis.VidRabotPrir), CheckAnyDependences = id => Any<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.vid_rabot_pr.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.vid_rabot_pr.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Вид работ природа] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Отдел и его наследники
        private void RegisterOtdel1Dependencies()
        {
#region Отдел
            References.Add(new EntityReference{ReferenceName = "Недропользования -> Отдел", BaseEntity = typeof (Bars.Nedragis.Otdel1), CheckAnyDependences = id => Any<Bars.Nedragis.NedropolZovanie>(x => x.name_n.Id == id), GetCountDependences = id => Count<Bars.Nedragis.NedropolZovanie>(x => x.name_n.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Отдел] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Мониторинг -> Отдел", BaseEntity = typeof (Bars.Nedragis.Otdel1), CheckAnyDependences = id => Any<Bars.Nedragis.monitor>(x => x.otdel.Id == id), GetCountDependences = id => Count<Bars.Nedragis.monitor>(x => x.otdel.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Отдел] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Землеустройство -> Отдел", BaseEntity = typeof (Bars.Nedragis.Otdel1), CheckAnyDependences = id => Any<Bars.Nedragis.Zemleustr>(x => x.name_z.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Zemleustr>(x => x.name_z.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Отдел] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Реестр природопользования -> Отдел", BaseEntity = typeof (Bars.Nedragis.Otdel1), CheckAnyDependences = id => Any<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.name_otdel_pr.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.name_otdel_pr.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Отдел] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Картография -> Отдел", BaseEntity = typeof (Bars.Nedragis.Otdel1), CheckAnyDependences = id => Any<Bars.Nedragis.Kartograf>(x => x.name.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Kartograf>(x => x.name.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Отдел] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Статусы объекта и его наследники
        private void RegisterStatusyObEktaDependencies()
        {
#region Статусы объекта
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Статусы объекта", BaseEntity = typeof (Bars.Nedragis.StatusyObEkta), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.Status_obj.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.Status_obj.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Статусы объекта] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Объекты права -> Статусы объекта", BaseEntity = typeof (Bars.Nedragis.StatusyObEkta), CheckAnyDependences = id => Any<Bars.Nedragis.ObEktyPrava>(x => x.Status_obj.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ObEktyPrava>(x => x.Status_obj.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Статусы объекта] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Вид работ ОПИНФ и его наследники
        private void RegisterVidRabotOPINFDependencies()
        {
#region Вид работ ОПИНФ
            References.Add(new EntityReference{ReferenceName = "Отдел подготовки информации -> Вид работ ОПИНФ", BaseEntity = typeof (Bars.Nedragis.VidRabotOPINF), CheckAnyDependences = id => Any<Bars.Nedragis.OtdelPodgotovkiInformacii>(x => x.Element1510135000178.Id == id), GetCountDependences = id => Count<Bars.Nedragis.OtdelPodgotovkiInformacii>(x => x.Element1510135000178.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Вид работ ОПИНФ] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Отдел подготовки информации -> Вид работ ОПИНФ", BaseEntity = typeof (Bars.Nedragis.VidRabotOPINF), CheckAnyDependences = id => Any<Bars.Nedragis.OtdelPodgotovkiInformacii>(x => x.Element1510135042374.Id == id), GetCountDependences = id => Count<Bars.Nedragis.OtdelPodgotovkiInformacii>(x => x.Element1510135042374.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Вид работ ОПИНФ] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Сотрудники и его наследники
        private void RegisterSotrudniki1Dependencies()
        {
#region Сотрудники
            References.Add(new EntityReference{ReferenceName = "Недропользования -> Сотрудники", BaseEntity = typeof (Bars.Nedragis.Sotrudniki1), CheckAnyDependences = id => Any<Bars.Nedragis.NedropolZovanie>(x => x.sotr_n.Id == id), GetCountDependences = id => Count<Bars.Nedragis.NedropolZovanie>(x => x.sotr_n.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Сотрудники] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Отдел подготовки информации -> Сотрудники", BaseEntity = typeof (Bars.Nedragis.Sotrudniki1), CheckAnyDependences = id => Any<Bars.Nedragis.OtdelPodgotovkiInformacii>(x => x.Element1510135121705.Id == id), GetCountDependences = id => Count<Bars.Nedragis.OtdelPodgotovkiInformacii>(x => x.Element1510135121705.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Сотрудники] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Природопользование 2 -> Сотрудники", BaseEntity = typeof (Bars.Nedragis.Sotrudniki1), CheckAnyDependences = id => Any<Bars.Nedragis.PrirodopolZovanie2>(x => x.IspolnitelPR2.Id == id), GetCountDependences = id => Count<Bars.Nedragis.PrirodopolZovanie2>(x => x.IspolnitelPR2.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Сотрудники] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Природопользование 2 -> Сотрудники", BaseEntity = typeof (Bars.Nedragis.Sotrudniki1), CheckAnyDependences = id => Any<Bars.Nedragis.PrirodopolZovanie2>(x => x.SoispolnitelPR2.Id == id), GetCountDependences = id => Count<Bars.Nedragis.PrirodopolZovanie2>(x => x.SoispolnitelPR2.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Сотрудники] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Землеустройство -> Сотрудники", BaseEntity = typeof (Bars.Nedragis.Sotrudniki1), CheckAnyDependences = id => Any<Bars.Nedragis.Zemleustr>(x => x.autor.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Zemleustr>(x => x.autor.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Сотрудники] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Реестр природопользования -> Сотрудники", BaseEntity = typeof (Bars.Nedragis.Sotrudniki1), CheckAnyDependences = id => Any<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.ispoln_pr.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.ispoln_pr.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Сотрудники] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Картография -> Сотрудники", BaseEntity = typeof (Bars.Nedragis.Sotrudniki1), CheckAnyDependences = id => Any<Bars.Nedragis.Kartograf>(x => x.Element1474352034381.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Kartograf>(x => x.Element1474352034381.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Сотрудники] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Лицензионные участки и его наследники
        private void RegisterLicUchastkiDependencies()
        {
#region Лицензионные участки
            References.Add(new EntityReference{ReferenceName = "Заявка на приобретение ГГИ -> Лицензионные участки", BaseEntity = typeof (Bars.Nedragis.LicUchastki), CheckAnyDependences = id => Any<Bars.Nedragis.ZajavkGGI>(x => x.luch.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZajavkGGI>(x => x.luch.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Лицензионные участки] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Типы субъекта права и его наследники
        private void RegisterTipySubEktaPravaDependencies()
        {
#region Типы субъекта права
            References.Add(new EntityReference{ReferenceName = "Юридические лица -> Типы субъекта права", BaseEntity = typeof (Bars.Nedragis.TipySubEktaPrava), CheckAnyDependences = id => Any<Bars.Nedragis.JuridicheskieLica>(x => x.type.Id == id), GetCountDependences = id => Count<Bars.Nedragis.JuridicheskieLica>(x => x.type.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Типы субъекта права] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Субъекты права -> Типы субъекта права", BaseEntity = typeof (Bars.Nedragis.TipySubEktaPrava), CheckAnyDependences = id => Any<Bars.Nedragis.SubEktyPrava>(x => x.type.Id == id), GetCountDependences = id => Count<Bars.Nedragis.SubEktyPrava>(x => x.type.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Типы субъекта права] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Районы ЯНАО и его наследники
        private void RegisterRajjonyJaNAODependencies()
        {
#region Районы ЯНАО
            References.Add(new EntityReference{ReferenceName = "Землеустройство -> Районы ЯНАО", BaseEntity = typeof (Bars.Nedragis.RajjonyJaNAO), CheckAnyDependences = id => Any<Bars.Nedragis.Zemleustr>(x => x.region.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Zemleustr>(x => x.region.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Районы ЯНАО] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Вид права на землю и его наследники
        private void RegisterVidPravaNaZemljuDependencies()
        {
#region Вид права на землю
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Вид права на землю", BaseEntity = typeof (Bars.Nedragis.VidPravaNaZemlju), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.vid_prava.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.vid_prava.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Вид права на землю] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Наименование объектов надзора и его наследники
        private void RegisterNaimenovanieObEktovNadzoraDependencies()
        {
#region Наименование объектов надзора
            References.Add(new EntityReference{ReferenceName = "Объекты регионального надзора -> Наименование объектов надзора", BaseEntity = typeof (Bars.Nedragis.NaimenovanieObEktovNadzora), CheckAnyDependences = id => Any<Bars.Nedragis.Nadzor>(x => x.Element1507271571833.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Nadzor>(x => x.Element1507271571833.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Наименование объектов надзора] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Реестр объектов -> Наименование объектов надзора", BaseEntity = typeof (Bars.Nedragis.NaimenovanieObEktovNadzora), CheckAnyDependences = id => Any<Bars.Nedragis.ReestrObEktov>(x => x.Element1507784253669.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ReestrObEktov>(x => x.Element1507784253669.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Наименование объектов надзора] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Наименование МО и его наследники
        private void RegisterNaimenovanieMODependencies()
        {
#region Наименование МО
            References.Add(new EntityReference{ReferenceName = "Организации -> Наименование МО", BaseEntity = typeof (Bars.Nedragis.NaimenovanieMO), CheckAnyDependences = id => Any<Bars.Nedragis.Organizacii>(x => x.Element1511331983555.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Organizacii>(x => x.Element1511331983555.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Наименование МО] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Субсидии -> Наименование МО", BaseEntity = typeof (Bars.Nedragis.NaimenovanieMO), CheckAnyDependences = id => Any<Bars.Nedragis.subsidies>(x => x.NAME_MO.Id == id), GetCountDependences = id => Count<Bars.Nedragis.subsidies>(x => x.NAME_MO.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Наименование МО] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Приема и контроля исполнения заявок -> Наименование МО", BaseEntity = typeof (Bars.Nedragis.NaimenovanieMO), CheckAnyDependences = id => Any<Bars.Nedragis.IspolnenijaZajavok>(x => x.Element1511506563562.Id == id), GetCountDependences = id => Count<Bars.Nedragis.IspolnenijaZajavok>(x => x.Element1511506563562.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Наименование МО] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Наименование проекта и его наследники
        private void RegisterNaimenovanieNapravlenijaRaskhodovanijaSredstvDependencies()
        {
#region Наименование проекта
            References.Add(new EntityReference{ReferenceName = "Субсидии -> Наименование проекта", BaseEntity = typeof (Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv), CheckAnyDependences = id => Any<Bars.Nedragis.subsidies>(x => x.Element1455708491495.Id == id), GetCountDependences = id => Count<Bars.Nedragis.subsidies>(x => x.Element1455708491495.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Наименование проекта] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Приема и контроля исполнения заявок -> Наименование проекта", BaseEntity = typeof (Bars.Nedragis.NaimenovanieNapravlenijaRaskhodovanijaSredstv), CheckAnyDependences = id => Any<Bars.Nedragis.IspolnenijaZajavok>(x => x.Element1511762285082.Id == id), GetCountDependences = id => Count<Bars.Nedragis.IspolnenijaZajavok>(x => x.Element1511762285082.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Наименование проекта] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Вид ограничения и его наследники
        private void RegisterVidOgranichenijaDependencies()
        {
#region Вид ограничения
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Вид ограничения", BaseEntity = typeof (Bars.Nedragis.VidOgranichenija), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.bremya.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.bremya.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Вид ограничения] так как на него есть ссылки"});
#endregion
        }

#endregion
#region ОКАТО и его наследники
        private void RegisterOKATODependencies()
        {
#region ОКАТО
            References.Add(new EntityReference{ReferenceName = "Юридические лица -> ОКАТО", BaseEntity = typeof (Bars.Nedragis.OKATO), CheckAnyDependences = id => Any<Bars.Nedragis.JuridicheskieLica>(x => x.OKATO.Id == id), GetCountDependences = id => Count<Bars.Nedragis.JuridicheskieLica>(x => x.OKATO.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКАТО] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "ОКАТО -> ОКАТО", BaseEntity = typeof (Bars.Nedragis.OKATO), CheckAnyDependences = id => Any<Bars.Nedragis.OKATO>(x => x.parent_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.OKATO>(x => x.parent_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКАТО] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> ОКАТО", BaseEntity = typeof (Bars.Nedragis.OKATO), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.okato.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.okato.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ОКАТО] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Земельные участки и его наследники
        private void RegisterZemelNyemUchastkiDependencies()
        {
#region Земельные участки
            References.Add(new EntityReference{ReferenceName = "ДокументыЗУ -> Земельные участки", BaseEntity = typeof (Bars.Nedragis.ZemelNyemUchastki), CheckAnyDependences = id => Any<Bars.Nedragis.DokumentyZU>(x => x.zemuch.Id == id), GetCountDependences = id => Count<Bars.Nedragis.DokumentyZU>(x => x.zemuch.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Земельные участки] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Виды работ и его наследники
        private void RegisterVidyRabotDependencies()
        {
#region Виды работ
            References.Add(new EntityReference{ReferenceName = "Подвиды -> Виды работ", BaseEntity = typeof (Bars.Nedragis.VidyRabot), CheckAnyDependences = id => Any<Bars.Nedragis.Podvidy>(x => x.Element1474349306046.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Podvidy>(x => x.Element1474349306046.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Виды работ] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Картография -> Виды работ", BaseEntity = typeof (Bars.Nedragis.VidyRabot), CheckAnyDependences = id => Any<Bars.Nedragis.Kartograf>(x => x.vid.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Kartograf>(x => x.vid.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Виды работ] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Реестр объектов и его наследники
        private void RegisterReestrObEktovDependencies()
        {
#region Реестр объектов
            References.Add(new EntityReference{ReferenceName = "объектопи -> Реестр объектов", BaseEntity = typeof (Bars.Nedragis.ReestrObEktov), CheckAnyDependences = id => Any<Bars.Nedragis.ObEktopi>(x => x.Element1508136885094.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ObEktopi>(x => x.Element1508136885094.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Реестр объектов] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Вода -> Реестр объектов", BaseEntity = typeof (Bars.Nedragis.ReestrObEktov), CheckAnyDependences = id => Any<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov>(x => x.Element1508150872914.Id == id), GetCountDependences = id => Count<Bars.Nedragis.RegionalNyjjNadzorVodnykhObEktov>(x => x.Element1508150872914.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Реестр объектов] так как на него есть ссылки"});
#endregion
        }

#endregion
#region ВРИ ЗУ и его наследники
        private void RegisterVRIZUDependencies()
        {
#region ВРИ ЗУ
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> ВРИ ЗУ", BaseEntity = typeof (Bars.Nedragis.VRIZU), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.vid_use.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.vid_use.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ВРИ ЗУ] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "ВРИ ЗУ -> ВРИ ЗУ", BaseEntity = typeof (Bars.Nedragis.VRIZU), CheckAnyDependences = id => Any<Bars.Nedragis.VRIZU>(x => x.parent.Id == id), GetCountDependences = id => Count<Bars.Nedragis.VRIZU>(x => x.parent.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [ВРИ ЗУ] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Спецификация и его наследники
        private void RegisterSpecifikacijaDependencies()
        {
#region Спецификация
            References.Add(new EntityReference{ReferenceName = "Решение по праву приобретения информации -> Спецификация", BaseEntity = typeof (Bars.Nedragis.Specifikacija), CheckAnyDependences = id => Any<Bars.Nedragis.Reshenie>(x => x.Element1478753351594.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Reshenie>(x => x.Element1478753351594.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Спецификация] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Вид права и его наследники
        private void RegisterVidPravaDependencies()
        {
#region Вид права
            References.Add(new EntityReference{ReferenceName = "табл1 -> Вид права", BaseEntity = typeof (Bars.Nedragis.VidPrava), CheckAnyDependences = id => Any<Bars.Nedragis.Tabl1>(x => x.kok.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Tabl1>(x => x.kok.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Вид права] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Типы объекта права и его наследники
        private void RegisterTipyObEktaPravaDependencies()
        {
#region Типы объекта права
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Типы объекта права", BaseEntity = typeof (Bars.Nedragis.TipyObEktaPrava), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.typeobj.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.typeobj.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Типы объекта права] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Объекты права -> Типы объекта права", BaseEntity = typeof (Bars.Nedragis.TipyObEktaPrava), CheckAnyDependences = id => Any<Bars.Nedragis.ObEktyPrava>(x => x.typeobj.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ObEktyPrava>(x => x.typeobj.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Типы объекта права] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Документы удостоверяющие личность и его наследники
        private void RegisterDokumentyUdostoverjajushhieLichnostDependencies()
        {
#region Документы удостоверяющие личность
            References.Add(new EntityReference{ReferenceName = "Руководители -> Документы удостоверяющие личность", BaseEntity = typeof (Bars.Nedragis.DokumentyUdostoverjajushhieLichnost), CheckAnyDependences = id => Any<Bars.Nedragis.Rukovoditeli>(x => x.udostovereniye.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Rukovoditeli>(x => x.udostovereniye.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Документы удостоверяющие личность] так как на него есть ссылки"});
#endregion
        }

#endregion
#region МесяцПрирода и его наследники
        private void RegisterMesjacPrirodaDependencies()
        {
#region МесяцПрирода
            References.Add(new EntityReference{ReferenceName = "Природопользование 2 -> МесяцПрирода", BaseEntity = typeof (Bars.Nedragis.MesjacPriroda), CheckAnyDependences = id => Any<Bars.Nedragis.PrirodopolZovanie2>(x => x.MonthPR2.Id == id), GetCountDependences = id => Count<Bars.Nedragis.PrirodopolZovanie2>(x => x.MonthPR2.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [МесяцПрирода] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Реестр природопользования -> МесяцПрирода", BaseEntity = typeof (Bars.Nedragis.MesjacPriroda), CheckAnyDependences = id => Any<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.o_month_pr.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ReestrPrirodopolZovanija>(x => x.o_month_pr.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [МесяцПрирода] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Объекты права и его наследники
        private void RegisterObEktyPravaDependencies()
        {
#region Объекты права
            References.Add(new EntityReference{ReferenceName = "Документ -> Объекты права", BaseEntity = typeof (Bars.Nedragis.ObEktyPrava), CheckAnyDependences = id => Any<Bars.Nedragis.Dokument>(x => x.ObjectOfLaw.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Dokument>(x => x.ObjectOfLaw.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Объекты права] так как на него есть ссылки"});
#endregion
#region Земельные участки
            References.Add(new EntityReference{ReferenceName = "Документ -> Земельные участки", BaseEntity = typeof (Bars.Nedragis.ZemelNyemUchastki), CheckAnyDependences = id => Any<Bars.Nedragis.Dokument>(x => x.ObjectOfLaw.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Dokument>(x => x.ObjectOfLaw.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Земельные участки] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Отрасль и его наследники
        private void RegisterOtraslDependencies()
        {
#region Отрасль
            References.Add(new EntityReference{ReferenceName = "Юридические лица -> Отрасль", BaseEntity = typeof (Bars.Nedragis.Otrasl), CheckAnyDependences = id => Any<Bars.Nedragis.JuridicheskieLica>(x => x.Sphere.Id == id), GetCountDependences = id => Count<Bars.Nedragis.JuridicheskieLica>(x => x.Sphere.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Отрасль] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Субъекты права и его наследники
        private void RegisterSubEktyPravaDependencies()
        {
#region Субъекты права
            References.Add(new EntityReference{ReferenceName = "Документ -> Субъекты права", BaseEntity = typeof (Bars.Nedragis.SubEktyPrava), CheckAnyDependences = id => Any<Bars.Nedragis.Dokument>(x => x.Signatory.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Dokument>(x => x.Signatory.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Субъекты права] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Субъекты права", BaseEntity = typeof (Bars.Nedragis.SubEktyPrava), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.owner_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.owner_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Субъекты права] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Субъекты права", BaseEntity = typeof (Bars.Nedragis.SubEktyPrava), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.holder_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.holder_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Субъекты права] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Объекты права -> Субъекты права", BaseEntity = typeof (Bars.Nedragis.SubEktyPrava), CheckAnyDependences = id => Any<Bars.Nedragis.ObEktyPrava>(x => x.owner_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ObEktyPrava>(x => x.owner_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Субъекты права] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Объекты права -> Субъекты права", BaseEntity = typeof (Bars.Nedragis.SubEktyPrava), CheckAnyDependences = id => Any<Bars.Nedragis.ObEktyPrava>(x => x.holder_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ObEktyPrava>(x => x.holder_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Субъекты права] так как на него есть ссылки"});
#endregion
#region Юридические лица
            References.Add(new EntityReference{ReferenceName = "Документ -> Юридические лица", BaseEntity = typeof (Bars.Nedragis.JuridicheskieLica), CheckAnyDependences = id => Any<Bars.Nedragis.Dokument>(x => x.Signatory.Id == id), GetCountDependences = id => Count<Bars.Nedragis.Dokument>(x => x.Signatory.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Юридические лица] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Юридические лица", BaseEntity = typeof (Bars.Nedragis.JuridicheskieLica), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.owner_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.owner_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Юридические лица] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Земельные участки -> Юридические лица", BaseEntity = typeof (Bars.Nedragis.JuridicheskieLica), CheckAnyDependences = id => Any<Bars.Nedragis.ZemelNyemUchastki>(x => x.holder_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ZemelNyemUchastki>(x => x.holder_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Юридические лица] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Объекты права -> Юридические лица", BaseEntity = typeof (Bars.Nedragis.JuridicheskieLica), CheckAnyDependences = id => Any<Bars.Nedragis.ObEktyPrava>(x => x.owner_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ObEktyPrava>(x => x.owner_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Юридические лица] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Объекты права -> Юридические лица", BaseEntity = typeof (Bars.Nedragis.JuridicheskieLica), CheckAnyDependences = id => Any<Bars.Nedragis.ObEktyPrava>(x => x.holder_id.Id == id), GetCountDependences = id => Count<Bars.Nedragis.ObEktyPrava>(x => x.holder_id.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Юридические лица] так как на него есть ссылки"});
#endregion
        }

#endregion
        private IQueryable<T> GetAll<T>() => Container.Resolve<IDataStore>().GetAll<T>();
        private bool Any<T>(Expression<Func<T, bool>> predicate) => GetAll<T>().Any(predicate);
        private int Count<T>(Expression<Func<T, bool>> predicate) => GetAll<T>().Count(predicate);
        private void SetNull<T>(Expression<Func<T, bool>> predicate, Action<T> action)where T : IEntity
        {
            var ds = Container.Resolve<IDataStore>();
            var records = ds.GetAll<T>().Where(predicate).ToArray();
            foreach (var record in records)
            {
                action.Invoke(record);
                ds.Update(record);
            }
        }

        private void RemoveRefs<T>(Expression<Func<T, bool>> predicate)where T : IEntity
        {
            var domainService = Container.ResolveDomain<T>();
            var entities = domainService.GetAll().Where(predicate).Select(x => new
            {
            Type = x.GetType(), Id = x.Id
            }

            ).ToArray().GroupBy(x => x.Type);
            foreach (var type in entities)
            {
                var x = type.Key;
                if (typeof (NHibernate.Proxy.INHibernateProxy).IsAssignableFrom(x))
                    x = type.Key.BaseType;
                var ds = Container.Resolve(typeof (IDomainService<>).MakeGenericType(x)) as IDomainService;
                foreach (var v in type)
                    ds.Delete(v.Id);
            }
        }
    }
}