namespace Bars.Nedragis
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Registrar;
    using Bars.B4.ResourceBundling;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [Bars.B4.Utils.Display("Nedragis")]
    [Bars.B4.Utils.Description("")]
    [Bars.B4.Utils.CustomValue("Version", "2.2018.0301.17")]
    [Bars.Rms.Core.Attributes.Uid("da4c8b01-0014-427b-9f8e-865b211b30be")]
    public partial class Module : AssemblyDefinedModule
    {
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            Container.RegisterResourceManifest<Bars.Nedragis.ResourceManifest>();
            Container.RegisterTransient<Bars.B4.License.ILicenseInfo, LicenceInformation>();
            RegisterControllers();
            RegisterDomainServices();
            RegisterNavigationProviders();
            RegisterPermissionMaps();
            RegisterVariables();
            RegisterExternalResources();
        }

        protected override void SetPredecessors()
        {
            base.SetPredecessors();
            SetPredecessor<Bars.B4.Modules.BryntumGantt.Module>();
            SetPredecessor<Bars.B4.Modules.ClientOlap.Module>();
            SetPredecessor<Bars.B4.Modules.ECM7.Module>();
            SetPredecessor<Bars.B4.Modules.ExtJs.Module>();
            SetPredecessor<Bars.B4.Modules.FIAS.Module>();
            SetPredecessor<Bars.B4.Modules.FileStorage.Module>();
            SetPredecessor<Bars.B4.Modules.FileSystemStorage.Module>();
            SetPredecessor<Bars.B4.Modules.FlexDesk.Module>();
            SetPredecessor<Bars.B4.Modules.LicenseProvider.Module>();
            SetPredecessor<Bars.B4.Modules.NH.Module>();
            SetPredecessor<Bars.B4.Modules.NHibernateChangeLog.Module>();
            SetPredecessor<Bars.B4.Modules.QueryDesigner.Module>();
            SetPredecessor<Bars.B4.Modules.ReportDesigner.Module>();
            SetPredecessor<Bars.B4.Modules.ReportPanel.Module>();
            SetPredecessor<Bars.B4.Modules.Reports.Module>();
            SetPredecessor<Bars.B4.Modules.Security.Module>();
            SetPredecessor<Bars.B4.Modules.Setup.Module>();
            SetPredecessor<Bars.B4.Modules.States.Module>();
            SetPredecessor<Bars.Rms.GeneratedApp.Module>();
        }
    }
}