namespace Bars.Nedragis.Custom
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.States;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;

    public partial class Module
    {
        protected virtual void RegisterDomainServices()
        {
        }
    }
}