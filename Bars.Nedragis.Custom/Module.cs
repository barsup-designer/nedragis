namespace Bars.Nedragis.Custom
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Registrar;
    using Bars.B4.ResourceBundling;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [Bars.B4.Utils.Display("Кастомная часть для модуля Nedragis")]
    [Bars.B4.Utils.Description("Кастомная часть для модуля Nedragis")]
    [Bars.B4.Utils.CustomValue("Version", "2.2018.0301.15")]
    [Bars.Rms.Core.Attributes.Uid("a0a85fe4-8122-5ca4-874b-8f62a486c60e")]
    public partial class Module : AssemblyDefinedModule
    {
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            Container.RegisterResourceManifest<Bars.Nedragis.Custom.ResourceManifest>();
            RegisterControllers();
            RegisterDomainServices();
            RegisterNavigationProviders();
            RegisterPermissionMaps();
            RegisterVariables();
            RegisterExternalResources();
        }

        protected override void SetPredecessors()
        {
            base.SetPredecessors();
            SetPredecessor<Bars.Nedragis.Module>();
        }
    }
}