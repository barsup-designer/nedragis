using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Bars.B4.Utils;
using Bars.B4.Modules.Ecm7.Framework;

[assembly: AssemblyTitle("Bars.Nedragis.Custom")]
[assembly: AssemblyDescription("Кастомная часть для модуля Nedragis (Кастомная часть для модуля Nedragis)")]
[assembly: AssemblyCompany("ЗАО \"БАРС Груп\"")]
[assembly: AssemblyProduct("Bars.Nedragis.Custom")]
[assembly: AssemblyCopyright("Copyright © 2018")]
[assembly: ComVisible(false)]
// формат версии ВерсияГенератора.Год.МесяцДень.КоличествоКомитов
[assembly: AssemblyVersion("2.2018.0301.15")]
// идентификатор модуля для миграций
[assembly: MigrationModule("Bars.Nedragis.Custom")]