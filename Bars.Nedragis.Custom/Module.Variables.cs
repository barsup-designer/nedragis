namespace Bars.Nedragis.Custom
{
    using Bars.B4.Application;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Variables;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Класс модуля. Регистрация переменных
    /// </summary>    
    public partial class Module
    {
        protected virtual void RegisterVariables()
        {
        }
    }
}